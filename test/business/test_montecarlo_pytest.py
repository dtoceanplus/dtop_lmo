# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Tue Mar  9 21:53:55 2021

@author: Francisco Fonseca
"""
# from montecarlo import *
# import pytest

# hindcast = pd.read_csv('WavEc13191_series_20140827132540_Irlanda.csv')

# hindcast2 = hindcast.drop(axis=0,index=0)
# hindcast2.loc[:,"datetime"] = pd.to_datetime(hindcast2[['Year', 'Month', 'Day','Hour']])
# hindcast2.u10=pd.to_numeric(hindcast2['u10']) 

# data_panda= hindcast2.iloc[0:8760,0:5]

# @pytest.mark.parametrize('data_panda', data_panda)

# def test_f_montecarlo(data_panda):
#   print(data_panda)
#   ts_p = 1
#   dt   = data_panda
#   ts_ids, data = f_montecarlo(dt,ts_p)
#   assert ts_ids == list(dt.index())

"não consigo por pytest a funcionar..."

import unittest
from dtop_lmo.business.montecarlo import *
import os

class MonteCarloTestCase(unittest.TestCase):

  def define_inputs(self):
    #Just to initialize inputs
    self.data_panda  = data_panda
    self.data_panda2 = data_panda2
    self.data_panda3 = data_panda3

  def test_f_montecarlo_1(self):
    #this test also proves that the random selection is non-repeatable
    dt   = data_panda
    ts_p = 1
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(list(np.sort(ts_ids)),list(dt.index))

  def test_f_montecarlo_0(self):
    #for zero percent, one simulation per month is run
    dt   = data_panda
    ts_p = 0
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(len(ts_ids),12)

  def test_f_montecarlo_very_low(self):
    #for zero percent, one simulation per month is run
    dt   = data_panda
    ts_p = 0.00001
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(len(ts_ids),12)

  def test_f_montecarlo_incompleteyear(self):
    #this case should produce an error due to incomplete year of data
    dt   = data_panda2
    ts_p = 1
    with self.assertRaises(Exception) as context:
      ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertTrue('At least one year of data is required to perform the analysis' in str(context.exception))

  def test_f_montecarlo_multipleyears(self):
    #repeat previous tests for multiple years of data in timeseries
    dt   = data_panda3
    ts_p = 1
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(list(np.sort(ts_ids)),list(dt.index))
    ts_p = 0
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(len(ts_ids),12)
    ts_p = 0.00001
    ts_ids, data = f_montecarlo(dt, ts_p)
    self.assertEqual(len(ts_ids),12)


if __name__ == '__main__':
  import os
  test_file_path = os.getcwd() + '/test/files'
  hindcast = pd.read_csv(test_file_path + '/metocean_sample.csv')
  hindcast2 = hindcast.drop(axis=0,index=0)
  hindcast2.loc[:,"datetime"] = pd.to_datetime(hindcast2[['Year', 'Month', 'Day','Hour']])
  hindcast2.u10=pd.to_numeric(hindcast2['u10']) 

  data_panda  = hindcast2.iloc[0:8760, 0:5]
  data_panda2 = hindcast2.iloc[0:333, 0:5]
  data_panda3 = hindcast2.iloc[:, 0:5]
  unittest.main()
