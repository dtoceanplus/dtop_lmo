# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json

# Import classes
from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Site

Operation = Operation_class.get_complexity('low')
Object = Object_class.get_complexity('low')

op1 = Operation(operation_id='OP01_1',
                operation_name='Foundation installation',
                operation_description='Pile',
                objects=[Object('F11', 'bar11', 'foo11', 'SK'),
                         Object('F12', 'bar12', 'foo12', 'SK')])
op2 = Operation(operation_id='OP02_1',
                operation_name='Mooring installation',
                operation_description='Pre-installed',
                objects=[Object('M21', 'bar21', 'foo21', 'SK'),
                         Object('M22', 'bar22', 'foo22', 'SK')])
op3 = Operation(operation_id='OP04_1',
                operation_name='Collection point installation',
                operation_description='Collection point',
                objects=[Object('C1', 'bar1', 'foo1', 'ED')])


def test_output_table_cpx1():
  op1.vec = 'vc_dummy1'
  op1.terminal = 'term_dummy1'
  op1.consumption = 1500
  op1.durations = {}
  op1.waitings = {}
  op1.durations["1"] = (10 * 24)
  op1.durations["2"] = (7 * 24)
  op1.durations["3"] = 20
  op1.durations["4"] = (5.5 * 24)
  op1.waitings["1"] = (3 * 24)
  op1.waitings["2"] = (2 * 24)
  op1.waitings["3"] = 8
  op1.waitings["4"] = (2.5 * 24)
  op1.costs["vessels"] = {}
  op1.costs["equipment"] = {}
  op1.costs["terminal"] = {}
  for key, value in op1.durations.items():
    op1.costs["vessels"][key] = (value / 24) * 15000
    op1.costs["equipment"][key] = (value / 24) * 800
    op1.costs["terminal"][key] = (value / 24) * (15000 + 800) * 0.005

  op2.vec = 'vc_dummy2'
  op2.terminal = 'term_dummy2'
  op2.consumption = 1200
  op2.durations = {}
  op2.waitings = {}
  op2.durations["1"] = (15 * 24)
  op2.durations["2"] = (10.5 * 24)
  op2.durations["3"] = 24
  op2.durations["4"] = (7 * 24)
  op2.waitings["1"] = (5 * 24)
  op2.waitings["2"] = (3 * 24)
  op2.waitings["3"] = 8
  op2.waitings["4"] = 48
  op2.costs["vessels"] = {}
  op2.costs["equipment"] = {}
  op2.costs["terminal"] = {}
  for key, value in op2.durations.items():
    op2.costs["vessels"][key] = (value / 24) * 10000
    op2.costs["equipment"][key] = (value / 24) * 1500
    op2.costs["terminal"][key] = (value / 24) * (10000 + 1500) * 0.005

  op3.vec = 'vc_dummy3'
  op3.terminal = 'term_dummy3'
  op3.consumption = 1000
  op3.durations = {}
  op3.waitings = {}
  op3.durations["1"] = (15 * 24)
  op3.durations["2"] = (10.5 * 24)
  op3.durations["3"] = 24
  op3.durations["4"] = (7 * 24)
  op3.waitings["1"] = (5 * 24)
  op3.waitings["2"] = (3 * 24)
  op3.waitings["3"] = 8
  op3.waitings["4"] = 48
  op3.costs["vessels"] = {}
  op3.costs["equipment"] = {}
  op3.costs["terminal"] = {}
  for key, value in op3.durations.items():
    op3.costs["vessels"][key] = (value / 24) * 20000
    op3.costs["equipment"][key] = 0
    op3.costs["terminal"][key] = (value / 24) * (20000) * 0.005

  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='05/2021',
      period='quarter'
  )
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy,
  )

  Inst_Funcs.site = site
  Inst_Funcs.operations = [op1, op2, op3]
  Inst_Funcs.define_operations_dates()

  inst_table = Inst_Funcs.build_output_table()

  assert inst_table['cost_vessel'].iat[0] == 105000
  assert inst_table['cost_vessel'].iat[1] == 105000
  assert inst_table['cost_vessel'].iat[2] == 210000


def test_output_table_cpx3():

  assert True
