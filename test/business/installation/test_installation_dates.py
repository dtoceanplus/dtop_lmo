# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json

# Import classes
from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Site

import copy

Operation = Operation_class.get_complexity('high')
Object = Object_class.get_complexity('high')

op_dummy1 = Operation(operation_id='id',
                      operation_name='op_name',
                      operation_description='op_desc',
                      objects=['dummy'])

op_dummy1.durations = {"total": {}}
op_dummy1.waitings = {"to_start": {}}
op_dummy1.durations["total"]["1"] = (10 * 24)
op_dummy1.durations["total"]["2"] = (7 * 24)
op_dummy1.durations["total"]["3"] = 20
op_dummy1.durations["total"]["4"] = (5.5 * 24)
op_dummy1.waitings["to_start"]["1"] = (3 * 24)
op_dummy1.waitings["to_start"]["2"] = (2 * 24)
op_dummy1.waitings["to_start"]["3"] = 8
op_dummy1.waitings["to_start"]["4"] = (2.5 * 24)

op_dummy2 = Operation(operation_id='id',
                      operation_name='op_name',
                      operation_description='op_desc',
                      objects=['dummy'])
op_dummy2.durations = {"total": {}}
op_dummy2.waitings = {"to_start": {}}
op_dummy2.durations["total"]["1"] = (15 * 24)
op_dummy2.durations["total"]["2"] = (10.5 * 24)
op_dummy2.durations["total"]["3"] = 24
op_dummy2.durations["total"]["4"] = (7 * 24)
op_dummy2.waitings["to_start"]["1"] = (5 * 24)
op_dummy2.waitings["to_start"]["2"] = (3 * 24)
op_dummy2.waitings["to_start"]["3"] = 8
op_dummy2.waitings["to_start"]["4"] = 48


def test_inst_dates_cpx1():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='5/2022',
      period='quarter'
  )
  Operation = Operation_class.get_complexity('low')
  Object = Object_class.get_complexity('low')
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  op_dummy1 = Operation(operation_id='id',
                        operation_name='op_name',
                        operation_description='op_desc',
                        objects=['dummy'])

  op_dummy1.durations = {}
  op_dummy1.waitings = {}

  op_dummy2 = copy.deepcopy(op_dummy1)

  op_dummy1.durations["1"] = (10 * 24)
  op_dummy1.durations["2"] = (7 * 24)
  op_dummy1.durations["3"] = 20
  op_dummy1.durations["4"] = (5.5 * 24)
  op_dummy1.waitings["1"] = (3 * 24)
  op_dummy1.waitings["2"] = (2 * 24)
  op_dummy1.waitings["3"] = 8
  op_dummy1.waitings["4"] = (2.5 * 24)

  op_dummy2.durations["1"] = (15 * 24)
  op_dummy2.durations["2"] = (10.5 * 24)
  op_dummy2.durations["3"] = 24
  op_dummy2.durations["4"] = (7 * 24)
  op_dummy2.waitings["1"] = (5 * 24)
  op_dummy2.waitings["2"] = (3 * 24)
  op_dummy2.waitings["3"] = 8
  op_dummy2.waitings["4"] = 48

  Inst_Funcs.operations = [op_dummy1, op_dummy2]
  Inst_Funcs.define_operations_dates()

  [op_dummy1, op_dummy2] = Inst_Funcs.operations

  assert op_dummy1.dates.may_start.hour == 8
  assert op_dummy1.dates.start.day == 3
  assert op_dummy1.dates.start.hour == 8
  assert op_dummy1.dates.end.day == 8
  assert op_dummy1.dates.end.hour == 8

  assert op_dummy2.dates.may_start.day == 8
  assert op_dummy2.dates.may_start.hour == 8
  assert op_dummy2.dates.start.day == 11
  assert op_dummy2.dates.start.hour == 8
  assert op_dummy2.dates.end.day == 18
  assert op_dummy2.dates.end.hour == 20


def test_inst_dates():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='high',
      start_date='1/6/2022',
      period='quarter'
  )
  site = Site(
      name='Dummy_site_1year',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  operations = [op_dummy1, op_dummy2]
  operations = Inst_Funcs.define_operations_dates(operations)

  assert operations[0].dates.may_start.hour == 8
  assert operations[0].dates.start.day == 3
  assert operations[0].dates.start.hour == 8
  assert operations[0].dates.end.day == 8
  assert operations[0].dates.end.hour == 8

  assert operations[1].dates.may_start.day == 8
  assert operations[1].dates.may_start.hour == 8
  assert operations[1].dates.start.day == 11
  assert operations[1].dates.start.hour == 8
  assert operations[1].dates.end.day == 18
  assert operations[1].dates.end.hour == 20


def test_inst_dates_month_change():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='high',
      start_date='31/8/2022',
      period='quarter'
  )
  site = Site(
      name='Dummy_site_1year',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  operations = [op_dummy1, op_dummy2]
  operations = Inst_Funcs.define_operations_dates(operations)

  assert operations[0].dates.start.day == 31
  assert operations[0].dates.start.hour == 16
  assert operations[1].dates.end.month == 9
  assert operations[0].dates.end.day == 1
  assert operations[0].dates.end.hour == 4

  assert operations[1].dates.start.day == 1
  assert operations[1].dates.start.hour == 12
  assert operations[1].dates.end.month == 9
  assert operations[1].dates.end.day == 2
  assert operations[1].dates.end.hour == 4


def test_inst_dates_year_change():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='high',
      start_date='25/12/2022',
      period='quarter'
  )
  site = Site(
      name='Dummy_site_1year',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  operations = [op_dummy1, op_dummy2]
  operations = Inst_Funcs.define_operations_dates(operations)

  assert operations[0].dates.start.day == 27
  assert operations[0].dates.start.hour == 20
  assert operations[0].dates.end.day == 30
  assert operations[0].dates.end.hour == 20

  assert operations[1].dates.end.year == 2023
  assert operations[1].dates.end.month == 1
  assert operations[1].dates.end.day == 6
  assert operations[1].dates.end.hour == 20


def test_inst_dates_monthly():
  op_dummy1.durations = {"total": {}}
  op_dummy1.waitings = {"to_start": {}}

  for key in range(1, 13):
    op_dummy1.durations["total"][str(key)] = (key * 0.5) + (5 * 24)
    op_dummy1.waitings["to_start"][str(key)] = (key * 0.25) + 24

  op_dummy2.durations = {"total": {}}
  op_dummy2.waitings = {"to_start": {}}
  for key in range(1, 13):
    op_dummy2.durations["total"][str(key)] = (key * 0.5) + (7 * 24)
    op_dummy2.waitings["to_start"][str(key)] = (key * 0.25) + (3 * 24)

  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='high',
      start_date='1/6/2022',
      period='month'
  )
  site = Site(
      name='Dummy_site_1year',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  operations = [op_dummy1, op_dummy2]
  operations = Inst_Funcs.define_operations_dates(operations)

  assert op_dummy1.durations["total"]["6"] == 123

  assert operations[1].dates.end.month == 6
  assert operations[1].dates.end.day == 13
  assert operations[1].dates.end.hour == 14


def test_inst_dates_trimester():
  op_dummy1.durations = {"total": {}}
  op_dummy1.waitings = {"to_start": {}}

  for key in range(1, 13):
    op_dummy1.durations["total"][str(key)] = (key * 0.5) + (5 * 24)
    op_dummy1.waitings["to_start"][str(key)] = (key * 0.25) + 24

  op_dummy2.durations = {"total": {}}
  op_dummy2.waitings = {"to_start": {}}
  for key in range(1, 13):
    op_dummy2.durations["total"][str(key)] = (key * 0.5) + (7 * 24)
    op_dummy2.waitings["to_start"][str(key)] = (key * 0.25) + (3 * 24)

  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='high',
      start_date='1/6/2022',
      period='trimester'
  )
  site = Site(
      name='Dummy_site_1year',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site

  operations = [op_dummy1, op_dummy2]
  operations = Inst_Funcs.define_operations_dates(operations)

  assert op_dummy1.durations["total"]["2"] == 121

  assert operations[1].dates.end.month == 6
  assert operations[1].dates.end.day == 13
  assert operations[1].dates.end.hour == 10
