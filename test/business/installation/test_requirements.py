# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json

# Import classes
from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

Object = Object_class.get_complexity(complexity='low')
Cable = Cable_class.get_complexity(complexity='low')

device = Object(id_='dummy',
                name_of_object='device01',
                type_of_object='floating wave',
                system='et',
                drymass=10000,
                topside_exists=True)
pile = Object(id_='dummy',
              name_of_object='foundation',
              type_of_object='pile',
              system='sk',
              drymass=50000,
              bathymetry=50)
anchor = Object(id_='dummy',
                name_of_object='drag-anchor',
                type_of_object='drag-anchor',
                system='sk',
                drymass=20000,
                upstream=['mooring001'])
mooring_line = Object(id_='mooring001',
                      name_of_object='mooring line',
                      type_of_object='drag-embedment',
                      system='sk',
                      drymass=80000)


def test_operation_requirements_cpx1():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='06/2021'
  )
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site
  Inst_Funcs.objects = [device]
  Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
  assert Inst_Funcs.operations[0].requirements['lift'] == 10
  assert Inst_Funcs.operations[0].requirements['depth_max'] == 0
  assert Inst_Funcs.operations[0].methods['assembly'] == 'pre'
  assert Inst_Funcs.operations[0].methods['load_out'] == 'lift'
  assert Inst_Funcs.operations[0].methods['transport'] == 'dry'

  Inst_Funcs.objects = [pile]
  Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
  assert Inst_Funcs.operations[0].requirements['lift'] == 50
  assert Inst_Funcs.operations[0].requirements['depth_max'] == 50

  Inst_Funcs.objects = [anchor, mooring_line]
  Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
  assert Inst_Funcs.operations[0].requirements['lift'] == 100

  mooring_line.type = 'pre-installed'
  Inst_Funcs.objects = [mooring_line]
  Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
  assert Inst_Funcs.operations[0].requirements['rov'] == 'work'
