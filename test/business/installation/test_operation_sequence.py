# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json

# Import classes
from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

Object = Object_class.get_complexity(complexity='low')
Cable = Cable_class.get_complexity(complexity='low')

device = Object(id_='dummy',
                name_of_object='device01',
                type_of_object='floating wave',
                system='et',
                topside_exists=True)
pile = Object(id_='dummy',
              name_of_object='foundation',
              type_of_object='pile',
              system='sk')
anchor = Object(id_='dummy',
                name_of_object='drag-anchor',
                type_of_object='drag-anchor',
                system='sk')
mooring_line = Object(id_='dummy',
                      name_of_object='mooring line',
                      type_of_object='pre-installed',
                      system='sk')
collection_point = Object(id_='dummy',
                          name_of_object='collection point',
                          type_of_object='hub',
                          system='ed')
export_cable = Cable(id_='dummy',
                     name='export cable',
                     cable_type='export',
                     system='ed',
                     bathymetry=20,
                     length=12000,
                     diameter=200)
array_cable = Cable(id_='dummy',
                    name='array cable',
                    cable_type='array',
                    system='ed',
                    bathymetry=20,
                    length=1000,
                    diameter=120)


def test_sequence_cpx1():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='06/2021'
  )
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site
  Inst_Funcs.objects = [device, pile, mooring_line, collection_point]
  Inst_Funcs.get_installation_operations_sequence()

  assert 'op01' in Inst_Funcs.operations_sequence
  assert 'op02' in Inst_Funcs.operations_sequence
  assert 'op03' not in Inst_Funcs.operations_sequence
  assert 'op04' not in Inst_Funcs.operations_sequence
  assert 'op05' in Inst_Funcs.operations_sequence
  assert 'op06' not in Inst_Funcs.operations_sequence
  assert 'op07' not in Inst_Funcs.operations_sequence

  Inst_Funcs.objects = [device, pile, mooring_line, collection_point]
  collection_point.type = 'substation'
  Inst_Funcs.get_installation_operations_sequence()
  assert 'op04' in Inst_Funcs.operations_sequence

  Inst_Funcs.objects = [anchor, mooring_line]
  Inst_Funcs.get_installation_operations_sequence()
  assert 'op01' not in Inst_Funcs.operations_sequence
  assert 'op02' in Inst_Funcs.operations_sequence


def test_operation_objects_cpx1():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='06/2021'
  )
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  Inst_Funcs.site = site
  Inst_Funcs.objects = [device, pile, mooring_line, collection_point]
  Inst_Funcs.cables = [export_cable] + [array_cable] * 3
  Inst_Funcs.operations_sequence = {
      "OP01": "foundation installation",
      "OP02": "mooring installation",
      "OP05": "device installation",
      "OP06": "export cable installation",
      "OP07": "array cable installation"
  }
  Inst_Funcs.set_installation_operations()

  assert Inst_Funcs.operations[0].id == 'OP01_00'
  assert Inst_Funcs.operations[3].id == 'OP06_00'
  assert len(Inst_Funcs.operations[0].objects) == 1
  assert len(Inst_Funcs.operations[4].cables) == 3

  found_object_names = ['foundation' in obj.name
                        for obj in Inst_Funcs.operations[0].objects]
  moor_object_names = ['mooring' in obj.name
                       for obj in Inst_Funcs.operations[1].objects]

  assert all(found_object_names) is True
  assert all(moor_object_names) is True

  collection_point.type = 'substation'
  Inst_Funcs.objects = [device, anchor, mooring_line, collection_point]
  Inst_Funcs.cables = None
  Inst_Funcs.operations_sequence = {
      "OP02": "mooring installation",
      "OP04": "collection point installation",
      "OP05": "device installation"
  }
  Inst_Funcs.set_installation_operations()

  assert Inst_Funcs.operations[0].id == 'OP02_00'
  assert Inst_Funcs.operations[1].id == 'OP04_00'
  assert Inst_Funcs.operations[2].id == 'OP05_00'
