# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json


import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy.reset_index(inplace=True, drop=True)

Inst_Funcs = Installation.get_complexity(
    complexity='high',
    start_date='dummy'
)

site = Site(
    name='Dummy_site_1year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy
)
Inst_Funcs.site = site

Object = Object_class.get_complexity('high')
Cable = Cable_class.get_complexity('high')

# Define a set of Objects to be used in tests
obj_device1 = Object(id_='oec1',
                     name_of_object='device 1',
                     type_of_object='float',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30000,
                     elect_interfaces=['jtube', 'itube'],
                     topside_exists=False)

# Define a set of Cables to be used in tests
cable_export = Cable(id_='001',
                     name='cable',
                     cable_type='export',
                     system=None,
                     burial_depth=[1.5] * 10,
                     burial_method='plough',
                     bathymetry=[30, 30, 40, 50, 60, 70, 80, 30, 40, 50],
                     seabed_type=['sands'] * 10,
                     diameter=200,
                     length=[1000] * 10,
                     drymass=300,
                     min_bend_radius=2,
                     elect_interfaces=['dry_mate', 'dry_mate'])


def test_operations_sequence_cpx3_objects():
  Inst_Funcs.objects = [obj_device1]
  operation_sequence = Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()


def test_operations_sequence_cpx3_cables():
  Inst_Funcs.cables = [cable_export]
  operation_sequence = Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()


def test_operations_sequence_cpx3():
  Inst_Funcs.objects = [obj_device1]
  Inst_Funcs.cables = [cable_export]
  operation_sequence = Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
