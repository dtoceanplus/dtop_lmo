# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import classes
from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json


Core_Funcs = Core.get_complexity(complexity='low')

Operation = Operation_class.get_complexity('low')
Object = Object_class.get_complexity('low')

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)

site = Site(
    name='Dummy_site',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)

obj_dummy = Object(id_='d001',
                   name_of_object='device1',
                   type_of_object='floating',
                   system='ET',
                   topside_exists=True)

op_dummy = Operation(operation_id='OP05',
                     operation_name='Device Installation',
                     operation_description='device',
                     objects=[obj_dummy])

op_dummy.allocate_site(site)
op_dummy.get_feasible_solutions(0.5)
op_dummy.duration_net = 100


def test_optimal_solution():
  operations = Core_Funcs.select_optimal_terminal([op_dummy])
  operations = Core_Funcs.select_optimal_solution(operations)

  assert operations[0].vec == 'vec_001'
  assert operations[0].terminal == 't118'
  assert operations[0].vessel == 'v11_0'
  assert operations[0].equipment == ['rov_001']

  return operations


def test_total_consumption():
  operations = test_optimal_solution()
  op_dummy = operations[0]
  [op_dummy] = Core_Funcs.allocate_consumptions([op_dummy])

  assert op_dummy.consumption == 11380 * 0.8 * 210 * 24 * (1 / 1000)**2
