# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json
import copy
from random import randrange

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)

site = Site(
    name='Dummy_site_1year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)


def test_operation_costs():
  Core_Funcs = Core.get_complexity('low')
  Operation = Operation_class.get_complexity('low')
  Object = Object_class.get_complexity('low')
  Cable = Cable_class.get_complexity('low')

  obj_device = Object(id_='dev001',
                      name_of_object='device 001',
                      type_of_object='floating',
                      system='ET',
                      topside_exists=True)
  cbl_export = Cable(id_='ce001',
                     name='export cable',
                     cable_type='export',
                     system='ED',
                     bathymetry=20,
                     length=10000,
                     diameter=200)
  obj_pile = Object(id_='found001',
                    name_of_object='foundation 001',
                    type_of_object='pile',
                    system='SK')

  op_device = Operation(operation_id='OP05',
                        operation_name='device installation',
                        operation_description='device',
                        objects=[obj_device])
  op_cable = Operation(operation_id='OP06',
                       operation_name='cable installation',
                       operation_description='simultaneous',
                       cables=[cbl_export])
  op_pile = Operation(operation_id='OP01',
                      operation_name='foundation installation',
                      operation_description='pile',
                      objects=[obj_pile])

  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )

  op_device.terminal = 'dummy_ter'
  op_device.terminal_dist = 10000
  op_device.vec = 'vec_001'
  op_device.vessel = 'v11_3'
  op_device.equipment = ['rov_001']
  op_device.site = site
  op_device.get_feasible_solutions(0.5)

  op_cable.terminal = 'dummy_ter'
  op_cable.terminal_dist = 10000
  op_cable.vec = 'vec_012'
  op_cable.vessel = 'v03_1'
  op_cable.equipment = ['bur_002']
  op_cable.site = site
  op_cable.get_feasible_solutions(0.5)

  op_pile.terminal = 'dummy_ter'
  op_pile.terminal_dist = 10000
  op_pile.vec = 'vec_004'
  op_pile.vessel = 'v11_3'
  op_pile.equipment = ['pil_002']
  op_pile.site = site
  op_pile.get_feasible_solutions(0.5)

  op_device.durations = {
      '1': 1249.5, '2': 733.0, '3': 604.0, '4': 244.0,
      '5': 249.5, '6': 78.0, '7': 130.0, '8': 112.0,
      '9': 270.0, '10': 299.5, '11': 1902.0, '12': 1611.5
  }
  op_device.waitings = {
      '1': 1171.5, '2': 655.0, '3': 526.0, '4': 166.0,
      '5': 171.5, '6': 0.0, '7': 52.0, '8': 34.0,
      '9': 192.0, '10': 221.5, '11': 1824.0, '12': 1533.5
  }
  op_cable.durations = op_device.durations
  op_cable.waitings = op_device.waitings
  op_pile.durations = op_device.durations
  op_pile.waitings = op_device.waitings

  operations = Core_Funcs.allocate_costs([op_device, op_cable, op_pile])
