# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import json

# Import classes
from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

# Import functions
from dtop_lmo.business import feasibilities as feas_funcs

Object = Object_class.get_complexity('high')
Cable = Cable_class.get_complexity('high')

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)

site = Site(
    name='Dummy_site',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)

# Define a set of Objects to be used in tests
obj_device1 = Object(id_='oec1',
                     name_of_object='device 1',
                     type_of_object='float',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=5,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30000,
                     elect_interfaces=['jtube', 'itube'],
                     topside_exists=False)
obj_device2 = Object(id_='oec2',
                     name_of_object='device 2',
                     type_of_object='float',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=5,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30000,
                     elect_interfaces=['dry-mate', 'dry-mate'],
                     topside_exists=False)

obj_pile1 = Object(id_='pile1',
                   name_of_object='anchor 1',
                   type_of_object='pile',
                   system=None,
                   length=70,
                   diameter=3,
                   bathymetry=50,
                   seabed_type='sands',
                   drymass=205300,
                   burial_depth=10)
obj_pile2 = Object(id_='pile2',
                   name_of_object='anchor 2',
                   type_of_object='pile',
                   system=None,
                   length=60,
                   diameter=3.5,
                   bathymetry=55,
                   seabed_type='sands',
                   drymass=230000,
                   burial_depth=10)

obj_caisson = Object(id_='sc1',
                     name_of_object='foundation 1',
                     type_of_object='suction-caisson',
                     system=None,
                     length=10,
                     diameter=1.3,
                     bathymetry=30,
                     seabed_type='sands',
                     drymass=3600)

obj_mooring1 = Object(id_='ml1',
                      name_of_object='mooring 1',
                      type_of_object='drag-embedment',
                      system=None,
                      length=200,
                      diameter=0.3,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=10000,
                      material='steel')

obj_anchor1 = Object(id_='anch1',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=4,
                     height=4,
                     width=4,
                     drymass=10000,
                     bathymetry=50,
                     seabed_type='sands',
                     upstream=['ml1'],
                     material='steel',
                     parent='ml1')
obj_anchor2 = Object(id_='anch2',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=5,
                     height=5,
                     width=5,
                     drymass=20000,
                     bathymetry=55,
                     seabed_type='sands',
                     upstream=['ml1'],
                     material='steel',
                     parent='ml2')

collectionpoint = Object(id_='cp1',
                         name_of_object='collection point',
                         type_of_object='floating',
                         system=None,
                         length=30,
                         height=10,
                         width=30,
                         drymass=3000000,
                         bathymetry=55,
                         seabed_type='sands',
                         elect_interfaces=['wet-mate',
                                           'wet-mate',
                                           'wet-mate'])

cable_export = Cable(id_='001',
                     name='cable',
                     cable_type='export',
                     system=None,
                     burial_depth=[1.5] * 10,
                     burial_method='plough',
                     bathymetry=[30, 30, 40, 50, 60, 70, 80, 30, 40, 50],
                     seabed_type=['sands'] * 10,
                     diameter=200,
                     length=[1000] * 10,
                     drymass=800,
                     min_bend_radius=2,
                     elect_interfaces=['dry_mate', 'dry_mate'])

cable_array = Cable(id_='002',
                    name='cable',
                    cable_type='array',
                    system=None,
                    bathymetry=[10, 15, 18, 20, 20, 20, 18, 15, 10],
                    seabed_type=['sands'] * 9,
                    diameter=75,
                    length=[100] * 9,
                    drymass=150,
                    min_bend_radius=1,
                    elect_interfaces=['wet_mate', 'wet_mate'],
                    burial_method='cutting',
                    burial_depth=[1] * 9)


def test_feasibilities_cpx1():
  Operation = Operation_class.get_complexity('low')

  op_device_install = Operation(operation_id='OP_DI1',
                                operation_name='Device Installation',
                                operation_description='device',
                                objects=[obj_device1, obj_device2])
  op_device_install.allocate_site(site)
  op_device_install.get_feasible_solutions(0.5)
  assert 'vec' in op_device_install.feasible_funcs
  assert 'terminal' in op_device_install.feasible_funcs
  assert 'vessel' in op_device_install.feasible_funcs
  assert 'equip_rov' in op_device_install.feasible_funcs
  assert 'equip_piling' not in op_device_install.feasible_funcs

  vec_table = op_device_install.feasible_solutions.vectable
  terminal_table = op_device_install.feasible_solutions.terminaltable
  rov_table = op_device_install.feasible_solutions.rovtable
  assert vec_table.shape[0] == 1
  dry_transport = ['dry' in item or 'deck' in item
                   for item in vec_table['transportation'].tolist()]
  assert all(dry_transport)
  wet_transport = ['wet' in item
                   for item in vec_table['transportation'].tolist()]
  assert any(wet_transport) is False
  assert op_device_install.feasible_solutions.vectable.shape[0] == 1
  assert (terminal_table['dist_to_port'] < 2000000).all()
  assert (rov_table['_class'] == 'inspection').all()

  op_pile_install = Operation(operation_id='OP_FI1',
                              operation_name='Foundation Installation',
                              operation_description='pile',
                              objects=[obj_mooring1, obj_pile2])
  op_pile_install.allocate_site(site)

  op_pile_install.get_feasible_solutions(0.5)
  assert 'equip_rov' in op_pile_install.feasible_funcs
  assert 'equip_piling' in op_pile_install.feasible_funcs
  piling_table = op_pile_install.feasible_solutions.pilingtable
  assert piling_table.shape[0] == 3
  piling_tool = ['hammer' in item
                 for item in piling_table['type'].tolist()]
  assert all(piling_tool) is True

  op_moor_anchor_install = Operation(operation_id='OP_MI1',
                                     operation_name='Mooring Installation',
                                     operation_description='drag-embedment',
                                     objects=[obj_mooring1, obj_anchor1])
  op_moor_anchor_install.allocate_site(site)
  op_moor_anchor_install.get_feasible_solutions(0.5)
  assert 'equip_rov' in op_moor_anchor_install.feasible_funcs
  assert 'equip_piling' not in op_moor_anchor_install.feasible_funcs
  vessel_main_table = op_moor_anchor_install.feasible_solutions.vessels_main
  ahts_in_main = ['ahts' in item
                  for item in vessel_main_table['vessel_type'].tolist()]
  sov_in_main = ['sov' in item
                 for item in vessel_main_table['vessel_type'].tolist()]
  assert all(ahts_in_main) is True
  assert any(sov_in_main) is False

  cable_export.length = 10000
  cable_export.bathymetry = 40
  op_cable_install = Operation(operation_id='OP_CI1',
                               operation_name='Cable Installation',
                               operation_description='export',
                               cables=[cable_export])
  op_cable_install.allocate_site(site)
  op_cable_install.get_feasible_solutions(0.5)
  assert 'equip_rov' in op_cable_install.feasible_funcs
  assert 'equip_burial' in op_cable_install.feasible_funcs
  vessel_main_table = op_cable_install.feasible_solutions.vessels_main
  clv_in_main = ['clv' in item.lower()
                  for item in vessel_main_table['vessel_type'].tolist()]
  assert all(clv_in_main) is True


def test_run_feasibilities_cpx1():
  Core_Funcs = Core.get_complexity('low')
  Operation = Operation_class.get_complexity('low')

  op_device_install = Operation(operation_id='OP_DI1',
                                operation_name='Device Installation',
                                operation_description='device',
                                objects=[obj_device1, obj_device2])
  op_device_install.allocate_site(site)
  operations = Core_Funcs.run_feasibility_functions([op_device_install])


Operation = Operation_class.get_complexity('high')
# Define a set of Operations to be used in tests
op_device_install = Operation(operation_id='OP_DI1',
                              operation_name='Device Installation',
                              operation_description='device',
                              objects=[obj_device1, obj_device2])
op_foundation_install = Operation(operation_id='OP_FI1',
                                  operation_name='Foundation Installation',
                                  operation_description='pile',
                                  objects=[obj_pile1, obj_pile2])
op_foundation_install_2 = Operation(operation_id='OP_FI2',
                                    operation_name='Foundation Installation',
                                    operation_description='suction-caisson',
                                    objects=[obj_caisson])
op_mooring_install = Operation(operation_id='OP_MI1',
                               operation_name='Mooring Installation',
                               operation_description='Drag-embedment',
                               objects=[obj_mooring1,
                                        obj_anchor1,
                                        obj_anchor2])
op_cp_install = Operation(operation_id='OP_CPI1',
                          operation_name='Collection point Installation',
                          operation_description='Collection point',
                          objects=[collectionpoint])
op_export_install = Operation(operation_id='OP_ECI1',
                              operation_name='Cable Installation',
                              operation_description='Simultaneous',
                              cables=cable_export)
op_array_install = Operation(operation_id='OP_ACI1',
                             operation_name='Cable Installation',
                             operation_description='Simultaneous',
                             cables=cable_array)
op_array_install_2 = Operation(operation_id='OP_ACI2',
                               operation_name='Cable Installation',
                               operation_description='Post-lay burial',
                               cables=cable_array)
op_burial = Operation(operation_id='OP_B1',
                      operation_name='Post-lay burial',
                      operation_description='not required')

op_device_install.allocate_site(site)
op_foundation_install.allocate_site(site)
op_foundation_install_2.allocate_site(site)
op_mooring_install.allocate_site(site)
op_cp_install.allocate_site(site)
op_export_install.allocate_site(site)
op_array_install.allocate_site(site)
op_array_install_2.allocate_site(site)
op_burial.allocate_site(site)


def test_feasibility_functions():
  op_example = op_device_install
  op_example.update_methods_requirements_user()

  op_example.feasibility_selector()

  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_rov',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs

  op_example = op_foundation_install
  op_example.update_methods_requirements_user(method_piling='vibro-driver')

  op_example.feasibility_selector()

  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_rov',
      'equip_piling',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs

  op_example = op_foundation_install_2
  op_example.update_methods_requirements_user()

  op_example.feasibility_selector()

  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_rov',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs

  op_example = op_export_install
  op_example.update_methods_requirements_user(method_burial='ploughing',
                                              method_landfall='OCT')

  op_example.feasibility_selector()
  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_rov',
      'equip_burial',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs

  op_example = op_array_install_2
  op_example.update_methods_requirements_user(requirement_rov=True)

  op_example.feasibility_selector()
  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_rov',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs

  op_example = op_burial
  op_example.update_methods_requirements_user(method_burial='cutting')

  op_example.feasibility_selector()
  feasible_funcs = [
      'vec',              # always called
      'terminal',         # always called
      'equip_burial',
      'vessel'            # always called
  ]
  assert op_example.feasible_funcs == feasible_funcs


def test_feasibility_vecs():
  op_example = op_device_install
  op_example.update_methods_requirements_user()

  feasible_vecs = feas_funcs.vecs(op_example)
  assert feasible_vecs.shape[0] == 6
  op_example.methods['transport'] = 'wet'
  feasible_vecs = feas_funcs.vecs(op_example)
  assert feasible_vecs.shape[0] == 4

  try:
    feasible_vecs.head()
    # feasible_vecs is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_vecs is not a pandas DataFrame -> it is None
    assert False


def test_feasibility_predefined_vecs():
  op_example = op_device_install
  op_example.update_methods_requirements_user()

  # Pre-defined VECs
  vec_user = {
      "vec_id": ['VEC_DI01', 'VEC_DI02', 'VEC_DI03'],
      "Type": [
          'Device Installation',
          'Device Installation',
          'Device Installation'],
      "Description": ['Device', 'Device', 'Device'],
      "Transportation": ['On deck', 'Wet tow', 'Wet tow'],
      "Qnt1": [1, 1, 1],
      "Vessel 1 - Installation": ['Propelled crane vessel', 'SOV', 'Tugboat'],
      "Qnt2": [None, None, 1],
      "Vessel 2 - Support": [None, None, 'Multicat'],
      "Qnt3": [None, None, None],
      "Equipment 1 – Installation": [None, None, None],
      "Qnt4": [None, None, None],
      "Equipment 2 - Support": [
          'Inspection Class ROV',
          'Inspection Class ROV',
          'Inspection Class ROV']}
  df_vec_user = pd.DataFrame(vec_user)

  feasible_vecs = feas_funcs.vecs(op_example, df_vec_user)
  assert feasible_vecs.shape[0] == 1


def test_feasibility_terminals():
  op_example = op_device_install
  op_example.update_methods_requirements_user(method_load_out='float')

  feasible_terminals = feas_funcs.terminals(op_example)
  assert feasible_terminals.shape[0] == 16

  try:
    feasible_terminals.head()
    # feasible_terminals is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_terminals is not a pandas DataFrame -> it is None
    assert False

  op_example = op_foundation_install_2
  op_example.update_methods_requirements_user(
      requirement_terminal_area=True,
      requirement_terminal_load=True
  )

  feasible_terminals = feas_funcs.terminals(op_example)
  assert feasible_terminals.shape[0] == 22
  assert all(feasible_terminals['area'] > 13)   # Device base_area = 10
  # ### assert all(feasible_terminals['load'] > 0.3)  # & drymass=30


def test_feasibility_predefined_terminals():
  op_example = op_device_install
  op_example.update_methods_requirements_user(requirement_terminal_area=True)

  terminal_user = {
      "id": ['T001', 'T002', 'T003'],
      "Name of Port": [
          'Porto de Peniche',
          'Porto de Peniche',
          'Porto de Peniche'],
      "Name of Terminal": ['Terminal 1', 'Terminal 2', 'Terminal 3'],
      "Country": ['Portugal', 'Portugal', 'Portugal'],
      "Latitude": [39.3528, 39.3528, 39.3528],
      "Longitude": [-9.3674, -9.3674, -9.3674],
      "previous_projects": ['False', 'False', 'False'],
      "type": ['Quay', 'Dry-dock', 'Dry-dock'],
      "area": ['', '660', ''],
      "load": ['', '100', '']
  }

  df_terminal_user = pd.DataFrame(terminal_user)

  feasible_terminals = feas_funcs.terminals(op_example,
                                            df_terminal_user)
  assert feasible_terminals.shape[0] == 1


def test_port_distance():
  op_example = op_device_install
  op_example.objects[0].draft = 6
  op_example.update_requirements()
  op_example.update_methods_requirements_user(method_transportation='wet')

  feasible_terminals = feas_funcs.terminals(op_example)

  port_dist = feasible_terminals['dist_to_port'] > 2000000
  assert bool(port_dist.any()) is False

  op_example.update_methods_requirements_user(port_max_dist=30000)

  feasible_terminals = feas_funcs.terminals(op_example)

  port_dist = feasible_terminals['dist_to_port'] > 30000
  assert bool(port_dist.any()) is False


def test_feasibility_equip_rov():
  op_example = op_export_install
  op_example.update_methods_requirements_user(
      method_burial='jetting',
      method_landfall='OCT',
      requirement_rov=True
  )

  feasible_rovs = feas_funcs.rov(op_example)
  assert feasible_rovs['_class'][0] == 'inspection'

  try:
    feasible_rovs.head()
    # feasible_rovs is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_rovs is not a pandas DataFrame -> it is None
    assert False

  # Errors
  try:
    op_example.update_methods_requirements_user(method_burial='jetting',
                                                method_landfall='OCT')
    feasible_rovs = feas_funcs.rov(op_example)
    assert False
  except AssertionError:
    assert True

# ### DIVERS MISSING! ###


def test_feasibility_equip_burial():
  Core_Funcs = Core.get_complexity(complexity='high')
  op_example = Core_Funcs.get_user_methods_requirements(
      opx=op_export_install,
      method_burial='cutting'
  )
  op_example = Core_Funcs.get_requirements_from_objects(op_example)

  feasible_burial = Core_Funcs.feasibility_equip_burial(op_example)
  assert feasible_burial.shape[0] == 1

  try:
    feasible_burial.head()
    # feasible_burial is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_burial is not a pandas DataFrame -> it is None
    assert False

  op_example = Core_Funcs.get_user_methods_requirements(
      opx=op_array_install
  )

  assert op_example.methods['burial'] == 'cutting'


# ### DIVERS MISSING! ###


def test_feasibility_equip_burial():
  op_example = op_export_install
  op_example.update_methods_requirements_user(method_burial='cutting',
                                              method_landfall='OCT')

  feasible_burial = feas_funcs.burial(op_example)
  assert feasible_burial.shape[0] == 1

  try:
    feasible_burial.head()
    # feasible_burial is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_burial is not a pandas DataFrame -> it is None
    assert False

  op_example = op_array_install
  op_example.update_methods_requirements_user(method_burial='cutting')

  assert op_example.methods['burial'] == 'cutting'


def test_feasibility_equip_piling():
  op_example = op_foundation_install
  op_example.update_methods_requirements_user(method_piling='vibro-driver')

  feasible_piling = feas_funcs.piling(op_example)
  assert feasible_piling.shape[0] == 1

  try:
    feasible_piling.head()
    # feasible_piling is a pandas DataFrame
    assert True
  except AttributeError:
    # feasible_piling is not a pandas DataFrame -> it is None
    assert False


def test_feasibility_vessels():
  # Device installation operation - dry tow or on deck
  op_example = op_device_install
  op_example.update_methods_requirements_user()

  feasible_vecs = feas_funcs.vecs(op_example)
  feasible_vessels = feas_funcs.vessels(op_example, feasible_vecs)

  main_vessels = feasible_vessels['main']
  tow_vessels = feasible_vessels['tow']
  sup_vessels = feasible_vessels['support']

  wet_transport = main_vessels['transportation'].str.contains('wet').any()
  assert bool(wet_transport) is False
  lift_capacity = main_vessels['crane_capacity'] >= 30
  assert bool(lift_capacity.all()) is True
  deck_area = main_vessels['free_deck'] >= 10
  assert bool(deck_area.all()) is True
  c1 = tow_vessels['vessel_type'].str.contains('tug')
  c2 = tow_vessels['vessel_type'].str.contains('ahts')
  tow_vessels = c1 | c2
  assert bool(tow_vessels.all()) is True
  assert sup_vessels.empty is True

  # Device installation operation - wet tow
  op_example.update_methods_requirements_user(method_transportation='wet')

  feasible_vecs = feas_funcs.vecs(op_example)
  feasible_vessels = feas_funcs.vessels(op_example, feasible_vecs)

  main_vessels = feasible_vessels['main']
  tow_vessels = feasible_vessels['tow']
  sup_vessels = feasible_vessels['support']

  assert main_vessels.empty is True
  wet_transport = tow_vessels['transportation'].str.contains('wet').all()
  assert bool(wet_transport) is True
  c1 = tow_vessels['vessel_type'].str.contains('tug')
  c2 = tow_vessels['vessel_type'].str.contains('ahts')
  cond = c1 | c2
  assert bool(cond.all()) is True
  multicat_vessels = sup_vessels['vessel_type'].str.contains('multicat')
  assert bool(multicat_vessels.all()) is True

  # Foundation installation operation - hammering
  op_example = op_foundation_install
  op_example.update_methods_requirements_user(method_piling='hammering')

  feasible_vecs = feas_funcs.vecs(op_example)
  feasible_vessels = feas_funcs.vessels(op_example, feasible_vecs)

  main_vessels = feasible_vessels['main']
  tow_vessels = feasible_vessels['tow']
  sup_vessels = feasible_vessels['support']

  vec_desc = main_vessels['description'].str.contains('pile').all()
  assert bool(vec_desc) is True
  c1 = tow_vessels['vessel_type'].str.contains('tug')
  c2 = tow_vessels['vessel_type'].str.contains('ahts')
  cond = c1 | c2
  assert bool(cond.all()) is True

  # Export cable installation operation - ploughing
  op_example = op_export_install
  op_example.update_methods_requirements_user(method_burial='ploughing',
                                              method_landfall='OCT')

  feasible_vecs = feas_funcs.vecs(op_example)
  feasible_vessels = feas_funcs.vessels(op_example, feasible_vecs)

  main_vessels = feasible_vessels['main']
  tow_vessels = feasible_vessels['tow']
  sup_vessels = feasible_vessels['support']

  clv_vessel = main_vessels['vessel_type'].str.contains('clv').all()
  assert bool(clv_vessel) is True
  assert tow_vessels.empty is True
  assert sup_vessels.empty is True


def test_bollard_pull():
  # Pile installation operation - wet tow
  op_example = op_foundation_install
  op_example.update_methods_requirements_user(
      method_transportation='wet',
      method_piling='hammering')

  feasible_vecs = feas_funcs.vecs(op_example)
  feasible_vessels = feas_funcs.vessels(op_example, feasible_vecs)

  main_vessels = feasible_vessels['main']
  tow_vessels = feasible_vessels['tow']
  sup_vessels = feasible_vessels['support']
