# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

# Import libraries
import pandas as pd
import json

Operation = Operation_class.get_complexity('high')
Object = Object_class.get_complexity('high')
Cable = Cable_class.get_complexity('high')


# Define a set of Objects to be used in tests
obj_device1 = Object(id_='oec1',
                     name_of_object='device 1',
                     type_of_object='fixed',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=85,
                     drymass=30000,
                     elect_interfaces=['jtube', 'itube'],
                     topside_exists=False)
obj_device2 = Object(id_='oec2',
                     name_of_object='device 2',
                     type_of_object='fixed',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=85,
                     drymass=30000,
                     elect_interfaces=['dry-mate', 'dry-mate'],
                     topside_exists=False)

obj_pile1 = Object(id_='pile1',
                   name_of_object='anchor 1',
                   type_of_object='pile',
                   system=None,
                   length=70,
                   diameter=3,
                   bathymetry=50,
                   seabed_type='sands',
                   drymass=205300,
                   burial_depth=10)
obj_pile2 = Object(id_='pile2',
                   name_of_object='anchor 2',
                   type_of_object='pile',
                   system=None,
                   length=60,
                   diameter=3,
                   bathymetry=55,
                   seabed_type='sands',
                   drymass=230000,
                   burial_depth=10)

obj_caisson = Object(id_='sc1',
                     name_of_object='foundation 1',
                     type_of_object='suction-caisson',
                     system=None,
                     length=10,
                     diameter=1.3,
                     bathymetry=30,
                     seabed_type='sands',
                     drymass=3600)

obj_mooring1 = Object(id_='ml1',
                      name_of_object='mooring 1',
                      type_of_object='drag-embedment',
                      system=None,
                      length=200,
                      diameter=0.3,
                      bathymetry=50,
                      drymass=1500,
                      material='steel')

obj_anchor1 = Object(id_='anch1',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=4,
                     height=4,
                     width=4,
                     drymass=3600,
                     bathymetry=50,
                     seabed_type='sands',
                     material='steel',
                     parent='ml1')
obj_anchor2 = Object(id_='anch2',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=5,
                     height=5,
                     width=5,
                     drymass=5000,
                     bathymetry=55,
                     seabed_type='sands',
                     material='steel',
                     parent='ml2')

collectionpoint = Object(id_='cp1',
                         name_of_object='collection point',
                         type_of_object='floating',
                         system=None,
                         length=30,
                         height=10,
                         width=30,
                         drymass=3000000,
                         bathymetry=55,
                         seabed_type='sands',
                         elect_interfaces=['wet-mate',
                                           'wet-mate',
                                           'wet-mate'])

cable_export = Cable(id_='001',
                     name='cable',
                     cable_type='export',
                     system=None,
                     burial_depth=[1.5] * 10,
                     burial_method='plough',
                     bathymetry=[30, 30, 40, 50, 60, 70, 80, 30, 40, 50],
                     seabed_type=['sands'] * 10,
                     diameter=200,
                     length=[1000] * 10,
                     drymass=700,
                     min_bend_radius=2,
                     elect_interfaces=['dry_mate', 'dry_mate'])

cable_array = Cable(id_='002',
                    name='cable',
                    cable_type='array',
                    system=None,
                    bathymetry=[10, 15, 18, 20, 20, 20, 18, 15, 10],
                    seabed_type=['sands'] * 9,
                    diameter=75,
                    length=[100] * 9,
                    drymass=150,
                    min_bend_radius=1,
                    elect_interfaces=['wet_mate', 'wet_mate'],
                    burial_method='cutting',
                    burial_depth=[1] * 9)

# Define a set of Operations to be used in tests
op_device_install = Operation(operation_id='OP_DI1',
                              operation_name='Device Installation',
                              operation_description='device',
                              objects=[obj_device1, obj_device2])
op_foundation_install = Operation(operation_id='OP_FI1',
                                  operation_name='Foundation Installation',
                                  operation_description='pile',
                                  objects=[obj_pile1, obj_pile2])
op_foundation_install_2 = Operation(operation_id='OP_FI2',
                                    operation_name='Foundation Installation',
                                    operation_description='suction-caisson',
                                    objects=[obj_caisson])
op_mooring_install = Operation(operation_id='OP_MI1',
                               operation_name='Mooring Installation',
                               operation_description='Drag-embedment',
                               objects=[obj_mooring1,
                                        obj_anchor1,
                                        obj_anchor2])
op_cp_install = Operation(operation_id='OP_CPI1',
                          operation_name='Collection point Installation',
                          operation_description='Collection point',
                          objects=[collectionpoint])
op_export_install = Operation(operation_id='OP_ECI1',
                              operation_name='Cable Installation',
                              operation_description='Simultaneous',
                              cables=cable_export)
op_array_install = Operation(operation_id='OP_ACI1',
                             operation_name='Cable Installation',
                             operation_description='Simultaneous',
                             cables=cable_array)
op_array_install_2 = Operation(operation_id='OP_ACI2',
                               operation_name='Cable Installation',
                               operation_description='Post-burial',
                               cables=cable_array)
op_burial = Operation(operation_id='OP_B1',
                      operation_name='Post-lay burial',
                      operation_description='not required')

# Define operations methods and requirements
Core_Funcs = Core.get_complexity(complexity='high')
op_device_install.update_methods_requirements_user(method_transportation='dry')

op_foundation_install.update_methods_requirements_user(
    method_piling='hammering')
op_mooring_install.update_methods_requirements_user()

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy.reset_index(inplace=True, drop=True)

site = Site(
    name='Dummy_site_1year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)

op_mooring_install.allocate_site(site)
op_device_install.allocate_site(site)
op_foundation_install.allocate_site(site)

test_ops = [op_mooring_install, op_device_install, op_foundation_install]


def test_dummy():
  Core_Funcs = Core.get_complexity(complexity='high')

  for op in test_ops:
    op.get_feasible_solutions(0.5)
    op.match_feasible_solutions()


def test_number_components():
  Core_Funcs = Core.get_complexity(complexity='high')

  class _Obj():
    base_area = None
  obj1 = _Obj()
  obj2 = _Obj()
  obj3 = _Obj()
  obj4 = _Obj()
  obj1.base_area = 60
  obj2.base_area = 180
  obj3.base_area = 80
  obj4.base_area = 10
  obj1.drymass, obj2.drymass, obj3.drymass, obj4.drymass = 1, 1, 1, 1
  obj1.bathymetry, obj2.bathymetry = 1, 1
  obj3.bathymetry, obj4.bathymetry = 1, 1
  obj1.length, obj2.length, obj3.length, obj4.length = 1, 1, 1, 1

  op_dummy = Operation(operation_id='XX',
                       operation_name='dummy',
                       operation_description='dummy',
                       objects=[obj1, obj1])
  num_objects_trip = op_dummy.get_number_components_trip(200)
  assert len(num_objects_trip) == 1
  assert num_objects_trip[0] == (2, 0)

  objects = [obj1, obj1, obj3, obj4]
  op_dummy = Operation(operation_id='XX',
                       operation_name='dummy',
                       operation_description='dummy',
                       objects=objects)
  num_objects_trip = op_dummy.get_number_components_trip(200)
  assert len(num_objects_trip) == 2
  assert num_objects_trip[1] == (1, 0)

  objects = [obj1, obj1, obj2, obj3, obj3, obj4, obj4, obj4, obj4]
  op_dummy = Operation(operation_id='XX',
                       operation_name='dummy',
                       operation_description='dummy',
                       objects=objects)
  num_objects_trip = op_dummy.get_number_components_trip(250)
  assert len(num_objects_trip) == 3
  assert num_objects_trip[2] == (1, 0)

  num_objects_trip = op_dummy.get_number_components_trip(490)
  assert len(num_objects_trip) == 2
