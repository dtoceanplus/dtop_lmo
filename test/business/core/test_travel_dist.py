# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json

Operation = Operation_class.get_complexity('high')
Object = Object_class.get_complexity('high')


obj_device1 = Object(id_='oec1',
                     name_of_object='device 1',
                     type_of_object='fixed',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30,
                     elect_interfaces=['jtube', 'itube'],
                     topside_exists=False)
obj_device2 = Object(id_='oec2',
                     name_of_object='device 2',
                     type_of_object='fixed',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30,
                     elect_interfaces=['dry-mate', 'dry-mate'],
                     topside_exists=False)

obj_mooring1 = Object(id_='ml1',
                      name_of_object='mooring 1',
                      type_of_object='drag-embedment',
                      system=None,
                      length=200,
                      diameter=0.3,
                      bathymetry=50,
                      drymass=10,
                      material='steel')

obj_anchor1 = Object(id_='anch1',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=4,
                     height=4,
                     width=4,
                     drymass=10,
                     bathymetry=50,
                     material='steel',
                     parent='ml1')
obj_anchor2 = Object(id_='anch2',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=5,
                     height=5,
                     width=5,
                     drymass=20,
                     bathymetry=55,
                     seabed_type='sands',
                     material='steel',
                     parent='ml2')

op_device_install = Operation(operation_id='OP_DI1',
                              operation_name='Device Installation',
                              operation_description='device',
                              objects=[obj_device1, obj_device2])
op_mooring_install = Operation(operation_id='OP_MI1',
                               operation_name='Mooring Installation',
                               operation_description='Drag-embedment',
                               objects=[obj_mooring1,
                                        obj_anchor1,
                                        obj_anchor2])

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy.reset_index(inplace=True, drop=True)

site = Site(
    name='Dummy_site_1year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)

op_device_install.allocate_site(site)
op_mooring_install.allocate_site(site)

Core_Funcs = Core.get_complexity(complexity='high')

op_device_install.update_methods_requirements_user(method_transportation='dry')
op_mooring_install.update_methods_requirements_user()

op_device_install.get_feasible_solutions(0.5)
op_device_install.match_feasible_solutions()


def test_graph():
  op_device_install.update_dist_to_port()
