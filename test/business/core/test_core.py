# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import classes
from dtop_lmo.business import Core
from dtop_lmo.business import Installation
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json


Object = Object_class.get_complexity('high')
Cable = Cable_class.get_complexity('high')


# Define a set of Objects to be used in tests
obj_device1 = Object(id_='oec1',
                     name_of_object='device 1',
                     type_of_object='float',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30000,
                     elect_interfaces=['jtube', 'itube'],
                     topside_exists=False)
obj_device2 = Object(id_='oec2',
                     name_of_object='device 2',
                     type_of_object='float',
                     system=None,
                     length=3,
                     width=4,
                     height=5,
                     bathymetry=50,
                     seabed_type='sands',
                     base_area=10,
                     drymass=30000,
                     elect_interfaces=['dry-mate', 'dry-mate'],
                     topside_exists=False)

obj_pile1 = Object(id_='pile1',
                   name_of_object='anchor 1',
                   type_of_object='pile',
                   system=None,
                   length=70,
                   diameter=3,
                   bathymetry=50,
                   seabed_type='sands',
                   drymass=205300,
                   burial_depth=50)
obj_pile2 = Object(id_='pile2',
                   name_of_object='anchor 2',
                   type_of_object='pile',
                   system=None,
                   length=60,
                   diameter=3,
                   bathymetry=55,
                   seabed_type='rock',
                   drymass=230000,
                   burial_depth=40)

obj_caisson = Object(id_='sc1',
                     name_of_object='foundation 1',
                     type_of_object='suction-caisson',
                     system=None,
                     length=10,
                     diameter=1.3,
                     bathymetry=30,
                     seabed_type='sands',
                     drymass=3600)

obj_mooring1 = Object(id_='ml1',
                      name_of_object='mooring 1',
                      type_of_object='drag-embedment',
                      system=None,
                      length=200,
                      diameter=0.3,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=10000,
                      material='steel')
obj_mooring2 = Object(id_='ml2',
                      name_of_object='mooring 2',
                      type_of_object='drag-embedment',
                      system=None,
                      length=200,
                      diameter=0.3,
                      bathymetry=60,
                      seabed_type='sands',
                      drymass=10000,
                      material='steel')

obj_anchor1 = Object(id_='anch1',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=4,
                     height=4,
                     width=4,
                     drymass=3000,
                     bathymetry=50,
                     seabed_type='sands',
                     material='steel',
                     parent='ml1')
obj_anchor2 = Object(id_='anch2',
                     name_of_object='anchor',
                     type_of_object='drag anchor',
                     system=None,
                     length=5,
                     height=5,
                     width=5,
                     drymass=3000,
                     bathymetry=55,
                     seabed_type='sands',
                     material='steel',
                     parent='ml2')

collectionpoint = Object(id_='cp1',
                         name_of_object='collection point',
                         type_of_object='floating',
                         system=None,
                         length=30,
                         height=10,
                         width=30,
                         drymass=3000000,
                         bathymetry=55,
                         seabed_type='sands',
                         elect_interfaces=['wet-mate',
                                           'wet-mate',
                                           'wet-mate'])

cable_export = Cable(id_='001',
                     name='cable',
                     cable_type='export',
                     system=None,
                     burial_depth=[1.5] * 10,
                     burial_method='plough',
                     bathymetry=[30, 30, 40, 50, 60, 70, 80, 30, 40, 50],
                     seabed_type=['sands'] * 10,
                     diameter=200,
                     length=[1000] * 10,
                     drymass=300,
                     min_bend_radius=2,
                     elect_interfaces=['dry_mate', 'dry_mate'])

cable_array = Cable(id_='002',
                    name='cable',
                    cable_type='array',
                    system=None,
                    bathymetry=[10, 15, 18, 20, 20, 20, 18, 15, 10],
                    seabed_type=['sands'] * 9,
                    diameter=75,
                    length=[80] * 9,
                    drymass=150,
                    min_bend_radius=1,
                    elect_interfaces=['wet_mate', 'wet_mate'],
                    burial_method='cutting',
                    burial_depth=[1] * 9)

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['month'] < 7]
df_metocean_dummy.reset_index(inplace=True, drop=True)

Core_Funcs = Core.get_complexity(complexity='high',
                                 comb_retain_ratio=0,
                                 comb_retain_min=3)
Inst_Funcs = Installation.get_complexity(
    complexity='high',
    start_date='dummy'
)
site = Site(
    name='Dummy_site_half_year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy
)

list_objects = [obj_device1, obj_device2,
                obj_pile1, obj_pile2,
                obj_mooring1, obj_mooring2,
                obj_anchor1, obj_anchor2,
                collectionpoint]
list_cables = [cable_export, cable_array]

Inst_Funcs.site = site
Inst_Funcs.objects = list_objects
Inst_Funcs.cables = list_cables
operation_sequence = Inst_Funcs.get_installation_operations_sequence()

# Define operations as Operation()
operations = Inst_Funcs.set_installation_operations()

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['month'] < 7]
df_metocean_dummy.reset_index(inplace=True, drop=True)

site = Site(
    name='Dummy_site_half_year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)


def test_pre_feasibility():
  for idx, _ in enumerate(operations):
    operations[idx].allocate_site(site)

  assert operations[0].name == 'foundation installation'
  assert operations[1].name == 'mooring installation'
  assert operations[2].name == 'collection point installation'
  assert operations[3].name == 'device installation'
  assert operations[4].name == 'cable installation'
  assert operations[5].name == 'cable installation'

  # Now we check operation by operation
  operations[0].update_methods_requirements_user(method_piling='hammering')
  operations[1].update_methods_requirements_user()
  operations[2].update_methods_requirements_user()
  operations[3].update_methods_requirements_user(method_transportation='wet')
  operations[4].update_methods_requirements_user(method_burial='plough',
                                                 method_landfall='OCT')
  operations[5].update_methods_requirements_user(method_burial='jetting')

  assert operations[0].methods['transport'] == 'dry'
  assert operations[4].requirements['rov'] == 'inspection'

  return operations


def test_feasibility():
  operations = test_pre_feasibility()
  operations = Core_Funcs.run_feasibility_functions(operations)

  assert operations[0].feasible_solutions.vectable.shape[0] == 5

  return operations


def test_matchability():
  operations = test_feasibility()
  operations = Core_Funcs.run_matchability_functions(operations)

  return operations


def test_activity_definition():
  list_objects = [obj_device1, obj_device2,
                  obj_pile1, obj_pile2,
                  obj_mooring1, obj_mooring2,
                  obj_anchor1, obj_anchor2]
  cable_export.burial_depth[5] = 0
  cable_export.split_pipes[5] = True
  cable_export.burial_depth[6] = 0
  cable_export.split_pipes[6] = True
  cable_export.burial_depth[9] = 0
  cable_export.buoyancy_modules[9] = True
  cable_array.burial_depth[6] = 0
  cable_array.split_pipes[6] = True
  cable_array.burial_depth[7] = 0
  cable_array.split_pipes[7] = True
  cable_array.burial_depth[8] = 0
  cable_array.buoyancy_modules[8] = True
  list_cables = [cable_export, cable_array, cable_array]

  Inst_Funcs.objects = list_objects
  Inst_Funcs.cables = list_cables
  Inst_Funcs.get_installation_operations_sequence()

  # Define operations as Operation()
  operations = Inst_Funcs.set_installation_operations()
  assert operations[0].name == 'foundation installation'
  assert operations[1].name == 'mooring installation'
  assert operations[2].name == 'device installation'

  # User methods and requirements
  operations[0].update_methods_requirements_user(method_piling='hammering')
  operations[1].update_methods_requirements_user()
  operations[2].update_methods_requirements_user(method_transportation='wet')
  operations[3].update_methods_requirements_user(method_burial='ploughing',
                                                 method_landfall='OCT')
  operations[4].update_methods_requirements_user(method_burial='cutting')

  operations[0].allocate_site(site)
  operations[1].allocate_site(site)
  operations[2].allocate_site(site)
  operations[3].allocate_site(site)
  operations[4].allocate_site(site)

  operations = Core_Funcs.run_feasibility_functions(operations)
  operations = Core_Funcs.run_matchability_functions(operations)

  operations = Core_Funcs.define_activities(operations)

  return operations


def test_workabilities():
  obj_device1 = Object(id_='oec1',
                       name_of_object='device 1',
                       type_of_object='float',
                       system=None,
                       length=3,
                       width=4,
                       height=5,
                       bathymetry=50,
                       seabed_type='sands',
                       base_area=10,
                       drymass=30,
                       elect_interfaces=['jtube', 'itube'],
                       topside_exists=False)
  obj_device2 = Object(id_='oec2',
                       name_of_object='device 2',
                       type_of_object='float',
                       system=None,
                       length=3,
                       width=4,
                       height=5,
                       bathymetry=30,
                       seabed_type='sands',
                       base_area=10,
                       drymass=30,
                       elect_interfaces=['jtube', 'itube'],
                       topside_exists=False)
  Inst_Funcs.objects = [obj_device1, obj_device2]
  Inst_Funcs.get_installation_operations_sequence()
  Inst_Funcs.set_installation_operations()
  operations = Inst_Funcs.operations

  # User methods and requirements
  # operations[0].update_methods_requirements_user(method_piling='drilling')
  # operations[1].update_methods_requirements_user()
  operations[0].update_methods_requirements_user(
      method_transportation='wet',
      method_load_out='skidded')
  # operations[0].allocate_site(site)
  # operations[1].allocate_site(site)
  operations[0].allocate_site(site)

  operations = Core_Funcs.run_feasibility_functions(operations)
  operations = Core_Funcs.run_matchability_functions(operations)
  operations = Core_Funcs.define_activities(operations)
  operations = Core_Funcs.delete_combinations(operations)

  # Check workability of each Operation combination
  import time
  time_st = time.time()
  operations = Core_Funcs.check_workabilities(operations)
  print(time.time() - time_st)

  # ### assert if workability is only Trues and False

  # import pickle
  # import os
  # local_path = '/TEMP_DATABASE'
  # db_path = os.getcwd() + local_path
  # file_operations_db = open(db_path + '/db_operations', 'wb')
  # pickle.dump(operations, file_operations_db)
  # file_operations_db.close()

  return operations


def test_startability():
  operations = test_workabilities()

  import time
  time_st = time.time()
  operations = Core_Funcs.check_startabilities(operations)
  print(time.time() - time_st)

  # assert False
  return operations


def test_waiting():
  operations = test_startability()
  import time
  time_st = time.time()
  operations = Core_Funcs.check_activities_waiting_time(operations)
  print(time.time() - time_st)

  # assert False
  return operations


def test_operation_values_def():
  operations = test_startability()

  import time
  time_st = time.time()
  operations = Core_Funcs.define_durations_waitings_timestep(operations)
  print(time.time() - time_st)

  # assert False
  return operations


def test_statistic_analyses():
  operations = test_operation_values_def()

  import time
  time_st = time.time()
  operations = Core_Funcs.statistical_analysis(operations)
  print(time.time() - time_st)
  # assert False

  # import pickle
  # import os
  # local_path = '/TEMP_DATABASE'
  # db_path = os.getcwd() + local_path
  # file_operations_db = open(db_path + '/db_op_durs_waitings', 'wb')
  # pickle.dump(operations, file_operations_db)
  # file_operations_db.close()

  return operations


def test_vessel_costs():
  operations = test_statistic_analyses()

  import time
  time_st = time.time()
  operations = Core_Funcs.get_vessels_cost(operations)
  print(time.time() - time_st)
  # assert False
  return operations


def test_combination_selection():
  operations = test_vessel_costs()

  import time
  time_st = time.time()
  operations = Core_Funcs.select_combination(operations)
  print(time.time() - time_st)
  # assert False
