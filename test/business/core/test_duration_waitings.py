# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business import Core
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Site

import pandas as pd
import json
import copy
from random import randrange


import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)

site = Site(
    name='Dummy_site',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)


def test_durs_waitings_cpx1():
  Core_Funcs = Core.get_complexity('low')
  Operation = Operation_class.get_complexity('low')
  Object = Object_class.get_complexity('low')

  obj_device = Object(id_='dev001',
                      name_of_object='device 001',
                      type_of_object='floating',
                      system='ET',
                      topside_exists=True)

  op_dummy = Operation(operation_id='OP05',
                       operation_name='device installation',
                       operation_description='device',
                       objects=[obj_device])
  op_dummy.terminal = 'dummy_ter'
  op_dummy.terminal_dist = 10000
  op_dummy.vec = 'dummy_vec'
  op_dummy.vessel = 'dummy_vessel'
  op_dummy.equipment = ['dummy_equip1', 'dummy_equip2', 'dummy_equip3']
  op_dummy.site = site

  operations = []
  for i in range(0, 30):
    operations.append(copy.deepcopy(op_dummy))

  for idx_op, _ in enumerate(operations):
    operations[idx_op].duration_net = randrange(15, 100)
    operations[idx_op].hs = randrange(10, 30) / 10

  operations = Core_Funcs.check_workabilities(operations)
  operations = Core_Funcs.check_startabilities(operations)
  operations = Core_Funcs.check_waiting_time(operations)
  operations = Core_Funcs.define_durations_waitings_timestep(operations)

  operations = Core_Funcs.define_statistics(operations)

  assert len(operations[0].durations) == 12
  assert len(operations[0].waitings) == 12
