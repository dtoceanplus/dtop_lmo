# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json

# Import classes
from dtop_lmo.business import Core
from dtop_lmo.business import Maintenance
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes.Site import Site

# import datetime
# import pandas as pd
# import random
# import warnings
# import re
# import numpy as np
# import math
Object = Object_class.get_complexity('low')
Cable = Cable_class.get_complexity('low')


# Define a set of Objects to be used in tests
# ET
sys_et1 = Object(id_='ET1',
                 name_of_object='device 1',
                 type_of_object='float',
                 system='ET',
                 # catalogue_id = None,
                 # comp_cost = 50,
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=30000,
                 child=['PTO1_1', 'PTO1_2'],
                 topside_exists=True)
sys_et2 = Object(id_='ET2',
                 name_of_object='device 2',
                 type_of_object='float',
                 system='ET',
                 # catalogue_id = None,
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=30000,
                 child=['PTO2_1', 'PTO2_2'],
                 topside_exists=True)
sys_et3 = Object(id_='ET3',
                 name_of_object='device 3',
                 type_of_object='float',
                 system='ET',
                 # catalogue_id = None,
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=30000,
                 child=['PTO3_1', 'PTO3_2'],
                 topside_exists=False)

sys_pto1_1 = Object(id_='PTO1_1',
                    name_of_object='PTO1_1',
                    type_of_object='dummy',
                    system='ET',
                    # catalogue_id = None,
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET1',
                    child=['MechT_1_1', 'ElecT_1_1', 'GridC_1_1'])
sys_pto1_2 = Object(id_='PTO1_2',
                    name_of_object='PTO1_2',
                    type_of_object='dummy',
                    system='ET',
                    # catalogue_id = None,
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET1',
                    child=['MechT_1_2', 'ElecT_1_2', 'GridC_1_2'])
sys_pto2_1 = Object(id_='PTO2_1',
                    name_of_object='PTO2_1',
                    type_of_object='dummy',
                    system='ET',
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET2',
                    child=['MechT_2_1', 'ElecT_2_1', 'GridC_2_1'])
sys_pto2_2 = Object(id_='PTO2_2',
                    name_of_object='PTO2_2',
                    type_of_object='dummy',
                    system='ET',
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET2',
                    child=['MechT_2_2', 'ElecT_2_2', 'GridC_2_2'])
sys_pto3_1 = Object(id_='PTO3_1',
                    name_of_object='PTO3_1',
                    type_of_object='dummy',
                    system='ET',
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET3',
                    child=['MechT_3_1', 'ElecT_3_1', 'GridC_3_1'])
sys_pto3_2 = Object(id_='PTO3_2',
                    name_of_object='PTO3_2',
                    type_of_object='dummy',
                    system='ET',
                    bathymetry=50,
                    seabed_type='sands',
                    drymass=1000,
                    parent='ET1',
                    child=['MechT_3_2', 'ElecT_3_2', 'GridC_3_2'])

obj_mecht_1_1 = Object(id_='MechT_1_1',
                       name_of_object='MechT_1_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_1',
                       failure_rates=[0.01, 0.01])
obj_mecht_1_2 = Object(id_='MechT_1_2',
                       name_of_object='MechT_1_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_2',
                       failure_rates=[0.01, 0.01])
obj_mecht_2_1 = Object(id_='MechT_2_1',
                       name_of_object='MechT_2_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_1',
                       failure_rates=[0.01, 0.01])
obj_mecht_2_2 = Object(id_='MechT_2_2',
                       name_of_object='MechT_2_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_2',
                       failure_rates=[0.01, 0.01])
obj_mecht_3_1 = Object(id_='MechT_3_1',
                       name_of_object='MechT_3_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO3_1',
                       failure_rates=[0.01, 0.01])
obj_mecht_3_2 = Object(id_='MechT_3_2',
                       name_of_object='MechT_3_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Mech_1',    #FCF CUIDADO
                       comp_cost = 50,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO3_2',
                       failure_rates=[0.01, 0.01])

obj_elect_1_1 = Object(id_='ElecT_1_1',
                       name_of_object='ElecT_1_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_1',
                       failure_rates=[0.01, 0.01])
obj_elect_1_2 = Object(id_='ElecT_1_2',
                       name_of_object='ElecT_1_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_2',
                       failure_rates=[0.01, 0.01])
obj_elect_2_1 = Object(id_='ElecT_2_1',
                       name_of_object='ElecT_2_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_1',
                       failure_rates=[0.01, 0.01])
obj_elect_2_2 = Object(id_='ElecT_2_2',
                       name_of_object='ElecT_2_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_2',
                       failure_rates=[0.01, 0.01])
obj_elect_3_1 = Object(id_='ElecT_3_1',
                       name_of_object='ElecT_3_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO3_1',
                       failure_rates=[0.01, 0.01])
obj_elect_3_2 = Object(id_='ElecT_3_2',
                       name_of_object='ElecT_3_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'Elec_1',    #FCF CUIDADO
                       comp_cost = 30,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO3_2',
                       failure_rates=[0.01, 0.01])

obj_gridc_1_1 = Object(id_='GridC_1_1',
                       name_of_object='GridC_1_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_1',
                       failure_rates=[0.01, 0.01])
obj_gridc_1_2 = Object(id_='GridC_1_2',
                       name_of_object='GridC_1_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO1_2',
                       failure_rates=[0.01, 0.01])
obj_gridc_2_1 = Object(id_='GridC_2_1',
                       name_of_object='GridC_2_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_1',
                       failure_rates=[0.01, 0.01])
obj_gridc_2_2 = Object(id_='GridC_2_2',
                       name_of_object='GridC_2_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1000,
                       parent='PTO2_2',
                       failure_rates=[0.01, 0.01])
obj_gridc_3_1 = Object(id_='GridC_3_1',
                       name_of_object='GridC_3_1',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1,
                       parent='PTO3_1',
                       failure_rates=[0.01, 0.01])
obj_gridc_3_2 = Object(id_='GridC_3_2',
                       name_of_object='GridC_3_2',
                       type_of_object='dummy',
                       system='ET',
                       catalogue_id = 'GridC_1',    #FCF CUIDADO
                       comp_cost = 20,
                       bathymetry=50,
                       seabed_type='sands',
                       drymass=1,
                       parent='PTO3_2',
                       failure_rates=[0.01, 0.01])

# SK
sys_sk1 = Object(id_='SK1_x',
                 name_of_object='mooring 1',
                 type_of_object='drag-embedment',
                 system='SK',
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=10,
                 child=['ML11', 'ML12', 'ML13'])
sys_sk2 = Object(id_='SK2_x',
                 name_of_object='mooring 2',
                 type_of_object='drag-embedment',
                 system='SK',
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=10,
                 child=['ML21', 'ML22', 'ML23'])
sys_sk3 = Object(id_='SK3_x',
                 name_of_object='mooring 3',
                 type_of_object='drag-embedment',
                 system='SK',
                 bathymetry=50,
                 seabed_type='sands',
                 drymass=10,
                 child=['ML31', 'ML32', 'ML33'])

sys_ml11 = Object(id_='ML11',
                  name_of_object='ML11',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK1')
sys_ml12 = Object(id_='ML12',
                  name_of_object='ML12',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK1')
sys_ml13 = Object(id_='ML13',
                  name_of_object='ML13',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK1')
sys_ml21 = Object(id_='ML21',
                  name_of_object='ML21',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK2')
sys_ml22 = Object(id_='ML22',
                  name_of_object='ML22',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK2')
sys_ml23 = Object(id_='ML23',
                  name_of_object='ML23',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK2')
sys_ml31 = Object(id_='ML31',
                  name_of_object='ML31',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK3')
sys_ml32 = Object(id_='ML32',
                  name_of_object='ML32',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK3')
sys_ml33 = Object(id_='ML33', #nao tem?
                  name_of_object='ML33',
                  type_of_object='dummy',
                  system='SK',
                  bathymetry=50,
                  seabed_type='sands',
                  drymass=1,
                  parent='SK3')

obj_ml11_seg = Object(id_='ML11_seg',
                      name_of_object='ML11_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1',    #FCF CUIDADO
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml12_seg = Object(id_='ML12_seg',
                      name_of_object='ML12_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1',    #FCF CUIDADO
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml13_seg = Object(id_='ML13_seg',
                      name_of_object='ML13_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1',    #FCF CUIDADO
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml21_seg = Object(id_='ML21_seg',
                      name_of_object='ML21_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1',    #FCF CUIDADO
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml22_seg = Object(id_='ML22_seg',
                      name_of_object='ML22_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1', #FCF CUIDADO (se tiver failure rate)
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml23_seg = Object(id_='ML23_seg',
                      name_of_object='ML23_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1', #FCF CUIDADO (se tiver failure rate)
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml31_seg = Object(id_='ML31_seg',
                      name_of_object='ML31_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1', #FCF CUIDADO (se tiver failure rate)
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml32_seg = Object(id_='ML32_seg',
                      name_of_object='ML32_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1', #FCF CUIDADO (se tiver failure rate)
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])
obj_ml33_seg = Object(id_='ML33_seg',
                      name_of_object='ML33_seg',
                      type_of_object='dummy',
                      system='SK',
                      catalogue_id = 'ML1', #FCF CUIDADO (se tiver failure rate)
                      comp_cost = 1000,
                      bathymetry=50,
                      seabed_type='sands',
                      drymass=1,
                      failure_rates=[0.01, 0.01])

obj_ml11_anchor = Object(id_='ML11_anchor',
                         name_of_object='ML11_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml12_anchor = Object(id_='ML12_anchor',
                         name_of_object='ML12_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml13_anchor = Object(id_='ML13_anchor',
                         name_of_object='ML13_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml21_anchor = Object(id_='ML21_anchor',
                         name_of_object='ML21_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml22_anchor = Object(id_='ML22_anchor',
                         name_of_object='ML22_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml23_anchor = Object(id_='ML23_anchor',
                         name_of_object='ML23_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml31_anchor = Object(id_='ML31_anchor',
                         name_of_object='ML31_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml32_anchor = Object(id_='ML32_anchor',
                         name_of_object='ML32_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
obj_ml33_anchor = Object(id_='ML33_anchor',
                         name_of_object='ML33_anchor',
                         type_of_object='dummy',
                         system='SK',
                         catalogue_id = 'ANCH1', #FCF CUIDADO (se tiver failure rate)
                         comp_cost = 2000,
                         bathymetry=50,
                         seabed_type='sands',
                         drymass=1,
                         failure_rates=[0.01, 0.01])
                                      
# ED
obj_cp1 = Object(id_='CP1',
                 name_of_object='collection point 1',
                 type_of_object='hub',
                 system='ED',
                 catalogue_id = 'CP1', #FCF CUIDADO (se tiver failure rate)
                 comp_cost = 3000,
                 bathymetry=55,
                 seabed_type='sands',
                 failure_rates=[0.01, 0.01])

cbl_ec1 = Cable(id_='EC1',
                name='export cable 1',
                cable_type='export',
                system='ED',
                catalogue_id = 'cat_AC1', #FCF CUIDADO (se tiver failure rate)
                comp_cost = 200,
                bathymetry=30,
                diameter=200,
                length=50000,
                failure_rates=[0.01, 0.01])

cbl_ac1 = Cable(id_='AC1',
                name='array cable 1',
                cable_type='array',
                system='ED',
                catalogue_id = 'cat_AC1', #FCF CUIDADO (se tiver failure rate)
                comp_cost = 200,
                bathymetry=20,
                diameter=75,
                length=300,
                failure_rates=[0.01, 0.01])
cbl_ac2 = Cable(id_='AC2',
                name='array cable 2',
                cable_type='array',
                system='ED',
                catalogue_id = 'cat_AC1', #FCF CUIDADO (se tiver failure rate)
                comp_cost = 200,
                bathymetry=30,
                diameter=75,
                length=200,
                failure_rates=[0.01, 0.01])
cbl_ac3 = Cable(id_='AC3',
                name='array cable 3',
                cable_type='array',
                system='ED',
                catalogue_id = 'cat_AC1', #FCF CUIDADO (se tiver failure rate)
                comp_cost = 200,
                bathymetry=30,
                diameter=75,
                length=100,
                failure_rates=[0.01, 0.01])

obj_dm1 = Object(id_='DM1',
                 name_of_object='DM1',
                 type_of_object='dry-mate',
                 system='ED',
                 catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                 comp_cost = 200,
                 drymass=3,
                 bathymetry=50,
                 seabed_type='sands',
                 failure_rates=[0.01, 0.01])

obj_wm1_1 = Object(id_='WM1_1',
                   name_of_object='WM1_1',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])
obj_wm1_2 = Object(id_='WM1_2',
                   name_of_object='WM1_2',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])
obj_wm2_1 = Object(id_='WM2_1',
                   name_of_object='WM2_1',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])
obj_wm2_2 = Object(id_='WM2_2',
                   name_of_object='WM2_2',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])
obj_wm3_1 = Object(id_='WM3_1',
                   name_of_object='WM3_1',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])
obj_wm3_2 = Object(id_='WM3_2',
                   name_of_object='WM3_2',
                   type_of_object='wet-mate',
                   system='ED',
                   catalogue_id = 'wetm_1', #FCF CUIDADO (se tiver failure rate)
                   comp_cost = 200,
                   drymass=3,
                   bathymetry=50,
                   seabed_type='sands',
                   failure_rates=[0.01, 0.01])

Core_Funcs = Core.get_complexity(complexity='low',
                                 comb_retain_ratio=0, #just one combination!
                                 comb_retain_min=1)

commissioning_date = '01/05/2021'
t_life = 20

Main_Funcs = Maintenance.get_complexity(complexity='low',
                                        start_date=commissioning_date,
                                        t_life=t_life,
                                        device_repair_at_port=True)

list_objects = [sys_et1, sys_et2, sys_et3,
                sys_pto1_1, sys_pto1_2, sys_pto2_1, sys_pto2_2, sys_pto3_1,
                sys_pto3_2,
                obj_mecht_1_1, obj_mecht_1_2, obj_mecht_2_1, obj_mecht_2_2,
                obj_mecht_3_1, obj_mecht_3_2,
                obj_elect_1_1, obj_elect_1_2, obj_elect_2_1, obj_elect_2_2,
                obj_elect_3_1, obj_elect_3_2,
                obj_gridc_1_1, obj_gridc_1_2, obj_gridc_2_1, obj_gridc_2_2,
                obj_gridc_3_1, obj_gridc_3_2,
                sys_sk1, sys_sk2, sys_sk3,
                sys_ml11, sys_ml12, sys_ml13, sys_ml21, sys_ml22, sys_ml23,
                sys_ml31, sys_ml32, sys_ml33,
                obj_ml11_seg, obj_ml12_seg, obj_ml13_seg, obj_ml21_seg,
                obj_ml22_seg, obj_ml23_seg, obj_ml31_seg, obj_ml32_seg,
                obj_ml33_seg,
                obj_ml11_anchor, obj_ml12_anchor, obj_ml13_anchor,
                obj_ml21_anchor, obj_ml22_anchor, obj_ml23_anchor,
                obj_ml31_anchor, obj_ml32_anchor, obj_ml33_anchor,
                obj_cp1,
                obj_dm1,
                obj_wm1_1, obj_wm1_2, obj_wm2_1, obj_wm2_2, obj_wm3_1,
                obj_wm3_2]
                                                              
                                  
list_cables = [cbl_ec1, cbl_ac1, cbl_ac2, cbl_ac3]

import os
local_path = '/test/test_inputs/Metocean'
file_path = os.getcwd() + local_path
with open(file_path + '/dummy.json', 'r') as fp:
  metocean_json = json.load(fp)
df_metocean_dummy = pd.DataFrame(metocean_json)
df_metocean_dummy = df_metocean_dummy[df_metocean_dummy['year'] == 2016]
df_metocean_dummy.reset_index(inplace=True, drop=True)

site = Site(
    name='Dummy_site_1year',
    coord=(41.4274, -8.9183),
    metocean=df_metocean_dummy,
    map_name='Iberia',
    map_res=(500, 500),
    map_bound=((34, 52), (-9, 5))
)
                                                                           
                                                                           
def test_failure_times():
  Main_Funcs.objects = list_objects
  Main_Funcs.cables = list_cables
  return Main_Funcs.set_object_failures()


def test_operations_list():
  [list_objects, list_cables] = test_failure_times()

  Main_Funcs.objects = list_objects
  Main_Funcs.cables = list_cables
  operations = Main_Funcs.set_maintenance_operations()

  return operations


def test_pre_feasibility():
  operations = test_operations_list()

  for op in operations:
    # ### assert if divers, then rov None
    if 'topside' in op.description.lower():
      assert op.requirements['rov'] is None
    elif 'device' in op.description.lower() and 'repair' in op.description.lower():
      assert op.requirements['rov'] is None
    else:
      assert op.requirements['rov'] == 'inspection'

  return operations


def test_feasibility():
  operations = test_pre_feasibility()

  for idx, _ in enumerate(operations):
    operations[idx].allocate_site(site)
                                  

  operations = Core_Funcs.run_feasibility_functions(operations)

  return operations


def test_matchability():
  operations = test_feasibility()
  
  # Pick random operations to improve running rime
  # operations = random.sample(operations, 10) #to not run everything
  operations = operations[0:4]                #to not run everything
  operations = Core_Funcs.run_matchability_functions(operations)

  return operations


def test_maintenance_activities_def(): 
  operations = test_matchability(random)
  operations_repair = Core_Funcs.define_activities(operations) #isto tá a fazer alguma coisa, uma vez nao estar a retornar?

  return operations_repair #FCF 2020.07.16 changed


def test_operation_values_def(operations):
  import pickle

  import time
  time_st = time.time()

  durs_waitings = [Core_Funcs.define_durations_waitings_timestep(operation)
                   for operation in operations]
  for i_op, operation in enumerate(operations):
    for i_comb, _ in enumerate(operation.combinations):
      operations[i_op].combinations[i_comb].values = \
          durs_waitings[i_op][i_comb]
    # f = open('OP_round2_%s'%i_op, 'wb')   #retirar
    # pickle.dump(operations[i_op], f) #retirar
    # f.close() #retirar
  
  print(time.time() - time_st)

  # assert False
  return operations

def test_statistic_analyses(operations):

  import time
  time_st = time.time()

  for idx_op, operation in enumerate(operations):
    # First, merge metocean data and each combination operation values
    operations[idx_op].merge_metocean_w_values()
    for idx_comb, combination in enumerate(operation.combinations):
      comb_stats = Core_Funcs.get_statistics(combination.values)
      operations[idx_op].combinations[idx_comb].statistics.durations = \
          comb_stats["durations"]
      operations[idx_op].combinations[idx_comb].statistics.waitings = \
          comb_stats["waitings"]

  print(time.time() - time_st)
  return operations

def test_operation_costs(operations): #former test vessel costs

  for idx_op, operation in enumerate(operations):
    for idx_comb, combination in enumerate(operation.combinations):
      comb_costs = Core_Funcs.calculate_combination_costs(
          operation.feasible_solutions,
          combination
      )
      operations[idx_op].combinations[idx_comb].costs.vessels = \
          comb_costs["vessels"]
      operations[idx_op].combinations[idx_comb].costs.equipment = \
          comb_costs["equipment"]
      operations[idx_op].combinations[idx_comb].costs.terminal = \
          comb_costs["terminal"]
  # assert False
  return operations

def test_combination_selection(operations):
  operations = Core_Funcs.optimal_solutions(operations)

  for idx, op in enumerate(operations):
    consumption = Core_Funcs.operation_consumption(op)
    operations[idx].consumption = consumption
  return operations




def test_get_downtime(operations):

  Main_Funcs.operations = operations
  operations = Main_Funcs.define_operations_dates()

  PTO_dict = Main_Funcs.get_downtime_cor(operations, list_objects,
                                                   list_cables, t_life)

  return [PTO_dict, operations]
  
def test_get_downtime_per_device(PTO_dict, operations):
  # [PTO_dict, _] = test_get_downtime()
                                                                                
  ET_dict = Main_Funcs.get_downtime_per_device(PTO_dict, list_objects)
        
  return ET_dict

df_operations = pd.read_csv('catalogues/Operations.csv')
df_activities = pd.read_csv('catalogues/Activities.csv')

# Load Hierarchies
# ### Get this from other modules API and not from local machine
hierarchy_et = pd.read_csv('test/test_inputs/Hierarchies/Hierarchy_ET.csv')
hierarchy_sk = pd.read_csv('test/test_inputs/Hierarchies/Hierarchy_SK.csv')
hierarchy_ed = pd.read_csv('test/test_inputs/Hierarchies/Hierarchy_ED.csv')
                                                 
                      
def test_build_output_table(operations,df_activities):
  # [_, operations] = test_get_downtime()

  dict_operations_table = Main_Funcs.build_output_table(operations,
                                                        df_activities)
                                                                       
  return dict_operations_table   

def test_all():

  operations = test_feasibility()
  operations_store = list(operations)
  operations = [operations_store[x] for x in [0,1,2,3,4,5,8,9,10,18]] #escolher aqui que operações considerar
  
  operations = Core_Funcs.run_matchability_functions(operations) 
  operations = Core_Funcs.define_activities(operations)
  
  operations = Core_Funcs.delete_combinations(operations)
  operations = Core_Funcs.check_workabilities(operations)
  operations = Core_Funcs.check_startabilities(operations)
  operations = Core_Funcs.check_activities_waiting_time(operations)
  operations = test_operation_values_def(operations)  
  
  operations = test_statistic_analyses(operations)
  operations = test_operation_costs(operations)
  operations = test_combination_selection(operations)
  # [PTO_dict, operations] =  test_get_downtime(operations)
  Main_Funcs.operations = operations
  operations = Main_Funcs.define_operations_dates()
  # PTO_dict2 = Main_Funcs.get_downtime(operations, list_objects,
  #                                                    list_cables, t_life)
  PTO_dict = Main_Funcs.get_downtime_cor(operations, list_objects,
                                                      list_cables, t_life)
  
  ET_dict = test_get_downtime_per_device(PTO_dict, operations)
  
  
  dict_operations_table = test_build_output_table(operations,df_activities)
  return dict_operations_table
