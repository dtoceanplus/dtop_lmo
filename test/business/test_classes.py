# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Barge
from dtop_lmo.business.classes import Site


def test_object_cpx1():
  Object = Object_class.get_complexity('low')
  device = Object(id_='d001',
                  name_of_object='device1',
                  type_of_object='floating',
                  system='ET',
                  topside_exists=True)
  assert device.id == 'd001'
  assert device.name == 'device1'
  assert device.type == 'floating'

  pile = Object(id_='dummy',
                name_of_object='foundation',
                type_of_object='pile',
                system='SK',
                comp_cost=10000,
                drymass=10000,
                bathymetry=50,
                seabed_type='sands',
                failure_rates=(0.003, 0.0001))
  assert pile.id == 'dummy'
  assert pile.bathymetry == 50.0

  try:
    Object(id_='dummy',
           name_of_object='foundation',
           type_of_object='pile',
           system='SK',
           failure_rates=(0.003, 0.0001, 0.1))
    assert False
  except AssertionError:
    assert True


def test_cable_cpx1():
  Cable = Cable_class.get_complexity('low')
  cbl_array = Cable(id_='c002',
                    name='array cable1',
                    cable_type='array',
                    system='ED',
                    bathymetry=20,
                    length=800,
                    diameter=150)
  assert cbl_array.id == 'c002'
  assert cbl_array.name == 'array cable1'
  assert cbl_array.type == 'array'

  cbl_export = Cable(id_='c008',
                     name='export cable',
                     cable_type='export',
                     system='ED',
                     comp_cost=100000,
                     bathymetry=25,
                     length=10000,
                     diameter=300,
                     failure_rates=(0.0004, 0.00002))
  assert cbl_export.id == 'c008'
  assert cbl_export.name == 'export cable'
  assert cbl_export.type == 'export'


def test_object_cpx3():
  Object = Object_class.get_complexity('high')
  obj_pile = Object(id_='FAP1',
                    name_of_object='Pile number 1',
                    type_of_object='pile',
                    system=None,
                    length='50',
                    diameter='1.5',
                    drymass='10',
                    bathymetry='15',
                    seabed_type='sands',
                    burial_depth='40')
  assert obj_pile.name == 'Pile number 1'
  assert obj_pile.type == 'pile'
  assert obj_pile.length == 50.0
  assert obj_pile.drymass == 10.0
  assert obj_pile.diameter == 1.5
  assert obj_pile.base_area == 75.0
  assert obj_pile.burial_depth == 40.0

  obj_achor = Object(id_='MA1',
                     name_of_object='Anchor 11',
                     type_of_object='anchor',
                     system=None,
                     length='3',
                     width='2',
                     height='1.5',
                     diameter='1.5',
                     drymass='5.3',
                     seabed_type='sands')
  assert obj_achor.name == 'Anchor 11'
  assert obj_achor.type == 'anchor'
  assert obj_achor.length == 3.0
  assert obj_achor.width == 2.0
  assert obj_achor.drymass == 5.3
  assert obj_achor.base_area == 6.0

  obj_wet_tow = Object(id_='MA1',
                       name_of_object='Anchor 11',
                       type_of_object='anchor',
                       system=None,
                       draft=3.5)
  assert obj_wet_tow.draft == 3.5


def test_object_cpx3_errors():
  Object = Object_class.get_complexity('high')
  try:
    Object(id_='fb1',
           name_of_object='Foo',
           type_of_object='Bar',
           system=None,
           length='3',
           drymass='2')
    assert False
  except AssertionError:
    assert True
  try:
    Object(id_='fb1',
           name_of_object='Anchor 11',
           type_of_object='anchor',
           system=None,
           length='3',
           drymass='2',
           width='2')
    assert False
  except AssertionError:
    assert True
  try:
    Object(id_='fb1',
           name_of_object='Foundation 21',
           type_of_object='pile',
           system=None,
           length='50',
           drymass='15',
           diameter='3')
    assert False
  except AssertionError:
    assert True
  try:
    Object(id_='fb1',
           name_of_object='Foundation 31',
           type_of_object='anchor',
           system=None,
           length='5',
           drymass='13',
           width='2',
           height='2',
           burial_depth='10')
    assert False
  except AssertionError:
    assert True


def test_cable_cpx3():
  Cable = Cable_class.get_complexity('high')
  # Test array cable
  cable_array = Cable(id_='001',
                      name='AC11',
                      cable_type='array',
                      system=None,
                      bathymetry=[30, 30, 40, 30, 40, 50],
                      seabed_type=['sands'] * 6,
                      diameter=200,
                      length=[10] * 6,
                      drymass=150,
                      min_bend_radius=2,
                      elect_interfaces=['jtube'],
                      burial_depth=[1.5, 2.0, 2.0, 1.5, 1.0, 1.0],
                      burial_method='plough')
  assert len(cable_array.bathymetry) == 6
  assert len(cable_array.burial_depth) == 6
  assert max(cable_array.bathymetry) == 50
  assert max(cable_array.burial_depth) == 2.0

  cable_array = Cable(id_='001',
                      name='AC11',
                      cable_type='array',
                      system=None,
                      bathymetry=[30, 30, 40, 30, 40, 50],
                      seabed_type=['sands'] * 6,
                      diameter=200,
                      length=[10] * 6,
                      drymass=150,
                      min_bend_radius=2,
                      elect_interfaces=['jtube'],
                      split_pipes=[True, False, True, True, False, True])
  assert cable_array.burial_depth == [0, 0, 0, 0, 0, 0]
  assert cable_array.burial_method is None


def test_cable_cpx3_errors():
  Cable = Cable_class.get_complexity('high')
  try:
    Cable(id_='error1',
          name='undefined',
          cable_type='inter-array',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'])
    assert False
  except AssertionError:
    assert True

  try:
    Cable(id_='error2',
          name='undefined',
          cable_type='array',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'],
          burial_method='jet')
    assert False
  except AssertionError:
    assert True

  try:
    Cable(id_='error3',
          name='undefined',
          cable_type='array',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'],
          burial_depth=[1.5, 2.0, 2.0, 1.5, 1.0, 1.0])
    assert False
  except AssertionError:
    assert True

  try:
    Cable(id_='error4',
          name='cable',
          cable_type='dummy',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'],
          burial_method='jet')
    assert False
  except AssertionError:
    assert True

  try:
    Cable(id_='error5',
          name='cable',
          cable_type='array',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'],
          burial_method='dummy')
    assert False
  except AssertionError:
    assert True

  try:
    Cable(id_='warning1',
          name='cable',
          cable_type='array',
          system=None,
          bathymetry=[30, 30, 40, 30, 40, 50],
          seabed_type=['sands'] * 6,
          diameter=200,
          length=[120] * 6,
          drymass=150,
          min_bend_radius=2,
          elect_interfaces=['jtube'],
          burial_depth=[0, 0, 0, 0, 0, 0],
          burial_method='cutting')
    assert True
  except AssertionError:
    assert False


def test_operation_cpx1():
  class Object_dummy:
    pass
  Operation = Operation_class.get_complexity('low')

  obj_device = Object_dummy()
  obj_device.name = 'device'
  obj_device.type = 'dummy'
  op_device_install = Operation(operation_id='OP05_1',
                                operation_name='Device installation',
                                operation_description='Device',
                                objects=[obj_device])
  assert op_device_install.methods['load_out'] == 'lift'
  assert op_device_install.methods['transport'] == 'dry'
  assert op_device_install.requirements['lift'] == 0

  obj_pile = Object_dummy()
  obj_pile.name = 'foundation'
  obj_pile.type = 'pile'
  obj_pile.drymass = 10000
  obj_pile.bathymetry = 70
  op_pile_install = Operation(operation_id='OP01_1',
                              operation_name='Foundation installation',
                              operation_description='Pile',
                              objects=[obj_pile] * 10)
  assert op_pile_install.methods['load_out'] == 'lift'
  assert op_pile_install.methods['transport'] == 'dry'
  assert op_pile_install.methods['piling'] == 'hammering'
  assert op_pile_install.requirements['lift'] == 10
  assert op_pile_install.requirements['depth_max'] == 70
  assert op_pile_install.requirements['depth_min'] == 70

  class Cable_dummy:
    pass
  clb_export = Cable_dummy()
  clb_export.name = 'export cable'
  clb_export.type = 'export'
  clb_export.bathymetry = 50
  clb_export.length = 12000
  clb_export.diameter = 300
  op_export_install = Operation(operation_id='OP06_1',
                                operation_name='Cable installation',
                                operation_description='Simultaneous',
                                cables=[clb_export])
  assert 'lift' not in op_export_install.methods
  assert 'dry' not in op_export_install.methods
  assert op_export_install.methods['landfall'] == 'OCT'
  assert 'lift' not in op_export_install.requirements
  assert op_export_install.requirements['depth_max'] == 50
  assert op_export_install.requirements['depth_min'] == 50
  assert op_export_install.requirements['turn_capacity'] > 845
  assert op_export_install.requirements['turn_capacity'] < 850
  assert op_export_install.requirements['rov'] == 'inspection'


def test_operation_cpx3():
  Operation = Operation_class.get_complexity('high')
  Object = Object_class.get_complexity('high')
  Cable = Cable_class.get_complexity('high')
  # Test operation 1
  obj_device = Object(id_='D11',
                      name_of_object='WEC11',
                      type_of_object='device',
                      system=None,
                      length='15',
                      diameter='5',
                      drymass='15',
                      bathymetry='0',
                      seabed_type='sands',
                      topside_exists=True)
  op_di1 = Operation(operation_id='OP_DI1',
                     operation_name='Device installation op_di1',
                     objects=obj_device,
                     operation_description='Device installation')
  assert op_di1.name == 'Device installation op_di1'
  assert op_di1.description == 'Device installation'

  op_di1.update_methods_requirements_user()
  del op_di1

  # Test operation 2
  list_devices = [obj_device] * 3
  list_devices[1].id = 'D12'
  list_devices[1].name = 'WEC12'
  list_devices[2].id = 'D13'
  list_devices[2].name = 'WEC13'
  op_di2 = Operation(operation_id='OP_DI2',
                     operation_name='Device installation op_di1',
                     objects=list_devices,
                     operation_description='Device installation')
  del op_di2

  # Test operation 3
  obj_cable = Cable(id_='AC11',
                    name='array cable 11',
                    cable_type='array',
                    system=None,
                    bathymetry=[20, 20, 22, 22, 20, 20],
                    seabed_type=['sands'] * 6,
                    diameter=75,
                    length=[120] * 6,
                    drymass=150,
                    min_bend_radius=1,
                    elect_interfaces=['dry-mate', 'dry-mate'])
  op_ci1 = Operation(operation_id='OP_CI1',
                     operation_name='Cable installation op_ci1',
                     cables=[obj_cable],
                     operation_description='Simultaneous')
  assert op_ci1.name == 'Cable installation op_ci1'
  assert op_ci1.description == 'Simultaneous'
  del op_ci1

  # Test operation 4
  obj_pile = Object(id_='F11',
                    name_of_object='Pile11',
                    type_of_object='pile',
                    system=None,
                    length='40',
                    diameter='1.5',
                    drymass='30',
                    bathymetry='30',
                    seabed_type='sands',
                    burial_depth=30)
  list_piles = [obj_pile] * 3
  list_devices[1].id = 'F12'
  list_devices[1].name = 'Pile12'
  list_devices[2].id = 'F13'
  list_devices[2].name = 'Pile13'

  op_fi1 = Operation(operation_id='OP_FI1',
                     operation_name='Foundation Installation op_fi1',
                     objects=list_piles,
                     operation_description='pile installation')
  op_fi1.update_methods_requirements_user(method_piling='vibro-driver')
  del op_fi1

  # ### Test operation.update_requirements()


def test_barge():
  barge = Barge(
      beam=15,
      loa=70.2,
      draft=5.7
  )
  assert barge.beam == 15
  assert barge.loa == 70.2
  assert barge.draft == 5.7


def test_site():
  site = Site(
      name='Dummy',
      coord=(0.0, 0.0),
      metocean=None,
      map_name='dummy'
  )
  assert site.name == 'Dummy'
  assert site.coord[0] == 0.0
  assert site.coord[1] == 0.0
