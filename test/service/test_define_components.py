# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import libraries
import pandas as pd
import json

# Import classes
from dtop_lmo.business import Installation


def test_anchors_cpx1():
  import os
  local_path = '/test/test_inputs/Metocean'
  file_path = os.getcwd() + local_path
  with open(file_path + '/dummy.json', 'r') as fp:
    metocean_json = json.load(fp)
  df_metocean_dummy = pd.DataFrame(metocean_json)
  df_metocean_dummy.reset_index(inplace=True, drop=True)

  Inst_Funcs = Installation.get_complexity(
      complexity='low',
      start_date='06/2021'
  )
  site = Site(
      name='Dummy_site',
      coord=(41.4274, -8.9183),
      metocean=df_metocean_dummy
  )
  Inst_Funcs.site = site

  Inst_Funcs.merge_hierarchy()
  Inst_Funcs.define_components(device_topside_exists=True)

  devices = ['device' in obj.name.lower() for obj in Inst_Funcs.objects]
  piles = ['pile' in obj.type.lower() for obj in Inst_Funcs.objects]
  moorings = ['mooring' in obj.name.lower() for obj in Inst_Funcs.objects]
  gbas = [('gravity' in obj.type.lower() and 'base' in obj.type.lower())
          for obj in Inst_Funcs.objects]
  arrays = ['array' in cbl.type.lower() for cbl in Inst_Funcs.cables]
  export = ['export' in cbl.name.lower() for cbl in Inst_Funcs.cables]

  assert sum(devices) == 2
  assert all(devices) is False
  assert any(piles) is False
  assert sum(moorings) == 6
  assert sum(gbas) == 1
  assert sum(arrays) == 2
  assert sum(export) == 1
