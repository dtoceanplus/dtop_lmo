# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Testing Flask Applications: http://flask.pocoo.org/docs/1.0/testing/

import pytest

from dtop_lmo.service import create_app


@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
    })

    yield app


@pytest.fixture
def client(app):
    client = app.test_client()

    yield client


def test_foo_en(client):
    response = client.get('/gui/foo')
    assert b'Hello from gui foo!' in response.data
    assert b'Content of foo() is:' in response.data

def test_foo_fr(client):
    # Test French version of site.
    response = client.get('/gui/foo', headers=[('Accept-Language', 'fr')])
    assert b'Bonjour de gui foo!' in response.data
    assert b'Le contenu de foo () est:' in response.data
