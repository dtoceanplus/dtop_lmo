# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({"TESTING": True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer("lmo").has_pact_with(Provider("ec"), port=1233)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ec_farm(client):
  pact.given(
      "ec 1 exists and has farm"
  ).upon_receiving(
      "a request for farm"
  ).with_request(
      "GET", Term(r"/ec/\d+/farm", "/ec/1/farm")
  ).will_respond_with(
      200,
      body=Like({
          "layout": Like({
              "deviceID": EachLike(0),
              "easting": EachLike(0.1),
              "northing": EachLike(0.1)
          }),
          "number_devices": Like(1)
      })
  )

  with pact:
    response = client.post(
        "api/ec-farm",
        json={"ec_farm": f"{pact.uri}/ec/1/farm"}
    )

    assert response.status_code == 201
