# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('lmo').has_pact_with(Provider('sc'), port=1231)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_sc_farm(client):
  pact.given(
      'sc 1 exists and it has farm'
  ).upon_receiving(
      'a request for SC farm'
  ).with_request(
      'GET', Term(r'/sc/\d+/farm', '/sc/1/farm')
  ).will_respond_with(
      200,
      body=Like({
          "direct_values": Like({
              "bathymetry": Like({
                  "latitude": EachLike(-0.3548),
                  "longitude": EachLike(14.7826),
                  "value": EachLike(50.4)
              }),
              "seabed_type": Like({
                  "latitude": EachLike(-0.3548),
                  "longitude": EachLike(14.7826),
                  "value": EachLike('sand')
              })
          }),
          "info": Like({
              "latitude": EachLike(-0.3548),
              "longitude": EachLike(14.7826)
          })
      })
  )

  with pact:
    response = client.post(
        'api/sc-farm',
        json={"sc_farm": f"{pact.uri}/sc/1/farm"}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_sc_point(client):
  pact.given(
      'sc 1 exists and it has point'
  ).upon_receiving(
      'a request for SC point'
  ).with_request(
      'GET', Term(r'/sc/\d+/point', '/sc/1/point')
  ).will_respond_with(
      200,
      body=Like({
          "name": Like('site name')
      })
  )

  with pact:
    response = client.post(
        'api/sc-point',
        json={"sc_point": f"{pact.uri}/sc/1/point"}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_sc_timeseries(client):
  pact.given(
      'sc 1 exists and it has timeseries'
  ).upon_receiving(
      'a request for SC timeseries'
  ).with_request(
      'GET', Term(r'/sc/\d+/point/time_series', '/sc/1/point/time_series')
  ).will_respond_with(
      200,
      body=Like({
          "waves": Like({
              "hs": Like({
                  "values": EachLike(1.0),
                  "times": EachLike(3245.0)
              }),
              "tp": Like({
                  "values": EachLike(15.0)
              })
          }),
          "currents": Like({
              "mag": Like({
                  "values": EachLike(0.5)
              })
          }),
          "winds": Like({
              "mag10": Like({
                  "values": EachLike(20.0)
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/sc-time_series',
        json={"sc_time_series": f"{pact.uri}/sc/1/point/time_series"}
    )
    assert response.status_code == 201
