# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('lmo').has_pact_with(Provider('sk'), port=1236)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_sk(client):
  pact.given(
      'sk 1 exists and it has hierarchy'
  ).upon_receiving(
      'a request for SK hierarchy'
  ).with_request(
      'GET', Term(r'/sk/\d+/hierarchy', '/sk/1/hierarchy')
  ).will_respond_with(
      200,
      body=Like({
          "system": EachLike('SK'),
          "name_of_node": EachLike('SK0'),
          "design_id": EachLike('SK0_x_ml_0_seg_0'),
          "node_type": EachLike('Component'),
          "node_subtype": EachLike('line_segment'),
          "category": EachLike('Level 0'),
          "parent": EachLike('SK0_x_ml_0'),
          "child": EachLike('NA'),
          "gate_type": EachLike('AND'),
          "failure_rate_repair": EachLike('0.1'),
          "failure_rate_replacement": EachLike('0.1'),
          "hierarchy_data": Like({
              "anchor_list": EachLike({
                  "design_id": Like('SK0_x_ml_0_anchor_n_2_0'),
                  "type": Like('drag_anchor'),
                  "height": Like(4.192638924210453),
                  "width": Like(7.514374725381646),
                  "length": Like(6.971041943930892),
                  "mass": Like(19717.005962067928),
                  "upstream_id": EachLike('SK0_x_ml_0'),
                  "coordinates": Like([343.6, 0.0, -100.0]),
                  "cost": Like(14982.2)
              }),
              "foundation_list": EachLike({
                  "design_id": Like('SK0_x_pile_foundation1'),
                  "type": Like('pile'),
                  "height": Like(45.0),
                  "width": Like(3.5),
                  "length": Like(3.5),
                  "mass": Like(149795.3215391777),
                  "upstream_id": EachLike('SK0_x'),
                  "coordinates": Like([0., 0.0, -50.0, 0.0, 0.0, 0.0]),
                  "cost": Like(224692.9823087666),
                  "buried_height": Like(15.0)
              }),
              "line_segment_list": EachLike({
                  "design_id": Like('SK0_x_ml_0_seg_0'),
                  "material": Like('steel'),
                  "length": Like('396.84504448998473'),
                  "total_mass": Like('97623.88094453624'),
                  "diameter": Like('0.111'),
                  "upstream_id": EachLike('NA'),
                  "cost": Like(17371.5679904)
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/sk-hierarchy',
        json={"sk_hierarchy": f"{pact.uri}/sk/1/hierarchy"}
    )
    assert response.status_code == 201
