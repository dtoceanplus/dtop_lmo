# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('lmo').has_pact_with(Provider('ed'), port=1235)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_ed_cpx3(client):
  pact.given(
      'ed 1 exists at cpx 3'
  ).upon_receiving(
      'a request for ED results at CPX 3'
  ).with_request(
      'GET', Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
  ).will_respond_with(
      200,
      body=Like({
          "cable_installation": Like('Jetting'),
          "hierarchy_new": Like({
              "system": EachLike('ED'),
              "name_of_node": EachLike('ED1'),
              "design_id": EachLike('1'),
              "node_type": EachLike('System'),
              "node_subtype": EachLike('Export'),
              "category": EachLike('Level 0'),
              "parent": [
                  'NA',
                  'ED Subsystem',
                  'ED Subsystem',
                  ['ED1'],
                  ['ED2'],
                  ['Route2_1'],
                  ['Route1_1'],
                  ['Route1_1',
                  'Route2_1'],
                  ['Route2_1'],
                  ['Route1_1'],
                  ['Route1_1',
                  'Route2_1'],
                  ['Route2_1'],
                  ['Route1_1'],
                  ['Route1_1',
                  'Route2_1'],
                  ['Route2_1'],
                  ['Route2_1'],
                  ['Route1_1'],
                  ['Route1_1']
              ],
              "child": [
                  ['ED1',
                  'ED2'],
                  ['Route1_1'],
                  ['Route2_1'],
                  ['7', '6', '5', '4', '3', '2', '1', '0'],
                  ['12', '11', '10', '9', '8', '2', '1', '0'],
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA'
              ],
              "gate_type": EachLike('AND'),
              "failure_rate_repair": [
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0,
                  0.0
              ],
              "failure_rate_replacement": [
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  0.0009786441030323538,
                  0.0009786441030323538,
                  0.0215,
                  0.00054696,
                  0.0019143600000000004,
                  0.00920716,
                  0.0475,
                  0.0475,
                  0.0475,
                  0.0475,
                  0.0475,
                  0.0475,
                  0.0475
              ]
          }),
          "cable_dict": EachLike({
              "type_": Like('array'),
              "marker": Like(1),
              "burial_depth": EachLike(1.0),
              "split_pipe": EachLike(True),
              "cable_mattress": EachLike(True),
              "cable_x": EachLike(1010.0),
              "cable_y": EachLike(1020.0),
              "layer_1_start": EachLike(-50),
              "layer_1_type": EachLike('pebbles'),
              "cost": Like(1000.0),
              "mass": Like(100.5),
              "mbr": Like(1.15),
              "diameter": Like(10.0)
          }),
          "collection_point_dict": EachLike({
              "marker": Like(1),
              "location": Like([1010.0, 1250.0, -50.0]),
              "type_": Like('passive hub'),
              "output_connectors": Like('dry-mate'),
              "input_connectors": Like('wet-mate'),
              "cost": Like(1000.0),
              "mass": Like(100.0),
              "height": Like(10.0),
              "width": Like(10.0),
              "length": Like(10.0)
          }),
          "connectors_dict": EachLike({
              "type_": Like('dry-mate'),
              "marker": Like(1),
              "utm_x": Like(1010.0),
              "utm_y": Like(1250.0),
              "cost": Like(1000.0),
              "height": Like(10.5),
              "width": Like(10.5)
          }),
          "umbilical_dict": EachLike({
              "marker": Like(1),
              "seabed_connection_point": Like([1020.0, 1250.0, -50.0]),
              "length": Like(108.5),
              "cost": Like(1000.0),
              "mass": Like(100.5),
              "mbr": Like(1.15),
              "diameter": Like(10.0)
          })
      })
  )

  with pact:
    response = client.post(
        'api/ed-results',
        json={
            "cpx": '3',
            "ed_results": f"{pact.uri}/api/energy-deliv-studies/1/results"
        }
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_ed_cpx1(client):
  pact.given(
      'ed 2 exists at cpx 1'
  ).upon_receiving(
      'a request for ED results at CPX 1'
  ).with_request(
      'GET', Term(r'/api/energy-deliv-studies/\d+/results', '/api/energy-deliv-studies/1/results')
  ).will_respond_with(
      200,
      body=Like({
          "hierarchy_new": Like({
              "system": EachLike('ED'),
              "name_of_node": EachLike('ED1'),
              "design_id": EachLike('1'),
              "node_type": EachLike('System'),
              "node_subtype": EachLike('Export'),
              "category": EachLike('Level 0'),
              "parent": [
                  # TODO: THIS IS NOT OK! WE NEED TO FIGURE OUT HOW TO CONSUME MULTIPLE TYPES
                  'NA',
                  'ED Subsystem',
                  'ED Subsystem',
                  ['ED1'],
                  ['ED2'],
                  ['Route1_1'],
                  ['Route1_1', 'Route2_1'],
                  ['Route1_1', 'Route2_1'],
                  ['Route1_1', 'Route2_1']
              ],
              "child": [
                  # TODO: THIS IS NOT OK! WE NEED TO FIGURE OUT HOW TO CONSUME MULTIPLE TYPES
                  ['ED1', 'ED2'],
                  ['Route1_1'],
                  ['Route2_1'],
                  ['AC1', 'AC2', 'CP1', 'EC1'],
                  ['AC2', 'CP1', 'EC1'],
                  'NA',
                  'NA',
                  'NA',
                  'NA'
              ],
              "gate_type": EachLike('AND'),
              "failure_rate_repair": [
                  # TODO: THIS IS NOT OK! WE NEED TO FIGURE OUT HOW TO CONSUME MULTIPLE TYPES
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  0.00023999999999999998,
                  0.0004,
                  0.03,
                  0.00096
              ],
              "failure_rate_replacement": [
                  # TODO: THIS IS NOT OK! WE NEED TO FIGURE OUT HOW TO CONSUME MULTIPLE TYPES
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  'NA',
                  0.00023999999999999998,
                  0.0004,
                  0.03,
                  0.00096
              ]
          }),
          "cable_dict": EachLike({
              "type_": Like('array'),
              "marker": Like('ED1'),
              "length": Like(60.0),
              "cost": Like(1000.0)
          }),
          "collection_point_dict": EachLike({
              "marker": Like('CP1'),
              "type_": Like('passive hub'),
              "cost": Like(1000.0)
          })
      })
  )

  with pact:
    response = client.post(
        'api/ed-results',
        json={
            "cpx": '1',
            "ed_results": f"{pact.uri}/api/energy-deliv-studies/1/results"
        }
    )
    assert response.status_code == 201
