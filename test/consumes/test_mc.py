# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({"TESTING": True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer("lmo").has_pact_with(Provider("mc"), port=1232)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_mc_general(client):
  pact.given(
      "mc 1 exists and it has general"
  ).upon_receiving(
      "a request for machine general characteristics"
  ).with_request(
      "GET", Term(r"/mc/\d+/general", "/mc/1/general")
  ).will_respond_with(
      200,
      body=Like({
          "floating": Like(True),
          "machine_cost": Like(1000000.0)
      })
  )

  with pact:
    response = client.post(
        "api/mc-general",
        json={"mc_general": f"{pact.uri}/mc/1/general"}
    )

    assert response.status_code == 201


# @pytest.mark.skip()
def test_mc_dimensions(client):
  pact.given(
      "mc 1 exists and it has dimensions"
  ).upon_receiving(
      "a request for machine dimensions"
  ).with_request(
      "GET", Term(r"/mc/\d+/dimensions", "/mc/1/dimensions")
  ).will_respond_with(
      200,
      body=Like({
          "height": Like(10.0),
          "width": Like(10.0),
          "length": Like(10.0),
          "mass": Like(700000.0)
      })
  )

  with pact:
    response = client.post(
        "api/mc-dimensions",
        json={"mc_dimensions": f"{pact.uri}/mc/1/dimensions"}
    )

    assert response.status_code == 201
