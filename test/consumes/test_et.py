# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import atexit

import pytest
from pact import Consumer, Provider, Term, Like, EachLike

from dtop_lmo.service import create_app


@pytest.fixture
def app():
  app = create_app({'TESTING': True})
  yield app


@pytest.fixture
def client(app):
  client = app.test_client()
  yield client


pact = Consumer('lmo').has_pact_with(Provider('et'), port=1234)
pact.start_service()
atexit.register(pact.stop_service)


# @pytest.mark.skip()
def test_et_array(client):
  pact.given(
      'et 1 exists and it has array'
  ).upon_receiving(
      'a request for devices'
  ).with_request(
      'GET', Term(r'/energy_transf/\d+/array', '/energy_transf/1/array')
  ).will_respond_with(
      200,
      body=Like({
          "Hierarchy": Like({
              "value": Like({
                  "system": EachLike('ET'),
                  "name_of_node": EachLike('Array_01'),
                  "design_id": EachLike('Array_01'),
                  "node_type": EachLike('System'),
                  "node_subtype": EachLike('NA'),
                  "category": EachLike('Level 3'),
                  "parent": EachLike('NA'),
                  "child": [
                      [
                        "ET0", 
                        "ET1"
                      ], 
                      [
                        "ET0_PTO_0_0"
                      ], 
                      [
                        "ET0_PTO_0_0_MechT", 
                        "ET0_PTO_0_0_ElectT", 
                        "ET0_PTO_0_0_GridC"
                      ], 
                      "NA", 
                      "NA", 
                      "NA", 
                      [
                        "ET1_PTO_0_0"
                      ], 
                      [
                        "ET1_PTO_0_0_MechT", 
                        "ET1_PTO_0_0_ElectT", 
                        "ET1_PTO_0_0_GridC"
                      ], 
                      "NA", 
                      "NA", 
                      "NA"
                  ],
                  "gate_type": EachLike('AND'),
                  "failure_rate_repair": [
                      "NA", 
                      "NA", 
                      "NA", 
                      1.443, 
                      0.002, 
                      10.508, 
                      "NA", 
                      "NA", 
                      1.334, 
                      0.002, 
                      9.774
                  ], 
                  "failure_rate_replacement": [
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA", 
                      "NA"
                  ]
              })
          })
      })
  )

  with pact:
    response = client.post(
        'api/et-array',
        json={"et_array": f"{pact.uri}/energy_transf/1/array"}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_et_devices(client):
  pact.given(
      'et 1 exists and it has devices'
  ).upon_receiving(
      'a request for devices'
  ).with_request(
      'GET', Term(r'/energy_transf/\d+/devices', '/energy_transf/1/devices')
  ).will_respond_with(
      200,
      body=EachLike({
          "Dev_PTO_cost": Like({
              "value": Like(810000.0)
          }),
          "Dev_PTO_mass": Like({
              "value": Like(213000.0)
          })
      })
  )

  with pact:
    response = client.post(
        'api/et-devices',
        json={"et_devices": f"{pact.uri}/energy_transf/1/devices"}
    )
    assert response.status_code == 201


# @pytest.mark.skip()
def test_et_ptos(client):
  pact.given('et 1 exists and it has ptos').upon_receiving(
      'a request for ptos'
  ).with_request(
      'GET', Term(r"/energy_transf/\d+/devices/\d+/ptos", "/energy_transf/1/devices/1/ptos")
  ).will_respond_with(
      200,
      body=EachLike({
          "Elect_mass": Like({
              "value": Like(615.0)
          }),
          "Grid_mass": Like({
              "value": Like(59.3)
          }),
          "Mech_mass": Like({
              "value": Like(91287.0)
          }),
          "id": Like({
              "value": Like('PTO_0_0')
          })
      })
  )

  with pact:
    response = client.post(
        'api/et-ptos',
        json={"et_ptos": f"{pact.uri}/energy_transf/1/devices/1/ptos"}
    )
    assert response.status_code == 201
