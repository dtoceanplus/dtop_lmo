import STStudyPage from '../pages/st/study'

import LMOEntityListPage from '../pages/lmo/entity-list'
import LMOEntityDetailPage from '../pages/lmo/entity-detail'

describe('events', function () {
  it('create', () => {
    STStudyPage.openStudyPage()

    STStudyPage.createEntity('CT1')
    cy.wait(10000);

    cy.scrollTo(0,0)

    STStudyPage.execInIframe([
      LMOEntityListPage.fillInput,
      LMOEntityListPage.openSelect,
      LMOEntityListPage.fillSelect,
      LMOEntityListPage.submitCreateForm
    ])
    cy.wait(10000);
    STStudyPage.getEditButton('CT1')
    STStudyPage.getDeleteButton('CT1')
  })
  it('edit', () => {
    STStudyPage.editEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      LMOEntityDetailPage.getTitle,
      (iframe, element) => {
        element.invoke('text').should('equal', '1234')
      },
      LMOEntityDetailPage.goToForm,
      (iframe, element) => {
        cy.wait(1000);
      },
      LMOEntityDetailPage.submitForm
    ])
    cy.wait(10000);
  })
  it('delete', () => {
    cy.scrollTo(0,0)
    STStudyPage.deleteEntity('CT1')
    cy.wait(10000);

    cy.viewport(1000, 5000)
    STStudyPage.execInIframe([
      LMOEntityListPage.submitDeleteForm
    ])
    cy.wait(10000);
    STStudyPage.getCreateButton('CT1')
  })
})
