import STStudyPage from '../pages/st/study'

import MTFormPage from '../pages/mt/form'

import LMOEntityListPage from '../pages/lmo/entity-list'
import LMOEntityDetailPage from '../pages/lmo/entity-detail'

describe('provide-consume', function () {
  it('create 1 provider - 1 consumer', () => {
    STStudyPage.openStudyPage()

    // create provider entity
    STStudyPage.createEntity('PT1')

    cy.wait(10000);
    STStudyPage.execInIframe([
      MTFormPage.fillInput,
      MTFormPage.submitForm
    ])

    // create consumer entity
    STStudyPage.createEntity('CT1')
    cy.wait(10000);
    STStudyPage.execInIframe([
      {
        func: LMOEntityListPage.getAvailibleId,
        parameters: ['PT1']
      },
      (iframe, element) => {
        element.invoke('text').should("match", /^Tool PT1: \d+$/)
      },
      LMOEntityListPage.fillInput,
      LMOEntityListPage.openSelect,
      LMOEntityListPage.fillSelect,
      LMOEntityListPage.submitCreateForm
    ])
    cy.wait(10000);
  })

  it('create 2 providers - 1 consumer', () => {
    // create second provider entity
    STStudyPage.createEntity('PT2')

    cy.wait(10000);
    STStudyPage.execInIframe([
      MTFormPage.fillInput,
      MTFormPage.submitForm
    ])

    // delete consumer entity
    STStudyPage.deleteEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      LMOEntityListPage.submitDeleteForm
    ])

    // create consumer entity
    STStudyPage.createEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      {
        func: LMOEntityListPage.getAvailibleId,
        parameters: ['PT1']
      },
      (iframe, element) => {
        element.invoke('text').should("match", /^Tool PT1: \d+$/)
      },
      {
        func: LMOEntityListPage.getAvailibleId,
        parameters: ['PT2']
      },
      (iframe, element) => {
        element.invoke('text').should("match", /^Tool PT2: \d+$/)
      },
      LMOEntityListPage.fillInput,
      LMOEntityListPage.openSelect,
      LMOEntityListPage.fillSelect,
      LMOEntityListPage.submitCreateForm
    ])
    cy.wait(10000);
  })


  it('create 2 providers and 1 consumer and check provide ids to first provider', () => {
    // delete first provider entity
    STStudyPage.deleteEntity('PT1')
    cy.wait(10000);
    // create first provider entity
    STStudyPage.createEntity('PT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      {
        func: MTFormPage.getAvailibleId,
        parameters: ['PT1']
      },
      (iframe, element) => {
        element.invoke('text').should("equal", 'Tool PT1: null')
      },
      {
        func: MTFormPage.getAvailibleId,
        parameters: ['PT2']
      },
      (iframe, element) => {
        element.invoke('text').should("match", /^Tool PT2: \d+$/)
      },
      {
        func: MTFormPage.getAvailibleId,
        parameters: ['CT1']
      },
      (iframe, element) => {
        element.invoke('text').should("match", /^Tool CT1: \d+$/)
      },
      MTFormPage.submitForm
    ])
    cy.wait(10000);
  })


  it('edit consumer tool entity', () => {
    STStudyPage.editEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      LMOEntityDetailPage.getTitle,
      (iframe, element) => {
        element.invoke('text').should('equal', '1234')
      },
      LMOEntityDetailPage.goToForm,
      (iframe, element) => {
        cy.wait(1000);
      },
      LMOEntityDetailPage.submitForm
    ])
    cy.wait(10000);
  })

  it('delete consumer tool entity', () => {
    STStudyPage.deleteEntity('CT1')
    cy.wait(10000);

    STStudyPage.execInIframe([
      LMOEntityListPage.submitDeleteForm
    ])
    STStudyPage.getCreateButton('CT1')
  })
})
