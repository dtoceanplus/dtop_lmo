export const getInput = iframe => {
    return cy.wrap(iframe).find("[data-cy-lmo=input]");
  };
  export const getAvailibleId = (iframe, result, name = "PT1") => {
    return cy.wrap(iframe).find(`[data-cy-lmo=availibleId${name}]`);
  };
  export const getConsumedId = (iframe, result, name = "PT1") => {
    return cy.wrap(iframe).find(`[data-cy-lmo=consumedId${name}]`);
  };
  export const fillInput = iframe => {
    getInput(iframe).type("1234");
  };

  export const openSelect = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-lmo=select]")
      .click()
  };
  export const fillSelect = iframe => {
    cy.wrap(iframe)
    .find("[data-cy-lmo=selectItem1]")
    .click()
  };
  export const submitCreateForm = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-lmo=submitCreateButton]")
      .click();
  };
  export const submitDeleteForm = iframe => {
    cy.wrap(iframe)
      .find("[data-cy-lmo=submitDeleteButton]")
      .click();
  };

  export default {
    getInput,
    fillInput,
    openSelect,
    fillSelect,
    submitCreateForm,
    submitDeleteForm,
    getAvailibleId,
    getConsumedId
  };
