export const getTitle = iframe => {
    return cy.wrap(iframe).find("[data-cy-lmo=title]");
  };
  export const goToForm = iframe => {
    cy.wrap(iframe).find("[data-cy-lmo=performLink]").click()
  };
  export const submitForm = iframe => {
    cy.wrap(iframe).find("[data-cy-lmo=submitButton]").click()
  };


  export default {
    getTitle,
    goToForm,
    submitForm
  };
