atomicwrites==1.4.0
attrs==19.3.0
Babel==2.8.0
certifi==2020.6.20
chardet==3.0.4
click==7.1.2
colorama==0.4.3
cycler==0.10.0
DateTime==4.3
decorator==4.4.2
dredd-hooks==0.2.
Flask==1.1.2
Flask-Babel==1.0.0
Flask-Cors==3.0.8
flask-marshmallow==0.11.0
Flask-SQLAlchemy==2.4.3
geographiclib==1.50
geopy==2.0.0
global-land-mask==0.0.3
gunicorn==20.0.4
idna==2.10
importlib-metadata==1.7.0
itsdangerous==1.1.0
Jinja2==2.11.2
joblib==0.16.0
kiwisolver==1.2.0
MarkupSafe==1.1.1
marshmallow==3.6.1
marshmallow-sqlalchemy==0.23.1
matplotlib==3.2.2
more-itertools==8.4.0
networkx==2.4
numpy==1.19.0
packaging==20.4
pandas==1.2.4
pluggy==0.13.1
py==1.9.0
pymongo==3.11.2
pyparsing==2.4.7
#pytest==5.4.3
python-dateutil==2.8.1
python-dotenv==0.13.0
pytz==2020.1
redis==3.4.1
rq==1.3.0
requests==2.24.0
scikit-learn==0.23.1
scipy==1.5.0
six==1.15.0
sklearn==0.0
SQLAlchemy==1.3.18
threadpoolctl==2.1.0
urllib3==1.25.9
utm==0.7.0
vincenty==0.1.4
wcwidth==0.2.5
Werkzeug==1.0.1
zipp==3.1.0
zope.interface==5.1.0
