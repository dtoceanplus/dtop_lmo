# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This file is for creating Python package.

# Packaging Python Projects: https://packaging.python.org/tutorials/packaging-projects/
# Setuptools documentation: https://setuptools.readthedocs.io/en/latest/index.html

import setuptools

setuptools.setup(
    name="dtop-lmo",
    version="v1.5.3",
    packages=setuptools.find_packages(where='src'),
    package_dir={'':'src'},
    install_requires=[
        "flask",
        "flask-babel",
        "flask_cors",
        "marshmallow-sqlalchemy",
        "flask-sqlalchemy",
        "flask-marshmallow<0.12",
        "requests",
        "pandas",
        "numpy",
        "scipy",
        "matplotlib",
        "geopy",
        "networkx",
        "utm==0.7.0",
        "global_land_mask",
        "datetime",
        "dtop-shared-library",

    ],
    include_package_data=True,
    zip_safe=False,
    dependency_links=[
        "git+ssh://git@gitlab.com/dtoceanplus/dtop-shared-library.git#egg=dtop_shared_library-0.0.1"
    ],

)
