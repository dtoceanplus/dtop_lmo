# Overview

This document - to be written ...

LMO module is in the process of development.

LMO is distributed under the AGPL license see LICENSE file for more details.​

# Using

## Getting dtop-shared-library submodule sources :

```bash
git submodule update --init --recursive
```

## Building Docker images and Running them with docker-compose:

### Development environment

See `Makefile` and the necessary targets for the details.

Building local LMO backend image :

```bash
make build-dev-be
```

```bash
docker images

REPOSITORY         TAG IMAGE     ID               CREATED             SIZE
lmo_dev_backend    v1.1          36ec0ba41ce9     26 minutes ago      749MB
```

Running local LMO images with docker-compose :

```bash
make run-local-dev
```

Stopping local LMO iamages with docker-compose :

```bash
make stop-local-dev
```

# Clone (with submodules)
```bash
git clone --recurse-submodules git@gitlab.com:dtoceanplus/dtop_lmo.git
cd dtop_lmo
```

# Frontend
TODO
will improve when docker-compose build run
```
pip install -r requirements.txt
pip install -e .
pip install python-dotenv
flask init-db
flask run
```
```
cd src/dtop_lmo/gui
npm install
export VUE_APP_API_URL=http://127.0.0.1:5000
npm run dev
```


### Install `dtop-shared-library` submodule

```bash
cd src/dtop-shared-library
python setup.py install
```

### Test if installation was successful

```python
from dtop_shared_library.dummy import dummy_foo

def test_foo():
    assert dummy_foo()
```

## `dtop_lmo` Installation

### Python Package

See [setup.py](setup.py) file and [src](src) folder.

Installing the package for development:
```bash
pip install -e .
```

Build package:
```bash
python setup.py sdist
```
Package will be in `dist` folder.

Install package from `dist`:
```bash
pip install dtop-lmo --find-links dist
```

Note: Before running [`flask`](https://flask.palletsprojects.com/en/1.1.x/), he service must be initialized (see [**Service**](https://gitlab.com/wavec/dtoceanplus/dtop_lmo#start-service) section).

Documentation:
- [Python Packaging User Guide](https://packaging.python.org)
- [Installing Packages](https://packaging.python.org/tutorials/installing-packages/)
- [Packaging Python Projects](https://packaging.python.org/tutorials/packaging-projects/)

# Testing

See [test](test) folder.

The [pytest](https://docs.pytest.org/en/latest/) framework makes it easy to write tests.

Install pytest:
```bash
pip install pytest
```

Run tests:
```bash
python -m pytest
```
Run tests without warnings
```bash
python -m pytest --disable-pytest-warnings
```

## API testing

You need to install and run [Dredd](https://dredd.org/) to perform automatic API testing. Before installation, make sure that [npm](https://www.npmjs.com/) is available in your environment (if you are in a Conda environment, just run `conda install nodejs`).

Install Dredd:
```bash
npm install -g dredd
pip install dredd-hooks
pip install -e .
```

Run tests:
```bash
flask initb-db  # this removes all data from existing database
dredd
```

## Coverage

Coverage reports about code which should be tested.

Python has tool [Coverage.py](https://coverage.readthedocs.io/)
and PyTest has plugin for it [pytest-cov](https://pytest-cov.readthedocs.io/).

Install pytest-cov plugin
```bash
pip install pytest-cov
```

Run pytest with coverage:
```bash
python -m pytest --cov=dtop_lmo
```

# Languages

Python has tool [Babel](http://babel.pocoo.org/) that assist in internationalizing and localizing
and Flask has plugin for it [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

See [babel.cfg](babel.cfg) file.

Extracting Text to Translate:
```bash
pybabel extract .
```
It will create `message.pot` file with translations.
This file should not be stored in repository.

Generating a Language Catalog:
```bash
pybabel init -i messages.pot -d src/dtop_lmo/service/translations -l fr
```
It will create [src/dtop_lmo/service/translations/fr/LC_MESSAGES/messages.po](src/service/gui/translations/fr/LC_MESSAGES/messages.po) text file.

Use tool [poEdit](https://www.poedit.net/) for catalog (.po file) editing.

Compile Catalog:
```bash
pybabel compile -d src/dtop_lmo/service/translations
```
It will create `src/dtop_lmo/service/translations/fr/LC_MESSAGES/messages.mo` binary file.
This file is used by [Flask-Babel](https://pythonhosted.org/Flask-Babel/).

Tools:
- [poEdit](https://www.poedit.net/)

Documentation:
- [I18n and L10n](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-xiii-i18n-and-l10n)
- [Babel](http://babel.pocoo.org/)
- [Flask-Babel](https://pythonhosted.org/Flask-Babel/)


# Service API documentation

For service API description there is [OpenAPI Specification](https://openapis.org).
See [openapi.json](src/dtop_lmo/service/static/openapi.json).

For API documentation page there is [ReDoc](https://github.com/Rebilly/ReDoc) tool.
It is OpenAPI/Swagger-generated API Reference Documentation.
See [templates/api/index.html](src/dtop_lmo/service/templates/api/index.html) and
[api/foo/__init__.py](src/dtop_lmo/service/api/foo/__init__.py).

For generating OpenAPI files from code comments there is tool [oas](https://openap.is).

Install oas with [Node.js npm](https://nodejs.org/):
```bash
npm install oas -g
```
Init template:
```bash
oas init
```
See [swagger.yaml](swagger.yaml).

Generate OpenAPI description:
```bash
oas generate > src/dtop_lmo/service/static/openapi.json
```
See [openapi.json](src/dtop_lmo/service/static/openapi.json).

Documentation:
- [ReDoc](https://github.com/Rebilly/ReDoc)
- [OpenAPI Specification by Swagger](https://swagger.io/specification/)
- [oas](https://openap.is)
- [OpenAPI Tools](https://openapi.tools)


# GitLab CI

The [.gitlab-ci.yml](.gitlab-ci.yml) file tells the GitLab runner what to do.

GitLab offers a CI (Continuous Integration) service.
CI is for build and check each modification in repository.

Documentation:
- [Getting started with GitLab CI/CD](https://gitlab.com/help/ci/quick_start/README)
- [GitLab CI/CD Pipeline Configuration Reference](https://gitlab.com/help/ci/yaml/README.md)


# Start Service

Install `python-dotenv` for taking service default configuration from `.flaskenv`:

```DOS .bat
pip install python-dotenv
```

Without `.flaskenv` you should set environment variables for `flask`:

```DOS .bat
set FLASK_APP=dtop_lmo.service
set FLASK_ENV=development
set FLASK_RUN_PORT=5000
```

Initialize database:
```DOS .bat
flask init-db
```
Database will be created in `instance/dtop_main.sqlite`.

Start the service:
```DOS .bat
flask run
```

Now you can open in browser this URL: http://localhost:5000/.
