# 'build-TEST' builds a Docker image for test
# 'TEST' runs build and then the test itself
# 'TEST-nodeps' runs test, assuming the image exists already
# 'x-TEST' is what actually runs inside the image
# Force re-build all

SHELL = bash
.PHONY: *
export MODULE_SHORT_NAME = lmo
export DTOP_DOMAIN = dto.com

build-%:
	docker-compose `find ci -name '*.docker-compose' -printf ' -f %p'` build $*

run:
	docker-compose --file docker-compose.yml up nginx frontend backend

build-backend:
	docker-compose --file docker-compose.test.yml --file docker-compose.yml build backend

build-frontend:
	docker-compose --file docker-compose.yml build frontend

impose:
	docker-compose run --rm contracts

impose-local:
	make -f ci.makefile lmo-deploy
	docker-compose run --rm contracts

verify:
	docker-compose --project-name lmo -f verifiable.docker-compose -f verifier.docker-compose run verifier
	docker-compose --project-name lmo -f verifiable.docker-compose stop lmo
	docker cp `docker-compose --project-name lmo -f verifiable.docker-compose ps -q`:/app/.coverage.make-verify
