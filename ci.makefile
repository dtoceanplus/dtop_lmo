ifeq ($(OS),Windows_NT)
	SHELL:=bash
	win_pwd := $(shell cygpath -w $(shell pwd))
	docker-run = docker run --rm --volume $(win_pwd):/code
	docker-kept-run = docker run --volume $(win_pwd):/code
else
	docker-run = docker run --rm --volume ${PWD}:/code
	docker-kept-run = docker run --volume ${PWD}:/code
endif

test: clean pytest-mb dredd

install: lmo-deploy
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach  lmo client nginx

lmo-base:
	cp -f ./environment.yml ./docker/base/
	(cd ./docker/base/ && docker build --tag lmo-base .)

rm-dist:
	rm -rf ./dist

dist/dtop_lmo-*.tar.gz:
	python setup.py sdist

dist/dtop_shared_library-*.tar.gz:
	(cd ./src/dtop-shared-library && python ./setup.py sdist --dist-dir ../dist)

x-build: rm-dist dist/dtop_lmo-*.tar.gz dist/dtop_shared_library-*.tar.gz

build: lmo-base x-build

lmo-deploy: build
	cp -fr ./dist ./docker/deploy/
	(cd ./docker/deploy/ && docker build --cache-from=lmo-deploy --tag lmo-deploy .)

lmo-pytest: lmo-deploy
	cp -fr ./test ./docker/pytest/
	cp -f ./testing.makefile ./docker/pytest/
	(cd ./docker/pytest/ && docker build --tag lmo-pytest .)

pytest: lmo-pytest
	$(docker-kept-run) --name lmo-pytest-cont lmo-pytest make --file testing.makefile pytest
	docker cp lmo-pytest-cont:/app/report.xml .
	docker container rm lmo-pytest-cont

pytest-mb: lmo-deploy
	docker-compose \
	--file docker-compose.yml \
	--file docker-compose.test.yml \
	up --abort-on-container-exit --exit-code-from=pytest \
	lmo pytest mc sc lmo ed ec et

shell-pytest: lmo-pytest
	$(docker-run) --tty --interactive lmo-pytest bash

dredd: lmo-deploy
	touch ./hooks-output.txt
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --build --abort-on-container-exit --exit-code-from=dredd  dredd

clean:
	rm -fr dist
	rm -fr ./docker/deploy/dist
	rm -fr ./docker/pytest/test
	docker image rm --force lmo-base lmo-deploy lmo-pytest lmo-dredd

cypress-run: lmo-deploy
	docker-compose --file docker-compose.yml --file docker-compose.test.yml down
	docker-compose --file docker-compose.yml --file docker-compose.test.yml up --detach  lmo client nginx e2e-cypress
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 client:8080
	docker exec e2e-cypress /usr/wait-for-it.sh --timeout=300 lmo:5000
	docker exec e2e-cypress npx cypress run
	docker exec e2e-cypress npx nyc report --reporter=text-summary



## !
## Set the required module nickname
MODULE_SHORT_NAME=lmo

## !
## Set the required docker image TAG
MODULE_TAG=v1.5.3

## !
## Update the values of CI_REGISTRY_IMAGE to your module registry
CI_REGISTRY_IMAGE?=registry.gitlab.com/dtoceanplus/dtop_lmo

login:
	echo ${CI_REGISTRY_PASSWORD} | docker login --username ${CI_REGISTRY_USER} --password-stdin ${CI_REGISTRY_IMAGE}

logout:
	docker logout ${CI_REGISTRY_IMAGE}

build-prod-be:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} || true
	docker build --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG} \
	  		--file ./${MODULE_SHORT_NAME}-prod.dockerfile \
				.
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_backend:${MODULE_TAG}
	docker images

build-prod-fe:
	docker pull ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} || true
	docker build --build-arg DTOP_MODULE_SHORT_NAME="${MODULE_SHORT_NAME}" \
	  --cache-from ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
	  --tag ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG} \
		--file ./src/dtop_lmo/gui/frontend-prod.dockerfile \
	  		./src/dtop_lmo
	docker push ${CI_REGISTRY_IMAGE}/${MODULE_SHORT_NAME}_frontend:${MODULE_TAG}
	docker images

help:
	@echo URLs for access to properly deployed module ${MODULE_SHORT_NAME} on Staging server :
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com
	@echo "       " - https://${MODULE_SHORT_NAME}.dto.opencascade.com/api
