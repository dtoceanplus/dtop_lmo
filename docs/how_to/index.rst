.. _lmo-how-to:

*************
How-to Guides
*************

Each *how-to guide* listed below contains step-by-step instructions on how to achieve specific outcomes using the Logistics and Marine Operations module. 
These guides are intended for users who have previously completed all the :ref:`Logistics and Marine Operations tutorials <lmo-tutorials>` and have a good knowledge of the features and workings of the Logistics and Marine Operations module. 
While the tutorials give an introduction to the basic usage of the module, these *how-to guides* tackle slightly more advanced topics.

- :ref:`lmo-how-to-complexity`

- :ref:`lmo-how-to-formatdata`

- :ref:`lmo-how-to-low-complexity`

- :ref:`lmo-how-to-high-complexity`


.. toctree::
   :maxdepth: 2
   :hidden:

   lmo-how-to-chose-complexity
   lmo-how-to-formatdata
   lmo-how-to-low-complexity
   lmo-how-to-high-complexity