.. _lmo-how-to-formatdata:

How to prepare data for using the Logistics and Marine Operations module
========================================================================

This guide summarises the data requirements and specifications for
running the Logistics and Marine Operations module in full complexity
standalone mode (introduced in the “Other module inputs” tab), but notes
which parameters are not required at low complexity and which come from
other modules in integrated mode.

Format the Machine Characterisation input file
----------------------------------------------

The Machine Characterisation input file compiles information related to
the device, stored in a json format. The file describes whether the
device is floating (TRUE) or bottom-fixed (FALSE), specifies the device
structural costs (*machine_costs*), as well as the device dimensions (in
m) and mass (in kg), crucial parameters to specify areas, and load
requirements. An example input file for the RM3 (VS2_VCx) test case is
provided below:


.. code:: 

    {    "general": {
          "floating": true,
          "machine_cost": 2939052.37    },
      "dimensions": {
          "draft": 35,
          "height": 42,
          "width": 30,
          "length": 30,
          "mass": 680000    } 
    }

  Machine Characterisation input file


Format the Energy Capture input file
------------------------------------

The Energy Capture input file compiles information related to the farm,
stored in a json format. The file includes data such as the number of
devices, list of device IDs, and coordinates (latitude and longitude).
An example input file for a farm of 10 devices (VS2_VC5) is provided below.

.. code:: 

  {  "layout": {
    "deviceID": [1,2,3,4,5,6,7,8,9,10],
    "latitude": [0,10,20,30,40,50,60,70,80,90,100],
    "longitude": [0,10,20,30,40,50,60,70,80,90,100]   
    },
    "number_devices": 10 
  }

  Energy Capture input file


Format the Energy Transformation input files 
--------------------------------------------

The input file from the Energy Transformation module is significantly
more complex than the two previous ones. Firstly, the input file
includes the ET system hierarchy, described in the D6.3 RAMS alpha
version [18], which expresses the relationships between components and
subsystems of the Energy Transformation system. Secondly, the ET input
file includes the costs and masses of the PTO components for each
device.

The hierarchy trees can be partially understood as the inverse of a
failure tree, built using Boolean logic to evaluate whether components
are working (1) or not (0). This allows the quantification of the
impacts of component critical failure on the system and identifies which
critical component failures to generate critical failures at the system
level for each device.

In the hierarchy, all components are listed. Each component/subsystem
has an identifiable *design id* and a node name *(name_of_node).*
Indivisible components are referred to as “Level 0” and have no
“Children”. A bottom-up approach from child to parent is adopted for
defining category levels, from Level 0 all the way up to the top node:
ET1 (installed in device OEC1). Each device may have more than one PTO,
which may be operating simultaneously (this is the case of RM1). Each
PTO may be decomposed into three different parts: the mechanical
transformation system (“MechT” – e.g. air turbine), the electrical
transformation system (“ElecT” – e.g. generator), and the grid
conditioning system (“GridC”, e.g. back to back power converter).
Components and subsystems may have specified failure rates. Hierarchical
relationships are expressed by the components listed as children, as
well as the logic gate, which defines the type of relationship (the AND
gate means that all children must be working for the parent system being
operational, OR gate means that at least one child must be operational).


.. code:: 

  { "array": {
    "Hierarchy": {
      "value": {
        "category": [
          "Level 3",
          "Level 2",
          "Level 1",
          "Level 0",
          "Level 0",
          "Level 0",
          "Level 1",
          "Level 0",
          "Level 0",
          "Level 0"
        ],
        "child": [
        ["ET1"],
        ["ET1_PTO_0_0", "ET1_PTO_1_0"],
        ["ET1_PTO_0_0_MechT","ET1_PTO_0_0_ElectT", "ET1_PTO_0_0_GridC"],
        "NA",
        "NA",
        "NA",
        ["ET1_PTO_1_0_MechT", "ET1_PTO_1_0_ElectT", "ET1_PTO_1_0_GridC"],
        "NA",
        "NA",
        "NA"  ],
        "design_id": [
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01",
        "Array_01"
        ],
        "failure_rate_replacement": [
          "NA",
          "NA",
          "NA",
          0.008785833,
          0.00136,
          0.004547059,
          "NA",
          0.008785833,
          0.00136,
          0.004547059
        ],
        "failure_rate_repair": [
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA"
        ],
        "gate_type": [
          "AND",
          "OR",
          "AND",
          "AND",
          "AND",
          "AND",
          "AND",
          "AND",
          "AND",
          "AND"
        ],
        "name_of_node": [
          "Array_01",
          "ET1",
          "ET1_PTO_0_0",
          "ET1_PTO_0_0_MechT",
          "ET1_PTO_0_0_ElectT",
          "ET1_PTO_0_0_GridC",
          "ET1_PTO_1_0",
          "ET1_PTO_1_0_MechT",
          "ET1_PTO_1_0_ElectT",
          "ET1_PTO_1_0_GridC"
        ],
        "node_subtype": [
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA",
          "NA"
        ],
        "node_type": [
          "System",
          "Device",
          "PTO",
          "Component",
          "Component",
          "Component",
          "PTO",
          "Component",
          "Component",
          "Component"
        ],
        "parent": [
        "NA",
        ["Array_01"],
        ["ET1"],
        ["ET1_PTO_0_0"],
        ["ET1_PTO_0_0"],
        ["ET1_PTO_0_0"],
        ["ET1"],
        ["ET1_PTO_1_0"],
        ["ET1_PTO_1_0"],
        ["ET1_PTO_1_0"]
        ],
        "system": [
          "ET",
          "ET",
          "ET",
          "ET",
          "ET",
          "ET",
          "ET",
          "ET",
          "ET",
          "ET"
        ]
      }
    }
  },
  "devices": [
    {
      "Dev_PTO_cost": {
        "value": 1908099
      },
      "Dev_PTO_mass": {
        "value": 109000
      },
      "Dev_rated_power": {
        "value": 300.0
      },
      "id": {
        "value": "1"
      },
      "ptos": [
      {
      "Elect_cost": {
        "value": 254725
      },
      "Elect_mass": {
        "value": 109000
      },
      "Grid_cost": {
        "value": 522587
      },
      "Grid_mass": {
        "value": 109000
      },
      "Mech_cost": {
        "value": 1130786.938
      },
      "Mech_mass": {
        "value": 109000
      },
      "id": {
        "value": "PTO_0_0"
      }
      },
      {
        "Elect_cost": {
          "value": 254725
        },
        "Elect_mass": {
          "value": 109000
        },
        "Grid_cost": {
          "value": 522587
        },
        "Grid_mass": {
          "value": 109000
        },
        "Mech_cost": {
          "value": 1130786.938
        },
        "Mech_mass": {
          "value": 109000

        },
        "id": {
          "value": "PTO_1_0"
        }
      }
      ]
    }
  ]
  }

  Energy Transformation input file for one RM1 device.

Format the Energy Delivery inputs
---------------------------------

.. code:: 

  {
      "cable_dict": [
          {
              "burial_depth": [
                  0.5,
                  0.5,
                  0.5,
                  0.5,
                  ...
              ],
              "cable_mattress": [
                  false,
                  false,
                  false,
                  false,
                  ...
              ],
              "cable_x": [
                  398675.0,
                  398625.0,
                  398575.0,
                  398525.0,
                  ...
              ],
              "cable_y": [
                  4518475.0,
                  4518475.0,
                  4518525.0,
                  4518575.0,
                  ...
              ],
              "cost": 5344755.072177423,
              "layer_1_start": [
                  -2.84319,
                  -3.43957,
                  -4.3625898,
                  -5.28549,
                  ...
              ],
              "layer_1_type": [
                  "loose sand",
                  "loose sand",
                  "loose sand",
                  "loose sand",
                  ...
              ],
              "length": 6680.943840221778,
              "marker": 0,
              "split_pipe": [
                  false,
                  false,
                  false,
                  ...
              ],
              "type_": "export",
          }
      ],
      "cable_installation": "Ploughing",
      "collection_point_dict": [
        {
          "cost": 1410128,
          "input_connectors": null,
          "location": null,
          "marker": "CP1",
          "output_connectors": null,
          "type_": "passive hub"
          }
      ],
      "connectors_dict": [
          {
              "cost": 150000.0,
              "db_key": 125,
              "marker": 1,
              "type_": "wet-mate",
              "utm_x": 393295.0,
              "utm_y": 4521615.0
          },
          {
              "cost": 150000.0,
              "db_key": 125,
              "marker": 3,
              "type_": "wet-mate",
              "utm_x": 393285.0,
              "utm_y": 4521615.0
          }
      ],

      "hierarchy_new": {
          "category": [
              "Level 3",
              "Level 2",
              "Level 1",
              "Level 0",
              "Level 0",
              "Level 0",
              "Level 0"
          ],
          "child": [
              [
                  "ED1"
              ],
              [
                  "Route1_1"
              ],
              [
                  "3",
                  "2",
                  "1",
                  "0"
              ],
              "NA",
              "NA",
              "NA",
              "NA"
          ],
          "design_id": [
              "NA",
              "NA",
              "NA",
              "2",
              "0",
              "3",
              "1"
          ],
          "failure_rate_repair": [
              "NA",
              "NA",
              "NA",
              0.00907905676510056,
              1.0007786634898617,
              0.047500574399999995,
              0.047500574399999995
          ],
          "failure_rate_replacement": [
              "NA",
              "NA",
              "NA",
              0.00907905676510056,
              1.0007786634898617,
              0.047500574399999995,
              0.047500574399999995
          ],
          "gate_type": [
              "OR",
              "OR",
              "AND",
              "NA",
              "NA",
              "NA",
              "NA"
          ],
          "name_of_node": [
              "ED Subsystem",
              "ED1",
              "Route1_1",
              "2",
              "0",
              "3",
              "1"
          ],
          "node_subtype": [
              "NA",
              "NA",
              "NA",
              " umbilical",
              " export",
              " wet-mate",
              " wet-mate"
          ],
          "node_type": [
              "System",
              "System",
              "Energy route",
              "Component",
              "Component",
              "Component",
              "Component"
          ],
          "parent": [
              "NA",
              "NA",
              [
                  "ED1"
              ],
              [
                  "Route1_1"
              ],
              [
                  "Route1_1"
              ],
              [
                  "Route1_1"
              ],
              [
                  "Route1_1"
              ]
          ],
          "system": [
              "ED",
              "ED",
              "ED",
              "ED",
              "ED",
              "ED",
              "ED"
          ]
      },
      "umbilical_dict": [
          {
              "cost": 48487.57918823232,
              "device": "Device001",
              "length": 60.60947398529039,
              "marker": 2,
              "seabed_connection_point": [
                  393295.0,
                  4521615.0,
                  -51.7464981
                ]

          }
      ]
  }

  Energy Delivery input file for one RM1 device



Format the Station Keeping input files 
--------------------------------------

.. code:: 

  {
    "hierarchy": {
        "system": [
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK",
            "SK"
        ],
        "name_of_node": [
            "SK1_x",
            "SK1_x_ml_0_seg_0",
            "SK1_x_ml_0_anchor_n_2_0",
            "SK1_x_ml_0",
            "SK1_x_ml_1_seg_0",
            "SK1_x_ml_1_anchor_n_2_0",
            "SK1_x_ml_1",
            "SK1_x_ml_2_seg_0",
            "SK1_x_ml_2_anchor_n_2_0",
            "SK1_x_ml_2",
            "SK1"
        ],
        "design_id": [
            "NA",
            "SK1_x_ml_0_seg_0",
            "SK1_x_ml_0_anchor_n_2_0",
            "NA",
            "SK1_x_ml_1_seg_0",
            "SK1_x_ml_1_anchor_n_2_0",
            "NA",
            "SK1_x_ml_2_seg_0",
            "SK1_x_ml_2_anchor_n_2_0",
            "NA",
            "NA"
        ],
        "node_type": [
            "System",
            "Component",
            "Component",
            "System",
            "Component",
            "Component",
            "System",
            "Component",
            "Component",
            "System",
            "System"
        ],
        "node_subtype": [
            "stationkeeping",
            "line_segment",
            "anchor",
            "mooring_line",
            "line_segment",
            "anchor",
            "mooring_line",
            "line_segment",
            "anchor",
            "mooring_line",
            "stationkeeping"
        ],
        "category": [
            "Level 2",
            "Level 0",
            "Level 0",
            "Level 1",
            "Level 0",
            "Level 0",
            "Level 1",
            "Level 0",
            "Level 0",
            "Level 1",
            "Level 3"
        ],
        "parent": [
            "NA",
            "SK1_x_ml_0",
            "SK1_x_ml_0",
            "SK1_x",
            "SK1_x_ml_1",
            "SK1_x_ml_1",
            "SK1_x",
            "SK1_x_ml_2",
            "SK1_x_ml_2",
            "SK1_x",
            "NA"
        ],
        "child": [
            [
                "SK1_x_ml_0",
                "SK1_x_ml_1",
                "SK1_x_ml_2"
            ],
            [
                "NA"
            ],
            [
                "NA"
            ],
            [
                "SK1_x_ml_0_seg_0",
                "SK1_x_ml_0_anchor_n_2_0"
            ],
            [
                "NA"
            ],
            [
                "NA"
            ],
            [
                "SK1_x_ml_1_seg_0",
                "SK1_x_ml_1_anchor_n_2_0"
            ],
            [
                "NA"
            ],
            [
                "NA"
            ],
            [
                "SK1_x_ml_2_seg_0",
                "SK1_x_ml_2_anchor_n_2_0"
            ],
            [
                "SK1_x"
            ]
        ],
        "gate_type": [
            "AND",
            "NA",
            "NA",
            "AND",
            "NA",
            "NA",
            "AND",
            "NA",
            "NA",
            "AND",
            "AND"
        ],
        "failure_rate_repair": [
            "NA",
            0.0000000001,
            0.0000000001,
            "NA",
            0.0000000001,
            0.0000000001,
            "NA",
            0.0000000001,
            0.0000000001,
            "NA",
            "NA"
        ],
        "failure_rate_replacement": [
            "NA",
            2.4352799999999997,
            0.0000000001,
            "NA",
            2.4352799999999997,
            0.0000000001,
            "NA",
            2.4352799999999997,
            0.0000000001,
            "NA",
            "NA"
        ],
        "hierarchy_data": {
            "anchor_list": [
                {
                    "design_id": "SK1_x_ml_0_anchor_n_2_0",
                    "type": "drag_anchor",
                    "height": 3.2907521354288622,
                    "width": 5.898160564005535,
                    "length": 5.471773895058202,
                    "mass": 9535.483174496047,
                    "upstream_id": [
                        "SK1_x_ml_0"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "coordinates": [
                        350,
                        0,
                        -70
                    ],
                    "cost": 47677.41587248023
                },
                {
                    "design_id": "SK1_x_ml_1_anchor_n_2_0",
                    "type": "drag_anchor",
                    "height": 3.2907521354288622,
                    "width": 5.898160564005535,
                    "length": 5.471773895058202,
                    "mass": 9535.483174496047,
                    "upstream_id": [
                        "SK1_x_ml_1"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "coordinates": [
                        -175.0,
                        303.108,
                        -70.0
                    ],
                    "cost": 47677.41587248023
                },
                {
                    "design_id": "SK1_x_ml_2_anchor_n_2_0",
                    "type": "drag_anchor",
                    "height": 3.2907521354288622,
                    "width": 5.898160564005535,
                    "length": 5.471773895058202,
                    "mass": 9535.483174496047,
                    "upstream_id": [
                        "SK1_x_ml_2"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "coordinates": [
                        -175.0,
                        -303.108,
                        -70.0
                    ],
                    "cost": 47677.41587248023
                }
            ],
            "foundation_list": [],
            "line_segment_list": [
                {
                    "design_id": "SK1_x_ml_0_seg_0",
                    "material": "nylon",
                    "length": 340.7,
                    "total_mass": 4703.105113119999,
                    "diameter": 0.146,
                    "upstream_id": [
                        "NA"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "cost": 17371.5679904
                },
                {
                    "design_id": "SK1_x_ml_1_seg_0",
                    "material": "nylon",
                    "length": 340.7,
                    "total_mass": 4703.105113119999,
                    "diameter": 0.146,
                    "upstream_id": [
                        "NA"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "cost": 17371.5679904
                },
                {
                    "design_id": "SK1_x_ml_2_seg_0",
                    "material": "nylon",
                    "length": 340.7,
                    "total_mass": 4703.105113119999,
                    "diameter": 0.146,
                    "upstream_id": [
                        "NA"
                    ],
                    "downstream_id": [
                        "NA"
                    ],
                    "cost": 17371.5679904
                }
            ]
        }
    }
  }

  Station Keeping input file for one RM1 device
