.. _lmo-how-to-low-complexity:

How to use Logistics and Marine Operations at low complexity
==========================================================

The low complexity (level 1) of the Logistics and Marine Operations tool
can be used to get a quick logistic design for the installation, maintenance, and 
Decommissioning lifecycle stages of a given project. 


1. Enter Project data
---------------------

At low complexity, the input parameters for the LMO tool are split
into Project, Machine, Farm, Site, and Subsystems.

In integrated mode, all of the Machine, Farm, Site, and Subsystem input parameters come 
from other tools, as shown in the middle column in the tables below. 

In standalone mode the user must enter all parameters, as shown in the right column.

In both cases, the user must specify which are the project lifecycle stages
to be analysed (i.e. installation, maintenance, and/or decommissioning). It must be
noted that the decommissioning phase may only be computed if the installation phase
is selected.

**Project inputs:**

+---------------------------------------+-----------------------------------+
|             Project inputs            |            Data source            |
|                                       +-----------------+-----------------+
|                                       | Integrated mode | Standalone mode |
+=======================================+=================+=================+
| Installation start month (mm/yyyy)    | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Maintenance start month (mm/yyyy)     | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Consider device repair at port (Bool) | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Device fully submerged (Bool)         | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Operations maximum wave height (m)    | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Project lifetime (years)              | User            | User            |
+---------------------------------------+-----------------+-----------------+

 
**Machine Characterisation inputs:** 

+---------------------+-----------------------------------+
|                     |            Data source            |
|    Device   input   +-----------------+-----------------+
|                     | Integrated mode | Standalone mode |
+=====================+=================+=================+
| Device type         | MC              | User            |
+---------------------+-----------------+-----------------+
| Device topology     | MC              | User            |
+---------------------+-----------------+-----------------+
| Device dimensions   | MC              | User            |
+---------------------+-----------------+-----------------+
| Device mass         | MC              | User            |
+---------------------+-----------------+-----------------+

**Farm inputs:** 

+-------------------+-----------------------------------+
|                   |            Data source            |
|    Farm   input   +-----------------+-----------------+
|                   | Integrated mode | Standalone mode |
+===================+=================+=================+
| Number of devices | EC              | User            |
+-------------------+-----------------+-----------------+
| Farm layout       | EC              | User            |
+-------------------+-----------------+-----------------+

**Site inputs:** 

+------------------------+-----------------------------------+
|                        |            Data source            |
|      Site   input      +-----------------+-----------------+
|                        | Integrated mode | Standalone mode |
+========================+=================+=================+
| Met-ocean timeseries   | SC              | User            |
| (Hs, Tp, Ws, Cs)       |                 |                 |
+------------------------+-----------------+-----------------+
| Site bathymetry        | SC              | User            |
+------------------------+-----------------+-----------------+
| Seabed characteristics | SC              | User            |
+------------------------+-----------------+-----------------+

**Energy transformation sub-system inputs:** 

+--------------------------------+-----------------------------------+
|                                |            Data source            |
| Energy   Transformation inputs +-----------------+-----------------+
|                                | Integrated mode | Standalone mode |
+================================+=================+=================+
| ET hierarchy                   | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_elect                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_mech                  | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_grid                  | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Total mass PTO                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs elect                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs mech                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs elect                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Total PTO costs                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Rated power                    | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO failure rates              | ET              | User            |
+--------------------------------+-----------------+-----------------+

**Station keeping sub-system inputs:** 

+------------------------------+-----------------------------------+
|                              |            Data source            |
|   Station   Keeping inputs   +-----------------+-----------------+
|                              | Integrated mode | Standalone mode |
+==============================+=================+=================+
| SK hierarchy file            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor types                 | SK              | User            |
+------------------------------+-----------------+-----------------+
| Number of anchors per device | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor height                | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor width                 | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor length                | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor mass                  | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor soil type             | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor cost                  | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring length               | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring    mass              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring diameter             | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring    line cost         | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation type              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation height            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation diameter          | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation length            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation mass              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation burial            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Component failure rates      | SK              | User            |
+------------------------------+-----------------+-----------------+

**Energy delivery sub-system inputs:** 

+---------------------------------+-----------------------------------+
|                                 |            Data source            |
|     Energy   Delivery inputs    +-----------------+-----------------+
|                                 | Integrated mode | Standalone mode |
+=================================+=================+=================+
| ED Hierarchy                    | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point catalogue ID   | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point location       | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point type           | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point costs          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Cable ID                        | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Cable route                     | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable length                    | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable burial depth              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable soil type                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable type                      | ED              | User            |
+---------------------------------+-----------------+-----------------+
| route splitpipe                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| route cable protection mattress | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector position              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector type                  | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector cost                  | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector catalogue ID          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical position              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical costs                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical catalogue ID          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Component failure rates         | ED              | User            |
+---------------------------------+-----------------+-----------------+

**Selection of the project phase to analyse:** 

+---------------------------------+-----------------------------------+
|                                 |            Data source            |
|   Project phases to consider    +-----------------+-----------------+
|                                 | Integrated mode | Standalone mode |
+=================================+=================+=================+
| Installation                    | User            | User            |
+---------------------------------+-----------------+-----------------+
| Maintenance                     | User            | User            |
+---------------------------------+-----------------+-----------------+
| Decommissioning                 | User            | User            |
+---------------------------------+-----------------+-----------------+

To **view, update or delete** the Project inputs, click ``View/Update/Delete`` 
under the appropriate input type.

- To update values, only the parameters that need to be modified should be entered. 
  Click ``Update`` after making the updates to save these.

- To delete the inputs click ``Delete``, which will produce a pop-up window asking for confirmation. 
  Click ``Delete`` again to permenantly delete these inputs from the database.

2. Enter operation inputs
---------------------------

At low complexity, the LMO module does not require additional inputs in the Operations tab.
However, for each of the desire lifecycle phases, the operations must be generated.
Depending on the lifecycle phases selected in the Project tab, the user will be asked 
to generate the operations for each lifecycle phase by pressing the ``Generate`` button.

- To generate the installation operations: ``Compute``

- To generate the maintenance operations: ``Generate``

- To generate the decommissioning operations: ``Generate``

.. tip::
  If only one lifecycle phase (e.g. installation) has been selected in the Project
  tab, then only one button (e.g. "Generate installation operations") will be 
  shown to the user.

3. Run computations 
-------------------

.. tip::
  Computation time in the low complexity level should take less than a minute.

In the Calculations tab, the user may choose to compute the results for the
previously selected lifecycle phases.

Once the calculations have been completed, an alert window will inform the user.
The user may then choose to view the results in the Results tab.

.. note::
  For each of the selected lifecycle phases, once the computation has been run,
  the user is allowed to delete the lifecycle phase (e.g. Installation) results,
  by pressing the ``Delete [phase] results``.

4. View results 
-----------------

In the Results tab, the user may click ``view results`` to see a summary of the
logistic design for each project lifecycle phase.

The following output parameters are presented in the tabular form for
for the simplified logistic design at low complexity.

-  **Name:** Operation name

- **Total duration:** Duration of the operation, in hours.

- **Vessel costs:** Total vessel costs, in €.

- **Terminal costs:** Total port terminal costs, in €.

- **Equipment costs:** Total equipment costs, in €.

- **Total costs:** Total operation costs, in €.

- **Terminal:** id of the selected port terminals 

- **Vessels:** ids of the selected vessels 

- **Equipment:** ids of the selected equipment 

A gantt chart is also produced, featuring:

-  **Operation name:** Operation name

-  **Start date:** Start date of the operation

-  **End date:** End date of the operation

-  **Duration:** Operation duration, in days.

-  **Waiting time:** Waiting time is represented graphically (in dark blue) on the gant chart bars. 

For the maintenance results, the gantt chart also includes:

-  **Failure date:** Calendar date in which component failure occured.

Optionally, the user is allowed to review the more detailed information 
provided on each of the logistics design solutions. The design data for each 
logistic design solutions can be downloaded in json format.
