.. _lmo-how-to-high-complexity:

How to use Logistics and Marine Operations at high complexity
==========================================================

.. warning::
    This content is still for the low complexity and must be updated

The high complexity (level 2 and 3) of the Logistics and Marine Operations tool
can be used to get a more in-depth and accurate design for the installation, maintenance, and 
Decommissioning lifecycle stages of a given project. 


1. Enter Project data
---------------------

At low complexity, the input parameters for the LMO tool are split
into Project, Machine, Farm, Site, and Subsystems.

In integrated mode, all of the Machine, Farm, Site, and Subsystem input parameters come 
from other tools, as shown in the middle column in the tables below. 

In standalone mode the user must enter all parameters, as shown in the right column.

In both cases, the user must specify which are the project lifecycle stages
to be analysed (i.e. installation, maintenance, and/or decommissioning). It must be
noted that the decommissioning phase may only be computed if the installation phase
is selected.

**Project inputs:**

+---------------------------------------+-----------------------------------+
|             Project inputs            |            Data source            |
|                                       +-----------------+-----------------+
|                                       | Integrated mode | Standalone mode |
+=======================================+=================+=================+
| Installation start day (dd/mm/yyyy)   | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Maintenance start day (dd/mm/yyyy)    | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Consider device repair at port (Bool) | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Device fully submerged (Bool)         | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Project lifetime (years)              | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Device towing draft                   | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Safety factor for vessel selection    | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Fuel price (€/ton)                    | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Specific Fuel Oil Consumption (g/kWh) | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Average vessel load factor            | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Weather window statistics             | User            | User            |
+---------------------------------------+-----------------+-----------------+
| Vessel statistics                     | User            | User            |
+---------------------------------------+-----------------+-----------------+

 
**Machine Characterisation inputs:** 

+---------------------+-----------------------------------+
|                     |            Data source            |
|    Device   input   +-----------------+-----------------+
|                     | Integrated mode | Standalone mode |
+=====================+=================+=================+
| Device type         | MC              | User            |
+---------------------+-----------------+-----------------+
| Device topology     | MC              | User            |
+---------------------+-----------------+-----------------+
| Device dimensions   | MC              | User            |
+---------------------+-----------------+-----------------+
| Device mass         | MC              | User            |
+---------------------+-----------------+-----------------+

**Farm inputs:** 

+-------------------+-----------------------------------+
|                   |            Data source            |
|    Farm   input   +-----------------+-----------------+
|                   | Integrated mode | Standalone mode |
+===================+=================+=================+
| Number of devices | EC              | User            |
+-------------------+-----------------+-----------------+
| Farm layout       | EC              | User            |
+-------------------+-----------------+-----------------+

**Site inputs:** 

+------------------------+-----------------------------------+
|                        |            Data source            |
|      Site   input      +-----------------+-----------------+
|                        | Integrated mode | Standalone mode |
+========================+=================+=================+
| Met-ocean timeseries   | SC              | User            |
| (Hs, Tp, Ws, Cs)       |                 |                 |
+------------------------+-----------------+-----------------+
| Site bathymetry        | SC              | User            |
+------------------------+-----------------+-----------------+
| Seabed characteristics | SC              | User            |
+------------------------+-----------------+-----------------+

**Energy transformation sub-system inputs:** 

+--------------------------------+-----------------------------------+
|                                |            Data source            |
| Energy   Transformation inputs +-----------------+-----------------+
|                                | Integrated mode | Standalone mode |
+================================+=================+=================+
| ET hierarchy                   | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_elect                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_mech                  | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Mass PTO_grid                  | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Total mass PTO                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs elect                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs mech                 | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO costs elect                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Total PTO costs                | ET              | User            |
+--------------------------------+-----------------+-----------------+
| Rated power                    | ET              | User            |
+--------------------------------+-----------------+-----------------+
| PTO failure rates              | ET              | User            |
+--------------------------------+-----------------+-----------------+

**Station keeping sub-system inputs:** 

+------------------------------+-----------------------------------+
|                              |            Data source            |
|   Station   Keeping inputs   +-----------------+-----------------+
|                              | Integrated mode | Standalone mode |
+==============================+=================+=================+
| SK hierarchy file            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor types                 | SK              | User            |
+------------------------------+-----------------+-----------------+
| Number of anchors per device | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor height                | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor width                 | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor length                | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor mass                  | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor soil type             | SK              | User            |
+------------------------------+-----------------+-----------------+
| Anchor cost                  | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring length               | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring    mass              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring diameter             | SK              | User            |
+------------------------------+-----------------+-----------------+
| Mooring    line cost         | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation type              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation height            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation diameter          | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation length            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation mass              | SK              | User            |
+------------------------------+-----------------+-----------------+
| Foundation burial            | SK              | User            |
+------------------------------+-----------------+-----------------+
| Component failure rates      | SK              | User            |
+------------------------------+-----------------+-----------------+

**Energy delivery sub-system inputs:** 

+---------------------------------+-----------------------------------+
|                                 |            Data source            |
|     Energy   Delivery inputs    +-----------------+-----------------+
|                                 | Integrated mode | Standalone mode |
+=================================+=================+=================+
| ED Hierarchy                    | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point catalogue ID   | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point location       | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point type           | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Collection Point costs          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Cable ID                        | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Cable route                     | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable length                    | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable burial depth              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable soil type                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| cable type                      | ED              | User            |
+---------------------------------+-----------------+-----------------+
| route splitpipe                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| route cable protection mattress | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector position              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector type                  | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector cost                  | ED              | User            |
+---------------------------------+-----------------+-----------------+
| connector catalogue ID          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical position              | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical costs                 | ED              | User            |
+---------------------------------+-----------------+-----------------+
| umbilical catalogue ID          | ED              | User            |
+---------------------------------+-----------------+-----------------+
| Component failure rates         | ED              | User            |
+---------------------------------+-----------------+-----------------+

**Selection of the project phase to analyse:** 

+---------------------------------+-----------------------------------+
|                                 |            Data source            |
|   Project phases to consider    +-----------------+-----------------+
|                                 | Integrated mode | Standalone mode |
+=================================+=================+=================+
| Installation                    | User            | User            |
+---------------------------------+-----------------+-----------------+
| Maintenance                     | User            | User            |
+---------------------------------+-----------------+-----------------+
| Decommissioning                 | User            | User            |
+---------------------------------+-----------------+-----------------+


To **view, update or delete** the Project inputs, click ``View/Update/Delete`` 
under the appropriate input type.

- To update values, only the parameters that need to be modified should be entered. 
  Click ``Update`` after making the updates to save these.

- To delete the inputs click ``Delete``, which will produce a pop-up window asking for confirmation. 
  Click ``Delete`` again to permenantly delete these inputs from the database.

2. Enter operation inputs
---------------------------

At low complexity, the LMO module does not require additional inputs in the Operations tab.
However, for each of the desire lifecycle phases, the operations must be generated.
Depending on the lifecycle phases selected in the Project tab, the user will be asked 
to generate the operations for each lifecycle phase by pressing the ``Generate`` button.

- To generate the installation operations: ``Compute``

- To generate the maintenance operations: ``Generate``

- To generate the decommissioning operations: ``Generate``

.. tip::
  If only one lifecycle phase (e.g. installation) has been selected in the Project
  tab, then only one button (e.g. "Generate installation operations") will be 
  shown to the user.

Once the operations have been generated, the user may specify operation methods
and infrastructure requirements.

**Operation methods** 

+----------------------------------------+-----------------------------------+
|                                        |            Data source            |
|        Operation   method input        +-----------------+-----------------+
|                                        | Integrated mode | Standalone mode |
+========================================+=================+=================+
| Device transportation method           | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Device load-out method                 | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Pile transportation method             | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Pile load-out method                   | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Anchors load-out method                | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Collection point transportation method | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Collection point  load-out method      | User            | User            |
+----------------------------------------+-----------------+-----------------+
| Cable burial method                    | ED              | User            |
+----------------------------------------+-----------------+-----------------+
| Cable landfall method                  | User            | User            |
+----------------------------------------+-----------------+-----------------+
..| Installation sequence (1,2,3,4,5)      | User            | User            |
..+----------------------------------------+-----------------+-----------------+

**Infrastructure preferences:** 

.. note::
  The infrastructure preferences are optional inputs which are independently specified
  for each of the project lifecycle phases being considered.

+-------------------------------------------------------------------------+-----------------------------------+
|                                                                         |            Data source            |
|                    Infrastructure   preferences input                   +-----------------+-----------------+
|                                                                         | Integrated mode | Standalone mode |
+-------------------------------------------------------------------------+-----------------+-----------------+
| Filter port terminals without past experience in marine energy (bool)   | User            | User            |
+-------------------------------------------------------------------------+-----------------+-----------------+
| Filter port terminals without sufficient terminal area (bool)           | User            | User            |
+-------------------------------------------------------------------------+-----------------+-----------------+
| Filter port terminals without sufficient crane lift capabilities (bool) | User            | User            |
+-------------------------------------------------------------------------+-----------------+-----------------+
| Filter port terminals without sufficient quay load bearing (bool)       | User            | User            |
+-------------------------------------------------------------------------+-----------------+-----------------+
| Filter port terminals not within a defined distance to site             | User            | User            |
+-------------------------------------------------------------------------+-----------------+-----------------+

3. Run computations 
-------------------

.. warning::
  Computation time in the high complexity level may take several hours, depending on 
  the size/number of years of the met-ocean timeseries. For a   20-years timeseries,
  depending on the CPU, it may take up to 5 - 10 hours per project lifecycle phase.

In the Calculations tab, the user may choose to compute the results for the
previously selected lifecycle phases.

Once the calculations have been completed, an alert window will inform the user.
The user may then choose to view the results in the Results tab.

.. note::
  For each of the selected lifecycle phases, once the computation has been run,
  the user is allowed to delete the lifecycle phase (e.g. Installation) results,
  by pressing the ``Delete [phase] results``.

4. View results 
-----------------

In the Results tab, the user may click ``view results`` to see a summary of the
logistic design for each project lifecycle phase.

The following output parameters are presented in the tabular form for
for the simplified logistic design at low complexity.

-  **Name:** Operation name

- **Total duration:** Duration of the operation, in hours.

- **Vessel costs:** Total vessel costs, in €.

- **Terminal costs:** Total port terminal costs, in €.

- **Equipment costs:** Total equipment costs, in €.

- **Total costs:** Total operation costs, in €.

- **Terminal:** id of the selected port terminals 

- **Vessels:** ids of the selected vessels 

- **Equipment:** ids of the selected equipment 

A gantt chart is also produced, featuring:

-  **Operation name:** Operation name

-  **Start date:** Start date of the operation

-  **End date:** End date of the operation

-  **Duration:** Operation duration, in days.

-  **Waiting time:** Waiting time is represented graphically (in dark blue) on the gant chart bars. 

For the maintenance results, the gantt chart also includes:

-  **Failure date:** Calendar date in which component failure occured.

Optionally, the user is allowed to review the more detailed information 
provided on each of the logistics design solutions. The design data for each 
logistic design solutions can be downloaded in json format.
