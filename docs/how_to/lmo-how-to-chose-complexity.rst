.. _lmo-how-to-complexity:

How to decide between complexity levels in Logistics and Marine Operations
==========================================================

.. warning::
    This content is mostly from D5.7 Logistics and Marine Operations alpha version,
    and still to be reviewed and updated

The functionality of the Logistics and Marine Operations module is different for early
versus later stage technology development, as a consequence of data
availability. 
To ensure consistency with the other tools, three levels of complexity (CPX1, 
CPX2 and CPX3) have been developed for the Logistics module.  However there is no
exact boundary between the two later stages. The LMO module will thus 
have two design modes, a low complexity (CPX1) and a full complexity (CPX2-3).s
 
In the full complexity mode, the main differences between complexity CPX2 and
CPX3 are the certainty of the inputs and whether default values are assumed in
the intermediate stage instead of requesting these from the user. The full complexity
process is discussed in :ref:`functionalities-simplified-design-process`.

Alternatively, the simplified mode (CPX1) can be used for early-stage 
technologies, at lower Technology Readiness Levels (TRL) 1-3, or whenever 
limited information is available about the technology design and project specifics.
The simplified mode may also be used to provide a quick and rough estimate for
higher TRL projects. The process is discussed in :ref:`functionalities-simplified-design-process`

Example Differences between complexity levels 2 and 3 for the LMO module
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

In the table below, the main differences in respect to the inputs and outputs of the
LMO module for the different complexity levels are presented.

+---------------------------------------------------------------+-----------------------------------------+---------------------------------------------------------------------+
|                                                               |            Simplified process           |            Full complexity calculation process                      |
+                          Parameter                            +-----------------------------------------+-------------------------------------+-------------------------------+
|                                                               |               CPX1: Simple              |             CPX2: Medium            |          CPX3: Complex        |
+===============================================================+=========================================+=====================================+===============================+
| Transportation   method                                       | Assumed dry                             | Assumed dry                         | User input (default dry).     |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Cable burial method                                           | Ignored (Surface lay)                   | Consider                            | Consider                      |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Cable Landfall method                                         | Assumed OCT                             | Assumed OCT                         | User input                    |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Consider post-lay cable burial                                | Ignored                                 | FALSE                               | User input                    |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Load-out method                                               | Ignored                                 | Assumed “lift”                      | User input                    |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Load-out from vessel                                          | Lift                                    | Lift                                | User input (default lift)     |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Piling method                                                 | Assumed hammering. Soil type ignored.   | Calculated                          | User preferences              |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| External protections                                          | Ignored                                 | User/ED input                       | User/ED input                 |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Met-ocean timeseries                                          | Reduced Hs timeseries                   | Full timeseries (Hs,Tp,Ws,Cs)       | Full timeseries (Hs,Tp,Ws,Cs) |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Site bathymetry                                               | Average value                           | Detailed bathymetry                 | Detailed bathymetry           |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Vessel route                                                  | Assumed straight line from port to site | Calculated                          | Calculated                    |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Start date                                                    | Ask month to user                       | Ask specific date to user           | Ask specific date to user     |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Export cable route                                            | Based on cable length                   | Detailed input from ED              | Detailed input from ED        |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Installation operation sequence                               | Assumed                                 | Default/User input                  | Default/User input            |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Topside inspections requirement                               | Assumed True                            | User specified                      | User specified                |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+
| Bollard pull calculation for vessel selection during wet-tows | Ignored                                 | Calculated                          | Calculated                    |
+---------------------------------------------------------------+-----------------------------------------+-------------------------------------+-------------------------------+

