.. _lmo-explanation:

*********************
Background and theory
*********************
.. warning::
    This section is still incomplete

This section describes the background and theory behind the module. 
Describe the calculation methods and assumptions in this section. 
Talk about why certain decisions were made. 
It is expected that a lot of this section can be copied from the alpha version deliverables. 

The sub-headings below are just examples. 
Each module can organise this section as appropriate. 

Background
==========

This is some background theory to XXXXX. 

.. toctree::
    :maxdepth: 1

    lmo-functionalities-simplified
    lmo-functionalities-full
    lmo-exp-installation

Calculation methods
===================

Detail any calculation methods here


Assumptions
===========

List any assumptions here


References
===========

.. [Thomsen14]	K. E. Thomsen, 
    Offshore wind: a comprehensive guide to successful offshore wind farm installation,
    2. ed. Amsterdam: Elsevier/AP, Academic Press, 2014.

.. [OREDA02] OREDA, ‘OREDA - Offshore Reliability Data Handbook’. 2002

.. [Smith17] G. Smith and G. Lamont, 
    ‘Decommissioning of Offshore Wind Installations - What we can Learn’,
    Offshore Wind Energy, p. 11, 2017.


.. [Statoil14]	Statoil, ‘Decommissioning Programme Sheringham Shoal - SCIRA Offshore Energy’, SC-00-NH-F15-00005, Mar. 2014.

.. [Tropham17]	E. Topham and D. McMillan, ‘Sustainable decommissioning of an offshore wind farm’, Renewable Energy, vol. 102, pp. 470–480, Mar. 2017, doi: 10.1016/j.renene.2016.10.066.

.. [Topper19]	M. B. R. Topper et al., ‘Reducing variability in the cost of energy of ocean energy arrays’, Renewable and Sustainable Energy Reviews, vol. 112, pp. 263–279, Sep. 2019, doi: 10.1016/j.rser.2019.05.032.

.. [Nicolas16]	T. Nicolas and M. Dominique, ‘Optimization of maintenance strategy of renewable energy production system (REPS) for minimizing production loss’, Int J Interact Des Manuf, vol. 10, no. 3, pp. 229–234, Aug. 2016, doi: 10.1007/s12008-016-0331-6.

.. [IMO94]	IMO - International Maritime Organization, Guidelines for vessels with dynamic positioning systems. London, 1994.

.. [DNV0SH203]	Det Norske Veritas (DNV), DNV-OS-H203: Transit and Positioning of Offshore Units. 2012

.. [Lundh16]	M. Lundh, W. Garcia-Gabin, K. Tervo, and R. Lindkvist, ‘Estimation and Optimization of Vessel Fuel Consumption’, IFAC-PapersOnLine, vol. 49, no. 23, pp. 394–399, Jan. 2016, doi: 10.1016/j.ifacol.2016.10.436.

.. [Wells11]	N. Wells and M. McConnell,
    ‘Assessment of the Irish Ports & Shipping Requirements for the Marine
    Renewable Energy Industry’, 2011.

.. [Teillant14]	Boris Teillant et al., ‘DTOcean Deliverable D5.2 - Characterization
    of logistic requirements’, Public Deliverable, 2014.

.. [Tacx19]	Jochem Tacx, Building an Offshore Wind Farm - Operational Master Guide. 2019.

.. [Gray17] A. Gray, B. Dickens, T. Bruce, I. Ashton, and L. Johanning, ‘Reliability and O&M sensitivity analysis as a consequence of site specific characteristics for wave energy converters’, Ocean Engineering, vol. 141, pp. 493–511, Sep. 2017, doi: 10.1016/j.oceaneng.2017.06.043.

.. [DNV-OS-H202]	Det Norske Veritas (DNV), DNV-OS-H202 Sea transport operations (VMO Standard - Part 2-2). 2015.

.. [DNVV-RP-H103]	Det Norske Veritas (DNV), DNV - RP-H103 Modelling and Analysis of Marine Operations, Recommended Practices. 2011.

.. [DNVGL-RP-C205]	Det Norske Veritas (DNV), DNVGL-RP-C205 Recommended practice - Environmental Conditions and Environmental Loads. 2017.

.. [BV87]	Bureau Veritas (BV), ‘Towage at sea of vessels and floating units’,
    Paris, France, 1987.

.. [Dalgic13]	Y. Dalgic, I. Lazakis, and O. Turan,
    ‘Vessel charter rate estimation for offshore wind O&M activities’,
    in Developments in Maritime Transportation and Exploitation of Sea Resources,
    C. Soares and F. Peña, Eds. CRC Press, 2013, pp. 899–907.

.. [Walker13]	R. T. Walker, J. Van Nieuwkoop-Mccall, L. Johanning, and R. J. Parkinson,
    ‘Calculating weather windows: Application to transit, installation and the
    implications on deployment success’, Ocean Engineering, vol. 68, pp. 88–101,
    2013, doi: 10.1016/j.oceaneng.2013.04.015.

.. [Bowers94]	J. A. Bowers and G. I. Mould, ‘Weather risk in offshore projects’,
    Journal of the Operational Research Society, vol. 45, no. 4, pp. 409–418, 1994,
    doi: 10.1057/jors.1994.59.

.. [Ochi98]	M. K. Ochi, Ocean Waves - The Stochastic Approach. Cambridge:
    Cambridge University Press, 1998.

.. [Goda00]	Y. Goda, Random Seas and Design of Maritime Structures.
    Singapore: World Scientific Publishing, 2000.

.. [Holthuijsen07] L. H. Holthuijsen, Waves in Oceanic and Coastal Waters.
    Cambridge: Cambridge University Press, 2007.

.. [Yang18]	X. Yang and Q. Zhang, ‘Joint probability distribution of winds and
    waves from wave simulation of 20 years ( 1989-2008 ) in Bohai Bay’,
    Water Science and Engineering, vol. 6, no. 3, pp. 296–307, 2018, doi: 10.3882/j.issn.1674-2370.2013.03.006.

.. [Teillant142]	Boris Teillant et al., ‘DTOcean Deliverable D5.6 - Report on
    logistical model for ocean energy and  considerations’, Public Deliverable, 2014.

.. [BVG2015]	BVG Associates, ‘Offshore Wind Market Overview –
    Gaps and opportunities for indigenous supply’, presented at the
    Opportunities in Offshore Renewables for Scottish SMEs
    (Oil & Gas) ~ ETP Workshop. - ppt download, Jan. 26, 2015,
    Accessed: May 05, 2020. [Online]. Available: https://slideplayer.com/slide/11699104/.

