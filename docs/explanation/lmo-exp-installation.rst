.. _lmo-exp-installation:

************************************
Installation of ocean energy devices
************************************

.. contents::


Components and installation operations
======================================



The installation of all subsystems and components related to a given
offshore renewable energy farm is typically grouped into large
operations according to component types, transport methodology, vessel
requirements and installation sequence.

Within the context of DTOceanPlus, eight different installation
operations were considered for installing all the envisaged component
types, as shown in the table below.

+----------------------------------+----------------------------------+
| **Name of installation           | **Component types to be          |
| operation**                      | installed**                      |
+==================================+==================================+
|   Foundation installation        | -  Gravity-based anchors         |
|                                  |                                  |
|                                  | -  Pile anchors and foundations  |
|                                  |                                  |
|                                  | -  Suction caissons (anchors and |
|                                  |    foundations)                  |
+----------------------------------+----------------------------------+
|  Mooring installation            | -  Mooring lines                 |
|                                  |                                  |
|                                  | -  All anchors except pile and   |
|                                  |    suction anchors.              |
+----------------------------------+----------------------------------+
|  Support structure installation  | -  Jacket                        |
|                                  |                                  |
|                                  | -  Gravity-based structures      |
|                                  |                                  |
|                                  | -  Tripod                        |
+----------------------------------+----------------------------------+
| Collection point installation    | -  Subsea hub                    |
|                                  |                                  |
|                                  | -  Surface-piercing offshore     |
|                                  |    substations                   |
|                                  |                                  |
|                                  | -  Floating offshore substations |
+----------------------------------+----------------------------------+
|  Device installation             | -  Floating WEC                  |
|                                  |                                  |
|                                  | -  Bottom-fixed WEC              |
|                                  |                                  |
|                                  | -  Floating TEC                  |
|                                  |                                  |
|                                  | -  Bottom-fixed TEC              |
+----------------------------------+----------------------------------+
|  Export cable installation       | -  Includes cables and           |
|                                  |    connectors                    |
|                                  |                                  |
|                                  | -  Static and dynamic            |
|                                  |    cables/segments.              |
|                                  |                                  |
|                                  | -  May include split pipes if    |
|                                  |    needed                        |
+----------------------------------+----------------------------------+
|  Array cable installation        | -  Includes cables and           |
|                                  |    connectors                    |
|                                  |                                  |
|                                  | -  Static and dynamic            |
|                                  |    cables/segments.              |
|                                  |                                  |
|                                  | -  May include split pipes if    |
|                                  |    needed                        |
+----------------------------------+----------------------------------+
|  External protections            | -  Concrete mattress             |
|  installation                    |                                  |
|                                  | -  Rock bags                     |
|                                  |                                  |
|                                  | -  Rocks                         |
+----------------------------------+----------------------------------+

In DTOceanPlus, it is assumed that each installation operation is
singular and thus responsible for installing all components associated
with the operation in question. In other words, for a given floating
wave energy farm with floating collection points, it is assumed there
will only be one *“Mooring installation”* operation, which will only end
after having installed every single mooring line of the farm.
Multi-phase installation projects are thus, not considered within the
scope of DTOceanPlus. The project commissioning date is assumed to be
the end date of the last installation operation.

Sequence of operations
======================================

For an offshore renewable energy project, the exact sequencing of the
installation operations is project specific, many times with multiple
operations being carried out in parallel. Still, there are physical
constraints that impose that some systems are installed before others.
One example is the case of foundations which are necessarily installed
before the device and frequently before everything else. When designing
the sequence of installation operations, the following motivations come
into play:

1) Cable connections using dry-mate connectors must be performed in a
   dry-environment, on deck of the installation vessel.

2) Dynamic cables (or cables with dynamic segments) should not be
   installed without performing the connection. This means that in
   theory, dynamic cables should be installed after the floating
   elements (device or collection point) they are connected to.

3) Devices and collection points installed on the seabed should be
   lowered just once during installation.

4) Surface piercing fixed elements (e.g. fixed offshore wind), are
   commonly installed before cables for asset safety reasons.

In DTOceanPlus, flexibility related to specifying operation sequences
was provided to the user. Building on the reasons stated above, the LMO
module proposes operation precedence rules for different device and
collection point topologies, and cable connectors. In Table 2.2, four
typical operation sequences are presented (Seq 1, Seq 2, Seq 3 and Seq
4), and then expanded in Table 2.3. However, the user may change the
operation sequence, and in case any violation to the stated rules
occurs, a warning will be presented.

Operation Precedence rules for installing offshore renewable energy farms

+---------------+-------------+-------------+---------------------+---------------+
|                             |     **Device: Bottom-fixed**      | **Floating**  |             
+                             +-------------+---------------------+---------------+
|                             |**Dry-mate** | **Wet-mate**        |  **Wet-mate   |
|                             |             | (or I-tube/J-tube)**|               |
+===============+=============+=============+=====================+===============+
|**Collection** |**Wet-mate** | Seq 1       | Seq 2               | Seq 2         |
|**point:**     |**/Dry-mate**|             |                     |               |
|**Surface**    |             |             |                     |               |
|**Piercing**   |             |             |                     |               |
+---------------+-------------+-------------+---------------------+---------------+
|**Collection** |**Dry-mate** | Seq 3       | Seq 4               | Seq 4         |                    
|**point:**     |             |             |                     |               |
|**Seabed**     +-------------+-------------+---------------------+---------------+
|               | **Wet-mate  | Seq 1       | Seq 2               | Seq 2         |
|               | (or I-tube/ |             |                     |               |
|               | J‑tube)**   |             |                     |               |
+---------------+-------------+-------------+---------------------+---------------+

Installation Operation sequence considered

+----------------+----------------+----------------+----------------+
| **Seq 1**      | **Seq 2**      | **Seq 3**      | **Seq 4**      |
+================+================+================+================+
| Foundation     | Foundation     | Foundation     | Foundation     |
| installation   | installation   | installation   | installation   |
|                |                |                |                |
|      ↓         |      ↓         |      ↓         |       ↓        |
|                |                |                |                |
| Moorings       | Moorings       | Moorings       | Moorings       |
| installation   | installation   | installation   | installation   |
|                |                |                |                |
|      ↓         |      ↓         |      ↓         |       ↓        |
|                |                |                |                |
| Support        | Support        | Support        | Support        |
| structures     | structures     | structures     | structures     |
| installation   | installation   | installation   | installation   |
|                |                |                |                |
|      ↓         |      ↓         |      ↓         |       ↓        |
|                |                |                |                |
| Collection     | Collection     | Export cable   | Device         |
| point          | point          | installation   | installation   |
| installation   | installation   |                |                |
|                |                |      ↓         |       ↓        |
|      ↓         |      ↓         |                |                |
|                |                | Array cable    | Export cable   |
| Export cable   | Device         | installation   | installation   |
| installation   | installation   |                |                |
|                |                |      ↓         |       ↓        |
|      ↓         |      ↓         |                |                |
|                |                | Post-lay cable | Array cable    |
| Array cable    | Export cable   | burial         | installation   |
| installation   | installation   |                |                |
|                |                |      ↓         |       ↓        |
|      ↓         |      ↓         |                |                |
|                |                | External       | Post-lay cable |
| Post-lay cable | Array cable    | protections    | burial         |
| burial         | installation   |                |                |
|                |                |      ↓         |       ↓        |
|      ↓         |      ↓         |                |                |
|                |                | Collection     | External       |
| External       | Post-lay cable | point          | protections    |
| protections    | burial         | installation   |                |
|                |                |      ↓         |       ↓        |
|      ↓         |      ↓         |                |                |
|                |                |                | Collection     |
| Device         | External       | Device         | point          |
| installation   | protections    | installation   | installation   |
+----------------+----------------+----------------+----------------+

