.. _lmo-functionalities-full-design-evaluation-process:

Full complexity design and evaluation process
==============================================

.. warning::
    This content is from D5.6 Energy Delivery alpha version,
    and still to be reviewed and updated

.. contents::


A high-level process flow diagram of the full complexity design and
evaluate functionalities of the ED module is shown in Figure 3.8. For
each of the network configurations selected by the user, the energy
delivery system is designed, and components selected. This starts with
the design of the transmission system to the shore and then proceeds to
the array network. Multiple network design options are produced by the
design functionalities of the module, which are then evaluated using a
techno-economic analysis, as discussed below. After being evaluated, the
user then selects the ‘best’ network option to be taken forward to the
other modules for the remainder of the analysis. Some of the
functionalities of the ED module depends on the network topology being
considered.

.. figure:: ../figures/ED-Process-Full-Overall.svg

    Overall process flow of the design and evaluate functionalities at CPX2 and CPX3





Bathymetry pre-processing
^^^^^^^^^^^^^^^^^^^^^^^^^

The site characteristics data, which include the lease area and the
cable corridor bathymetry, need to be pre-processed for the cable
routing optimisation routines used in the ED module. The pre-processing
of the bathymetry data involves the following tasks:

-  Merging the lease area and export corridor bathymetry data,

-  Calculating the gradient and distance between all neighbouring points
   in the bathymetry data,

-  Creating a NetworkX graph object for cable routing analysis,

-  Removing points that lie within exclusion zones and lines that breach
   the gradient constraint specified as an input.






Cable installation and protection technique selection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Possible cable installation and protection techniques are briefly
described in :ref:`cable-installation-protection`
with those considered in DTOceanPlus being
jetting, ploughing, cutting, dredging and surface lay.

The cable installation and protection techniques that will be used on a
project are largely defined by the seabed type, and within the ED module
can either be user defined or can be selected by the module. Cable
protection in offshore projects is an important aspect, as cable damage
can be expensive, and may have a significant impact on the profitability
of the project [OWI17]_.

Cable burial is the most common protection method used in the offshore
wind industry. All of the installation techniques included in the ED
module with the exception of surface lay aim to do this in a range of
different seabed types. Of the techniques being considered, the optimal
technique is selected based on the cable length and installation rates
(in metres/hour) in different soil types These are shown in Table 3.14,
colour coded by tool suitability. This table exists as a catalogue that
the ED module uses. The table entries of zero denotes incompatible
installation technique and seabed type combinations.


**Cable installation technique installation rates, colour coded by tool suitability**

+-----------------+----------------------+-------------------------------------------------------+
|                 |                      |                                                       |
|                 |                      | Installation rate [m/hr]                              |
+                 +                      +-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
| Soil group      | Seabed type          | Jetting     | Ploughing   | Cutting     | Dredging    |
+=================+======================+=============+=============+=============+=============+
|                 |                      |             |             |             |             |
| Cohesionless    | Loose sand           | 250         | 100         | 0           | 150         |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Medium sand          | 200         | 350         | 275         | 87.5        |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Dense sand           | 0           | 100         | 275         | 75          |
+-----------------+----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
| Cohesive        | Very soft clay       | 475         | 0           | 0           | 150         |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Soft clay            | 475         | 375         | 275         | 100         |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Firm clay            | 250         | 500         | 275         | 75          |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Stiff clay           | 0           | 550         | 75          | 50          |
+-----------------+----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
| Other           | Hard glacial till    | 0           | 300         | 75          | 50          |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Cemented             | 0           | 0           | 75          | 50          |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Soft rock coral      | 0           | 0           | 50          | 50          |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Hard rock            | 0           | 0           | 0           | 0           |
|                 +----------------------+-------------+-------------+-------------+-------------+
|                 |                      |             |             |             |             |
|                 | Gravel cobble        | 0           | 350         | 0           | 75          |
+-----------------+----------------------+-------------+-------------+-------------+-------------+
|                                                                                                |
|                                                                                                |
+-----------------+-----------------+-----------------+------------------------+-----------------+
|                 |                 |                 |                        |                 |
| Key:            | Untrenchable    | Not suitable    | Suitable but not ideal | Suitable        |
+-----------------+-----------------+-----------------+------------------------+-----------------+

The target cable burial depth in the array and the transmission networks
are user inputs in the module. If these values are not provided, then
default values for the burial depth based on the seabed type are
selected. For all sandy seabed types (including loose, medium and dense
sand), the burial depth target is 0.5 m, for clay seabed types
(including very soft, soft, firm and stiff clay ), it is 1.0 m, and for
all the other seabed types (including hard glacial till, cemented, soft
rock coral, hard rock and gravel cobble) the cable is surface laid.

Many of the early test facilities and array deployments have used
surface lay as the option for cables, which is also an installation
technique included in the module. Cables that are surface laid are
susceptible to a number of risks and need to be protected. Surface laid
cables are often protected using split pipes, concrete mattresses, or
rock dumping, as illustrated in Figure 2.11. Within the ED module,
whether or not to use any protection for surface laid cables and the
protection option to be used is a user input. The design and costs of
the protection method used and its equipment and installation costs are
not included in ED module. The LMO tool includes the installation costs
to the overall solution.

As a first step, in identifying the cable installation approach, all the
seabed types present in the lease and cable corridor areas are
identified during the bathymetry pre-processing. All installation
techniques that are compatible with the soil types present in the
bathymetry files are initially considered. For each of these cable
installation techniques, areas in the lease and cable corridor areas
compatible with the installation equipment are found. Separate NetworkX
graph objects are then created for each cable installation technique
individually.

In the use case where the cable installation technique is not specified
by the user, the selected cable installation techniques are sorted in
the descending order of installation rates and iteratively assessed,
starting with the one with the highest installation rate. If an ED
network design solution is obtained for a technique, the iterative
search is stopped, and the cable installation technique used is saved as
an output.

If the cable installation technique is user defined, the NetworkX graph
of the particular installation tool is used to create network designs.
In case this fails (e.g. if the chosen installation technique is not
suitable for the seabed), the best tool based on installation rates is
chosen and the user is informed of this.





Design of transmission system 
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. figure:: ../figures/ED-Process-Full-Transmission1.svg
    :align: right

    High-level design process for transmission system

The network design process starts with the design of the transmission
system, which is often the most expensive part of the entire array’s
electrical infrastructure. Other than for very early array deployments,
in which direct cables from every device to the shore are used, larger
arrays will likely have one or more collection points. The collection
point and the transmission system, as defined in Figure 2.1, are often
designed together because they are interrelated.

For direct to shore connections, the transmission system is designed
using the array design algorithm, but is referred to as a transmission
system rather than an array network.

The transmission system design process is shown in Figure 3.9. This
involves determining the transmission/export voltage required, which in
turn determines whether some form of voltage transformation is required
on the collection points. If collection points are required, their
locations and the route of the export cable from the collection point to
the onshore landing point need to be optimised.




Selection of export voltage
'''''''''''''''''''''''''''

The first step of the transmission system design involves determining
the transmission/export voltage to be used for the device/array in
question. Technical and operational requirements place limits on the
transmission of electrical energy for a given generation voltage, power
magnitude and transmission distance. The export voltage required depends
on the size of the device/array and the distance of the device/array to
the onshore landing point. Determining the export voltage to be used is
primarily based on the estimated maximum power and distance capability
of a range of different voltage levels, available in the module as a
lookup table, shown in :ref:`Electrical component catalogue input data`

The algorithm, presented below 
3.10, then determines suitable voltage levels for analysis and, if
necessary, proposes the use of step-up transformers on the collection
point. The lowest voltage suitable for the device/array power and
distance to the shore, starting from the device voltage, and the next
higher voltage up are selected to be further assessed by the module.
Choosing the next higher voltage up is important to examine if the
savings due to the lower energy losses at the higher voltage is higher
than the increase in equipment cost.

There are six network topologies included in the ED module and the
selection of the export voltage occurs slightly differently for those
that do not have a transmission collection point. In many planned and
deployed small arrays, one of the devices in the array can also play the
role of a collection point. In this case, the possibility of using the
device voltage to transmit the power is assessed. If the device voltage
is found to be insufficient, an error message informs the user of this
constraint. This is the case with the direct connection to shore
topology as well, wherein every device in a small array has a dedicated
cable to the shore.

The export voltage can be user defined and the same check is applied. 
The next voltage up is not selected for assessment in that case.

.. figure:: ../figures/ED-Process-Full-Transmission2.svg
    :align: center

    Transmission voltage selection algorithm


Design of collection point(s)
'''''''''''''''''''''''''''''

With larger deployments of devices further away from the shore, there
often is a need for one or more collections points (CPs) in the array.
The type of CP required also depends on the device voltage and on the
export voltage, as determined by the assessment described in the
preceding section. At a high level, CPs can be classified as:

1. **Transmission CPs** that connect the array to the OLP through the
   export cable.

2. **Array CPs**, to which OECs are directly connected, occur within
   star networks and collect the power generated by the clusters.

3. **Device CPs** are found in cases wherein a device in the array
   performs the role of a CP. The export cable to the shore originates
   from this CP. Device CPs are not referred to as transmission CP in
   this document.

4. **Virtual CPs** are used internally in the module for direct to shore
   networks. The onshore landing point acts as virtual CPs in such
   networks.

Transmission and array CPs can be further divided into the following
sub-types:

1. **Passive offshore collection hubs**, wherein no voltage
   transformation is required between the array network and transmission
   system. These are expected to be seabed mounted.

2. **Offshore substations**, wherein voltage transformation is required
   between the array network and the transmission system. These will be
   fixed surface-piercing.

The first part of the CP design involves specifying the location of the
collection point(s). Based on the network topology selected or being
assessed, a network design can either have one CP (in radial networks
and star networks with one cluster) or
multiple CPs (in star networks with more than one cluster). 

.. ref/link to figures?

The ED module only considers one transmission CP,
which is where the export cable originates from and carries the array’s
power to the shore. Any other CP(s) in the network topology, collecting
power generated by clusters of devices, will be called array CPs.
Network topologies that use one of the devices as their CP, do not have
either a transmission or an array CP, to help differentiate these 
topologies from others that have a substation or hub as the CP.

.. _k-means: 

The location of the transmission CP is defined in the module by a
*k-means* clustering algorithm, a widely used algorithm for portioning
data points into subsets [Wolfram20]_, [SciPy15]_. 
The layout of the devices in the
array and the location of the onshore landing point are the main inputs
to the function. In addition to these inputs, the grid of points, which
is the output from the bathymetry pre-processing stage and includes
exclusion zones, is also required. The *k*-means clustering algorithm
attempts to minimise the Euclidian distance between observations
(device/array CP location and OLP) and centroids (transmission CP
location). For network topologies with only a transmission CP, the
algorithm returns the centroid of the area defined by the device and OLP
coordinates.

The output of the clustering algorithm is only an interim solution and
is accepted as the final solution after further checks. The first check
involves determining whether the interim transmission CP location lies
within the lease area. If not, the transmission CP is moved from the
interim location to the edge of the lease area closest to the interim
location. The transmission CP location now becomes the intermediate
location. The next check involves examining the proximity of the
transmission CP to the devices. There is a threshold level set for this
proximity and if there are breaches the transmission CP location is
adjusted until no more threshold breaches are found. This now becomes
the final transmission CP location. The algorithm used, which has been
described here, is shown below.

For network topologies in which a device acts as a CP, the device
closest to the onshore landing point is selected as the CP.

.. figure:: ../figures/ED-Process-Full-Transmission3.svg
    :align: center

    Collection point locating algorithms for transmission CP and array CP



.. _dijkstra:

Routing of export cable
'''''''''''''''''''''''

The next part of the design of the transmission system is specifying the
export cable route between the transmission or device CP and the onshore
landing point. The cable routing algorithm routes the cables along the
seabed and evaluates the length of the cable. Exclusion zones and areas
with steep gradients are avoided. The cable routing algorithm was
implemented using a modified version of the Dijkstra’s algorithm, a
widely used algorithm for finding the shortest path between nodes in a
graph [NetworkX19]_. 
The algorithm has the capability of removing constrained
nodes, e.g. from exclusion zones or steep gradients, and works with
NetworkX objects created in the bathymetry pre-processing stage. 
The implementation requires the start and end points of the
cable along with information of any constraints to be applied. The
algorithm outputs the cable route, the length and the route path as a
list of coordinates. Figure below shows the export cable routing
algorithm implemented within the ED module.


.. figure:: ../figures/ED-Process-Full-Transmission2.svg
    :align: center

    Export cable routing algorithm



Design of array network
^^^^^^^^^^^^^^^^^^^^^^^

The design of the array network, which is the network that collects the
power from the devices/array CPs to the transmission CP, is the next
part of the energy delivery system design process.

At this stage in the sector development, there is no consensus on the
best practice approach for array network layouts. Transfer of knowledge
from the offshore wind sector is possible, but there are significant
differences. Power and voltage ratings of wave and tidal device
technologies are lower than of offshore wind turbines. The array layout
in OEC farms is driven primarily by device hydrodynamics, with lower
spacing between devices than those seen in wind farms. 
These will all have an overriding influence on the array network 
configuration. The network topologies considered in the ED module, 
discussed briefly in :ref:`electrical-networks`
were selected keeping in mind this difference. They cater to both 
commonly used network topologies in offshore wind and planned/installed
network topologies in wave and tidal arrays.

The array network design process depends on the network topology, as
shown in the figure below, and involves three main processes:

1. Selection of array voltages,

2. Clustering of the devices around CPs, and

3. Routing of array cables between devices and between array CPs.


.. figure:: ../figures/ED-Process-Full-Array.svg
    :align: center

    Array network design process



Selection of array voltages
'''''''''''''''''''''''''''

Any part of the array network directly connected to a device operates at
the device voltage. In the case of network topologies derived from the
generic radial topology, this means that the entire array network
operates at the device voltage. In network topologies derived from the
generic star topology (i.e. multi-cluster star networks), the array
network between the array CPs and the transmission CP can be at a
different voltage to that of the device, denoted by Array voltage 2 in
the figure above. This decision is based on the power rating of the
individual clusters and the distance/route between the array CPs and the
transmission CP. The data in the table below is used for this purpose and 
two voltage levels are chosen for evaluation: the minimum voltage 
required and the next one higher up.

**Estimated maximum power and distance capability of different voltage levels (design limits)**

============================== ============== ================
Transmission  voltage (kV)      |Smax| (MVA)   |Lmax| (km)
============================== ============== ================
0.4                            0.2–0.6            0.4–0.8
0.69                           0.4–1.0            0.6–1.3
3.3                            1.7–4.7            3.0–6.4
6.6                            3.4–9.4            5.9–12.8
10                             5.0–14             9–19
20                             10–28              18–38
30                             15–43              27–58
35                             18–50              31–68
66                             34–94              59–128
110                            57–157             99–213
============================== ============== ================

.. |Smax| replace:: S\ :sub:`max`
.. |Lmax| replace:: L\ :sub:`max`

.. note this table also used in simplified-design-process




Clustering of devices around collection point(s)
''''''''''''''''''''''''''''''''''''''''''''''''

The clustering requirements, and hence the algorithm used, for network
topologies based on radial and star layouts are different. In variants
of the generic radial topology there is only one CP, which is the
transmission CP (or a device CP/virtual CP), and all the devices in the
array need to be connected to it.

In variants of the generic star topology, there may be array CPs and
devices need to be clustered and connected to them. Array CPs having
from 2 to 24 devices connected to them are allowed, and all possible
combinations of cluster sizes are assessed to obtain the best performing
and the lowest cost solution. Designs made with clusters having only one
device connected to every array CP are considered uneconomically and are
ignored. The number of clusters to be considered for a particular array
with a certain number of devices are determined and assessed
iteratively.

For a specified number of clusters, the *k-means* algorithm, described
:ref:`above <k-means>`, is used to assign devices to array CPs. In
addition to the layout of the devices in the array, the algorithm also
requires the number of array CPs as an input and divides the array
devices into the number of clusters required. The locations of the array
CPs are also output. The clustering algorithm works to minimise the
Euclidian distance between the devices in a cluster and their
corresponding array CP.



Connection of devices and CPs 
'''''''''''''''''''''''''''''

Connections need to be made for one or more cases:

-  Devices to each other,

-  Devices to CP(s), and

-  Array CP(s) to transmission CP

To obtain the lowest cost array network solution, the objective is to
use the shortest total array cable length within the allowable
bathymetry points output from the pre-processing stage. This is achieved
by using a travelling salesperson vehicle routing algorithm developed
for cable routing in offshore wind farms [Bauer15]_. 
At this stage, the cables are not routed but the connections between 
device(s) and CP(s) are optimised.

In variants of the generic radial topology there is only one CP and all
the devices in the array need to be connected to it. The vehicle routing
algorithm starts with defining a matrix of distances between all devices
to each other and the CP. It then explores all possible topologies of
radials from having just 1 device per string to having all the devices
in a string and any combination in between. The maximum number of
devices in a string is an optional user input, represented by
*devices_per_string* in Table 3.12. If user defined, then radial
topologies having a maximum of *devices_per_string* per string are
considered. The next step involves using the vehicle routing algorithm
to connect all the devices in the string(s) together to the CP. The
algorithm does not allow crossing of array cables and the network
solutions with cable crossings are ignored. All feasible radial network
designs after this step are then evaluated. The maximum number of
devices on a string can also be a user input as a constraint to the
algorithm. Hence, iteratively, this process optimises the number of
devices in the strings and the number of strings in a radial network
with the objective of having the shortest array cable length.

In the case of network topologies derived from the star layout, the
devices assigned to a particular array CP are directly connected to it.
The vehicle routing algorithm is still used to connect the array CP(s)
to the transmission CP using the shortest total intra-collection point
cable length. Using this algorithm leads to hybrid network topologies
with star clusters connected to each other and/or to the transmission CP
either as radials or stars. The array CPs’ locations become points on
the route and the transmission CP becomes the target of the algorithm.



Routing of array cables
'''''''''''''''''''''''

Once the connections between the devices, between devices and the CP(s)
and between CPs are finalised, the next step involves the actual routing
of the cables on the seabed. The requirements of the cable routing
algorithm for array networks are identical to that of the export cable.
Therefore, the same cable routing algorithm based on the Dijkstra’s
algorithm, explained :ref:`above <dijkstra>`, is used here.






Design of umbilical cables for floating devices
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Umbilical cables are used with floating devices to transfer their power
generated to the subsea network. Umbilicals in floating wave and tidal
devices are installed in highly energetic environments and are subject
to dynamic and cyclical loading regimes. Double armouring is provided to
umbilicals for hydrodynamic stability during the installation and
operational phases, which is the main difference between them and static
cables. 
The design of umbilicals in the ED module includes both their
electrical and mechanical design and is as per DTOcean 1.0 and 2.0. The
algorithm used for their design is shown in the figure below.

.. figure:: ../figures/ED-Process-Full-Umbilical.svg
    :align: center

    Umbilical design algorithm

An umbilical is selected from the catalogue based on the voltage rating
of the floating device. The device location is taken to be the starting
point of the umbilical. The first step in the design process involves
setting the termination point of the umbilical on the seabed. The
termination point is set along the projection of the static cable route
between the device and its downstream device at approximately 1.5 times
the minimum water depth from the device. This termination point of the
umbilical is then used to reduce the virtual distance paths of the
static cable between the device and the downstream component.

The lazy wave configuration of umbilicals, shown below, is used
for its mechanical design, which requires an iterative approach to solve
for the cable forces at the main stress points. The approach used is
based on the methodology presented in [Bauer15]. 
This approach had been shown to be comparable with the commercially 
available finite-element software Orcaflex.


.. note:: 
    Figure of Umbilical lazy wave configuration to be added

    where: HOP is hang-off point, LP lift point, DP decline point, TDP touchdown point, BLP boundary-layer point

Umbilical design equations
''''''''''''''''''''''''''

The set of equations to be solved to calculate the mechanical loading of
an umbilical in the lazy wave configuration are included below. The
outputs of the mechanical design, that are used for further processing
by the ED module, are the umbilical cable length, and the x and z
coordinates along the entire length of the umbilical. The length of the
umbilical cable is used to evaluate its impedance from its impedance per
unit length and the impedance matrices used for the power flow analysis
are updated to reflect their inclusion.

The equations used for the mechanical design of the umbilical, 
based on the methodology presented in [Ruan14]_ are given below 
:eq:`eq1` to :eq:`eq8`.

.. math:: V_{i} = V_{i-1} + F_{\tau} ds \sin\theta_{i-1} - F_{n} ds \cos\theta_{i-1} - wds
    :label: eq1

.. math:: H_{i} = H_{i-1} + F_{n} ds \sin\theta_{i-1} - F_{\tau} ds \cos\theta_{i - 1}
    :label: eq2

.. math:: 
    :label: eq3

    T_{i} = \sqrt{V_{i}^{2} + H_{i}^{2}}        

.. math:: 
    :label: eq4

    \theta_{i} = \tan^{-1}\left( \frac{V_{i}}{H_{i}} \right)

.. math:: 
    :label: eq5

    x_{i} = x_{i-1} + ds \cos\theta_{i-1}    

.. math:: 
    :label: eq6

    y_{i} = y_{i-1} + ds \sin\theta_{i-1}    

.. math:: 
    :label: eq7

    M_{i} = - EI\frac{d\theta_{i}}{ds}   

.. math:: 
    :label: eq8

    S_{i} = - \frac{dM_{i} }{ds}        


where: :math:`T`, :math:`H`, :math:`V` represent the axial tension, horizontal and vertical force; 
:math:`F_n` and :math:`F_\tau` represent the drag force in the normaland tangential direction per unit length; 
:math:`\theta` is the inclination angle of the cable, 
:math:`w` is the submerged weight per unit length; 
:math:`ds` is the length of the section, 
:math:`EI` is the flexural stiffness; 
:math:`M` is the bending moment, 
:math:`S` is the shear force and :math:`x` and :math:`y` represent the local coordinate system of the touchdown point.

As the lazy wave cable configuration is divided into three segments,
continuity conditions are necessary at the boundary-layer point (BLP)
and the touchdown point (TDP). BLP continuity is calculated by :eq:`eq9` to :eq:`eq12`,
where :math:`D` is the cable outer diameter, with TDP continuity represented by :eq:`eq13` to :eq:`eq16`,.


.. math:: 
    :label: eq9

    y_{1}(L) = y_{BLP} - \left( W_{D} - D/2 \right)

.. math:: 
    :label: eq10

    y_{1}^{'}(L) = \tan\left( \theta_{BLP} \right)

.. math:: 
    :label: eq11

    -EIy_{1}^{''}(L) = M_{BLP} \left( T_{0},\theta_{BLP} \right)

.. math:: 
    :label: eq12

    EIy_{1}^{''}(L) = S_{BLP} \left( T_{0},\theta_{BLP} \right)


.. math::
    :label: eq13

     y_{1}\left( 0 \right) = y_{2}(0) = 0

.. math::   
    :label: eq14
     
    y_{1}^{'}(0) = y_{2}^{'}(0)

.. math::
    :label: eq15

    y_{1}^{''}(0) = y_{2}^{''}(0)

.. math::
    :label: eq16

    y_{1}^{'''} (0) = y_{2}^{'''} (0)

The conditions for passing the bending moment and shear force test are
given in :eq:`eq17` and :eq:`eq18` where: :math:`\varepsilon_{1}` and 
:math:`\varepsilon_{1}` are specified tolerances.

.. math:: 
    :label: eq17

    \left| W_{D} - D/2 - y_{BLP} + y_{1}(L) \right| < \varepsilon_{1}

.. math:: 
    :label: eq18

    \frac{\left| S_{BLP}^{+} - S_{BLP}^{-}\right|}{S_{BLP}^{+}} < \varepsilon_{2}


