FUNCTIONALITIES OF THE SIMPLIFIED LOGISTICS MODULE
--------------------------------------------------

In the lowest complexity level (CPX1), the LMO module produces a generic
logistic solution comprised of a selection of vessels, equipment and
ports, and a simplified operation plan which include durations and rough
estimates of waiting on weather contingencies. Some simplified
assumptions are adopted as follows:

-  Operation methods such as transportation, load-out at port, cable
   burial, and cable landfall methods are assumed.

-  The infrastructure selection is based on a pre-defined combination of
   vessels for the required operations.

-  Infrastructure solution matching is not carried out.

|image1|

Figure 3.10 Main functionalities of the Logistics module at simplified
complexity

Similarly to the full complexity version, the simplified version of the
LMO module keeps its four major functionalities, as shown in Figure
3.10, although with some simplifications:

1. **Operation pre-configuration**: For the low complexity level, the
   operation pre-configuration functionality identifies operation
   requirements and assumes pre-defined operation methods (e.g.
   transport: “dry”).

2. **Infrastructure pre-selection:** In the simplified version of the
   module, this functionality is common to all three phases of the
   project and consists of selecting a pre-defined combination of
   vessels, equipment and ports terminal that comply with operation
   assumptions, although compatibility between vessels, ports and
   equipment are not assessed.

3. **Operation computation:** For the lowest complexity level, this
   functionality, common to all three phases of the operation, is
   responsible for analysing the pre-selected infrastructure
   combinations and calculating expected operation durations and waiting
   on weather for different months of the year. Based on operation
   durations and selected infrastructure, the operation costs are
   calculated for the pre-selected infrastructure solution.

1. **Operation calendarization:** In the simplified version of the
   Logistics module, the operation calendarization operates exactly in
   the same way as in the full complexity level. For more information
   see Section 3.3.4.

OPERATION PRE-CONFIGURATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the simplified version of the LMO code, some realistic assumptions
are adopted to simplify the computation process and input requirements.

INPUTS
^^^^^^

Input table for the Operation pre-configuration functionality
at CPX1

+-------------+-------------+-------------+-------------+-----------+
| **Variable  | **Brief     | **Origin of | **Data      | **Units** |
| name**      | Description | the Data**  | Model in    |           |
|             | of the      |             | LMO**       |           |
|             | Input       |             |             |           |
|             | Quantity**  |             |             |           |
+=============+=============+=============+=============+===========+
| *hi         | Hierarchy   | ET          | Pandas      | [-]       |
| erarchy_et* | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | energy      |             |             |           |
|             | tra         |             |             |           |
|             | nsformation |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *hi         | Hierarchy   | ED          | Pandas      | [-]       |
| erarchy_ed* | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | energy      |             |             |           |
|             | delivery    |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *hi         | Hierarchy   | SK          | Pandas      | [-]       |
| erarchy_sk* | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | station     |             |             |           |
|             | keeping     |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *bom_et*    | Simplified  | ET          | Pandas      | [-]       |
|             | BOM         |             |             |           |
|             | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | energy      |             |             |           |
|             | tra         |             |             |           |
|             | nsformation |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *bom_ed*    | Simplified  | ED          | Pandas      | [-]       |
|             | BOM         |             |             |           |
|             | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | energy      |             |             |           |
|             | delivery    |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *bom_sk*    | Simplified  | SK          | Pandas      | [-]       |
|             | BOM         |             |             |           |
|             | datafile    |             |             |           |
|             | from the    |             |             |           |
|             | station     |             |             |           |
|             | keeping     |             |             |           |
|             | system      |             |             |           |
+-------------+-------------+-------------+-------------+-----------+
| *Hs_        | Met-ocean   | SC          | Pandas      | [-]       |
| timeseries* | timeseries  |             |             |           |
|             | of Hs       |             |             |           |
|             | m           |             |             |           |
|             | easurements |             |             |           |
|             | (default    |             |             |           |
|             | site)       |             |             |           |
+-------------+-------------+-------------+-------------+-----------+

OUTPUTS
^^^^^^^

-  OPERATION IDENTIFICATION AND SEQUENCE

Operation Pre-configuration outputs for installation,
maintenance and decommissioning operations at CPX1

+------------------------+-----------------+------------------------+
| **Requirement**        | **Inputs**      | **Function**           |
+========================+=================+========================+
| List of installation   | *Hierarchy_ET*  | *If ndevices>0:*       |
| operations             |                 |                        |
|                        | *Hierarchy_ED,* | *o                     |
|                        |                 | px_list.append(“device |
|                        | *Hierarchy_SK*  | installation”)*        |
|                        |                 |                        |
|                        | *Ndevices*      | *If hierarchy_ED       |
|                        |                 | includes “array        |
|                        | *cp.type*       | cable”:*               |
|                        |                 |                        |
|                        |                 | *                      |
|                        |                 | opx_list.append(“array |
|                        |                 | cable installation”)*  |
|                        |                 |                        |
|                        |                 | *If hierarchy_ED       |
|                        |                 | includes “export       |
|                        |                 | cable”:*               |
|                        |                 |                        |
|                        |                 | *o                     |
|                        |                 | px_list.append(“export |
|                        |                 | cable installation”)*  |
|                        |                 |                        |
|                        |                 | *If hierarchy_ED       |
|                        |                 | includes “collection   |
|                        |                 | point”:*               |
|                        |                 |                        |
|                        |                 | *If not all cp’s are   |
|                        |                 | “hub”:*                |
|                        |                 |                        |
|                        |                 | *opx_l                 |
|                        |                 | ist.append(“collection |
|                        |                 | point installation”)*  |
|                        |                 |                        |
|                        |                 | *if “moorings” in      |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *opx                   |
|                        |                 | _list.append(“moorings |
|                        |                 | installation”)*        |
|                        |                 |                        |
|                        |                 | *if “pile” or “suction |
|                        |                 | caisson” in            |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *opx_l                 |
|                        |                 | ist.append(“foundation |
|                        |                 | installation”)*        |
|                        |                 |                        |
|                        |                 | *if “support           |
|                        |                 | structure” in          |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *op                    |
|                        |                 | x_list.append(“support |
|                        |                 | structure              |
|                        |                 | installation”)*        |
+------------------------+-----------------+------------------------+
| Operation sequence     | *None*          | Seq1 = [“Foundation    |
| suggestion [3]_        |                 | installation”,”        |
|                        |                 | Moorings               |
|                        |                 | installation”,         |
|                        |                 | “Support structures    |
|                        |                 | ins                    |
|                        |                 | tallation”,“Collection |
|                        |                 | point installation”,   |
|                        |                 | Device installation”,  |
|                        |                 | “Export cable          |
|                        |                 | installation”, “Array  |
|                        |                 | cable installation”,   |
|                        |                 | “Post-lay cable        |
|                        |                 | burial”, “External     |
|                        |                 | protections”]          |
+------------------------+-----------------+------------------------+
| List of preventive     | Same as CPX2-3  | *Same as CPX2-3.*      |
| maintenance operations |                 |                        |
+------------------------+-----------------+------------------------+
| List of corrective     | Same as CPX2-3  | *Same as CPX2-3*       |
| maintenance operations |                 |                        |
+------------------------+-----------------+------------------------+
| List of                | Same as CPX2-3  | *Same as CPX2-3*       |
| decommissioning        |                 |                        |
| operations             |                 |                        |
+------------------------+-----------------+------------------------+
| Decommissioning        | Same as CPX2-3  | *Same as CPX2-3*       |
| operations sequence    |                 |                        |
+------------------------+-----------------+------------------------+

-  OPERATION METHODS

In the simplified version of the Logistics module, all operation methods
are fixed as described in the table below.

Operation methods

================================ ========== ==========================
**Method**                       **Source** **Function**
================================ ========== ==========================
Transportation method            LMO        *Dry (on deck)*
Load-out method                  LMO        *Ignored*
Load-out from vessel deck method LMO        Lift
Piling method                    LMO        *Hammering (soil ignored)*
Cable burial method              LMO        *ploughing*
Post laying burial               LMO        *False*
Cable landfall method            LMO        *OCT*
================================ ========== ==========================

-  INFRASTRUCTURE REQUIREMENTS DEFINITION

Based on the available inputs at complexity level CPX1, the definition
of the infrastructure requirements is presented in the table below.

Operation requirements definition in respect to
infrastructure capabilities

+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| **PORT TERMINALS**   |                      |                      |
+----------------------+----------------------+----------------------+
| Filter according to  | *filter_max_dist*    | .. math              |
| maximum Euclidean    |                      | :: \text{op.requirem |
| distance to site     |                      | ent}\left\lbrack fil |
|                      |                      | ter\_ max\_ dist \ri |
|                      |                      | ght\rbrack = 2000000 |
+----------------------+----------------------+----------------------+
|                      |                      |                      |
+----------------------+----------------------+----------------------+
| **EQUIPMENT**        |                      |                      |
+----------------------+----------------------+----------------------+
| Maximum depth at     | *OEC.bathymetry,*    | *if op.name="device  |
| farm                 |                      | installation":*      |
|                      | *Sub.bathymetry*     |                      |
|                      |                      | :math:`\tex          |
|                      |                      | t{op.requirements}\l |
|                      |                      | eft\lbrack depth\_ m |
|                      |                      | ax \right\rbrack = \ |
|                      |                      | max\left( \text{OEC. |
|                      |                      | bathymetry} \right)` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ max\rbrack\  = \ \ |
|                      |                      | max\left( \text{sub. |
|                      |                      | bathymetry} \right)` |
+----------------------+----------------------+----------------------+
| Minimum depth at     |                      | *if op.name="device  |
| farm                 |                      | installation":*      |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ min\rbrack\  = \ \ |
|                      |                      | min\left( \text{OEC. |
|                      |                      | bathymetry} \right)` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ min\rbrack\  = \ \ |
|                      |                      | min\left( \text{sub. |
|                      |                      | bathymetry} \right)` |
+----------------------+----------------------+----------------------+
| Cable burial depth   | *Cable.bathymetry*   | .. math:: op.requir  |
|                      |                      | ements\lbrack cable\ |
|                      |                      | _ depth\rbrack\  = \ |
|                      |                      |  \max\left( cable.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Crane lift           | *Sub.drymass*        | .. m                 |
| requirement          |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  \text{lift} \right\ |
|                      |                      | rbrack = sub.drymass |
+----------------------+----------------------+----------------------+
| Maximum depth of     | *Sub.bathymetry*     | .. math:: op.requi   |
| piles                |                      | rements\lbrack depth |
|                      |                      | \_ max\rbrack\  = \  |
|                      |                      | \max\left( \text{sub |
|                      |                      | .bathymetry} \right) |
+----------------------+----------------------+----------------------+
| Maximum penetration  | *Sub.burial_depth*   | .. math:: op.requ    |
| depth of piles       |                      | irements\lbrack pill |
|                      |                      | ing\_ max\rbrack\  = |
|                      |                      |  \ \max\left( sub.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Maximum diameter of  | *Sub.diameter*       | .. math:             |
| piles                |                      | : op.requirements\lb |
|                      |                      | rack object\_ diamet |
|                      |                      | er\_ max\rbrack\  =  |
|                      |                      | \ \max\left( \text{s |
|                      |                      | ub.diameter} \right) |
+----------------------+----------------------+----------------------+
| Minimum diameter of  |                      | .. math:             |
| piles                |                      | : op.requirements\lb |
|                      |                      | rack object\_ diamet |
|                      |                      | er\_ min\rbrack\  =  |
|                      |                      | \ \min\left( \text{s |
|                      |                      | ub.diameter} \right) |
+----------------------+----------------------+----------------------+
| Maximum depth at     | *Cable. bathymetry*  | .. math:: \text      |
| farm location        |                      | {op.requirements}\le |
|                      |                      | ft\lbrack depth\_ ma |
|                      |                      | x \right\rbrack = \m |
|                      |                      | ax\left( \text{cable |
|                      |                      | .bathymetry} \right) |
+----------------------+----------------------+----------------------+
| Maximum cable burial | *Cable.burial_depth* | .. math:: \tex       |
| depth                |                      | t{op.requirements}\l |
|                      |                      | eft\lbrack cable\_ d |
|                      |                      | epth \right\rbrack = |
|                      |                      |  \max\left( cable.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Maximum cable        | *Cable.diameter*     | .. m                 |
| diameter             |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  cable\_ diameter\_  |
|                      |                      | max \right\rbrack =  |
|                      |                      | \max\left( \text{cab |
|                      |                      | le.diameter} \right) |
+----------------------+----------------------+----------------------+
| Minimum cable        |                      | .. m                 |
| diameter             |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  cable\_ diameter\_  |
|                      |                      | min \right\rbrack =  |
|                      |                      | \min\left( \text{cab |
|                      |                      | le.diameter} \right) |
+----------------------+----------------------+----------------------+
| **VESSELS**          |                      |                      |
+----------------------+----------------------+----------------------+
| Lifting power        | *OEC.drymass*        | *if op.name="device  |
| requirement          |                      | installation":*      |
|                      | *Sub.drymass*        |                      |
|                      |                      | :math:`op.requiremen |
|                      |                      | ts\lbrack lift\rbrac |
|                      |                      | k\  = \ OEC.drymass` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :m                   |
|                      |                      | ath:`\text{op.requir |
|                      |                      | ements}\left\lbrack  |
|                      |                      | \text{lift} \right\r |
|                      |                      | brack = sub.drymass` |
+----------------------+----------------------+----------------------+
| Maximum depth        | *OEC.bathymetry*     | .. math::            |
| requirement          |                      |  \text{op.requiremen |
|                      | *Sub.bathymetry*     | ts}\left\lbrack dept |
|                      |                      | h\_ max \right\rbrac |
|                      |                      | k = max(OEC.bathymet |
|                      |                      | ry,\ Sub.bathymetry) |
+----------------------+----------------------+----------------------+
| Minimum depth        |                      | .. math::            |
| requirement          |                      |  \text{op.requiremen |
|                      |                      | ts}\left\lbrack dept |
|                      |                      | h\_ min \right\rbrac |
|                      |                      | k = min(OEC.bathymet |
|                      |                      | ry,\ Sub.bathymetry) |
+----------------------+----------------------+----------------------+

INFRASTRUCTURE PRE-SELECTION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In order to reduce the number of potential infrastructure solutions, in
the simplified version of the LMO module, a pre-defined vessel
combination is fixed for each operation type. Given the device’s
dimensions and project requirements, as well as the vessel roles defined
in the vessel combination, vessels that do not comply with requirements
are discarded. However, in this functionality, the infrastructure
matching process is not carried out.

.. _inputs-1:

INPUTS
^^^^^^

Input table for infrastructure pre-selection functionality

+-------------+-------------+-------------+-------------+-------------+
| **Variable  | **Brief     | **Origin of | **Data      | **Units**   |
| name**      | Description | the Data**  | Model in    |             |
|             | of the      |             | LMO**       |             |
|             | Input       |             |             |             |
|             | Quantity**  |             |             |             |
+=============+=============+=============+=============+=============+
| *op.re      | Operation   | LMO         | Dictionary  | [-]         |
| quirements* | r           |             |             |             |
|             | equirements |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *           | Operation   | LMO         | Dictionary  | [-]         |
| op.methods* | methods     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ro         | Database    | Catalogue   | Dictionary  | [-]         |
| v_database* | with all    |             |             |             |
|             | ROVs        |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *diver      | Database    | Catalogue   | Dictionary  | [-]         |
| s_database* | with all    |             |             |             |
|             | divers      |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *pilin      | Database    | Catalogue   | Dictionary  | [-]         |
| g_database* | with all    |             |             |             |
|             | piling      |             |             |             |
|             | equipment   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *buria      | Database    | Catalogue   | Dictionary  | [-]         |
| l_database* | with all    |             |             |             |
|             | burial      |             |             |             |
|             | equipment   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ter        | Database    | Catalogue   | Dictionary  | [-]         |
| m_database* | with all    |             |             |             |
|             | terminals   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.name*   | Operation   | LMO         | string      | [-]         |
|             | name        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.d       | Operation   | LMO         | string      | [-]         |
| escription* | description |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessels     | LMO         | Catalogue   | [-]         |
| c_database* | C           |             |             |             |
|             | ombinations |             |             |             |
|             | database    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessels     | LMO         | Catalogue   | [-]         |
| e.database* | database    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Feasible    | LMO         | Catalogue   | [-]         |
| c_feasible* | Vessel      |             |             |             |
|             | C           |             |             |             |
|             | ombinations |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.dp_re   | DP          | LMO         | Int         | [-]         |
| quirements* | r           |             |             |             |
|             | equirements |             |             |             |
|             | for the     |             |             |             |
|             | operation   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.bp_re   | Required    | LMO         | Float       | ton         |
| quirements* | vessel      |             |             |             |
|             | bollard     |             |             |             |
|             | pull        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.site    | Minimum     | LMO         | Float       | m           |
| _min_depth* | water depth |             |             |             |
|             | at site     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.site    | Maximum     | LMO         | Float       | m           |
| _max_depth* | water depth |             |             |             |
|             | at site     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.si      | Load-out    | LMO         | String      | [-]         |
| te_loadout* | method at   |             |             |             |
|             | site (Lift) |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.LOA*    | Vessel      | Catalogue   | Float       | m           |
|             | Length      |             |             |             |
|             | Overall     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.beam*   | Vessel beam | Catalogue   | Float       | m           |
+-------------+-------------+-------------+-------------+-------------+
| *ve.draft*  | Vessel      |             |             | m           |
|             | draft       |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve         | Vessel free | Catalogue   | Float       | m\ :sup:`2` |
| .free_deck* | deck area   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessel deck | Catalogue   | Float       | ton/        |
| e.deck_str* | strength    |             |             | m\ :sup:`2` |
+-------------+-------------+-------------+-------------+-------------+
| *ve.        | Vessel      | Catalogue   | Float       | ton         |
| crane_lift* | crane       |             |             |             |
|             | maximum     |             |             |             |
|             | lifting     |             |             |             |
|             | capability  |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.DP*     | Vessel      | Catalogue   | Int         | [-]         |
|             | Dynamic     |             |             |             |
|             | Positioning |             |             |             |
|             | system      |             |             |             |
|             | rating      |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.type*   | Vessel type | Catalogue   | String      | [-]         |
+-------------+-------------+-------------+-------------+-------------+
| *ve.jup     | Vessel jack | Catalogue   | Float       | m           |
| _max_water* | up maximum  |             |             |             |
|             | operational |             |             |             |
|             | water depth |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.totalca | Vessel      | Catalogue   | Float       | Ton         |
| blestorage* | turntable   |             |             |             |
|             | loading     |             |             |             |
|             | capacity    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Inner       | Catalogue   | Float       | m           |
| e.turn_diam | diameter    |             |             |             |
| eter_inner* | vessel turn |             |             |             |
|             | table       |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *numberobj  | Maximum     | LMO         | Int         | [-]         |
| ectsondeck* | number of   |             |             |             |
|             | items       |             |             |             |
|             | (piles,     |             |             |             |
|             | devices) on |             |             |             |
|             | deck        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve_Te*     | Tug         | LMO         | Float       | [-]         |
|             | efficiency. |             |             |             |
|             | Hard coded  |             |             |             |
|             | as 0.75     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *UK_        | Under keel  | LMO         | Float       | [-]         |
| contigency* | clearence   |             |             |             |
|             | c           |             |             |             |
|             | ontingency, |             |             |             |
|             | set as 10%  |             |             |             |
|             | of draft.   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+

.. _outputs-1:

OUTPUTS
^^^^^^^

Infrastructure pre-selection functionality outputs

+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| **Ports**            |                      |                      |
+----------------------+----------------------+----------------------+
| Port maximum         | *op.requirem         | .. math:: if\ ter    |
| distance             | ents[port_max_dist]* | minal.distance \leq  |
|                      |                      | op.requirements\left |
|                      | *t                   | \lbrack port\_ max\_ |
|                      | erminal.coordinates* |  dist \right\rbrack: |
|                      |                      |                      |
|                      |                      | :                    |
|                      |                      | math:`terminal\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math:: e          |
|                      |                      | lse,\ terminal\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Area capabilities    | *op.requirem         | .. math:: \te        |
|                      | ents[terminal_area]* | xt{if\ op.requiremen |
|                      |                      | ts}\left\lbrack term |
|                      | *op                  | inal\_ area \right\r |
|                      | .requirements[area]* | brack = \text{True}: |
|                      |                      |                      |
|                      | *terminal.area*      | :math:`\text         |
|                      |                      | {if\ }terminal.area  |
|                      |                      | \geq op.requirements |
|                      |                      | \left\lbrack \text{a |
|                      |                      | rea} \right\rbrack:` |
|                      |                      |                      |
|                      |                      | :                    |
|                      |                      | math:`terminal\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | :math:`el            |
|                      |                      | se,\ terminal\_ feas |
|                      |                      | ible = \text{False}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      | else,\ terminal\_ fe |
|                      |                      | asible = \text{True} |
+----------------------+----------------------+----------------------+
| **Equipment**        |                      |                      |
+----------------------+----------------------+----------------------+
| ROV class            | *o                   | .. mat               |
|                      | p.requirements[rov]* | h:: \text{if\ op.req |
|                      |                      | uirements}\left\lbra |
|                      | *rov.class*          | ck \text{rov} \right |
|                      |                      | \rbrack = rov.class: |
|                      |                      |                      |
|                      |                      | :math:`rov\_ fea     |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. mat               |
|                      |                      | h:: else,\ rov\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| ROV depth            | *op.requ             | .. math:: \text{     |
| capabilities         | irements[depth_max]* | if\ }rov.max\_ depth |
|                      |                      |  \geq op.requirement |
|                      | *rov.max_depth*      | s\left\lbrack depth\ |
|                      |                      | _ max \right\rbrack: |
|                      |                      |                      |
|                      |                      | :ma                  |
|                      |                      | th:`\text{rov}\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. mat               |
|                      |                      | h:: else,\ rov\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Depth rating (m)     | Maximum water depth  | .. math:: \text{if\  |
|                      | at foundation        |  piling}.max\_ depth |
|                      | location (m):        |  \geq op.requirement |
|                      |                      | s\left\lbrack depth\ |
|                      | *op.requ             | _ max \right\rbrack: |
|                      | irements[depth_max]* |                      |
|                      |                      | :math:`piling\_ fea  |
|                      | *piling.max_depth*   | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ piling\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Pile sleeve diameter | *pil                 | .. math:: \text{     |
| (m)                  | ing.hammer_max_diam* | if\ piling}.hammer\_ |
|                      |                      |  max\_ diam \geq op. |
|                      | *pil                 | requirements\left\lb |
|                      | ing.hammer_min_diam* | rack obj\_ diameter\ |
|                      |                      | _ max \right\rbrack\ |
|                      | *op.requirement      |  \mathbf{\text{AND}} |
|                      | s[obj_diameter_max]* |                      |
|                      |                      | .. mat               |
|                      | *op.requirement      | h:: \text{\ \ \ \ \  |
|                      | s[obj_diameter_min]* | piling}.hammer\_ min |
|                      |                      | \_ diameter \leq op. |
|                      |                      | requirements\left\lb |
|                      |                      | rack obj\_ diameter\ |
|                      |                      | _ min \right\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`piling\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ piling\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | Maximum water depth  | .. math:: \text{if\  |
| equipment depth      | of cables (m):       |  burial}.max\_ depth |
| rating (m)           |                      |  \geq op.requirement |
|                      | *op.requ             | s\left\lbrack depth\ |
|                      | irements[depth_max]* | _ max \right\rbrack: |
|                      |                      |                      |
|                      | *burial.max_depth*   | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | Maximum cable        | .. math:: \text{i    |
| equipment cable      | diameter (mm):       | f\ burial}.max\_ cab |
| diameter (mm)        |                      | le\_ diam \geq op.re |
|                      | *op.requirements[    | quirements\left\lbra |
|                      | cable_diameter_max]* | ck cable\_ diameter\ |
|                      |                      | _ max \right\rbrack: |
|                      | *bu                  |                      |
|                      | rial.max_cable_diam* | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | *burial.cap          | .. math:: if\ buria  |
| equipment            | abilities_ploughing* | l.capabilities\_ plo |
| capabilities         |                      | ughing = \text{True} |
|                      |                      |                      |
|                      |                      | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | *burial.             | .. ma                |
| equipment Ploughing  | max_depth_ploughing* | th:: if\ burial.max\ |
| depth rating (m)     |                      | _ depth\_ ploughing  |
|                      | *op.requir           | \geq op.requirements |
|                      | ements[cable_depth]* | \left\lbrack cable\_ |
|                      |                      |  depth \right\rbrack |
|                      |                      |                      |
|                      |                      | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| **Vessels**          |                      |                      |
+----------------------+----------------------+----------------------+
| Vessel Combination   | *op.name*            | *See* Table 3.61     |
|                      |                      |                      |
|                      | *vc.type*            |                      |
+----------------------+----------------------+----------------------+
| Crane capabilities   | *op                  | .. math:: if\ ves    |
| [ton]                | .requirements[lift]* | sel.crane\_ capacity |
|                      |                      |  \geq op.requiremnts |
|                      | *ve.crane_capacity*  | \lbrack lift\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Dynamic positioning  | *                    | .                    |
|                      | op.requirements[dp]* | . math:: if\ vessel. |
|                      |                      | dp \geq op.requiremn |
|                      | *ve.dp*              | ts\lbrack dp\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Jack-up capabilities | *op.requ             | :math                |
|                      | irements[depth_max]* | :`if\ vessel.jup\_ c |
|                      |                      | apabilities = True`: |
|                      | Jack-up vessel legs  |                      |
|                      | operating depth (m): | :ma                  |
|                      |                      | th:`if\ vessel.jup\_ |
|                      | *                    |  max\_ water \geq \t |
|                      | ve.jup_capabilities* | ext{op.requirements} |
|                      |                      | \left\lbrack depth\_ |
|                      | *ve.jup_max_water*   |  max \right\rbrack`: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | :math:               |
|                      |                      | `else,\ vesse\_ feas |
|                      |                      | ible = \text{False}` |
|                      |                      |                      |
|                      |                      | .. math:             |
|                      |                      | : else,\ vessel\_ fe |
|                      |                      | asible = \text{True} |
+----------------------+----------------------+----------------------+
| Depth clearance [m]  | *op.requ             | .. math              |
|                      | irements[depth_min]* | :: c\_ ukc = UKC\tex |
|                      |                      | t{\_}\text{contigenc |
|                      | *ve.draft*           | y} \times \ ve.draft |
|                      |                      |                      |
|                      |                      | :math:`if\ vessel.d  |
|                      |                      | raft*c\_ ukc \leq \t |
|                      |                      | ext{op.requirements} |
|                      |                      | \left\lbrack depth\_ |
|                      |                      |  min \right\rbrack`: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math:             |
|                      |                      | : else,\ vesse\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| ROV capabilities     | *o                   | ..                   |
|                      | p.requirements[rov]* | math:: \text{if\ op. |
|                      |                      | requiremnts}\left\lb |
|                      | *ve.rov_ready*       | rack \text{rov} \rig |
|                      |                      | ht\rbrack = = "work" |
|                      |                      |                      |
|                      |                      | :math:               |
|                      |                      | `if\ vessels.rov\_ r |
|                      |                      | eady = \text{True}:` |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
+----------------------+----------------------+----------------------+
| Vessel type          | *vc.type*            | .. math:: if\ ve     |
|                      |                      | ssel.type = vc.type: |
|                      | *ve.type*            |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Turntable storage    | *op.require          | .. ma                |
| [ton]                | ments[turn_storage]* | th:: if\ vessel.turn |
|                      |                      | \_ storage \geq op.r |
|                      | *vesturn_storage*    | equiremnts\lbrack tu |
|                      |                      | rn\_ storage\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+

Default vessel combinations for the different operations in
CPX1

+-------+-------+-------+-------+-------+-------+-------+-------+
| *     | **T   | **    | *     | **    | **Tow | **    | **Su  |
| *Oper | ransp | No.** | *Main | No.** | ves   | No.** | pport |
| ation | ort** |       | ves   |       | sel** |       | ves   |
| n     |       |       | sel** |       |       |       | sel** |
| ame** |       |       |       |       |       |       |       |
+=======+=======+=======+=======+=======+=======+=======+=======+
| D     | *On   | *1*   | Prop  | *–*   | *–*   | *–*   | *–*   |
| evice | deck* |       | elled |       |       |       |       |
| in    |       |       | crane |       |       |       |       |
| stall |       |       | v     |       |       |       |       |
| ation |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
|       | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
|       | -tow* |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Colle | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| ction | deck* |       | elled |       |       |       |       |
| point |       |       | crane |       |       |       |       |
| in    |       |       | v     |       |       |       |       |
| stall |       |       | essel |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Found | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| ation | deck* |       | elled |       |       |       |       |
| In    |       |       | crane |       |       |       |       |
| stall |       |       | v     |       |       |       |       |
| ation |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| in    | deck* |       | L     |       |       |       |       |
| stall |       |       | aying |       |       |       |       |
| ation |       |       | V     |       |       |       |       |
| (e    |       |       | essel |       |       |       |       |
| xport |       |       |       |       |       |       |       |
| and   |       |       |       |       |       |       |       |
| a     |       |       |       |       |       |       |       |
| rray) |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Moo   | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| rings | deck* |       |       |       |       |       |       |
| in    |       |       |       |       |       |       |       |
| stall |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Su    | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| pport | deck* |       | elled |       |       |       |       |
| Stru  |       |       | crane |       |       |       |       |
| cture |       |       | v     |       |       |       |       |
| In    |       |       | essel |       |       |       |       |
| stall |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| To    | *On   | 1     | CTV   | *–*   | *–*   | *–*   | *–*   |
| pside | deck* |       |       |       |       |       |       |
| Inspe |       |       |       |       |       |       |       |
| ction |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Under | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| water | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Mo    | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| oring | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Array | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| Cable | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| E     | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| xport | deck* |       | su    |       |       |       |       |
| Cable |       |       | pport |       |       |       |       |
| Inspe |       |       | v     |       |       |       |       |
| ction |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| evice | -tow* |       |       |       |       |       |       |
| Retr  |       |       |       |       |       |       |       |
| ieval |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| evice | -tow* |       |       |       |       |       |       |
| Re    |       |       |       |       |       |       |       |
| deplo |       |       |       |       |       |       |       |
| yment |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| evice | deck* |       | elled |       |       |       |       |
| R     |       |       | crane |       |       |       |       |
| epair |       |       | v     |       |       |       |       |
| On    |       |       | essel |       |       |       |       |
| Site  |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Mo    | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| oring | deck* |       |       |       |       |       |       |
| Line  |       |       |       |       |       |       |       |
| R     |       |       |       |       |       |       |       |
| eplac |       |       |       |       |       |       |       |
| ement |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| R     | deck* |       | L     |       |       |       |       |
| eplac |       |       | aying |       |       |       |       |
| ement |       |       | V     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| R     | deck* |       | L     |       |       |       |       |
| epair |       |       | aying |       |       |       |       |
|       |       |       | V     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| d     |       |       | v     |       |       |       |       |
| evice |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
|       | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
|       | -tow* |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| colle |       |       | v     |       |       |       |       |
| ction |       |       | essel |       |       |       |       |
| point |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| su    |       |       | v     |       |       |       |       |
| pport |       |       | essel |       |       |       |       |
| stru  |       |       |       |       |       |       |       |
| cture |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       |       |       |       |       |       |
| oning |       |       |       |       |       |       |       |
| moo   |       |       |       |       |       |       |       |
| rings |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| f     |       |       | v     |       |       |       |       |
| ounda |       |       | essel |       |       |       |       |
| tions |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+

OPERATION COMPUTATION
~~~~~~~~~~~~~~~~~~~~~

In complexity level CPX1 of the operation computation functionality,
operations are not broken down into a sequence of activities. The
transit distance from port to site is estimated as a straight line
between the two coordinates. Net duration is calculated assuming a total
duration and estimating the number of trips from port to site for
transporting equipment. However, the remaining functionalities remain
the same as in the full complexity version described in Section 3.3.3.

OPERATION CALENDARIZATION
~~~~~~~~~~~~~~~~~~~~~~~~~

The operation calendarization functionality operates exactly in the same
way for both versions (full complexity and simplified) of the Logistics
and Marine Operations module. For more information see Section 3.3.4.

.. [1]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.

.. [2]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.

.. [3]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.

.. |image1| image:: media/image1.png
   :width: 4.22803in
   :height: 4.73614in
