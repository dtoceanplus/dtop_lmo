.. _lmo-functionalities-simplified-design-process:

Functionalities of simplified design process
=============================================

.. warning::
    This content is from D5.7 Logistics and Marine Operations alpha version,
    and still to be reviewed and updated

.. contents::

In the lowest complexity level (CPX1), the LMO module produces a generic
logistic solution comprised of a selection of vessels, equipment and
ports, and a simplified operation plan which include durations and rough
estimates of waiting on weather contingencies. Some simplified
assumptions are adopted as follows:

-  Operation methods such as transportation, load-out at port, cable
   burial, and cable landfall methods are assumed.

-  The infrastructure selection is based on a pre-defined combination of
   vessels for the required operations.

-  Infrastructure solution matching is not carried out.


.. figure:: fig/exp_simp_fig1.png
   :alt: Main functionalities of the Logistics module at simplified complexity

   Main functionalities of the Logistics module at simplified complexity


Similarly to the full complexity version, the simplified version of the
LMO module keeps its four major functionalities, as shown in Figure
3.10, although with some simplifications:

1. **Operation pre-configuration**: For the low complexity level, the
   operation pre-configuration functionality identifies operation
   requirements and assumes pre-defined operation methods (e.g.
   transport: “dry”).

2. **Infrastructure pre-selection:** In the simplified version of the
   module, this functionality is common to all three phases of the
   project and consists of selecting a pre-defined combination of
   vessels, equipment and ports terminal that comply with operation
   assumptions, although compatibility between vessels, ports and
   equipment are not assessed.

3. **Operation computation:** For the lowest complexity level, this
   functionality, common to all three phases of the operation, is
   responsible for analysing the pre-selected infrastructure
   combinations and calculating expected operation durations and waiting
   on weather for different months of the year. Based on operation
   durations and selected infrastructure, the operation costs are
   calculated for the pre-selected infrastructure solution.

4. **Operation calendarization:** In the simplified version of the
   Logistics module, the operation calendarization operates exactly in
   the same way as in the full complexity level. For more information
   see Section 3.3.4.

Operation Pre-Configuration
---------------------------

In the simplified version of the LMO code, some realistic assumptions
are adopted to simplify the computation process and input requirements.

Inputs
^^^^^^

Table 3.55 Input table for the Operation pre-configuration functionality
at CPX1

+---------------+-------------+-------------+-------------+-----------+
| **Variable    | **Brief     | **Origin of | **Data      | **Units** |
| name**        | Description | the Data**  | Model in    |           |
|               | of the      |             | LMO**       |           |
|               | Input       |             |             |           |
|               | Quantity**  |             |             |           |
+===============+=============+=============+=============+===========+
|*hierarchy_et* | Hierarchy   | ET          | Pandas      | [-]       |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | energy      |             |             |           |
|               | tra         |             |             |           |
|               | nsformation |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
|*hierarchy_ed* | Hierarchy   | ED          | Pandas      | [-]       |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | energy      |             |             |           |
|               | delivery    |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
|*hierarchy_sk* | Hierarchy   | SK          | Pandas      | [-]       |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | station     |             |             |           |
|               | keeping     |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
| *bom_et*      | Simplified  | ET          | Pandas      | [-]       |
|               | BOM         |             |             |           |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | energy      |             |             |           |
|               | tra         |             |             |           |
|               | nsformation |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
| *bom_ed*      | Simplified  | ED          | Pandas      | [-]       |
|               | BOM         |             |             |           |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | energy      |             |             |           |
|               | delivery    |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
| *bom_sk*      | Simplified  | SK          | Pandas      | [-]       |
|               | BOM         |             |             |           |
|               | datafile    |             |             |           |
|               | from the    |             |             |           |
|               | station     |             |             |           |
|               | keeping     |             |             |           |
|               | system      |             |             |           |
+---------------+-------------+-------------+-------------+-----------+
| *Hs_          | Met-ocean   | SC          | Pandas      | [-]       |
| timeseries*   | timeseries  |             |             |           |
|               | of Hs       |             |             |           |
|               | m           |             |             |           |
|               | easurements |             |             |           |
|               | (default    |             |             |           |
|               | site)       |             |             |           |
+---------------+-------------+-------------+-------------+-----------+

Outputs
^^^^^^^

Operation identification and sequence
"""""""""""""""""""""""""""""""""""""

Table 3.56 Operation Pre-configuration outputs for installation,
maintenance and decommissioning operations at CPX1

+------------------------+-----------------+------------------------+
| **Requirement**        | **Inputs**      | **Function**           |
+========================+=================+========================+
| List of installation   | *Hierarchy_ET*  | *If ndevices>0:*       |
| operations             |                 |                        |
|                        | *Hierarchy_ED,* | *o                     |
|                        |                 | px_list.append(“device |
|                        | *Hierarchy_SK*  | installation”)*        |
|                        |                 |                        |
|                        | *Ndevices*      | *If hierarchy_ED       |
|                        |                 | includes “array        |
|                        | *cp.type*       | cable”:*               |
|                        |                 |                        |
|                        |                 | *                      |
|                        |                 | opx_list.append(“array |
|                        |                 | cable installation”)*  |
|                        |                 |                        |
|                        |                 | *If hierarchy_ED       |
|                        |                 | includes “export       |
|                        |                 | cable”:*               |
|                        |                 |                        |
|                        |                 | *o                     |
|                        |                 | px_list.append(“export |
|                        |                 | cable installation”)*  |
|                        |                 |                        |
|                        |                 | *If hierarchy_ED       |
|                        |                 | includes “collection   |
|                        |                 | point”:*               |
|                        |                 |                        |
|                        |                 | *If not all cp’s are   |
|                        |                 | “hub”:*                |
|                        |                 |                        |
|                        |                 | *opx_l                 |
|                        |                 | ist.append(“collection |
|                        |                 | point installation”)*  |
|                        |                 |                        |
|                        |                 | *if “moorings” in      |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *opx                   |
|                        |                 | _list.append(“moorings |
|                        |                 | installation”)*        |
|                        |                 |                        |
|                        |                 | *if “pile” or “suction |
|                        |                 | caisson” in            |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *opx_l                 |
|                        |                 | ist.append(“foundation |
|                        |                 | installation”)*        |
|                        |                 |                        |
|                        |                 | *if “support           |
|                        |                 | structure” in          |
|                        |                 | hierarchy_SK:*         |
|                        |                 |                        |
|                        |                 | *op                    |
|                        |                 | x_list.append(“support |
|                        |                 | structure              |
|                        |                 | installation”)*        |
+------------------------+-----------------+------------------------+
| Operation sequence     | *None*          | Seq1 = [“Foundation    |
| suggestion [3]_        |                 | installation”,”        |
|                        |                 | Moorings               |
|                        |                 | installation”,         |
|                        |                 | “Support structures    |
|                        |                 | ins                    |
|                        |                 | tallation”,“Collection |
|                        |                 | point installation”,   |
|                        |                 | Device installation”,  |
|                        |                 | “Export cable          |
|                        |                 | installation”, “Array  |
|                        |                 | cable installation”,   |
|                        |                 | “Post-lay cable        |
|                        |                 | burial”, “External     |
|                        |                 | protections”]          |
+------------------------+-----------------+------------------------+
| List of preventive     | Same as CPX2-3  | *Same as CPX2-3.*      |
| maintenance operations |                 |                        |
+------------------------+-----------------+------------------------+
| List of corrective     | Same as CPX2-3  | *Same as CPX2-3*       |
| maintenance operations |                 |                        |
+------------------------+-----------------+------------------------+
| List of                | Same as CPX2-3  | *Same as CPX2-3*       |
| decommissioning        |                 |                        |
| operations             |                 |                        |
+------------------------+-----------------+------------------------+
| Decommissioning        | Same as CPX2-3  | *Same as CPX2-3*       |
| operations sequence    |                 |                        |
+------------------------+-----------------+------------------------+

Operation Methods
"""""""""""""""""

In the simplified version of the Logistics module, all operation methods
are fixed as described in Table 3.57.

Table 3.57 Operation methods

================================ ========== ==========================
**Method**                       **Source** **Function**
================================ ========== ==========================
Transportation method            LMO        *Dry (on deck)*
Load-out method                  LMO        *Ignored*
Load-out from vessel deck method LMO        Lift
Piling method                    LMO        *Hammering (soil ignored)*
Cable burial method              LMO        *ploughing*
Post laying burial               LMO        *False*
Cable landfall method            LMO        *OCT*
================================ ========== ==========================

Infrastructure requirements definition
"""""""""""""""""""""""""""""""""""""""

Based on the available inputs at complexity level CPX1, the definition
of the infrastructure requirements is presented in Table 3.58.

Table 3.58 Operation requirements definition in respect to
infrastructure capabilities

Port terminal requirements
""""""""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| Filter according to  | *filter_max_dist*    | .. math              |
| maximum Euclidean    |                      | :: \text{op.requirem |
| distance to site     |                      | ent}\left\lbrack fil |
|                      |                      | ter\_ max\_ dist \ri |
|                      |                      | ght\rbrack = 2000000 |
+----------------------+----------------------+----------------------+

Equipment requirements
""""""""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| Maximum depth at     | *OEC.bathymetry,*    | *if op.name="device  |
| farm                 |                      | installation":*      |
|                      | *Sub.bathymetry*     |                      |
|                      |                      | :math:`\tex          |
|                      |                      | t{op.requirements}\l |
|                      |                      | eft\lbrack depth\_ m |
|                      |                      | ax \right\rbrack = \ |
|                      |                      | max\left( \text{OEC. |
|                      |                      | bathymetry} \right)` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ max\rbrack\  = \ \ |
|                      |                      | max\left( \text{sub. |
|                      |                      | bathymetry} \right)` |
+----------------------+----------------------+----------------------+
| Minimum depth at     |                      | *if op.name="device  |
| farm                 |                      | installation":*      |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ min\rbrack\  = \ \ |
|                      |                      | min\left( \text{OEC. |
|                      |                      | bathymetry} \right)` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :math:`op.requir     |
|                      |                      | ements\lbrack depth\ |
|                      |                      | _ min\rbrack\  = \ \ |
|                      |                      | min\left( \text{sub. |
|                      |                      | bathymetry} \right)` |
+----------------------+----------------------+----------------------+
| Cable burial depth   | *Cable.bathymetry*   | .. math:: op.requir  |
|                      |                      | ements\lbrack cable\ |
|                      |                      | _ depth\rbrack\  = \ |
|                      |                      |  \max\left( cable.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Crane lift           | *Sub.drymass*        | .. m                 |
| requirement          |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  \text{lift} \right\ |
|                      |                      | rbrack = sub.drymass |
+----------------------+----------------------+----------------------+
| Maximum depth of     | *Sub.bathymetry*     | .. math:: op.requi   |
| piles                |                      | rements\lbrack depth |
|                      |                      | \_ max\rbrack\  = \  |
|                      |                      | \max\left( \text{sub |
|                      |                      | .bathymetry} \right) |
+----------------------+----------------------+----------------------+
| Maximum penetration  | *Sub.burial_depth*   | .. math:: op.requ    |
| depth of piles       |                      | irements\lbrack pill |
|                      |                      | ing\_ max\rbrack\  = |
|                      |                      |  \ \max\left( sub.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Maximum diameter of  | *Sub.diameter*       | .. math:             |
| piles                |                      | : op.requirements\lb |
|                      |                      | rack object\_ diamet |
|                      |                      | er\_ max\rbrack\  =  |
|                      |                      | \ \max\left( \text{s |
|                      |                      | ub.diameter} \right) |
+----------------------+----------------------+----------------------+
| Minimum diameter of  |                      | .. math:             |
| piles                |                      | : op.requirements\lb |
|                      |                      | rack object\_ diamet |
|                      |                      | er\_ min\rbrack\  =  |
|                      |                      | \ \min\left( \text{s |
|                      |                      | ub.diameter} \right) |
+----------------------+----------------------+----------------------+
| Maximum depth at     | *Cable. bathymetry*  | .. math:: \text      |
| farm location        |                      | {op.requirements}\le |
|                      |                      | ft\lbrack depth\_ ma |
|                      |                      | x \right\rbrack = \m |
|                      |                      | ax\left( \text{cable |
|                      |                      | .bathymetry} \right) |
+----------------------+----------------------+----------------------+
| Maximum cable burial | *Cable.burial_depth* | .. math:: \tex       |
| depth                |                      | t{op.requirements}\l |
|                      |                      | eft\lbrack cable\_ d |
|                      |                      | epth \right\rbrack = |
|                      |                      |  \max\left( cable.bu |
|                      |                      | rial\_ depth \right) |
+----------------------+----------------------+----------------------+
| Maximum cable        | *Cable.diameter*     | .. m                 |
| diameter             |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  cable\_ diameter\_  |
|                      |                      | max \right\rbrack =  |
|                      |                      | \max\left( \text{cab |
|                      |                      | le.diameter} \right) |
+----------------------+----------------------+----------------------+
| Minimum cable        |                      | .. m                 |
| diameter             |                      | ath:: \text{op.requi |
|                      |                      | rements}\left\lbrack |
|                      |                      |  cable\_ diameter\_  |
|                      |                      | min \right\rbrack =  |
|                      |                      | \min\left( \text{cab |
|                      |                      | le.diameter} \right) |
+----------------------+----------------------+----------------------+

Vessel requirements
""""""""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| Lifting power        | *OEC.drymass*        | *if op.name="device  |
| requirement          |                      | installation":*      |
|                      | *Sub.drymass*        |                      |
|                      |                      | :math:`op.requiremen |
|                      |                      | ts\lbrack lift\rbrac |
|                      |                      | k\  = \ OEC.drymass` |
|                      |                      |                      |
|                      |                      | *else:*              |
|                      |                      |                      |
|                      |                      | :m                   |
|                      |                      | ath:`\text{op.requir |
|                      |                      | ements}\left\lbrack  |
|                      |                      | \text{lift} \right\r |
|                      |                      | brack = sub.drymass` |
+----------------------+----------------------+----------------------+
| Maximum depth        | *OEC.bathymetry*     | .. math::            |
| requirement          |                      |  \text{op.requiremen |
|                      | *Sub.bathymetry*     | ts}\left\lbrack dept |
|                      |                      | h\_ max \right\rbrac |
|                      |                      | k = max(OEC.bathymet |
|                      |                      | ry,\ Sub.bathymetry) |
+----------------------+----------------------+----------------------+
| Minimum depth        |                      | .. math::            |
| requirement          |                      |  \text{op.requiremen |
|                      |                      | ts}\left\lbrack dept |
|                      |                      | h\_ min \right\rbrac |
|                      |                      | k = min(OEC.bathymet |
|                      |                      | ry,\ Sub.bathymetry) |
+----------------------+----------------------+----------------------+

Infrastructure Pre-selection
----------------------------

In order to reduce the number of potential infrastructure solutions, in
the simplified version of the LMO module, a pre-defined vessel
combination is fixed for each operation type. Given the device’s
dimensions and project requirements, as well as the vessel roles defined
in the vessel combination, vessels that do not comply with requirements
are discarded. However, in this complexity level, the infrastructure
matching process is not carried out.

.. _inputs-1:

Inputs
^^^^^^

Table 3.59 Input table for infrastructure pre-selection functionality

+-------------+-------------+-------------+-------------+-------------+
| **Variable  | **Brief     | **Origin of | **Data      | **Units**   |
| name**      | Description | the Data**  | Model in    |             |
|             | of the      |             | LMO**       |             |
|             | Input       |             |             |             |
|             | Quantity**  |             |             |             |
+=============+=============+=============+=============+=============+
| *op.re      | Operation   | LMO         | Dictionary  | [-]         |
| quirements* | r           |             |             |             |
|             | equirements |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *           | Operation   | LMO         | Dictionary  | [-]         |
| op.methods* | methods     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ro         | Database    | Catalogue   | Dictionary  | [-]         |
| v_database* | with all    |             |             |             |
|             | ROVs        |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *diver      | Database    | Catalogue   | Dictionary  | [-]         |
| s_database* | with all    |             |             |             |
|             | divers      |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *pilin      | Database    | Catalogue   | Dictionary  | [-]         |
| g_database* | with all    |             |             |             |
|             | piling      |             |             |             |
|             | equipment   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *buria      | Database    | Catalogue   | Dictionary  | [-]         |
| l_database* | with all    |             |             |             |
|             | burial      |             |             |             |
|             | equipment   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ter        | Database    | Catalogue   | Dictionary  | [-]         |
| m_database* | with all    |             |             |             |
|             | terminals   |             |             |             |
|             | available   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.name*   | Operation   | LMO         | string      | [-]         |
|             | name        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.d       | Operation   | LMO         | string      | [-]         |
| escription* | description |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessels     | LMO         | Catalogue   | [-]         |
| c_database* | C           |             |             |             |
|             | ombinations |             |             |             |
|             | database    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessels     | LMO         | Catalogue   | [-]         |
| e.database* | database    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Feasible    | LMO         | Catalogue   | [-]         |
| c_feasible* | Vessel      |             |             |             |
|             | C           |             |             |             |
|             | ombinations |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.dp_re   | DP          | LMO         | Int         | [-]         |
| quirements* | r           |             |             |             |
|             | equirements |             |             |             |
|             | for the     |             |             |             |
|             | operation   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.bp_re   | Required    | LMO         | Float       | ton         |
| quirements* | vessel      |             |             |             |
|             | bollard     |             |             |             |
|             | pull        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.site    | Minimum     | LMO         | Float       | m           |
| _min_depth* | water depth |             |             |             |
|             | at site     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.site    | Maximum     | LMO         | Float       | m           |
| _max_depth* | water depth |             |             |             |
|             | at site     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *op.si      | Load-out    | LMO         | String      | [-]         |
| te_loadout* | method at   |             |             |             |
|             | site (Lift) |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.LOA*    | Vessel      | Catalogue   | Float       | m           |
|             | Length      |             |             |             |
|             | Overall     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.beam*   | Vessel beam | Catalogue   | Float       | m           |
+-------------+-------------+-------------+-------------+-------------+
| *ve.draft*  | Vessel      |             |             | m           |
|             | draft       |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve         | Vessel free | Catalogue   | Float       | m\ :sup:`2` |
| .free_deck* | deck area   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Vessel deck | Catalogue   | Float       | ton/        |
| e.deck_str* | strength    |             |             | m\ :sup:`2` |
+-------------+-------------+-------------+-------------+-------------+
| *ve.        | Vessel      | Catalogue   | Float       | ton         |
| crane_lift* | crane       |             |             |             |
|             | maximum     |             |             |             |
|             | lifting     |             |             |             |
|             | capability  |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.DP*     | Vessel      | Catalogue   | Int         | [-]         |
|             | Dynamic     |             |             |             |
|             | Positioning |             |             |             |
|             | system      |             |             |             |
|             | rating      |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.type*   | Vessel type | Catalogue   | String      | [-]         |
+-------------+-------------+-------------+-------------+-------------+
| *ve.jup     | Vessel jack | Catalogue   | Float       | m           |
| _max_water* | up maximum  |             |             |             |
|             | operational |             |             |             |
|             | water depth |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve.totalca | Vessel      | Catalogue   | Float       | Ton         |
| blestorage* | turntable   |             |             |             |
|             | loading     |             |             |             |
|             | capacity    |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *v          | Inner       | Catalogue   | Float       | m           |
| e.turn_diam | diameter    |             |             |             |
| eter_inner* | vessel turn |             |             |             |
|             | table       |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *numberobj  | Maximum     | LMO         | Int         | [-]         |
| ectsondeck* | number of   |             |             |             |
|             | items       |             |             |             |
|             | (piles,     |             |             |             |
|             | devices) on |             |             |             |
|             | deck        |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *ve_Te*     | Tug         | LMO         | Float       | [-]         |
|             | efficiency. |             |             |             |
|             | Hard coded  |             |             |             |
|             | as 0.75     |             |             |             |
+-------------+-------------+-------------+-------------+-------------+
| *UK_        | Under keel  | LMO         | Float       | [-]         |
| contigency* | clearence   |             |             |             |
|             | c           |             |             |             |
|             | ontingency, |             |             |             |
|             | set as 10%  |             |             |             |
|             | of draft.   |             |             |             |
+-------------+-------------+-------------+-------------+-------------+

.. _outputs-1:

Ouputs
^^^^^^^

Port pre-selection
"""""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| Port maximum         | *op.requirem         | .. math:: if\ ter    |
| distance             | ents[port_max_dist]* | minal.distance \leq  |
|                      |                      | op.requirements\left |
|                      | *t                   | \lbrack port\_ max\_ |
|                      | erminal.coordinates* |  dist \right\rbrack: |
|                      |                      |                      |
|                      |                      | :                    |
|                      |                      | math:`terminal\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math:: e          |
|                      |                      | lse,\ terminal\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Area capabilities    | *op.requirem         | .. math:: \te        |
|                      | ents[terminal_area]* | xt{if\ op.requiremen |
|                      |                      | ts}\left\lbrack term |
|                      | *op                  | inal\_ area \right\r |
|                      | .requirements[area]* | brack = \text{True}: |
|                      |                      |                      |
|                      | *terminal.area*      | :math:`\text         |
|                      |                      | {if\ }terminal.area  |
|                      |                      | \geq op.requirements |
|                      |                      | \left\lbrack \text{a |
|                      |                      | rea} \right\rbrack:` |
|                      |                      |                      |
|                      |                      | :                    |
|                      |                      | math:`terminal\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | :math:`el            |
|                      |                      | se,\ terminal\_ feas |
|                      |                      | ible = \text{False}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      | else,\ terminal\_ fe |
|                      |                      | asible = \text{True} |
+----------------------+----------------------+----------------------+

Equipment pre-selection
"""""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| ROV class            | *o                   | .. mat               |
|                      | p.requirements[rov]* | h:: \text{if\ op.req |
|                      |                      | uirements}\left\lbra |
|                      | *rov.class*          | ck \text{rov} \right |
|                      |                      | \rbrack = rov.class: |
|                      |                      |                      |
|                      |                      | :math:`rov\_ fea     |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. mat               |
|                      |                      | h:: else,\ rov\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| ROV depth            | *op.requ             | .. math:: \text{     |
| capabilities         | irements[depth_max]* | if\ }rov.max\_ depth |
|                      |                      |  \geq op.requirement |
|                      | *rov.max_depth*      | s\left\lbrack depth\ |
|                      |                      | _ max \right\rbrack: |
|                      |                      |                      |
|                      |                      | :ma                  |
|                      |                      | th:`\text{rov}\_ fea |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. mat               |
|                      |                      | h:: else,\ rov\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Depth rating (m)     | Maximum water depth  | .. math:: \text{if\  |
|                      | at foundation        |  piling}.max\_ depth |
|                      | location (m):        |  \geq op.requirement |
|                      |                      | s\left\lbrack depth\ |
|                      | *op.requ             | _ max \right\rbrack: |
|                      | irements[depth_max]* |                      |
|                      |                      | :math:`piling\_ fea  |
|                      | *piling.max_depth*   | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ piling\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Pile sleeve diameter | *pil                 | .. math:: \text{     |
| (m)                  | ing.hammer_max_diam* | if\ piling}.hammer\_ |
|                      |                      |  max\_ diam \geq op. |
|                      | *pil                 | requirements\left\lb |
|                      | ing.hammer_min_diam* | rack obj\_ diameter\ |
|                      |                      | _ max \right\rbrack\ |
|                      | *op.requirement      |  \mathbf{\text{AND}} |
|                      | s[obj_diameter_max]* |                      |
|                      |                      | .. mat               |
|                      | *op.requirement      | h:: \text{\ \ \ \ \  |
|                      | s[obj_diameter_min]* | piling}.hammer\_ min |
|                      |                      | \_ diameter \leq op. |
|                      |                      | requirements\left\lb |
|                      |                      | rack obj\_ diameter\ |
|                      |                      | _ min \right\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`piling\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ piling\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | Maximum water depth  | .. math:: \text{if\  |
| equipment depth      | of cables (m):       |  burial}.max\_ depth |
| rating (m)           |                      |  \geq op.requirement |
|                      | *op.requ             | s\left\lbrack depth\ |
|                      | irements[depth_max]* | _ max \right\rbrack: |
|                      |                      |                      |
|                      | *burial.max_depth*   | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | Maximum cable        | .. math:: \text{i    |
| equipment cable      | diameter (mm):       | f\ burial}.max\_ cab |
| diameter (mm)        |                      | le\_ diam \geq op.re |
|                      | *op.requirements[    | quirements\left\lbra |
|                      | cable_diameter_max]* | ck cable\_ diameter\ |
|                      |                      | _ max \right\rbrack: |
|                      | *bu                  |                      |
|                      | rial.max_cable_diam* | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | *burial.cap          | .. math:: if\ buria  |
| equipment            | abilities_ploughing* | l.capabilities\_ plo |
| capabilities         |                      | ughing = \text{True} |
|                      |                      |                      |
|                      |                      | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Cable burial         | *burial.             | .. ma                |
| equipment Ploughing  | max_depth_ploughing* | th:: if\ burial.max\ |
| depth rating (m)     |                      | _ depth\_ ploughing  |
|                      | *op.requir           | \geq op.requirements |
|                      | ements[cable_depth]* | \left\lbrack cable\_ |
|                      |                      |  depth \right\rbrack |
|                      |                      |                      |
|                      |                      | :math:`burial\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ burial\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+

Vessel pre-selection
""""""""""""""""""""
+----------------------+----------------------+----------------------+
| **Requirement**      | **Inputs**           | **Function**         |
+======================+======================+======================+
| Vessel Combination   | *op.name*            | *See* Table 3.61     |
|                      |                      |                      |
|                      | *vc.type*            |                      |
+----------------------+----------------------+----------------------+
| Crane capabilities   | *op                  | .. math:: if\ ves    |
| [ton]                | .requirements[lift]* | sel.crane\_ capacity |
|                      |                      |  \geq op.requiremnts |
|                      | *ve.crane_capacity*  | \lbrack lift\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Dynamic positioning  | *                    | .                    |
|                      | op.requirements[dp]* | . math:: if\ vessel. |
|                      |                      | dp \geq op.requiremn |
|                      | *ve.dp*              | ts\lbrack dp\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Jack-up capabilities | *op.requ             | :math                |
|                      | irements[depth_max]* | :`if\ vessel.jup\_ c |
|                      |                      | apabilities = True`: |
|                      | Jack-up vessel legs  |                      |
|                      | operating depth (m): | :ma                  |
|                      |                      | th:`if\ vessel.jup\_ |
|                      | *                    |  max\_ water \geq \t |
|                      | ve.jup_capabilities* | ext{op.requirements} |
|                      |                      | \left\lbrack depth\_ |
|                      | *ve.jup_max_water*   |  max \right\rbrack`: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | :math:               |
|                      |                      | `else,\ vesse\_ feas |
|                      |                      | ible = \text{False}` |
|                      |                      |                      |
|                      |                      | .. math:             |
|                      |                      | : else,\ vessel\_ fe |
|                      |                      | asible = \text{True} |
+----------------------+----------------------+----------------------+
| Depth clearance [m]  | *op.requ             | .. math              |
|                      | irements[depth_min]* | :: c\_ ukc = UKC\tex |
|                      |                      | t{\_}\text{contigenc |
|                      | *ve.draft*           | y} \times \ ve.draft |
|                      |                      |                      |
|                      |                      | :math:`if\ vessel.d  |
|                      |                      | raft*c\_ ukc \leq \t |
|                      |                      | ext{op.requirements} |
|                      |                      | \left\lbrack depth\_ |
|                      |                      |  min \right\rbrack`: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math:             |
|                      |                      | : else,\ vesse\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| ROV capabilities     | *o                   | ..                   |
|                      | p.requirements[rov]* | math:: \text{if\ op. |
|                      |                      | requiremnts}\left\lb |
|                      | *ve.rov_ready*       | rack \text{rov} \rig |
|                      |                      | ht\rbrack = = "work" |
|                      |                      |                      |
|                      |                      | :math:               |
|                      |                      | `if\ vessels.rov\_ r |
|                      |                      | eady = \text{True}:` |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
+----------------------+----------------------+----------------------+
| Vessel type          | *vc.type*            | .. math:: if\ ve     |
|                      |                      | ssel.type = vc.type: |
|                      | *ve.type*            |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+
| Turntable storage    | *op.require          | .. ma                |
| [ton]                | ments[turn_storage]* | th:: if\ vessel.turn |
|                      |                      | \_ storage \geq op.r |
|                      | *vesturn_storage*    | equiremnts\lbrack tu |
|                      |                      | rn\_ storage\rbrack: |
|                      |                      |                      |
|                      |                      | :math:`vessel\_ fea  |
|                      |                      | sible = \text{True}` |
|                      |                      |                      |
|                      |                      | .. math::            |
|                      |                      |  else,\ vessel\_ fea |
|                      |                      | sible = \text{False} |
+----------------------+----------------------+----------------------+

Table 3.61 Default vessel combinations for the different operations in
CPX1

+-------+-------+-------+-------+-------+-------+-------+-------+
| *     | **T   | **    | *     | **    | **Tow | **    | **Su  |
| *Oper | ransp | No.** | *Main | No.** | ves   | No.** | pport |
| ation | ort** |       | ves   |       | sel** |       | ves   |
| n     |       |       | sel** |       |       |       | sel** |
| ame** |       |       |       |       |       |       |       |
+=======+=======+=======+=======+=======+=======+=======+=======+
| D     | *On   | *1*   | Prop  | *–*   | *–*   | *–*   | *–*   |
| evice | deck* |       | elled |       |       |       |       |
| in    |       |       | crane |       |       |       |       |
| stall |       |       | v     |       |       |       |       |
| ation |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
|       | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
|       | -tow* |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Colle | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| ction | deck* |       | elled |       |       |       |       |
| point |       |       | crane |       |       |       |       |
| in    |       |       | v     |       |       |       |       |
| stall |       |       | essel |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Found | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| ation | deck* |       | elled |       |       |       |       |
| In    |       |       | crane |       |       |       |       |
| stall |       |       | v     |       |       |       |       |
| ation |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| in    | deck* |       | L     |       |       |       |       |
| stall |       |       | aying |       |       |       |       |
| ation |       |       | V     |       |       |       |       |
| (e    |       |       | essel |       |       |       |       |
| xport |       |       |       |       |       |       |       |
| and   |       |       |       |       |       |       |       |
| a     |       |       |       |       |       |       |       |
| rray) |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Moo   | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| rings | deck* |       |       |       |       |       |       |
| in    |       |       |       |       |       |       |       |
| stall |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Su    | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| pport | deck* |       | elled |       |       |       |       |
| Stru  |       |       | crane |       |       |       |       |
| cture |       |       | v     |       |       |       |       |
| In    |       |       | essel |       |       |       |       |
| stall |       |       |       |       |       |       |       |
| ation |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| To    | *On   | 1     | CTV   | *–*   | *–*   | *–*   | *–*   |
| pside | deck* |       |       |       |       |       |       |
| Inspe |       |       |       |       |       |       |       |
| ction |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Under | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| water | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Mo    | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| oring | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Array | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| Cable | deck* |       | su    |       |       |       |       |
| Inspe |       |       | pport |       |       |       |       |
| ction |       |       | v     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| E     | *On   | 1     | Diver | *–*   | *–*   | *–*   | *–*   |
| xport | deck* |       | su    |       |       |       |       |
| Cable |       |       | pport |       |       |       |       |
| Inspe |       |       | v     |       |       |       |       |
| ction |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| evice | -tow* |       |       |       |       |       |       |
| Retr  |       |       |       |       |       |       |       |
| ieval |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| evice | -tow* |       |       |       |       |       |       |
| Re    |       |       |       |       |       |       |       |
| deplo |       |       |       |       |       |       |       |
| yment |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| D     | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| evice | deck* |       | elled |       |       |       |       |
| R     |       |       | crane |       |       |       |       |
| epair |       |       | v     |       |       |       |       |
| On    |       |       | essel |       |       |       |       |
| Site  |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Mo    | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| oring | deck* |       |       |       |       |       |       |
| Line  |       |       |       |       |       |       |       |
| R     |       |       |       |       |       |       |       |
| eplac |       |       |       |       |       |       |       |
| ement |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| R     | deck* |       | L     |       |       |       |       |
| eplac |       |       | aying |       |       |       |       |
| ement |       |       | V     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Cable | *On   | 1     | Cable | *–*   | *–*   | *–*   | *–*   |
| R     | deck* |       | L     |       |       |       |       |
| epair |       |       | aying |       |       |       |       |
|       |       |       | V     |       |       |       |       |
|       |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| d     |       |       | v     |       |       |       |       |
| evice |       |       | essel |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
|       | *Wet  | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
|       | -tow* |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| colle |       |       | v     |       |       |       |       |
| ction |       |       | essel |       |       |       |       |
| point |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| su    |       |       | v     |       |       |       |       |
| pport |       |       | essel |       |       |       |       |
| stru  |       |       |       |       |       |       |       |
| cture |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | AHTS  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       |       |       |       |       |       |
| oning |       |       |       |       |       |       |       |
| moo   |       |       |       |       |       |       |       |
| rings |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+
| Decom | *On   | 1     | Prop  | *–*   | *–*   | *–*   | *–*   |
| missi | deck* |       | elled |       |       |       |       |
| oning |       |       | crane |       |       |       |       |
| f     |       |       | v     |       |       |       |       |
| ounda |       |       | essel |       |       |       |       |
| tions |       |       |       |       |       |       |       |
+-------+-------+-------+-------+-------+-------+-------+-------+

Operation Computation
---------------------

In complexity level CPX1 of the operation computation functionality,
operations are not broken down into a sequence of activities. The
transit distance from port to site is estimated as a straight line
between the two coordinates. Net duration is calculated assuming a total
duration and estimating the number of trips from port to site for
transporting equipment. However, the remaining functionalities remain
the same as in the full complexity version described in Section 3.3.3.

Operation Calendarization
-------------------------

The operation calendarization functionality operates exactly in the same
way for both versions (full complexity and simplified) of the Logistics
and Marine Operations module. For more information see Section 3.3.4.

.. [1]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.

.. [2]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.

.. [3]
   This sequence is based on pre-defined precedence rules between
   operations, defining the overall order of the installation operations
   to carry out. Depending on the farm design, the final installation
   operation sequence is likely to not feature every single operation
   listed.











ED version
----------

The ED module comprises of two main functionalities, namely designing
and evaluating electrical network options. For the lowest complexity
level, CPX1, a simplified network design will be produced within the ED
module. This does not depend on an array layout or specific site parameters.
At this early stage, the design does not consider the spatial layout of
the array, only cable lengths. The main, high-level, network topology
constraints made are as follows:

-  Every network design has a collection point, which could either be a
   substation or a hub

-  The devices in the array are always connected in a radial layout

-  Floating devices are treated the same as fixed devices

In essence, all network designs output at CPX1 consider only the radial
layout.

.. image:: ../figures/network3.svg
    :alt: Radial network schematic

The following sections describe the inputs required to run the ED
module, the process flow of the design and evaluate functionalities, and
the outputs from the module at CPX1.

Inputs
------

The device and array level inputs for the ED module at CPX1 are given in
the three tables below, with required and optional inputs, plus default 
values used. 

It is assumed that the user will know the power rating of
each device and the number of devices in the array, or can fill in
appropriate numbers to get the total rated power of the array/farm.
Typical values for the distances between devices and from the farm to
shore may be given by the user, depending on technology type and size.
Nominal values could be suggested in the GUI, e.g. 100 m & 1 km.
Guidance on device voltage could also be offered based on typical
designs, e.g. a 100 kW device could be rated at 690 V or a 2 MW device
could be rated at 6.6 kV.

Depending on
the user choice, the onshore infrastructure may be included in the cost
analysis. The onshore infrastructure cost can be a user input or may be
estimated based on the farm size and the distance between the onshore
landing point (OLP) and the grid connection point (GCP).

The parameters *fac_export*, *fac_dev_dev* and *fac_dev_cp*, 
are internal to the ED module, used to obtain the
lengths of array and export cables at CPX1. These are the default values
for these parameters at the time of writing, however these may be
updated in future.


**Device and array level input data at CPX1**
 
.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: CPX1 required input data

   * - Parameter
     - Description 
     - Origin of the Data
     - Data Model 
     - Units
   * - power
     - The rated active power output of the OEC
     - ET module or user input
     - float
     - W
   * - voltage
     - The rated voltage of the OEC 
     - ET module or user input
     - float
     - V
   * - number_of_devices 
     - The number of devices in the array
     - EC module or user input
     - integer
     - -
   * - array_spacing
     - The separation between devices in the array, assumed to be equal in the x and in the y directions
     - EC module or user input
     - float
     - m
   * - distance_to_shore 
     - The distance between the deployment site and the OLP
     - SC module or user input
     - float
     - m
   * - onshore_infrastructure_flag
     - Control flag to decide whether to include the onshore infrastructure cost in the cost analysis
     - User input
     - bool
     - -

**Optional input data at CPX1**

.. list-table:: 
   :width: 100%
   :header-rows: 1
   :name: CPX1 optional input data

   * - Parameter
     - Description 
     - Origin of the Data
     - Data Model 
     - Units
   * - onshore_infrastructure_cost
     - Cost of onshore distribution/transmission lines and any onshore substation
     - User input
     - float
     - €
   * - onshore_distance
     - Distance between the onshore landing point and the nearest onshore substation
     - User input
     - float
     - m
   * - AEP_in
     - The annual energy production for the whole array, measured at the generator terminals without any network losses considered
     - ET module or user input
     - float
     - Wh
   * - cap_factor
     - Capacity factor of the array to be used in the analysis
     - User input
     - float
     - %

**Default parameter values at CPX1**

=========================== ================= =========
Parameter                   Default value     Units
=========================== ================= =========
onshore_infrastructure_cost 0.0               €
onshore_losses              0.0               %
cap_factor                  30                %
fac_export                  1.2               –
fac_dev_dev                 1.2               –
fac_dev_cp                  2.0               –
max_devices_per_radial      10                –
transformer_efficiency      99.47             %
=========================== ================= =========


Design and evaluation process
-----------------------------

This section describes the design constraints and the process flow used
to produce a simplified network design by the ED module at CPX1. The
overall design process is firstly to design the transmission system,
then the simplified array network, and finally evaluate the network
option produced as detailed in the following sections, and shown schematically below.

.. figure:: ../figures/ED-Process-Simple-1.svg
    :align: center

    Process flow of the design and evaluate functionalities at CPX1


Design of transmission system 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The process for the design of the transmission system at CPX1 is shown
below. The first step is the selection of an appropriate export
voltage for the array. The export voltage is specified using the design
limits guidance provided in the Table below, and is a function of the array
rated power (i.e.\ :math:`S_{max} = power \times number\_of\_devices`)
and the distance to shore :math:`L_{max}`. If the calculated export voltage
is the same or lower than the device voltage, then the device voltage is
selected. In this case, an offshore substation is not required, and the
collection point will be a “hub”. On the other hand, if the calculated
export voltage is higher than the device voltage, then an offshore
substation is required for the voltage transformation.

To account for an indirect cable route, the export cable length is
assumed to be :math:`= fac\_export \times distance\_to\_shore`,
wherein :math:`fac\_export` was set at 1.2.

A catalogue of generic cables with their cross-section areas,
resistances and current ratings is available in the module. The export
cable is selected from the catalogue for the export cable voltage
specified by the earlier step and the array power rating
:math:`(power \times number\_of\_devices)`.


.. figure:: ../figures/ED-Process-Simple-2.svg
    :align: center

    Process flow of the transmission system design at CPX1

**Estimated maximum power and distance capability of different voltage levels (design limits)**

============================== ============== ================
Transmission  voltage (kV)      |Smax| (MVA)   |Lmax| (km)
============================== ============== ================
0.4                            0.2–0.6            0.4–0.8
0.69                           0.4–1.0            0.6–1.3
3.3                            1.7–4.7            3.0–6.4
6.6                            3.4–9.4            5.9–12.8
10                             5.0–14             9–19
20                             10–28              18–38
30                             15–43              27–58
35                             18–50              31–68
66                             34–94              59–128
110                            57–157             99–213
============================== ============== ================

.. |Smax| replace:: S\ :sub:`max`
.. |Lmax| replace:: L\ :sub:`max`

.. note this table also used in full-design-process

Design of array network
~~~~~~~~~~~~~~~~~~~~~~~

The array network at CPX1 is always connected in a radial layout. The
division of devices between radials depends on the number of devices in
the array. For arrays with up to 5 devices, all the devices are
connected in a single radial. For arrays with between 6 and 10 devices,
the devices are distributed between two radials. For arrays with greater
than 10 devices, the devices are distributed between
:math:`(number\_of\_devices/10)` radials, rounded
up to the next integer (using the ``ceil()`` function). At present, a radial
can have a maximum of 10 devices. The split of devices between radials
in the network is stored in an internal list *n_dev_str*. The length of
this list gives the number of radials, while each element of the list
gives the number of devices in each radial.

The array cable length between devices
:math:`= fac\_dev\_dev \times array\_spacing`. 
The array cable length between the first device in a radial and the
collection point :math:`= fac\_dev\_cp \times array\_spacing`. The
values :math:`fac\_dev\_dev` and :math:`fac\_dev\_cp` are
constants, which at present have been set to 1.2 and 2 respectively.

Using these approximations, the total array cable length is evaluated
for the array network design.

The array cable is selected from the catalogue of generic cables for the
device voltage specified and the power rating required for the longest
radial in the network :math:`(max(n\_dev\_str) \times power)`. 
The same array cable selected is used across the array network.

.. figure:: ../figures/ED-Process-Simple-3.svg
    :align: center

    Process flow of the array design at CPX1


Evaluation of the network design
--------------------------------

The simplified network design is then evaluated technically in terms of
the AEP and losses, and economically in terms of estimated costs.

Technical evaluation
~~~~~~~~~~~~~~~~~~~~

To specify the power level of the devices in the array, which is
required to evaluate the power losses, the capacity factor of the array
needs to be determined. The capacity factor *cap_factor* and the AEP
input *AEP\ :sub:`in`* from the ET module or obtained as a user input, are
optional inputs to the ED module. If they are not provided, then a
typical capacity factor of *0.3* is assumed. If the ET module provides the
AEP input, then the capacity factor of the array is calculated using:

.. math::
  cap\_factor = \frac{AEP_{in}/({number\_of\_devices \times 8766)}}{power} 

The power output of the devices in the array is then evaluated by:

.. math::
    AEP_{out} = cap\_factor \times power.

The current through each section of the array network cable, connecting
device/collection point :math:`a` and :math:`b`, is then calculated
using:

.. math::
    i_{\text{ab}} = \frac{n \times power\_output}{\sqrt{3} voltage}

where, *n* depends on the position of the device in the radial. 
For the last device in a radial :math:`n = 1`, 
for the second to last :math:`n = 2` and so on, 
untill the device connected to the collection point 
for which :math:`n =` number of devices in the radial.

Based on the resistance per unit length :math:`r` of the selected array cable, 
the cable length :math:`l_{ab}` and the current :math:`i_{ab}`
through each array cable, the power loss in each array cable is evaluated using: 

.. math:: 
    power\_loss = 3 \times {i_{ab}}^2 \times l_{ab} \times r

The total power loss in the array network is obtained by summing the 
power loss seen in all the array cables.

At CPX1, devices are connected using array cables only, i.e. connectors
and umbilical cables are excluded.

If a transformer is required on the collection point, its efficiency is
used to calculate the power flow in the export cable. A transformer
efficiency of *99.47%*, which is approximately the average of the
efficiencies of transformers seen in [EC2014]_, has been used in the module.

The power output of the collection point transformer is taken to be the
power flow through the export cable. The power loss in the export cable
is calculated in the same fashion as array cables, which has been
described above. The transformer power output minus the power loss in
the export cable is the power delivered by the array at the OLP,
*array_power_output*, which is calculated using: 

.. math::
    array\_power\_output = \left( cap\_factor \times power \right) 
    \times number\_of\_devices - power\_loss\_array 
    - power\_loss\_transformer - power\_loss\_export

where :math:`power\_loss\_array`, :math:`power\_loss\_transformer` and 
:math:`power\_loss\_export` refer to the total power loss in the array 
network, CP transformer and the export cable as calculated before.

The energy delivered to the shore (AEP) math:`annual_yield` is calculated
from the power delivered at the OLP using: 

.. math:: annual\_yield = array\_power\_output \times 8766

From all the calculations completed, the network efficiency *annual_efficiency* is
evaluated using:

.. math:: annual\_efficiency = (annual\_yield/AEP_{in}) \times 100\%


Typical failure rates for the cables and transformers in the network,
obtained from [Djapic08]_, are also included for further analysis by 
subsequent modules.

Economic evaluation
~~~~~~~~~~~~~~~~~~~

Costs for components such as cables and CPs come from cost functions and
not a catalogue. These cost functions were obtained from [10].

The onshore infrastructure costs, if not provided as a user input, are
estimated based on just the array rated power and the length of the
onshore line required, based on [Ofgem15]_ [OWPB16]_ [NG19]_.

The total cost of the electrical system *total_cost* is calculated by
adding the cost of the four main parts of the network design: the array
network, the collection point, the export cable, and the onshore
infrastructure. A simplified cost of energy *coe_elec* representing only
the cost of the electrical infrastructure, is calculated using:

.. math:: coe\_elec = total\_cost / annual\_yield

.. note::
    Users will be notified that power flow solutions may not be run at this
    stage (although this is still to be implemented in the GUI).


Outputs
-------

At CPX1 the ED module outputs the results of the network design and
evaluation. The user will be informed of the array layout assumptions
used, cable lengths estimated, and the AEP and losses calculated in a
simplified manner. They will not be able to see the array layout
graphically as it is not based on device locations.

A simplified bill of materials (BOM) will be prepared with four subtotal
costs:

-  Total transmission network

-  Total array network

-  Total collection point

-  Total onshore infrastructure

A simplified hierarchy will be produced. The energy routes for each
device will comprise of one or more array cables, a collection point and
an export cable. The list of the outputs from the ED module at CPX1 
is shown below.

+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| Parameter                    | Description                                                                  | Data Model          | Units     |
+==============================+==============================================================================+=====================+===========+
|                              |                                                                              |                     |           |
| annual_efficiency            | Network efficiency over a year                                               | float               | %         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| annual_losses                | Total electrical losses over a year                                          | float               | Wh        |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| annual_yield                 | Array energy output over a year at the Grid Connection Point (GCP)           | float               | Wh        |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| array_power_output           | Real power delivered to the GCP for the capacity factor considered           | float               | MW        |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| array_voltage                | Array network voltage                                                        | float               | V         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| array_cable_total_length     | Total length of the array cables used in the design                          | float               | m         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| b_o_m_new                    | Network bill of materials: includes cost estimates of the array network,     | pandas DataFrame    | €         |
|                              | collection point, export cable and the onshore infrastructure                |                     |           |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| configuration                | Network configuration                                                        | string              | -         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| export_voltage               | Export cable voltage                                                         | float               | V         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| export_cable_total_length    | Total length of the export cable used in the design                          | float               | m         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| hierarchy_new                | Component-to-component connection relationship for reliability   analysis    | pandas DataFrame    | -         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| coe_elec                     | Simplified cost of energy for network design                                 | float               | €/kWh     |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| n_array_cables               | Number of array cables in the network                                        | integer             | -         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| n_cp                         | Number of collections points in the network                                  | integer             | -         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| n_export                     | Number of export cables in the network                                       | integer             | -         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+
|                              |                                                                              |                     |           |
| total_cost                   | Total cost of all components in the network                                  | float               | €         |
+------------------------------+------------------------------------------------------------------------------+---------------------+-----------+

