Example submodule
=================

You can use ``automodule`` to autogenerate docstrings from Python source code. 
For example:

.. code-block:: RST

   .. automodule:: dtop_stagegate.business.stage_gate_studies
      :members:
      :undoc-members:
      :show-inheritance:

See the Stage Gate folder for further examples. 
