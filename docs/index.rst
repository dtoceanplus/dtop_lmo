.. _lmo-home:

Logistics and Marine Operations
===============================

.. contents::

Introduction 
------------
The Logistics and Marine Operations module is responsible for designing logistical solutions for the installation, 
operation and maintenance (O&M), and decommissioning phases of ocean energy projects.
Logistic solutions consist of an operation plan and an optimal combination of vessels,
equipment and ports that minimise the costs of each operation individually,
reducing capital and operational expenditures simultaneously (CAPEX and OPEX).  


Structure
---------

This module's documentation is divided into four main sections:

- :ref:`lmo-tutorials` to give step-by-step instructions on using LMO for new users. 

- :ref:`lmo-how-to` that show how to achieve specific outcomes using LMO. 

- A section on :ref:`background, theory and calculation methods <lmo-explanation>` that describes how LMO works and aims to give confidence in the tools. 

- The :ref:`API reference <lmo-reference>` section documents the code of modules, classes, API, and GUI. 

.. toctree::
   :hidden:
   :maxdepth: 2

   tutorials/index
   how_to/index
   explanation/index
   reference/index

Functionalities
---------------

The main purpose of the Logistics and Marine Operations module is to
design logistical solutions for the installation, operation and
maintenance (O&M), and decommissioning phases of ocean energy projects.
Logistic solutions consist of an operation plan and an optimal
combination of vessels, equipment and ports that minimise the costs of
each operation individually, reducing capital and operational
expenditures simultaneously (CAPEX and OPEX).

For the different project lifecycle phases (installation, O&M,
decommissioning), the logistical solutions include:

1. **Infrastructure solutions** – optimal selection of vessels, ports
   and support equipment to carry out the
   installation/O&M/decommissioning operations

2. **Operation plans** – operation durations, weather contingencies,
   start dates, end dates.

3. **Operation costs** – cost of operations, including vessel chartering
   costs, fuel costs, port costs and equipment costs. These costs
   grouped into installation, maintenance and decommissioning

.. figure:: fig/fig1_functionalities.png
   :alt: Functionalities of the LMO module

   Functionalities of the LMO module

The module can either be run in simplified mode (complexity 1) or full
detail mode (complexity 2/3). Note there is no difference in the
logistic design process between complexity 2 and 3, but these have been
retained for consistency with other tools.

Workflow for using the LMO module
---------------------------------

The workflow for using the Logistics and Marine Operations module can be
summarised as 1) provide the first round of inputs, 2) provide a second
round of inputs after first intermediate calculations, 3) perform a
design, and 4) view the results, as shown in the figure below.

.. figure:: fig/fig2_workflow.png
   :alt: Workflow of the LMO module
   
   Workflow of the LMO module

Overview of LMO data requirements
---------------------------------

This section summarises the types of input data required to run the Logistics and Marine Operations module. Full details and data specifications are given in the how to guide on preparing data (Section 7.8.5).
The required and optional inputs to run the module are summarised in the tables below. Note that in integrated mode, the required inputs will come from three different sources:

#. :ref:`Inputs from the GUI <lmo-main-gui-inputs>`

#. :ref:`Inputs from External modules (MC, EC, ET, ED, SK) <lmo-main-external-inputs>`

#. :ref:`Inputs from the Catalogues<lmo-main-db-inputs>`


Summary of mandatory inputs:

+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|     Input Page     |                                Low complexity (cpx1)                                |                Full complexity (cpx2 & cpx3)                |
+====================+=====================================================================================+=============================================================+
| Project inputs     | Installation start month (mm/yyyy)                                                  | Installation start date (dd/mm/yyyy)                        |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Maintenance start month (mm/yyyy)                                                   | Maintenance start date (dd/mm/yyyy)                         |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Consider device repair at port (Bool)                                               | Consider device repair at port (Bool)                       |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Device fully submerged (Bool)                                                       | Device fully submerged (Bool)                               |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Operations maximum wave height (m)                                                  | Project lifetime (years)                                    |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Project lifetime (years)                                                            |                                                             |
+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+
| External inputs    | Device type (WEC/TEC)                                                               | Device type (WEC/TEC)                                       |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Device topology (fixed/floating)                                                    | Device topology (fixed/floating)                            |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Device dimensions (m)                                                               | Device dimensions (m)                                       |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Device mass (kg)                                                                    | Device mass (kg)                                            |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Number of devices                                                                   | Number of devices                                           |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Farm layout                                                                         | Farm layout                                                 |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Energy transformation hierarchy                                                     | Energy transformation hierarchy                             |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Mass of PTO components                                                              | Mass of PTO components                                      |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Cost of PTO components                                                              | Cost of PTO components                                      |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | PTO rated power                                                                     | PTO rated power                                             |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | PTO failure rates                                                                   | PTO failure rates                                           |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Station keeping hierarchy                                                           | Station keeping hierarchy                                   |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Anchor type, number, mass dimensions, soil type, failure rates and costs            | Anchor type, number, mass dimensions, soil type,            |
|                    |                                                                                     | failure rates and costs                                     |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Mooring line type, number, mass, dimensions, soil type, failure rates and costs     | Mooring line type, number, mass dimensions, soil type,      |
|                    |                                                                                     | failure rates and costs                                     |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Foundation type, number, mass, dimensions, soil type, burial depth,                 | Foundation type, number, mass dimensions, soil type,        |
|                    | failure rates, and costs                                                            | burial depth, failure rates, and costs                      |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Energy delivery hierarchy                                                           | Energy delivery hierarchy                                   |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Collection point type, number, mass, dimensions, soil type, failure rates and costs | Collection point type, number, mass dimensions, soil type,  |
|                    |                                                                                     | failure rates, and costs                                    |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Cable type, length, burial depth, route, soil type, burial method,                  | Cable type, length, burial depth, route, soil type,         |
|                    | cable protections, connector type, cable costs                                      | burial method, cable protections, connector type,           |
|                    |                                                                                     | cable costs                                                 |
+                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Site bathymetry                                                                     | Site bathymetry                                             |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Site met-ocean timeseries (Hs)                                                      | Site met-ocean timeseries (Hs, Ws, Cs)                      |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Site seabed characteristics                                                         | Site seabed characteristics                                 |
+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+
| Project lifecycle  | Installation                                                                        | Installation                                                |
| phases             +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Maintenance                                                                         | Maintenance                                                 |
|                    +-------------------------------------------------------------------------------------+-------------------------------------------------------------+
|                    | Decommissioning                                                                     | Decommissioning                                             |
+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+
| Phase requirements | N/A                                                                                 | Installation/Maintenance/Decommissioning phase requirements |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Consider ROV/Divers                                         |
+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+
| Operation methods  | N/A                                                                                 | Device transportation method                                |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Device load-out method                                      |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Pile transportation method                                  |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Pile load-out method                                        |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Anchors load-out method                                     |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Collection point transportation method                      |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Collection point load-out method                            |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Cable burial method                                         |
|                    |                                                                                     +-------------------------------------------------------------+
|                    |                                                                                     | Cable landfall method                                       |
+--------------------+-------------------------------------------------------------------------------------+-------------------------------------------------------------+

Summary of optional inputs:

+--------------------+-----------------------+---------------------------------------------------------------+
|     Input Page     | Low complexity (cpx1) |                 Full complexity (cpx2 & cpx3)                 |
+====================+=======================+===============================================================+
| Project inputs     | N/A                   | Consider device towing draft (Bool)                           |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Device towing draft (m)                                       |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Safety factor for vessel selection                            |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Fuel price (€/ton)                                            |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Specific Fuel Oil Consumption (g/kWh)                         |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Average vessel load factor                                    |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Weather window statistics                                     |
|                    |                       +---------------------------------------------------------------+
|                    |                       | Vessel statistics                                             |
+--------------------+-----------------------+-------------+-------------------------------------------------+
| Phase requirements | N/A                   | Disregard   | No previous experience in MRE projects          |
|                    |                       + ports       +-------------------------------------------------+
|                    |                       | if          | Insufficient terminal area                      |
|                    |                       +             +-------------------------------------------------+
|                    |                       |             | Insufficient terminal quay load bearing capacity|
|                    |                       +             +-------------------------------------------------+
|                    |                       |             | Insufficient crane capacity at the terminal     |
|                    |                       +             +-------------------------------------------------+
|                    |                       |             | Outside radius from site                        |
+--------------------+-----------------------+-------------+-------------------------------------------------+



User inputs from the GUI
^^^^^^^^^^^^^^^^^^^^^^^^

.. _lmo-main-gui-inputs:

The User will set basic information about the LMO study and provide the main project inputs, device and subsystem characteristics, as well as operation methods and preferences, depending on the complexity level and technology.

- **Study**: Name, description, complexity and standalone mode (yes/no);

- **Project inputs**: project installation date, maintenance start date, consider repair at port (yes/no), device towing draft, project lifetime, vessel fuel consumption calculation parameters, vessel statistics

- 	**Project lifecycle phases to consider**: Installation, Maintenance, Decommissioning;

- 	**Phase requirements**: Installation, maintenance, and decommissioning preferences. Port selection preferences.

-	**Operation methods**: Operation methods to consider, namely, device load-out method, cable landfall method, etc. 

Inputs from External modules
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. _lmo-main-external-inputs: 

In order to run the Logistics and Marine Operations module, different inputs from external modules are required:

1. Site bathymetry and environmental conditions, from the Site Characterisation module;	

2. Device dimensions, mass, and technology type from the Machine Characterisation module;

3.	Number of devices and farm layout from the Energy Capture module;

4.	Hierarchy file and PTO design inputs from the Energy Transformation module;

5.	Hierarchy file and energy grid design inputs from the Energy Delivery module;

6.	Hierarchy file and station keeping design inputs (moorings, foundations) from the Station Keeping module;

In standalone mode, these inputs will be uploaded to the LMO study through five independent json files. All external modules input studies must have the same complexity level.

Catalogue inputs
^^^^^^^^^^^^^^^^

.. _lmo-main-db-inputs:

Apart from external inputs and User inputs, the Logistics and Marine Operations module 
uses databases of vessels, port terminals and equipment, as well as operations and
activities data stored in a catalogue. These parameters may be changed by directly 
modifying the catalogue.

+----------------------------------------------+-------------+-------+
|               Catalogues                     | Data origin | Units |
+==============================================+=============+=======+
| Port terminals                               |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Vessel: Vessel combinations                  |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Vessel: Vessel clusters                      |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Equipment: Cable burial                      |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Equipment: Piling                            |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Equipment: ROVs                              |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Equipment: Divers                            |  Catalogue  |   -   |
+----------------------------------------------+-------------+-------+
| Operations and activities                    |  Catalogue  |   -   |
| (Installation, Maintenance, Decommissioning) |             |       |
+----------------------------------------------+-------------+-------+