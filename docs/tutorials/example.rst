.. _lmo-tutorial-example: 

Example tutorial
================

Give a brief introduction to the tutorial. 
Talk about what it will achieve. 
Can divide each page into "sub-tutorials" as below. 

Part one of tutorial
--------------------

#. You must use numbered steps in the tutorial. 

#. Provide explicit steps to the user on how to perform a certain task. 

Part two of tutorial
--------------------

#. Maybe you won't need to divide into additional sections.

#. But it can be useful. 

#. A user who has never seen the module before should be able to complete a tutorial from start to finish. 

#. Use the ``inline reference`` command when referring to buttons. 
   For example, click the ``Create a Study`` button to create a study. 
