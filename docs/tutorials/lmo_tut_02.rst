.. _lmo_tut_02: 

Using the Logistics and Marine Operations module at low complexity in standalone mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. warning::
    This content is mostly from D5.8 Deliverable,
    and still to be reviewed and updated

At low complexity (CPX1), the LMO module was developed to provide
simplified logistic designs, requiring minimum inputs from the User and
other design modules while minimizing computation times. The LMO GUI is
divided into four stages: i) Project, ii) Operations, iii) Calculations,
and iv) Results. In the first page, “Project”, inputs are grouped into
four input categories: i) Project inputs, which includes fundamental
project parameters and device characteristics, ii) Other module inputs,
which groups all the inputs related to farm subsystems from other
modules run upstream and that are required to run LMO , iii) Site
inputs, which consists of the input file from Site Characterisation
related to the lease area coordinates, bathymetry and environmental
timeseries, and iv) “Project lifecycle phases”, where the user is able
to select which phases to analyse (i.e. installation, maintenance,
and/or decommissioning).

1)  If required, create a new complexity level 1 study, as described in
    :ref:`Creating a new standalone LMO study <lmo_tut_01>`.

2)  From the list of studies, click ‘Open’ to start working on the
    complexity level 1 study

3)  Click on the “Add” button in front of the “Project inputs” tab and:

    a. Select an Installation start date *[required*\  [1]_\ *].*

    b. Select a Maintenance start date *[required*\  [2]_\ *].*

    c. Specify whether device repair at the port is to be
       considered [3]_ *[optional]*

    d. Specify whether the device is fully submerged [4]_ *[optional]*

    e. Specify the maximum significant wave height (Hs) [5]_
       *[required]*

    f. Specify the number of project years *[required]*

    g. Click “Save”.

    h. If successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

    i. To modify or visualise the introduced Project inputs, the
       “Update” button is now available. Otherwise, the User may just
       delete these project inputs by pressing “Delete”.

4)  Click on the “Add” button in front of the “Other module inputs” tab
    and:

    a. Confirm that you are on the MC module page. Click the upload
       button to introduce the MC input file. *[required]*

    b. On the module horizontal tab, select the EC module and upload the
       EC module input file. *[required]*

    c. Repeat the previous steps for each module. In the end, press the
       “Create” button *[required]*

    d. A confirmation pop-up message will appear. Press confirm
       *[required]*

    e. If successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

5)  Click on the “Add” button in front of the “Site inputs” tab and:

    a. Click on the upload button to introduce the Site data, as
       produced by the Site Characterisation module. *[required]*

    b. Press the “Create” button. *[required]*

    c. A loading sign will appear on top of the create button. If
       successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

6)  Specify which project lifecycle phases should be analysed in the
    current test.

    a. Press installation for simulating the installation phase

    b. Press maintenance for simulating the maintenance phase

    c. Press decommissioning for simulating the decommissioning
       phase [6]_

7)  Press the “Save and Lock". A loading sign will appear on top of the
    “Save and Lock” button. Otherwise, an error message will pop-up.

8)  Once loading has been completed, the input tabs will be locked, not
    allowing for further changes. In case the inputs are to be changed,
    press the “Unlock” button. This will erase inputs that may have been
    introduced downstream in the next pages (Operations or
    Calculations). Then, to advance again, Step 7 must be repeated.

9)  Once loading has been completed, the “Next” button will be unlocked.
    Press it to advance to the next page. *[required]*

10) The lifecycle phases selected on the “Project” page are now
    displayed. If all three phases were selected, then:

    a. Press the “Generate” button in front of the “Generate
       Installation operations”. A loading sign will appear on top of
       the Generate button *[required]*

    b. Press the “Generate” button in front of the “Generate Maintenance
       operations”. A loading sign will appear on top of the Generate
       button *[required]*

    c. Press the “Generate” button in front of the “Generate
       Decommissioning operations”. A loading sign will appear on top of
       the Generate button. *[required]*

    d. If successful, the “Generate” buttons will change to “Delete”
       buttons, which may be pressed to delete the generated operations.
       Otherwise, an error message will be shown.

    e. When every operation has been generated (every “Generate” button
       was replaced by a “Delete” button), press “Next”.

11) The User will be redirected to the Calculations page.

    a. Press “Compute installation results” and wait. If successful, a
       confirmation message with the computation time will be presented,
       and the “Compute installation results” button will be replaced by
       a red “Delete installation results” button. Otherwise, an error
       message will be presented. *[required]*

    b. Press “Compute maintenance results” and wait. If successful, a
       confirmation message with the computation time will be presented
       and the “Compute maintenance results” button will be replaced by
       a red “Delete maintenance results” button. Otherwise, an error
       message will be presented. *[required]*

    c. Press “Compute decommissioning results” and wait. If successful,
       a confirmation message with the computation time will be
       presented and the “Compute decommissioning results” button will
       be replaced by a red “Delete decommissioning results” button.
       Otherwise, an error message will be presented. *[required]*

    d. Finally, press “View results”

12) The User will be redirected to the results page.

    a. Press to view the Installation Solution. This will redirect to
       the installation results page.

    b. Press to view the Maintenance Solution. This will redirect to the
       Maintenance results page.

    c. Press to view the Decommissioning Solution. This will redirect to
       the instal Decommissioning results page.

.. [1]
   Selecting an installation start month is only required in case the
   installation phase is to be analysed.

.. [2]
   Selecting a maintenance start month is only required in case the
   maintenance phase is to be analysed.

.. [3]
   In case this option is not selected, repair on site shall be
   considered.

.. [4]
   In case device is fully submerged, inspections to PTOs shall be
   carried out using ROVs or divers.

.. [5]
   Default: 2.5m

.. [6]
   The decommissioning phase can only be simulated if the installation
   phase also was selected.
