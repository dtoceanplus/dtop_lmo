.. _lmo_tut_01: 

Creating a new Logistics and Marine Operations study in standalone mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Once logged into the server, the next step is to create a new study
within the Logistics and Marine Operations module. 

.. tip::
   Since multiple users across multiple  organisations may be 
   simultaneously accessing the tool on the server,
   **please add your organisation’s name in the name of the study you
   create**. This is to ensure that all users work on independent studies
   and are not editing the same study at the same time.

1. In the left menu, select ‘Create project’.

2. Fill in an appropriate title and description to identify your study,
   then select the appropriate complexity level. 

   - Complexity level 1 can    be used to get a quick estimate with minimal inputs.

   - Complexity levels 2 & 3 have the same functionalities, although inputs are expected to have different uncertainties.

3. Click ‘create’ to save these inputs and return to the list of
   studies.

4. From the list of studies, click ‘Open’ to start working on a study,
   ‘Edit’ to change the name or description, or ‘Delete’ to permanently
   remove a study. The status progress bar denotes the percentage of
   inputs that have already been filled in order to run the module.

.. note::
   This tutorial will be updated once studies are centrally
   managed, but this reflects the current version of the tool.
