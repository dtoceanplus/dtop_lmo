.. _lmo_tut_03: 

Using the Logistics and Marine Operations module at medium/high complexity in standalone mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. warning::
    This content is mostly from D5.8 Deliverable,
    and still to be reviewed and updated

In the case of higher data availability, the Logistics and Marine
Operations module can be run at a higher complexity level (CPX2 or
CPX3), to provide more detailed assessments. In these complexities, the
financial assessment functionality is available. In this case, inputs
are grouped into four input categories: i) Project inputs, which
includes fundamental project parameters and device characteristics, ii)
Other module inputs, which groups all the inputs related to farm
subsystems from other modules run upstream and that are required to run
LMO, iii) Site inputs, which consists of the input file from Site
Characterisation related to the lease area coordinates, bathymetry and
environmental timeseries, and iv) “Project lifecycle phases”, where the
user is able to select which phases to analyse (i.e. installation,
maintenance, and/or decommissioning).

1)  If required, create a new complexity level 3 study, as described in
    tutorial 1.

2)  From the list of studies, click ‘Open’ to start working on the
    complexity level 3 study

3)  Click on the “Add” button in front of the “Project inputs” tab and:

    a. Select an Installation start day *[required*\  [1]_\ *].*

    b. Select a Maintenance start day *[required*\  [2]_\ *].*

    c. Specify whether device repair at the port is to be
       considered [3]_ *[optional]*

    d. Specify whether the device is fully submerged [4]_ *[optional]*

    e. In case the device may be towed to the site, and the towing draft
       is significantly different from the device draft in resting
       conditions, then the towing draft may be specified:

       i.  Press the consider checkbox *[optional]*

       ii. Specify the device towing draft in meters. *[optional]*

    f. Specify the number of project years *[required]*

    g. Specify the Safety factor for vessel selection. *[optional]*

    h. Specify the vessel Fuel price to calculate fuel costs.
       *[optional]*

    i. Specify Specific fuel oil consumption. *[optional]*

    j. Specify the Average vessel load factor. *[optional]*

    k. Press the statistics tab

       i.  To modify the weather window statistic parameter, select the
           dropdown table. For this tutorial, leave it as Median (P50).

       ii. To modify the vessel statistics parameter, select the
           dropdown table. For this tutorial, leave it as Median (P50).

    l. Click “Create”.

    m. If successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

    n. To modify or visualise the introduced Project inputs, the
       “Update” button is now available. Otherwise, the User may just
       delete these project inputs by pressing “Delete”.

4)  Click on the “Add” button in front of the “Other module inputs” tab
    and:

    a. Confirm that you are in the MC module page. Click the upload
       button to introduce the correct MC input file. *[required]*

    b. On the module horizontal tab, select the EC module, and upload
       the correct EC module input file. *[required]*

    c. Repeat the previous steps for each module. In the end, press the
       “Create” button *[required]*

    d. A confirmation pop-up message will appear. Press confirm
       *[required]*

    e. If successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

5)  Click on the “Add” button in front of the “Site inputs” tab and:

    a. Click on the upload button to introduce the Site data, as
       produced by the Site Characterisation module. *[required]*

    b. Press the “Create” button. *[required]*

    c. A loading sign will appear on top of the create button. If
       successful, the User will be redirected to the Project page.
       Otherwise, an error message will pop-up.

6)  Specify which project lifecycle phases should be analysed in the
    current test.

    a. Press installation for simulating the installation phase

    b. Press maintenance for simulating the maintenance phase

    c. Press decommissioning for simulating the decommissioning
       phase [5]_

7)  Press the “Save and Lock". A loading sign will appear on top of the
    “Save and Lock” button. Otherwise, an error message will pop-up.

8)  Once loading has been completed, the input tabs will be locked, not
    allowing for further changes. In case the inputs are to be changed,
    press the “Unlock” button. This will erase inputs that may have been
    introduced downstream in the next pages (Operations or
    Calculations). Then, to advance again, Step 7 must be repeated.

9)  Once loading has been completed, the “Next” button will be unlocked.
    Press it to advance to the next page. *[required]*

10) The lifecycle phases selected on the “Project” page are now
    displayed. If all three phases were selected, then:

    a. Press the “Generate” button in front of the “Generate
       Installation operations”. A loading sign will appear on top of
       the Generate button *[required]*

    b. Press the “Generate” button in front of the “Generate Maintenance
       operations”. A loading sign will appear on top of the Generate
       button *[required]*

    c. Press the “Generate” button in front of the “Generate
       Decommissioning operations”. A loading sign will appear on top of
       the Generate button. *[required]*

    d. If successful, the “Generate” buttons will change to “Delete”
       buttons, which may be pressed to delete the generated operations.
       Otherwise, an error message will be shown.

    e. When every operation has been generated (every “Generate” button
       was replaced by a “Delete” button), press “Next”. The operation
       methods button will also be unlocked.

    f. In order to specify the optional phase requirements, press the
       “View” button. The User will be redirected to the Phase
       requirements page *[optional]*

       i.  For each tab of the previously selected lifecycle phases to
           be considered (Installation, Maintenance, Decommissioning):

           1. Specify whether ROVs or Divers should be considered to
              support subsea operations *[optional]*

           2. Specify discarding criteria for the port selection
              process:

              a. To discard ports that were not identified in the
                 terminal catalogue has to have previous experience in
                 MRE projects, select the respective checkbox
                 *[optional]*

              b. To discard ports with insufficient terminal area to
                 accommodate the largest component, select the
                 respective checkbox *[optional]*

              c. To discard ports with insufficient quay loadbearing
                 capacity, select the respective checkbox *[optional]*

              d. To discard ports with insufficient crane capacity to
                 lift the heaviest component [6]_ (in case lift loadouts
                 are required), select the respective checkbox
                 *[optional]*

              e. To discard ports too far away, specify a radius centre
                 on the site location, outside which the ports will be
                 disregarded [7]_ *[optional]*

           3. Repeat the same process for the maintenance and
              decommissioning lifecycle phases.

       ii. In the end, press the button “Submit all”. If successful, the
           User will be redirected to the Operations page. Otherwise, an
           error message will pop-up.

    g. In order to specify the operation methods, press the “View”
       button. The User will be redirected to the Operation Methods page
       *[required]*

       i.   In the Devices tab:

            1. Specify the load-out method and the transportation method
               from the respective dropdown menus.

       ii.  In the Foundations tab, in case of Foundations exist in the
            project:

            1. Specify the foundation load-out and transportation
               methods from the respective dropdown menus.

            2. Specify the piling method for installing piles (if piles
               are to be installed)

       iii. In the “Anchors and Moorings” tab, in case Moorings and
            anchors exist in the project:

            1. Specify the Anchor and Moorings load-out method from the
               dropdown menus.

       iv.  In the Collection Points tab, in case of Collection Points
            exist and require an individual operation in the project:

            1. Specify the collection point load-out and transportation
               methods from the respective dropdown menus.

       v.   In the Cables tab:

            1. Specify the cables burial-method and landfall method,
               which will affect the installation operations from the
               respective dropdown menus.

       vi.  In the end, press the button “Submit all”. If successful,
            the User will be redirected to the Operations page.
            Otherwise, an error message will pop-up. *[required]*

11) The User will be redirected to the Calculations page.

    a. Press “Compute installation results” and wait. If successful, a
       confirmation message with the computation time will be presented,
       and the “Compute installation results” button will be replaced by
       a red “Delete installation results” button. Otherwise, an error
       message will be presented. *[required]*

    b. Press “Compute maintenance results” and wait. If successful, a
       confirmation message with the computation time will be presented,
       and the “Compute maintenance results” button will be replaced by
       a red “Delete maintenance results” button. Otherwise, an error
       message will be presented. *[required]*

    c. Press “Compute decommissioning results” and wait. If successful,
       a confirmation message with the computation time will be
       presented, and the “Compute decommissioning results” button will
       be replaced by a red “Delete decommissioning results” button.
       Otherwise, an error message will be presented. *[required]*

    d. Finally, press “View results”

12) The User will be redirected to the results page.

    a. Press to view the Installation Solution. This will redirect to
       the installation results page.

    b. Press to view the Maintenance Solution. This will redirect to the
       Maintenance results page.

    c. Press to view the Decommissioning Solution. This will redirect to
       the instal Decommissioning results page.

.. [1]
   Selecting an installation start date is only required in case the
   installation phase is to be analysed.

.. [2]
   Selecting a maintenance start date is only required in case the
   maintenance phase is to be analysed.

.. [3]
   In case this option is not selected, repair on site shall be
   considered.

.. [4]
   In case device is fully submerged, inspections to PTOs shall be
   carried out using ROVs or divers.

.. [5]
   The decommissioning phase can only be simulated if the installation
   phase also was selected.

.. [6]
   Bear in mind that onshore cranes may be externally hired so this may
   not be a strict port terminal requirement.

.. [7]
   Specifying port radius and reducing the total number of ports to be
   analysed will speed up calculations
