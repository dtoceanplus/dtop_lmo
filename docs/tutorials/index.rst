.. _lmo-tutorials:

*********
Tutorials
*********

Below is a set of tutorials that guide the user through each of the main functionalities of the Logistics and Marine Operations tool.
They are intended for those who are new to the Logistics and Marine Operations tool.
It is recommended to follow the tutorials in the suggested order listed below, as certain tutorials are dependent on others. 

**Using the Logistics and Marine Operations tool in integrated mode**

These show how to use Logistics and Marine Operations in integrated mode along with the other
DTOceanPlus Deployment Design tools. Most of the input parameters come from other modules. 

- :ref:`Using LMO at low complexity standalone for RM3 wave example <lmo_tut_02>`

- :ref:`Using LMO at medium complexity standalone for RM1 tidal example <lmo_tut_03>`

**Using the Logistics and Marine Operations tool in standalone mode**

These show how to use Logistics and Marine Operations in standalone mode where the user needs to enter
all input parameters for the design process. 

- :ref:`Creating a new standalone LMO study <lmo_tut_01>`


.. toctree::
   :maxdepth: 1
   :hidden:

   lmo_tut_01
   lmo_tut_02
   lmo_tut_03
