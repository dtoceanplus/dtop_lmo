# -*- coding: utf-8 -*-
"""
Created on Wed Apr 17 17:27:20 2019

@author: Lamaral
"""
import pandas as pd
import networkx as nx
import os

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_terminals

# True to print logs
PRINT_LOGS = True


class Map_Graph():
  def __init__(self,
               graph: nx.Graph(),
               lat_min: float, lat_max: float,
               lon_min: float, lon_max: float,
               grid_resolution: tuple):
    self.graph = graph
    self.limits = {}
    self.limits['lat'] = (lat_min, lat_max)
    self.limits['lon'] = (lon_min, lon_max)
    self.grid_res = grid_resolution

def create_graph(np, nx, time, tqdm,
                 graph_name, basemap, map_limits, mesh_res):
    
    import geopy.distance as gp_dist
    
    time_st = time.time()
    if PRINT_LOGS is True:
      print('Graph creation module')
    
    b_map_resolution = basemap.resolution
    file_name = 'maps_graphs//Graph_' + graph_name + '_' + \
                    str(mesh_res[0]) +'x'+ str(mesh_res[1]) + '_' + \
                    b_map_resolution
    
    try:
      map_graph = nx.read_gpickle(file_name + '.gpickle')
      matrix_water = np.load(file_name + '.npy')
      if PRINT_LOGS is True:
        print('\tGraph imported.')
        
    except:      
      G = nx.Graph()
      
      matrix_water = np.zeros([mesh_res[0], mesh_res[1]])   # Only for visualization porposes
      
      # Define the degree steps for the mesh
      lat_deg_step = (map_limits[0][1]-map_limits[0][0]) / mesh_res[0]
      lon_deg_step = (map_limits[1][1]-map_limits[1][0]) / mesh_res[1]
      
      # Download the list of terminals and its coordinates
      try:
        df_terminals = get_catalog_terminals()
      except ConnectionError:
        path_catalogue_terminals = os.path.join(os.getcwd(), 'catalogues', 'Terminals.csv')
        df_terminals = pd.read_csv(path_catalogue_terminals, sep=',', encoding="ISO-8859-1")

      df_coords = df_terminals.loc[:,['latitude','longitude']]
      df_coords.insert(2, 'x', np.nan)
      df_coords.insert(3, 'y', np.nan)
      # Convert lat and lon to x an y
      for idx, row in df_coords.iterrows():
        lat = row['latitude']
        lon = row['longitude']
        df_coords.at[idx, 'x'] = np.floor((lon - map_limits[1][0]) / lon_deg_step)
        df_coords.at[idx, 'y'] = np.floor((lat - map_limits[0][0]) / lat_deg_step)
      
      count_edges = 0
      for x in range(0,mesh_res[0]):
          if PRINT_LOGS is True:
            print('\tstep %d of %d' % (x, mesh_res[0]))
          # node definition - x
          node_x = 'x'+str(x)
          # node longitud
          node_lon = map_limits[1][0] + x*lon_deg_step + (lon_deg_step/2)
          
          # run neighbour nodes
          if x > 0 and x < mesh_res[0] - 1:
              neig_x_ini = x-1
              neig_x_fin = x+1
          else:
              if x == 0:
                  neig_x_ini = x
                  neig_x_fin = x+1
              elif x < mesh_res[0]-1:
                  neig_x_ini = x-1
                  neig_x_fin = x
      
          for y in range(mesh_res[1]-1,-1,-1):
              # node definition - y
              node_y = 'y'+str(y)
              # node longitud
              node_lat = map_limits[0][0] + y*lat_deg_step + (lat_deg_step/2)
              
              node_def = node_x+node_y
              node_coord = (node_lat,node_lon)
              
              is_terminal = False
              # If x and y are in terminals dataframe: it is water
              for idxxy, rowxy in df_coords.iterrows():
                if rowxy['x'] == x and  rowxy['y'] == y:
                  df_coords.drop(index=idxxy, inplace=True)
                  is_terminal = True
                  break
              if is_terminal is False:
                xpt, ypt = basemap(node_lon,node_lat)    # convert to projection map
                b_node_land = basemap.is_land(xpt,ypt)
              else:
                b_node_land = False
              
              if b_node_land == False:    # If the node is in the sea
                  matrix_water[mesh_res[1]-1-y,x] = 1
#                    print('%s %s' % (node_lat,node_lon))
                  # run neighbour nodes
                  if y > 0 and y < mesh_res[1]-1:
                      neig_y_ini = y-1
                      neig_y_fin = y+1
                  else:
                      if y == 0:
                          neig_y_ini = y
                          neig_y_fin = y+1
                      elif y == mesh_res[1]-1:
                          neig_y_ini = y-1
                          neig_y_fin = y
                          
                  for x_neig in range(neig_x_ini,neig_x_fin+1):
                      # neighbour longitud
                      if x_neig < x:
                          neig_lon = node_lon - lon_deg_step
                      elif x_neig > x:
                          neig_lon = node_lon + lon_deg_step
                      elif x_neig == x:
                          neig_lon = node_lon
                      
                      for y_neig in range(neig_y_ini,neig_y_fin+1):
                          if x_neig == x and y_neig == y:
                              # We don't want to create an edge from a node to itself
                              pass
                          else:
                              # neighbour latitud
                              if y_neig < y:
                                  neig_lat = node_lat - lat_deg_step
                              elif y_neig > y:
                                  neig_lat = node_lat + lat_deg_step
                              elif y_neig == y:
                                  neig_lat = node_lat

                              # Neighbour coordinates
                              neig_coord = (neig_lat,neig_lon)

                              xpt_n, ypt_n = basemap(neig_lon,neig_lat)     # convert to projection map
                              b_neig_land = basemap.is_land(xpt_n,ypt_n)

                              if b_neig_land == False:
                                  weight_neig = gp_dist.great_circle(node_coord,
                                                                     neig_coord).m
                                  weight_neig = round(weight_neig)

                                  node_neig_def = 'x'+str(x_neig) + 'y'+str(y_neig)

                                  # Check if the edge was already created
                                  if G.has_edge(node_def,node_neig_def) == False:
                                      # If there is no edge, create it
                                      #   print('%s - > %s' % (node_def, node_neig_def))
                                      G.add_edge(node_def, node_neig_def,
                                                 weight=weight_neig)
                                      count_edges += 1

      # Save graph as a Pickle
      map_graph = Map_Graph(G,
                            map_limits[0][0], map_limits[0][1],
                            map_limits[1][0], map_limits[1][1],
                            tuple(mesh_res))
      nx.write_gpickle(map_graph, file_name + '.gpickle')
      np.save(file_name + '.npy', matrix_water)

      if PRINT_LOGS is True:
        print('\tGraph created.')
        print('\t\tNodes created: %d' % sum(sum(matrix_water)))
        print('\t\tEdges created: %d' % count_edges)

    if PRINT_LOGS is True:
      print('\tElapsed time: %s s\n\n' % (round(time.time()-time_st,3)))
    
    return map_graph, matrix_water
    