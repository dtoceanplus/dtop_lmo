# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 12:49:34 2021
MonteCarlo_20210309
@author: Francisco Fonseca
"""

import pandas as pd
import numpy as np
import math as mt
import datetime
import warnings
import numpy as np
import random
import os


def f_montecarlo(data_panda: pd.DataFrame, ts_percent_dec: float):
  """Consider different years first

  Args:
      data_panda (pd.DataFrame): Timeseries Panda DataFrame
      ts_percent_dec (float): Total percentage (decimal) of timesteps to analyse of each month

  Returns:
    ts_ids: list - Index of panda hindcast timeseries to consider in the analysis as operation starts
    data: dictionary- Dictionary including:
                                - Timeseries per month
                                - Original ids of timeseries
                                - Number of original ids
                                - Number of reduced ids, based on input percentage (ts_percent_dec)
                                - list of reduced ids
  """
  data_panda['month'] = data_panda['month'].astype(int)
  data_panda['year'] = data_panda['year'].astype(int)
  data_panda['day'] = data_panda['day'].astype(int)
  years = data_panda['year'].unique()
  months = [int(x) for x in data_panda['month'].unique()]

  data = {
      1: "",
      2: "",
      3: "",
      4: "",
      5: "",
      6: "",
      7: "",
      8: "",
      9: "",
      10: "",
      11: "",
      12: ""
  }
  ids_list_reduced = list()   # empty list of total timestep ids

  for m in months:
    data_m         = data_panda.loc[data_panda['month']==m]  #unnecessary    
    ids_orig       = list(data_m.index)               #list of ids from original timeseries in month m
    n_ids_orig_m   = len(ids_orig)                       #number of ids in original timeseries for month m
    n_ts_reduced_m = int( np.ceil( ts_percent_dec * n_ids_orig_m ) ) #reduced number of ids based on percentage
    if (ts_percent_dec * n_ids_orig_m) < 1 or n_ts_reduced_m == 0:
      warnings.warn('Extremely low percentage results in less than a simulation per month. One simulation value will be used instead')
      n_ts_reduced_m = 1

    # Random selection of timesteps
    ids_list_red_m = random.sample(ids_orig, n_ts_reduced_m) 
    data[m] = {
        "data": data_m,
        "original_ids": ids_orig,
        "n_ids_orig_m": n_ids_orig_m,
        "n_ts_reduced_m": n_ts_reduced_m,
        "ids_list_red_m": ids_list_red_m
    }

    ids_list_reduced += ids_list_red_m

  return ids_list_reduced, data
