# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import os
# import scipy
import warnings
warnings.simplefilter("ignore")

# Import Shared Libraries
# from dtop_shared_library.pandas_tables import schema_from_document

# Import classes
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Site

# Import functions
from dtop_lmo.business import feasibilities as feas_funcs
from dtop_lmo.business import workability
from dtop_lmo.business import startability
from dtop_lmo.business import waiting
from dtop_lmo.business.feasibilities.vessels import _read_vessel_clusters
from dtop_lmo.service.api.catalogues import get_catalog_vecs
from dtop_lmo.service.api.catalogues import get_catalog_vessels

# Get classes according to complexity
Operation = Operation_class.get_complexity('high')

# True to print logs
PRINT_LOGS = True


class Core3(object):
  """
  Core3:
  --------
  The class assesses a set of core functionalities for late stage

  Arguments:
  var_name = variable description - var type

  Optional Arguments:
  max_wait = Maximum waiting time between activities - integer
  comb_retain_ratio = Combinations retain ratio - float
  comb_retain_min = Minimum number of Combinations - integer
  quantile = Statistical quantile to be analysed [0.25, 0.5, 0.75] - float
  period = Period of aggregation to analyse: "month", "quarter", "trimester" -
  string

  Return:
  var_name = variable description - var type
  """
  complexity = 'high'

  def __init__(
      self,
      max_wait: int,
      comb_retain_ratio: float = 0.05,
      comb_retain_min: int = 5,
      quantile: float = 0.5,
      period: str = 'month',
      load_factor: float = 0.8,
      sfoc: float = 210,
      fuel_price: float = 515
  ):
    # Default values
    self.max_wait = max_wait
    if comb_retain_ratio is None:
      self.comb_retain_ratio = 0.05
    if comb_retain_min is None:
      self.comb_retain_min = 5
    if quantile is None:
      self.quantile = 0.5
    if period is None:
      self.period = 'month'
    if load_factor is None:
      self.load_factor = 0.8
    if sfoc is None:
      self.sfoc = 210
    if fuel_price is None:
      self.fuel_price = 515

    # Optional inputs
    self.comb_retain_ratio = comb_retain_ratio
    self.comb_retain_min = comb_retain_min
    self.quantile = quantile
    self.period = period
    self.load_factor = load_factor
    self.sfoc = sfoc
    self.fuel_price = fuel_price

    # Run initializing functions
    # self.get_inputs()

  def get_inputs(
      self
  ) -> dict:
    # Print inputs in console

    # Return inputs as a dictionary
    return {
        # "Argument 1": self.arg1,
        # "Argument 2": self.arg2,
        "Maximum waiting time between activities": self.max_wait,
        "Combinations retain ratio": self.comb_retain_ratio,
        "Minimum number of Combinations": self.comb_retain_min,
        "Statistical quantile to be analysed": self.quantile,
        "Period of the year to be analysed": self.period
    }

  # Feasibility functions
  def run_feasibility_functions(
      self,
      operations: list,
      quantile_vessels: float = 0.5,
      safety_factor: float = 0.1,
  ) -> list:
    """
    Runs all Operation methods required to get feasible solutions.

    Args:
        operations (list): List of :meth:`~dtop_lmo.business.classes.Operation_class()`
        quantile_vessels (float, optional): Quatile to analyse vessels statistically. Defaults to 0.5.
        safety_factor (float, optional): Safety factor for logistic selection. Defaults to 0.2.

    Returns:
        list: List of :meth:`~dtop_lmo.business.classes.Operation_class() with feasibility solutions
    """
    print('Running feasibility functions')
    for idx, _ in enumerate(operations):
      if ('repair' in operations[idx].description and
          'port' in operations[idx].description):
        # No need to evaluate feasibility
        continue
      if PRINT_LOGS is True:
        print('\t' + operations[idx].id + ' - ' + operations[idx].name + ' - ' + operations[idx].description)
      operations[idx].get_feasible_solutions(
          quantile_vessels=quantile_vessels,
          safety_factor=safety_factor
      )

    return operations

  def run_matchability_functions(
      self,
      operations: list
  ) -> list:
    """
    run_matchability_functions
    ==========================
    Runs all Operation methods required to match feasible solutions and get
    machable combinations.

    Inputs:
    -------
    * operations = List of the operations - list of Operation()

    Returns:
    --------
    * operations = List of operations with combinations - list of Operation()
    """
    print('Running matchability functions')
    for idx, op in enumerate(operations):
      if ('repair' in operations[idx].description and
          'port' in operations[idx].description):
        # No need to evaluate matchability. Just need to define a Combination
        operations[idx].combinations = [
            operations[idx].Combination(terminal=None,
                                        vec=None,
                                        travel_dist=0,
                                        num_trips=0,
                                        max_components=0,
                                        vessel_main=None,
                                        vessel_tow=None,
                                        vessel_support=None,
                                        equipment=[])
        ]
        continue
      if PRINT_LOGS is True:
        print('\t' + operations[idx].id + ' - ' + operations[idx].name + ' - ' + operations[idx].description)
      operations[idx].match_feasible_solutions()
      if len(operations[idx].combinations) == 0:
        _e = 'For operation %s - %s there are no possible ' % (op.id, op.name)
        _e = _e + 'combinations'
        raise AssertionError(_e)
      terminals = [comb.terminal for comb in operations[idx].combinations]
      operations[idx].update_dist_to_port()

    return operations

  def define_activities(
      self,
      operations: list
  ) -> list:
    """
    ### Check if this function couldn't be a method of the Operation() class
    define_activities
    =================
    For each possible combination (Operation.combinations), define the sequence
    of activities (a list of Operation.Activity)

    Inputs:
    -------
    * operations = List of the operations - list of Operation()

    Returns:
    --------
    * operations = List of operations with combinations - list of Operation()
    """
    print('Definning activities')
    for idx, _ in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t' + operations[idx].id + ' - ' + operations[idx].name + ' - ' + operations[idx].description)
      operations[idx].define_combinations_activities()
      for idx2, _ in enumerate(operations[idx].combinations):
        operations[idx].combinations[idx2].get_net_duration()

    return operations

  def delete_combinations(
      self,
      operations: list
  ) -> list:
    """
    delete_combinations
    ====================
    Considering the net duration of each combination, calculate the net costs
    for the vessels, equipment and terminal and delete the worst combinations

    Worst combinations: (1-'self.comb_retain_ratio')%
    """
    def combination_net_cost(
        operation: Operation,
        combination: Operation.Combination
    ) -> float:
      vec_id = combination.vec
      duration_net = combination.duration_net
      # Vessels
      list_vessels = [
          combination.vessel_main,
          combination.vessel_tow,
          combination.vessel_support
      ]
      list_vessels = [vessel
                      for vessel in list_vessels
                      if vessel is not None]
      # Number of vessels
      df_vecs = operation.feasible_solutions.vectable
      ds_vec = df_vecs[df_vecs['vec_id'] == vec_id].iloc[0, :]
      try:
        qnt_vessel1 = int(ds_vec['qnt1'])
      except ValueError:
        qnt_vessel1 = 0
      try:
        qnt_vessel2 = int(ds_vec['qnt2'])
      except ValueError:
        qnt_vessel2 = 0
      try:
        qnt_vessel3 = int(ds_vec['qnt3'])
      except ValueError:
        qnt_vessel3 = 0
      # Equipment
      list_equipment = combination.equipment
      terminal = combination.terminal

      df_vessels = pd.DataFrame()
      try:
        df_main = operation.feasible_solutions.vessels_main
        df_main_f = df_main[df_main['id_name'].isin([combination.vessel_main])]
        df_main_f.drop_duplicates(subset='id_name', inplace=True)
        df_main_f['vec_id'] = vec_id
        for idx in range(1, qnt_vessel1):
          df_main_f = df_main_f.append(df_main_f.iloc[0, :])
      except TypeError:
        df_main_f = pd.DataFrame()
      try:
        df_tow = operation.feasible_solutions.vessels_tow
        df_tow_f = df_tow[df_tow['id_name'].isin([combination.vessel_tow])]
        df_tow_f.drop_duplicates(subset='id_name', inplace=True)
        df_tow_f['vec_id'] = vec_id
        for idx in range(1, qnt_vessel2):
          df_tow_f = df_tow_f.append(df_tow_f.iloc[0, :])
      except TypeError:
        df_tow_f = pd.DataFrame()
      try:
        df_sup = operation.feasible_solutions.vessels_support
        df_sup_f = df_sup[df_sup['id_name'].isin([combination.vessel_support])]
        df_sup_f.drop_duplicates(subset='id_name', inplace=True)
        df_sup_f['vec_id'] = vec_id
        for idx in range(1, qnt_vessel3):
          df_sup_f = df_sup_f.append(df_sup_f.iloc[0, :])
      except TypeError:
        df_sup_f = pd.DataFrame()

      df_vessels = df_vessels.append(df_main_f, ignore_index=True)
      df_vessels = df_vessels.append(df_tow_f, ignore_index=True)
      df_vessels = df_vessels.append(df_sup_f, ignore_index=True)

      list_ds_vessels = [row for _, row in df_vessels.iterrows()]

      # Values in € / day
      list_cost_vessel_charter = [self.vessel_daily_charter(ds_vessel)
                                  for ds_vessel in list_ds_vessels]
      # Values in ton / day
      list_vessel_cons = [
          self.vessel_fuel_consumption(
              ps_vessel=ds_vessel,
              alf=self.load_factor,
              sfoc=self.sfoc
          )
          for ds_vessel in list_ds_vessels
      ]
      # Values in € / day
      list_vessel_cost_fuel = [
          self.vessel_fuel_costs(
              fuel_cons=consumption,
              mdo_price=self.fuel_price
          )
          for consumption in list_vessel_cons
      ]
      # Values in € / day (charter + fuel)
      list_vessel_cost_daily = [
          sum(x)
          for x in zip(*(list_cost_vessel_charter, list_vessel_cost_fuel))
      ]
      # Values in €
      list_vessel_cost = [cost * (duration_net / 24)
                          for cost in list_vessel_cost_daily]
      total_cost_vessels = sum(list_vessel_cost)

      list_df_equipment = []
      try:
        list_df_equipment.append(operation.feasible_solutions.rovtable)
      except AttributeError:
        pass
      try:
        list_df_equipment.append(operation.feasible_solutions.divertable)
      except AttributeError:
        pass
      try:
        list_df_equipment.append(operation.feasible_solutions.burialtable)
      except AttributeError:
        pass
      try:
        list_df_equipment.append(operation.feasible_solutions.protecttable)
      except AttributeError:
        pass
      try:
        list_df_equipment.append(operation.feasible_solutions.pilingtable)
      except AttributeError:
        pass

      equip_daily_cost = 0      # Value in € / day
      equip_half_day_cost = 0   # Value in € / half_day
      for id_ in list_equipment:
        for df in list_df_equipment:
          try:
            equip_daily_cost += df[df['id_name'] == id_]['cost_day'].iloc[0]
            equip_half_day_cost += df[df['id_name'] == id_]['cost_half_day'].iloc[0]
            break
          except IndexError:
            pass

      # Value in €
      equip_cost = equip_daily_cost * int(duration_net / 24)
      if (duration_net / 24) % 1 < 0.5:
        equip_cost += equip_half_day_cost * ((duration_net / 24) % 1)
      else:
        equip_cost += equip_daily_cost * ((duration_net / 24) % 1)

      return float(total_cost_vessels + equip_cost)

    print('Deleting combinations')
    for idx_op, operation in enumerate(operations):
      if ('repair' in operations[idx_op].description and
          'port' in operations[idx_op].description):
        # No combinations to delete
        continue

      if PRINT_LOGS is True:
        print('\t' + operation.id + ' - ' + operation.name + ' - ' + operation.description)
      # Define the number of combinations to leave
      total_num_comb = len(operation.combinations)
      num_comb = max(self.comb_retain_min,
                     self.comb_retain_ratio * total_num_comb)
      num_comb = int(num_comb)

      # First delete combinations with duration_net = inf
      comb_new = [comb
                  for comb in operation.combinations
                  if not np.isinf(comb.duration_net)]

      if len(comb_new) == 0:
        _e = 'For operation %s, all combinations are ' % operation.name
        _e = _e + 'impossible'
        raise AssertionError(_e)

      # Calculate net cost for each combination
      list_costs = [combination_net_cost(operation, comb)
                    for comb in comb_new]

      # Sort combinations by net cost
      zipped = zip(list_costs, comb_new)
      zip_sorted = sorted(zipped, key=lambda x: x[0])
      comb_new = [comb for cost, comb in zip_sorted]

      # Delete worst combinations
      operations[idx_op].combinations = comb_new[0:num_comb]

    return operations

  # Operation Limit Criteria realted functions
  def check_workabilities(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking workabilities')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))

      for i_comb, comb in enumerate(operation.combinations):
        operations[i_op].combinations[i_comb].workability = \
            workability.workability(comb.activities, operation.site)

    return operations

  def check_startabilities(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking startabilities')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))

      for i_comb, comb in enumerate(operation.combinations):
        operations[i_op].combinations[i_comb].startability = \
            startability.startability(comb.activities,
                                      comb.workability,
                                      operation.site.name)
        del operations[i_op].combinations[i_comb].workability
        operations[i_op].combinations[i_comb].workability = None

    return operations

  def check_activities_waiting_time(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking activities waiting time')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))

      for i_comb, comb in enumerate(operation.combinations):
        operations[i_op].combinations[i_comb].waiting = \
            waiting.waiting_time(comb.activities,
                                 comb.startability,
                                 operation.site.name)
        del operations[i_op].combinations[i_comb].startability
        operations[i_op].combinations[i_comb].startability = None

    return operations

  def define_durations_waitings_timestep(
      self,
      operations: list
  ) -> list:
    """
    Define operations durations and waitings per time step.

    Define durations and waitings per time step. For each metocean time step,
    define the operations total duration, the duration at port, the duration at
    site, the waiting to start the operation, the waiting at site and the
    waiting at port

    Args:
      operations (list): List of operations

    Returns:
      operations (list): List of operations updated
    """
    print('Calculating duration per timestep')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t' + operation.id + ' - ' + operation.name + ' - ' + operation.description)
      for i_comb, comb in enumerate(operation.combinations):
        operations[i_op].combinations[i_comb].values = \
            comb.define_operation_values(
                site_name=operation.site.name,
                ts_analyse=operation.site.timesteps_analyse,
                MAX_WAIT=self.max_wait
            )
        del operations[i_op].combinations[i_comb].waiting
        operations[i_op].combinations[i_comb].waiting = None

    return operations

  def get_statistics(
      self,
      df_values
  ) -> dict:
    """
    get_statistics
    ===============
    For a given DataFrame with durations and waitings per metocean time step,
    this functions preforms a statistic analysis considering the period and the
    quantile selected.
    It returns the expected value (with a certainty of "quantile") for each
    duration and waiting of the operation

    Inputs
    ------
    * df_values = Duration and Waitings values per time step - pd.DataFrame

    Returns
    --------
    durations_waitings = Dictionary with durations and waitings for each
    selected interval
    """
    durations = {
        "total": None,
        "port": None,
        "site": None,
        "transit": None,
        "mobilization": None
    }
    for key in durations:
      durations[key] = {}

    waitings = {
        "to_start": None,
        "port": None,
        "site": None
    }
    for key in waitings:
      waitings[key] = {}

    if self.period.lower() == 'month':
      intervals = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6),
                   (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)]
    elif self.period.lower() == 'quarter':
      intervals = [(1, 3), (4, 6), (7, 9), (10, 12)]
    elif self.period.lower() == 'trimester':
      intervals = [(1, 4), (5, 8), (9, 12)]
    else:
      raise AssertionError('\"period\" defined unrecognized')

    keys = [i + 1 for i in range(0, len(intervals))]
    for k in keys:
      int_min = intervals[k - 1][0]
      int_max = intervals[k - 1][1]
      # Filtered by period values
      df_values_f = df_values[(df_values['month'] >= int_min) & \
                              (df_values['month'] <= int_max)]

      durations["total"][str(k)] = df_values_f['duration'].quantile(
          q=self.quantile,
          interpolation='nearest'
      )
      dur = durations["total"][str(k)]

      duration_row = df_values_f[df_values_f['duration'] == dur]
      try:
        durations["port"][str(k)] = duration_row['duration_port'].iat[0]
        durations["site"][str(k)] = duration_row['duration_site'].iat[0]
        durations["transit"][str(k)] = duration_row['transit'].iat[0]
        durations["mobilization"][str(k)] = duration_row['mobilization'].iat[0]
        waitings["to_start"][str(k)] = duration_row['waiting_start'].iat[0]
        waitings["port"][str(k)] = duration_row['waiting_start'].iat[0]
        waitings["site"][str(k)] = duration_row['waiting_site'].iat[0]
      except IndexError:
        durations["total"][str(k)] = np.nan
        durations["port"][str(k)] = np.nan
        durations["site"][str(k)] = np.nan
        durations["transit"][str(k)] = np.nan
        durations["mobilization"][str(k)] = np.nan
        waitings["to_start"][str(k)] = np.nan
        waitings["port"][str(k)] = np.nan
        waitings["site"][str(k)] = np.nan

    return {
        "durations": durations,
        "waitings": waitings
    }

  def statistical_analysis(
      self,
      operations: list
  ) -> list:
    for idx_op, operation in enumerate(operations):
      # First, merge metocean data and each combination operation values
      operations[idx_op].merge_metocean_w_values()
      for idx_comb, combination in enumerate(operation.combinations):
        comb_stats = self.get_statistics(combination.values)
        operations[idx_op].combinations[idx_comb].statistics.durations = \
            comb_stats["durations"]
        operations[idx_op].combinations[idx_comb].statistics.waitings = \
            comb_stats["waitings"]

    return operations

  def calculate_combination_costs(
      self,
      feasible_sols: Operation.Feasible,
      combination: Operation.Combination
  ) -> dict:
    """
    calculate_combination_costs
    ============================
    For a given Combination() calculates the costs related to vessels,
    equipment and terminal for all possible combinations
    ### TERMINAL COSTS MISSING

    Inputs
    -------
    * Operation,
    * combination = Operation possible combination - Operation.Combination()

    Retruns
    ---------
    dict: Costs in each month
    * vessels = Cost of the vessels in each month
    * equipment = Cost of the equipment in each month
    """
    # Operation duration in hours, in each period for this combination
    durations_period = combination.statistics.durations["total"]
    waiting_period = combination.statistics.waitings["to_start"]
    # Number of days of the operation per month
    n_days_period = {}
    for key, value in durations_period.items():
      try:
        n_days_period[key] = value / 24
      except ValueError:
        n_days_period[key] = np.nan
    op_dur_total_days = {key: value / 24
                         for key, value in durations_period.items()}
    op_wait_start_days = {key: value / 24
                          for key, value in waiting_period.items()}

    # Get this combination vec ID and vec DataSeries
    try:
      df_vecs = get_catalog_vecs()
    except ConnectionError:
      path_catalogue_vecs = os.path.join(os.getcwd(), 'catalogues', 'VECs.csv')
      df_vecs = pd.read_csv(path_catalogue_vecs, sep=',')
    df_vecs = df_vecs[df_vecs['vec_id'].str.lower() == combination.vec.lower()]
    ds_vec = df_vecs.iloc[0, :]

    try:
      qnt_vessel1 = int(ds_vec['qnt1'])
    except ValueError:
      qnt_vessel1 = 0
    try:
      qnt_vessel2 = int(ds_vec['qnt2'])
    except ValueError:
      qnt_vessel2 = 0
    try:
      qnt_vessel3 = int(ds_vec['qnt3'])
    except ValueError:
      qnt_vessel3 = 0

    # Get this combination vessels IDs and vessels DataFrames
    # df_vessels = pd.DataFrame()
    vessels_ids = []
    if combination.vessel_main is not None:
      for i in range(0, qnt_vessel1):
        vessels_ids.append(combination.vessel_main)
    if combination.vessel_tow is not None:
      for i in range(0, qnt_vessel2):
        vessels_ids.append(combination.vessel_tow)
    if combination.vessel_support is not None:
      for i in range(0, qnt_vessel3):
        vessels_ids.append(combination.vessel_support)

    try:
      df_vessels = get_catalog_vessels()
    except ConnectionError:
      path_catalogue_vessels = os.path.join(os.getcwd(), 'catalogues', 'Vessels.csv')
      df_vessels = pd.read_csv(path_catalogue_vessels, sep=',')

    if self.quantile == 0.25:
      percentil = 'p25'
    elif self.quantile == 0.50:
      percentil = 'p50'
    elif self.quantile == 0.75:
      percentil = 'p75'
    df_vessels = _read_vessel_clusters(df_vessels, percentil)

    # Get this combination equipment DataFrames
    try:
      df_rov = feasible_sols.rovtable
    except AttributeError:
      df_rov = None
    try:
      df_diver = feasible_sols.divertable
    except AttributeError:
      df_diver = None
    try:
      df_burial = feasible_sols.burialtable
    except AttributeError:
      df_burial = None
    try:
      df_protect = feasible_sols.protecttable
    except AttributeError:
      df_protect = None
    try:
      df_piling = feasible_sols.pilingtable
    except AttributeError:
      df_piling = None

    # Get this combination equipment IDs
    equipment_ids = combination.equipment

    costs_total_vessels_period = {}
    costs_total_equipment_period = {}
    costs_total_terminal_period = {}
    for per, days in n_days_period.items():
      # Check for how many complete days and how many half days does this
      # operation lasts
      if days % 1 < 0.5 and days % 1 != 0:
        days_complete = int(days)
        days_half = 1
      else:
        try:
          days_complete = mt.ceil(days)
          days_half = 0
        except ValueError:
          days_complete = np.nan
          days_half = np.nan

      costs_charter_daily = 0
      costs_fuel_daily = 0
      costs_equip_daily = 0
      costs_equip_half = 0
      # For each vessel ID, get the pandas Series from the Catalogue
      for id_ in vessels_ids:
        # Filter DataFrame to a single Series
        ds_vessel = df_vessels[df_vessels['id_name'].str.lower() == id_.lower()]
        ds_vessel = ds_vessel.iloc[0, :]

        # Charter costs
        costs_charter_daily += self.vessel_daily_charter(ds_vessel)   # €/day
        # Fuel costs
        vessel_cons = self.vessel_fuel_consumption(
            ps_vessel=ds_vessel,
            alf=self.load_factor,
            sfoc=self.sfoc
        )   # ton/day
        costs_fuel_daily += self.vessel_fuel_costs(
            fuel_cons=vessel_cons,
            mdo_price=self.fuel_price
        )     # €/day

      # For each equipment ID, get the pandas Series from the Catalogue
      for id_ in equipment_ids:
        # Filter DataFrame to a single Serie
        if 'rov' in id_:
          ds_equip = df_rov[df_rov['id_name'].str.lower() == id_.lower()]
        elif 'div' in id_:
          ds_equip = df_diver[df_diver['id_name'].str.lower() == id_.lower()]
        elif 'bur' in id_:
          ds_equip = df_burial[df_burial['id_name'].str.lower() == id_.lower()]
        elif 'pro' in id_:
          ds_equip = df_protect[df_protect['id_name'].str.lower() == id_.lower()]
        elif 'pil' in id_:
          ds_equip = df_piling[df_piling['id_name'].str.lower() == id_.lower()]

        costs_equip_daily += float(ds_equip['cost_day'])      # €/day
        costs_equip_half += float(ds_equip['cost_half_day'])  # €/half_day

      costs_total_vessels_charter = \
          costs_charter_daily * (days_complete + days_half)
      costs_total_vessels_fuel = costs_fuel_daily * \
                      (op_dur_total_days[per] - op_wait_start_days[per])

      costs_total_vessels_period[per] = \
          costs_total_vessels_charter + costs_total_vessels_fuel
      costs_total_equipment_period[per] = \
          (costs_equip_daily * days_complete) + (costs_equip_half * days_half)
      vessels_and_equip = costs_total_vessels_period[per] + \
                                          costs_total_equipment_period[per]
      costs_total_terminal_period[per] = vessels_and_equip * 0.005

    return {
        "vessels": costs_total_vessels_period,
        "equipment": costs_total_equipment_period,
        "terminal": costs_total_terminal_period
    }

  def get_vessels_cost(
      self,
      operations: list
  ) -> list:
    for idx_op, operation in enumerate(operations):
      for idx_comb, combination in enumerate(operation.combinations):
        if 'repair at port' in operation.description.lower():
          # Repair at port does not require vessel operations but it must be
          # considered for cost calculation
          operations[idx_op].combinations[idx_comb].vec = \
              operations[idx_op - 1].combinations[idx_comb].vec
          operations[idx_op].combinations[idx_comb].vessel_main = \
              operations[idx_op - 1].combinations[idx_comb].vessel_main
          operations[idx_op].combinations[idx_comb].vessel_tow = \
              operations[idx_op - 1].combinations[idx_comb].vessel_tow
          operations[idx_op].combinations[idx_comb].vessel_support = \
              operations[idx_op - 1].combinations[idx_comb].vessel_support
          operations[idx_op].combinations[idx_comb].terminal = \
              operations[idx_op - 1].combinations[idx_comb].terminal

        comb_costs = self.calculate_combination_costs(
            operation.feasible_solutions,
            combination
        )
        operations[idx_op].combinations[idx_comb].costs.vessels = \
            comb_costs["vessels"]
        operations[idx_op].combinations[idx_comb].costs.equipment = \
            comb_costs["equipment"]
        operations[idx_op].combinations[idx_comb].costs.terminal = \
            comb_costs["terminal"]

    return operations

  def optimal_solutions(
      self,
      operations: list
  ) -> list:
    """
    ### develop definition
    Selects the best combination for each operation
    """
    for op_idx, _ in enumerate(operations):
      operations[op_idx].define_operation_attributes()

    return operations

  def operation_consumption(
      self,
      opx: Operation
  ) -> float:
    try:
      df_vessels = get_catalog_vessels()
    except ConnectionError:
      path_catalogue_vessels = os.path.join(os.getcwd(), 'catalogues', 'Vessels.csv')
      df_vessels = pd.read_csv(path_catalogue_vessels, sep=',')

    if self.quantile == 0.25:
      percentil = 'p25'
    elif self.quantile == 0.50:
      percentil = 'p50'
    elif self.quantile == 0.75:
      percentil = 'p75'
    df_vessels = _read_vessel_clusters(df_vessels, percentil)

    daily_cons = 0
    for id_ in opx.vessels:
      # Filter DataFrame to a single Series
      ds_vessel = df_vessels[df_vessels['id_name'].str.lower() == id_.lower()]
      ds_vessel = ds_vessel.iloc[0, :]

      vessel_cons = self.vessel_fuel_consumption(
          ps_vessel=ds_vessel,
          alf=self.load_factor,
          sfoc=self.sfoc
      )   # ton/day

      daily_cons += vessel_cons

    return daily_cons

  def select_combination(
      self,
      operations: list
  ) -> list:
    operations = self.optimal_solutions(operations)
    for idx, operation in enumerate(operations):
      consumption = self.operation_consumption(operation)
      operations[idx].consumption = consumption

    return operations

  # Vessel related functions
  def vessel_daily_charter(
      self,
      ps_vessel: pd.Series
  ):
    """
    vessel_daily_charter:
    This function returns the a given vessel charter costs.
    Author: FCF
      Last review: on 12/02/2020
    Some vessels are still missing:
      - survery vessel (possibly to be removed)
      - semi-sub
      - rock dumper
      - rib
      - guard vessel
      - dsv
      - dredger
    Input:
    ------
    * ps_vessel = Pandas series with all information about the vessel

    Output:
    -------
    * vessel_type = Type of the vessel - string
    * chart_costs = Vessel charter costs [€/day] - float ### Confirm units
    """
    vessel_type = ps_vessel['vessel_type'].lower()

    # Initialization
    chart_costs = np.nan

    if 'tug' in vessel_type:
      x = float(ps_vessel['bollard'])
      if x < 13:
        x = 13
        _w = 'Bollard pull falls outside of the defined cost_function domain'
        warnings.warn(_w)
      elif x > 80:
        x = 80
        _w = 'Bollard pull falls outside of the defined cost_function domain'
        warnings.warn(_w)

      if 13 <= x < 25:
        chart_costs = 151.34 * x - 467.47
      elif 25 <= x < 70:
        chart_costs = 2.18 * x + 3261.61
      elif 70 <= x <= 80:
        chart_costs = 508.57 * x - 32186
      else:
        raise ValueError('Bollard pull value error')

    elif 'ahts' in vessel_type:
      x = float(ps_vessel['bollard'])
      if x < 70:
        x = 70
        _w = 'Bollard pull falls outside of the defined cost_function domain'
        warnings.warn(_w)
      elif x > 338:
        x = 338
        chart_costs = -8.3 * 10**(-3) * x**2 + 114.90 * x - 261.87
        _w = 'Bollard pull falls outside of the defined cost_function domain'
        warnings.warn(_w)

      chart_costs = -8.3 * 10**(-3) * x**2 + 114.90 * x - 261.87

    elif ('multi' in vessel_type and 'cat' in vessel_type):
      x = float(ps_vessel['loa'])
      if x < 21:
        x = 21
        warnings.warn('LOA falls outside of the defined cost_function domain')
      elif x > 42:
        x = 42
        warnings.warn('LOA falls outside of the defined cost_function domain')

      if 21 <= x < 28:
        chart_costs = 63.23 * x + 1812.4
      elif 28 <= x < 35:
        chart_costs = 916.74 * x - 22086
      elif 35 <= x <= 42:
        chart_costs = 10000
      else:
        raise ValueError('LOA value error')

    elif 'clv' in vessel_type:
      x = float(ps_vessel['turn_storage'])
      if x < 565:
        x = 565
        _w = 'Turntable storage falls outside of the defined cost_function '
        _w = _w + 'domain'
        warnings.warn(_w)
      elif x > 10000:
        x = 10000
        _w = 'Turntable storage falls outside of the defined cost_function '
        _w = _w + 'domain'
        warnings.warn(_w)

      chart_costs = 2.46 * 10**(-4) * x**2 + 7.25 * x + 53090

    elif 'ctv' in vessel_type:
      x = float(ps_vessel['loa'])
      if x < 15:
        x = 15
        warnings.warn('LOA falls outside of the defined cost_function domain')
      elif x > 33:
        x = 33
        warnings.warn('LOA falls outside of the defined cost_function domain')

      chart_costs = -1.26 * x**2 + 179.16 * x - 85.57

    elif 'dsv' in vessel_type:
      x = float(ps_vessel['loa'])
      if x < 35:
        x = 35
        warnings.warn('LOA falls outside of the defined cost_function domain') 
      elif x > 150:
        x = 150
        warnings.warn('LOA falls outside of the defined cost_function domain')

      chart_costs = 4308.81 * np.exp(0.02 * x)

    elif ('guard' in vessel_type and 'vessel' in vessel_type):
      x = float(ps_vessel['speed_service'])
      if x < 7:
        x = 7
        _w = 'Service Speed falls outside of the defined cost_function domain'
        warnings.warn(_w)
      elif x > 24:
        x = 24
        _w = 'Service Speed falls outside of the defined cost_function domain'
        warnings.warn(_w)

      chart_costs = 77.11 * x + 1345.48

    elif ('transport' in vessel_type and 'barge' in vessel_type):
      loa = float(ps_vessel['loa'])
      beam = float(ps_vessel['beam'])
      draft = float(ps_vessel['draft'])
      x = loa * beam * draft
      if x < 1557:
        x = 1557
        chart_costs = 953.92 * np.log(x) - 6761.18
        _w = 'Dimensions fall outside of the defined cost_function domain'
        warnings.warn(_w)
      elif x > 19950:
        x = 19950
        chart_costs = 953.92 * np.log(x) - 6761.18
        _w = 'Dimensions fall outside of the defined cost_function domain'
        warnings.warn(_w)

      chart_costs = 953.92 * np.log(x) - 6761.18

    elif ('jack' in vessel_type and 'up' in vessel_type):
      x = float(ps_vessel['crane_capacity'])
      if x < 50:
        x = 50
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)
      elif x > 4400:
        x = 4400
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)

      if 50 <= x < 755:
        chart_costs = 64.71 * x + 21448.41
      elif 755 <= x <= 896:
        chart_costs = 586.18 * x - 372275
      elif 896 <= x <= 4400:
        chart_costs = 26.83 * x + 128892
      else:
        raise ValueError('Crane capacity value error')

    elif ('non' in vessel_type and
          'propelled' in vessel_type and
          'crane' in vessel_type):
      x = float(ps_vessel['crane_capacity'])
      if x < 4:
        x = 4
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)
      elif x > 3300:
        x = 3300
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)

      if 4 <= x < 500:
        chart_costs = 26.15 * x + 5842.59
      elif 500 <= x < 1500:
        chart_costs = 56.33 * x - 9254.94
      elif 1500 <= x <= 3300:
        chart_costs = 42.24 * x + 11871.96
      else:
        raise ValueError('Value error') 

    # TODO: REVER ###
    elif ('propelled' in vessel_type and 'crane' in vessel_type):
      x = float(ps_vessel['crane_capacity'])
      if x < 50:
        x = 50
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)
      elif x > 12000:
        x = 12000
        _w = 'Vessel crane capacity falls outside of the defined cost_function'
        _w = _w + ' domain'
        warnings.warn(_w)

      chart_costs = -5.54 * 10**(-3) * x**2 + 88.90 * x + 12714.58

    elif 'psv' in vessel_type:
      x = float(ps_vessel['free_deck'])
      if x < 30:
        x = 30
        _w = 'Vessel free deck area falls outside of the defined cost_function'
        warnings.warn(_w)
      elif x > 5005:
        x = 5005
        _w = 'Vessel free deck area falls outside of the defined cost_function'
        warnings.warn(_w)

      chart_costs = 1.01 * x + 8970

    elif ('rock' in vessel_type and 'dumper' in vessel_type):
      x = float(ps_vessel['rock_capacity'])
      if x < 5400:
        x = 5400
        _w = 'Rock cargo capacity falls outside of the defined cost_function '
        _w = _w + 'domain'
        warnings.warn(_w)
      elif x > 31500:
        x = 31500
        _w = 'Rock cargo capacity falls outside of the defined cost_function '
        _w = _w + 'domain'
        warnings.warn(_w)

      chart_costs = 3.99 * x + 69212.41

    elif ('sov' in vessel_type and
          # 'pure' in vessel_type and
          'accomodation' in vessel_type):
      x = float(ps_vessel['passengers'])
      if x < 60:
        chart_costs = 12000
      elif x >= 60:
        chart_costs = 20000
      else:
        raise ValueError('Passengers value error')

    elif ('sov' in vessel_type and
          'gangway' in vessel_type): # and
          # 'equipped' in vessel_type):
      x = float(ps_vessel['passengers'])
      if x < 60:
        chart_costs = 24000
      elif x >= 60:
        chart_costs = 50000
      else:
        raise ValueError('Passengers value error')

    # ### Este vai desaparecer ###
    # elif ('sov' in vessel_type and
    #       'gangway' in vessel_type and
    #       'relevant' in vessel_type):
    #   x = float(ps_vessel['passengers'])
    #   if x < 60:
    #     chart_costs = 24000
    #   if x >= 60:
    #     chart_costs = 42000
    #   else:
    #     raise ValueError('Passengers value error')

    elif 'survey' in vessel_type:
      x = float(ps_vessel['loa'])
      if x < 23:
        x = 23
        _w = 'Vessel LOA falls outside of the defined cost_function domain'
        warnings.warn(_w)
      elif x > 56:
        x = 56
        _w = 'Vessel LOA falls outside of the defined cost_function domain'
        warnings.warn(_w)

      chart_costs = 333.33 * x - 4166.67

    elif 'semi-submersible' in vessel_type:
      chart_costs = 3000    # TODO: THIS IS WRONG!

    else:
      raise AssertionError('Vessel type not found.')

    return chart_costs

  def vessel_fuel_consumption(
      self,
      ps_vessel: pd.Series,
      alf: float = 0.8,
      sfoc: float = 210
  ) -> float:
    '''
    vessel_fuel_consumption:
    Function that calculates fuel consumption of a given vessel.

    Inputs:
    -------
    * ps_vessel = Vessel information - Pandas Series
    * alf = Average Load Factor of the vessel (value suggested by GRS Offshore)
    - float
    * sfoc = Specific Fuel Oil Consumption (value suggested by GRS Offshore)
    - float

    Returns:
    --------
    * fuel_cons = vessel fuel consumption [ton/day] - float
    '''
    fuel_cons = ps_vessel['power'] * alf * sfoc * 24 * (1/1000)**2
    return fuel_cons

  def vessel_fuel_costs(
      self,
      fuel_cons: float,
      mdo_price: float = 515
  ) -> float:
    '''
    vessel_fuel_costs:
    Function that calculates daily fuel costs of a given vessel or
    combination of vessels.

    Inputs:
    -------
    * fuel_cons = fuel consumption of a given vessel or list of vessels - float
    * mdo_price = Price of MDO in €/ton. Default value defined as 515€/ton -
    float

    Returns:
    --------
    * vessel fuel cost in €/day        
    '''
    return fuel_cons * mdo_price
