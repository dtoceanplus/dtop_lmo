# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import networkx as nx
from networkx.readwrite import json_graph
import geopy.distance as gp_dist
from global_land_mask import globe
import json
import re
import warnings
warnings.simplefilter("ignore")


class Map_Graph():
  def __init__(self,
               graph: nx.Graph(),
               lat_min: float, lat_max: float,
               lon_min: float, lon_max: float,
               grid_resolution: tuple):
    self.graph = graph
    self.limits = {}
    self.limits['lat'] = (lat_min, lat_max)
    self.limits['lon'] = (lon_min, lon_max)
    self.grid_res = grid_resolution


def create_graph(
    graph_name: str,
    mesh_res: tuple,
    map_limits: tuple
) -> Map_Graph:
  try:
    import os
    local_path = '/TEMP_DATABASE/Maps'
    map_path = os.getcwd() + local_path
    file_name = '/Graph_' + graph_name + '_' + \
                str(mesh_res[0]) + 'x' + str(mesh_res[1])
    with open(map_path + file_name + '.json') as f:
      G_dict = json.load(f)
      G = json_graph.node_link_graph(G_dict)

    # G = nx.read_gpickle(map_path + file_name + '.gpickle')
    # matrix_water = np.load(file_name + '.npy')
    warnings.warn('%s Graph imported.' % graph_name)

  except FileNotFoundError:
    G = nx.Graph()
    # matrix_water = np.zeros([mesh_res[0], mesh_res[1]])
    # Only for visualization porposes

    # Define the degree steps for the mesh
    lat_deg_step = (map_limits[0][1] - map_limits[0][0]) / mesh_res[0]
    lon_deg_step = (map_limits[1][1] - map_limits[1][0]) / mesh_res[1]

    count_edges = 0
    for x in range(0, mesh_res[0]):
      # node definition - x
      node_x = 'x' + str(x)
      # node longitud
      node_lon = map_limits[1][0] + x * lon_deg_step + (lon_deg_step / 2)

      # run neighbour nodes
      if x > 0 and x < mesh_res[0] - 1:
        neig_x_ini = x - 1
        neig_x_fin = x + 1
      else:
        if x == 0:
          neig_x_ini = x
          neig_x_fin = x + 1
        elif x < mesh_res[0] - 1:
          neig_x_ini = x - 1
          neig_x_fin = x

      for y in range(mesh_res[1] - 1, -1, -1):
        # node definition - y
        node_y = 'y' + str(y)
        # node longitud
        node_lat = map_limits[0][0] + y * lat_deg_step + (lat_deg_step / 2)

        node_def = node_x + node_y
        node_coord = (node_lat, node_lon)

        # Previous approach - basemap
        # xpt, ypt = basemap(node_lon, node_lat)    # convert to projection map
        # b_node_land = basemap.is_land(xpt, ypt)
        # New approach - globe.is_land
        b_node_land = globe.is_land(node_lat, node_lon)

        if not b_node_land:    # If the node is in the sea
          # matrix_water[mesh_res[1] - 1 - y, x] = 1
          # run neighborwood nodes
          if y > 0 and y < mesh_res[1] - 1:
            neig_y_ini = y - 1
            neig_y_fin = y + 1
          else:
            if y == 0:
              neig_y_ini = y
              neig_y_fin = y + 1
            elif y == mesh_res[1] - 1:
              neig_y_ini = y - 1
              neig_y_fin = y

          for x_neig in range(neig_x_ini, neig_x_fin + 1):
            # neighbour longitud
            if x_neig < x:
              neig_lon = node_lon - lon_deg_step
            elif x_neig > x:
              neig_lon = node_lon + lon_deg_step
            elif x_neig == x:
              neig_lon = node_lon

            for y_neig in range(neig_y_ini, neig_y_fin + 1):
              if x_neig == x and y_neig == y:
                # We don't want to create an edge from a node to itself
                pass
              else:
                # neighbour latitud
                if y_neig < y:
                  neig_lat = node_lat - lat_deg_step
                elif y_neig > y:
                  neig_lat = node_lat + lat_deg_step
                elif y_neig == y:
                  neig_lat = node_lat

                # Neighbour coordinates
                neig_coord = (neig_lat, neig_lon)

                # Previous approach - basemap
                # xpt_n, ypt_n = basemap(neig_lon, neig_lat)
                # b_neig_land = basemap.is_land(xpt_n, ypt_n)
                b_neig_land = globe.is_land(neig_lat, neig_lon)

                if not b_neig_land:
                  weight_neig = gp_dist.great_circle(node_coord,
                                                     neig_coord).m
                  weight_neig = round(weight_neig)

                  node_neig_def = 'x' + str(x_neig) + 'y' + str(y_neig)

                  # Check if the edge was already created
                  if G.has_edge(node_def, node_neig_def) is False:
                    # If there is no edge, create it
                    G.add_edge(node_def, node_neig_def,
                               weight=weight_neig)
                    count_edges += 1

    # Save graph as json file
    G_dict = json_graph.node_link_data(G)
    with open(map_path + file_name + '.json', 'w+') as f:
      json.dump(G_dict, f)

  map_graph = Map_Graph(G,
                        map_limits[0][0], map_limits[0][1],
                        map_limits[1][0], map_limits[1][1],
                        tuple(mesh_res))

  return map_graph


def closest_node_sea(G, node):
  node_init = node
  xy_ini = re.findall('\d+', node)
  x_ini = int(xy_ini[0])
  y_ini = int(xy_ini[1])
  i = 1
  node_found = False

  dist = 1
  while True:
    for i in range(-dist, dist + 1):
      for j in range(-dist, dist + 1):
        node = 'x' + str(x_ini + i) + 'y' + str(y_ini + j)
        if len(G.edges(node)) != 0:
          node_found = True
          break
      if node_found:
        return node
    if dist > 100:
      return 'not found'
    dist += 1
