# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import pickle

# Import classes
from dtop_lmo.business.classes import Site


def waiting_time(
    activities: list,
    df_startability: pd.DataFrame,
    site_name: str,
) -> pd.DataFrame:
  """
  waiting_time
  ============
  index         = 0 12345678...
  startability  = 1 000001111 00000001111 000000111 00001111111111 00000000011
  diff        = NaN-100001000-10000001000-100000100-10001000000000-10000000010
  start_st_index = [5, 11, 16, 24, 39, 43, 52]
  start_ed_index = [4, 7, 14, 21, 35, 40, 46]

  Inputs
  -------
  * activities = List of activities of the operation for this combination - 
  list of Activity()
  * df_startability = Table with all activities startability - pandas DataFrame
  * site_name = Name of the site - string

  Returns:
  --------
  * df_waiting_time - Table of waiting on weather - pandas DataFrame
  """
  # Try to load Waiting database
  try:
    # ### LOAD THIS FROM CATALOGUES AND NOT FROM LOCAL MACHINE ###
    import os
    from pathlib import Path
    local_path = '/TEMP_DATABASE'
    db_path = os.getcwd() + local_path + '/' + site_name
    Path(db_path).mkdir(parents=True, exist_ok=True)
    file_waiting_db = open(db_path + '/db_acts_waiting', 'rb')
    list_wait_db = pickle.load(file_waiting_db)
    file_waiting_db.close()
  except FileNotFoundError:
    # Waiting pickle database not defined yet
    # Define a list of dict "database" to improve df_waiting_time calculations
    list_wait_db = []

  # Initialize a pandas DataFrame df_waiting_time with zeros
  df_waiting_time = pd.DataFrame(0,
                                 index=df_startability.index.tolist(),
                                 columns=df_startability.columns.tolist())

  for activity in activities:
    try:
      act_id = activity.id
    except AttributeError:
      # It is an operation
      act_id = 'operation'

    # Boolean pandas Series representing the startability for this activity
    startability_ones = df_startability[act_id] == True

    # First, search in the database if the waiting time for this startability
    # already exists
    act_startability = df_startability[act_id]

    waiting_exists = False
    for db_elem in list_wait_db:
      if act_startability.equals(db_elem['startability']) is True:
        waiting_exists = True
        del act_startability
        break

    if waiting_exists is True:
      # Load workability from database
      list_waiting = db_elem['waiting']
      ds_waiting = pd.Series(list_waiting, name=act_id)
      df_waiting_time[act_id] = ds_waiting
      df_waiting_time[act_id] = db_elem['waiting']
      del db_elem
    else:
      # Check if there is any restriction in startability
      if startability_ones.all():
        # If there is no restrictions, waiting time is always 0
        # DataFrame was initialized with 0's, so, nothing to be done
        pass
      else:
        # For some timesteps, the startability of the activity is 0
        # Check when startability changes from 0->1 or from 1->0
        df_start_aux = df_startability[act_id].notnull()
        df_start_aux = df_startability[act_id][df_start_aux].astype(int)
        df_start_diff = df_start_aux.diff()
        del df_start_aux
        # Save indexes where:
        # - startability changes from 0->1
        start_start_index = df_start_diff[df_start_diff == 1].index.tolist()
        # - startability changes from 1->0
        start_end_index = df_start_diff[df_start_diff == -1].index.tolist()

        if len(start_start_index) != 0:
          # Startability has at least one transition from 0->1
          if len(start_end_index) != 0:
            # Startability has at least one transition from 0->1 and 1->0
            start_start_index = start_start_index[::-1]   # Invert list
            start_end_index = start_end_index[::-1]       # Invert list
            if start_start_index[0] < start_end_index[0]:
              # Startability ends with False and we don't know the waiting time
              # for this timesteps
              # Check for how many time steps the waiting cannot be known
              index = start_end_index[0]
              df_waiting_time[act_id].iloc[index:] = np.nan
              # ### When np.nan is inserted, this Series changes from int type
              # to float type. FIX THIS
              # This entry of the list was already considered
              start_end_index.pop(0)      # Delete it      # Invert list
            else:
              # Startability ends with True
              pass

            for index in start_start_index:
              # Number of startability timesteps
              try:
                index2 = start_end_index[0]
                start_end_index.pop(0)
              except IndexError:
                index2 = 0

              no_start_time_steps = index - index2
              list_waittime = [i for i in range(1, no_start_time_steps + 1)]
              list_waittime = list_waittime[::-1]
              df_waiting_time[act_id].iloc[index2:index] = list_waittime
          else:
            # Startability has only one transition from 0->1
            max_waittime = start_start_index[0]
            list_waittime = [i for i in range(1, max_waittime + 1)]
            list_waittime = list_waittime[::-1]
            df_waiting_time[act_id].iloc[:max_waittime] = list_waittime

        else:
          # Startability has no transition 0->1, so or it has only one
          # transition or none.
          if len(start_end_index) != 0:
            # Startability has at least one transition from 0->1 and 1->0
            # The waiting time is always 0 until the startability is False
            index = start_end_index[0]
            df_waiting_time[act_id].iloc[index:] = np.nan
            # ### When np.nan is inserted, this Series changes from int type
            # to float type. FIX THIS
          else:
            # Startability is always False
            df_waiting_time[act_id] = np.nan

        # Convert waiting time to int type
        # ### NOT WORKING
        ds_not_null = df_waiting_time[act_id].notnull()
        df_waiting_time[act_id][ds_not_null] = df_waiting_time[act_id][ds_not_null].astype(int)

      # Where Startability is NaN, Waiting is also NaN
      ds_start_null = df_startability[act_id].isnull()
      df_waiting_time[act_id][ds_start_null] = np.nan
      ds_not_null = df_waiting_time[act_id].notnull()
      # ### NOT WORKING
      df_waiting_time[act_id][ds_not_null] = df_waiting_time[act_id][ds_not_null].astype(int)

      # Save new element to database
      db_elem = {}
      db_elem['startability'] = df_startability[act_id]
      db_elem['waiting'] = df_waiting_time[act_id]
      list_wait_db.append(db_elem)

      # ### Adapt this!
      file_waiting_db = open(db_path + '/db_acts_waiting', 'wb')
      pickle.dump(list_wait_db, file_waiting_db)
      file_waiting_db.close()
      # ###

      del db_elem
  del list_wait_db

  return df_waiting_time
