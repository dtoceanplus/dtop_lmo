# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import geopy.distance as geo_dist
import os

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_terminals


def terminals(
    opx,
    sf,
    df_terminals: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_terminal
  --------------------
  This function checks if a given Terminal can preform a given operation
  taking into account the operation requirements and the components involved
  in that operation

  Inputs:
  -------
  * opx = Operation X - Operation class
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * df_terminals = Table with terminals - pandas DataFrame

  Outputs:
  --------
  * feasible_terminals = Feasible Terminals - pandas DataFrame
  """
  # If no df_terminals is specified, Terminals from Catalogue are considered
  try:
    df_terminals.head()
    # df_terminals was introduced as a pandas DataFrame
    df_terminaltable = df_terminals
  except AttributeError:
    try:
      df_terminaltable = get_catalog_terminals()
    except ConnectionError:
      path_catalogue_terminals = os.path.join(os.getcwd(), 'catalogues', 'Terminals.csv')
      df_terminaltable = pd.read_csv(path_catalogue_terminals, sep=',', encoding="ISO-8859-1")

  if df_terminaltable.empty is True:
    _e = 'For operation \"%s\", Terminals catalogue is empty' % opx.name
    raise AssertionError(_e)

  # Set pandas DataFrame ready for comparisons
  df_terminaltable = _catalogue_integrity(df_terminaltable)

  # Distance to port
  # Add a new column to terminals table with the distance to ports
  df_terminaltable['dist_to_port'] = np.nan

  # Evaluate site distance to ports
  port_farm_dist = list()
  for x in df_terminaltable.index:
    port_coord = (df_terminaltable.at[x, 'latitude'],
                  df_terminaltable.at[x, 'longitude'])
    dist_to_port = geo_dist.distance(port_coord, opx.site.coord)
    port_farm_dist.append(int(dist_to_port.m))
  port_farm_dist = pd.Series(port_farm_dist)
  df_terminaltable['dist_to_port'] = port_farm_dist

  port_farm_dist = df_terminaltable['dist_to_port'].astype(float)   # In meters
  series_dist = (port_farm_dist / 1000) < opx.requirements['port_max_dist']
  df_terminaltable = df_terminaltable[series_dist]
  if df_terminaltable.empty is True:
    _e = 'For operation \"%s\", all Terminals are to far away' % opx.name
    raise AssertionError(_e)

  # Previous experience requirement
  if opx.requirements['prev_proj'] is True:
    series_experience = df_terminaltable['previous_projects'] == True
    df_terminaltable = df_terminaltable[series_experience]
  if df_terminaltable.empty is True:
    _e = 'For operation \"%s\", there are no Terminals with experience' % opx.name
    raise AssertionError(_e)

  # Drydock requirements
  if 'dry_dock' in opx.requirements and opx.requirements['dry_dock'] is True:
    c1 = df_terminaltable['type'].str.contains('dry')
    c2 = df_terminaltable['type'].str.contains('dock')
    cond = c1 & c2
    df_terminaltable = df_terminaltable[cond]
    if df_terminaltable.empty is True:
      _e = 'For operation \"%s\", there are no Terminals with dry dock' % opx.name
      raise AssertionError(_e)

  # Slipway requirement
  if 'slipway' in opx.requirements and opx.requirements['slipway'] is True:
    series_experience = df_terminaltable['slip'] == True
    df_terminaltable = df_terminaltable[series_experience]
    if df_terminaltable.empty is True:
      _e = 'For operation \"%s\", there are no Terminals with slipway' % opx.name
      raise AssertionError(_e)

  # Terminal lift capacity
  # Terminal must have lift capacity when:
  #   - requirements['terminal_crane'] = True AND
  #         methods['load_out'] = 'lift' AND methods['transport'] = 'wet'
  if opx.requirements['terminal_crane'] is True:
    if 'lift' in opx.requirements:
      try:
        if ('lift' in opx.methods['load_out'] and
            'wet' in opx.methods['transport']):
          lift_req = opx.requirements['lift'] * sf
          c1 = df_terminaltable['gantry_crane_lift'] >= lift_req
          c2 = df_terminaltable['tower_crane_lift'] >= lift_req
          cond = c1 & c2
          df_terminaltable = df_terminaltable[cond]
          if df_terminaltable.empty is True:
            _e = 'For operation \"%s\", there are no cranes powerfull enough in any Terminal' % opx.name
            raise AssertionError(_e)
      except KeyError:
        # load_out method or transport method not in methods
        pass

  # Terminal area requirement
  if opx.requirements['terminal_area'] is True:
    if 'area' in opx.requirements:
      base_area_min = opx.requirements['area']
      area_as_float = df_terminaltable['area'].astype(float)
      series_area = base_area_min * (1 + sf) <= area_as_float
      df_terminaltable = df_terminaltable[series_area]
      if df_terminaltable.empty is True:
        _e = 'For operation \"%s\", there are no enougth area in any Terminal' % opx.name
        raise AssertionError(_e)

  # Terminal load requirements
  if opx.requirements['terminal_load'] is True:
    if 'strength' in opx.requirements:
      strength_min = opx.requirements['strength']
      load_as_float = df_terminaltable['load'].astype(float)
      series_load = strength_min * (1 + sf) <= load_as_float
      df_terminaltable = df_terminaltable[series_load]
      if df_terminaltable.empty is True:
        _e = 'For operation \"%s\", there are no Terminals that can hold the objects/cables' % opx.name
        raise AssertionError(_e)

  # Wet tow component - minimum draught requirement
  if ('transport'  in opx.methods and
      'wet' in opx.methods['transport'] and
      'terminal_draught' in opx.requirements):
    draught_min = opx.requirements['terminal_draught']
    draught_as_float = df_terminaltable['draught'].astype(float)
    series_draught = draught_min * (1 + sf) <= draught_as_float
    df_terminaltable = df_terminaltable[series_draught]
    if df_terminaltable.empty is True:
      _e = 'For operation \"%s\", there are no Terminals with enough depth for wet tow' % opx.name
      raise AssertionError(_e)

  feasible_terminals = df_terminaltable
  if feasible_terminals.empty is True:
    _e = 'For operation \"%s\", there are no feasible Terminals' % opx.name
    raise AssertionError(_e)
  feasible_terminals = feasible_terminals.reset_index(drop=True)

  return feasible_terminals


def _catalogue_integrity(catalogue: pd.DataFrame):
  """
  _catalogue_integrity: Lowers every character from the input catalogue and
  converts every number to numeric

  Inputs:
  -------
  catalogue = Catalogue to be converted - pandas Dataframe

  Returns:
  --------
  catalogue = Catalogue updated - pandas Dataframe
  """
  # Converts string numbers to numerics
  catalogue = catalogue.apply(pd.to_numeric, errors='ignore')
  # Convert string to lower strings
  catalogue = catalogue.applymap(lambda x: x.lower()
                                 if isinstance(x, str) else x)
  # Convert string to boolean
  catalogue = catalogue.applymap(lambda x: True
                                 if (isinstance(x, str) and
                                     x.lower() == 'true')
                                 else x)
  catalogue = catalogue.applymap(lambda x: False
                                 if (isinstance(x, str) and
                                     x.lower() == 'false')
                                 else x)
  catalogue.columns = map(str.lower, catalogue.columns)
  catalogue = catalogue.dropna(how='all')
  catalogue = catalogue.reset_index(drop=True)

  return catalogue
