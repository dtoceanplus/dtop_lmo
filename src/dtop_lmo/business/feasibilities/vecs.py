# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import os

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_vecs


def vecs(
    opx,
    df_vecs: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_vec
  ----------------
  This functions checks if a given VEC can preform a given operation
  Inputs:
  -------
  * opx = Operation X - Operation class
  Optional Inputs:
  ----------------
  * df_vecs = Vessels & Equipment Combinations - pandas DataFrame
  Outputs:
  --------
  * feasible_vecs = Feasible Vessels & Equipment Combinations - pandas
  DataFrame
  """
  # If no df_vecs is specified, VECs from Catalogue are considered
  try:
    df_vecs.head()
    # df_vecs was introduced as a pandas DataFrame
    df_vectable = df_vecs
  except AttributeError:
    try:
      df_vectable = get_catalog_vecs()
    except ConnectionError:
      path_catalogue_vecs = os.path.join(os.getcwd(), 'catalogues', 'VECs.csv')
      df_vectable = pd.read_csv(path_catalogue_vecs, sep=',')

  # Set pandas DataFrame ready for comparisons
  df_vectable = _catalogue_integrity(df_vectable)

  op_name = opx.name.lower()
  op_desc = opx.description.lower()

  # Check if Operation's name is in the type of each VEC
  c1 = [op_name in row['type'] for idx, row in df_vectable.iterrows()]
  if any(c1) is False:
    # No VEC was found for this operation name
    # Check if it works with op_desc (cpx1)
    c1 = [op_desc in row['type'] for idx, row in df_vectable.iterrows()]
    if any(c1) is False:
      _e = 'No feasible VEC was found for operation \"%s\"' % opx.name
      raise AssertionError(_e)
  # Check if Operation's description is in the description of each VEC
  try:
    c2 = [op_desc in row['description'] for idx, row in df_vectable.iterrows()]
  except KeyError:
    c2 = [True] * df_vectable.shape[0]
  if any(c2) is False:
    # No VEC was found for this description
    _e = 'No feasible VEC was found for description \"%s\"' % opx.description
    raise AssertionError(_e)
  # Check if Operation transportation method is required
  if 'transport' in opx.methods:
    if opx.methods['transport'] == 'dry':
      # Check if 'on deck' or 'dry tow' is in the Operation's transport method
      # of each VEC
      c3_1 = ['on deck' in row['transportation']
              for idx, row in df_vectable.iterrows()]
      c3_2 = ['dry tow' in row['transportation']
              for idx, row in df_vectable.iterrows()]
      c3_zip = zip(c3_1, c3_2)
      c3 = [c1 or c2 for c1, c2 in c3_zip]
    elif opx.methods['transport'] == 'wet':
      # Check if 'wet' is in the Operation's transport method of each VEC
      c3 = ['wet' in row['transportation']
            for idx, row in df_vectable.iterrows()]
  else:
    c3 = [True] * df_vectable.shape[0]

  # This way, VECs are filtered by description and by transportation method
  cond_zip = zip(c1, c2, c3)
  cond = [c1 and c2 and c3 for c1, c2, c3 in cond_zip]
  df_vectable = df_vectable[cond]    # VECs Filtered
  if df_vectable.empty is True:
    _e = 'For operation \"%s\", there are no feasible VECs' % opx.name
    raise AssertionError(_e)
  df_vectable = df_vectable.reset_index(drop=True)

  feasible_vecs = df_vectable

  return feasible_vecs


def _catalogue_integrity(catalogue: pd.DataFrame):
  """
  _catalogue_integrity: Lowers every character from the input catalogue and
  converts every number to numeric

  Inputs:
  -------
  catalogue = Catalogue to be converted - pandas Dataframe

  Returns:
  --------
  catalogue = Catalogue updated - pandas Dataframe
  """
  # Converts string numbers to numerics
  catalogue = catalogue.apply(pd.to_numeric, errors='ignore')
  # Convert string to lower strings
  catalogue = catalogue.applymap(lambda x: x.lower()
                                 if isinstance(x, str) else x)
  # Convert string to boolean
  catalogue = catalogue.applymap(lambda x: True
                                 if (isinstance(x, str) and
                                     x.lower() == 'true')
                                 else x)
  catalogue = catalogue.applymap(lambda x: False
                                 if (isinstance(x, str) and
                                     x.lower() == 'false')
                                 else x)
  catalogue.columns = map(str.lower, catalogue.columns)
  catalogue = catalogue.dropna(how='all')
  catalogue = catalogue.reset_index(drop=True)

  return catalogue
