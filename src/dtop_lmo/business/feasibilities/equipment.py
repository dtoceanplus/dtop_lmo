# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import os

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_rovs
from dtop_lmo.service.api.catalogues import get_catalog_divers
from dtop_lmo.service.api.catalogues import get_catalog_equip_burial
from dtop_lmo.service.api.catalogues import get_catalog_equip_piling


def rov(
    opx,
    sf,
    df_rov: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_equip_rov
  ---------------------
  This function checks, for a given Operation, which ROVs could be used
  to preform the operation taking into account the operation requirements
  and the components involved.
  Inputs:
  -------
  * opx = Operation X - Operation class
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * df_rov = List of available ROVs - pandas DataFrame
  Outputs:
  --------
  * feasible_equip_rov = Feasible ROVs - pandas DataFrame
  """
  # If no df_rov is specified, ROVs from Catalogue are considered
  try:
    df_rov.head()
    # df_rov was introduced as a pandas DataFrame
    df_rovtable = df_rov
  except AttributeError:
    try:
      df_rovtable = get_catalog_rovs()
    except ConnectionError:
      path_catalogue_rovs = os.path.join(os.getcwd(), 'catalogues', 'Equip_rov.csv')
      df_rovtable = pd.read_csv(path_catalogue_rovs, sep=',')

  # Set pandas DataFrame ready for comparisons
  df_rovtable = _catalogue_integrity(df_rovtable)

  # Filter by ROV class
  if opx.requirements['rov'] is not None:
    rov_class = opx.requirements['rov']
    df_rovtable = df_rovtable[df_rovtable['_class'] == rov_class]
  else:
    _e = 'For operation \"%s\", ROV feasibility should be run' % opx.name
    raise AssertionError(_e)

  # Filter by maximum operation depth
  depth_min = opx.requirements['depth_max']
  depth_as_float = df_rovtable['max_depth'].astype(float)
  df_rovtable = df_rovtable[depth_as_float >= depth_min]

  if df_rovtable.empty is True:
    _e = 'For operation \"%s\", there are no feasible ROVs' % opx.name
    raise AssertionError(_e)

  feasible_equip_rov = df_rovtable
  feasible_equip_rov = feasible_equip_rov.reset_index(drop=True)

  return feasible_equip_rov


def divers(
    opx,
    sf,
    df_divers: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_equip_divers
  ------------------------
  This function checks, for a given Operation, which team of Divers
  could be considered to preform the operation taking into account the
  operation requirements.
  Inputs:
  -------
  * opx = Operation X - Operation class
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * df_divers = List of available Divers teams - pandas DataFrame
  Outputs:
  --------
  * feasible_equip_divers = Feasible teams of Drivers - pandas DataFrame
  """
  # If no df_divers is specified, Divers from Catalogue are considered
  try:
    df_divers.head()
    # df_divers was introduced as a pandas DataFrame
    df_diverstable = df_divers
  except AttributeError:
    try:
      df_diverstable = get_catalog_divers()
    except ConnectionError:
      path_catalogue_divers = os.path.join(os.getcwd(), 'catalogues', 'Equip_divers.csv')
      df_diverstable = pd.read_csv(path_catalogue_divers, sep=',')

  # Set pandas DataFrame ready for comparisons
  df_diverstable = _catalogue_integrity(df_diverstable)

  # Filter by maximum operation depth
  depth_max = opx.requirements['depth_max']
  # ### PREVIOUSE IT WAS depth_max = opx.requirements['objects_depth']
  depth_as_float = df_diverstable['max_depth'].astype(float)
  df_diverstable = df_diverstable[depth_as_float >= depth_max]

  if df_diverstable.empty is True:
    _e = 'For operation \"%s\", there are no feasible Divers' % opx.name
    raise AssertionError(_e)

  feasible_equip_divers = df_diverstable
  feasible_equip_divers = feasible_equip_divers.reset_index()

  return feasible_equip_divers


def burial(
    opx,
    sf,
    df_burial: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_equip_burial
  --------------------
  This function checks, for a given Operation, which Burial tool could be
  considered to preform the operation taking into account the operation
  requirements and the cable

  Inputs:
  -------
  * opx = Operation X - Operation class
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * df_burial = List of available Burial tools - pandas DataFrame

  Outputs:
  --------
  * feasible_equip_burial = Feasible Burial tools - pandas DataFrame
  """
  # If no df_burial is specified, Burial tools from Catalogue are considered
  try:
    df_burial.head()
    # df_burial was introduced as a pandas DataFrame
    df_burialtable = df_burial
  except AttributeError:
    try:
      df_burialtable = get_catalog_equip_burial()
    except ConnectionError:
      path_catalogue_burial = os.path.join(os.getcwd(), 'catalogues', 'Equip_burial.csv')
      df_burialtable = pd.read_csv(path_catalogue_burial, sep=',')


  # Set pandas DataFrame ready for comparisons
  df_burialtable = _catalogue_integrity(df_burialtable)

  max_diameter = opx.requirements['cable_diameter_max']
  max_burial_depth = opx.requirements['cable_depth']
  max_bathymetry_depth = opx.requirements['depth_max']
  max_mbr = opx.requirements['mbr']

  # Filter by opx.methods['burial'] ('plough', 'jet' or 'cut') and by
  # technique max depth
  if 'plough' in opx.methods['burial']:
    # Condition 1
    c1 = pd.Series(df_burialtable['capabilities_ploughing'] == True)
    # Condition 2
    max_depth_ploughing = df_burialtable['max_depth_ploughing'].astype(float)
    c2 = pd.Series(max_depth_ploughing >= max_burial_depth)

  elif 'jet' in opx.methods['burial']:
    # Condition 1
    c1 = pd.Series(df_burialtable['capabilities_jetting'] == True)
    # Condition 2
    max_depth_jetting = df_burialtable['max_depth_jetting'].astype(float)
    c2 = pd.Series(max_depth_jetting >= max_burial_depth)

  elif 'cut' in opx.methods['burial']:
    # Condition 1
    c1 = pd.Series(df_burialtable['capabilities_cutting'] == True)
    # Condition 2
    max_depth_cutting = df_burialtable['max_depth_cutting'].astype(float)
    c2 = pd.Series(max_depth_cutting >= max_burial_depth)

  else:
    _e = 'For operation \"%s\", burial method is not recognized' % opx.name
    raise AssertionError(_e)

  cond = c1 & c2
  df_burialtable = df_burialtable[cond]
  del c1, c2, cond

  # Filter by maximum depth
  depth_as_float = df_burialtable['max_depth'].astype(float)
  df_burialtable = df_burialtable[depth_as_float >= max_bathymetry_depth]

  # Filter by maximum cable diameter
  max_cable_diam = df_burialtable['max_cable_diam'].astype(float)
  df_burialtable = df_burialtable[max_cable_diam >= max_diameter]

  # Filter by cable minimum bend radius
  min_cable_bend = df_burialtable['min_cable_bend'].astype(float)
  df_burialtable = df_burialtable[min_cable_bend >= max_mbr]

  if df_burialtable.empty is True:
    _e = 'For operation \"%s\", there are no feasible Burial ' % opx.name
    _e = _e + 'tools'
    raise AssertionError(_e)

  feasible_equip_burial = df_burialtable
  feasible_equip_burial = feasible_equip_burial.reset_index(drop=True)

  return feasible_equip_burial


def piling(
    opx,
    sf,
    df_piling: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_equip_piling
  -------------------------
  This function checks, for a given Operation, which piling tools are more
  feasible to preform the operation.

  Inputs:
  -------
  * opx = Operation X - Operation class
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * df_piling = List of available piling tools - pandas DataFrame
  Outputs:
  --------
  * feasible_equip_piling = Feasible Burial tools - pandas DataFrame
  """
  # If no df_piling is specified, piling tools from Catalogue are
  # considered
  try:
    df_piling.head()
    # df_burial was introduced as a pandas DataFrame
    df_pilingtable = df_piling
  except AttributeError:
    try:
      df_pilingtable = get_catalog_equip_piling()
    except ConnectionError:
      path_catalogue_piling = os.path.join(os.getcwd(), 'catalogues', 'Equip_piling.csv')
      df_pilingtable = pd.read_csv(path_catalogue_piling, sep=',')

  # Set pandas DataFrame ready for comparisons
  df_pilingtable = _catalogue_integrity(df_pilingtable)

  piling_method = opx.methods['piling']
  max_depth = opx.requirements['depth_max']
  max_piling_depth = opx.requirements['piling_max']
  max_diameter = opx.requirements['object_diameter_max']
  min_diameter = opx.requirements['object_diameter_min']
  max_mass = opx.requirements['lift']

  # Condition 0 - piling method
  piling_as_str = df_pilingtable['type'].astype(str)
  if 'hammer' in piling_method:
    c0 = df_pilingtable['type'].str.contains('hammer')
  elif 'drill' in piling_method:
    c0 = df_pilingtable['type'].str.contains('drill')
  elif 'vibro' in piling_method:
    c0 = df_pilingtable['type'].str.contains('vibro')
  else:
    _e = 'For operation %s, pilling method unrecognized' % opx.name
    raise AssertionError(_e)

  # Condition 1 - max depth
  depth_as_float = df_pilingtable['max_depth'].astype(float)
  c1 = pd.Series(depth_as_float >= max_depth)

  df_piling_hammer = df_pilingtable[df_pilingtable['type'].str.contains('hammer')]
  df_piling_drill = df_pilingtable[df_pilingtable['type'].str.contains('drill')]
  df_piling_vibro = df_pilingtable[df_pilingtable['type'].str.contains('vibro')]

  c2_1 = df_piling_hammer['hammer_max_diam'].astype(float) >= max_diameter
  c2_2 = df_piling_hammer['hammer_min_diam'].astype(float) <= min_diameter
  c2 = c2_1 & c2_2

  c3_1 = df_piling_drill['drilling_max_diam'].astype(float) >= max_diameter
  c3_2 = df_piling_drill['drilling_min_diam'].astype(float) <= min_diameter
  c3 = c3_1 & c3_2

  c4_1 = df_piling_vibro['vibro_max_pile_diam'].astype(float) >= max_diameter
  c4_2 = df_piling_vibro['vibro_min_pile_diam'].astype(float) <= min_diameter
  c4_3 = df_piling_vibro['vibro_max_pile_weight'].astype(float) >= max_mass
  c4 = c4_1 & c4_2 & c4_3

  c_final = c2.append(c3)
  c_final = c_final.append(c4)

  df_pilingtable = df_pilingtable[c0 & c1 & c_final]
  del c0, c1, c2_1, c2_2, c2, c3_1, c3_2, c3, c4_1, c4_2, c4_3, c4, c_final

  if df_pilingtable.empty is True:
    _e = 'For operation \"%s\", there are no feasible piling ' % opx.name
    _e = _e + 'equipment'
    raise AssertionError(_e)

  feasible_equip_piling = df_pilingtable
  feasible_equip_piling = feasible_equip_piling.reset_index(drop=True)

  return feasible_equip_piling


def _catalogue_integrity(catalogue: pd.DataFrame):
  """
  _catalogue_integrity: Lowers every character from the input catalogue and
  converts every number to numeric

  Inputs:
  -------
  catalogue = Catalogue to be converted - pandas Dataframe

  Returns:
  --------
  catalogue = Catalogue updated - pandas Dataframe
  """
  # Converts string numbers to numerics
  catalogue = catalogue.apply(pd.to_numeric, errors='ignore')
  # Convert string to lower strings
  catalogue = catalogue.applymap(lambda x: x.lower()
                                 if isinstance(x, str) else x)
  # Convert string to boolean
  catalogue = catalogue.applymap(lambda x: True
                                 if (isinstance(x, str) and
                                     x.lower() == 'true')
                                 else x)
  catalogue = catalogue.applymap(lambda x: False
                                 if (isinstance(x, str) and
                                     x.lower() == 'false')
                                 else x)
  catalogue.columns = map(str.lower, catalogue.columns)
  catalogue = catalogue.dropna(how='all')
  catalogue = catalogue.reset_index(drop=True)

  return catalogue
