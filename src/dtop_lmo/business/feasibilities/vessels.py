# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import warnings
import os

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_vessels

warnings.simplefilter("ignore")


def vessels(
    opx,
    vecs,
    sf,
    percentile: str = 'p50',
    df_vessels: pd.DataFrame = None
) -> pd.DataFrame:
  """
  feasibility_vessels
  -------------------
  This function checks, for a given Operation and a set of VECs,
  which vessels are feasible to preform the operation.

  Inputs:
  -------
  * opx = Operation X - Operation class
  * vecs = Set of VECs - pandas DataFrame
  * sf = Safety factor - float
  Optional Inputs:
  ----------------
  * percentile =  - string
  * df_vessels = List of available vessels - pandas DataFrame
  Outputs:
  --------
  feasible_vessels = All feasible vessels for this operation - Dictionary of
  pandas DataFrame
    * main = Feasible main vessels - pandas DataFrame
    * tow = Feasible tow vessels - pandas DataFrame
    * support = Feasible support vessels - pandas DataFrame
  """
  # If no df_vessels is specified, Vessels from Catalogue are considered
  try:
    df_vessels.head()
    # df_vessels was introduced as a pandas DataFrame
    df_vessels = df_vessels
  except AttributeError:
    try:
      df_vessels = get_catalog_vessels()
    except ConnectionError:
      path_catalogue_vessels = os.path.join(os.getcwd(), 'catalogues', 'Vessels.csv')
      df_vessels = pd.read_csv(path_catalogue_vessels, sep=',')

  df_vesselpXX = _read_vessel_clusters(df_vessels, percentile)

  # Set pandas DataFrame ready for comparisons
  df_vesselpXX = _catalogue_integrity(df_vesselpXX)

  # contingency = 1.0     # Previous
  contingency = 1 - sf
  # Under Keel Clearance: https://safeshippingbc.ca/?page_id=231
  contingency_UDK = 1.1

  list_trues = [True] * 10
  c0, c1, c2, c3, c4, c5, c6, c7, c8, c9 = pd.Series(list_trues)

  bathymetry_min = opx.requirements['depth_min']
  bathymetry_max = opx.requirements['depth_max']

  # First (and universal) condition: depth clearance # and positioning
  # Condition 0 - Vessel under keel clearance
  draft_as_float = df_vesselpXX['draft'].astype(float)
  c0 = pd.Series(draft_as_float * contingency_UDK <= bathymetry_min)
  # Condition 1 - Jack-up max water depth
  jup_max_water = df_vesselpXX['jup_max_water'].astype(float)
  c1_list = [jup_max_water[idx] >= bathymetry_max
             if row['jackup_capabilities'] == True
             else True
             for idx, row in df_vesselpXX.iterrows()]
  c1 = pd.Series(c1_list)

  cond = c0 & c1

  df_feasible_vessels = df_vesselpXX[cond].reset_index(drop=True)
  if df_feasible_vessels.empty:
    _e = 'No feasible vessels. Draft may be to high or they may require a terminal wiht Jack-up capapabilities. '
    _e += 'Operation %s - %s.' % (opx.id, opx.name)
    raise Exception(_e)

  # Condition 3 - Returns a pandas Series with Trues and Falses if the vessel
  # type in Vessels Catalogue matches any main vessel type from the VECs
  cond3 = []
  # Loop through vessels
  for idx, row in df_feasible_vessels.iterrows():
    vessel = row['vessel_type']
    # We assume that 'vessel' doesn't have the same type of any main vessel
    same_type = False
    # Separete all words of vessel type from vessel Catalogue
    vessel = vessel.split()
    vessel = [word.split('-') for word in vessel]
    # Flat list
    vessel = [word for list_words in vessel for word in list_words]
    vessel = sorted(vessel)
    for idx2, row2 in vecs.iterrows():
      vessel_type = row2['main_vessel']
      # First check if there is a main vessel for this VEC
      if isinstance(vessel_type, str) is True:
        # Check if it a single vessel or a set of vessels
        # Example: AHTS / Tug
        vessel_types = vessel_type.split(' / ')
        for vessel_x in vessel_types:
          # Separete all words of vessel_type from VECs catalogue
          vessel_x = vessel_x.split()
          vessel_x = [word.split('-') for word in vessel_x]
          # Flat list
          vessel_x = [word for list_words in vessel_x for word in list_words]
          vessel_x = sorted(vessel_x)
          if vessel_x == vessel:
            same_type = True
            break
      else:
        same_type = True
    cond3.append(same_type)
  c3 = pd.Series(cond3)

  # Condition 4 - Vessel type in VECs[towing]
  c4 = pd.Series([any([(row['vessel_type'] in row2['tow_vessel'] or
                        row2['tow_vessel'] in row['vessel_type'])
                       if isinstance(row2['tow_vessel'], str)
                       else False
                       for idx2, row2 in vecs.iterrows()])
                  for idx, row in df_feasible_vessels.iterrows()])

  # Condition 5 - Vessel type in VECs[support]
  c5 = pd.Series([any([(row['vessel_type'] in row2['support_vessel'] or
                        row2['support_vessel'] in row['vessel_type'])
                       if isinstance(row2['support_vessel'], str)
                       else False
                       for idx2, row2 in vecs.iterrows()])
                  for idx, row in df_feasible_vessels.iterrows()])

  df_vessels_main = df_feasible_vessels[c3].reset_index(drop=True)
  df_vessels_tow = df_feasible_vessels[c4].reset_index(drop=True)
  df_vessels_support = df_feasible_vessels[c5].reset_index(drop=True)

  if 'description' in vecs.columns.tolist():
    vecs_columns = ['vec_id', 'type', 'description', 'transportation']
  else:
    # Complexity 1
    vecs_columns = ['vec_id', 'type', 'transportation']
  vessel_columns = vecs_columns + df_vessels_main.columns.tolist()

  vess_sol_main = pd.DataFrame(columns=vessel_columns)
  vess_sol_tow = pd.DataFrame(columns=vessel_columns)
  vess_sol_support = pd.DataFrame(columns=vessel_columns)

  for ind in vecs.index:
    # First, save VEC info: ID, Operation type, Operation description and
    # Transportation method
    series_vec_info = vecs.loc[ind, vecs_columns]

    # Check if there is any Main vessel
    if vecs['qnt1'][ind] > 0:
      # Check if vessels characteristics are feasible
      # Vessel name is the same of the main_vessel column in VECs
      c1_0_list = []
      for idx in df_vessels_main.index:
        vessel_in_vec = False
        if isinstance(vecs['main_vessel'][ind], str) is True:
          # Vessel from Vessels Catalogue
          vessel_vessels = df_vessels_main['vessel_type'][idx].split()
          vessel_vessels = [word.split('-') for word in vessel_vessels]
          # Flat list
          vessel_vessels = [word
                            for list_words in vessel_vessels
                            for word in list_words]
          vessel_vessels = sorted(vessel_vessels)

          vessels_vecs = vecs['main_vessel'][ind].split(' / ')
          for vessel_x in vessels_vecs:
            # Vessel in VECs Catalogue
            vessel_x = vessel_x.split()
            vessel_x = [word.split('-') for word in vessel_x]
            # Flat list
            vessel_x = [word
                        for list_words in vessel_x
                        for word in list_words]
            vessel_x = sorted(vessel_x)

            # Vessel from Vessels Catalogue
            vessel_vessels = df_vessels_main['vessel_type'][idx].split()
            vessel_vessels = [word.split('-') for word in vessel_vessels]
            # Flat list
            vessel_vessels = [word
                              for list_words in vessel_vessels
                              for word in list_words]
            vessel_vessels = sorted(vessel_vessels)
            if vessel_x == vessel_vessels:
              vessel_in_vec = True
              break
        c1_0_list.append(vessel_in_vec)
      c1_0 = pd.Series(c1_0_list)
      if not c1_0.any():
        _e = 'No "%s" found in vessels catalogue for operation %s - %s.' % (vecs['main_vessel'][ind], opx.id, opx.name)
        raise Exception(_e)

      # Dynamic Positioning (DP) requirements
      c1_1_list = [df_vessels_main['dp'][idx] >= opx.requirements['dp']
                   if 'dp' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_1 = pd.Series(c1_1_list)
      if not c1_1.any():
        _e = '%s vessels have low DP. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['dp']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)
      # Deck area requirements (always required for installation vessel)
      free_deck = df_vessels_main['free_deck'].astype(float)
      c1_2_list = [free_deck[idx] * contingency >= opx.requirements['area']
                   if 'area' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_2 = pd.Series(c1_2_list)
      if not c1_2.any():
        _e = '%s vessels have low free_deck. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['area']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)
      # Deck strength requirements
      deck_str = df_vessels_main['deck_str'].astype(float)
      c1_3_list = [deck_str[idx] * contingency >= opx.requirements['strength']
                   if 'strength' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_3 = pd.Series(c1_3_list)
      if not c1_3.any():
        _e = '%s vessels have low deck_str. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['strenght']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)

      # Crane capacity requirements
      crane_capacity = df_vessels_main['crane_capacity'].astype(float)
      try:
        c1_4_list = [crane_capacity[idx] * contingency >= opx.requirements['lift']
                     if ('lift' in opx.requirements and
                         ('dry' in opx.methods['transport'] or
                          'lift' not in opx.methods['load_out']))
                     else True
                     for idx in df_vessels_main.index]
      except KeyError:
        # 'transport' or 'load_out' is not in methods
        c1_4_list = [crane_capacity[idx] * contingency >= opx.requirements['lift']
                     if 'lift' in opx.requirements
                     else True
                     for idx in df_vessels_main.index]
      c1_4 = pd.Series(c1_4_list)
      if not c1_4.any():
        _e = '%s vessels have low crane_capacity. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['lift']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)

      # Turntable capacity requirements
      turn_capacity = df_vessels_main['turn_capacity'].astype(float)
      c1_5_list = [turn_capacity[idx] * contingency >= opx.requirements['turn_capacity']
                   if 'turn_capacity' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_5 = pd.Series(c1_5_list)
      if not c1_5.any():
        _e = '%s vessels have low turn_capacity. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['turn_capacity']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)
      # Turntable storage requirements
      turn_storage = df_vessels_main['turn_storage'].astype(float)
      c1_6_list = [turn_storage[idx] * contingency >= opx.requirements['turn_storage']
                   if 'turn_storage' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_6 = pd.Series(c1_6_list)
      if not c1_6.any():
        _e = '%s vessels have low turn_storage. Requirement: %s + contingency. ' % (vecs['main_vessel'][ind], str(opx.requirements['turn_storage']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)

      # ROV requirements
      rov_ready = df_vessels_main['rov_ready'].astype(float)
      c1_7_list = [rov_ready[idx] >= 0.5   # Meaning True
                   if (opx.requirements['rov'] is not None and
                       'work' in opx.requirements['rov'])
                   else True
                   for idx in df_vessels_main.index]
      c1_7 = pd.Series(c1_7_list)
      if not c1_7.any():
        _e = '%s vessels do not have rov_ready. Requirement: %s. ' % (vecs['main_vessel'][ind], str(opx.requirements['rov']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)

      # Number of passengers in CTV
      df_vessels_main['passengers'] = df_vessels_main['passengers'].fillna(0)
      passengers = df_vessels_main['passengers'].astype(int)
      c1_8_list = [passengers[idx] >= opx.requirements['passengers']
                   if 'passengers' in opx.requirements
                   else True
                   for idx in df_vessels_main.index]
      c1_8 = pd.Series(c1_8_list)
      if not c1_8.any():
        _e = '%s vessels have low passengers number. Requirement: %s. ' % (vecs['main_vessel'][ind], str(opx.requirements['passengers']))
        _e += 'Operation %s - %s.' % (opx.id, opx.name)
        raise Exception(_e)

      # Aggregate conditions for main vessels
      cond1 = c1_0 & c1_1 & c1_2 & c1_3 & c1_4 & c1_5 & c1_6 & c1_7 & c1_8

      # Vessels filtered for this VEC main vessel type and all the other
      # conditions
      df_vec_vessels_main = df_vessels_main[cond1]

      # Add VEC information to df_vec_vessels_main DataFrame
      vec_info = vecs.loc[ind, vecs_columns]

      df_vecs_info = pd.DataFrame(index=df_vec_vessels_main.index,
                                  columns=vecs_columns)
      for i in range(0, len(vecs_columns)):
        df_vecs_info.iloc[:, i] = vec_info[i]
      df_vec_vessels_main = pd.concat([df_vecs_info, df_vec_vessels_main],
                                      axis=1,
                                      sort=False)

      vess_sol_main = vess_sol_main.append(df_vec_vessels_main)

    if vecs['qnt2'][ind] > 0:
      # Check if vessels characteristics are feasible
      # Vessel name is the same of the tow_vessel column in VECs
      c2_0_list = [df_vessels_tow['vessel_type'][idx] in vecs['tow_vessel'][ind]
                   if isinstance(vecs['tow_vessel'][ind], str) is True
                   else False
                   for idx in df_vessels_tow.index]
      c2_0 = pd.Series(c2_0_list)
      if not c2_0.any():
        _e = 'No "%s" found in vessels catalogue for operation %s - %s.' % (vecs['tow_vessel'][ind], opx.id, opx.name)
        raise Exception(_e)
      # Dynamic Positioning (DP) requirements
      # c2_1_list = [df_vessels_tow['dp'][idx] >= opx.requirements['dp']
      #              if 'dp' in opx.requirements
      #              else True
      #              for idx in df_vessels_tow.index]
      # ### JSUT FOR TESTING!!!
      c2_1_list = [True] * df_vessels_tow.shape[0]    # DELETE THIS
      c2_1 = pd.Series(c2_1_list)
      # Bollard Pull requirements
      c2_2_list = [True] * df_vessels_tow.shape[0]
      if 'wet' in vecs['transportation'][ind]:
        c2_2_list = list()
        cx = 0
        for idx in df_vessels_tow.index:
          # Bollard pull requirement for wet tows
          vessel_bp = df_vessels_tow.loc[idx, 'bollard']
          vessel_loa = df_vessels_tow.loc[idx, 'loa']
          try:
            bp_req, _, _, _ = opx.bollard_pull_finder(vessel_bp, vessel_loa)
          except AttributeError:
            # CPX1 operation
            bp_req = 0

          # Number of tugs/ahts
          n_tugs = int(vecs['qnt2'][ind])

          # ### THE BOLLARD PULL REQUIRED IF THERE ARE MORE THAN 1 TUG IS NOT
          # THIS LINEAR!!!
          cx = bp_req <= n_tugs * df_vessels_tow['bollard'][idx].astype(float)
          c2_2_list.append(cx)

      c2_2 = pd.Series(c2_2_list)

      # Aggregate conditions for tow vessels
      cond2 = c2_0 & c2_1 & c2_2

      # Vessels filtered for this VEC towing vessel type and all the other
      # conditions
      df_vec_vessels_tow = df_vessels_tow[cond2]

      # Add VEC information to df_vec_vessels_tow DataFrame
      vec_info = vecs.loc[ind, vecs_columns]

      df_vecs_info = pd.DataFrame(index=df_vec_vessels_tow.index,
                                  columns=vecs_columns)
      for i in range(0, len(vecs_columns)):
        df_vecs_info.iloc[:, i] = vec_info[i]

      df_vec_vessels_tow = pd.concat([df_vecs_info, df_vec_vessels_tow],
                                     axis=1,
                                     sort=False)

      vess_sol_tow = vess_sol_tow.append(df_vec_vessels_tow)
    if vecs['qnt3'][ind] > 0:
      # Check if vessels characteristics are feasible
      # Vessel name is the same of the support_vessel column in VECs
      c3_0_list = [df_vessels_support['vessel_type'][idx] in vecs['support_vessel'][ind]
                   if isinstance(vecs['support_vessel'][ind], str) is True
                   else False
                   for idx in df_vessels_support.index]
      c3_0 = pd.Series(c3_0_list)
      if not c3_0.any():
        _e = 'No "%s" found in vessels catalogue for operation %s - %s.' % (vecs['support_vessel'][ind], opx.id, opx.name)
        raise Exception(_e)

      # Aggregate conditions for support vessels
      cond3 = c3_0

      # Vessels filtered for this VEC support vessel type and all the other
      # conditions
      df_vec_vessels_support = df_vessels_support[cond3]

      # Add VEC information to df_vec_vessels_support DataFrame
      vec_info = vecs.loc[ind, vecs_columns]

      df_vecs_info = pd.DataFrame(index=df_vec_vessels_support.index,
                                  columns=vecs_columns)
      for i in range(0, len(vecs_columns)):
        df_vecs_info.iloc[:, i] = vec_info[i]

      df_vec_vessels_support = pd.concat([df_vecs_info,
                                          df_vec_vessels_support],
                                         axis=1,
                                         sort=False)

      vess_sol_support = vess_sol_support.append(df_vec_vessels_support)

  vess_sol_main = vess_sol_main.reset_index(drop=True)
  vess_sol_tow = vess_sol_tow.reset_index(drop=True)
  vess_sol_support = vess_sol_support.reset_index(drop=True)

  if vess_sol_main.empty and vess_sol_tow.empty and vess_sol_support.empty:
    _e = 'For operation \"%s\", there are no feasible vessels.\n' % opx.name
    req_str = ''
    for req, value in opx.requirements.items():
      req_str += str(req) + '-' + str(value) + '; '
    req_str = req_str[:-2]
    _e += 'Requirements: ' + req_str
    raise AssertionError(_e)

  # Remove columns related to VECs
  # ### TBDone ###

  feasible_vessels = {
      "main": vess_sol_main,
      "tow": vess_sol_tow,
      "support": vess_sol_support
  }
  return feasible_vessels


def _read_vessel_clusters(df_vessels: pd.DataFrame,
                          percentile: str) -> pd.DataFrame:
  """
  read_vessel_clusters:
  ---------------------
  Reads a pandas DataFrame with a list of vessels with the following format:
  p25   p50   p75
  0.5 ; 0.6 ; 0.8
  and extracts the required quantile.

  Inputs:
  -------
  df_vessels = List of vessels = Pandas DataFrame
  percentile =  - string

  Outputs:
  -------
  ###
  """
  try:
    vesseltable = df_vessels.set_index(keys='id', drop=True)
  except KeyError:
    vesseltable = df_vessels

  vesselp50 = pd.DataFrame(columns=vesseltable.columns)
  vesselp25 = pd.DataFrame(columns=vesseltable.columns)
  vesselp75 = pd.DataFrame(columns=vesseltable.columns)

  # Separete main vessel table in 3 different tables
  df_columns = ['id_name', 'vessel_type', 'cluster', 'user_introduced',
                'jackup_capabilities']
  vesselp50[df_columns] = vesseltable[df_columns]
  vesselp25[df_columns] = vesseltable[df_columns]
  vesselp75[df_columns] = vesseltable[df_columns]

  df_columns_floats = [
      elem
      for elem in list(vesseltable.columns)
      if elem not in df_columns
  ]

  for col_name in df_columns_floats:
    try:
      try:
        df_percentiles = vesseltable[col_name].str.replace(' ','')
        df_percentiles = df_percentiles.str.split(';', expand=True)
      except:
        pass
      df_percentiles = df_percentiles.rename(columns={0: 'p25',
                                                      1: 'p50',
                                                      2: 'p75'})
      vesselp50[col_name] = df_percentiles['p50'].astype(float)
      vesselp25[col_name] = df_percentiles['p25'].astype(float)
      vesselp75[col_name] = df_percentiles['p75'].astype(float)
    except AttributeError:
      raise Exception('In vessels catalogue, could not convert column %s to percentiles' % col_name)

  if percentile == 'p25':
    return vesselp25
  elif percentile == 'p50':
    return vesselp50
  elif percentile == 'p75':
    return vesselp75
  else:
    _e = 'Unreadable percentile level \'%s\'. Choose \'p25\', ' % percentile
    _e = _e + '\'p50\' or \'p75\''
    raise ValueError(_e)


def _catalogue_integrity(catalogue: pd.DataFrame):
  """
  _catalogue_integrity: Lowers every character from the input catalogue and
  converts every number to numeric

  Inputs:
  -------
  catalogue = Catalogue to be converted - pandas Dataframe

  Returns:
  --------
  catalogue = Catalogue updated - pandas Dataframe
  """
  # Converts string numbers to numerics
  catalogue = catalogue.apply(pd.to_numeric, errors='ignore')
  # Convert string to lower strings
  catalogue = catalogue.applymap(lambda x: x.lower()
                                 if isinstance(x, str) else x)
  # Convert string to boolean
  catalogue = catalogue.applymap(lambda x: True
                                 if (isinstance(x, str) and
                                     x.lower() == 'true')
                                 else x)
  catalogue = catalogue.applymap(lambda x: False
                                 if (isinstance(x, str) and
                                     x.lower() == 'false')
                                 else x)
  catalogue.columns = map(str.lower, catalogue.columns)
  catalogue = catalogue.dropna(how='all')
  catalogue = catalogue.reset_index(drop=True)

  return catalogue
