# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
Summary:
  Import different business logics depending on the complexity selected by the
  user
"""

# Import logic for Early Stage - low complexity -> 1 (Non-experienced users)
from .cpx1 import Core1
from .cpx1 import Installation1
from .cpx1 import Maintenance1
from .cpx1 import Decommissioning1
# Import logic for Mid Stage - medium complexity -> 2 (Intermediate users)
# from .cpx2 import Core2
# from .cpx2 import Installation2
# from .cpx2 import Maintenance2
# Import logic for Late Stage - high complexity -> 3 (Experienced users)
from .cpx3 import Core3
from .cpx3 import Installation3
from .cpx3 import Maintenance3
from .cpx3 import Decommissioning3


class Core:
  """

  """
  @staticmethod
  def get_complexity(
      complexity,
      max_wait: int = 8760,
      comb_retain_ratio: float = 0.2,
      comb_retain_min: int = 10,
      quantile: float = 0.5,
      period: str = 'month',
      load_factor = 0.8,
      sfoc = 210,
      fuel_price = 515
  ):
    # Default values
    if max_wait is None:
      max_wait = 8760
    if comb_retain_ratio is None:
      comb_retain_ratio = 0.0
    if comb_retain_min is None:
      comb_retain_min = 5
    if quantile is None:
      quantile = 0.5
    if period is None:
      period = 'month'
    if load_factor is None:
      load_factor = 0.8
    if sfoc is None:
      sfoc = 210
    if fuel_price is None:
      fuel_price = 515

    try:
      if complexity == 'low':
        return Core1(
            quantile,
            period
        )
      # elif complexity == 'med':
        # return Core2(
        #     max_wait,
        #     comb_retain_ratio,
        #     comb_retain_min,
        #     quantile,
        #     period
        # )
      elif complexity == 'high' or complexity == 'med':
        return Core3(
            max_wait,
            comb_retain_ratio,
            comb_retain_min,
            quantile,
            period,
            load_factor,
            sfoc,
            fuel_price
        )
      # If complexity is not fount, raise an Assertion Error
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)


class Installation:
  """
  """
  @staticmethod
  def get_complexity(
      complexity: str,
      start_date: str = None,
      operations_hs: float = 1.8,
      post_burial: bool = False,
      period: str = 'month',
      hierarchy_et=None,
      hierarchy_ed=None,
      hierarchy_sk=None
  ):
    # Default values
    if operations_hs is None:
      operations_hs = 1.8
    if post_burial is None:
      post_burial = False
    if period is None:
      period = 'month'

    try:
      if complexity == 'low':
        return Installation1(
            start_date,
            operations_hs,
            period,
            hierarchy_et,
            hierarchy_ed,
            hierarchy_sk
        )
      # elif complexity == 'med':
      #   return Installation2(
      #       start_date,
      #       period
      #   )
      elif complexity == 'high' or complexity == 'med':
        return Installation3(
            start_date,
            post_burial,
            period,
            hierarchy_et,
            hierarchy_ed,
            hierarchy_sk
        )
      # If complexity is not fount, raise an Assertion Error
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)


class Maintenance:
  """

  """
  @staticmethod
  def get_complexity(
      complexity: str,
      start_date: str = None,
      t_life: int = None,
      operations_hs: float = 1.8,
      device_repair_at_port: bool = None,
      period: str = None,
      hierarchy_et=None,
      hierarchy_ed=None,
      hierarchy_sk=None
  ):
    # Default values
    if operations_hs is None:
      operations_hs = 1.8
    if device_repair_at_port is None:
      device_repair_at_port = False
    if period is None:
      period = 'month'

    try:
      if complexity == 'low':
        return Maintenance1(
            start_date,
            t_life,
            operations_hs,
            device_repair_at_port,
            period,
            hierarchy_et,
            hierarchy_ed,
            hierarchy_sk
        )
      # elif complexity == 'med':
        # return Maintenance2(
        #     # argument1,
        #     # argument2,
        #     # device_repair_at_port,
        #     # optional_argument2
        # )
      elif complexity == 'high' or complexity == 'med':
        return Maintenance3(
            start_date,
            t_life,
            device_repair_at_port,
            period,
            hierarchy_et,
            hierarchy_ed,
            hierarchy_sk
        )
      # If complexity is not fount, raise an Assertion Error
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)


class Decommissioning:
  """

  """
  @staticmethod
  def get_complexity(
      complexity: str,
      start_date: str = None,
      period: str = 'month'
  ):
    try:
      if complexity == 'low':
        return Decommissioning1(
            start_date=start_date,
            period=period
        )
      # elif complexity == 'med'':
        # return Decommissioning2(
        #     # argument1,
        #     # argument2,
        #     optional_argument1,
        #     optional_argument2
        # )
      elif complexity == 'high' or complexity == 'med':
        return Decommissioning3(
            start_date=start_date,
            period=period
        )
      # If complexity is not fount, raise an Assertion Error
      raise AssertionError('Complexity not found')

    except AssertionError as _e:
      print(_e)
