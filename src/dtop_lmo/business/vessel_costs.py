# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import libraries
import pandas as pd
import numpy as np
import warnings
warnings.simplefilter("ignore")


def vessel_daily_charter(ps_vessel: pd.Series):
  """
  vessel_daily_charter:
  This function returns the a given vessel charter costs.
  Author: FCF
    Last review: on 12/02/2020
  Some vessels are still missing:
    - survery vessel (possibly to be removed)
    - semi-sub
    - rock dumper
    - rib
    - guard vessel
    - dsv
    - dredger
  Input:
  ------
  * ps_vessel = Pandas series with all information about the vessel

  Output:
  -------
  * vessel_type = Type of the vessel - string
  * chart_costs = Vessel charter costs [€/day] - float ### Confirm units
  """
  vessel_type = ps_vessel['vessel_type'].lower()

  # Initialization
  chart_costs = np.nan

  if 'tug' in vessel_type:
    x = float(ps_vessel['bollard'])
    if x < 13:
      x = 13
      _w = 'Bollard pull falls outside of the defined cost_function domain'
      warnings.warn(_w)
    elif x > 80:
      x = 80
      _w = 'Bollard pull falls outside of the defined cost_function domain'
      warnings.warn(_w)

    if 13 <= x < 25:
      chart_costs = 151.34 * x - 467.47
    elif 25 <= x < 70:
      chart_costs = 2.18 * x + 3261.61
    elif 70 <= x <= 80:
      chart_costs = 508.57 * x - 32186
    else:
      raise ValueError('Bollard pull value error')

  elif 'ahts' in vessel_type:
    x = float(ps_vessel['bollard'])
    if x < 70:
      x = 70
      _w = 'Bollard pull falls outside of the defined cost_function domain'
      warnings.warn(_w)
    elif x > 338:
      x = 338
      chart_costs = -8.3 * 10**(-3) * x**2 + 114.90 * x - 261.87
      _w = 'Bollard pull falls outside of the defined cost_function domain'
      warnings.warn(_w)

    chart_costs = -8.3 * 10**(-3) * x**2 + 114.90 * x - 261.87

  elif ('multi' in vessel_type and 'cat' in vessel_type):
    x = float(ps_vessel['loa'])
    if x < 21:
      x = 21
      warnings.warn('LOA falls outside of the defined cost_function domain')
    elif x > 42:
      x = 42
      warnings.warn('LOA falls outside of the defined cost_function domain')

    if 21 <= x < 28:
      chart_costs = 63.23 * x + 1812.4
    elif 28 <= x < 35:
      chart_costs = 916.74 * x - 22086
    elif 35 <= x <= 42:
      chart_costs = 10000
    else:
      raise ValueError('LOA value error')

  elif 'clv' in vessel_type:
    x = float(ps_vessel['turn_storage'])
    if x < 565:
      x = 565
      _w = 'Turntable storage falls outside of the defined cost_function '
      _w = _w + 'domain'
      warnings.warn(_w)
    elif x > 10000:
      x = 10000
      _w = 'Turntable storage falls outside of the defined cost_function '
      _w = _w + 'domain'
      warnings.warn(_w)

    chart_costs = 2.46 * 10**(-4) * x**2 + 7.25 * x + 53090

  elif 'ctv' in vessel_type:
    x = float(ps_vessel['loa'])
    if x < 15:
      x = 15
      warnings.warn('LOA falls outside of the defined cost_function domain')
    elif x > 33:
      x = 33
      warnings.warn('LOA falls outside of the defined cost_function domain')

    chart_costs = -1.26 * x**2 + 179.16 * x - 85.57

  elif 'dsv' in vessel_type:
    x = float(ps_vessel['loa'])
    if x < 35:
      x = 35
      warnings.warn('LOA falls outside of the defined cost_function domain')
    elif x > 150:
      x = 150
      warnings.warn('LOA falls outside of the defined cost_function domain')

    chart_costs = 4308.81 * np.exp(0.02 * x)

  elif ('guard' in vessel_type and 'vessel' in vessel_type):
    x = float(ps_vessel['speed_service'])
    if x < 7:
      x = 7
      _w = 'Service Speed falls outside of the defined cost_function domain'
      warnings.warn(_w)
    elif x > 24:
      x = 24
      _w = 'Service Speed falls outside of the defined cost_function domain'
      warnings.warn(_w)

    chart_costs = 77.11 * x + 1345.48

  elif ('transport' in vessel_type and 'barge' in vessel_type):
    loa = float(ps_vessel['loa'])
    beam = float(ps_vessel['beam'])
    draft = float(ps_vessel['draft'])
    x = loa * beam * draft
    if x < 1557:
      x = 1557
      chart_costs = 953.92 * np.log(x) - 6761.18
      _w = 'Dimensions fall outside of the defined cost_function domain'
      warnings.warn(_w)
    elif x > 19950:
      x = 19950
      chart_costs = 953.92 * np.log(x) - 6761.18
      _w = 'Dimensions fall outside of the defined cost_function domain'
      warnings.warn(_w)

    chart_costs = 953.92 * np.log(x) - 6761.18

  elif ('jack' in vessel_type and 'up' in vessel_type):
    x = float(ps_vessel['crane_capacity'])
    if x < 50:
      x = 50
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)
    elif x > 4400:
      x = 4400
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)

    if 50 <= x < 755:
      chart_costs = 64.71 * x + 21448.41
    elif 755 <= x <= 896:
      chart_costs = 586.18 * x - 372275
    elif 896 <= x <= 4400:
      chart_costs = 26.83 * x + 128892
    else:
      raise ValueError('Crane capacity value error')

  elif ('non' in vessel_type and
        'propelled' in vessel_type and
        'crane' in vessel_type):
    x = float(ps_vessel['crane_capacity'])
    if x < 4:
      x = 4
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)
    elif x > 3300:
      x = 3300
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)

    if 4 <= x < 500:
      chart_costs = 26.15 * x + 5842.59
    elif 500 <= x < 1500:
      chart_costs = 56.33 * x - 9254.94
    elif 1500 <= x <= 3300:
      chart_costs = 42.24 * x + 11871.96
    else:
      raise ValueError('Value error') 

  # ### REVER ###
  elif ('propelled' in vessel_type and 'crane' in vessel_type):
    x = float(ps_vessel['crane_capacity'])
    if x < 50:
      x = 50
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)
    elif x > 12000:
      x = 12000
      _w = 'Vessel crane capacity falls outside of the defined cost_function'
      _w = _w + ' domain'
      warnings.warn(_w)

    chart_costs = -5.54 * 10**(-3) * x**2 + 88.90 * x + 12714.58

  elif 'psv' in vessel_type:
    x = float(ps_vessel['free_deck'])
    if x < 30:
      x = 30
      _w = 'Vessel free deck area falls outside of the defined cost_function'
      warnings.warn(_w)
    elif x > 5005:
      x = 5005
      _w = 'Vessel free deck area falls outside of the defined cost_function'
      warnings.warn(_w)

    chart_costs = 1.01 * x + 8970

  elif ('rock' in vessel_type and 'dumper' in vessel_type):
    x = float(ps_vessel['rock_capacity'])
    if x < 5400:
      x = 5400
      _w = 'Rock cargo capacity falls outside of the defined cost_function '
      _w = _w + 'domain'
      warnings.warn(_w)
    elif x > 31500:
      x = 31500
      _w = 'Rock cargo capacity falls outside of the defined cost_function '
      _w = _w + 'domain'
      warnings.warn(_w)

    chart_costs = 3.99 * x + 69212.41

  elif ('sov' in vessel_type and
        'pure' in vessel_type and
        'accomodation' in vessel_type):
    x = float(ps_vessel['passengers'])
    if x < 60:
      chart_costs = 12000
    elif x >= 60:
      chart_costs = 20000
    else:
      raise ValueError('Passengers value error')

  elif ('sov' in vessel_type and
        'gangway' in vessel_type and
        'equipped' in vessel_type):
    x = float(ps_vessel['passengers'])
    if x < 60:
      chart_costs = 24000
    elif x >= 60:
      chart_costs = 50000
    else:
      raise ValueError('Passengers value error')

  # ### Este vai desaparecer ###
  elif ('sov' in vessel_type and
        'gangway' in vessel_type and
        'relevant' in vessel_type):
    x = float(ps_vessel['passengers'])
    if x < 60:
      chart_costs = 24000
    if x >= 60:
      chart_costs = 42000
    else:
      raise ValueError('Passengers value error')

  elif 'survey' in vessel_type:
    x = float(ps_vessel['loa'])
    if x < 23:
      x = 23
      _w = 'Vessel LOA falls outside of the defined cost_function domain'
      warnings.warn(_w)
    elif x > 56:
      x = 56
      _w = 'Vessel LOA falls outside of the defined cost_function domain'
      warnings.warn(_w)

    chart_costs = 333.33 * x - 4166.67

  else:
    raise AssertionError('Vessel type not found.')

  return chart_costs


def vessel_fuel_consumption(
    ps_vessel: pd.Series,
    alf: float = 0.8,
    sfoc: float = 210
) -> float:
  '''
  vessel_fuel_consumption:
  Function that calculates fuel consumption of a given vessel.

  Inputs:
  -------
  * ps_vessel = Vessel information - Pandas Series
  * alf = Average Load Factor of the vessel (value suggested by GRS Offshore)
  - float
  * sfoc = Specific Fuel Oil Consumption (value suggested by GRS Offshore)
  - float

  Returns:
  --------
  * fuel_cons = vessel fuel consumption [ton/day] - float
  '''
  fuel_cons = ps_vessel['power'] * alf * sfoc * 24 * (1 / 1000)**2
  return fuel_cons


def vessel_fuel_costs(
    fuel_cons: float,
    mdo_price: float = 515
) -> float:
  '''
  vessel_fuel_costs:
  Function that calculates daily fuel costs of a given vessel or
  combination of vessels.

  Inputs:
  -------
  * fuel_cons = fuel consumption of a given vessel or list of vessels - float
  * mdo_price = Price of MDO in €/ton. Default value defined as 515€/ton -
  float

  Returns:
  --------
  * vessel fuel cost in €/day        
  '''
  return fuel_cons * mdo_price
