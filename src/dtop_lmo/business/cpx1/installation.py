# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import math as mt
import json
import datetime
import copy

# Import classes
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes import Cable_class
from dtop_lmo.business.classes import Site

# Import functions

# Get classes according to complexity
Operation = Operation_class.get_complexity('low')
Object = Object_class.get_complexity('low')
Cable = Cable_class.get_complexity('low')


class Installation1(object):
  """
  Installation3:
  ----------------
  The class assesses a set of installation functionalities for early stage

  Arguments:
  start_date = Month to start the installation phase ("MM/YYYY") - string
  hierarchy = Project hierarchy - pandas DataFrame

  Optional Arguments:
  post_burial = Consider post-burial operation for cables installation -
  boolean
  period = period of aggregation to analyse: "month", "quarter", "trimester" -
  string
  """
  complexity = 'low'

  def __init__(
      self,
      start_date,
      operations_hs,
      period,
      hierarchy_et,
      hierarchy_ed,
      hierarchy_sk
  ):
    # Inputs
    self.start_date = start_date

    # Optional inputs
    self.operations_hs = operations_hs
    self.period = period
    self.hierarchy_et = hierarchy_et
    self.hierarchy_ed = hierarchy_ed
    self.hierarchy_sk = hierarchy_sk

    # Outputs
    self.operations = None
    self.vec = None
    self.terminal = None
    self.vessel = None
    self.equipment = []
    self.durations = None
    self.waitings = None
    self.costs = None
    self.consumption = None             # Consumption in ton/24h
    self.plan = None
    self.site = None

    self.objects = []
    self.cables = []

    # Run initializing functions
    # self.get_inputs()

  def get_inputs(
      self
  ) -> dict:
    # Return inputs as a dictionary
    return {
        "Start date [MM/YYYY]": self.start_date,
        "Operation Wave Height [m]": self.operations_hs,
        "Selected period": self.period
    }

  def merge_hierarchy(self):
    """
    merge_hierarchy
    ===============
    Get hierarchies from ET, ED and SK
    Edits each hierarchy column to ensure consistency
    Aggregates the three hierarchies
    """
    df_hierarchy = pd.DataFrame()

    df_hierarchy = df_hierarchy.append(self.hierarchy_et)
    df_hierarchy = df_hierarchy.append(self.hierarchy_ed)
    df_hierarchy = df_hierarchy.append(self.hierarchy_sk)

    # Convert string to lower strings
    df_hierarchy = df_hierarchy.applymap(lambda x: x.lower()
                                         if isinstance(x, str) else x)

    df_hierarchy.reset_index(inplace=True)
    self.hierarchy = df_hierarchy

  # Functions for a set of operations (sort of a phase)
  def get_operations_sequence(self) -> dict:
    """
    get_operations_sequence
    ------------------------------------
    This function reads a list of objects in the project and identifies the
    sequence of installation operations to carry out as a dictionary.

    Outputs:
    --------
    operation_sequence = Sequence of the operations to carry out - dictionary
    """
    # Check if at least one of self.objects or self.cables were defined
    if len(self.objects) + len(self.cables) == 0:
      _e = 'One of \"objects\" or \"cables\" must be defined'
      raise AssertionError(_e)

    # Define an empty operations list
    opx_list = list()

    try:
      obj_names = [obj.name.lower() for obj in self.objects]
      obj_types = [obj.type.lower() for obj in self.objects]
    except TypeError:
      obj_names = []
      obj_types = []
    try:
      cables_type = [cable.type.lower() for cable in self.cables]
    except TypeError:
      cables_type = []

    # Check array cable installation
    if any(['array' in x for x in cables_type]):
      opx_list.append('cable installation')

    # Check export cable installation
    if any(['export' in x for x in cables_type]):
      opx_list.append('cable installation')

    # Check collection point installation
    c1 = ['collection' in x for x in obj_names]
    c2 = ['point' in x for x in obj_names]
    conds = [c1i and c2i for c1i, c2i in zip(c1, c2)]
    list_cps_type = [obj_types[idx]
                     for idx, cond in enumerate(conds)
                     if cond is True]

    if len(list_cps_type) != 0:
      # There are CPs to install
      if any(['hub' not in x for x in list_cps_type]):
        # At least one cp is not an hub.
        opx_list.append('collection point installation')
    del c1, c2, conds

    # Check mooring lines and anchors installation
    if any(['line' in x for x in obj_names]):
      opx_list.append('mooring installation')
    if any(['anchor' in x for x in obj_names]):
      opx_list.append('mooring installation')

    # Check suport structure installation
    if any(['support' in x for x in obj_names]):
        opx_list.append('support structure installation')

    # Check foundations installation
    c1 = ['pile' in x for x in obj_types]
    c2 = ['suction' in x for x in obj_types]
    c3 = ['foundation' in x for x in obj_names]
    cond = [c1i or c2i or c3i for c1i, c2i, c3i in zip(c1, c2, c3)]
    if any(cond):
      opx_list.append('foundation installation')
      # pre-installed GBA is not considered.
    del c1, c2, c3, cond

    if any(['device' in x for x in obj_names]):
      opx_list.append('device installation')

    predef_sequence = ['foundation installation',
                       'mooring installation',
                       'support structure installation',
                       'collection point installation',
                       'device installation',
                       'export cable installation',
                       'array cable installation']

    # ### GET THIS FROM CATALOGUES AND INTEGRATE ITS INFO WITH opx_sequence
    df_operations = {
        "op01": "foundation installation",
        "op02": "mooring installation",
        "op03": "support structure installation",
        "op04": "collection point installation",
        "op05": "device installation",
        "op06": "export cable installation",
        "op07": "array cable installation"
    }
    # ###

    opx_sequence = {}
    # Loop trough default operations
    for predef_op in predef_sequence:
      # Loop through operations got from objects
      for opx in opx_list:
        if opx in predef_op:
          # opx operation will be part of the opx sequence
          # Operation ID
          for key in df_operations:
            if df_operations[key] == predef_op:
              op_id = key
              break
          opx_sequence[op_id] = opx
          opx_list.remove(opx)
          del op_id
          break

    return opx_sequence

  def set_operations(self, operations_sequence) -> list:
    """
    set_operations
    ---------------------------
    This function reads the dictionary of operations with operations as strings
    and returns a list of operations as Operation class.
    The Operations are initialized but not fully defined.

    Returns:
    --------
    operations = List of operations in Operation format - list of Operation()
    """
    # Check if at least one of objects or cables were defined
    if len(self.objects) + len(self.cables) == 0:
      _e = 'One of \"objects\" or \"cables\" must be defined'
      raise AssertionError(_e)

    operations = list()

    objects = copy.deepcopy(self.objects)
    cables = copy.deepcopy(self.cables)
    try:
      obj_names = [obj.name.lower() for obj in self.objects]
      obj_types = [obj.type.lower() for obj in self.objects]
    except TypeError:
      # objects not defined
      obj_names = []
      obj_types = []

    try:
      cables_names = [cable.name.lower() for cable in self.cables]
      cables_types = [cable.type.lower() for cable in self.cables]
    except TypeError:
      # cables not defined
      cables_names = []
      cables_types = []

    # Flag to check if an export cable was already added. This because array
    # and export cable are both "cable installation"
    flag_export = False

    for opx_id, op_name in operations_sequence.items():
      # Based on operation name and list of objects, get objects associated to
      # this operation
      op_name = op_name.lower()
      # Foundation installation - find 'foundation' in self.objects
      # Mooring installation - find 'mooring' in self.objects
      # Device installation - find 'device' in self.objects
      # Export cable installation - find 'export' and 'cable' in self.objects
      # Array cable installation - find 'array' and 'cable' in self.objects
      # Collection point installation - find 'collection' and 'point' in
      # self.objects
      # Support structure installation - find 'suport' and 'structure' in
      # self.objects
      if 'foundation' in op_name and 'installation' in op_name:
        # Indexes of the objects containing the word 'foundation', 'pile'
        obj_index = [i
                     for i in range(0, len(obj_names))
                     if 'foundation' in obj_names[i] or 'pile' in obj_types[i]]
        op_desc = obj_types[obj_index[0]]     # ### CHECK THIS

      if 'mooring' in op_name and 'installation' in op_name:
        # Indexes of the objects containing the word 'mooring', 'anchor', 'pile
        obj_index = [i
                     for i in range(0, len(obj_names))
                     if ('line' in obj_names[i] or
                         ('anchor' in obj_names[i] and
                          'pile' not in obj_types[i]))]
        op_desc = obj_types[obj_index[0]]     # ### CHECK THIS
        op_desc = op_desc.replace('anchor', 'embedment')

      if 'device' in op_name and 'installation' in op_name:
        # Indexes of the objects containing the word 'device'
        obj_index = [i for i in range(0, len(obj_names))
                     if 'device' in obj_names[i]]
        op_desc = 'device'

      if ('support' in op_name and
          'structure' in op_name and
          'installation' in op_name):
        # Indexes of the objects containing the word 'support', 'structure'
        obj_index = [i for i in range(0, len(obj_names))
                     if ('support' in obj_names[i] and
                         'structure' in obj_names[i])]
        op_desc = obj_types[obj_index[0]]     # ### CHECK THIS

      if ('cable' in op_name and 'installation' in op_name):
        if flag_export is False:
          # Indexes of the cables containing the word 'cable' and 'export'
          cbl_index = [i for i in range(0, len(cables_types))
                       if ('export' in cables_types[i] and
                           'cable' in cables_names[i])]
          flag_export = True  # Means we already added a cable installation op
        elif flag_export is True or not cbl_index:
          # If flag_export is True means that we are in the second cable
          # installation operation. Check if there is an array cable to install
          # If cbl_index is empty, means that there is no export cable to be
          # installed. Check if there is an array for the first cable
          # installation.
          # Indexes of the cables containing the word 'cable' and 'array'
          cbl_index = [i for i in range(0, len(cables_types))
                       if (('array' in cables_types[i] and
                            'cable' in cables_names[i]) or
                           ('array' in cables_types[i] and
                            'umbilical' in cables_names[i]))]
        op_desc = 'simultaneous'      # ### For cpx1, cable is not buried

      if ('collection' in op_name and
          'point' in op_name and
          'installation' in op_name):
        # Indexes of the objects containing the word 'collection', 'point'
        obj_index = [i for i in range(0, len(obj_names))
                     if ('collection' in obj_names[i] and
                         'point' in obj_names[i])]
        op_desc = 'collection point'

      # Get Objects from stored positions
      try:
        op_objs_list = [objects[i] for i in obj_index]
        # Delete objects that will be installed from objects
        for i, obj_idx in enumerate(obj_index):
          del objects[obj_idx - i]
          del obj_names[obj_idx - i]
          del obj_types[obj_idx - i]
        del obj_index
      except NameError:
        op_objs_list = None
      # Get Cables from stored positions
      try:
        op_cbls_list = [cables[i] for i in cbl_index]
        # Delete cables that will be installed from cables
        for i, cbl_idx in enumerate(cbl_index):
          del cables[cbl_idx - i]
          del cables_names[cbl_idx - i]
          del cables_types[cbl_idx - i]
        del cbl_index
      except NameError:
        op_cbls_list = None

      # Operation index differentiation: "_0", "_1", "_2", ...
      op_idx_in_ops = [opx_id in op.id for op in operations]
      diff = sum(op_idx_in_ops)
      if diff < 10:
        op_suffix = '_0' + str(sum(op_idx_in_ops))
      elif diff < 100:
        op_suffix = '_' + str(sum(op_idx_in_ops))
      else:
        raise AssertionError('To many operations of the same type')
      opx_id_suffix = str(opx_id) + op_suffix

      # Create Operation
      op = Operation(
          operation_id=opx_id_suffix,
          operation_name=op_name,
          operation_description=op_desc,
          operation_hs=self.operations_hs,
          objects=op_objs_list,
          cables=op_cbls_list
      )
      operations.append(op)
      del op, op_objs_list, op_cbls_list, op_desc

    return operations

  def redefine_operations(
      self,
      lmo_operations: list
  ) -> list:
    operations = []
    for lmo_op in lmo_operations:
      objects = []
      cables = []
      if self.objects is not None:
        for lmo_obj in lmo_op.objects:
          for obj in self.objects:
            if lmo_obj.id_external == obj.id:
              objects.append(obj)
              del obj
              break
      if self.cables is not None:
        for lmo_cbl in lmo_op.cables:
          for cbl in self.cables:
            if lmo_cbl.id_external == cbl.id:
              cables.append(cbl)
              del cbl
              break
      # Create Operation
      op = Operation(
          operation_id=lmo_op.id_external,
          operation_name=lmo_op.name,
          operation_description=lmo_op.description,
          operation_hs=self.operations_hs,
          objects=objects,
          cables=cables
      )
      operations.append(op)
      del objects, cables, op

    self.operations = operations
    return operations

  # Function to allocate site to operations
  def allocate_site_to_operations(self):
    if self.site is None:
      raise AssertionError('Site is not defined yet')
    elif type(self.site) is not Site:
      raise AssertionError('Site type is not correct')

    for idx, op in enumerate(self.operations):
      self.operations[idx].site = self.site

  # All set for Core Functionalities

  def define_operations_dates(self, operation_durations: dict) -> dict:
    """
    For all installation operations, this function evaluate when they can start
    and tries to schedule all of them considering durations, waitings,
    start date and period

    Args:
        operation_durations (dict): Dictionary with all operations IDs,
        durations and waitings per period

    Returns:
        dict: Dictionary with all operations dates in datetime format
    """
    date_split = self.start_date[0:7].split('/')
    if len(date_split) > 2:
      return AssertionError('Date format should be "yyyy-mm"')
    start_day = 1     # For complexity 1, installation always starts in day 1
    start_month = int(date_split[1])
    start_year = int(date_split[0])

    # Initialize may start date - always at 8a.m.
    dt_may_start = datetime.datetime(start_year, start_month, start_day, 8)

    for op_id, item in operation_durations.items():
      # Check the duration of this operation for this "may_start" date
      month = dt_may_start.month
      if self.period.lower() == 'month':
        period = month
      elif self.period.lower() == 'quarter':
        # Check in which quarter this month is
        period = mt.ceil(month / 3)
      elif self.period.lower() == 'trimester':
        # Check in which trimester this month is
        period = mt.ceil(month / 4)

      period = str(period)
      op_dur = item["durations"][period]
      op_waiting = item["waitings"][period]
      try:
        dt_duration = datetime.timedelta(hours=op_dur)
        dt_waiting = datetime.timedelta(hours=op_waiting)
      except ValueError:
        _e = 'Operation %s - %s cannot be performed in period %s. ' % (op_id, item["name"], period)
        _e = _e + 'Probably it is too restrictive and/or too long'
        raise AssertionError(_e)

      operation_durations[op_id]["dates"] = {
          "may_start": dt_may_start,
          "start": dt_may_start + dt_waiting,
          "end": dt_may_start + dt_duration
      }

      # Update "dt_may_start" for next operation
      dt_may_start = operation_durations[op_id]["dates"]["end"]

    return operation_durations

  def build_output_table(
      self,
      operation_data: dict
  ) -> pd.DataFrame:
    """
    define_output_table
    ====================
    Pandas DataFrame with all relevant outputs from installation phase

    Returns
    ----------
    * df_operations_table = Installation operations plan table - pandas
    DataFrame
    """
    dict_operations_table = {
        "operation_id": [],
        "name": [],
        "tech_group": [],
        "operation_type": ['installation'] * len(operation_data.keys()),
        "technologies": [],
        "date_may_start": [],
        "date_start": [],
        "date_end": [],
        "duration_total": [],
        "waiting_to_start": [],
        "vessel_consumption": [],
        "vessels_combination": [],
        "vessels": [],
        "equipment": [],
        "cost": [],
        "cost_vessel": [],
        "cost_terminal": [],
        "cost_equipment": [],
        "terminal": [],
        "downtime": ['NA'] * len(operation_data.keys()),
        "fail_date": ['NA'] * len(operation_data.keys()),
        "mttr": ['NA'] * len(operation_data.keys()),
        "replacement_parts": ['NA'] * len(operation_data.keys()),
        "replacement_costs": ['NA'] * len(operation_data.keys()),
        "cost_label": ['CAPEX'] * len(operation_data.keys())
    }

    for op_id, item in operation_data.items():
      # Operation ID
      dict_operations_table["operation_id"].append(op_id)
      # Operation name
      op_name = item["name"]
      dict_operations_table["name"].append(op_name)
      # Operation technology group
      if ('foundation' in op_name.lower() or 'mooring' in op_name.lower() or
          ('support' in op_name.lower() and 'structure' in op_name.lower())):
        dict_operations_table["tech_group"].append('station keeping')
      elif (('collection' in op_name.lower() and
             'point' in op_name.lower()) or
            'cable' in op_name.lower() or 
            'burial' in op_name.lower() or
            ('external' in op_name.lower() and
             'protection' in op_name.lower())):
        dict_operations_table["tech_group"].append('electrical')
      elif 'device' in op_name.lower():
        dict_operations_table["tech_group"].append('device')
      else:
        dict_operations_table["tech_group"].append('other')

      # Operation technologies - components dealt with during the operation
      dict_operations_table["technologies"].append(item["component_ids"])

      # Operation dates
      date_may_start = item["dates"]["may_start"].strftime('%Y/%m/%d')
      date_start = item["dates"]["start"].strftime('%Y/%m/%d')
      date_end =item["dates"]["end"].strftime('%Y/%m/%d')
      dict_operations_table["date_may_start"].append(date_may_start)
      dict_operations_table["date_start"].append(date_start)
      dict_operations_table["date_end"].append(date_end)

      # Operation period
      op_month = item["dates"]["may_start"].month
      if self.period == 'month':
        period = op_month
      elif self.period == 'quarter':
        period = mt.ceil(op_month / 3)
      elif self.period == 'trimester':
        period = mt.ceil(op_month / 4)
      period = str(period)

      # Operation durations and waitings
      dur_total = item["durations"][period]
      wait_start = item["waitings"][period]

      dict_operations_table["duration_total"].append(dur_total)
      dict_operations_table["waiting_to_start"].append(wait_start)

      # Vessels consumption
      vessels_consumption = item["consumption"] * ((dur_total - wait_start) / 24)
      dict_operations_table["vessel_consumption"].append(vessels_consumption)

      # Vessels Combination ID
      dict_operations_table["vessels_combination"].append(item["vec"])
      # Vessel ID
      dict_operations_table["vessels"].append(item["vessels"])
      # Equipment ID
      dict_operations_table["equipment"].append(item["equipment"])

      # Operation costs
      total_costs = 0
      for key, costs_per_period in item["costs"].items():
        total_costs += costs_per_period[period]

      dict_operations_table["cost"].append(total_costs)
      dict_operations_table["cost_vessel"].append(item["costs"]["vessels"][period])
      dict_operations_table["cost_terminal"].append(item["costs"]["terminal"][period])
      dict_operations_table["cost_equipment"].append(item["costs"]["equipment"][period])

      # Operation Terminal ID
      dict_operations_table["terminal"].append(item["terminal"])

    df_operations_table = pd.DataFrame(dict_operations_table)

    return df_operations_table
