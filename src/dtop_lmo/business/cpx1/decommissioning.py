# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import math as mt
import datetime
import copy

# Import classes
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Site

# Get classes according to complexity
Operation = Operation_class.get_complexity('low')


class Decommissioning1(object):
  """
  Decommissioning1:
  ----------------
  The class is a copy of Installation1.
  Decommissioning functionality will only run in case the installation has
  already run
  """
  complexity = 'low'

  def __init__(
      self,
      start_date,
      period
  ):
    # Inputs
    self.start_date = start_date
    self.period = period

    # Outputs
    self.operations = None
    self.vec = None
    self.terminal = None
    self.vessels = []
    self.equipment = []
    self.durations = None
    self.waitings = None
    self.costs = None
    self.consumption = None             # Consumption in ton/24h
    self.plan = None
    self.site = None

    self.objects = []
    self.cables = []

    # Run initializing functions
    # self.get_inputs()

  def get_inputs(
      self
  ) -> dict:
    # Return inputs as a dictionaty
    return {
        "Start date [MM/YYYY]": self.start_date,
        "Selected period": self.period
    }

  def define_operations_dates(self, operation_durations: dict) -> dict:
    """
    For all decommissioning operations, this function evaluate when they can start
    and tries to schedule all of them considering durations, waitings,
    start date and period

    Args:
        operation_durations (dict): Dictionary with all operations IDs,
        durations and waitings per period

    Returns:
        dict: Dictionary with all operations dates in datetime format
    """
    date_split = self.start_date[0:7].split('/')
    if len(date_split) > 2:
      return AssertionError('Date format should be "yyyy-mm"')
    start_day = 1     # For complexity 1, decommissioning always starts in day 1
    start_month = int(date_split[1])
    start_year = int(date_split[0])

    # Initialize may start date - always at 8a.m.
    dt_may_start = datetime.datetime(start_year, start_month, start_day, 8)

    for op_id, item in operation_durations.items():
      # Check the duration of this operation for this "may_start" date
      month = dt_may_start.month
      if self.period.lower() == 'month':
        period = month
      elif self.period.lower() == 'quarter':
        # Check in which quarter this month is
        period = mt.ceil(month / 3)
      elif self.period.lower() == 'trimester':
        # Check in which trimester this month is
        period = mt.ceil(month / 4)

      period = str(period)
      op_dur = item["durations"][period]
      op_waiting = item["waitings"][period]
      dt_duration = datetime.timedelta(hours=op_dur)
      dt_waiting = datetime.timedelta(hours=op_waiting)

      operation_durations[op_id]["dates"] = {
          "may_start": dt_may_start,
          "start": dt_may_start + dt_waiting,
          "end": dt_may_start + dt_duration
      }

      # Update "dt_may_start" for next operation
      dt_may_start = operation_durations[op_id]["dates"]["end"]

    return operation_durations

  def build_output_table(
      self,
      operation_data: dict
  ) -> pd.DataFrame:
    """
    define_output_table
    ====================
    Pandas DataFrame with all relevant outputs from decommissioning phase

    Returns
    ----------
    * df_operations_table = Decommissioning operations plan table - pandas
    DataFrame
    """
    dict_operations_table = {
        "operation_id": [],
        "name": [],
        "tech_group": [],
        "operation_type": ['decommissioning'] * len(operation_data.keys()),
        "technologies": [],
        "date_may_start": [],
        "date_start": [],
        "date_end": [],
        "duration_total": [],
        "waiting_to_start": [],
        "vessel_consumption": [],
        "vessels_combination": [],
        "vessels": [],
        "equipment": [],
        "cost": [],
        "cost_vessel": [],
        "cost_terminal": [],
        "cost_equipment": [],
        "terminal": [],
        "downtime": ['NA'] * len(operation_data.keys()),
        "fail_date": ['NA'] * len(operation_data.keys()),
        "mttr": ['NA'] * len(operation_data.keys()),
        "replacement_parts": ['NA'] * len(operation_data.keys()),
        "replacement_costs": ['NA'] * len(operation_data.keys()),
        "cost_label": ['CAPEX'] * len(operation_data.keys())
    }

    for op_id, item in operation_data.items():
      # Operation ID
      dict_operations_table["operation_id"].append(op_id)
      # Operation name
      op_name = item["name"]
      dict_operations_table["name"].append(op_name)
      # Operation technology group
      if ('foundation' in op_name.lower() or 'mooring' in op_name.lower() or
          ('support' in op_name.lower() and 'structure' in op_name.lower())):
        dict_operations_table["tech_group"].append('station keeping')
      elif (('collection' in op_name.lower() and
             'point' in op_name.lower()) or
            'cable' in op_name.lower() or 
            'burial' in op_name.lower() or
            ('external' in op_name.lower() and
             'protection' in op_name.lower())):
        dict_operations_table["tech_group"].append('electrical')
      elif 'device' in op_name.lower():
        dict_operations_table["tech_group"].append('device')
      else:
        dict_operations_table["tech_group"].append('other')

      # Operation technologies - components dealt with during the operation
      dict_operations_table["technologies"].append(item["component_ids"])

      # Operation dates
      date_may_start = item["dates"]["may_start"].strftime('%Y/%m/%d')
      date_start = item["dates"]["start"].strftime('%Y/%m/%d')
      date_end =item["dates"]["end"].strftime('%Y/%m/%d')
      dict_operations_table["date_may_start"].append(date_may_start)
      dict_operations_table["date_start"].append(date_start)
      dict_operations_table["date_end"].append(date_end)

      # Operation period
      op_month = item["dates"]["may_start"].month
      if self.period == 'month':
        period = op_month
      elif self.period == 'quarter':
        period = mt.ceil(op_month / 3)
      elif self.period == 'trimester':
        period = mt.ceil(op_month / 4)
      period = str(period)

      # Operation durations and waitings
      dur_total = item["durations"][period]
      wait_start = item["waitings"][period]

      dict_operations_table["duration_total"].append(dur_total)
      dict_operations_table["waiting_to_start"].append(wait_start)

      # Vessels consumption
      vessels_consumption = item["consumption"] * ((dur_total - wait_start) / 24)
      dict_operations_table["vessel_consumption"].append(vessels_consumption)

      # Vessels Combination ID
      dict_operations_table["vessels_combination"].append(item["vec"])
      # Vessel ID
      dict_operations_table["vessels"].append(item["vessels"])
      # Equipment ID
      dict_operations_table["equipment"].append(item["equipment"])

      # Operation costs
      total_costs = 0
      for key, costs_per_period in item["costs"].items():
        total_costs += costs_per_period[period]

      dict_operations_table["cost"].append(total_costs)
      dict_operations_table["cost_vessel"].append(item["costs"]["vessels"][period])
      dict_operations_table["cost_terminal"].append(item["costs"]["terminal"][period])
      dict_operations_table["cost_equipment"].append(item["costs"]["equipment"][period])

      # Operation Terminal ID
      dict_operations_table["terminal"].append(item["terminal"])

    df_operations_table = pd.DataFrame(dict_operations_table)

    return df_operations_table
