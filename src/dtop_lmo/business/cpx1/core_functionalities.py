# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import os

# Import Shared Libraries
# from dtop_shared_library.pandas_tables import schema_from_document

# Import classes


# Import functions
from dtop_lmo.business import workability
from dtop_lmo.business import startability
from dtop_lmo.business import waiting
from dtop_lmo.business import vessel_costs
from dtop_lmo.business.feasibilities.vessels import _read_vessel_clusters

# True to print logs
PRINT_LOGS = True


class Core1(object):
  """
  Core3:
  --------
  The class assesses a set of core functionalities for early stage

  Arguments:
  var_name = variable description - var type

  Optional Arguments:
  max_wait = Maximum waiting time between activities - integer
  comb_retain_ratio = Combinations retain ratio - float
  comb_retain_min = Minimum number of Combinations - integer
  quantile = Statistical quantile to be analysed [0.25, 0.5, 0.75] - float
  period = Period of aggregation to analyse: "month", "quarter", "trimester" -
  string

  Return:
  var_name = variable description - var type
  """
  complexity = 'low'

  def __init__(
      self,
      quantile: float = 0.5,
      period: str = 'month'
  ):
    # Optional inputs
    self.quantile = quantile
    self.period = period

  def get_inputs(
      self
  ) -> dict:
    # Return inputs as a dictionary
    return {
        "Statistical quantile to be analysed": self.quantile,
        "Period of the year to be analysed": self.period
    }

  # Feasibility functions
  def run_feasibility_functions(
      self,
      operations: list
  ) -> list:
    """
    Runs all Operation methods required to get feasible solutions.

    Args:
        operations (list): List of :meth:`~dtop_lmo.business.classes.Operation_class()`

    Returns:
        list: List of :meth:`~dtop_lmo.business.classes.Operation_class() with feasibility solutions
    """
    print('Running feasibility functions')
    for idx, _ in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t' + operations[idx].id + ' - ' + operations[idx].name + ' - ' + operations[idx].description)
      operations[idx].get_feasible_solutions()

    return operations

  def select_optimal_terminal(
      self,
      operations
  ) -> list:
    print('Select optimal terminal')
    for idx, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t' + operation.id + ' - ' + operation.name + ' - ' + operation.description)
      # Get closest terminal ID
      if 'repair at port' in operation.description:
        operations[idx].terminal_dist = 0
        operations[idx].terminal = operations[idx - 1].terminal
        continue
      feasible_terminal = operation.feasible_solutions.terminaltable
      feasible_terminal.sort_values('dist_to_port', inplace=True)
      feasible_terminal.reset_index(inplace=True)
      operations[idx].terminal = feasible_terminal.loc[0, 'id_name']
      operations[idx].terminal_dist = int(feasible_terminal.loc[0, 'dist_to_port'])

    return operations

  def get_net_durations(
      self,
      operations: list
  ) -> list:
    for idx, _ in enumerate(operations):
      operations[idx].get_operation_net_duration()

    return operations

  def select_optimal_solution(
      self,
      operations
  ) -> list:
    for idx, operation in enumerate(operations):
      if 'repair at port' in operation.description:
        operations[idx].terminal = operations[idx - 1].terminal
        operations[idx].vec = operations[idx - 1].vec
        operations[idx].vessel = operations[idx - 1].vessel
        operations[idx].equipment = []
        continue

      op_dur = operation.duration_net
      try:
        df_vessels = operation.feasible_solutions.vessels_main
      except AttributeError:
        df_vessels = operation.feasible_solutions.vessels_tow

      cost_vessels = []
      for idx_v, row in df_vessels.iterrows():
        costs_charter_daily = vessel_costs.vessel_daily_charter(row)
        # €/day
        vessel_cons = vessel_costs.vessel_fuel_consumption(row)
        # ton/day
        costs_fuel_daily = vessel_costs.vessel_fuel_costs(vessel_cons)
        # €/day

        total_costs = costs_charter_daily * mt.ceil(op_dur / 24)
        total_costs += costs_fuel_daily * (op_dur / 24)

        cost_vessels.append(total_costs)

      # Find optimal cost index
      min_idx = cost_vessels.index(min(cost_vessels))
      operations[idx].vessel = df_vessels['id_name'].iat[min_idx]

      # Get other logistical solution ids
      operations[idx].get_optimal_solution()

    return operations

  # Operation Limit Criteria realted functions
  def check_workabilities(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking workabilities')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))

      operations[i_op].workability = workability.workability_cpx1(operation, operation.site)

    return operations

  def check_startabilities(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking startabilities')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))

      operations[i_op].startability = startability.startability_cpx1(
          operation,
          operation.workability,
          operation.site.name
      )
      del operations[i_op].workability
      operations[i_op].workability = None

    return operations

  def check_waiting_time(
      self,
      operations: list
  ) -> list:
    """
    ###
    Return
    --------
    operations = List of operations updated - List of Operation()
    """
    print('Checking waiting times')
    for i_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))
      operations[i_op].waiting = waiting.waiting_time(
          [operation],
          operation.startability,
          operation.site.name
      )
      del operations[i_op].startability
      operations[i_op].startability = None

    return operations

  def define_durations_waitings_timestep(
      self,
      operations
  ) -> list:
    """
    define_durations_waitings_timestep
    ========================================
    Define durations and waitings per time step. For each metocean time step,
    define the operations total duration and the waiting time

    Inputs
    --------
    * operation = List of operations - List of Operation()

    Returns
    --------
    * operations = List of operations updated - List of Operation()
    """
    print('Calculating duration per timestep')
    # For each operation, get total duration and waiting to start
    for idx_op, operation in enumerate(operations):
      if PRINT_LOGS is True:
        print('\t%s - %s - %s' % (operation.id, operation.name, operation.description))
      df_waiting = operation.waiting
      duration = operation.duration_net
      df_metocean = operation.site.metocean
      df_metocean_dates = df_metocean[['year', 'month', 'day', 'hour']]
      del df_metocean

      df_values = pd.DataFrame(columns=['duration', 'waiting_start'])
      ds_waiting = df_waiting[operation.id]

      df_values['duration'] = ds_waiting + duration
      df_values['waiting_start'] = ds_waiting
      del duration, ds_waiting

      df_values = pd.concat([df_metocean_dates, df_values],
                            axis=1,
                            sort=False)
      del df_metocean_dates

      operations[idx_op].values = df_values
      del df_values
    return operations

  def get_statistics(
      self,
      df_values
  ) -> dict:
    """
    get_statistics
    ===============
    For a given DataFrame with durations and waitings per metocean time step,
    this functions preforms a statistic analysis considering the period and the
    quantile selected.
    It returns the expected value (with a certainty of "quantile") for each
    duration and waiting of the operation

    Inputs
    ------
    * df_values = Duration and Waitings values per time step - pd.DataFrame

    Returns
    --------
    durations_waitings = Dictionary with durations and waitings for each
    selected interval
    """
    duration = {}
    waiting = {}

    if self.period.lower() == 'month':
      intervals = [(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6),
                   (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)]
    elif self.period.lower() == 'quarter':
      intervals = [(1, 3), (4, 6), (7, 9), (10, 12)]
    elif self.period.lower() == 'trimester':
      intervals = [(1, 4), (5, 8), (9, 12)]
    else:
      raise AssertionError('\"period\" defined unrecognized')

    keys = [i + 1 for i in range(0, len(intervals))]
    for k in keys:
      int_min = intervals[k - 1][0]
      int_max = intervals[k - 1][1]
      # Filtered by period values
      df_values_f = df_values[(df_values['month'] >= int_min) & \
                              (df_values['month'] <= int_max)]
      duration[str(k)] = df_values_f['duration'].quantile(
          q=self.quantile,
          interpolation='nearest'
      )

      # Get the expected waiting times for this total duration
      dur = duration[str(k)]
      try:
        ds_dur_min = df_values_f['duration'] >= int(dur)
        ds_dur_max = df_values_f['duration'] <= mt.ceil(dur)
        df_values_f_2 = df_values_f[ds_dur_min & ds_dur_max].quantile(
            q=self.quantile,
            interpolation='nearest'
        )
        waiting[str(k)] = df_values_f_2['waiting_start']
        del df_values_f, df_values_f_2
      except ValueError:
        # The duration for this key is nan due to lack of metocean data
        waiting[str(k)] = np.nan

      # # Index of the closest value of dur
      # result_index = df_values_f['duration'].sub(dur).abs().idxmin()

      # waiting_expected = df_values_f.loc[result_index, :]['waiting_start']
      # waiting[str(k)] = waiting_expected

    return {
        "duration": duration,
        "waiting": waiting
    }

  def define_statistics(
      self,
      operations
  ) -> list:
    """
    define_statistics
    ========================
    Defines the duration and waiting time for each operation considering the
    period to analyse.

    Inputs
    ------
    * operation = List of operations - List of Operation()

    Returns
    --------
    * operations = List of operations updated - List of Operation()
    """
    for idx_op, operation in enumerate(operations):
      durs_waits = self.get_statistics(operation.values)
      del operations[idx_op].values
      operations[idx_op].durations = durs_waits["duration"]
      operations[idx_op].waitings = durs_waits["waiting"]
    return operations

  def allocate_costs(
      self,
      operations
  ) -> dict:
    """
    allocate_costs
    ============================
    For a given list of Operation() calculates the costs related to vessels,
    equipment and terminal for all each operation per period

    Inputs
    -------
    * operation = List of operations - List of Operation()

    Retruns
    ---------
    * operations = List of operations updated - List of Operation()
    """
    for idx_op, operation in enumerate(operations):
      # Operation duration in hours, in each period
      durations_period = operation.durations
      waiting_period = operation.waitings

      # Number of days of the operation per month
      n_days_period = {}
      for key, value in durations_period.items():
        try:
          n_days_period[key] = value / 24
        except ValueError:
          n_days_period[key] = np.nan
      op_duration_days = {key: value / 24
                          for key, value in durations_period.items()}
      op_waiting_days = {key: value / 24
                         for key, value in waiting_period.items()}

      # Vessel info
      vessel_id = operation.vessel.lower()

      catalogue_path = os.path.join(os.getcwd(), 'catalogues')

      # Get vec ID and vec DataSeries
      df_vecs = pd.read_csv(os.path.join(catalogue_path, 'VECs_simple.csv'), sep=',')
      df_vecs = df_vecs[df_vecs['vec_id'].str.lower() == operation.vec.lower()]
      ds_vec = df_vecs.iloc[0, :]
      try:
        qnt_vessel = int(ds_vec['qnt1'])
      except ValueError:
        qnt_vessel = 0
        try:
          qnt_vessel = int(ds_vec['qnt2'])
        except ValueError:
          raise AssertionError('No vessels for operaion %s', operation.id)

      # Get vessels DataFrame
      df_vessels = pd.read_csv(catalogue_path + '/Vessels.csv', sep=',')
      df_vessels = _read_vessel_clusters(df_vessels, 'p50')

      # Equipment info
      equipment_ids = operation.equipment
      try:
        df_rov = operation.feasible_solutions.rovtable
      except AttributeError:
        df_rov = None
      try:
        df_burial = operation.feasible_solutions.burialtable
      except AttributeError:
        df_burial = None
      try:
        df_piling = operation.feasible_solutions.pilingtable
      except AttributeError:
        df_piling = None

      costs_total_vessels_period = {}
      costs_total_equipment_period = {}
      costs_total_terminal_period = {}
      for per, days in n_days_period.items():
        # Check for how many complete days and how many half days does this
        # operation lasts
        if days % 1 < 0.5 and days % 1 != 0:
          days_complete = int(days)
          days_half = 1
        else:
          try:
            days_complete = mt.ceil(days)
            days_half = 0
          except ValueError:
            days_complete = np.nan
            days_half = np.nan

        # Vessel ID, get the pandas Series from the Catalogue
        # Filter DataFrame to a single Series
        ds_vessel = df_vessels[df_vessels['id_name'].str.lower() == vessel_id]
        ds_vessel = ds_vessel.iloc[0, :]

        costs_charter_daily = 0
        vessel_cons = 0
        costs_fuel_daily = 0

        for v in range(0, qnt_vessel):
          # Charter costs
          costs_charter_daily += vessel_costs.vessel_daily_charter(ds_vessel)
          # €/day
          # Fuel costs
          vessel_cons += vessel_costs.vessel_fuel_consumption(ds_vessel)
          # ton/day
          costs_fuel_daily += vessel_costs.vessel_fuel_costs(vessel_cons)
          # €/day

        # For each equipment ID, get the pandas Series from the Catalogue
        costs_equip_daily = 0
        costs_equip_half = 0
        for id_ in equipment_ids:
          # Filter DataFrame to a single Serie
          if 'rov' in id_:
            ds_equip = df_rov[df_rov['id_name'].str.lower() == id_.lower()]
          elif 'bur' in id_:
            ds_equip = df_burial[df_burial['id_name'].str.lower() == id_.lower()]
          elif 'pil' in id_:
            ds_equip = df_piling[df_piling['id_name'].str.lower() == id_.lower()]

          costs_equip_daily += float(ds_equip['cost_day'])      # €/day
          costs_equip_half += float(ds_equip['cost_half_day'])  # €/half_day

        costs_total_vessels_charter = \
            costs_charter_daily * (days_complete + days_half)
        if 'repair at port' in operation.description:
          costs_total_vessels_fuel = 0
        else:
          costs_total_vessels_fuel = costs_fuel_daily * \
                          (op_duration_days[per] - op_waiting_days[per])

        costs_total_vessels_period[per] = \
            costs_total_vessels_charter + costs_total_vessels_fuel
        costs_total_equipment_period[per] = \
            (costs_equip_daily * days_complete) + (costs_equip_half * days_half)
        vessels_and_equip = costs_total_vessels_period[per] + \
                                            costs_total_equipment_period[per]
        costs_total_terminal_period[per] = vessels_and_equip * 0.005

        operations[idx_op].costs = {
            "vessels": costs_total_vessels_period,
            "equipment": costs_total_equipment_period,
            "terminal": costs_total_terminal_period
        }

    return operations

  def allocate_consumptions(
      self,
      operations
  ) -> list:
    for idx_op, operation in enumerate(operations):
      if 'repair at port' in operation.description:
        continue

      feasible_sols = operation.feasible_solutions
      df_vessels = pd.DataFrame()
      try:
        df_vessels = df_vessels.append(feasible_sols.vessels_main,
                                       ignore_index=True)
      except AttributeError:
        df_vessels = df_vessels.append(feasible_sols.vessels_tow,
                                       ignore_index=True)

      vessel = operation.vessel
      # Filter DataFrame to a single Series
      ds_vessel = df_vessels[df_vessels['id_name'].str.lower() == vessel.lower()]
      ds_vessel = ds_vessel.iloc[0, :]

      daily_cons = vessel_costs.vessel_fuel_consumption(ds_vessel)
      operations[idx_op].consumption = daily_cons

    return operations
