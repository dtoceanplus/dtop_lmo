# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import datetime
import copy
import warnings
warnings.simplefilter("ignore")
import re

# Import classes
from dtop_lmo.business.classes import Operation_class
from dtop_lmo.business.classes import Site

# Import Shared Libraries
from dtop_shared_library.sim_failure_event import sim_failure_event
# TODO: DELETE THIS
from dtop_lmo.business.sim_failure_event import sim_failure_event

# Get classes according to complexity
Operation = Operation_class.get_complexity('low')


class Maintenance1(object):
  """
  Maintenance1:
  -------------
  The class assesses a set of maintenance functionalities for early stage

  Arguments:
  start_date = Date to start the maintenance phase ("DD/MM/YYYY") - string
  ### has to be after installation, equal to commissioning date
  t_life = Project life time - int
  device_repair_at_port = If device are to repaired at port - bool
  period = period of aggregation to analyse: "month", "quarter", "trimester" - str
  """
  complexity = 'low'

  def __init__(
      self,
      start_date,
      t_life,
      operations_hs,
      device_repair_at_port,
      period,
      hierarchy_et,
      hierarchy_ed,
      hierarchy_sk
  ):

    # Inputs
    self.start_date = start_date
    self.t_life = t_life
    self.operations_hs = operations_hs
    self.device_repair_at_port = device_repair_at_port
    self.period = period
    self.hierarchy_et = hierarchy_et
    self.hierarchy_ed = hierarchy_ed
    self.hierarchy_sk = hierarchy_sk

    # Outputs
    self.operations = None
    self.vec = None
    self.terminal = None
    self.vessel = None
    self.equipment = []
    self.durations = None
    self.waitings = None
    self.costs = None
    self.consumption = None             # Consumption in ton/24h
    self.plan = None
    self.downtime = None
    self.site = None

    self.objects = []
    self.cables = []

    # Run initializing functions
    # self.get_inputs()
    self.hierarchies_consistency()

  def get_inputs(
      self
  ) -> dict:
    # Print inputs in console

    # Return inputs as a dictionaty
    return {
        # "Argument 1": self.arg1,
        # "Argument 2": self.arg2,
        "Optional Argument 1": self.opt_arg1,
        "Optional Argument 1": self.opt_arg2
    }

  def hierarchies_consistency(self):
    if self.hierarchy_et is not None:
      for col_name in self.hierarchy_et.columns:
        self.hierarchy_et[col_name] = self.hierarchy_et[col_name].apply(
            lambda x: x.lower() if type(x) == str else float(x)
        )
    if self.hierarchy_ed is not None:
      for col_name in self.hierarchy_ed.columns:
        self.hierarchy_ed[col_name] = self.hierarchy_ed[col_name].apply(
            lambda x: x.lower() if type(x) == str else float(x)
        )
    if self.hierarchy_sk is not None:
      for col_name in self.hierarchy_sk.columns:
        self.hierarchy_sk[col_name] = self.hierarchy_sk[col_name].apply(
            lambda x: x.lower() if type(x) == str else float(x)
        )

  def set_object_failures(self) -> dict:
    """
    set_object_failures
    ===================
    Summary

    Inputs:
    -------
    Input

    Outputs:
    --------
    Output
    """
    failures = {}

    components_id = []
    failure_times_all = []
    for component in self.objects + self.cables:
      if component.failure_rates is not None:
        # TODO: To be removed when sim_failure_event acepts fr=0
        failure_rates = list(component.failure_rates)
        for idx, fr in enumerate(failure_rates):
          if fr == 0:
            failure_rates[idx] = 0.0000000001
        [component_id, failure_times] = sim_failure_event(
            component.id,
            failure_rates,
            self.t_life
        )
        if len(failure_times[0]) != 0 or len(failure_times[1]) != 0:
          failures[component_id] = failure_times

    return failures

  def set_maintenance_operations(
      self,
      failure_times: dict,
      daylight_hours: float = 14.0
  ) -> list:
    """
    [summary]

    Args:
        daylight_hours (float, optional): [description]. Defaults to 14.0.

    Raises:
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]

    Returns:
        list: [description]
    """
    def split_list(alist, wanted_parts=1):
      length = len(alist)
      return [alist[i*length // wanted_parts: (i+1)*length // wanted_parts] 
              for i in range(wanted_parts)]

    # Load Catalogues
    # ### Get this from API and not from local machine
    df_operations = pd.read_csv('catalogues/Operations.csv')
    df_activities = pd.read_csv('catalogues/Activities.csv')

    # Check if at least one of self.objects or self.cables were defined
    if len(self.objects) + len(self.cables) == 0:
      _e = 'One of \"objects\" or \"cables\" must be defined'
      raise AssertionError(_e)

    operations = []

    # PREVENTIVE MAINTENANCE OPERATIONS
    # Periodicity for inspections
    idx = df_operations.index[
        df_operations['description'].str.lower() == 'topside inspection']
    p_topside_insp = int(df_operations.iloc[idx]['Periodicity'].to_list()[0])

    idx = df_operations.index[
        df_operations['description'].str.lower() == 'underwater inspection']
    p_underwater_insp = int(df_operations.iloc[idx]['Periodicity']
                            .to_list()[0])

    idx = df_operations.index[
        df_operations['description'].str.lower() == 'mooring inspection']
    p_mooring_insp = int(df_operations.iloc[idx]['Periodicity'].to_list()[0])

    idx = df_operations.index[
        df_operations['description'].str.lower() == 'array cable inspection']
    p_array_insp = int(df_operations.iloc[idx]['Periodicity'].to_list()[0])

    idx = df_operations.index[
        df_operations['description'].str.lower() == 'export cable inspection']
    p_export_insp = int(df_operations.iloc[idx]['Periodicity'].to_list()[0])

    # Duration of inspection activity
    idx = df_activities.index[
        df_activities['name'].str.lower() == 'topside inspection']
    t_topside_insp = float(df_activities.iloc[idx]['duration'].to_list()[0])

    idx = df_activities.index[
        df_activities['name'].str.lower() == 'underwater inspection with divers/rov']
    t_underwater_insp = float(df_activities.iloc[idx]['duration'].to_list()[0])

    idx = df_activities.index[
        df_activities['name'].str.lower() == 'mooring inspection']
    t_mooring_insp = float(df_activities.iloc[idx]['duration'].to_list()[0])

    idx = df_activities.index[
        df_activities['name'].str.lower() == 'array cable inspection']
    t_array_insp = float(df_activities.iloc[idx]['duration'].to_list()[0])

    t_transit = 0.2

    # Calculate maximum number of inspections possible during daylight
    n_topside_insp = 1
    t_tot = t_topside_insp
    while t_tot <= daylight_hours:
      t_tot += t_topside_insp + t_transit
      if t_tot > daylight_hours:
        break
      else:
        n_topside_insp += 1

    n_underwater_insp = 1
    t_tot = t_underwater_insp
    while t_tot <= daylight_hours:
      t_tot += t_underwater_insp + t_transit
      if t_tot > daylight_hours:
        break
      else:
        n_underwater_insp += 1

    n_mooring_insp = 1
    t_tot = t_mooring_insp
    while t_tot <= daylight_hours:
      t_tot += t_mooring_insp + t_transit
      if t_tot > daylight_hours:
        break
      else:
        n_mooring_insp += 1

    n_array_insp = 1
    t_tot = t_array_insp
    while t_tot <= daylight_hours:
      t_tot += t_array_insp + t_transit
      if t_tot > daylight_hours:
        break
      else:
        n_array_insp += 1

    preventive_ops = {
        'op10': {
            'considered': False,
            'name': 'topside inspection',
            'objects': [],
            'max_objects': n_topside_insp,
            'periodicity': p_topside_insp
        },
        'op11': {
            'considered': False,
            'name': 'underwater inspection',
            'objects': [],
            'max_objects': n_underwater_insp,
            'periodicity': p_underwater_insp
        },
        'op12': {
            'considered': False,
            'name': 'mooring inspection',
            'objects': [],
            'max_objects': n_mooring_insp,
            'periodicity': p_mooring_insp
        },
        'op13': {
            'considered': False,
            'name': 'array cable inspection',
            'cables': [],
            'max_cables': n_array_insp,
            'periodicity': p_array_insp
        },
        'op14': {
            'considered': False,
            'name': 'export cable inspection',
            'cables': [],
            'max_cables': 100000,
            'periodicity': p_export_insp
        }
    }

    for obj in self.objects:
      # Check if Topside inspection will be considered
      if hasattr(obj, 'topside_exists'):
        if obj.topside_exists is True:
          preventive_ops["op10"]["considered"] = True
          preventive_ops["op10"]["objects"].append(obj)

      # Check if Underwater inspection will be considered
      if ('device' in obj.name.lower() or 'foundation' in obj.name.lower() or
          ('collection' in obj.name.lower() and 'point' in obj.name.lower())):
        preventive_ops["op11"]["considered"] = True
        preventive_ops["op11"]["objects"].append(obj)

    # Moorings require a different apporach
    all_moorings = [
        obj for obj in self.objects
            if (('sk' in obj.id.lower() and 'mooring' in obj.name.lower()) or
                ('sk' in obj.id.lower() and 'anchor' in obj.name.lower()) or
                ('sk' in obj.id.lower() and 'line' in obj.name.lower()))
    ]
    # Check with objects have upstream objects
    moorings_upstream = []
    for mooring in all_moorings:
      if mooring.upstream is not None:
        upstream_ids_str = mooring.upstream
        upstream_ids_str = upstream_ids_str[1:-1].lower()
        upstream_ids_list = upstream_ids_str.split(', ')
        for mooring_id in upstream_ids_list:
          moorings_upstream.append(mooring_id)

    # Remove upstream objects
    i = 0
    while i < len(all_moorings):
      mooring = all_moorings[i]
      if mooring.id.lower()[3:] in moorings_upstream:
        all_moorings.pop(i)
        i -= 1
      i += 1

    for obj in all_moorings:
      # And now, check if Mooring inspection will be considered
      if (('sk' in obj.id.lower() and 'mooring' in obj.name.lower()) or
          ('sk' in obj.id.lower() and 'anchor' in obj.name.lower()) or
          ('sk' in obj.id.lower() and 'line' in obj.name.lower())):
        preventive_ops["op12"]["considered"] = True
        preventive_ops["op12"]["objects"].append(obj)

    for cbl in self.cables:
      # Check if Array cable inspection will be considered
      if cbl.type.lower() == 'array':
        preventive_ops["op13"]["considered"] = True
        preventive_ops["op13"]["cables"].append(cbl)

      # Check if Export cable inspection will be considered
      if cbl.type.lower() == 'export':
        preventive_ops["op14"]["considered"] = True
        preventive_ops["op14"]["cables"].append(cbl)

    # Split objects / cables in different operations considering the max number
    # of inspected cables in the same operation
    for op, args in preventive_ops.items():
      try:
        preventive_ops[op]['objects'] = split_list(
            alist=args["objects"],
            wanted_parts=(mt.ceil(len(args["objects"]) / args["max_objects"]))
        )
      except KeyError:
        preventive_ops[op]['cables'] = split_list(
            alist=args["cables"],
            wanted_parts=(mt.ceil(len(args["cables"]) / args["max_cables"]))
        )

    # Finally, define preventive maintenance operations
    for op, args in preventive_ops.items():
      if args["considered"] is True:
        try:
          for objects in preventive_ops[op]['objects']:
            # Operation index differentiation
            op_idx_in_ops = [op in op_aux.id.lower() for op_aux in operations]
            diff = sum(op_idx_in_ops)
            if diff < 10:
              op_suffix = '_00' + str(sum(op_idx_in_ops))
            elif diff < 100:
              op_suffix = '_0' + str(sum(op_idx_in_ops))
            elif diff < 1000:
              op_suffix = '_' + str(sum(op_idx_in_ops))
            else:
              raise AssertionError('Too many operations of the same type')

            lifetime_may_starts = [
                y * 365 * 24
                for y in range(1, self.t_life + 1, args["periodicity"])
            ]
            operations.append(
                Operation(operation_id=op + op_suffix,
                          operation_name='preventive maintenance',
                          operation_description=args["name"],
                          objects=objects,
                          operation_hs=self.operations_hs,
                          multiple_operations=True,
                          maystart_deltas=lifetime_may_starts)
            )
        except KeyError:
          for cables in preventive_ops[op]['cables']:
            # Operation index differentiation
            op_idx_in_ops = [op in op_aux.id.lower() for op_aux in operations]
            diff = sum(op_idx_in_ops)
            if diff < 10:
              op_suffix = '_00' + str(sum(op_idx_in_ops))
            elif diff < 100:
              op_suffix = '_0' + str(sum(op_idx_in_ops))
            elif diff < 1000:
              op_suffix = '_' + str(sum(op_idx_in_ops))
            else:
              raise AssertionError('Too many operations of the same type')

            lifetime_may_starts = [
                y * 365 * 24
                for y in range(1, self.t_life + 1, args["periodicity"])
            ]
            operations.append(
                Operation(operation_id=op + op_suffix,
                          operation_name='preventive maintenance',
                          operation_description=args["name"],
                          cables=cables,
                          operation_hs=self.operations_hs,
                          multiple_operations=True,
                          maystart_deltas=lifetime_may_starts)
            )


    # CORRECTIVE MAINTENANCE OPERATIONS
    corrective_ops = {
        'op15': {
            'considered': False,
            'name': 'device retrieval',
            'objects': [],
            'failure_time': []
        },
        'op16': {
            'considered': False,
            'name': 'device repair at port',
            'objects': [],
            'failure_time': []
        },
        'op17': {
            'considered': False,
            'name': 'device redeployment',
            'objects': [],
            'failure_time': []
        },
        'op18': {
            'considered': False,
            'name': 'device repair on site',
            'objects': [],
            'failure_time': []
        },
        'op19': {
            'considered': False,
            'name': 'mooring line replacement',
            'objects': [],
            'failure_time': []
        },
        'op20': {
            'considered': False,
            'name': 'cable replacement',
            'objects': [],
            'failure_time': []
        },
        'op21': {
            'considered': False,
            'name': 'cable repair',
            'objects': [],
            'failure_time': []
        }
    }

    for component_id, failures in failure_times.items():
      # Get object python struct
      for component in self.objects + self.cables:
        if component.id.lower() == component_id.lower():
          break

      if (component.system.lower() == 'et' or
          ('collection' in component.name.lower()) and
           'point' in component.name.lower()):
        op_objects = [component]
        if self.device_repair_at_port and component.type != 'hub':
          if 'et' in component.system.lower():
            # Add device to objects to maintain in this operation
            device_num = re.search(r'\d+', component.id).group()
            device_id = 'device_' + device_num
            for obj in self.objects:
              if obj.id.lower() == device_id.lower():
                op_objects.append(obj)
                break

          # failures[1] - Considering only major fails ###
          if op_objects not in corrective_ops["op15"]["objects"]:
            corrective_ops["op15"]["considered"] = True
            corrective_ops["op15"]["objects"].append(op_objects)
            corrective_ops["op15"]["failure_time"].append(failures[1])
          if op_objects not in corrective_ops["op16"]["objects"]:
            corrective_ops["op16"]["considered"] = True
            corrective_ops["op16"]["objects"].append(op_objects)
            corrective_ops["op16"]["failure_time"].append(failures[1])
          if op_objects not in corrective_ops["op17"]["objects"]:
            corrective_ops["op17"]["considered"] = True
            corrective_ops["op17"]["objects"].append(op_objects)
            corrective_ops["op17"]["failure_time"].append(failures[1])
        else:
          if op_objects not in corrective_ops["op18"]["objects"]:
            corrective_ops["op18"]["considered"] = True
            corrective_ops["op18"]["objects"].append(op_objects)
            corrective_ops["op18"]["failure_time"].append(failures[1])
        del op_objects

      elif component.system.lower() == 'sk':
        # Mooring line replacement
        if component not in corrective_ops["op19"]["objects"]:
          corrective_ops["op19"]["considered"] = True
          corrective_ops["op19"]["objects"].append([component])
          corrective_ops["op19"]["failure_time"].append(failures[1])

      elif (component.system.lower() == 'ed' and
            component.type.lower() == 'array'):
        # Cable replacement
        if component not in corrective_ops["op20"]["objects"]:
          corrective_ops["op20"]["considered"] = True
          corrective_ops["op20"]["objects"].append([component])
          corrective_ops["op20"]["failure_time"].append(failures[1])

      elif (component.system.lower() == 'ed' and
            component.type.lower() == 'export'):
        # Cable repair
        if component not in corrective_ops["op21"]["objects"]:
          corrective_ops["op21"]["considered"] = True
          corrective_ops["op21"]["objects"].append([component])
          corrective_ops["op21"]["failure_time"].append(failures[1])

      del component

    # Finally, define corrective maintenance operations
    for op, args in corrective_ops.items():
      if args["considered"] is True:
        for idx_obj, set_objects in enumerate(corrective_ops[op]['objects']):
          # Operation index differentiation
          op_idx_in_ops = [op in op_aux.id.lower() for op_aux in operations]
          diff = sum(op_idx_in_ops)
          if diff < 10:
            op_suffix = '_00' + str(sum(op_idx_in_ops))
          elif diff < 100:
            op_suffix = '_0' + str(sum(op_idx_in_ops))
          elif diff < 1000:
            op_suffix = '_' + str(sum(op_idx_in_ops))
          else:
            raise AssertionError('Too many operations of the same type')

          lifetime_may_starts = corrective_ops[op]['failure_time'][idx_obj]
          if 'op20' not in op.lower() and 'op21' not in op.lower():
            operations.append(
                Operation(operation_id=op + op_suffix,
                          operation_name='corrective maintenance',
                          operation_description=args["name"],
                          objects=set_objects,
                          operation_hs=self.operations_hs,
                          multiple_operations=True,
                          maystart_deltas=lifetime_may_starts)
            )
          else:
            operations.append(
                Operation(operation_id=op + op_suffix,
                          operation_name='corrective maintenance',
                          operation_description=args["name"],
                          cables=set_objects,
                          operation_hs=self.operations_hs,
                          multiple_operations=True,
                          maystart_deltas=lifetime_may_starts)
            )

    self.operations = operations
    return operations

  def redefine_operations(
      self,
      lmo_operations: list
  ) -> list:
    operations = []
    for lmo_op in lmo_operations:
      objects = []
      cables = []
      if self.objects is not None:
        for lmo_obj in lmo_op.objects:
          for obj in self.objects:
            if lmo_obj.id_external == obj.id:
              objects.append(obj)
              del obj
              break
      if self.cables is not None:
        for lmo_cbl in lmo_op.cables:
          for cbl in self.cables:
            if lmo_cbl.id_external == cbl.id:
              cables.append(cbl)
              del cbl
              break
      # Create Operation
      if 'op16' in lmo_op.id_external:
        op = Operation(
            operation_id=lmo_op.id_external,
            operation_name=lmo_op.name,
            operation_description=lmo_op.description,
            operation_hs=1000,    # No OLC
            objects=objects,
            cables=cables,
            multiple_operations=lmo_op.multiple_operations,
            maystart_deltas=lmo_op.dates_maystart_delta["dates_maystart_delta"]
        )
      else:
        op = Operation(
            operation_id=lmo_op.id_external,
            operation_name=lmo_op.name,
            operation_description=lmo_op.description,
            operation_hs=self.operations_hs,
            objects=objects,
            cables=cables,
            multiple_operations=lmo_op.multiple_operations,
            maystart_deltas=lmo_op.dates_maystart_delta["dates_maystart_delta"]
        )
      operations.append(op)
      del objects, cables, op

    self.operations = operations
    return operations

  # Function to allocate site to operations
  def allocate_site_to_operations(self):
    if self.site is None:
      raise AssertionError('Site is not defined yet')
    elif type(self.site) is not Site:
      raise AssertionError('Site type is not correct')

    for idx, op in enumerate(self.operations):
      self.operations[idx].site = self.site

  # All set for Core Functionalities

  def define_operations_dates(self, operation_durations: dict) -> dict:
    """
    For all installation operations, this function evaluate when they can start
    and tries to schedule all of them considering durations, waitings,
    start date and period

    Args:
        operation_durations (dict): Dictionary with all operations IDs,
        durations and waitings per period

    Returns:
        dict: [description]
    """
    # Sort operations by may_start dates
    operation_data_list = []
    for op_id, operation in operation_durations.items():
      operation_data_list.append(operation)
      operation_data_list[-1]["id"] = op_id

    operation_data_list_sorted = sorted(operation_data_list, key=lambda i: i['may_start'])

    date_split = self.start_date[0:7].split('/')
    if len(date_split) > 2:
      return AssertionError('Date format should be "yyyy/mm"')
    start_day = 1     # For complexity 1, installation always starts in day 1
    start_month = int(date_split[1]) #CHANGED FCF 15.07.2020
    start_year = int(date_split[0])  #CHANGED FCF 15.07.2020

    # Initialize (fixed) reference start date (commissioning) - always at 8a.m.
    ref_date = datetime.datetime(start_year, start_month, start_day, 8)

    for idx, operation in enumerate(operation_data_list_sorted):
      op_id = operation["id"]
      if idx == 0:
        # Calculate may_start date from reference date and time difference that
        # already was specified in operation definition
        dt_may_start = ref_date + datetime.timedelta(hours=operation["may_start"])

      elif (operation["description"].lower() == 'device repair at port' or
            operation["description"].lower() == 'device redeployment'):
        # In this case "may_start" was not set yet. Set to next day after
        # previous operation finished at 8a.m.
        # ### Assumes that op15, op16 and op17 are defined consecutively
        dt_may_start = last_dt_may_start + datetime.timedelta(days=1)
        dt_may_start = datetime.datetime(dt_may_start.year, dt_may_start.month,
                                         dt_may_start.day, 8)
      else:
        dt_may_start = ref_date + datetime.timedelta(hours=operation["may_start"])

        # Set to next day after previous operation if there is overlap
        if dt_may_start < last_dt_may_start:
          dt_may_start = last_dt_may_start + datetime.timedelta(days=1)
          dt_may_start = datetime.datetime(dt_may_start.year,
                                           dt_may_start.month,
                                           dt_may_start.day, 8)

      # Check the duration of this operation for this "may_start" date
      month = dt_may_start.month
      if self.period.lower() == 'month':
        period = month
      elif self.period.lower() == 'quarter':
        # Check in which quarter this month is
        period = mt.ceil(month / 3)
      elif self.period.lower() == 'trimester':
        # Check in which trimester this month is
        period = mt.ceil(month / 4)

      period = str(period)
      op_dur = operation["durations"][period]
      op_waiting = operation["waitings"][period]
      try:
        dt_duration = datetime.timedelta(hours=op_dur)
        dt_waiting = datetime.timedelta(hours=op_waiting)
      except ValueError:
        _e = 'Operation %s - %s cannot be performed in period %s. ' % (op_id, operation["name"], period)
        _e = _e + 'Probably it is too restrictive and/or too long'
        raise AssertionError(_e)

      operation_durations[op_id]["dates"] = {
          "may_start": dt_may_start,
          "start": dt_may_start + dt_waiting,
          "end": dt_may_start + dt_duration
      }

      # Update last_dt_may_start
      last_dt_may_start = operation_durations[op_id]["dates"]["end"]

    return operation_durations

  def hierarchy_child_linearizer(
      self,
      hierarchy: pd.DataFrame
  ) -> pd.DataFrame:
    """
    hierarchy_child_linearizer
    =========================
    For the introduced hierarchy file, this function will linearize, for each component in the hierarchy, the dependencies to its
    children, taking into consideration the logic gates. The linearization is only carried out to the lowest level which has 
    a failure rate specified.
    E.g. A.Child       = [B,C] , A.Gates = AND
         B.Child       = [D,E] , B.Gates = OR
         C.Child       = [Y,Z],  C.Gates = AND
         D.Child       = ''
         E.Child       = ''
         Y.Child       = ''
         Z.Child       = ''

         C.failurerate = 0.1
         D.failurerate = 0.1
         E.failurerate = 0.1
         Y.failurerate = ''
         Z.failurerate = ''

         Returns: A.Child = '(D or E) and C' 

    The Child column in the hierarchy_lin file is now a string which can be used for the eval functionality.

    Inputs
    ---------
    * hierarchy = Hierarchy file previously loaded (SK, ED or ET)

    Returns
    --------
    * hierarchy_lin = Hierarchy file with linearized relationships. 
    """
    if hierarchy.empty:
      return hierarchy

    hierarchy = hierarchy.fillna(value='')
    hierarchy_lin = copy.deepcopy(hierarchy)
    hierarchy_lin['child_lin'] = ''
    # Add underscore after each node name to avoid errors comparing 'et1' and 'et10'
    # and
    # Remove [, ], ( and ) from children
    for idx, row in hierarchy_lin.iterrows():
      hierarchy_lin.loc[idx, 'name_underscore'] = row['name_of_node'] + '_'
      children = row['child']
      if children.lower() != 'na':
        children = children.replace('[', '').replace('(', '')
        children = children.replace(']', '').replace(')', '')
        # Add underscore after each element on list
        children = children.replace(' ', '')
        children_list = children.split(',')
        children_list = [value + '_' for value in children_list]
        children = ', '.join(children_list)
      hierarchy_lin.loc[idx, 'child_underscore'] = children

    # List of elements that have failure rates
    element_fr = hierarchy_lin['name_underscore'][hierarchy_lin.loc[:, 'failure_rate_replacement'].str.lower() != 'na'].to_list()
    for i in hierarchy_lin.index:        #The code will compare for each row i of the hierarchy file
      k = 0
      childs_str = hierarchy_lin['child_underscore'][i]
      childs_str = childs_str.replace(',', ' %s' % hierarchy_lin['gate_type'][i].lower())
      failure_rate = hierarchy_lin.loc[i, 'failure_rate_replacement']
      try:
        failure_rate = failure_rate.lower()
      except AttributeError:
        pass

      opt = True    # initialize optimization as True.
      # if no children or there is a failure rate specified, pass to next element i
      if childs_str == '' or failure_rate != 'na':
        if childs_str == '' and failure_rate == 'na':
          _e = 'No failure rate specified for component \"%s\"' \
              % hierarchy_lin.loc[i, 'name_of_node']
          raise AssertionError(_e)
        pass  #pass if element does not have children or already has F.R.

      else:
        cycle_ctr = 0
        while opt is True:
          childs_total_line = childs_str.replace('(', '').replace(')', '')
          # Find AND, OR and X/Y gates in string
          childs_total_line = re.split(' and | or | \d/\d ', childs_total_line)
          exists_flag = hierarchy_lin['name_underscore'][k] in childs_total_line
          child_k = hierarchy_lin['child_underscore'][k]
          if exists_flag is False or child_k.lower() == 'na':
            k += 1
          else:
            child_k = hierarchy_lin['child_underscore'][k]

            childs_str = childs_str.replace(hierarchy_lin['name_underscore'][k],
                                            '(%s)' % child_k)
            gate = hierarchy_lin['gate_type'][k].lower()
            # substituir gates
            childs_str = childs_str.replace(',', ' %s' % gate)

          if k == len(hierarchy_lin.index) - 1:
            k = 0

          childs_total_line = childs_str.replace('(', '').replace(')', '')
          childs_total_line = re.split(' and | or | \d/\d ', childs_total_line)
          var = [zz in element_fr for zz in childs_total_line]

          if all(var) is True:
            opt = False
          cycle_ctr += 1
          if cycle_ctr > 1000:
            raise AssertionError('Error trying to linearize children')

        hierarchy_lin.loc[i, 'child_lin'] = '(' + childs_str + ')'

    return hierarchy_lin

  def get_downtime_cor(
      self,
      operation_data: dict,
      components_list: list
  ) -> dict:
    """
    get_downtime
    ============
    Calculates downtime per PTO per project year and month

    Outputs:
    --------
    Dictionary of PTO names and corresponding downtime array
    """
    # Load Catalogues
    # ### Get this from API and not from local machine
    df_operations = pd.read_csv('catalogues/Operations.csv')
    df_activities = pd.read_csv('catalogues/Activities.csv')

    hierarchy_et_lin = self.hierarchy_child_linearizer(self.hierarchy_et)
    hierarchy_ed_lin = self.hierarchy_child_linearizer(self.hierarchy_ed)
    hierarchy_sk_lin = self.hierarchy_child_linearizer(self.hierarchy_sk)

    # If there is no ET inputs, there is no downtime
    if hierarchy_et_lin.empty is True:
      return {}

    # Convert start date from string to datetime
    date_split = self.start_date[0:7].split('/')
    if len(date_split) > 2:
      return AssertionError('Date format should be "yyyy/mm"')
    start_day = 1     # For complexity 1, installation always starts in day 1
    start_month = int(date_split[1])
    start_year = int(date_split[0])

    start_date = datetime.datetime(start_year, start_month, start_day, 8)
    # CHANGED FCF 16.07.2020 ; CHANGED LA 15.09.2020

    # Create empty downtime array template
    arr_downtime_init = np.zeros((self.t_life + 1, 12))

    PTO_dict = {}
    # Create dict with PTO id and corresponding downtime array (deep copied!)
    for node in hierarchy_et_lin["name_of_node"].to_list():
      device_num = re.search('et(.*)_pto', node.lower())
      if device_num is None:
        continue
      device_num = device_num.group(1)
      pto_in_str = re.search('pto\w+', node.lower())
      if pto_in_str is None:
        continue
      pto_in_str = pto_in_str.group(0)

      pto_strs = pto_in_str.split('_')
      try:
        pto_num = pto_strs[1]
        pto_subnum = pto_strs[2]
      except IndexError:
        continue
      pto_final = 'et' + str(device_num) + \
          '_pto_' + str(pto_num) + '_' + str(pto_subnum)
      PTO_dict.update({pto_final: copy.deepcopy(arr_downtime_init)})

    EPT_dict = {}
    # Create dict with PTO id and corresponding energy production tree
    EPT_base = 'PTO and ET and SK and ED'
    # TODO: ASK FCF IF THIS MAKES SENSE. SHOULDN'T WE REMOVE PTO???
    # Otherwise we are repeating ourselves (PTO is the same than ET)

    for pto_id in PTO_dict.keys():
      # --- Set PTO ---
      # Get this pto index in ET hierarchy
      idx_pto = self.hierarchy_et.index[
          self.hierarchy_et['name_of_node'].str.lower() == pto_id
      ]

      # If PTO has a failure rate it's done, otherwise check children
      pto_id_fr = hierarchy_et_lin.iloc[idx_pto]['failure_rate_replacement'].to_list()[0]
      if str(pto_id_fr).lower() != 'na':
        PTO = '(' + pto_id + ')'
      else:
        PTO = hierarchy_et_lin['child_lin'][idx_pto].str.lower().values[0]

      # Add 'et_' to children
      PTO_elements = re.split(r'\s', PTO)
      PTO_elements = [re.sub('\(', '', string) for string in PTO_elements]
      PTO_elements = [re.sub('\)', '', string) for string in PTO_elements]
      # Delete and, or and x/y gates
      PTO_elements = [elem
                      for elem in PTO_elements
                      if elem != 'or' and elem != 'and' and '/' not in elem]

      for child in PTO_elements:
        PTO = PTO.replace(child, 'et_' + child)

      # --- Set ET ---
      try:
        sys_et = self.hierarchy_et.iloc[idx_pto]['parent'].to_list()[0]
        # Get this sys_et index in ET hierarchy
        idx_et = self.hierarchy_et.index[
            self.hierarchy_et['name_of_node'].str.lower() == sys_et.lower()]
        ET = hierarchy_et_lin['child_lin'][idx_et].str.lower().values[0]
        # Add 'et_' to children
        ET_elements = re.split(r'\s', ET)
        ET_elements = [re.sub('\(', '', string) for string in ET_elements]
        ET_elements = [re.sub('\)', '', string) for string in ET_elements]
        # Delete and, or and x/y gates
        ET_elements = [elem
                      for elem in ET_elements
                      if elem != 'or' and elem != 'and' and '/' not in elem]

        for child in ET_elements:
          ET_old = copy.deepcopy(ET)
          if len(ET_old.split(' ')) > 1:
            ET = ET.replace(' ' + child, ' et_' + child)
            if ET_old == ET:
              ET = ET.replace(child + ' ', 'et_' + child + ' ')
          else:
            ET = ET.replace(child, 'et_' + child)
      except KeyError:
        ET = '1'
        _w = 'ET hierarchy not defined'
        warnings.warn(_w)

      # --- Set SK ---
      # Get SK corresponding to ET system
      # ### Numbering MUST be identical, assumes SK1_x belongs to ET1
      try:
        sys_sk = sys_et.replace('et', 'sk') + '_x'
        idx_sk = self.hierarchy_sk.index[
            self.hierarchy_sk['name_of_node'].str.lower() == sys_sk.lower()]

        try:
          SK = hierarchy_sk_lin['child_lin'][idx_sk].str.lower().values[0]
          # Add 'sk_' and to children
          SK_elements = re.split(r'\s', SK)
          SK_elements = [re.sub('\(', '', string) for string in SK_elements]
          SK_elements = [re.sub('\)', '', string) for string in SK_elements]
          # Delete and, or and x/y gates
          SK_elements = [elem
                        for elem in SK_elements
                        if elem != 'or' and elem != 'and' and '/' not in elem]

          for child in SK_elements:
            SK_old = copy.deepcopy(SK)
            if len(SK_old.split(' ')) > 1:
              SK = SK.replace(' ' + child, ' sk_' + child)
              if SK_old == SK:
                SK = SK.replace(child + ' ', 'sk_' + child + ' ')
            else:
              SK = SK.replace(child, 'sk_' + child)
        except IndexError:
          SK = '1'
          _w = 'For device \"%s\", there is no SK system defined' % pto_id
          warnings.warn(_w)
      except KeyError:
        SK = '1'
        _w = 'SK hierarchy not defined'
        warnings.warn(_w)

      # --- Set ED ---
      # Get ED corresponding t ET system
      # ### Numbering MUST be identical, assumes ED1 belongs to ET1
      try:
        sys_ed = sys_et.replace('et', 'ed')
        idx_ed = self.hierarchy_ed.index[
            self.hierarchy_ed['name_of_node'].str.lower() == sys_ed.lower()]

        try:
          ED = hierarchy_ed_lin['child_lin'][idx_ed].values[0]
          # Add 'ed_' and to children
          ED_elements = re.split(r'\s', ED)
          ED_elements = [re.sub('\(', '', string) for string in ED_elements]
          ED_elements = [re.sub('\)', '', string) for string in ED_elements]
          # Delete and, or and x/y gates
          ED_elements = [elem
                        for elem in ED_elements
                        if elem != 'or' and elem != 'and' and '/' not in elem]

          for child in ED_elements:
            ED_old = copy.deepcopy(ED)
            if len(ED_old.split(' ')) > 1:
              ED = ED.replace(' ' + child, ' ed_' + child)
              if ED_old == ED:
                ED = ED.replace(child + ' ', 'ed_' + child + ' ')
            else:
              ED = ED.replace(child, 'ed_' + child)
        except IndexError:
          ED = '1'
          _w = 'For device \"%s\", there is no ED system defined' % pto_id
          warnings.warn(_w)
      except KeyError:
        ED = '1'
        _w = 'ED hierarchy not defined'
        warnings.warn(_w)

      # --- Combine to EPT ---
      EPT = EPT_base.replace(
          'PTO', PTO).replace(
              'ET', ET).replace(
                  'SK', SK).replace(
                      'ED', ED)

      EPT_dict.update({pto_id: EPT})

    # Dictionary of all components (items with failure rates) and functional
    # state (by default 1, will be set 0 in case of failure)
    components_dict = {}
    for component in components_list:
      if component.failure_rates is not None:
        components_dict.update({component.id_external.lower(): '1'})    #FCF left it like this

    for op_id, operation in operation_data.items():
      op_id = op_id.lower()
      dt_equivalent = 0     # Downtime equivalent
      # Preventive maintenance
      if ('op10' in op_id) or ('op11' in op_id):  # Topside or underwater inspections
        # Check if device is shut down during inspection
        # ### Maintenance operation IDs must have 4 chars before underscore
        # ### Assumes uppercase ID in Activities catalogue
        idx_op = df_operations.index[
            df_operations['id'].str.lower() == op_id.split('_')[0]]
        if (df_operations.iloc[idx_op]['Device shutdown'].to_list()[0].lower()
            == 'yes'):
          # Get inspection activity duration
          # Get durations from activities with word 'inspection'
          df_op_acts = df_activities[
              df_activities['op_id'].str.lower() == op_id.split('_')[0]]
          ds_contains_insp = df_op_acts['name'].str.contains('inspect',
                                                             case=False)
          df_inspection = df_op_acts[ds_contains_insp]
          act_duration = df_inspection['duration'].sum()
          act_duration = float(act_duration)

          # Set accumulated downtime during operation
          # Get month and year (counting from commissioning) of operation
          month = operation["dates"]["start"].month
          project_year = operation["dates"]["start"].year - start_date.year

          # Go through inspected devices
          for obj in operation["objects"]:
            # Collection point and foundation inspection doesn't cause downtime
            if (('collection' in obj["name"].lower() and 'point' in obj["name"].lower()) or
                'foundation' in obj["name"].lower()): #FCF do we ever have foundation in obj.name?
              pass
            elif 'device' in obj["name"].lower() or 'et' in obj["id"].lower():
              if 'device' in obj["name"].lower():
                ints_instr = re.findall(r'\d+', obj["id"].lower())
                device_num = ints_instr[0]
                pto_num = None
                pto_subnum = None
              else:
                ints_instr = re.findall(r'\d+', obj["id"].lower())
                device_num = ints_instr[0]
                try:
                  pto_num = ints_instr[1]
                  try:
                    pto_subnum = ints_instr[2]
                  except IndexError:
                    pto_subnum = None
                except IndexError:
                  pto_num = None
                  pto_subnum = None

              # Get PTOs affected by the act
              ptos_str_affected = 'et' + str(device_num)
              if pto_num is not None:
                ptos_str_affected = ptos_str_affected + 'pto_' + str(pto_num)
                if pto_subnum is not None:
                  ptos_str_affected = ptos_str_affected + '_' + str(pto_subnum)

              idx_et = hierarchy_et_lin.index[
                  hierarchy_et_lin['name_of_node'].str.lower().str.contains(ptos_str_affected)]
              try:
                ptos = hierarchy_et_lin.iloc[idx_et]['child'].tolist()[0].split(', ')
              except IndexError:
                _e = '%s not found in ET hierarchy.\n' % ptos_str_affected
                _e = _e + 'Probably there is an inconsistency between EC and ET device numbering'
                raise LookupError(_e)
              ptos = [str(elem).lower() for elem in ptos]
              for pto in ptos:
                # Load downtime array of corresponding PTO and update it
                dt_equivalent += act_duration / len(ptos)
                if project_year < self.t_life:
                  PTO_dict[pto][project_year][month - 1] += act_duration
            else:
              _w = 'Unknown object ID \"%s\" for maintenance' % obj["id"]
              _w = _w + ' operation ID \"op10\" or \"op11\"'
              raise NameError(_w)
        elif df_operations.loc[op_id]['Device shutdown'].str.lower() == 'no':
          pass
        else:
          raise AssertionError('Device shutdown must be indicated with' +
                               ' \"Yes\" or \"No\"')
      elif 'op12' in op_id:
        # Check if device is shut down during inspection
        # ### Maintenance operation IDs must have 4 chars before underscore
        # ### Assumes uppercase ID in Activities catalogue
        idx_op = df_operations.index[df_operations['id'].str.lower() == op_id.split('_')[0]]
        if (df_operations.iloc[idx_op]['Device shutdown'].to_list()[0].lower()
            == 'yes'):
          # Get inspection activity duration
          # Get durations from activities with word 'inspection'
          df_op_acts = df_activities[
              df_activities['op_id'].str.lower() == op_id.split('_')[0]]
          ds_contains_insp = df_op_acts['name'].str.contains('inspect',
                                                             case=False)
          df_inspection = df_op_acts[ds_contains_insp]
          act_duration = df_inspection['duration'].sum()
          act_duration = float(act_duration)

          # Get month and year (counting from commissioning) of operation
          month = operation["dates"]["start"].month
          project_year = operation["dates"]["may_start"].year - start_date.year

          # Go through inspected devices
          for obj in operation["objects"]:
            # ### skms in hierarchy not considered
            if 'sk' in obj["id"].lower():
              ints_instr = re.findall(r'\d+', obj["id"].lower())
              device_num = ints_instr[0]

              # Get PTOs affected by the act
              ptos_str_affected = 'et' + str(device_num)
              idx_et = hierarchy_et_lin.index[
                  hierarchy_et_lin['name_of_node'].str.lower().str.contains(ptos_str_affected)]
              try:
                ptos = hierarchy_et_lin.iloc[idx_et]['child'].tolist()[0].split(', ')
              except IndexError:
                _e = '%s not found in ET hierarchy.\n' % ptos_str_affected
                _e = _e + 'Probably there is an inconsistency between SK and ET device numbering'
                raise LookupError(_e)
              ptos = [str(elem).lower() for elem in ptos]
              for pto in ptos:
                # Load downtime array of corresponding PTO and update it
                dt_equivalent += act_duration / len(ptos)
                if project_year < self.t_life:
                  PTO_dict[pto][project_year][month - 1] += act_duration
            else:
              raise NameError('Unknown object ID \"%s\" for maintenance' +
                              'operation ID \"op12\"' % obj.id)
        elif df_operations.loc[op_id]['Device shutdown'].str.lower() == 'no':
          pass
        else:
          raise AssertionError('Device shutdown must be indicated with' +
                               '\"Yes\" or \"No\"')
      elif 'op13' in op_id:
        # ### Shutdown both devices connected to array cable (not coll. point)
        # ### Not possible, as cable doesn't know which objects it connects
        pass

      elif 'op14' in op_id:
        # Export cable inspection doesn't require shutdown
        pass

      # Corrective maintenance!!!
      elif operation["name"].lower() == 'corrective maintenance':
        # Get ID of component that failed and set state in dictionary to 0
        if ('op20' in op_id) or ('op21' in op_id):
          # TODO: REVIEW. ASSUMING IF A CABLE FAILS:
          # IF ONLY ONE CABLE -> ALL devices fail
          # IF TWO CABLEs -> HALF ptos fail
          # ...
          # Check if Export cable is part of this operation cables
          export_considered = [
              True if 'export' in cbl["type"].lower() else False
              for cbl in operation["cables"]
          ]
          array_considered = [
              True if 'array' in cbl["type"].lower() else False
              for cbl in operation["cables"]
          ]
          if any(export_considered) is True:
            cable_type = 'export'
            # Number of failed cables
            cable_fail_number = sum(export_considered)
          else:
            cable_type = 'array'
            # Number of failed cables
            cable_fail_number = sum(array_considered)

          # Total number of cables
          cable_number = sum([
              True
              if cable_type in cbl.type_.lower()
              else False
              for cbl in components_list
          ])
          pto_number = sum([
              True
              if 'pto' == obj.type_.lower()
              else False
              for obj in components_list
          ])

          num_ptos = round(pto_number * (cable_fail_number / cable_number))

          comp_id = [cbl["id"] for cbl in operation["cables"]]
          pto_shutted = None
        elif ('op15' in op_id) or ('op16' in op_id) or ('op17' in op_id) or ('op18' in op_id):
          # Check if Collection Point is part of this operation objects
          cp_considered = [
              True if 'collection' in obj["name"].lower() else False
              for obj in operation["objects"]
          ]
          if any(cp_considered) is True:
            # TODO: REVIEW. ASSUMING IF A CP FAILS:
            # IF ONLY ONE CP -> ALL devices fail
            # IF TOW CPs -> HALF devices fail
            # ...
            # Number of failed CPs
            cp_fail_number = sum(cp_considered)
            cp_number = sum([
                True
                if 'collection_point' in obj.name.lower()
                else False
                for obj in components_list
            ])
            pto_number = sum([
                True
                if 'pto' == obj.type_.lower()
                else False
                for obj in components_list
            ])

            num_ptos = round(pto_number * (cp_fail_number / cp_number))

            comp_id = [obj["id"] for obj in operation["objects"]]
            pto_shutted = None
          else:
            #In tow to port maintenance, all ptos of the device must fail:
            #FCF this part is assuming that only PTO children fail, not PTO themselves. REVIEW
            # Failed PTO
            pto_failed = operation["objects"][0]["parent"]
            comps_fail = hierarchy_et_lin['child'][
                hierarchy_et_lin['name_of_node'].str.lower() == pto_failed.lower()
            ].values[0]
            comps_fail = comps_fail.replace(' ', '').split(',')
            comp_id = ['et_' + comp for comp in comps_fail]

            dev = hierarchy_et_lin['parent'][hierarchy_et_lin['name_of_node'].str.lower() == pto_failed.lower()].values[0]
            ptos = hierarchy_et_lin['child'][hierarchy_et_lin['name_of_node'].str.lower() == dev.lower()].values[0]
            ptos = ptos.replace(' ', '').split(',')
            num_ptos = len(ptos)

            # Shutted down PTOs due to failure and device disconnection
            # Only if ET GATE is not AND
            et_name = hierarchy_et_lin['parent'][
                hierarchy_et_lin['name_of_node'].str.lower() == pto_failed.lower()
            ].values[0]
            et_gate = hierarchy_et_lin['gate_type'][
                hierarchy_et_lin['name_of_node'].str.lower() == et_name.lower()
            ].values[0]
            if et_gate != 'and':
              pto_idx = 0
              while pto_idx < len(ptos):
                for comp in comp_id:
                  if ptos[pto_idx] in comp:
                    del ptos[pto_idx]
                    break
                pto_idx += 1
              pto_shutted = ptos
              if len(pto_shutted) == 0:
                pto_shutted = None
            else:
              pto_shutted = None
        else:
          comp_id = [obj["id"] for obj in operation["objects"]]
          pto_shutted = None
        # every pto child of the device fails.
        components_dict_op = copy.deepcopy(components_dict)
        for comp in comp_id:
          components_dict_op[str(comp).lower()] = '0'

        # Evaluate if energy is produced for all EPTs
        for pto_id, pto_ept in EPT_dict.items():
          EPT = copy.deepcopy(pto_ept)
          for comp_id, comp_state in components_dict_op.items():
            comp_id_us = comp_id + '_'
            EPT_old = copy.deepcopy(EPT)
            EPT = EPT.replace(comp_id_us + ' ', comp_state + ' ')
            if EPT_old == EPT:
              EPT = EPT.replace(' ' + comp_id_us, ' ' + comp_state)
            if EPT_old == EPT:
              EPT = EPT.replace(comp_id_us, comp_state)
          # If energy is not produced, add downtime in PTO dict
          # ### Still includes transit back from site to port´
          EPT = re.sub(' \d/\d ', ' or ', EPT)    # TODO: THIS IS WRONG
          # Replace unknown components (and, therefore, they never fail) with '1'
          if not eval(EPT):   # eval(0 and <something_unknown>) is 0 / eval(1 and <something_unknown>) is ERROR!
            month = []
            project_year = []
            op_hours = []
            help_date = operation["dates"]["end"] - datetime.timedelta(seconds=1)
            # Allocate downtime to correct month(s)
            while help_date > operation["dates"]["may_start"]:
              month.append(help_date.month)
              project_year.append(help_date.year - start_date.year)
              # If downtime occurs only in one month, it stops right at break
              if (help_date.month == operation["dates"]["may_start"].month and
                  help_date.year == operation["dates"]["may_start"].year):
                op_hours.append(((help_date - operation["dates"]["may_start"])
                                .total_seconds() + 1) / 3600)
                break

              # Only executed if downtime occurs in multiple months
              else:
                first_of_month = datetime.datetime(help_date.year,
                                                   help_date.month, 1)
                op_hours.append(((help_date - first_of_month)
                                .total_seconds() + 1) / 3600)

                # Jump to last day of previous month
                help_date = first_of_month - datetime.timedelta(seconds=1)

            dt_equivalent += sum(op_hours) / num_ptos
            for mo, py, oh in zip(month, project_year, op_hours):
              # print('year: %d\t\tmonth: %d' % (py, mo))
              if py < self.t_life + 1:
                PTO_dict[pto_id][py][mo - 1] += oh

            # Now the downtime for components that does not fail but must be
            # stoped for intervention
            if pto_shutted is not None:
              month_par = []
              project_year_par = []
              op_hours_par = []
              help_date_par = operation["dates"]["end"] - datetime.timedelta(seconds=1)
              # Allocate downtime to correct month(s)
              while help_date_par > operation["dates"]["may_start"]:
                month_par.append(help_date_par.month)
                project_year_par.append(help_date_par.year - start_date.year)
                # If downtime occurs only in one month, it stops right at break
                if (help_date_par.month == operation["dates"]["may_start"].month and
                    help_date_par.year == operation["dates"]["may_start"].year):
                  op_hours_par.append(((help_date_par - operation["dates"]["may_start"])
                                      .total_seconds() + 1) / 3600)
                  break

                # Only executed if downtime occurs in multiple months
                else:
                  first_of_month = datetime.datetime(help_date_par.year,
                                                     help_date_par.month, 1)
                  op_hours_par.append(((help_date_par - first_of_month)
                                      .total_seconds() + 1) / 3600)

                  # Jump to last day of previous month
                  help_date_par = first_of_month - datetime.timedelta(seconds=1)

              dt_equivalent += sum(op_hours_par) / num_ptos
              for mo, py, oh in zip(month_par, project_year_par, op_hours_par):
                # print('year: %d\t\tmonth: %d' % (py, mo))
                if py < self.t_life + 1:
                  for pto_id in pto_shutted:
                    PTO_dict[pto_id][py][mo - 1] += oh

        del components_dict_op
      else:
        raise NameError('Unknown maintenance type \" %s \"' % operation["description"])
      operation_data[op_id]["downtime"] = dt_equivalent

    # Convert array into dataframe
    index = np.arange(1, self.t_life + 2)  # Project years
    columns = np.arange(1, 13)  # Months

    for pto_id, arr_downtime in PTO_dict.items():
      PTO_dict.update({pto_id: pd.DataFrame(data=arr_downtime,
                                            index=index,
                                            columns=columns)})

    return PTO_dict

  def get_downtime_per_device(
      self,
      PTO_dict: dict
  ) -> dict:
    """
    get_downtime
    ============
    Calculates downtime per device per project year and month

    Inputs:
    -------
    PTO_dict: dictionary of PTO names and corresponding downtime array

    Outputs:
    --------
    Dictionary of device/ET names and corresponding downtime array
    """
    if not PTO_dict:
      # No downtime
      return

    ET_dict = {}
    for id_pto, df_downtime in PTO_dict.items():
      dev_num = id_pto.split('_')[0][2:]
      downtimes = [
          df_downtime.to_numpy()
          for id_pto, df_downtime in PTO_dict.items()
          if ('et' + dev_num + '_') in (id_pto + '_').lower()
      ]

      ET_dict.update({'device_' + dev_num: sum(downtimes) / len(downtimes)})

    # Convert array into dataframe
    index = np.arange(1, len(downtimes[0]) + 1)  # Project years
    columns = np.arange(1, 13)  # Months

    for et_id, arr_downtime in ET_dict.items():
      ET_dict.update({et_id: pd.DataFrame(data=arr_downtime,
                                          index=index,
                                          columns=columns)})

    # Convert years from integers to strings
    months_integers = {
        1: "jan",
        2: "feb",
        3: "mar",
        4: "apr",
        5: "may",
        6: "jun",
        7: "jul",
        8: "aug",
        9: "sep",
        10: "oct",
        11: "nov",
        12: "dec"
    }
    for device_id, df_downtime in ET_dict.items():
      ET_dict[device_id].reset_index(inplace=True)
      ET_dict[device_id] = ET_dict[device_id].rename(
          columns={'index': 'year'}
      )
      ET_dict[device_id] = ET_dict[device_id].rename(columns=months_integers)

    return ET_dict

  def build_output_table(
      self,
      operation_data: dict,
      ET_dict: dict
  ) -> pd.DataFrame:
    """
    build_output_table
    ==================
    Pandas DataFrame with all relevant outputs from maintenance phase

    Inputs
    ------
    operation_data (dict): data related with maintenance operations.
    ET_dict (dict): dictionary of device/ET names and corresponding downtime array.

    Returns
    -------
    """
    dict_operations_table = {
        "operation_id": [],
        "name": [],
        "description": [],
        "tech_group": [],
        "operation_type": ['maintenance'] * len(operation_data.keys()),
        "technologies": [],
        "date_may_start": [],
        "date_start": [],
        "date_end": [],
        "proj_year": [],
        "duration_total": [],
        "waiting_to_start": [],
        "vessel_consumption": [],
        "vessels_combination": [],
        "vessels": [],
        "equipment": [],
        "terminal": [],
        "cost": [],
        "cost_vessel": [],
        "cost_terminal": [],
        "cost_equipment": [],
        "downtime": [],
        "fail_date": [],
        "mttr": [],
        "replacement_parts": [],
        "replacement_costs": [],
        "cost_label": ['opex'] * len(operation_data.keys()),
    }

    for op_id, item in operation_data.items():
      # Operation ID
      dict_operations_table["operation_id"].append(op_id)
      # Operation name
      op_name = item["name"]
      dict_operations_table["name"].append(op_name)
      # Operation description
      dict_operations_table["description"].append(item["description"])

      # Operation technologies - components dealt with during the operation
      dict_operations_table["technologies"].append(item["component_ids"])

      # Operation technology group
      tech_group = ''
      for obj in item["objects"] + item["cables"]:
        if 'et_' in obj["id"].lower():
          tech_group = tech_group + 'device, '
        if 'ed_' in obj["id"].lower():
          tech_group = tech_group + 'electrical, '
        if 'sk_' in obj["id"].lower():
          tech_group = tech_group + 'station keeping, '
      if len(tech_group) > 1:
        # Remove last coma
        tech_group = tech_group[:-2]
      dict_operations_table["tech_group"].append(tech_group)

      # Operation dates
      date_may_start = item["dates"]["may_start"].strftime('%Y/%m/%d')
      date_start = item["dates"]["start"].strftime('%Y/%m/%d')
      date_end = item["dates"]["end"].strftime('%Y/%m/%d')
      dict_operations_table["date_may_start"].append(date_may_start)
      dict_operations_table["date_start"].append(date_start)
      dict_operations_table["date_end"].append(date_end)

      # Project year
      ## Convert start date from string to datetime
      date_split = self.start_date[0:7].split('/')
      if len(date_split) > 2:
        return AssertionError('Date format should be "yyyy/mm"')
      start_year = int(date_split[0])
      project_year = item["dates"]["start"].year - start_year + 1
      dict_operations_table["proj_year"].append(project_year)
      del project_year

      # Operation period
      op_month = item["dates"]["may_start"].month
      if self.period == 'month':
        period = op_month
      elif self.period == 'quarter':
        period = mt.ceil(op_month / 3)
      elif self.period == 'trimester':
        period = mt.ceil(op_month / 4)
      period = str(period)

      # Operation durations and waitings
      dur_total = item["durations"][period]
      wait_start = item["waitings"][period]

      dict_operations_table["duration_total"].append(dur_total)
      dict_operations_table["waiting_to_start"].append(wait_start)

      # Vessels consumption
      if 'repair at port' in item["description"]:
        vessels_consumption = 0
      else:
        vessels_consumption = item["consumption"] * ((dur_total - wait_start) / 24)
      dict_operations_table["vessel_consumption"].append(vessels_consumption)

      # Vessels Combination ID
      dict_operations_table["vessels_combination"].append(item["vec"])
      # Vessel ID
      dict_operations_table["vessels"].append(item["vessels"])
      # Equipment ID
      dict_operations_table["equipment"].append(item["equipment"])
      # Operation Terminal ID
      dict_operations_table["terminal"].append(item["terminal"])

      # Downtime
      try:
        downtime = item["downtime"]
      except KeyError:
        downtime = 0
      dict_operations_table["downtime"].append(downtime)

      # Failure date
      if op_name.lower() == 'corrective maintenance':
        # ### Assumes that failure is instantly known
        try:
          # Linux machines
          fail_date = item["dates"]["may_start"].strftime('%Y/%m/%d %-H')
        except ValueError:
          # Windows machines
          fail_date = item["dates"]["may_start"].strftime('%Y/%m/%d %#H')

        dict_operations_table["fail_date"].append(fail_date)
      else:
        dict_operations_table["fail_date"].append('NA')

      # MTTR
      if op_name.lower() == 'corrective maintenance':
        # ### Assumes that failure is instantly known and includes transit back
        # ### from site to port
        mttr = (item["dates"]["end"] - item["dates"]["may_start"]).total_seconds() / 3600

        dict_operations_table["mttr"].append(mttr)
      else:
        dict_operations_table["mttr"].append('NA')

      # Replacement parts
      # ### For now, only what fails is repaired (no system replacement)
      if op_name.lower() == 'corrective maintenance':
        if ('op20' in op_id) or ('op21' in op_id):
          parts = [cbl["id"].lower() for cbl in item["cables"]]
          replacement_part = ', '.join(parts)
        elif any([x in op_id for x in ['op16', 'op18', 'op19']]) is True:
          parts = [obj["id"].lower() for obj in item["objects"]]
          replacement_part = ', '.join(parts)
        else:
          replacement_part = 'NA'
        dict_operations_table["replacement_parts"].append(replacement_part)
      else:
        dict_operations_table["replacement_parts"].append('NA')

      # Replacement cost
      replacement_costs = 0
      if op_name.lower() == 'corrective maintenance':
        if any([x in op_id for x in ['op16', 'op18', 'op19']]) is True:
          costs = [obj["cost"] for obj in item["objects"]]
          replacement_costs += sum(costs)
        elif op_id in "op20":
          costs = [cbl["cost"] for cbl in item["cables"]]
          replacement_costs += sum(costs)
        elif op_id in "op21":
          repair_cable_length = 2 * max([max(cbl.bathymetry) for cbl in item["cables"]]) + 20
          costs = [cbl["cost"] * 0.1    ### 10% of the cable cost
                   for cbl in item["cables"]]
          replacement_costs += sum(costs)
      dict_operations_table["replacement_costs"].append(replacement_costs)

      # Operation costs
      total_costs = 0
      for key, costs_per_period in item["costs"].items():
        total_costs += costs_per_period[period]

      dict_operations_table["cost"].append(total_costs + replacement_costs)
      dict_operations_table["cost_vessel"].append(item["costs"]["vessels"][period])
      dict_operations_table["cost_terminal"].append(item["costs"]["terminal"][period])
      dict_operations_table["cost_equipment"].append(item["costs"]["equipment"][period])

    df_operations_table = pd.DataFrame(dict_operations_table)
    df_operations_table = df_operations_table.sort_values(by='date_may_start')

    return df_operations_table
