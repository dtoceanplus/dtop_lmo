# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import pickle

# Import classes
from dtop_lmo.business.classes import Site


def startability_cpx1(
    operation,
    df_workability: pd.DataFrame,
    site_name: str
) -> pd.DataFrame:
  """
  startability
  ============
  index         = 0123 456 78...
  workability   = 1111 011 0000111 0011111 00011111111111 00001 000111 0000001
  diff        = NaN000-110-1000100-1010000-10010000000000-10001-100100-1000001
  work_st_index = [5, 11, 16, 24, 39, 43, 52]
  work_ed_index = [4, 7, 14, 21, 35, 40, 46]

  Inputs:
  -------
  * df_workability = Table with all activities workability - pandas DataFrame
  * site_name = Name of the site - string

  Returns:
  --------
  * df_startability - Table of startabilities - pandas DataFrame
  """
  # Try to load Startability database
  try:
    # ### LOAD THIS FROM CATALOGUES AND NOT FROM LOCAL MACHINE ###
    import os
    from pathlib import Path
    local_path = '/TEMP_DATABASE'
    db_path = os.getcwd() + local_path + '/' + site_name
    Path(db_path).mkdir(parents=True, exist_ok=True)
    file_startability_db = open(db_path + '/db_ops_startability', 'rb')
    list_start_db = pickle.load(file_startability_db)
    file_startability_db.close()
  except FileNotFoundError:
    # Startability pickle database not defined yet
    # Define a list of dict "database" to improve df_startability calculations
    list_start_db = []

  # Initialize a pandas DataFrame df_startability with False
  df_startability = pd.DataFrame(False,
                                 index=df_workability.index.tolist(),
                                 columns=df_workability.columns.tolist())

  op_id = operation.id
  hs = operation.hs
  duration = operation.duration_net
  # How many timesteps are covered by this operation duration
  op_duration_ts = int(mt.ceil(duration))
  # How many timesteps are covered by this operation duration
  act_duration_ts = int(mt.ceil(duration))

  # Boolean pandas Series representing the workability for this operation
  workability_ones = df_workability[op_id] == True

  # First, search in the database if the startability for this OLCs and
  # duration was already defined
  startability_exists = False
  for db_elem in list_start_db:
    if db_elem['hs'] == hs and db_elem['duration'] == op_duration_ts:
      startability_exists = True
      break

  if startability_exists is True:
    # Load workability from database
    list_startability = db_elem['startability']
    ds_startability = pd.Series(list_startability, name=op_id)
    df_startability[op_id] = ds_startability
    del db_elem
  else:
    # Check if there is any restriction in workability
    if workability_ones.all():
      # If there is no restrictions, startability is always 1
      # Startability = 1
      df_startability[op_id] = True

      if op_duration_ts > 1:
        df_startability[op_id].iloc[-(op_duration_ts - 1):] = np.nan
      # Nan because it is not known if the operation can start or not, and,
      # for that reason, it doesn't have statitical meaning
      df_startability[op_id] = df_startability[op_id].astype(bool)
    else:
      # For some timesteps, the workability of the operation is 0
      # Check when workability changes from 0->1 or from 1->0
      df_work_diff = df_workability[op_id].astype(int).diff()
      # Save indexes where:
      # - workability changes from 0->1
      work_start_index = df_work_diff[df_work_diff == 1].index.tolist()
      # - workability changes from 1->0
      work_end_index = df_work_diff[df_work_diff == -1].index.tolist()

      if len(work_start_index) != 0:
        # Workability has at least one transition from 0->1
        if len(work_end_index) != 0:
          # Workability has at least one transition from 0->1 and 1->0
          if work_start_index[0] > work_end_index[0]:
            # Workability starts with 1s
            index2 = work_end_index[0] - mt.ceil(duration)
            if index2 > 0:
              df_startability.loc[0:index2, op_id] = True
            del index2
            # This entry of the list was already considered
            work_end_index.pop(0)       # Delete it
          else:
            # Workability starts with 0s
            pass

          for index in work_start_index:
            # Number of workability timesteps
            try:
              index2 = work_end_index[0]
              work_end_index.pop(0)
            except IndexError:
              index2 = df_startability.shape[0]

            work_time_steps = index2 - index
            if work_time_steps >= duration:
              # operation may start
              # for how many times steps may it start?
              start_time_steps = work_time_steps - mt.floor(duration)
              start_time_steps += 1
              index2 = index + start_time_steps - 1
              df_startability.loc[index:index2, op_id] = True
            else:
              # operation cannot start: workability < duration
              pass
        else:
          # Workability has only one transition from 0->1
          # Fill all rows with 1
          df_startability.loc[work_start_index[0]:, op_id] = True

      else:
        # Workability has no transition 0->1, so the only transition must be
        # from 1->0. The operation can always start until workability is 0
        # less operation duration
        last_time_step = work_end_index[0] - op_duration_ts + 1
        df_startability[op_id].iloc[0:last_time_step] = True

      if op_duration_ts > 1:
        # ### This should only occur for workabilities = True
        df_startability[op_id].iloc[-(op_duration_ts - 1):] = np.nan
      # Nan because it is not known if the operation can start or not, and,
      # for that reason, it doesn't have statitical meaning

      # Convert startability to boolean type
      ds_not_null = df_startability[op_id].notnull()
      df_startability[op_id][ds_not_null] = df_startability[op_id].astype(bool)

    # Update database
    ds_op_start = df_startability[op_id]
    list_startability = ds_op_start.tolist()

    # Save new element to database
    db_elem = {}
    db_elem['hs'] = hs
    db_elem['duration'] = op_duration_ts
    db_elem['startability'] = list_startability
    list_start_db.append(db_elem)

    # ### Adapt this!
    file_startability_db = open(db_path + '/db_ops_startability', 'wb')
    pickle.dump(list_start_db, file_startability_db)
    file_startability_db.close()
    # ###

    del db_elem

  del hs, duration
  del list_start_db

  return df_startability


def startability(
    activities: list,
    df_workability: pd.DataFrame,
    site_name: str
) -> pd.DataFrame:
  """
  startability
  ============
  index         = 0123 456 78...
  workability   = 1111 011 0000111 0011111 00011111111111 00001 000111 0000001
  diff        = NaN000-110-1000100-1010000-10010000000000-10001-100100-1000001
  work_st_index = [5, 11, 16, 24, 39, 43, 52]
  work_ed_index = [4, 7, 14, 21, 35, 40, 46]

  Inputs:
  -------
  * activities = List of the activities of this combination - list of
  Activity()
  * df_workability = Table with all activities workability - pandas DataFrame
  * site_name = Name of the site - string

  Returns:
  --------
  * df_startability - Table of startabilities - pandas DataFrame
  """
  # Try to load Startability database
  try:
    # ### LOAD THIS FROM CATALOGUES AND NOT FROM LOCAL MACHINE ###
    import os
    from pathlib import Path
    local_path = '/TEMP_DATABASE'
    db_path = os.getcwd() + local_path + '/' + site_name
    Path(db_path).mkdir(parents=True, exist_ok=True)
    file_startability_db = open(db_path + '/db_acts_startability', 'rb')
    list_start_db = pickle.load(file_startability_db)
    file_startability_db.close()
  except FileNotFoundError:
    # Startability pickle database not defined yet
    # Define a list of dict "database" to improve df_startability calculations
    list_start_db = []

  # Initialize a pandas DataFrame df_startability with False
  df_startability = pd.DataFrame(False,
                                 index=df_workability.index.tolist(),
                                 columns=df_workability.columns.tolist())

  for activity in activities:
    act_id = activity.id

    hs = np.inf
    tp = np.inf
    ws = np.inf
    cs = np.inf
    # light = False
    duration = 0

    try:
      hs = float(activity.hs)
    except TypeError:
      pass
    try:
      tp = float(activity.tp)
    except TypeError:
      pass
    try:
      ws = float(activity.ws)
    except TypeError:
      pass
    try:
      cs = float(activity.cs)
    except TypeError:
      pass
    # try:
    #   light = int(activity.light)
    # except TypeError:
    #   pass
    duration = activity.duration
    # How many timesteps are covered by this activity duration
    act_duration_ts = int(mt.ceil(duration))

    # Boolean pandas Series representing the workability for this activity
    workability_ones = df_workability[act_id] == True

    # First, search in the database if the startability for this OLCs and
    # duration was already defined
    startability_exists = False
    for db_elem in list_start_db:
      if (db_elem['hs'] == hs and
          db_elem['tp'] == tp and
          db_elem['ws'] == ws and 
          db_elem['cs'] == cs and 
          # db_elem['light'] == light and
          db_elem['duration'] == act_duration_ts):
        startability_exists = True
        break

    if startability_exists is True:
      # Load workability from database
      list_startability = db_elem['startability']
      ds_startability = pd.Series(list_startability, name=activity.id)
      df_startability[activity.id] = ds_startability
      del db_elem
    else:
      # Check if there is any restriction in workability
      if workability_ones.all():
        # If there is no restrictions, startability is always 1
        # Startability = 1
        df_startability[act_id] = True

        if (hs == np.inf and tp == np.inf and ws == np.inf and cs == np.inf): # and
            # light is False):
          # If there are no OLC, then all startabilities = 1
          pass
        else:
          if act_duration_ts > 1:
            df_startability[act_id].iloc[-(act_duration_ts - 1):] = np.nan
          # Nan because it is not known if the activity can start or not, and,
          # for that reason, it doesn't have statitical meaning
          df_startability[act_id] = df_startability[act_id].astype(bool)
      else:
        # For some timesteps, the workability of the activity is 0
        # Check when workability changes from 0->1 or from 1->0
        df_work_diff = df_workability[act_id].astype(int).diff()
        # Save indexes where:
        # - workability changes from 0->1
        work_start_index = df_work_diff[df_work_diff == 1].index.tolist()
        # - workability changes from 1->0
        work_end_index = df_work_diff[df_work_diff == -1].index.tolist()

        if len(work_start_index) != 0:
          # Workability has at least one transition from 0->1
          if len(work_end_index) != 0:
            # Workability has at least one transition from 0->1 and 1->0
            if work_start_index[0] > work_end_index[0]:
              # Workability starts with 1s
              index2 = work_end_index[0] - mt.ceil(activity.duration)
              if index2 > 0:
                df_startability.loc[0:index2, act_id] = True
              del index2
              # This entry of the list was already considered
              work_end_index.pop(0)       # Delete it
            else:
              # Workability starts with 0s
              pass

            for index in work_start_index:
              # Number of workability timesteps
              try:
                index2 = work_end_index[0]
                work_end_index.pop(0)
              except IndexError:
                index2 = df_startability.shape[0]

              work_time_steps = index2 - index
              if work_time_steps >= activity.duration:
                # activity may start
                # for how many times steps may it start?
                start_time_steps = work_time_steps - mt.floor(activity.duration)
                start_time_steps += 1
                index2 = index + start_time_steps - 1
                df_startability.loc[index:index2, act_id] = True
              else:
                # activity cannot start: workability < duration
                pass
          else:
            # Workability has only one transition from 0->1
            # Fill all rows with 1
            df_startability.loc[work_start_index[0]:, act_id] = True

        else:
          # Workability has no transition 0->1, so the only transition must be
          # from 1->0. The activity can always start until workability is 0
          # less the duration of the activity
          last_time_step = work_end_index[0] - act_duration_ts + 1
          df_startability[act_id].iloc[0:last_time_step] = True

        if act_duration_ts > 1:
          # ### This should only occur for workabilities = True
          df_startability[act_id].iloc[-(act_duration_ts - 1):] = np.nan
        # Nan because it is not known if the activity can start or not, and,
        # for that reason, it doesn't have statitical meaning

        # Convert startability to boolean type
        ds_not_null = df_startability[act_id].notnull()
        df_startability[act_id][ds_not_null] = df_startability[act_id].astype(bool)

      # Update database
      ds_act_start = df_startability[act_id]
      list_startability = ds_act_start.tolist()

      # Save new element to database
      db_elem = {}
      db_elem['hs'] = hs
      db_elem['tp'] = tp
      db_elem['ws'] = ws
      db_elem['cs'] = cs
      # db_elem['light'] = light
      db_elem['duration'] = act_duration_ts
      db_elem['startability'] = list_startability
      list_start_db.append(db_elem)

      # ### Adapt this!
      file_startability_db = open(db_path + '/db_acts_startability', 'wb')
      pickle.dump(list_start_db, file_startability_db)
      file_startability_db.close()
      # ###

      del db_elem

    del hs, tp, ws, cs, duration
  del list_start_db

  return df_startability
