# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import warnings
warnings.simplefilter("ignore")


class Cable3():
  """
  Cable
  =======
  Attributes
  ----------
  * id_ = ID of the cable - string
  * name = Cable name - string
  * cable_type = Type of cable - string
  * system = System that the object belongs to (hierarchy) - string
  * bathymetry = Bathymetry of the different sections of the cable - list of
  floats [m]
  * diameter = Diameter of the cable - float [mm]
  * length = Length of the cable in each section [m] - list of floats
  * drymass = Specific cable drymass [kg/m] - float
  * min_bend_radius = Minimum bend radius of the cable [m] - float
  * burial_depth = Burial depth of the different sections of the cable [m] -
  list of floats
  * elect_interfaces = Object Electrical Interface (EI) - list of string
  * split_pipes = Existence of split pipes in each section of the cable - list
  of booleans
  * buoyancy_modules = Existence of buoyancy modules in each section of the
  cable - list of booleans

  Optional Attributes:
  --------------------
  * catalogue_id: ID of the Object in catalogue - string
  * comp_cost: Component cost - float
  * subtype = Subtype of cable - string
  * burial_method = Technique used to burry the cable - string
  * failure_rates = Failure rate for minor and major repair - list
  """
  def __init__(self,
               id_: str,
               name: str,
               cable_type: str,
               system: str,
               bathymetry: list,
               diameter: float,
               length: list,
               drymass: float,
               min_bend_radius: float,
               elect_interfaces,
               seabed_type: list = None,
               catalogue_id: str = None,
               comp_cost: float = None,
               subtype: str = None,
               burial_depth: float = None,
               burial_method: str = None,
               split_pipes: list = None,
               buoyancy_modules: list = None,
               parent: str = None,
               child: list = None,
               failure_rates: list = None):
    self.id = str(id_)
    self.name = str(name)
    self.type = str(cable_type)
    if 'export' not in self.type.lower() and 'array' not in self.type.lower():
      raise AssertionError('The cable type defined doesn\'t exist.')
    self.subtype = str(subtype)
    self.system = str(system)
    self.bathymetry = [float(bathy_len) for bathy_len in bathymetry]
    self.diameter = float(diameter)
    self.length = [float(route_len) for route_len in length]
    self.drymass = float(drymass)
    self.mbr = float(min_bend_radius)
    if seabed_type is not None:
      self.seabed_type = [str(route_seabed) for route_seabed in seabed_type]
    # Check burial inputs. They must be both None or both different than None
    if burial_depth is None and burial_method is None:
      # Cable is surface layed. burial_depth = [0,0,0,0,...,0]
      burial_depth = [0] * len(self.bathymetry)
    elif burial_depth is not None and burial_method is not None:
      burial_depth = [float(burial) for burial in burial_depth]
    elif (burial_depth == [0] * len(self.bathymetry) and
          burial_method is not None):
      # Surface lay
      pass
    else:
      raise AssertionError('Both burial depth and burial type must be defined')
    self.burial_depth = burial_depth
    if burial_method is not None:
      if ('plough' not in burial_method.lower() and
          'jet' not in burial_method.lower() and
          'cut' not in burial_method.lower() and
          'seabed lay' not in burial_method.lower()):
        raise AssertionError('The burial type defined doesn\'t exist.')
    self.burial_method = burial_method
    self.ei = elect_interfaces
    if elect_interfaces is not None:
      # Check if the electrical interfaces are correct
      ei_ok = [True if ('jtube' in ei or 'itube' in ei or
                        'j-tube' in ei or 'i-tube' in ei or
                        ('dry' in ei and 'mate' in ei) or
                        ('wet' in ei and 'mate' in ei) or
                        ('hard' in ei and 'wire' in ei))
               else False
               for ei in elect_interfaces]
      if all(ei_ok) is False:
        _e = 'The Electrical Interface defined is not recognized.'
        raise AssertionError(_e)
    try:
      self.catalogue_id = str(catalogue_id)
    except TypeError:
      pass
    try:
      self.comp_cost = float(comp_cost)
    except TypeError:
      pass
    if split_pipes is None:
      self.split_pipes = [False] * len(self.bathymetry)
    else:
      # Convert list of strings to list of bools
      self.split_pipes = []
      for value in split_pipes:
        if value.lower() == 'true':
          self.split_pipes.append(True)
        else:
          self.split_pipes.append(False)
    if buoyancy_modules is None:
      self.buoyancy_modules = [False] * len(self.bathymetry)
    else:
      # Convert list of strings to list of bools
      buoyancy_modules_str = buoyancy_modules
      buoyancy_modules_str = buoyancy_modules_str.replace('[','').replace(']','').replace(' ','')
      buoyancy_modules_list = buoyancy_modules_str.split(',')
      self.buoyancy_modules = [
          True 
          if elem.lower() == 'true'
          else False
          for elem in buoyancy_modules_list
      ]
    try:
      self.parent = str(parent)
    except TypeError:
      pass
    try:
      self.child = str(child)
    except TypeError:
      pass
    self.failure_rates = failure_rates
    if failure_rates is not None:
      # Check if list with two numbers
      if len(failure_rates) != 2:
        _e = 'For object \"%s\", exactly two failure rates must be specified' \
            % self.name
        raise AssertionError(_e)
    # Raise error: Cost must be specified in case the failure rate is specified.
    if failure_rates is not None and comp_cost is None:
      _e = 'If failure rate is specified, cost must as well be specified.'
      raise AssertionError(_e)
