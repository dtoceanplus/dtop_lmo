# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import re
import networkx as nx
import geopy.distance as geo_dist
import json
import pickle
import warnings
import os
warnings.simplefilter("ignore")

# Import other classes
#from dtop_lmo.business.classes import Object_class
from dtop_lmo.business.classes.Barge import Barge
from dtop_lmo.business.classes import Site

# Import functions
from dtop_lmo.business import feasibilities as feas_funcs
from dtop_lmo.business import waiting
from dtop_lmo.service.api.catalogues import get_catalog_vecs
from dtop_lmo.service.api.catalogues import get_catalog_activities
from dtop_lmo.service.api.catalogues import get_catalog_speeds

# True to print logs
PRINT_LOGS = True


class Operation3(object):
  """
  Operation:
  ----------
  This class represents a Logistic operation and its attributes

  Attributes:
  ----------
  * id = ID of the operation - string
  * name = Name of the operation - string
  * description = Description of the operation - string
  * activities_table = Table of possible activities for this operation - pandas
  DataFrame
  * requirements = Set of requirements for this operation - dictionary
  * methods = Set of methods for this operation - dictionary
  * durations = Set of durations of the operation - Durations class
  * dates = Set of dates of the operation - Dates class
  * fails = Set of fails of the operation - Fails class
  * downtime = List of downtime per Device/per month/per year - list of pandas
  DataFrame
  * logistic_solution = Solution of vec, terminal, vessels and equipment - dict

  Optional Attributes:
  -------------------
  * objects = List of Objects handled in this operation - list of Objects
  * cables = IDs of the Cables handled in this operation - list of Cables
  # * method_load_out = Load out from terminal to vessel/water strategy -
  # string
  # * method_transportation = Transport from terminal to site method - string
  # * method_piling = Piling method if the object is a pile - string
  # * method_burial = Burial method if the object is a cable - string
  # * requirement_rov = If ROV is required for this operation - boolean
  # * requirement_divers = If Divers are required for this operation - boolean
  # * requirement_prev_proj = If considered terminals must already had previous
  # MRE projects - Boolean
  """

  class Activity():
    """
    Activity
    ========
    Operations are decomposed in Activities. Each Activity has a specific
    duration, Operating Limit Criteria, etc. ###

    Attributs:
    ----------
    id = Activity ID - string
    name = Activity description - string
    duration = The amount of time to preform this activity - float
    location = Where the activity takes place (port, transit, site or mobilization)

    Optional attributs:
    -------------------
    hs = Limit wave height - float
    tp = Limit wave period - float
    ws = Limit wind speed - float
    cs = Limit current speed - float
    light = The need of day light to preform the activity - bool
    """
    def __init__(self,
                 id_,
                 name,
                 location,
                 duration=None,
                 speed=None,
                 wave_height=None,
                 wave_period=None,
                 wind_speed=None,
                 current_speed=None,
                 light=None):
      self.id = id_
      self.name = name
      self.duration = duration
      self.location = location
      self.hs = None
      self.tp = None
      self.ws = None
      self.cs = None
      self.light = None
      if wave_height is not None:
        self.hs = wave_height
      if wave_period is not None:
        self.tp = wave_period
      if wind_speed is not None:
        self.ws = wind_speed
      if current_speed is not None:
        self.cs = current_speed
      if light is not None:
        self.light = light

  class Feasible():
    """
    Feasible
    ========
    Set of feasible solutuions for this operation.

    Optional attributes:
    ----------
    * vectable = List of feasible VECs - Pandas DataFrame
    * terminaltable = List of feasible Terminals - Pandas DataFrame
    * rovtable = List of feasible ROVs - Pandas DataFrame
    * divertable = List of feasible Divers teams - Pandas DataFrame
    * burialtable = List of feasible Burial tools - Pandas DataFrame
    * protecttable = List of feasible Cable Protection techs - Pandas DataFrame
    * pilingtable = List of feasible Piling tools - Pandas DataFrame
    * vessels_main = List of feasible Main vessels - Pandas DataFrame
    * vessels_tow = List of feasible Tow vessels - Pandas DataFrame
    * vessels_support = List of feasible Support vessels - Pandas DataFrame
    """

    def __init__(self,
                 vectable: pd.DataFrame = None,
                 terminaltable: pd.DataFrame = None,
                 rovtable: pd.DataFrame = None,
                 divertable: pd.DataFrame = None,
                 burialtable: pd.DataFrame = None,
                 protecttable: pd.DataFrame = None,
                 pilingtable: pd.DataFrame = None,
                 vess_sol_main: pd.DataFrame = None,
                 vess_sol_tow: pd.DataFrame = None,
                 vess_sol_support: pd.DataFrame = None):
      if vectable is not None:
        self.vectable = vectable
      if terminaltable is not None:
        self.terminaltable = terminaltable
      if rovtable is not None:
        self.rovtable = rovtable
      if divertable is not None:
        self.divertable = divertable
      if burialtable is not None:
        self.burialtable = burialtable
      if protecttable is not None:
        self.protecttable = protecttable
      if pilingtable is not None:
        self.pilingtable = pilingtable
      if vess_sol_main is not None:
        self.vessels_main = vess_sol_main
      if vess_sol_tow is not None:
        self.vessels_tow = vess_sol_tow
      if vess_sol_support is not None:
        self.vessels_support = vess_sol_support

  class Combination():
    """
    Combination
    ===========
    Possible matchable solution for the operation.

    Attributs:
    ----------
    terminal = Terminal ID considered - string
    vec = VEC ID considered - string
    travel_dist = Distance from port to farm by sea [m] - integer
    num_trips = Number of trips to preform the operation - integer
    max_components = Maximum number of components on deck per trip - integer
    ### VESSELS MISSING
    equipment = List with equipment ids used for this operation - list of
    string
    activities = List of activities to preform for this combination - list of
    Activity()
    duration_net = Net time to complete the operation [h] - float
    workability = Table of workabilities for a this combination activities -
    pandas DataFrame
    startability = Table of startabilities for a this combination activities -
    pandas DataFrame
    waitings = List of waitings for weather in this combination - list of
    Waiting()

    Optional attributs:
    ----------
    vessel_main = Main vessel ID considered - string
    vessel_tow = Tow vessel ID considered - string
    vessel_support = Support vessel ID considered - string
    """
    class Statistics():
      """
      Statistics
      ===========
      Statistics related to operation duration and waiting times

      Attributs
      ----------
      durations = Durations of the operation per interval - dictionary
      waitings = Waitings of the operation per interval - dictionary
      """
      def __init__(self):
        self.durations = None
        self.waitings = None

    class Costs():
      """
      Costs
      ===========
      Vessels, Equipment and Terminal costs related to this combination

      Attributs
      ----------
      vessels = Vessels costs for each month - dictionary
      equipment = Equipment costs for each month - dictionary
      terminal = Terminal costs for each month - dictionary
      """
      def __init__(self):
        self.vessels = {}
        self.equipment = {}
        self.terminal = {}

    def __init__(self,
                 terminal: str,
                 vec: str,
                 travel_dist: float,
                 num_trips: int,
                 max_components: int,
                 vessel_main: str,
                 vessel_tow: str,
                 vessel_support: str,
                 equipment: list):
      self.terminal = terminal
      self.vec = vec
      self.travel_dist = int(travel_dist)
      self.num_trips = int(num_trips)
      self.max_components = int(max_components)
      # ### IDEA 1 ### replace num_trips and max_components for a dict with
      # both
      self.vessel_main = vessel_main
      self.vessel_tow = vessel_tow
      self.vessel_support = vessel_support
      self.equipment = equipment

      self.activities = []
      self.duration_net = None

      self.workability = None     # Workability of each activity
      self.startability = None    # Startability of each activity
      self.waiting = None         # Waiting time of each activity

      self.values = None    # Table of durations and waiting per timestep

      self.statistics = self.Statistics()

      self.costs = self.Costs()

    def get_net_duration(self):
      total_duration = 0
      for act in self.activities:
        total_duration += act.duration
      self.duration_net = total_duration

    def define_operation_values(
        self,
        site_name: str,
        ts_analyse: list,
        MAX_WAIT: float
    ) -> pd.DataFrame:
      """
      define_operation_values
      ==========================
      This functions defines when each activity of the operation (for this
      combination) starts, ends, the operation duration and the waiting time
      between activities. It does this for each time step of the metocean data

      Intputs
      -------
      * site_name = Name of the site - string
      * ts_analyse = Time steps to be analysed
      * MAX_WAIT = Maximum waiting time between activities - float
      """
      def operation_variables(
          metocean_ts: int,
          MAX_WAIT: float
      ) -> list:
        df_start = self.startability
        # Before the analyses, check if it will be possible to complete this
        # operation:
        # - If, for the maximum possible interval, exists an activity with all
        # startability = False, then it is impossible to complete the operation
        # Maximum interval of timesteps for this operation
        max_int = mt.ceil(self.duration_net + MAX_WAIT)
        if not (df_start.iloc[metocean_ts:(metocean_ts + max_int), :].any()).all():
          # At least one of the activities startability is always False
          # No need to analyse this time step
          return metocean_ts

        # Initialize current time step
        ts_curr = metocean_ts
        # Initialize current time
        time_curr = metocean_ts

        # Initialize durations
        durations = {
            "port": 0.0,
            "site": 0.0,
            "transit": 0.0,
            "mobilization": 0.0
        }
        # Initialize waiting times (only between activities)
        waitings = {
            "port": 0.0,
            "site": 0.0
        }
        # Initialize operation duration (with waiting time between acts)
        op_dur = 0
        # Initialize time steps for the activities start and end
        ts_acts_start = []  # Example: [0, 48, .., 104] or [0, 49, ..., 105]
        ts_acts_end = []

        # Loop through activities
        for act_idx, activity in enumerate(self.activities):
          act_id = activity.id
          act_name = activity.name
          act_dur = activity.duration
          act_loc = activity.location
          try:
            act_loc_wait = self.activities[act_idx-1].location
          except IndexError:
            act_loc_wait = activity.location

          act_startability = df_start.iloc[ts_curr:, act_idx].dropna()
          if act_startability.sum() > 0:
            # It means that it is possible to start act_id
            # Activity may start in the next_act_st time step: first possible
            # time step
            next_act_st = act_startability[act_startability == True].index[0]

            wait_time = max(next_act_st - (time_curr + act_dur), 0)
            if wait_time > MAX_WAIT:
              # The time interval between this activity and the previous
              # one is too large
              return metocean_ts

            # Update durations
            if act_loc == 'transit':
              durations["transit"] += act_dur
            elif act_loc == 'port':
              durations["port"] += act_dur
            elif act_loc == 'site':
              durations["site"] += act_dur
            elif act_loc == 'mob':
              durations["mobilization"] += act_dur
            else:
              raise AssertionError('Activity location not recognized')

            # Update waitings
            if act_loc_wait == 'port':
              waitings['port'] += wait_time
            elif act_loc_wait == 'site' or act_loc_wait == 'transit':
              waitings['site'] += wait_time
            elif act_loc_wait == 'mob':
              waitings['port'] += wait_time
            else:
              raise AssertionError('Activity location not recognized')

            op_dur += wait_time + act_dur
            ts_acts_start.append(next_act_st)
            ts_acts_end.append(mt.ceil(time_curr + act_dur - 1))

            # Update time steps and time for next activity analysis
            if (metocean_ts + op_dur) % 1 == 0:
              ts_curr = ts_acts_end[act_idx] + 1
            else:
              ts_curr = ts_acts_end[act_idx]
            time_curr = metocean_ts + op_dur

          else:
            # It is not possible to start the next activity until the end of
            # the metocean dataframe
            # The operation cannot be completed
            return metocean_ts

        return ts_acts_start, op_dur, durations, waitings

      # Try to load operation values database
      try:
        # TODO: ADAPT THIS ###
        local_path = '/TEMP_DATABASE'
        db_path = os.getcwd() + local_path
        file_op_values_db = open(db_path + '/db_op_values', 'rb')
        list_op_values_db = pickle.load(file_op_values_db)
        file_op_values_db.close()
      except FileNotFoundError:
        # Operation waiting time pickle database not defined yet
        # Define a list of dict "database" to improve acts starts, ends,
        # durations and waiting times
        list_op_values_db = []

      # Check if this startability and MAX_WAIT is already in the database
      values_exist = False
      for db_elem in list_op_values_db:
        if (db_elem['MAX_WAIT'] == MAX_WAIT and
            len(db_elem['ts_analyse']) == len(ts_analyse)):
          if self.startability.equals(db_elem['startability']) is True:
            values_exist = True
            break

      if values_exist is True:
        if PRINT_LOGS is True:
          print('\tCombination already calculated!')
        # Load operation values from database
        list_op_start = db_elem['op_start']
        list_op_dur = db_elem['op_dur']
        list_durations = db_elem['op_durations']
        list_waitings = db_elem['op_waitings']
        ts_analyse = db_elem['ts_analyse']
        del db_elem

      else:
        if PRINT_LOGS is True:
          print('\tCombination must be calculated!')
        # Define fisrt activity
        first_act = self.activities[0]

        # For every start of the first activity, get when each activity
        # of the operation would start, the operation total duration (no
        # waiting to start), the other durations and the waiting times between
        # activities
        list_st_dur_wt = [operation_variables(timestep, MAX_WAIT)
                          for timestep in ts_analyse]

        # Store time steps where the operation cannot start
        ts_none = [elem
                   for elem in list_st_dur_wt
                   if type(elem) != tuple]

        # Drop irrelevant values
        list_st_dur_wt = [elem
                          for elem in list_st_dur_wt
                          if type(elem) == tuple]
        if len(list_st_dur_wt) == 0:
          # This operation can never occur
          list_op_start = [float('NaN')
                           for i in range(0, len(ts_analyse))]
          list_op_dur = [float('NaN')
                         for i in range(0, len(ts_analyse))]
          list_durations = [
              {
                  "port": float('NaN'),
                  "site": float('NaN'),
                  "transit": float('NaN'),
                  "mobilization": float('NaN')
              }
              for i in range(0, len(ts_analyse))
          ]
          list_waitings = [{"port": float('NaN'), "site": float('NaN')}
                           for i in range(0, len(ts_analyse))]

        else:
          list_op_start = [elem[0][0] for elem in list_st_dur_wt]
          list_op_dur = [elem[1] for elem in list_st_dur_wt]
          list_durations = [elem[2] for elem in list_st_dur_wt]
          list_waitings = [elem[3] for elem in list_st_dur_wt]

        # Save this data in the database
        db_elem = {}
        db_elem['startability'] = self.startability
        db_elem['op_start'] = list_op_start
        db_elem['op_dur'] = list_op_dur
        db_elem['op_durations'] = list_durations
        db_elem['op_waitings'] = list_waitings
        db_elem['MAX_WAIT'] = MAX_WAIT
        db_elem['ts_analyse'] = ts_analyse
        list_op_values_db.append(db_elem)

        # TODO: Adapt this! ###
        file_op_values_db = open(db_path + '/db_op_values', 'wb')
        pickle.dump(list_op_values_db, file_op_values_db)
        file_op_values_db.close()

        del db_elem

      # With all the possible operation starts, a Startability pandas DataFrame
      # can be defined.
      # Define a Startability DataFrame
      df_op_startability = pd.DataFrame(False,
                                        index=ts_analyse,
                                        columns=['operation'])
      # Define a Operation values DataFrame
      df_op_values = pd.DataFrame(0.0,
                                  index=ts_analyse,
                                  columns=['duration',
                                           'duration_port',
                                           'duration_site',
                                           'transit',
                                           'mobilization',
                                           'waiting_start',
                                           'waiting_port',
                                           'waiting_site'])

      df_op_startability[df_op_startability.index.isin(list_op_start)] = True
      df_durations = pd.DataFrame(list_durations)
      df_waitings = pd.DataFrame(list_waitings)

      # Operation values DataFrame filtered
      df_op_values_f = df_op_values[df_op_values.index.isin(list_op_start)]
      df_op_values_f['duration'] = list_op_dur
      df_op_values_f['duration_port'] = df_durations['port'].tolist()
      df_op_values_f['duration_site'] = df_durations['site'].tolist()
      df_op_values_f['transit'] = df_durations['transit'].tolist()
      df_op_values_f['mobilization'] = df_durations['mobilization'].tolist()
      df_op_values_f['waiting_port'] = df_waitings['port'].tolist()
      df_op_values_f['waiting_site'] = df_waitings['site'].tolist()

      df_op_values.update(df_op_values_f)

      if len(ts_analyse) == self.startability.shape[0] and MAX_WAIT == 0:
        # Montecarlo percentage < 100%
        # Get the waiting for weather for this operation (same logic that was
        # used for activity waiting time but now we have only one "activity"
        # which is the complete operation)
        df_waiting_to_start = waiting.waiting_time(['operation'],
                                                   df_op_startability,
                                                   site_name)
        df_op_values['waiting_start'] = df_waiting_to_start['operation']

        expand_cols = ['duration',
                       'duration_port',
                       'duration_site',
                       'transit',
                       'mobilization',
                       'waiting_port',
                       'waiting_site']
        df_dur_aux = df_op_values.filter(items=expand_cols)
        # Iterate over columns and replace 0 with nan if the column is not
        # always 0
        for col in range(0, df_dur_aux.shape[1]):
          if df_dur_aux.iloc[:, col].sum() > 0:
            df_dur_aux.iloc[:, col][df_dur_aux.iloc[:, col] == 0] = np.nan
        df_dur_aux = df_dur_aux.fillna(method='bfill')
        df_op_values[expand_cols] = df_dur_aux[expand_cols]

      # Iterate over columns and replace 0 with nan if the operation net_duration is > remaining TS
      for ts in reversed(df_op_values.index):
        if (self.startability.shape[0] - ts) < self.duration_net:
          df_op_values.loc[ts, :] = np.nan
        else:
          break

      # Update operation duration: Sum waiting to start to the overall duration
      total_duration = df_op_values['duration'] + df_op_values['waiting_start']
      df_op_values['duration'] = total_duration

      # if any duration is still 0.0, then the operation cannot be preformed
      # -> duration total = 1000000
      impossible_ts = df_op_values['duration'] == 0
      if PRINT_LOGS is True:
        if impossible_ts.sum() > 0:
          print('There is %d time steps where this operation cannot start' % impossible_ts.sum())

      df_op_values.loc[impossible_ts, 'duration'] = np.nan

      return df_op_values

  class Dates():
    """
    Dates:
    ------
    This class stands for the dates of an operation

    Attributes:
    ----------
    * start = Date and time of when the operation may start (precedent
    operations finished) - datetime
    * may_start = When the operation precedences are complete - datetime
    * may_start_delta = When the operation precedences are complete - datetime.timedelta
    * start = Start date of the operation - datetime
    * end = End date of the operation - datetime
    """
    def __init__(self):
      self.may_start = None
      self.may_start_delta = None
      self.start = None
      self.end = None

  class Fails():
    """
    Fails:
    ------
    This class stands for the systems failures that this operation is repairing

    Attributes:
    ----------
    * components = ID of the failed components/system - list of strings
    * date = Failure date - datetime
    * parts = List of the replacing parts - list of strings
    * parts_cost = List of the replacing parts cost - list of floats
    """
    def __init__(self):
      self.components = None
      self.date = None
      self.parts = None
      self.parts_cost = None

  def __init__(self,
               operation_id: str,
               operation_name: str,
               operation_description: str,
               objects: list = None,
               cables: list = None,
               multiple_operations: bool = False,
               maystart_deltas: list = None):

    self.id = operation_id
    self.name = operation_name
    self.description = operation_description

    # Check if at least one of objects or cables were defined
    # if objects is None and cables is None:
    #   _e = 'At least one of \"objects\" or \"cables\" must be defined'
    #   raise AssertionError(_e)
    self.objects = None
    if objects is not None:
      if isinstance(objects, list) is True:
        if len(objects) > 0:
          self.objects = objects
        else:
          self.objects = None
      else:
        # objects is not a list, it is a string
        self.objects = [objects]    # creates a list with a single element
    self.cables = None
    if cables is not None:
      if isinstance(cables, list) is True:
        if len(cables) > 0:
          self.cables = cables
        else:
          self.cables = None
      else:
        # cables is not a list, it is a string
        self.cables = [cables]    # creates a list with a single element

    self.multiple_operations = multiple_operations
    self.maystart_deltas = maystart_deltas

    self.activities_table = None  # DataFrame with all possible activities
    self.activities = []          # List of activities id (got from best comb)

    self.requirements = {}
    self.methods = {}

    self.site = None

    self.feasible_funcs = None        # Feasible functions that must be run
    self.feasible_solutions = None    # Feasible solutions for this operation
    self.combinations = []            # List of matchable combinations

    self.vec = None                   # VEC ID
    self.terminal = None              # Terminal ID
    self.vessels = []                 # List of vessels IDs
    self.equipment = []               # List of equipment IDs
    self.durations = {}               # Dictionary of durations
    self.waitings = {}                # Dictionary of waitings

    self.consumption = None           # Daily consumption [ton/day]

    self.costs = {}                   # Operation costs per period

    self.dates = self.Dates()
    self.fails = self.Fails()

    self.downtime = None

    self.logistic_solution = {}

    # Run initialization functions
    self.get_activities_table()
    self.update_requirements()

  # Functions related with activities
  def get_activities_table(
      self,
      activities: pd.DataFrame = None
  ):
    """
    get_activities_table:
    =====================
    For a given Operation ID and a set of activities (not required), returns
    a pandas DataFrame with the possible activities for this operation

    Optional inputs:
    ----------------
    * activities = User input activities - pandas DataFrame
    """
    # If no activities is specified, Activities from Catalogue are considered
    try:
      activities.head()
      # activities was introduced as a pandas DataFrame
      df_activities = activities
    except AttributeError:
      # Get activities Catalogue
      try:
        df_activities = get_catalog_activities()
      except ConnectionError:
        path_catalogue_activities = os.path.join(os.getcwd(), 'catalogues', 'Activities.csv')
        df_activities = pd.read_csv(path_catalogue_activities, sep=',')
      df_activities.dropna(how='all', inplace=True)

    # Check which activities are related to the Operarion ID
    acts_w_opid = [row['op_id'].lower() in self.id.lower()
                   for idx, row in df_activities.iterrows()]
    df_activities = df_activities[acts_w_opid]
    df_activities.reset_index(drop=True, inplace=True)

    self.activities_table = df_activities

  def define_activity_sequence(
      self,
      max_components_deck: int
  ) -> list:
    """
    ### DOCUMENTATION MISSING
    Returns:
    --------
    list_activity_seq = List of activities sequence IDs = list of string
    """
    # Sequence of activities
    list_activity_seq = []
    # First activity is the first activity in the precedents table
    act_idx = self.activities_table.index[0]
    present_activity = self.activities_table.at[0, 'id_name']

    # Number of components in the operation
    if self.objects is not None:
      # For objects it is considered a list with a single element and that
      # element represents the number of objects to install in this operation
      tot_objs = len(self.objects)
    elif self.cables is not None:
      tot_cables = len(self.cables)
      # TODO: I think this next if can be deleted
      if 'installation' in self.name:
        # For cable installation it is considered a list with more than one
        # element where each element represents the number of routes of each
        # cable
        num_cables_routes = []
        for cbl in self.cables:
          num_cables_routes.append(len(cbl.length))
    else:
      tot_comp = 0
      tot_cables = 0

    # Components location initialization
    if self.objects is not None:
      obj_quay = tot_objs         # Number of objects in the quay
      obj_deck = 0                # Number of objects on deck
    elif self.cables is not None:
      # ### This is wrong
      cbl_quay = 0                # Number of cables in the quay
      cbl_deck = tot_cables       # Number of cables on deck

    # Initialize total components install/inspected
    if self.objects is not None:
      obj_inst = 0                  # Number of objects installed
      obj_insp = 0                  # Number of objects inspected
    elif self.cables is not None:
      cbl_inst = 0                  # Number of cables installed
      routes_inst = 0               # Cable routes installed
      cbl_insp = 0                  # Number of cables inspected

    # Burial tool deployed (only for cable burial operations)
    burial_tool_dep = False

    cycle_ctr = 0       # cycle control
    while True:
      # Loop through all activities
      str_act = self.activities_table.at[act_idx, 'name']
      next_acts = self.activities_table.at[act_idx, 'next_act']
      next_acts = str(next_acts)

      # Add components to deck and to quay (add or remove)
      # if = -1 -> remove
      add_comp_deck = self.activities_table.at[act_idx, 'comp_deck']
      add_comp_quay = self.activities_table.at[act_idx, 'comp_quay']
      # ### IDEA 1 ###

      if next_acts == 'nan' or next_acts == '' or next_acts == 'None':
        # Last activity
        next_activity = 'finished'

      elif next_acts.count(';') == 0:
        # Only one way to go -> no condition
        next_activity = self.activities_table.at[act_idx, 'next_act']

        # Update cable burial tool status
        if 'burial' in str_act.lower() and 'tool' in str_act.lower():
          if 'remove' in str_act.lower():
            burial_tool_dep = False
          elif 'deploy' in str_act.lower():
            burial_tool_dep = True
      elif next_acts.count(';') > 0:
        # More than one possible next activity, meaning this "activity" is
        # not an activity but a condition
        possible_activities = self.activities_table.at[act_idx, 'next_act']
        possible_activities = possible_activities.split(';')
        possible_activities = [act.replace(' ', '')
                               for act in possible_activities]

        # Get which condition
        condition = re.search('_(.*)', str_act)
        condition = condition.group(1)
        try:
          conditions_split = condition.split(':')
          # If it can be splited, this condition is related to the operation
          # cond1 can be methods or requirements, for example
          cond1 = conditions_split[0]
          cond1 = cond1.lower()
          # cond2 refers to the methods or requirements keys
          cond2 = conditions_split[1]
          cond2 = cond2.lower()
          if cond1 == 'methods':
            option = self.methods[cond2]
          elif cond1 == 'requirements':
            option = str(self.requirements[cond2])
          elif cond1 == 'device':
            if 'type' in cond2:
              option = self.objects[0].type   # ### CHECK THIS - [0]
            elif 'ei' in cond2:
              option = 'dry'                  # ### ALWAYS ASSUMING DRY ELECT CONNECTION
            else:
              _e = 'Condition %s:%s not coded' % (cond1, cond2)
              raise AssertionError(_e)
          elif cond1 == 'object':
            option = self.objects[obj_inst].type
          elif cond1 == 'anchor':
            option = self.objects[obj_inst].type
          elif cond1 == 'cable_route':
            if 'burial' in cond2 and 'depth' in cond2:
              depth = float(self.cables[cbl_inst].burial_depth[routes_inst])
              if depth <= 0:
                option = 'zero'
              else:
                option = 'higher than zero'
              del depth
            elif 'split' in cond2 and 'pipe' in cond2:
              split = bool(self.cables[cbl_inst].split_pipes[routes_inst])
              if split is True:
                option = 'true'
              else:
                option = 'false'
              del split
            elif 'buoyanc' in cond2:
              buoy = bool(self.cables[cbl_inst].buoyancy_modules[routes_inst])
              if buoy is True:
                option = 'true'
              else:
                option = 'false'
              del buoy
          elif cond1 == 'operation' and cond2 == 'description':
            option = self.description
          else:
            _e = 'For operation %s, condition ' % self.name
            _e = _e + '%s unrecognized' % condition
            raise AssertionError(_e)

        except IndexError:
          if condition.lower() == 'deck full':
            # Check if the deck is full
            if obj_deck == max_components_deck:
              # The deck is full
              option = 'true'
            else:
              option = 'false'
          elif condition.lower() == 'quay empty':
            # Check if quay is empty
            try:
              if obj_quay <= 0:
                # Quay is empty
                option = 'true'
              else:
                option = 'false'
            except NameError:
              if cbl_quay <= 0:
                # Quay is empty
                option = 'true'
              else:
                option = 'false'
          elif (condition.lower() == 'deck empty'):
            # Check if the deck is empty
            if obj_deck <= 0:
              # The deck is empty
              option = 'true'
            else:
              option = 'false'
          elif (condition.lower() == 'vessel empty'):
            # Check if vessel is empty
            if cbl_deck <= 0:
              # The deck is empty
              option = 'true'
            else:
              option = 'false'
          elif ('end' in condition.lower() and
                'maintenance' in condition.lower()):
            # Check if it is the last object to maintain
            if self.objects is not None:
              if obj_insp >= len(self.objects):
                # Last maintenance
                  option = 'true'
              else:
                option = 'false'
            elif self.cables is not None:
              if cbl_insp >= len(self.cables):
                # Last maintenance
                  option = 'true'
              else:
                option = 'false'
          elif ('simultaneous' in condition.lower() and
                'install' in condition.lower()):
            # ### MUST BE IMPROVED. CHECK MOORING BY MOORING AND NOT ALL OF THE
            # Check if the mooring and the anchor are to be installed at the
            # same time
            # Check if foundations was installed
            mooring_pre = [True
                           if 'pre' in obj.type and 'install' in obj.type
                           else False
                           for obj in self.objects]
            if any(mooring_pre) is True:
              # Morings are pre-install type, it means they are to be installed
              # seperetly
              option = 'false'
            else:
              option = 'true'
            # INSTALLATION OPERATION
          elif 'burial tool deployed' in condition.lower():
            # Check burial tool is deployed
            if burial_tool_dep is True:
              option = 'true'
            else:
              option = 'false'
          elif 'end' in condition.lower() and 'cable' in condition.lower():
            # Check if this is the last route
            if routes_inst >= len(self.cables[cbl_inst].length):
              # Last route
              option = 'true'
              routes_inst = 0
              cbl_inst += 1
              cbl_deck -= 1
            else:
              option = 'false'
          elif 'upstream component installed' in condition.lower():
            # ### CHECK THIS. FOR NOW, ASSUME FALSE
            option = 'false'
          elif 'downstream component installed' in condition.lower():
            # ### CHECK THIS. FOR NOW, ASSUME FALSE
            option = 'false'
          else:
            raise AssertionError('Condition unrecognized.')

        try:
          option = option.lower()
        except NameError:
          pass
        # Check which options can be found for this condition
        possible_options = self.activities_table.at[act_idx, 'options']
        # Convert string to list
        possible_options = possible_options.split(';')

        option_found = False
        for i in range(0, len(possible_options)):
          if option in possible_options[i].lower():
            option_found = True
            break
        if option_found is False:
          _e = 'Option %s not found in %s' % (option, possible_options)
          raise AssertionError(_e)
        next_activity = possible_activities[i]
        del option

      # Only add activities, not conditions
      if 'cond_' not in str_act and 'dynam_' not in str_act:
        list_activity_seq.append(present_activity)
      present_activity = next_activity
      try:
        next_acts_position = self.activities_table['id_name'] == next_activity
        act_idx = self.activities_table[next_acts_position].index[0]
      except IndexError:
        if next_activity == 'finished':
          # Next activity is = 'finished' and there is no id = finished
          break
        else:
          raise AssertionError('Activity %s unrecognized' % next_activity)
      del next_activity

      if 'installation' in self.name.lower():
        # Update components on deck and on quay
        if self.objects is not None:
          obj_deck += add_comp_deck
          obj_quay += add_comp_quay
        elif self.cables is not None:
          cbl_quay += add_comp_quay

        if add_comp_deck == -1:
          if self.objects is not None:
            obj_inst += 1
          elif self.cables is not None:
            routes_inst += 1
      elif 'maintenance' in self.name.lower():
        # Update components maintained
        if self.objects is not None:
          obj_insp += abs(add_comp_deck)
        elif self.cables is not None:
          cbl_insp += abs(add_comp_deck)

      if cycle_ctr > 10000:
        raise AssertionError('Infinite loop. More than 10000 activities')
      cycle_ctr += 1

    return list_activity_seq

  # Functions related with methods and requirements
  def update_requirements(self):
    """
    update_requirements
    ===================
    This functions updates the Operation requirements considering the
    Operation.objects and Operation.cables

    This functions reads a list of objects and/or cables and updates the
    requirements of a given operation
    """
    objects = self.objects
    cables = self.cables

    # Requirements are different from installation and maintenance operations

    # Installation operations
    if 'installation' in self.name.lower():
      if 'burial' not in self.name.lower():
        # Check if at least objects or cables were defined
        if objects is None and cables is None:
          _e = 'For operation %s, at least one of \"objects\" or ' % self.name
          _e = _e + '\"cables\" must be defined'
          raise AssertionError(_e)

      # Area requirement
      if objects is not None:
        base_areas = [float(obj.base_area)
                      for obj in objects
                      if hasattr(obj, 'base_area')]
        try:
          base_area_max = max(base_areas)
        except ValueError:
          # No object has base_area attributes
          base_area_max = 0
        self.requirements['area'] = base_area_max

      # Lift requirement
      if objects is not None:
        masses = [float(obj.drymass)
                  for obj in objects
                  if hasattr(obj, 'drymass')]
        try:
          mass_max = max(masses)
        except ValueError:
          # No object has drymass attribute
          mass_max = 0
        self.requirements['lift'] = mass_max / 1000     # in ton
        # self.requirements['dwat'] = mass_max / 1000     # in ton

      # Deck and terminal minimum strength requirement
      # if self.requirements['terminal_load'] is True:
      if objects is not None:
        pressures = [float(obj.drymass) / float(obj.base_area)
                     for obj in objects
                     if hasattr(obj, 'drymass') and hasattr(obj, 'base_area')]
        try:
          pressure_max = max(pressures)
        except ValueError:
          # No object has drymass and base_area attributes
          pressure_max = 0
        self.requirements['strength'] = pressure_max / 1000   # in ton/m^2

      # Depth requirement
      depth_max_objs = 0
      depth_max_cbls = 0
      depth_min_objs = np.inf
      depth_min_cbls = np.inf
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           'float' not in obj.type.lower())]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      try:
        depths_cbls = [depth
                       for cable in cables
                       for depth in cable.bathymetry
                       if depth is not None and 'umbilical' not in cable.name.lower()]
        try:
          depth_max_cbls = max(depths_cbls)
          depth_min_cbls = min(depths_cbls)
          # TODO: VERIFY
          depth_max_cbls = 0
          depth_min_cbls = 1000
        except ValueError:
          pass
      except TypeError:
        # cables not defined
        pass
      if objects is not None or cables is not None:
        try:
          depth_max = max(depth_max_objs, depth_max_cbls)
          self.requirements['depth_max'] = depth_max
          depth_min = min(depth_min_objs, depth_min_cbls)
          self.requirements['depth_min'] = depth_min
        except ValueError:
          # No object has bathymetry attribute
          pass

      # Maximum piling depth requirement
      depth_piling_max = 0
      if objects is not None:
        burial_objs = [float(obj.burial_depth)
                       for obj in objects
                       if hasattr(obj, 'burial_depth')]
        try:
          depth_piling_max = max(burial_objs)
          self.requirements['piling_max'] = burial_objs
        except ValueError:
          # No object has burial_depth attribute
          pass

      # Cable burial depth requirements
      burial_max = 0
      if cables is not None:
        for cable in cables:
          burials = [max(cable.burial_depth) for cable in cables]
        burial_max = max(burials)
        self.requirements['cable_depth'] = burial_max

      # Diameter
      if objects is not None:
        try:
          diameters = [float(obj.diameter) for obj in objects]
          # Minimum diameter
          diam_min = min(diameters)
          self.requirements['object_diameter_min'] = diam_min
          # Maximum diameter
          diam_max = max(diameters)
          self.requirements['object_diameter_max'] = diam_max
        except AttributeError:
          # No object has diameter attribute
          pass
      if cables is not None:
        diameters = [float(cable.diameter) for cable in cables]
        # Minimum diameter
        diam_min = min(diameters)
        self.requirements['cable_diameter_min'] = diam_min
        # Maximum diameter
        diam_max = max(diameters)
        self.requirements['cable_diameter_max'] = diam_max

      # Minimum bending radius
      if cables is not None:
        mbrs = [float(cable.mbr) for cable in cables]
        mbr_max = max(mbrs)
        self.requirements['mbr'] = mbr_max

      # Turntable capacity
      if cables is not None:
        # Calculates cables overall volume
        # diameter in mm, length in m
        sections = [float(mt.pi * ((cable.diameter / 1000) / 2) ** 2)
                    for cable in cables]
        lengths = [float(sum(cable.length)) for cable in cables]

        cables_vols = [sections[idx] * lengths[idx]
                       for idx, _ in enumerate(cables)]   # in m^3
        vol_max = max(cables_vols)
        self.requirements['turn_capacity'] = vol_max

      # Turntable storage
      if cables is not None:
        # Calculates cables overall weight
        # drymass in kg/m, length in m
        cables_weight = [float(cable.drymass / 1000) for cable in cables]     # in ton
        weight_max = max(cables_weight)
        self.requirements['turn_storage'] = weight_max

      # Largest object - for wet towing purposes
      if objects is not None:
        # If objects are beeing towed in water, the largest object dimensions
        # must be stored
        i_max = 0
        volumes = []
        for obj in objects:
          if (hasattr(obj, 'base_area') and hasattr(obj, 'length')):
            volumes.append(obj.base_area * obj.length)
        if len(volumes) > 0:
          vol_max = max(volumes)
          i_max = [i for i in range(0, len(volumes)) if volumes[i] == vol_max]
          i_max = i_max[0]

        self.requirements['largest_object'] = objects[i_max]

      # Terminal draught requirements
      if self.objects is not None:
        drafts = [float(obj.draft)
                  for obj in objects
                  if hasattr(obj, 'draft')]
        try:
          draft_max = max(drafts)
          self.requirements['terminal_draught'] = draft_max
        except ValueError:
          # No object has draft attribute
          pass



    # Preventive maintenance operations
    elif ('preventive' in self.name.lower() and
          'maintenance' in self.name.lower()):
      # Depth requirement
      depth_max_objs = 0
      depth_max_cbls = 0
      depth_min_objs = np.inf
      depth_min_cbls = np.inf
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           ('device' in obj.name.lower() and
                            'float' not in obj.type.lower()))]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      try:
        depths_cbls = [depth
                       for cable in cables
                       for depth in cable.bathymetry
                       if depth is not None and 'umbilical' not in cable.name.lower()]
        try:
          depth_max_cbls = max(depths_cbls)
          depth_min_cbls = min(depths_cbls)
          # TODO: VERIFY
          depth_max_cbls = 0
          depth_min_cbls = 1000
        except ValueError:
          pass
      except TypeError:
        # cables not defined
        pass
      if objects is not None or cables is not None:
        try:
          depth_max = max(depth_max_objs, depth_max_cbls)
          self.requirements['depth_max'] = depth_max
          depth_min = min(depth_min_objs, depth_min_cbls)
          self.requirements['depth_min'] = depth_min
        except ValueError:
          # No object has bathymetry attribute
          pass

      # Passenger requirement
      # TODO: THIS HARDCODED BUT IT SHOULDN'T. GET this from op maintenane catalogue
      self.requirements['passengers'] = 2

    # Corrective maintenance operations
    elif ('corrective' in self.name.lower() and
          'maintenance' in self.name.lower()):
      # Depth requirement
      depth_max_objs = 0
      depth_max_cbls = 0
      depth_min_objs = np.inf
      depth_min_cbls = np.inf
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           ('device' in obj.name.lower() and
                            'float' not in obj.type.lower()))]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      try:
        depths_cbls = [depth
                       for cable in cables
                       for depth in cable.bathymetry
                       if depth is not None and 'umbilical' not in cable.name.lower()]
        try:
          depth_max_cbls = max(depths_cbls)
          depth_min_cbls = min(depths_cbls)
          # TODO: VERIFY
          depth_max_cbls = 0
          depth_min_cbls = 1000
        except ValueError:
          pass
      except TypeError:
        # cables not defined
        pass
      if objects is not None or cables is not None:
        try:
          depth_max = max(depth_max_objs, depth_max_cbls)
          self.requirements['depth_max'] = depth_max
          depth_min = min(depth_min_objs, depth_min_cbls)
          self.requirements['depth_min'] = depth_min
        except ValueError:
          # No object has bathymetry attribute
          pass

      # Passenger requirement
      # ### THIS HARDCODED BUT IT SHOULDN'T
      self.requirements['passengers'] = 6

      # Area requirement - It only matters if we need to transport something
      if (('device' in self.description.lower() and
           ('retrieval' in self.description.lower() or
            'redeployment' in self.description.lower())) or
          'replacement' in self.description.lower()):
        if objects is not None:
          base_areas = [float(obj.base_area)
                        for obj in objects
                        if hasattr(obj, 'base_area')]
          try:
            base_area_max = max(base_areas)
            self.requirements['area'] = base_area_max
          except ValueError:
            # No object has base_area attributes
            pass

      # Lift requirement
      if objects is not None:
        masses = [float(obj.drymass)
                  for obj in objects
                  if hasattr(obj, 'drymass')]
        try:
          mass_max = max(masses)
          self.requirements['lift'] = mass_max / 1000     # in ton
          # self.requirements['dwat'] = mass_max / 1000     # in ton
        except ValueError:
          # No object has drymass attribute
          pass

      # Deck and terminal minimum strength requirement - It only matters if we
      # need to transport something
      if (('device' in self.description.lower() and
           ('retrieval' in self.description.lower() or
            'redeployment' in self.description.lower())) or
          'replacement' in self.description.lower()):
        if objects is not None:
          pressures = [float(obj.drymass) / float(obj.base_area)
                       for obj in objects
                       if (hasattr(obj, 'drymass') and
                           hasattr(obj, 'base_area'))]
          try:
            pressure_max = max(pressures)
            self.requirements['strength'] = pressure_max / 1000   # in ton/m^2
          except ValueError:
            # No object has drymass and base_area attributes
            pass

      # Depth requirement
      depth_max_objs = 0
      depth_max_cbls = 0
      depth_min_objs = np.inf
      depth_min_cbls = np.inf
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           'float' not in obj.type.lower())]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      try:
        depths_cbls = [depth
                       for cable in cables
                       for depth in cable.bathymetry
                       if depth is not None and 'umbilical' not in cable.name.lower()]
        try:
          depth_max_cbls = max(depths_cbls)
          depth_min_cbls = min(depths_cbls)
          # TODO: VERIFY
          depth_max_cbls = 0
          depth_min_cbls = 1000
        except ValueError:
          pass
      except TypeError:
        # cables not defined
        pass
      if objects is not None or cables is not None:
        try:
          depth_max = max(depth_max_objs, depth_max_cbls)
          self.requirements['depth_max'] = depth_max
          depth_min = min(depth_min_objs, depth_min_cbls)
          self.requirements['depth_min'] = depth_min
        except ValueError:
          # No object has bathymetry attribute
          pass

      # Cable burial depth requirements
      burial_max = 0
      if cables is not None:
        burials = [max(cable.burial_depth) for cable in cables]
        burial_max = max(burials)
        self.requirements['cable_depth'] = burial_max

      # Diameter
      if cables is not None:
        diameters = [float(cable.diameter) for cable in cables]
        # Minimum diameter
        diam_min = min(diameters)
        self.requirements['cable_diameter_min'] = diam_min
        # Maximum diameter
        diam_max = max(diameters)
        self.requirements['cable_diameter_max'] = diam_max

      # Minimum bending radius
      if cables is not None:
        mbrs = [float(cable.mbr) for cable in cables]
        mbr_max = max(mbrs)
        self.requirements['mbr'] = mbr_max

      # Turntable capacity
      if cables is not None:
        # Calculates cables overall volume
        # diameter in mm, length in m
        sections = [float(mt.pi * ((cable.diameter / 1000) / 2) ** 2)
                    for cable in cables]
        lengths = [float(sum(cable.length)) for cable in cables]

        cables_vols = [sections[idx] * lengths[idx]
                       for idx, _ in enumerate(cables)]   # in m^3
        vol_max = max(cables_vols)
        self.requirements['turn_capacity'] = vol_max

      # Turntable storage
      if cables is not None:
        # Calculates cables overall weight
        # drymass in kg/m, length in m
        cables_weight = [float(cable.drymass / 1000) for cable in cables]   # in ton
        weight_max = max(cables_weight)
        self.requirements['turn_storage'] = weight_max

      # Largest object - for wet towing purposes
      if objects is not None:
        # If objects are beeing towed in water, the largest object dimensions
        # must be stored
        i_max = 0
        volumes = []
        for obj in objects:
          if (hasattr(obj, 'base_area') and hasattr(obj, 'length')):
            volumes.append(obj.base_area * obj.length)
        if len(volumes) > 0:
          vol_max = max(volumes)
          i_max = [i for i in range(0, len(volumes)) if volumes[i] == vol_max]
          i_max = i_max[0]

        self.requirements['largest_object'] = objects[i_max]


  def update_methods_requirements_user(
      self,
      method_load_out: str = None,
      method_transportation: str = None,
      method_piling: str = None,
      method_burial: str = None,
      method_assembly: str = None,
      method_landfall: str = None,
      requirement_rov: bool = None,
      requirement_divers: bool = None,
      requirement_prev_proj: bool = None,
      requirement_terminal_area: bool = None,
      requirement_terminal_load: bool = None,
      requirement_terminal_crane: bool = None,
      requirement_connect_to_elect: bool = None,
      port_max_dist: float = None
  ):
    """
    This functions updates the Operation for a set of user inputs related with methods and requirements.

    Args:
        method_load_out (str, optional): Load out from terminal to vessel/water strategy. Defaults to None.
        method_transportation (str, optional): Piling method if the object is a pile. Defaults to None.
        method_piling (str, optional): Burial method if the object is a cable. Defaults to None.
        method_burial (str, optional): Export cable landfall method. Defaults to None.
        method_assembly (str, optional): Assembly method (for devices only). Defaults to None.
        method_landfall (str, optional): Landfall method (for export cables only). Defaults to None.
        requirement_rov (bool, optional): If ROV is required for this operation. Defaults to None.
        requirement_divers (bool, optional): If Divers are required for this operation. Defaults to None.
        requirement_prev_proj (bool, optional): Considered terminals must already host MRE projects. Defaults to None.
        requirement_terminal_area (bool, optional): Consider terminals area. Defaults to None.
        requirement_terminal_load (bool, optional): Consider terminals maximum load. Defaults to None.
        requirement_terminal_crane (bool, optional): Consider terminals crane's capabilities. Defaults to None.
        requirement_connect_to_elect (bool, optional): Connect object to the electrical system. Defaults to None.
        port_max_dist (float, optional): Maximum distance to terminals. Defaults to None.

    Raises:
        AssertionError: If load_out method is not recognized
        AssertionError: If transportation method is not recognized
        AssertionError: If a piling method is not defined for an operation where piling is required
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
        AssertionError: [description]
    """
    opx_name = self.name.lower()
    try:
      opx_description = self.description.lower()
    except AttributeError:
      opx_description = 'UNDEFINED'

    # For operations with Transportation block (Flowchart) and maintenance with
    # transport requirements
    if ((('device' in opx_name or 'foundation' in opx_name or 'mooring' or
          'structure' in opx_name or 'collection' in opx_name) and
          'installation' in opx_name) or
        (('device' in opx_description and
           ('retrieval' in opx_description or
            'redeployment' in opx_description)) or
          'replacement' in opx_description)):
      # If this is an operation with transportation, it requires transportation
      # method and load-out method
      if method_load_out is None:
        # If load out method was not specified, it is assumed lift away
        self.methods['load_out'] = 'lift'
        _w = 'For operation %s, Load-out method assumed as ' % opx_name
        _w = _w + '\"Lift away\"'
        warnings.warn(_w)
      elif 'lift' in method_load_out.lower():
        self.methods['load_out'] = 'lift'
      elif 'float' in method_load_out.lower():
        self.methods['load_out'] = 'float'
        self.requirements['dry_dock'] = True
      elif 'skidded' in method_load_out.lower():
        self.methods['load_out'] = 'skidded'
      elif 'railer' in method_load_out.lower():
        self.methods['load_out'] = 'railer'
      else:
        _e = 'For operation %s, \"method_load_out\" not recognized' % opx_name
        raise AssertionError(_e)

      if method_transportation is None:
        # If transportation method was not specified, transport is assumed dry
        # (on deck or dry tow) if load out is not float away. Otherwise is
        # assumed wet
        # Check if method_load_out is float
        if 'float' in self.methods['load_out']:
          self.methods['transport'] = 'wet'
          _w = 'For operation %s, Transportation method assumed as ' % opx_name
          _w = _w + '\'wet\''
          warnings.warn(_w)
        else:
          self.methods['transport'] = 'dry'
          _w = 'For operation %s, Transportation method assumed as ' % opx_name
          _w = _w + '\'dry\''
          warnings.warn(_w)
      elif 'wet' in method_transportation.lower():
        self.methods['transport'] = 'wet'
        # Check if method_load_out is skidded or railer
        if ('skidded' in self.methods['load_out'] or
            'railer' in self.methods['load_out']):
          # It is required the existence of slipway in the terminal
          self.requirements['slipway'] = True
      elif 'dry' in method_transportation.lower():
        # Check if method_load_out is float
        if 'float' in self.methods['load_out']:
          _e = 'For operation %s, \"method_transportation\" cannot' % opx_name
          _e = _e + ' be \'dry\' if \"method_load_out\" is float'
          raise AssertionError(_e)
        self.methods['transport'] = 'dry'
    else:
      if method_load_out is not None:
        _w = 'For operation %s, load-out method is neglected' % opx_name
        warnings.warn(_w)
      if method_transportation is not None:
        _w = 'For operation %s, transportation method is neglected' % opx_name
        warnings.warn(_w)

    # For operations with Piling block (Flowchart)
    if method_piling is not None:
      # If piling method was specified, check if it could be specified for
      # this operation
      if 'foundation' in opx_name and 'installation' in opx_name:
        if 'pile' not in opx_description:
          _e = 'For operation %s, \"method_piling\" cannot be ' % opx_name
          _e = _e + 'defined since there is no piles to install'
          raise AssertionError(_e)
      # it must be checked if it is correct
      if 'hammer' in method_piling.lower():
        self.methods['piling'] = 'hammering'
      elif 'drill' in method_piling.lower():
        self.methods['piling'] = 'drilling'
      elif 'vibro' in method_piling.lower():
        self.methods['piling'] = 'vibro-piling'
      else:
        _e = 'For operation %s, \"method_piling\" not recognized' % opx_name
        raise AssertionError(_e)
    else:
      # If piling method was not specified, check if it should be
      if 'foundation' in opx_name and 'installation' in opx_name:
        if 'pile' in opx_description:
          _e = 'For operation %s, \"method_piling\" must be ' % opx_name
          _e = _e + 'defined'
          raise AssertionError(_e)

    # For cable laying related operations
    if method_burial is not None:
      # If burial method was specified, check if it could be specified for
      # this operation
      if ('trenching' in opx_name or
          ('cable' in opx_name and 'installation' in opx_name and
           ('simultaneous' in opx_description or
            'umbilical' in opx_description)) or
          ('post' in opx_name and 'lay' in opx_name and 'burial' in opx_name)):
        # OK, it can be defined.
        pass
      elif ('corrective' in opx_name and 'maintenance' in opx_name and
            'cable' in opx_description):
        # OK, it can be defined.
        pass
      else:
        _e = 'For operation %s, \"method_burial\" cannot be ' % opx_name
        _e = _e + 'defined since there is no cables to bury'
        raise AssertionError(_e)
      # Check if burial method is correct
      if 'plough' in method_burial.lower():
        # ### CHECK IF A PLOUGH COULD BE USED??? ### MAYBE IT CAN ONLY BE USED FOR SIMULTANEOUS
        self.methods['burial'] = 'ploughing'
      elif 'jet' in method_burial.lower():
        self.methods['burial'] = 'jetting'
      elif 'cut' in method_burial.lower():
        self.methods['burial'] = 'cutting'
      else:
        self.methods['burial'] = 'none'

    # For Device installation operation
    if method_assembly is not None:
      # If assembly method was specified, check if it could be specified for
      # this operation
      if 'device' in opx_name and 'installation' in opx_name:
        # Assembly can be defined for this operation
        if (method_assembly != 'pre' or method_assembly != 'post'):
          _e = 'For operation %s, \"method_assembly\" must be ' % opx_name
          _e = _e + '\"pre\" or \"pre\"'
          raise AssertionError(_e)
        self.methods['assembly'] = method_assembly
      else:
        _e = 'For operation %s, \"method_assembly\" cannot be ' % opx_name
        _e = _e + 'defined'
        raise AssertionError(_e)
    else:
      # Assembly method was not defined
      if 'device' in opx_name and 'installation' in opx_name:
        # Default assembly method
        self.methods['assembly'] = 'pre'
        _w = 'For operation %s, method_assembly assumed as \'pre\'' % self.name
        warnings.warn(_w)

    # For export cable installation operation
    if method_landfall is not None:
      # If landfall method was specified, check if it could be specified for
      # this operation
      if ('cable' in opx_name and 'installation' in opx_name and
          self.cables is not None):
        cable_types = [cable.type for cable in self.cables]
        if 'export' in cable_types:
          # It can be defined
          self.methods['landfall'] = str(method_landfall)
      else:
        _e = 'For operation %s, \"method_landfall\" cannot be ' % opx_name
        _e = _e + 'defined.'
        raise AssertionError(_e)
    else:
      # If landfall method was not specified, check if it should be
      if ('cable' in opx_name and 'installation' in opx_name and
          self.cables is not None):
        cable_types = [cable.type for cable in self.cables]
        if 'export' in cable_types:
          # It must be defined
          _e = 'For operation %s, \"method_landfall\" must be ' % opx_name
          _e = _e + 'defined.'
          raise AssertionError(_e)

    # ROVs
    if requirement_rov is None:
      # ROV requirement was not specified. Check if ROV is necessary.
      rov_class = self._rov_class_req(opx_name, opx_description)
      self.requirements['rov'] = rov_class
    elif requirement_rov:
      # Check which class
      rov_class = self._rov_class_req(opx_name, opx_description)
      if rov_class is None:
        self.requirements['rov'] = 'inspection'
      else:
        self.requirements['rov'] = rov_class
    elif not requirement_rov:
      self.requirements['rov'] = None
    else:
      _e = 'For operation %s, \"requirement_rov\" must be True or ' % opx_name
      _e = _e + 'False'
      raise AssertionError(_e)

    # Divers
    if requirement_divers is None:
      # Divers requirement was not specified. It is assumed unnecessary
      self.requirements['divers'] = False
      warnings.warn('For operation %s, Divers are neglected' % opx_name)
    else:
      self.requirements['divers'] = requirement_divers
      # Assumes that no ROV is needed, when divers are requested
      if requirement_divers is True:
        self.requirements['rov'] = None

    # Previous projects
    if requirement_prev_proj is None:
      # Previous projects requirement was not specified. It is assumed
      # unnecessary
      self.requirements['prev_proj'] = False
      _w = 'For operation %s, previous projects considered ' % opx_name
      _w = _w + 'unnecessary'
      warnings.warn(_w)
    else:
      self.requirements['prev_proj'] = requirement_prev_proj

    # Terminal area
    if requirement_terminal_area is None:
      # Terminal area requirement was not specified. It is assumed unnecessary
      self.requirements['terminal_area'] = False
      _w = 'For operation %s, terminal area considered ' % opx_name
      _w = _w + 'unnecessary'
      warnings.warn(_w)
    else:
      self.requirements['terminal_area'] = requirement_terminal_area

    # Terminal load
    if requirement_terminal_load is None:
      # Terminal load requirement was not specified. It is assumed unnecessary
      # And, therefore, the "strength" requirement is False
      self.requirements['terminal_load'] = False
      _w = 'For operation %s, terminal strength considered ' % opx_name
      _w = _w + 'unnecessary'
      warnings.warn(_w)
    else:
      self.requirements['terminal_load'] = requirement_terminal_load

    # Terminal cranes capabilities
    if requirement_terminal_crane is None:
      # Terminal crane requirement was not specified. It is assumed unnecessary
      # And, therefore, the "crane" requirement is False
      self.requirements['terminal_crane'] = False
      _w = 'For operation %s, terminal crane capabilities ' % opx_name
      _w = _w + 'considered unnecessary'
      warnings.warn(_w)
    else:
      self.requirements['terminal_crane'] = requirement_terminal_load

    # Dynamic Positioning
    if ('topside' in self.description.lower() and
        'inspection' in self.description.lower() and
        'preventive' in self.name.lower() and
        'maintenance' in self.name.lower()):
      self.requirements['dp'] = 1
    else:
      self.requirements['dp'] = 2

    if requirement_connect_to_elect is None:
      # Connect to electrical system requirement was not specified. Check if it
      # is necessary
      if 'device' in opx_name and 'installation' in opx_name:
        # It should be defined. It is assumed as False
        self.requirements['connect_to_elect'] = False
        _w = 'For operation %s, connect to electrical system ' % opx_name
        _w = _w + 'considered False'
        warnings.warn(_w)
    else:
      # Check if it could be defined
      if (('device' in opx_name and 'installation' in opx_name) or
          ('collection' in opx_name and 'point' in opx_name and
           'installation' in opx_name)):
        self.requirements['connect_to_elect'] = requirement_connect_to_elect
      else:
        _e = 'For operation %s, connect to electrical system ' % opx_name
        _e = _e + 'cannot be defined'
        raise AssertionError(_e)

    # Port maximum distance to farm
    if port_max_dist is None:
      self.requirements['port_max_dist'] = 1000     # 1000 km - default value
    else:
      self.requirements['port_max_dist'] = port_max_dist

  # Functions related with feasibility
  def feasibility_selector(self):
    """
    feasibility_selector
    ---------------------
    This functions checks which feasibility functions must be called for a
    given operation
    """
    # Predefined feasibility functions
    self.feasible_funcs = [
        'vec',              # always called
        'terminal',         # always called
        'equip_rov',
        'equip_divers',
        'equip_burial',
        'equip_piling',
        'vessel'            # always called
    ]

    opx_name = self.name.lower()
    opx_desc = self.description.lower()

    if self.requirements['rov'] is None:
      self.feasible_funcs.remove('equip_rov')
    if self.requirements['divers'] is False:
      self.feasible_funcs.remove('equip_divers')

    if 'installation' in opx_name:
      if 'foundation' in opx_name:
        self.feasible_funcs.remove('equip_burial')
        if 'pile' not in opx_desc:
          self.feasible_funcs.remove('equip_piling')
      elif 'support' in opx_name or 'structure' in opx_name:
        self.feasible_funcs.remove('equip_burial')
        if 'pile' not in opx_desc:
          self.feasible_funcs.remove('equip_piling')
      elif 'cable' in opx_name:
        self.feasible_funcs.remove('equip_piling')
        if 'simultaneous' not in opx_desc:
          self.feasible_funcs.remove('equip_burial')
        if self.methods['burial'].lower() == 'none':
          self.feasible_funcs.remove('equip_burial')
      else:
        self.feasible_funcs.remove('equip_piling')
        self.feasible_funcs.remove('equip_burial')

    elif 'post' in opx_name and 'burial' in opx_name:
      self.feasible_funcs.remove('equip_piling')

    elif 'corrective' in opx_name and 'maintenance' in opx_name:
      # if 'cable' in opx_desc and 'repair' in opx_desc:
      #   self.feasible_funcs.remove('equip_piling')
      # elif 'cable' in opx_desc and 'replacement' in opx_desc:
      #   self.feasible_funcs.remove('equip_piling')
      # else:
      self.feasible_funcs.remove('equip_piling')
      self.feasible_funcs.remove('equip_burial')

    else:
      self.feasible_funcs.remove('equip_piling')
      self.feasible_funcs.remove('equip_burial')

  def get_feasible_solutions(
      self,
      quantile_vessels: float,
      safety_factor: float
  ):
    sol_vecs = None
    sol_terminals = None
    sol_rovs = None
    sol_divers = None
    sol_burial = None
    sol_protect = None
    sol_piling = None
    sol_vessels = None

    # Check which feasible functions must be called
    self.feasibility_selector()

    # Call feasibility functions that must be called according to
    # self.feasible_funcs
    if 'vec' in self.feasible_funcs:
      sol_vecs = feas_funcs.vecs(self)
    if 'terminal' in self.feasible_funcs:
      sol_terminals = feas_funcs.terminals(self, safety_factor)
    if 'equip_rov' in self.feasible_funcs:
      sol_rovs = feas_funcs.rov(self, safety_factor)
    if 'equip_divers' in self.feasible_funcs:
      sol_divers = feas_funcs.divers(self, safety_factor)
    if 'equip_burial' in self.feasible_funcs:
      sol_burial = feas_funcs.burial(self, safety_factor)
    if 'equip_piling' in self.feasible_funcs:
      sol_piling = feas_funcs.piling(self, safety_factor)
    if 'vessel' in self.feasible_funcs:
      if quantile_vessels == 0.25:
        percent = 'p25'
      elif quantile_vessels == 0.5:
        percent = 'p50'
      elif quantile_vessels == 0.75:
        percent = 'p75'

      sol_vessels = feas_funcs.vessels(
          opx=self,
          vecs=sol_vecs,
          sf=safety_factor,
          percentile=percent
      )

    self.feasible_solutions = self.Feasible(
        vectable=sol_vecs,
        terminaltable=sol_terminals,
        rovtable=sol_rovs,
        divertable=sol_divers,
        burialtable=sol_burial,
        protecttable=sol_protect,
        pilingtable=sol_piling,
        vess_sol_main=sol_vessels['main'],
        vess_sol_tow=sol_vessels['tow'],
        vess_sol_support=sol_vessels['support']
    )

  # Functions related with matchability
  def match_feasible_solutions(self):
    """
    matching_functions:
    ### REVER DESCR
    Este codigo analisa as soluções de barcos faziveis e cria solucoes
    possiveis em função dos vecs.

    1st - Create a pandas DataFrame with all information related to that
    combination (temrinal, vec, op_description, op_type, vessels, etc.)

    2nd - Create Combination() types for each possible combination and store
    them in a list
    """
    # Variables definition
    df_vecs = self.feasible_solutions.vectable
    df_terminals = self.feasible_solutions.terminaltable

    vessel_main = self.feasible_solutions.vessels_main
    vessel_tow = self.feasible_solutions.vessels_tow
    vessel_support = self.feasible_solutions.vessels_support

    equipment_ids = []
    # Select optimal equipment from each feasible equipment section
    try:
      df_rov = self.feasible_solutions.rovtable
      df_rov.sort_values('cost_day', inplace=True)
      equipment_ids.append(df_rov['id_name'].iat[0])
    except AttributeError:
      pass
    try:
      df_divers = self.feasible_solutions.divertable
      df_divers.sort_values('cost_day', inplace=True)
      equipment_ids.append(df_divers['id_name'].iat[0])
    except AttributeError:
      pass
    try:
      df_burial = self.feasible_solutions.burialtable
      df_burial.sort_values('cost_day', inplace=True)
      equipment_ids.append(df_burial['id_name'].iat[0])
    except AttributeError:
      pass
    try:
      df_protect = self.feasible_solutions.protecttable
      df_protect.sort_values('cost_day', inplace=True)
      equipment_ids.append(df_protect['id_name'].iat[0])
    except AttributeError:
      pass
    try:
      df_piling = self.feasible_solutions.pilingtable
      df_piling.sort_values('cost_day', inplace=True)
      equipment_ids.append(df_piling['id_name'].iat[0])
    except AttributeError:
      pass

    # Vessel-Vessel matching (Can a tug boat tow a certain barge?)
    # VEC and Vessels combinations
    vcomb = pd.DataFrame(columns=["vec_id", "v1_id", "v2_id", "v3_id"])

    for ind, row in df_vecs.iterrows():
      vec_id = row['vec_id']
      # Filter vessels from feasible solution by vec_id
      # v1 - main ; v2 - tow ; v3 - support
      v1 = vessel_main[vessel_main['vec_id'] == vec_id]
      v2 = vessel_tow[vessel_tow['vec_id'] == vec_id]
      v3 = vessel_support[vessel_support['vec_id'] == vec_id]

      if v1.empty is True:
        v1_list = [0]
      else:
        v1_list = v1['id_name'].tolist()
      if v2.empty is True:
        v2_list = [0]
      else:
        v2_list = v2['id_name'].tolist()
      if v3.empty is True:
        v3_list = [0]
      else:
        v3_list = v3['id_name'].tolist()

      for v1_aux in v1_list:
        for v2_aux in v2_list:
          for v3_aux in v3_list:
            append_list = [vec_id, v1_aux, v2_aux, v3_aux]
            v_series = pd.Series([vec_id, v1_aux, v2_aux, v3_aux],
                                 index=['vec_id', 'v1_id', 'v2_id', 'v3_id'])
            vcomb = vcomb.append(v_series, ignore_index=True)

      del v1, v2, v3

      # vcomb is the vessel combination matrix that still needs to be filtered
      # and compared with df_vecs (self.feasible_solutions.vectable)
      # vcomb = vcomb.append(df)
      # del df

    # After this for, vcomb should be something like
    #         vec_id  v1_id  v2_id v3_id
    # 0   vec_001  v11_0      0     0
    # 1   vec_001  v11_1      0     0
    # 5   vec_003  v16_0      0     0
    # 6   vec_003  v16_1      0     0
    # 7   vec_003  v16_2      0     0
    # 0   vec_004      0  v19_0     0
    # 10  vec_005  v02_0      0     0
    # 11  vec_006  v15_0  v19_1     0
    # 12  vec_006  v15_1  v19_2     0
    # 14  vec_006  v15_3  v19_4     0
    vcomb.reset_index(drop=True, inplace=True)

    vsol_comb = vcomb.copy(deep=True)
    # Loop to remove impossible solutions for not having necessary vessels
    # Remove, for example, "0   vec_004      0  v19_0     0"
    # (Besides the tow vessel, there should also be a main vessel for this VEC)
    for idx, row in vcomb.iterrows():
      # for this vcomb row, check which vessels were considered (main tow sup)
      vessel_considered = row.tolist()
      vessel_considered = vessel_considered[1:]   # Drop vec_id
      # If element of vessel_considered lsit =! 0 -> a vessel was considered
      vessel_considered = [True
                           if value != 0
                           else False
                           for value in vessel_considered]

      # Now we check the vessels combination required for this VEC id
      # Get vec_id from vcomb matrix
      vec_id = row['vec_id']
      ds_vec_row = df_vecs[df_vecs['vec_id'] == vec_id]
      df_vec_considered = ds_vec_row[['qnt1', 'qnt2', 'qnt3']]
      ps_vec_considered = df_vec_considered.iloc[0, :]
      vec_considered = [True
                        if value > 0
                        else False
                        for idx, value in ps_vec_considered.items()]
      # If they don't match -> drop it
      if vessel_considered != vec_considered:
        vsol_comb.drop(axis=0, index=idx, inplace=True)
    vsol_comb.reset_index(inplace=True, drop=True)
    # vsol_comb is the matrix of possible solutions, already filtered

    vsol_comb.insert(1, "type", np.nan)
    vsol_comb.insert(2, "description", np.nan)
    vsol_comb.insert(3, "transportation", np.nan)
    del vcomb

    # Add information about type, description and transportation again
    for idx, row in df_vecs.iterrows():
      vec_id = row['vec_id']
      vec_type = row['type']
      vec_desc = row['description']
      vec_tran = row['transportation']
      vsol_comb['type'].loc[vsol_comb['vec_id'] == vec_id] = vec_type
      vsol_comb['description'].loc[vsol_comb['vec_id'] == vec_id] = vec_desc
      vsol_comb['transportation'].loc[vsol_comb['vec_id'] == vec_id] = vec_tran

    # Index of dry tow VECs
    ind_dry_tow = vsol_comb.index[["dry" in x
                                   for x in vsol_comb['transportation']]]

    # Filter tow vessels by bollard pull
    # For loop to check if the tow vessel can tow the main vessel
    for idx in ind_dry_tow:
      # Main vessel ID (barge)
      v1_id = vsol_comb.at[idx, 'v1_id']
      # Get this main vessel idx information from vess_sol_main
      ps_barge = vessel_main[vessel_main['id_name'] == v1_id]
      ps_barge = ps_barge.iloc[0, :]    # Select first barge (if repeated)
      barge = Barge(beam=ps_barge['beam'],
                    loa=ps_barge['loa'],
                    draft=ps_barge['draft'])

      # Tow vessel ID (tug / AHTS)
      v2_id = vsol_comb.at[idx, 'v2_id']
      # Get this tow vessel idx information from vess_sol_tow
      ps_towvessel = vessel_tow[vessel_tow['id_name'] == v2_id]
      ps_towvessel = ps_towvessel.iloc[0, :]   # Select first tow (if repeated)

      # Get maximum number os strucutres/objects per trip for this barge
      n_structures_trip = self.get_number_components_trip(
          area=ps_barge['free_deck'])
      # Get max number of strucutres in one trip
      n_structures = max([max(n_structures_trip[key])
                          for key in n_structures_trip])
      ### MISSING CABLE TURNTABLE IF operation.objects = None and operation.cables != None

      # Calculate bollard pull for this combination barge + tow
      bp_req, _, _, _ = self.bollard_pull_finder(
          ps_towvessel['bollard'],
          ps_towvessel['loa'],
          barge,
          n_structures)

      ds_vec = df_vecs[df_vecs['vec_id'] == vsol_comb.at[idx, 'vec_id']]
      n_tow = ds_vec['qnt2'].iat[0]
      # ### THE BOLLARD PULL REQUIRED IF THERE ARE MORE THAN 1 TUG IS NOT THIS
      # LINEAR!!!
      if ps_towvessel['bollard'] < bp_req * n_tow:
        vsol_comb.drop(idx, inplace=True)
      del ps_towvessel, ps_barge, barge
    vsol_comb.reset_index(inplace=True, drop=True)

    # Terminal-VEC matching (Can a vessel port at a certain terminal?)
    # Terminal VEC combinations
    tvcomb = pd.DataFrame(columns=["term_id",
                                   "vec_id",
                                   "type",
                                   "description",
                                   "transportation",
                                   "v1_id", "v2_id", "v3_id",
                                   "dist_to_port",
                                   'num_trips',
                                   'max_components_trip'])
    vsol_comb_final = vsol_comb.copy(deep=True)
    del vsol_comb

    # Terminal IDs
    terminals = df_terminals['id_name'].tolist()

    # Create a pandas DataFrame with all possible combinations Terminal IDs and
    # VEC ID
    terminal_vec = [[terminals[i], vsol_comb_final.index[j]]
                    for i in range(0, len(terminals))
                    for j in range(0, vsol_comb_final.shape[0])]
    df_terminal_vec = pd.DataFrame(terminal_vec,
                                   columns=["term_id", "vsolx_id"])

    # Fill the Terminals + VEC DataFrame with information related to VECs
    vec_ids = df_terminal_vec['vsolx_id']
    tvcomb["term_id"] = df_terminal_vec['term_id']
    tvcomb["vec_id"] = vsol_comb_final.loc[vec_ids, 'vec_id'].tolist()
    tvcomb["type"] = vsol_comb_final.loc[vec_ids, 'type'].tolist()
    tvcomb["description"] = vsol_comb_final.loc[vec_ids,
                                                'description'].tolist()
    tvcomb["transportation"] = vsol_comb_final.loc[vec_ids,
                                                   'transportation'].tolist()
    tvcomb["v1_id"] = vsol_comb_final.loc[vec_ids, 'v1_id'].tolist()
    tvcomb["v2_id"] = vsol_comb_final.loc[vec_ids, 'v2_id'].tolist()
    tvcomb["v3_id"] = vsol_comb_final.loc[vec_ids, 'v3_id'].tolist()

    # Create Conditions and append them to self.combinations list
    for idx, row in tvcomb.iterrows():
      # Distance
      term_id = row['term_id']
      c = df_terminals['id_name'] == term_id
      terminal_dist = df_terminals.loc[c, 'dist_to_port'].item()

      # Number of trips and number of components per trip
      # If transportation method is wet tow, num_trips = number of components
      if 'transport' in self.methods and self.methods['transport'] == 'dry':
        df_main_vessel_row = vessel_main[vessel_main['id_name'] == row['v1_id']]
        vessel_area = df_main_vessel_row['free_deck'].iat[0]
        # Trips and components per trip
        trips_components = self.get_number_components_trip(
            area=vessel_area)

        max_n_components = max([max(trips_components[key])
                                for key in trips_components])
        num_trips = len(trips_components)
      elif 'transport' in self.methods and self.methods['transport'] == 'wet':
        max_n_components = 1
        num_trips = len(self.objects)
      else:
        # This operation doesn't require transport
        max_n_components = 1000000
        num_trips = 1

      vessel_main_id = None
      vessel_tow_id = None
      vessel_sup_id = None
      if row['v1_id'] != 0:
        vessel_main_id = row['v1_id']
      if row['v2_id'] != 0:
        vessel_tow_id = row['v2_id']
      if row['v3_id'] != 0:
        vessel_sup_id = row['v3_id']

      combination_aux = self.Combination(
          terminal=row['term_id'],
          vec=row['vec_id'],
          travel_dist=terminal_dist,
          num_trips=num_trips,
          max_components=max_n_components,
          vessel_main=vessel_main_id,
          vessel_tow=vessel_tow_id,
          vessel_support=vessel_sup_id,
          equipment=equipment_ids
      )
      self.combinations.append(combination_aux)
      del combination_aux
      # ### STUDY THE POSSIBILITY OF ADDING trips_components VARIABLE INSTEAD
      # OF max_components AND num_trips (search for: ### IDEA 1 ###)

    # Matching conditions
    # 1) LOA < terminal length (vessel 1)
    # 2) Beam < terminal entrance width (all vessels)
    # 3) Draft + clearance < terminal depth (all vessels)
    # 4) JU appropriate? (vessel 1)

    # Loop through all possible combinations
    i = 0
    while i < len(self.combinations):
      t_id = self.combinations[i].terminal          # Terminal ID
      v1_id = self.combinations[i].vessel_main      # main vessel ID
      v2_id = self.combinations[i].vessel_tow       # tow vessel ID
      v3_id = self.combinations[i].vessel_support   # support vessel ID

      # With the temrinal ID, get terminal information from df_temrinals
      terminal = df_terminals[df_terminals['id_name'] == t_id]
      if terminal.empty is False:
        terminal = terminal.iloc[0, :]  # Select first option
      else:
        terminal.loc[len(terminal)] = np.nan
        raise AssertionError('Something went wrong')

      # Now, if there is a vX_id, check if it can fit the temrinal
      if v1_id is not None:
        # Get v1 information
        v1 = vessel_main[vessel_main['id_name'] == v1_id]    # Vessel main
        if v1.empty is False:
          v1 = v1.iloc[0, :]    # Select first option
        else:
          raise AssertionError('Main vessel ID %s not found' % v1_id)

        # If any of cond1_X conditions is True, this vessels is not feasible
        cond1_1 = v1['beam'] > terminal['width']      # too wide
        cond1_2 = v1['draft'] > terminal['draught']   # insuf depth clearance
        cond1_3 = v1['loa'] > terminal['length']      # too long
        try:
          cond1_4 = (v1['jackup_capabilities'] is True &
                     terminal['accomodate_jup'] is False)
        except TypeError:
          # Terminal accomodate_jup not defined
          if v1['jackup_capabilities'] is True:
            cond1_4 = True
          else:
            cond1_4 = False
        cond1 = cond1_1 or cond1_2 or cond1_3 or cond1_4

      else:
        cond1 = False   # Dont delete this option

      if v2_id is not None:
        # Get v2 information
        v2 = vessel_tow[vessel_tow['id_name'] == v2_id]      # Vessel tow
        if v2.empty is False:
          v2 = v2.iloc[0, :]    # Select first option
        else:
          raise AssertionError('Tow vessel ID %s not found' % v2_id)

        # If any of cond2_X conditions is True, this vessels is not feasible
        cond2_1 = v2['beam'] > terminal['width']      # too wide
        cond2_2 = v2['draft'] > terminal['draught']   # insuf depth clearance
        cond2 = cond2_1 or cond2_2
      else:
        cond2 = False   # Dont delete this option

      if v3_id is not None:
        # Get v3 information
        v3 = vessel_support[vessel_support['id_name'] == v3_id]  # Vessel support
        if v3.empty is False:
          v3 = v3.iloc[0, :]    # Select first option
        else:
          raise AssertionError('Support vessel ID %s not found' % v3_id)

        # If any of cond3_X conditions is True, this vessels is not feasible
        cond3_1 = v3['beam'] > terminal['width']      # too wide
        cond3_2 = v3['draft'] > terminal['draught']   # insuf depth clearance
        cond3 = cond3_1 or cond3_2
      else:
        cond3 = False   # Dont delete this option

      if cond1 or cond2 or cond3:
        del self.combinations[i]
      else:
        i += 1

  def get_number_components_trip(
      self,
      area: float,
  ) -> dict:
    """
    get_number_components_trip:
    For a given area and a list of components, return the number of
    components transported per trip

    Inputs:
    -------
    area = Area in m^2 - float
    components = List of components - list of Object/Cable

    Returns:
    --------
    number_components_trip = Maximum number of components - dict
    """
    contingency = 1.0

    # If objects are being transported, check free deck area
    # If cables are being transported, check if cables fit in turntable

    max_num_object = np.inf
    if self.objects is not None:
      # Objects are being transported
      objects_areas = [obj.base_area
                       if hasattr(obj, 'base_area') is True else 0
                       for obj in self.objects]
      avr_area = sum(objects_areas) / len(objects_areas)
      try:
        max_num_object = int(area / avr_area)
      except OverflowError:
        max_num_object = len(self.objects)

    max_num_cables = np.inf
    if self.cables is not None:
      # Cables are being transported
      # ### REVIEW THIS - for now, we assume all cables can be transported at
      # the same time
      max_num_cables = len(self.cables)

    try:
      num_obj_left = len(self.objects)
    except TypeError:
      num_obj_left = 0
    try:
      num_cbl_left = len(self.cables)
    except TypeError:
      num_cbl_left = 0

    number_components_trip = {}
    # num_componets_trip = {
    #     0: (5, 2),      element0 -> objects; element1 -> cables
    #     1: (3, 1),
    #     2: (1, 0)
    # }
    trip = 0
    while num_obj_left > 0 or num_cbl_left > 0:
      number_objects = min(num_obj_left, max_num_object)
      number_cables = min(num_cbl_left, max_num_cables)

      number_components_trip[trip] = (number_objects, number_cables)

      num_obj_left -= number_objects
      num_cbl_left -= number_cables
      trip += 1
      if trip > 1000:
        raise AssertionError('Infinite loop')

    return number_components_trip

  # Functions related with combinations
  # Net duration
  def define_combinations_activities(self):
    """
    define_combinations_activities
    ==============================
    This functions defines which activities will be performed for this
    operation (taking into acount the operation requirements and methods) and
    updates the activities atributie of each Combination.

    Working principle:
    ------------------
    1st - get list of activities for this operation
    2nd - get net duration of each activity for each possible combination
      - this must be done because each combination have different vessels and
      that may influence the amount of components transported in each trip and,
      consequently the number of trips
    3rd - create Operation.Activity() and append it to Combination.activities
    """
    def get_dynamic_duration_data(speed_id: str) -> pd.DataFrame:
      try:
        df_speeds = get_catalog_speeds()
      except ConnectionError:
        path_catalogue_speeds = os.path.join(os.getcwd(), 'catalogues', 'Speeds.json')
        file_speeds = open(path_catalogue_speeds, 'r')
        data_speeds = file_speeds.read()
        speeds = json.loads(data_speeds)
        df_speeds = pd.DataFrame(speeds)

      df_speeds = df_speeds.set_index('id_name')
      ds_contains_id = df_speeds.index.str.contains(speed_id)
      return df_speeds[ds_contains_id]

    # Get all possible activities table (from the Catalogue, filtered)
    df_activities = self.activities_table

    # Get the sequence of activities for this operation for each possible
    # combination
    for i in range(0, len(self.combinations)):
      # Get the sequence of activities
      max_comps = self.combinations[i].max_components
      # ### IDEA 1 ###
      activity_sequence = self.define_activity_sequence(max_comps)

      # Creat a new pandas DataFrame only with activity IDs present in 
      # activity_sequence list
      # 1st - Create an empty pandas DataFrame w/ the same columns as
      # df_activities
      df_op_activities = pd.DataFrame(columns=df_activities.columns.tolist())
      # 2nd - Fill this DataFrame with information from df_activities
      for act in activity_sequence:
        df_op_activities = df_op_activities.append(
            df_activities[df_activities['id_name'] == act])
      df_op_activities.reset_index(drop=True, inplace=True)
      # pandas DataFrame with the activities of this operation is created

      # Activity indexes which duration is dynamic (column durations == nan)
      ds_duration_null = df_op_activities['duration'].isnull()
      duration_null_idxs = df_op_activities[ds_duration_null].index.tolist()

      cable_installed = 0

      # Loop through activities and create an Operation.Activity() for each one
      for idx, row in df_op_activities.iterrows():
        act_id = row['id_name']
        act_name = row['name']
        act_location = row['location']
        if str(row['hs']) != 'nan' and str(row['hs']) != '':
          act_hs = row['hs']
        else:
          act_hs = None
        if str(row['tp']) != 'nan' and str(row['tp']) != '':
          act_tp = row['tp']
        else:
          act_tp = None
        if str(row['ws']) != 'nan' and str(row['ws']) != '':
          act_ws = row['ws']
        else:
          act_ws = None
        if str(row['cs']) != 'nan' and str(row['cs']) != '':
          act_cs = row['cs']
        else:
          act_cs = None
        # if (str(row['light']) != 'nan' and
        #     str(row['light']) != '' and
        #     row['light'] != 0):
        #   act_light = row['light']
        # else:
        act_light = None

        # Activity duration is more trick to get
        if idx not in duration_null_idxs:
          # The duration is fixed
          act_dur = row['duration']
          # Check if it is "Cable lay with buoyancy module"
          act_str = row['name']
          act_str = act_str.lower()
          if ('cable' in act_str and 'lay' in act_str and
              'buoy' in act_str and 'module' in act_str):
            # 1st - Identify which route is being installed
            act_current_idx = idx
            # Count how many '-1' exist in comp_deck column before this
            # activity
            df_prev_acts = df_op_activities[df_op_activities.index < idx]
            df_installed_routes = df_prev_acts[df_prev_acts['comp_deck'] == -1]
            # 2nd - Define which route is being installed
            installing_route = df_installed_routes.shape[0]
            # Subtract the number of routes from previous cables already
            # installed
            prev_routes = 0
            for cbl_prev in self.cables[0:cable_installed]:
              prev_routes += len(cbl_prev.length)
            installing_route = installing_route - prev_routes
            # Update cable installed
            if installing_route + 1 >= len(self.cables[cable_installed].length):
              cable_installed += 1
        else:
          # The duration is dynamic
          duration_str = row['speed']
          duration_str = duration_str.lower()
          df_act_dur_dynam = get_dynamic_duration_data(duration_str)

          # Hardcoded options: ### THIS SHOULD BE DYNAMIC WITH SPEEDS CATALOG
            # transit_port; km/h; dist_to_port
            # tow; km/h; dist_to_port
            # transit_site; km/h; dist_to_port
            # transit_next; km/h; dist_objects
            # piling; m/h; burial_depth
            # cable_lay; m/h; route_len
          if ('transit_port' in duration_str or
              'transit_site' in duration_str):
            # There can be only one value for "value", "units" and "depend"
            value = df_act_dur_dynam['speed'].iat[0]
            units = df_act_dur_dynam['unit'].iat[0]
            depend = df_act_dur_dynam['depend'].iat[0]
            if units.lower() == 'km/h' and depend.lower() == 'dist_to_port':
              duration = (self.combinations[i].travel_dist / 1000) / value
              # (m/1000 / km/h)
            else:
              raise AssertionError('Unrecognized units or depend')
          elif 'tow' in duration_str:
            # There can be only one value for "value", "units" and "depend"
            value = df_act_dur_dynam['speed'].iat[0]
            units = df_act_dur_dynam['unit'].iat[0]
            depend = df_act_dur_dynam['depend'].iat[0]
            if units.lower() == 'km/h' and depend.lower() == 'dist_to_port':
              duration = (self.combinations[i].travel_dist / 1000) / value
              # (m/1000 / km/h)
            else:
              raise AssertionError('Unrecognized units or depend')
          elif 'transit_next' in duration_str:
            # There can be only one value for "value", "units" and "depend"
            value = df_act_dur_dynam['speed'].iat[0]
            units = df_act_dur_dynam['unit'].iat[0]
            depend = df_act_dur_dynam['depend'].iat[0]
            if units.lower() == 'km/h' and depend.lower() == 'dist_objects':
              # Calculate distance between objects
              # 1st - Identify which object will be installed next
              act_current_idx = idx
              # Count how many '-1' exist in comp_deck column before this
              # activity
              df_prev_acts = df_op_activities[df_op_activities.index < idx]
              df_installed_comp = df_prev_acts[df_prev_acts['comp_deck'] == -1]
              num_installed_comp = df_installed_comp.shape[0]
              # 2nd - Define which component was installed and which will be
              comp_installed_idx = num_installed_comp - 1
              comp_tobe_installed_idx = num_installed_comp
              # 3rd - Calculate distance between those compoennts
              obji_coords = self.objects[comp_installed_idx].coordinates
              objj_coords = self.objects[comp_tobe_installed_idx].coordinates
              # Calculate distance between objects and append to the list
              if obji_coords is not None and objj_coords is not None:
                distance_objs = geo_dist.distance(obji_coords, objj_coords).m
                distance_objs = int(distance_objs)
              else:
                distance_objs = 0
              distance_comp = int(geo_dist.distance(obji_coords, objj_coords).m)

              # Duration of this transit activity
              duration = (distance_comp / 1000) / value
            else:
              raise AssertionError('Unrecognized units or depend')
          elif ('hammer' in duration_str or
                'drill' in duration_str or
                'vibro' in duration_str):
            # 1st - Identify which object is being installed
            act_current_idx = idx
            # Count how many '-1' exist in comp_deck column before this
            # activity
            df_prev_acts = df_op_activities[df_op_activities.index < idx]
            df_installed_comp = df_prev_acts[df_prev_acts['comp_deck'] == -1]
            # 2nd - Define which component is being installed
            num_installing_comp = df_installed_comp.shape[0] - 1
            # 3rd - get component burial depth
            comp_burial_depth = self.objects[num_installing_comp].burial_depth
            # 4rd - get component seabed type
            comp_seabed_type = self.objects[num_installing_comp].seabed_type

            piling_id = duration_str + '_' + comp_seabed_type
            piling_speed = df_act_dur_dynam.loc[piling_id, 'speed']
            units = df_act_dur_dynam.loc[piling_id, 'unit']
            depend = df_act_dur_dynam.loc[piling_id, 'depend']

            if units.lower() == 'm/h' and depend.lower() == 'burial_depth':
              # Duration of this piling activity
              duration = comp_burial_depth / piling_speed     # m / (m/h)
            else:
              raise AssertionError('Unrecognized units or depend')
          elif 'cable_lay_split' in duration_str:
            # 1st - Identify which route is being installed
            act_current_idx = idx
            # Count how many '-1' exist in comp_deck column before this
            # activity
            df_prev_acts = df_op_activities[df_op_activities.index < idx]
            df_installed_routes = df_prev_acts[df_prev_acts['comp_deck'] == -1]
            # 2nd - Define which route is being installed
            installing_route = df_installed_routes.shape[0]
            # Subtract the number of routes from previous cables already
            # installed
            prev_routes = 0
            for cbl_prev in self.cables[0:cable_installed]:
              prev_routes += len(cbl_prev.length)
            installing_route = installing_route - prev_routes
            # 3rd - get route length
            route_length = \
                self.cables[cable_installed].length[installing_route]
            # Update cable installed
            if installing_route + 1 >= len(self.cables[cable_installed].length):
              cable_installed += 1

            value = df_act_dur_dynam['speed'].iat[0]
            units = df_act_dur_dynam['unit'].iat[0]
            depend = df_act_dur_dynam['depend'].iat[0]

            if units.lower() == 'm/h' and depend.lower() == 'route_len':
              # Duration of this piling activity
              duration = route_length / value     # m / (m/h)
            else:
              raise AssertionError('Unrecognized units or depend')
          elif 'cable_lay' in duration_str:
            # 1st - Identify which route is being installed
            act_current_idx = idx
            # Count how many '-1' exist in comp_deck column before this
            # activity
            df_prev_acts = df_op_activities[df_op_activities.index < idx]
            df_installed_routes = df_prev_acts[df_prev_acts['comp_deck'] == -1]
            # 2nd - Define which route is being installed
            installing_route = df_installed_routes.shape[0]
            # Subtract the number of routes from previous cables already
            # installed
            prev_routes = 0
            for cbl_prev in self.cables[0:cable_installed]:
              prev_routes += len(cbl_prev.length)
            installing_route = installing_route - prev_routes
            # 3rd - get route length
            route_length = \
                self.cables[cable_installed].length[installing_route]
            # 4rd - get cable seabed type
            route_seabed_type = self.cables[cable_installed].seabed_type[installing_route]
            route_seabed_type = route_seabed_type.replace(' ', '')
            # # Remove last 's' to match catalogues
            # if route_seabed_type[-1].lower() == 's':
            #   route_seabed_type = route_seabed_type[:-1]
            # 5 th - get burial tool
            burial_tool = self.methods['burial']

            # Update cable installed
            if installing_route + 1 >= len(self.cables[cable_installed].length):
              cable_installed += 1

            if burial_tool.lower() == 'none':
              burial_id = duration_str + '_surface'
            else:
              burial_id = duration_str + '_' + burial_tool + '_' + route_seabed_type
            burial_speed = df_act_dur_dynam.loc[burial_id, 'speed']
            units = df_act_dur_dynam.loc[burial_id, 'unit']
            depend = df_act_dur_dynam.loc[burial_id, 'depend']

            if units.lower() == 'm/h' and depend.lower() == 'route_len':
              # Duration of this piling activity
              duration = route_length / burial_speed    # m / (m/h)
            else:
              raise AssertionError('Unrecognized units or depend')

          # ### elif plough/jet/cut ###
          # ### elif array_insp/export_insp ###
          else:
            _e = 'Speed definition and/or Units and/or Dependend defined in '
            _e = _e + 'speeds table not recognized'
            raise AssertionError(_e)

          act_dur = duration

        # Check if this act_id is already present in the activities list
        act_in_prev_acts = [act_id in act.id
                            for act in self.combinations[i].activities]
        act_ids = [act.id for act in self.combinations[i].activities]
        if any(act_in_prev_acts) is True:
          # This activity id was already appended to combinations.activities.
          # Increment the new activity number
          for act_idx, act in reversed(list(enumerate(
                  self.combinations[i].activities))):
            if act_id in act.id:
              us_idx2 = act.id.rfind('_')   # Find last underscore (_)
              try:
                curr_int = int(act.id[(us_idx2 + 1):])
                next_int = curr_int + 1
                act_id = act_id + '_' + str(next_int)
              except ValueError:
                # First duplicate
                self.combinations[i].activities[act_idx].id = act.id + '_1'
                act_id = act_id + '_2'
              break

        activity_aux = self.Activity(id_=act_id,
                                     name=act_name,
                                     duration=float(act_dur),
                                     wave_height=act_hs,
                                     wave_period=act_tp,
                                     wind_speed=act_ws,
                                     current_speed=act_cs,
                                     light=act_light,
                                     location=act_location)
        self.combinations[i].activities.append(activity_aux)
        del activity_aux

  # Best combination regarding costs
  def combination_selector(self):
    """
    combination_selector
    ========================
    Select the optimal combination for this operation. The optimal combination
    for the first period is the best combination for all months.

    Returns
    --------
    opt_combination = Optimal combination for this operation (based on costs) -
    Operation.Combination()
    # """
    for period in ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']:
      list_total_comb_costs = [
          comb.costs.vessels[period] + comb.costs.equipment[period]
          for comb in self.combinations
      ]
      try:
        min_cost = min(list_total_comb_costs)
        min_cost
        min_index = [
            idx
            for idx, cost in enumerate(list_total_comb_costs)
            if cost == min_cost
        ]
        min_index = min_index[0]
        return self.combinations[min_index]
      except IndexError:
        # No combination for this period
        pass
    # No combination
    return

  def define_operation_attributes(self):
    """
    ### Further explanation Cpoies the combination values to the Operation
    """
    combination = self.combination_selector()

    if combination is None:
      _e = 'Operation %s - %s: %s does not have any viable combination' \
          % (self.id, self.name, self.description)
      _e = _e + '\nPossible activities OLCs are too restrictive'
      raise AssertionError(_e)

    self.activities = combination.activities
    self.terminal = combination.terminal

    self.vec = combination.vec
    try:
      df_vectable = get_catalog_vecs()
    except ConnectionError:
      path_catalogue_vecs = os.path.join(os.getcwd(), 'catalogues', 'VECs.csv')
      df_vectable = pd.read_csv(path_catalogue_vecs, sep=',')

    df_vecs = df_vectable[df_vectable['vec_id'].str.lower() == self.vec.lower()]
    ds_vec = df_vecs.iloc[0, :]
    vessels_qnt = [0, 0, 0]
    try:
      vessels_qnt[0] = int(ds_vec['qnt1'])
    except ValueError:
      pass
    try:
      vessels_qnt[1] = int(ds_vec['qnt2'])
    except ValueError:
      pass
    try:
      vessels_qnt[2] = int(ds_vec['qnt3'])
    except ValueError:
      pass

    vessels = [[combination.vessel_main] * vessels_qnt[0],
               [combination.vessel_tow] * vessels_qnt[1],
               [combination.vessel_support] * vessels_qnt[2]]
    vessels = [vessel2 for vessel1 in vessels for vessel2 in vessel1]
    vessels = [vessel for vessel in vessels if vessel is not None]
    self.vessels = vessels
    self.equipment = combination.equipment
    self.terminal_dist = combination.travel_dist

    self.logistic_solution["terminal"] = [self.terminal]
    self.logistic_solution["vec"] = [self.vec]
    self.logistic_solution["vessels"] = self.vessels
    self.logistic_solution["equipment"] = self.equipment
    self.logistic_solution["terminal_dist"] = self.terminal_dist

    self.durations = combination.statistics.durations
    self.waitings = combination.statistics.waitings

    self.costs["terminal"] = combination.costs.terminal
    self.costs["vessels"] = combination.costs.vessels
    self.costs["equipment"] = combination.costs.equipment

  # OR _bollard_pull_(...)
  def bollard_pull_finder(
      self,
      tug_bollard: float,
      tug_loa: float,
      barge: Barge = None,
      n_structures: int = 1
  ):
    """
    bollard_pull_finder:
    --------------------
    This function returns the required bollard pull (in tonnes) to tow the
    largest object to be towed in a given operation###

    Inputs:
    -------
    * opx = Operation X - Operation class
    * tug_bollard = Vessel bollard pull force - float
    * tug_loa = Vessel length overall - float
    Optional inputs:
    ----------------
    * barge = Barge for dry tow operations - Barge class
    * n_structures = Number of structures/components to be towed - integer

    Outputs:
    --------
    * bp_required = Bollard Pull required for this vessel and this components
    [tonnes] - float
    * fwind = Wind forces [N] - float
    * fwave = Wave forces [N] - float
    * fcurrent = Current forces [N] - float
    """
    def wave_drift_calc(objectt,
                        barge: Barge = None, g: float = 9.81,
                        rho_sw: float = 1029, Hs: float = 5) -> float:
      try:
        B = barge.beam
        r_coef1 = 0.55
      except AttributeError:
        # No barge -> wet tow
        try:
          # Rectangular object
          B = objectt.width
        except AttributeError:
          # Circular object
          B = objectt.diameter
        r_coef1 = 0.55  ### ATENCAO ###

      return (1 / 8) * rho_sw * g * r_coef1**2 * B * Hs**2   # Newton

    def wind_loads(objectt,
                   n_structures: int,
                   Ws: float = 20, rho_air: float = 1.2) -> float:
      def get_wind_shape_coeff(b_d: float = None, h_b: float = None) -> float:
        """
        get_shape_coeff:
        This fuction returns the Shape Coefficient (C) of a given component
        considering its b/d and h/b ratios (width over length and height over
        length, respectively).\
        Table 5.5 from DNV Standard RP-C205 was considered for this function.
        It is hardcoded.

        Inputs:
        -------
        * b_d = width over lenght - float
        * h_b = heigth over lenght - float
        Outputs:
        --------
        * shape_coeff = Shape coefficient (C) - float
        """
        def convert_to_float(frac_str):
          try:
            return float(frac_str)
          except ValueError:
            num, denom = frac_str.split('/')
            try:
              leading, num = num.split(' ')
              whole = float(leading)
            except ValueError:
              whole = 0
            frac = float(num) / float(denom)
            return whole - frac if whole < 0 else whole + frac

        # Value considered to over estimate the C factor if the user doesn't
        # input them or if their out of boundaries
        if b_d is None:
          b_d = 4
          warnings.warn('b/d ratio assumed 4')
        elif b_d > 4:
          b_d = 4
          warnings.warn('b/d ratio assumed 4')
        elif b_d < (0.25):
          b_d = 0.25
          warnings.warn('b/d ratio assumed 1/4')
        if h_b is None:
          h_b = 6
          warnings.warn('h/b ratio assumed 6')
        elif h_b > 6:
          h_b = 6
          warnings.warn('h/b ratio assumed 6')
        elif h_b < 0:
          h_b = 0
          warnings.warn('h/b ratio assumed 0')

        # DNV-GL - STANDARD RP-C205 TABLE 5-5 (August 2017) p102
        h_b_index = ['1/4', '1/3', '0.5', '2/3', '1.5', '2', '3', '4']

        shape_coeff_table = {
            '0': [0.70, 0.70, 0.75, 0.80, 0.95, 1.00, 1.10, 1.20],
            '1': [0.70, 0.75, 0.75, 0.85, 1.00, 1.05, 1.20, 1.30],
            '2': [0.75, 0.75, 0.80, 0.90, 1.05, 1.10, 1.25, 1.40],
            '4': [0.75, 0.75, 0.85, 0.95, 1.10, 1.15, 1.35, 1.50],
            '6': [0.75, 0.80, 0.90, 1.00, 1.15, 1.20, 1.40, 1.60]
        }

        shape_coeff_table = pd.DataFrame(shape_coeff_table)
        shape_coeff_table.index = h_b_index

        # Rows
        i = 0
        while i < shape_coeff_table.shape[0]:
          b_d_curr = convert_to_float(shape_coeff_table.index[i])
          if b_d <= b_d_curr:
            break
          i += 1

        # Columns
        j = 0
        while j < shape_coeff_table.shape[1]:
          h_b_curr = convert_to_float(shape_coeff_table.columns[j])
          if h_b <= h_b_curr:
            break
          j += 1

        # Create a grid to interpolate and get C shape coefficient
        array_columns = [convert_to_float(idx)
                         for idx in shape_coeff_table.columns]
        array_rows = [convert_to_float(idx)
                      for idx in shape_coeff_table.index]
        array_values = shape_coeff_table.values
        grid = interpolate.interp2d(array_columns, array_rows, array_values)

        shape_coeff = float(grid(b_d, h_b))

        return shape_coeff

      if "wet" in self.methods['transport']:
        a_windage = 0   # assumed no windage area and thus no wind loads
        c_shape = 0
      elif "dry" in self.methods['transport']:
        try:
          # Rectangular object
          a_windage = (objectt.width * objectt.height) * n_structures
          b_d = objectt.width / objectt.length
          h_b = objectt.height / objectt.width
        except AttributeError:
          # Circular object
          a_windage = (np.pi * (objectt.diameter / 2)**2) * n_structures
          b_d = objectt.diameter / objectt.length
          h_b = 1

        c_shape = get_wind_shape_coeff(b_d, h_b)
      else:
        raise ValueError("Error calculating wind loads, invalid windage area.")
        a_windage = -1
      return 0.5 * c_shape * rho_air * a_windage * Ws**2  # Newton

    def current_loads(objectt,
                      barge: Barge = None,
                      towspeed: float = 0,
                      Cs: float = 0.5,
                      rho_sw: float = 1029) -> float:
                      # tow speed = 5knots = 2.57 m/s 
      def get_dry_tow_drag_coefficient(barge: Barge,
                                       Cs: float,
                                       towspeed: float = 0,
                                       ndayssincedrydock: int = 100) -> float:
                                       # friction for barges
        # Friction Coefficient
        # BV Standards: TOWAGE AT SEA OF VESSELS OR FLOATING UNITS -
        # Bureau Veritas p13 (Chapter 3.3)
        niu = 1.2 * 10**(-6)
        reynolds = (Cs + towspeed)**2 * barge.loa / niu
        cf = 0.075 / (np.log10(reynolds) - 2)**2 
        delta_cf = 0.008 * ndayssincedrydock * cf          

        cft = cf + delta_cf + 0.0004
        return cft

      def get_wet_tow_drag_coefficient(diameter: float) -> float:
        # total drag for cylinders 
        # DNV-GL - STANDARD RP-H103 TABLE B-2 (APRIL 2011) p147
        l_d = [0, 1, 2, 4, 7]
        cds = [1.12, 0.91, 0.85, 0.87, 0.99]
        if diameter >= max(l_d):
          diameter = max(l_d)
        elif diameter <= min(l_d):
          diameter = min(l_d)

        cd = interpolate.interp1d(l_d, cds, kind="linear")            
        return cd(diameter)

      try:
        # total wet area, formula by BV
        a_wt = 0.92 * barge.loa * (barge.beam + 1.81 * barge.draft)
        cft = get_dry_tow_drag_coefficient(barge,Cs)
        fcurrent_dry_tow = 0.5 * cft * rho_sw * a_wt * (Cs+towspeed)**2 #Newton
        fcurrent = fcurrent_dry_tow

      except AttributeError:
        # No barge -> wet tow
        # area is frontal area
        # DNV Standards - RP-H103 TABLE B-2 (APRIL 2011) p147
        try:
          # Rectangular object
          a_frontal_area = objectt.height * objectt.width
          D = max(objectt.width, objectt.height)
        except AttributeError:
          # Circular object
          a_frontal_area = np.pi * (objectt.diameter / 2)**2
          D = objectt.diameter
        c_drag = get_wet_tow_drag_coefficient(D)
        fcurrent_wet_tow = (0.5 * c_drag * rho_sw * a_frontal_area
                            * (Cs + towspeed)**2)   # Newton
        fcurrent = fcurrent_wet_tow

      return fcurrent

    def tug_eff(bp, loa): ### cuidado, rever. Possivelmente remover
      # if bp < 100 and bp > 0:
      #   eff = (80 - (18 - 0.0417 * min(loa, 45) * (bp - 20)**0.5)*(5 - 1))
      #   return eff / 100
      # elif bp >= 100:
      #   return 1
      # else:
      #   raise ValueError('Error bollard pull calculation')
      return 0.75

    from scipy import interpolate

    objectt = self.requirements['largest_object']

    # Current forces [N]
    fcurrent = current_loads(objectt, barge)
    # Wave forces [N]
    fwave = wave_drift_calc(objectt, barge)
    # Wind forces [N]
    if "dry" in self.methods['transport']:
      fwind = wind_loads(objectt, n_structures)
    elif "wet" in self.methods['transport']:
      fwind = 0

    g = 9.81    ### deveria ser variavel global? ###
    ftowlinepull = (1 / g) * (fwave + fwind + fcurrent)       # [kg]
    vessel_efficiecy = tug_eff(tug_bollard, tug_loa)
    bp_required = ftowlinepull / (1000 * vessel_efficiecy)    # [tonnes]

    return bp_required, fwind, fwave, fcurrent

  # Real travel distance
  def update_dist_to_port(self):
    """
    update_dist_to_port:
    The real distance (by sea) from port to the farm
    ### RESTRICTED AREAS NOT CONSIDERED
    ### USUAL OTHER VESSEL ROUTES NOT CONSIDERED
    """
    # TODO: I have no clue why I'm getting an error when calling this on the top
    from dtop_lmo.business.cpx3.travel.graph import create_graph
    from dtop_lmo.business.cpx3.travel.graph import closest_node_sea

    site_lat = self.site.coord[0]
    site_lon = self.site.coord[1]

    df_terminals = self.feasible_solutions.terminaltable

    try:
      import os
      local_path = '/TEMP_DATABASE/Maps'
      map_path = os.getcwd() + local_path
      file_name = '/term_distances_' + \
          str(round(site_lat, 4)) + str(round(site_lon, 4))
      with open(map_path + file_name + '.json') as f:
        dist_terminals = json.load(f)  # Dictionary to store distance to terminal
    except FileNotFoundError:
      dist_terminals = dict()     # Dictionary to store distance to terminal

    # For each possible terminal, get the port-farm distance
    # Get terminals in combinations
    possible_terminals = []
    for comb in self.combinations:
      if comb.terminal not in possible_terminals:
        possible_terminals.append(comb.terminal)

    # Get distances for possible terminals
    for term_id in possible_terminals:
      # Check if this terminal was already checked
      if term_id in dist_terminals:
        pass
      else:
        if PRINT_LOGS is True:
          print('\tGenerating path from (%f,%f) to Terminal %s.' % (site_lat, site_lon, term_id))
        # Generate map graph
        map_graph = create_graph(
            graph_name=self.site.map_name,
            mesh_res=self.site.map_res,
            map_limits=self.site.map_bound
        )
        mesh_res = map_graph.grid_res

        map_limits_lat = map_graph.limits['lat']
        map_limits_lon = map_graph.limits['lon']

        # Calculate distance
        # Get terminals coordinates
        ds_term = df_terminals[df_terminals['id_name'] == term_id].squeeze()
        term_lat = ds_term.at['latitude']
        term_lon = ds_term.at['longitude']

        # Check if terminal is inside map boundaries
        if (term_lat > map_limits_lat[0] and term_lat < map_limits_lat[1] and
            term_lon > map_limits_lon[0] and term_lon < map_limits_lon[1]):
          # graph_map coordinate is x and y, not lat and lon
          # Though, farm and terminal corrdinate must be converted to x, y
          lat_deg_step = (map_limits_lat[1] - map_limits_lat[0]) / mesh_res[0]
          lon_deg_step = (map_limits_lon[1] - map_limits_lon[0]) / mesh_res[1]

          term_node_x = np.floor((term_lon - map_limits_lon[0]) / lon_deg_step)
          term_node_y = np.floor((term_lat - map_limits_lat[0]) / lat_deg_step)
          site_node_x = np.floor((site_lon - map_limits_lon[0]) / lon_deg_step)
          site_node_y = np.floor((site_lat - map_limits_lat[0]) / lat_deg_step)

          end_node = 'x' + str(int(site_node_x)) + \
                     'y' + str(int(site_node_y))
          # Get closest note on sea for terminal
          start_node = 'x' + str(int(term_node_x)) + \
                       'y' + str(int(term_node_y))

          if len(map_graph.graph.edges(start_node)) == 0:
            start_node = closest_node_sea(map_graph.graph, start_node)

          # shortest_path is a sequence of nodes from terminal to site
          try:
            shortest_path = nx.astar_path(map_graph.graph,
                                          start_node,
                                          end_node)
            # Calculate distance based on nodes
            distance = 0
            for idx_node in range(0, len(shortest_path) - 1):
              pres_node = shortest_path[idx_node]
              next_node = shortest_path[idx_node + 1]
              distance += map_graph.graph[pres_node][next_node].get('weight')

          except nx.exception.NetworkXNoPath:
            warnings.warn('Could not find a path from terminal %s' % term_id)
            distance = np.inf

          except nx.exception.NodeNotFound:
            warnings.warn('Terminal %s is out of the graph.' % term_id)
            distance = np.inf

          # Store this value in dist_terminals
          dist_terminals[term_id] = distance
        else:
          dist_terminals[term_id] = np.inf

        # Update dist_terminals as json file
        with open(map_path + file_name + '.json', 'w+') as f:
          json.dump(dist_terminals, f)

    # Allocate distances to combinations
    for idx, comb in enumerate(self.combinations):
      if dist_terminals[comb.terminal] == np.inf:
        del self.combinations[idx]
      else:
        self.combinations[idx].travel_dist = dist_terminals[comb.terminal]

    if len(self.combinations) == 0:
      _e = 'For operation \"%s\", there are no reachible' % self.name
      _e = _e + ' terminals'
      raise AssertionError(_e)

  def merge_metocean_w_values(self):
    """
    merge_metocean_w_values
    ============================
    This function merges the metocean data related with time (year, month, day
    and hour) with all possible combinations values table (durations and
    waiting times)
    """
    metocean_data = ['year', 'month', 'day', 'hour']
    df_metocean_time = self.site.metocean.filter(items=metocean_data)
    for idx, comb in enumerate(self.combinations):
      df_values = comb.values
      df_metocean_time_f = df_metocean_time[df_metocean_time.index.isin(df_values.index)]
      self.combinations[idx].values = pd.concat([df_metocean_time_f, df_values],
                                                axis=1)

  def _rov_class_req(self, op_name: str, op_desc: str) -> str:
    rov_class = None
    if 'installation' in op_name:
      # Foundation Installation
      if 'foundation' in op_name:
        if 'caisson' in op_desc:
          rov_class = 'work'
        else:
          rov_class = 'inspection'
      elif 'mooring' in op_name or 'anchor' in op_name:
        if 'pre' in op_desc and 'install' in op_desc:
          rov_class = 'work'
        else:
          rov_class = 'inspection'
      elif 'support' in op_name and 'structure' in op_name:
        rov_class = 'inspection'
      elif 'cable' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'collection' in op_name and 'point' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'device' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'external' in op_name and 'protection' in op_name:
        rov_class = 'inspection'

    elif 'preventive' in op_name and 'maintenance' in op_name:
      if 'topside' in op_desc and 'inspection' in op_desc:
        pass
      else:
        rov_class = 'inspection'

    elif 'corrective' in op_name and 'maintenance' in op_name:
      if 'mooring' in op_desc and 'replacement' in op_desc:
        rov_class = 'inspection'
      elif 'cable' in op_desc and 'repair' in op_desc:
        rov_class = 'inspection'
      elif 'cable' in op_desc and 'replacement' in op_desc:
        rov_class = 'inspection'

    elif 'decommission' in op_name:
      rov_class = 'inspection'

    return rov_class
