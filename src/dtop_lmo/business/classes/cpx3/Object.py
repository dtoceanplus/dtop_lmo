# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import warnings
warnings.simplefilter("ignore")


class Object3():
  """
  Object
  ========
  Attributes:
  ----------
  * id_ = ID of the Object - string
  * name_of_object = Name of the Object - string
  * type_of_object = Type of Object - string
  * system = System that the object belongs to (hierarchy) - string
  Optional Attributes:
  --------------------
  * catalogue_id = ID of the Object in catalogue - string
  * comp_cost = Cost of component - float [€]
  * length = Length of the Object - float [m]
  * drymass = Dry mass of the Object - float [ton]
  * base_area = Base of the area of the Object - float [m]
  * draft = Draft of the object - float [m]
  * bathymetry = At what depth the object will be installed - float [m]
  * seabed_type = Seabed soil type - string [m]
  * width = Width of the Object - float [m]
  * height = Height of the Object - float [m]
  * diameter = Diameter of the Object - float [m]
  * density = Density of the Object - float [m]
  * material = Main material of what the object is made of - string
  * upstream = Upstream connected components ids - list
  * parent = Parent of this Object (hierarchy) - string
  * child = Child(ren) of this Object (hierarchy) - list
  * burial_depth = Burial depth for piles - float [m]
  * elect_interfaces = Electrical Interface - list of string
  * coordinates = Object coordinates - tuple
  * failure_rates = Failure rate for minor and major repair - list
  * topside_exists = Obligatory attribute for devices only - boolean

  TODO: Add draft attribute ### for wet tows
  """
  def __init__(self,
               id_: str,
               name_of_object: str,
               type_of_object: str,
               system: str,
               catalogue_id: str = None,
               comp_cost: float = None,
               length: float = None,
               drymass: float = None,
               width: float = None,
               height: float = None,
               diameter: float = None,
               bathymetry: float = None,
               seabed_type: str = None,
               density: float = None,
               base_area: float = None,
               draft: float = None,
               material: str = None,
               upstream: list = None,
               parent: str = None,
               child: list = None,
               burial_depth: float = None,
               elect_interfaces: list = None,
               coordinates: tuple = None,
               topside_exists: bool = None,
               failure_rates: list = None):
    self.id = str(id_)
    self.name = str(name_of_object)
    self.type = str(type_of_object)
    self.system = str(system)
    try:
      self.catalogue_id = str(catalogue_id)
    except TypeError:
      pass
    try:
      self.comp_cost = float(comp_cost)
    except TypeError:
      pass
    try:
      self.length = float(length)
    except TypeError:
      pass
    try:
      self.drymass = float(drymass)
    except TypeError:
      pass
    try:
      self.width = float(width)
    except TypeError:
      # width was not defined
      if length is not None:
        try:
          self.diameter = float(diameter)
        except TypeError:
          _e = 'If length of object \"%s\" is defined, width or ' % self.name
          _e = _e + 'diameter must be defined too.'
          raise AssertionError(_e)
    try:
      self.height = float(height)
    except TypeError:
      pass
    try:
      self.bathymetry = float(bathymetry)
    except TypeError:
      _w = 'Bathymetry of object \"%s\" not defined' % self.name
      warnings.warn(_w)
    try:
      self.seabed_type = str(seabed_type)
    except TypeError:
      _w = 'Seabed type of object \"%s\" not defined' % self.name
      warnings.warn(_w)
    try:
      self.density = float(density)
    except TypeError:
      pass
    try:
      self.base_area = float(base_area)
    except TypeError:
      try:
        if 'pile' in self.id.lower():
          self.base_area = float(length) * float(height)
        else:
          self.base_area = float(length) * float(width)
      except TypeError:
        try:
          self.base_area = float(length) * float(diameter)
        except TypeError:
          try:
            self.base_area = float(length) * float(width)
          except TypeError:
            try:
              self.base_area = float(width) * float(diameter)
            except TypeError:
              # This object doesn't have dimensions
              pass
    if hasattr(self, 'base_area') is True:
      if self.base_area == 0:
        _e = 'Object \"%s\" dimensions are incorrect.' % self.id
        raise AssertionError(_e)
    try:
      self.draft = float(draft)
    except TypeError:
      pass
    try:
      self.material = str(material)
    except TypeError:
      pass
    # Upstream ids
    self.upstream = upstream
    try:
      self.parent = str(parent)
    except TypeError:
      pass
    try:
      self.child = str(child)
    except TypeError:
      pass
    try:
      if float(burial_depth) != 0:
        self.burial_depth = float(burial_depth)
      else:
        burial_depth = None
    except TypeError:
      if 'pile' in self.type.lower():
        raise AssertionError('\"burial_depth\" must be defined for piles')
    if burial_depth is not None and 'pile' not in self.type.lower():
      _e = '\"burial_depth\" cannot be defined for this object'
      raise AssertionError(_e)
      # ### OTHER OBJECTS WHERE burial_depth can be defined?
    self.ei = elect_interfaces
    if elect_interfaces is not None:
      # Check if the electrical interfaces are correct
      ei_ok = [True if (('jtube' in ei or 'itube' in ei) or
                        'j-tube' in ei or 'i-tube' in ei or
                        ('dry' in ei and 'mate' in ei) or
                        ('wet' in ei and 'mate' in ei) or
                        ('hard' in ei and 'wire' in ei))
               else False
               for ei in elect_interfaces]
      if all(ei_ok) is False:
        _e = 'The Electrical Interface defined is not recognized.'
        raise AssertionError(_e)
      # Check if Electrical Interface can be defined for this Object
      obj_name = self.name.lower()
      if ('device' not in obj_name and
          ('collection' not in obj_name and 'point' not in obj_name) and
          'connector' not in obj_name):
         # ### OTHER OBJECTS WHERE elect_interfaces can be defined?
        _e = '\"elect_interfaces\" cannot be defined for object '
        _e = _e + '%s' % self.name
        raise AssertionError(_e)

    self.coordinates = coordinates

    # Check if topside exists
    if 'device' in self.name.lower():
      if topside_exists is None:
        raise AssertionError('For devices, topside_exists must be defined')
      # ### Check if it is a bool
      self.topside_exists = topside_exists
    else:
      if topside_exists is not None:
        _w = 'For object \"%s\", topside attribute was neglected' % self.name
        warnings.warn(_w)
    self.failure_rates = failure_rates
    if failure_rates is not None:
      # Check if list with two numbers
      if len(failure_rates) != 2:
        _e = 'For object \"%s\", exactly two failure rates must be specified' \
            % self.name
        raise AssertionError(_e)
    # Raise error: Cost must be specified in case the failure rate is specified.
    if self.failure_rates is not None and self.comp_cost is None:
      _e = 'If failure rate is specified, cost must specified too.'
      raise AssertionError(_e)
