# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from .Site import Site
from .Barge import Barge

from .cpx1 import Operation1
from .cpx1 import Object1
from .cpx1 import Cable1
from .cpx3 import Operation3
from .cpx3 import Object3
from .cpx3 import Cable3


class Operation_class:
  @staticmethod
  def get_complexity(complexity):
    if complexity == 'low':
      return Operation1
    # elif complexity == 'med':
      # return Operation2
    elif complexity == 'high' or complexity == 'med':
      return Operation3


class Object_class:
  @staticmethod
  def get_complexity(complexity):
    if complexity == 'low':
      return Object1
    # elif complexity == 'med':
      # return Object2
    elif complexity == 'high' or complexity == 'med':
      return Object3


class Cable_class:
  @staticmethod
  def get_complexity(complexity):
    if complexity == 'low':
      return Cable1
    # elif complexity == 'med':
      # return Cable2
    elif complexity == 'high' or complexity == 'med':
      return Cable3
