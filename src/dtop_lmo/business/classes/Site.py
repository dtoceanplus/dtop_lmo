# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd

# Import functions
from dtop_lmo.business import montecarlo


class Site():
  """
  Site

  Args:
    name (str): name of the site
    coord (tuple): corrdinates of the site. (lat, lon).
    metocean (pd.DataFrame): Metocean timeseries related to weather conditions
    map_name (str, optional): Name of the map. Defaults to 'Europe'.
    map_res (tuple, optional): Ma resolution. Defaults to (1000, 1000).
    map_bound (tuple, optional): Map boundaries (lat_min, lat_max), (lon_min, lon_max). Defaults to ((35, 70), (-20, 20)).
    montecarlo_per (float, optional): Percentage of time steps of the timeseries that will be considered.
  """
  def __init__(self,
               name: str,
               coord: tuple,
               metocean: pd.DataFrame,
               map_name: str = 'Europe',
               map_res: tuple = (1000, 1000),
               map_bound: tuple = ((35, 70), (-20, 20)),
               montecarlo_per: float = 1):
    # Default values
    if map_name is None:
      map_name = 'Europe'
    if map_res is None:
      map_res = (1000, 1000)
    if map_bound is None:
      map_bound = ((35, 70), (-20, 20))

    self.name = name
    self.coord = coord
    self.metocean = metocean
    self.map_name = map_name
    self.map_res = map_res
    self.map_bound = map_bound

    if metocean is not None:
      self.metocean_integrity()

      [montecarlo_timesteps, _] = montecarlo.f_montecarlo(
          data_panda=self.metocean,
          ts_percent_dec=montecarlo_per
      )
      montecarlo_timesteps.sort()
      self.timesteps_analyse = montecarlo_timesteps

  def metocean_integrity(self):
    """metocean_integrity"""
    df_metocean = self.metocean
    # Convert index to lower strings
    df_metocean.columns = map(str.lower, df_metocean.columns)
    # Convert columns to the right format
    df_metocean['year'] = df_metocean['year'].astype(int)
    df_metocean['month'] = df_metocean['month'].astype(int)
    df_metocean['day'] = df_metocean['day'].astype(int)
    df_metocean['hour'] = df_metocean['hour'].astype(int)
    try:
      df_metocean['hs'] = df_metocean['hs'].astype(float)
    except KeyError:
      pass
    try:
      df_metocean['tp'] = df_metocean['tp'].astype(float)
    except KeyError:
      pass
    try:
      df_metocean['ws'] = df_metocean['ws'].astype(float)
    except KeyError:
      pass
    try:
      df_metocean['cs'] = df_metocean['cs'].astype(float)
    except KeyError:
      pass
    # try:
    #   df_metocean['light'] = df_metocean['light'].astype(bool)
    # except KeyError:
    #   pass

    self.metocean = df_metocean
