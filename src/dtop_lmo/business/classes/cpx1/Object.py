# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import warnings
warnings.simplefilter("ignore")


class Object1():
  """
  Object
  =======
  Attributes:
  ----------
  * id_ = ID of the Object - string
  * name = Name of the Object - string
  * type = Type of Object - string
  * system = System that the object belongs to (hierarchy) - string
  Optional Attributes:
  --------------------
  * catalogue_id = ID of the object in the catalogue - string
  * comp_cost = Component cost - float
  * drymass = Dry mass of the Object - float [ton]
  * bathymetry = At what depth the object will be installed - float [m]
  * seabed_type = Seabed soil type - string [m]
  * upstream = Upstream connected components ids - list
  * parent = Parent of this Object (hierarchy) - string
  * child = Child(ren) of this Object (hierarchy) - list
  * failure_rates = Failure rate for minor and major repair - list
  * topside_exists = Obligatory attribute for devices only - boolean
  """
  def __init__(self,
               id_: str,
               name_of_object: str,
               type_of_object: str,
               system: str,
               catalogue_id: str = None,
               comp_cost: float = None,
               drymass: float = None,
               bathymetry: float = None,
               seabed_type: str = None,
               upstream: list = None,
               parent: str = None,
               child: list = None,
               topside_exists: bool = None,
               failure_rates: list = None):
    self.id = str(id_)
    self.name = name_of_object
    self.type = type_of_object
    self.system = system
    try:
      self.catalogue_id = str(catalogue_id)
    except TypeError:
      pass
    try:
      self.comp_cost = float(comp_cost)
    except TypeError:
      pass
    try:
      self.parent = str(parent)
    except TypeError:
      pass
    try:
      self.child = str(child)
    except TypeError:
      pass
    try:
      self.drymass = float(drymass)
    except TypeError:
      self.drymass = 0
    try:
      self.bathymetry = float(bathymetry)
    except TypeError:
      _w = 'Bathymetry of object \"%s\" not defined' % self.name
      warnings.warn(_w)
    try:
      self.seabed_type = str(seabed_type)
    except TypeError:
      _w = 'Seabed type of object \"%s\" not defined' % self.name
      warnings.warn(_w)
    # Upstream ids
    self.upstream = upstream
    try:
      self.parent = str(parent)
    except TypeError:
      pass
    try:
      self.child = str(child)
    except TypeError:
      pass
    # Check if topside exists
    if 'device' in self.name.lower():
      if topside_exists is None:
        raise AssertionError('For devices, topside_exists must be defined')
      # ### Check if it is a bool
      self.topside_exists = topside_exists
    else:
      if topside_exists is not None:
        _w = 'For object \"%s\", topside attribute was neglected' % self.name
        warnings.warn(_w)
    self.failure_rates = failure_rates
    if failure_rates is not None:
      # Check if list with two numbers
      if len(failure_rates) != 2:
        _e = 'For object \"%s\", exactly two failure rates must be specified' \
            % self.name
        raise AssertionError(_e)
    if self.failure_rates is not None and self.comp_cost is None:
      _e = 'If failure rate is specified, cost must be specified as well.'
      raise AssertionError(_e)
