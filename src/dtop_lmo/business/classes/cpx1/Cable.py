# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import warnings
warnings.simplefilter("ignore")


class Cable1():
  """
  Cable
  =======
  Attributes
  ----------
  * id_ = ID of the cable - string
  * name = Cable name - string
  * type = Type of cable - string
  * system = System that the object belongs to (hierarchy) - string
  * length = Length of the cable in each section [m] - list of floats
  Optional Attributes
  --------------------
  * catalogue_id = ID of the cable in the catalogue - string
  * comp_cost = Component costs - float
  * failure_rates = Failure rate for minor and major repair - list
  """
  def __init__(self,
               id_: str,
               name: str,
               cable_type: str,
               system: str,
               length: float,
               catalogue_id: str = None,
               comp_cost: float = None,
               failure_rates: list = None):
    self.id = id_
    self.name = str(name)
    self.type = str(cable_type)
    if 'export' not in self.type.lower() and 'array' not in self.type.lower():
      raise AssertionError('The cable type defined doesn\'t exist.')
    self.system = system
    self.length = float(length)
    try:
      self.catalogue_id = str(catalogue_id)
    except TypeError:
      pass
    try:
      self.comp_cost = float(comp_cost)
    except TypeError:
      pass
    self.failure_rates = failure_rates
    if failure_rates is not None:
      # Check if list with two numbers
      if len(failure_rates) != 2:
        _e = 'For object \"%s\", exactly two failure rates must be specified' \
            % self.name
        raise AssertionError(_e)
    if failure_rates is not None and comp_cost is None: 
      raise AssertionError('If failure rate is specified, cost must as well be specified.')
