# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import math as mt
import re
import networkx as nx
import geopy.distance as geo_dist
import warnings
import os
warnings.simplefilter("ignore")

# Import other classes
from dtop_lmo.business.classes.Barge import Barge
from dtop_lmo.business.classes import Site

# Import functions
from dtop_lmo.business import feasibilities as feas_funcs
from dtop_lmo.business import waiting


class Operation1(object):

  """
  Operation:
  ----------
  This class represents a Logistic operation and its attributes

  Attributes:
  ----------
  * id = ID of the operation - string
  * name = Name of the operation - string
  * description = Description of the operation - string
  * hs = Operation limit Wave Height - float
  * requirements = Set of requirements for this operation - dictionary
  * methods = Set of methods for this operation - dictionary
  * durations = Set of durations of the operation - Durations class
  * dates = Set of dates of the operation - Dates class
  * fails = Set of fails of the operation - Fails class
  * downtime = List of downtime per Device/per month/per year - list of pandas
  DataFrame
  * logistic_solution = Solution of vec, terminal, vessels and equipment - dict
  * terminal_dist = Distance between site and closest feasible terminal [m] - int

  Optional Attributes:
  -------------------
  * objects = List of Objects handled in this operation - list of Objects
  * cables = IDs of the Cables handled in this operation - list of Cables
  # * requirement_divers = If Divers are required for this operation - boolean
  """

  class Feasible():
    """
    Feasible
    ========
    Set of feasible solutuions for this operation.

    Optional attributes:
    ----------
    * vectable = List of feasible VECs - Pandas DataFrame
    * terminaltable = List of feasible Terminals - Pandas DataFrame
    * rovtable = List of feasible ROVs - Pandas DataFrame
    * burialtable = List of feasible Burial tools - Pandas DataFrame
    * pilingtable = List of feasible Piling tools - Pandas DataFrame
    * vessels_main = List of feasible Main vessels - Pandas DataFrame
    * vessels_tow = List of feasible Tow vessels - Pandas DataFrame
    * vessels_support = List of feasible Support vessels - Pandas DataFrame
    """

    def __init__(self,
                 vectable: pd.DataFrame = None,
                 terminaltable: pd.DataFrame = None,
                 rovtable: pd.DataFrame = None,
                 burialtable: pd.DataFrame = None,
                 pilingtable: pd.DataFrame = None,
                 vess_sol_main: pd.DataFrame = None,
                 vess_sol_tow: pd.DataFrame = None,
                 vess_sol_support: pd.DataFrame = None):
      if vectable is not None:
        self.vectable = vectable
      if terminaltable is not None:
        self.terminaltable = terminaltable
      if rovtable is not None:
        self.rovtable = rovtable
      if burialtable is not None:
        self.burialtable = burialtable
      if pilingtable is not None:
        self.pilingtable = pilingtable
      if vess_sol_main is not None:
        self.vessels_main = vess_sol_main
      if vess_sol_tow is not None:
        self.vessels_tow = vess_sol_tow
      if vess_sol_support is not None:
        self.vessels_support = vess_sol_support

  class Dates():
  
    """
    Dates:
    ------
    This class stands for the dates of an operation

    Attributes:
    ----------
    * start = Date and time of when the operation may start (precedent
    operations finished) - datetime
    * may_start = When the operation precedences are complete - datetime
    * may_start_delta = When the operation precedences are complete - datetime.timedelta
    * start = Start date of the operation - datetime
    * end = End date of the operation - datetime
    """
    def __init__(self):
      self.may_start = None
      self.may_start_delta = None
      self.start = None
      self.end = None

  class Fails():
    """
    Fails:
    ------
    This class stands for the systems failures that this operation is repairing

    Attributes:
    ----------
    * components = ID of the failed components/system - list of strings
    * date = Failure date - datetime
    * parts = List of the replacing parts - list of strings
    * parts_cost = List of the replacing parts cost - list of floats
    """
    def __init__(self):
      self.components = None
      self.date = None
      self.parts = None
      self.parts_cost = None

  def __init__(self,
               operation_id: str,
               operation_name: str,
               operation_description: str,
               operation_hs: float = 1.8,
               objects: list = None,
               cables: list = None,
               multiple_operations: bool = False,
               maystart_deltas: list = None):

    self.id = operation_id
    self.name = operation_name
    self.description = operation_description

    # Operation limit criteria
    self.hs = operation_hs

    # Check if at least one of objects or cables were defined
    self.objects = None
    if objects is not None:
      if isinstance(objects, list) is True:
        if len(objects) > 0:
          self.objects = objects
        else:
          self.objects = None
      else:
        # objects is not a list, it is a string
        self.objects = [objects]    # creates a list with a single element
    self.cables = None
    if cables is not None:
      if isinstance(cables, list) is True:
        if len(cables) > 0:
          self.cables = cables
        else:
          self.cables = None
      else:
        # cables is not a list, it is a string
        self.cables = [cables]    # creates a list with a single element

    self.multiple_operations = multiple_operations
    self.maystart_deltas = maystart_deltas

    self.requirements = {}
    self.methods = {}

    self.site = None

    self.feasible_funcs = None        # Feasible functions that must be run
    self.feasible_solutions = None    # Feasible solutions for this operation

    self.terminal_dist = None         # Distance between site and closest term
    self.duration_net = None          # Operation net duration

    self.vec = None                   # VEC ID
    self.terminal = None              # Terminal ID
    self.vessel = None                # Vessels ID
    self.equipment = []               # List of equipment IDs
    self.logistic_solution = {}

    self.workability = None           # Operation workability
    self.startability = None          # Operation startability
    self.waiting = None               # Operation waiting time
    self.values = None    # Operation total duration and waiting per timestep

    self.durations = None             # Durations per period
    self.waitings = None              # Waitings per period

    self.consumption = None           # Daily consumption [ton/day]

    self.costs = {}                   # Operation costs per period

    self.dates = self.Dates()
    self.fails = self.Fails()

    self.downtime = None

    # Run initialization functions
    self.update_methods_requirements()

  # Functions related with methods and requirements
  def update_methods_requirements(self):
    """
    update_methods_requirements
    ===============================
    This functions updates the Operation methods and requirements considering
    the Operation.objects and Operation.cables

    This functions reads a list of objects and/or cables and updates the
    methods and requirements of a given operation
    """
    objects = self.objects
    cables = self.cables

    # Requirements and methods are different from installation and maintenance
    # operations

    # Installation operations
    if 'installation' in self.name.lower():
      # Methods
      self.methods['load_out'] = 'lift'
      self.methods['transport'] = 'dry'

      if 'pile' in self.description.lower():
        self.methods['piling'] = 'hammering'

      if 'device' in self.name.lower():
        self.methods['assembly'] = 'pre'

      if 'cable' in self.name.lower():
        self.methods['burial'] = 'ploughing'
        cable_types = [cable.type for cable in cables]
        if 'export' in cable_types:
          self.methods['landfall'] = 'OCT'

      # Requirements
      # Lift requirement
      if objects is not None:
        if ('mooring' in self.name and 'pre' not in self.description and
            'installed' not in self.description):
          # More than one object installed at the same time
          masses = []
          for obj in objects:
            if obj.upstream is not None:
              mass = obj.drymass
              # Find upstream objects
              for up_obj in obj.upstream:
                for obj2 in objects:
                  if up_obj.lower() == obj2.id.lower():
                    try:
                      mass += obj2.drymass
                    except AttributeError:
                      pass
                    break
              masses.append(mass)
        else:
          # If objects are installed one by one
          masses = [float(obj.drymass)
                    for obj in objects
                    if hasattr(obj, 'drymass')]
        try:
          mass_max = max(masses)
        except ValueError:
          # No object has drymass attribute
          mass_max = 0
        self.requirements['lift'] = mass_max / 1000       # in ton
        # self.requirements['dwat'] = mass_max / 1000       # in ton

      # Depth requirement
      depth_max_objs = 0
      # depth_max_cbls = 0        # No cables bathymetry for CPX 1
      depth_min_objs = np.inf
      # depth_min_cbls = np.inf   # No cables bathymetry for CPX 1
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           'float' not in obj.type.lower())]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      self.requirements['depth_max'] = depth_max_objs
      self.requirements['depth_min'] = depth_min_objs

      # ROV requirement
      rov_class = self._rov_class_req(self.name,
                                      self.description)
      self.requirements['rov'] = rov_class

      # Diameter requirements (neglected)
      if objects is not None:
        self.requirements['object_diameter_max'] = 0
        self.requirements['object_diameter_min'] = np.inf
      if cables is not None:
        self.requirements['cable_diameter_max'] = 0
        self.requirements['cable_diameter_min'] = np.inf

      # Burial depth requirements
      if cables is not None:
        # Burial depth always 1 meter
        self.requirements['cable_depth'] = 1
      # MBR requirements
      if cables is not None:
        self.requirements['mbr'] = 0

      # Piling requirements
      if objects is not None:
        pile_in_types = ['pile' in obj.type for obj in self.objects]
        if any(pile_in_types):
          # Maximum piling depth not considered
          self.requirements['piling_max'] = 0

    # Preventive maintenance operations
    elif ('preventive' in self.name.lower() and
          'maintenance' in self.name.lower()):
      # Methods
      self.methods['load_out'] = 'lift'
      self.methods['transport'] = 'dry'

      # Depth requirement
      depth_max_objs = 0
      # depth_max_cbls = 0        # No cables bathymetry for CPX 1
      depth_min_objs = np.inf
      # depth_min_cbls = np.inf   # No cables bathymetry for CPX 1
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           ('device' in obj.name.lower() and
                            'float' not in obj.type.lower()))]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      self.requirements['depth_max'] = depth_max_objs
      self.requirements['depth_min'] = depth_min_objs

      if cables is not None:
        self.methods['burial'] = 'ploughing'

      # Diameter requirements (neglected)
      if objects is not None:
        self.requirements['object_diameter_max'] = 0
        self.requirements['object_diameter_min'] = np.inf
      if cables is not None:
        self.requirements['cable_diameter_max'] = 0
        self.requirements['cable_diameter_min'] = np.inf

      # Burial depth requirements
      if cables is not None:
        # Burial depth always 1 meter
        self.requirements['cable_depth'] = 1
      # MBR requirements
      if cables is not None:
        self.requirements['mbr'] = 0

      # Passenger requirement
      # TODO: THIS HARDCODED BUT IT SHOULDN'T. GET this from op maintenane catalogue
      self.requirements['passengers'] = 2

      # ROV requirements
      self.requirements['rov'] = self._rov_class_req(self.name,self.description)

    # Corrective maintenance operations
    elif ('corrective' in self.name.lower() and
          'maintenance' in self.name.lower()):
      # Methods
      self.methods['load_out'] = 'lift'
      self.methods['transport'] = 'dry'

      # Depth requirement
      depth_max_objs = 0
      # depth_max_cbls = 0        # No cables bathymetry for CPX 1
      depth_min_objs = np.inf
      # depth_min_cbls = np.inf   # No cables bathymetry for CPX 1
      try:
        depths_objs = [float(obj.bathymetry)
                       for obj in objects
                       if (hasattr(obj, 'bathymetry') and
                           ('device' in obj.name.lower() and
                            'float' not in obj.type.lower()))]
        try:
          depth_max_objs = max(depths_objs)
          depth_min_objs = min(depths_objs)
        except ValueError:
          # No object has bathymetry attribute
          pass
      except TypeError:
        # objects not defined
        pass
      self.requirements['depth_max'] = depth_max_objs
      self.requirements['depth_min'] = depth_min_objs

      if cables is not None:
        self.methods['burial'] = 'ploughing'

      # Diameter requirements (neglected)
      if objects is not None:
        self.requirements['object_diameter_max'] = 0
        self.requirements['object_diameter_min'] = np.inf
      if cables is not None:
        self.requirements['cable_diameter_max'] = 0
        self.requirements['cable_diameter_min'] = np.inf

      # Burial depth requirements
      if cables is not None:
        # Burial depth always 1 meter
        self.requirements['cable_depth'] = 1
      # MBR requirements
      if cables is not None:
        self.requirements['mbr'] = 0

      # Passenger requirement
      # TODO: THIS HARDCODED BUT IT SHOULDN'T. GET this from op maintenane catalogue
      self.requirements['passengers'] = 2

      # ROV requirements
      self.requirements['rov'] = self._rov_class_req(self.name,self.description)

    # Previous projects
    self.requirements['prev_proj'] = False
    # Consider terminal area
    self.requirements['terminal_area'] = False
    # Consider terminal load
    self.requirements['terminal_load'] = False
    # Consider terminal crane capacity
    self.requirements['terminal_crane'] = False
    # Maximum distance to port
    self.requirements['port_max_dist'] = 1000     # 1000 km

  # Allocate a site to the operation
  def allocate_site(self, site: Site):
    self.site = site

  # Functions related with feasibility
  def feasibility_selector(self):
    """
    feasibility_selector
    ---------------------
    This functions checks which feasibility functions must be called for a
    given operation
    """
    # Predefined feasibility functions
    self.feasible_funcs = [
        'vec',              # always called
        'terminal',         # always called
        'equip_rov',
        'equip_burial',
        'equip_piling',
        'vessel'            # always called
    ]

    opx_name = self.name.lower()
    opx_desc = self.description.lower()

    if self.requirements['rov'] is None:
      self.feasible_funcs.remove('equip_rov')
    if 'installation' in opx_name and 'cable' not in opx_name:
      self.feasible_funcs.remove('equip_burial')
    if 'pile' not in opx_desc:
      self.feasible_funcs.remove('equip_piling')
    if 'preventive' in opx_name and 'maintenance' in opx_name:
      self.feasible_funcs.remove('equip_burial')
    if 'corrective' in opx_name and 'maintenance' in opx_name:
      if 'cable' not in opx_desc:
        self.feasible_funcs.remove('equip_burial')
    if 'burial' in self.methods:
      if self.methods['burial'].lower() == 'none':
        self.feasible_funcs.remove('equip_burial')


  def get_feasible_solutions(self):
    sol_vecs = None
    sol_terminals = None
    sol_rovs = None
    sol_burial = None
    sol_piling = None
    sol_vessels = None

    if 'repair at port' in self.description:
      self.feasible_solutions = None
      return

    # Check which feasible functions must be called
    self.feasibility_selector()

    # Call feasibility functions that must be called according to
    # self.feasible_funcs
    if 'vec' in self.feasible_funcs:
      # For cpx1, VC combinations are different and hard coded
      path_catalogue_vecs = os.path.join(os.getcwd(), 'catalogues', 'VECs_simple.csv')
      df_vectable = pd.read_csv(path_catalogue_vecs, sep=',')

      sol_vecs = feas_funcs.vecs(self, df_vectable)
    if 'terminal' in self.feasible_funcs:
      sol_terminals = feas_funcs.terminals(self, 0)
    if 'equip_rov' in self.feasible_funcs:
      sol_rovs = feas_funcs.rov(self, 0)
    if 'equip_burial' in self.feasible_funcs:
      sol_burial = feas_funcs.burial(self, 0)
    if 'equip_piling' in self.feasible_funcs:
      sol_piling = feas_funcs.piling(self, 0)
    if 'vessel' in self.feasible_funcs:
      sol_vessels = feas_funcs.vessels(
          opx=self,
          vecs=sol_vecs,
          sf=0
      )

    self.feasible_solutions = self.Feasible(
        vectable=sol_vecs,
        terminaltable=sol_terminals,
        rovtable=sol_rovs,
        burialtable=sol_burial,
        pilingtable=sol_piling,
        vess_sol_main=sol_vessels['main'],
        vess_sol_tow=sol_vessels['tow'],
        vess_sol_support=sol_vessels['support']
    )

  def get_operation_net_duration(self):
    def get_component_duration(op_id: str) -> float:
      # TODO: This should not be hard coded ###
      object_duration = {
          "op01": 8,
          "op02": 6.5,
          "op03": 8,
          "op04": 16,
          "op05": 5,
          "op06": 0.003,    # h/m
          "op07": 0.002,    # h/m
          "op10": 3,
          "op11": 4,
          "op12": 4,
          "op13": 4,
          "op14": 12,
          "op15": 8,
          "op16": 72,
          "op17": 8,
          "op18": 12,
          "op19": 20,
          "op20": 20,
          "op21": 60,
      }
      id_simple = op_id.lower().split('_')[0]
      return float(object_duration[id_simple])

    # Get transit duration
    VESSEL_SPEED = 20   # km/h
    VESSEL_TOW = 5      # km/h

    op_name = self.name.lower()
    if 'installation' in op_name or 'decommission' in op_name:
      mobilization_duration = 48 + 48
    elif 'maintenance' in op_name:
      if 'inspection' in self.description:
        mobilization_duration = 4 + 4
      else:
        if 'op15' in self.id.lower():
          mobilization_duration = 48 + 0
        elif 'op16' in self.id.lower():
          mobilization_duration = 0
        elif 'op17' in self.id.lower():
          mobilization_duration = 0 + 48
        else:
          mobilization_duration = 48 + 48

    if 'wet' in self.methods['transport']:
      # Number of trips equal to number of objects
      transit_duration = (self.terminal_dist / 1000) / VESSEL_TOW
      transit_duration += (self.terminal_dist / 1000) / VESSEL_SPEED
      transit_duration = transit_duration * len(self.objects)
    elif 'dry' in self.methods['transport']:
      # Single trip
      transit_duration = 2 * (self.terminal_dist / 1000) / VESSEL_SPEED

    # Operation duration per component
    duration_per_component = get_component_duration(self.id)
    # Operation duration (excluding transit)
    if 'op06' not in self.id.lower() and 'op07' not in self.id.lower():
      # Not cable installation
      operation_duration = 0
      try:
        operation_duration += duration_per_component * len(self.objects)
      except TypeError:
        pass
      try:
        operation_duration += duration_per_component * len(self.cables)
      except TypeError:
        pass
    else:
      # Cable installation
      durations = [duration_per_component * cbl.length
                   for cbl in self.cables]
      operation_duration = sum(durations)

    # Total net duration
    total_duration = mobilization_duration + transit_duration + \
        operation_duration

    self.duration_net = total_duration

  def get_optimal_solution(self):
    # Terminal selected in "get_operation_net_duration" function

    # Get the only vec ID
    vec_id = self.feasible_solutions.vectable.loc[0, 'vec_id']
    self.vec = vec_id

    # Get less expensive equipment
    try:
      feasible_rovs = self.feasible_solutions.rovtable
      feasible_rovs.sort_values('cost_day', inplace=True)
      feasible_rovs.reset_index(inplace=True)
      equip_rov_id = feasible_rovs.loc[0, 'id_name']
    except AttributeError:
      equip_rov_id = None
    try:
      feasible_piling = self.feasible_solutions.pilingtable
      feasible_piling.sort_values('cost_day', inplace=True)
      feasible_piling.reset_index(inplace=True)
      equip_piling_id = feasible_piling.loc[0, 'id_name']
    except AttributeError:
      equip_piling_id = None
    try:
      feasible_burial = self.feasible_solutions.burialtable
      feasible_burial.sort_values('cost_day', inplace=True)
      feasible_burial.reset_index(inplace=True)
      equip_burial_id = feasible_burial.loc[0, 'id_name']
    except AttributeError:
      equip_burial_id = None

    equipment_ids = []
    if equip_rov_id is not None:
      equipment_ids.append(equip_rov_id)
    if equip_piling_id is not None:
      equipment_ids.append(equip_piling_id)
    if equip_burial_id is not None:
      equipment_ids.append(equip_burial_id)
    self.equipment = equipment_ids

    self.logistic_solution["terminal"] = [self.terminal]
    self.logistic_solution["vec"] = [self.vec]
    self.logistic_solution["vessels"] = [self.vessel]
    self.logistic_solution["equipment"] = self.equipment
    self.logistic_solution["terminal_dist"] = self.terminal_dist

  def merge_metocean_w_values(self):
    """
    merge_metocean_w_values
    ============================
    This function merges the metocean data related with time (year, month, day
    and hour) with all possible combinations values table (durations and
    waiting times)
    """
    metocean_data = ['year', 'month', 'day', 'hour']
    df_metocean_time = self.site.metocean.filter(items=metocean_data)
    for idx, comb in enumerate(self.combinations):
      df_values = comb.values
      self.combinations[idx].values = pd.concat([df_metocean_time, df_values],
                                                axis=1)

  def _rov_class_req(self, op_name: str, op_desc: str) -> str:
    op_name = op_name.lower()
    op_desc = op_desc.lower()
    rov_class = None
    if 'installation' in op_name:
      # Foundation Installation
      if 'foundation' in op_name:
        if 'caisson' in op_desc:
          rov_class = 'work'
        else:
          rov_class = 'inspection'
      elif 'mooring' in op_name or 'anchor' in op_name:
        if 'pre' in op_desc and 'install' in op_desc:
          rov_class = 'work'
        else:
          rov_class = 'inspection'
      elif 'support' in op_name and 'structure' in op_name:
        rov_class = 'inspection'
      elif 'cable' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'collection' in op_name and 'point' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'device' in op_name:
        # ### CHECK ELECTRICAL INTERFACES
        # IF Wet-mate
        # 'work', else 'inspect'
        rov_class = 'inspection'
      elif 'external' in op_name and 'protection' in op_name:
        rov_class = 'inspection'

    elif 'preventive' in op_name and 'maintenance' in op_name:
      if 'topside' in op_desc and 'inspection' in op_desc:
        pass
      else:
        rov_class = 'inspection'

    elif 'corrective' in op_name and 'maintenance' in op_name:
      if 'retrieval' in op_desc or 'redeployment' in op_desc:
        rov_class = 'inspection'
      if 'mooring' in op_desc and 'replacement' in op_desc:
        rov_class = 'inspection'
      elif 'cable' in op_desc and 'repair' in op_desc:
        rov_class = 'inspection'
      elif 'cable' in op_desc and 'replacement' in op_desc:
        rov_class = 'inspection'
    elif 'decommissioning' in op_name:
      rov_class = 'inspection'

    return rov_class
