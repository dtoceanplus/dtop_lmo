# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
import pandas as pd
import numpy as np
import pickle

# Import classes
from dtop_lmo.business.classes import Site


def workability_cpx1(
    operation,
    site: Site
) -> pd.DataFrame:
  """
  workability
  ===========

  Inputs:
  -------
  * operation = Operation to check workability - Operation()
  * site = Site - Site()

  Returns:
  --------
  * df_workability = Table of workabilities - pandas DataFrame
  """
  # Try to load Workability database
  try:
    # ### LOAD THIS FROM CATALOGUES AND NOT FROM LOCAL MACHINE ###
    import os
    from pathlib import Path
    local_path = '/TEMP_DATABASE'
    db_path = os.getcwd() + local_path + '/' + site.name
    Path(db_path).mkdir(parents=True, exist_ok=True)
    file_workability_db = open(db_path + '/db_ops_workability', 'rb')
    list_work_db = pickle.load(file_workability_db)
    file_workability_db.close()
  except FileNotFoundError:
    # Workability pickle database not defined yet
    # Define a list of dict "database" to improve df_workability calculations
    list_work_db = []

  df_metocean = site.metocean

  # Initialize a pandas DataFrame df_workability with True (operation always
  # "workable")
  rows_df = df_metocean.shape[0]    # Same size of metocean data in rows

  df_workability = pd.DataFrame(True,
                                index=df_metocean.index.tolist(),
                                columns=[operation.id])

  hs = operation.hs

  # First, search in the database if the workability for this OLCs was
  # already defined
  workability_exists = False

  for db_elem in list_work_db:
    if db_elem['hs'] == hs:
      workability_exists = True
      break

  if workability_exists is True:
    # Load workability from database
    list_workability = db_elem['workability']
    ds_workability = pd.Series(list_workability, name=operation.id)
    df_workability[operation.id] = ds_workability
    del db_elem
  else:
    # The workability for these OLCs was not define yet
    # For the given metocean data, check when this operation CANNOT be
    # performed
    c_hs = df_metocean['hs'] > hs     # Condition Hs

    ds_op_work = df_workability[operation.id]
    ds_op_work[c_hs] = False
    df_workability[operation.id] = ds_op_work

    if ds_op_work.sum() == 0:
      _e = 'Operation %s does not have workability' % operation.id
      raise AssertionError(_e)

    # Update database
    list_workability = ds_op_work.tolist()
    # Save new element to database
    db_elem = {}
    db_elem['hs'] = hs
    db_elem['workability'] = list_workability
    list_work_db.append(db_elem)

    # ### Adapt this!
    file_workability_db = open(db_path + '/db_ops_workability', 'wb')
    pickle.dump(list_work_db, file_workability_db)
    file_workability_db.close()
    # ###

    del db_elem
  del hs
  del list_work_db

  return df_workability


def workability(
    activities: list,
    site: Site
) -> pd.DataFrame:
  """
  workability
  ===========

  Inputs:
  -------
  * activities = List of the activities to check workability - list of
  Activity()
  * site = Site - Site()

  Returns:
  --------
  * df_workability = Table of workabilities - pandas DataFrame
  """
  # Try to load Workability database
  try:
    # ### LOAD THIS FROM CATALOGUES AND NOT FROM LOCAL MACHINE ###
    import os
    from pathlib import Path
    local_path = '/TEMP_DATABASE'
    db_path = os.getcwd() + local_path + '/' + site.name
    Path(db_path).mkdir(parents=True, exist_ok=True)
    file_workability_db = open(db_path + '/db_acts_workability', 'rb')
    list_work_db = pickle.load(file_workability_db)
    file_workability_db.close()
  except FileNotFoundError:
    # Workability pickle database not defined yet
    # Define a list of dict "database" to improve df_workability calculations
    list_work_db = []

  df_metocean = site.metocean

  # Initialize a pandas DataFrame df_workability with ones (activities always
  # "workable")
  rows_df = df_metocean.shape[0]    # Same size of metocean data in rows

  df_workability = pd.DataFrame(True,
                                index=df_metocean.index.tolist(),
                                columns=[act.id for act in activities])

  for activity in activities:
    hs = np.inf
    tp = np.inf
    ws = np.inf
    cs = np.inf
    # light = False

    olc_exists = False
    try:
      hs = float(activity.hs)
      olc_exists = True
    except TypeError:
      pass
    try:
      tp = float(activity.tp)
      olc_exists = True
    except TypeError:
      pass
    try:
      ws = float(activity.ws)
      olc_exists = True
    except TypeError:
      pass
    try:
      cs = float(activity.cs)
      olc_exists = True
    except TypeError:
      pass
    # try:
    #   light = int(activity.light)
    #   olc_exists = True
    # except TypeError:
    #   pass

    # If there is any weather restriction
    if olc_exists is True:
      # First, search in the database if the workability for this OLCs was
      # already defined
      workability_exists = False

      for db_elem in list_work_db:
        if (db_elem['hs'] == hs and
            db_elem['tp'] == tp and
            db_elem['ws'] == ws and 
            db_elem['cs'] == cs): # and 
            # db_elem['light'] == light):
          workability_exists = True
          break

      if workability_exists is True:
        # Load workability from database
        list_workability = db_elem['workability']
        ds_workability = pd.Series(list_workability, name=activity.id)
        df_workability[activity.id] = ds_workability
        del db_elem
      else:
        # The workability for these OLCs was not define yet
        # For the given metocean data, check when this activity CANNOT be
        # performed
        c_hs = df_metocean['hs'] > hs      # Condition Hs
        c_tp = df_metocean['tp'] > tp      # Condition Tp
        c_ws = df_metocean['ws'] > ws      # Condition Ws
        c_cs = df_metocean['cs'] > cs      # Condition Cs
        # c_light = df_metocean['light'] < light   # Condition light

        condition = c_hs | c_tp | c_ws | c_cs # | c_light

        ds_act_work = df_workability[activity.id]
        ds_act_work[condition] = False
        df_workability[activity.id] = ds_act_work

        if ds_act_work.sum() == 0:
          _e = 'Activity %s does not have workability' % activity.id
          raise AssertionError(_e)

        # Update database
        list_workability = ds_act_work.tolist()
        # Save new element to database
        db_elem = {}
        db_elem['hs'] = hs
        db_elem['tp'] = tp
        db_elem['ws'] = ws
        db_elem['cs'] = cs
        # db_elem['light'] = light
        db_elem['workability'] = list_workability
        list_work_db.append(db_elem)

        # ### Adapt this!
        file_workability_db = open(db_path + '/db_acts_workability', 'wb')
        pickle.dump(list_work_db, file_workability_db)
        file_workability_db.close()
        # ###

        del db_elem
      del hs, tp, ws, cs #, light
  del list_work_db

  return df_workability
