import defaultSettings from '@/settings'

const title = defaultSettings.title || 'Logistics and Marine Operations Planning Module'

export default function getPageTitle(pageTitle) {
  if (pageTitle) {
    return `${pageTitle} - ${title}`
  }
  return `${title}`
}
