// Logistics and Marine Operations (LMO) tool.
// LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
// energy projects.
// Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
//
// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
// or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public
// License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

import LmoStudyMain from '@/components/LmoStudy/LmoStudyMain.vue'
import LmoStudyCreate from '@/components/LmoStudy/LmoStudyCreate.vue'
import LmoStudyUpdate from '@/components/LmoStudy/LmoStudyUpdate.vue'
import LmoStudyOpen from '@/components/LmoStudy/LmoStudyOpen.vue'
import LmoStudyList from '@/components/LmoStudy/LmoStudyList.vue'
import InputsSiteCreate from '@/components/LmoStudy/inputs/InputsSiteCreate.vue'
import InputsProjectCreate from '@/components/LmoStudy/inputs/InputsProjectCreate.vue'
import InputsPhasesRequirementsCreate from '@/components/LmoStudy/inputs/InputsPhasesRequirementsCreate.vue'
import InputsOperationsMethods from '@/components/LmoStudy/inputs/InputsOperationsMethods.vue'
import LmoStudyResults from '@/components/LmoStudy/LmoStudyResults.vue'
import InstallationResults from '@/components/LmoStudy/results/InstallationResults.vue'
import MaintenanceResults from '@/components/LmoStudy/results/MaintenanceResults.vue'
import DecommissioningResults from '@/components/LmoStudy/results/DecommissioningResults.vue'
import ExportStudy from '@/components/LmoStudy/results/ExportStudy.vue'
export const constantRoutes = [
  // {
  //   path: '/login',
  //   component: () => import('@/views/login/index'),
  //   hidden: true
  // },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'Home', icon: 'dashboard' }
    }]
  },
  {
    path: '/study',
    component: Layout,
    children: [{
      path: 'create',
      name: 'lmostudy-create',
      component: LmoStudyCreate,
      meta: { title: 'Create LMO Study', icon: 'form' }
    }]
  },
  {
    path: '/study/',
    component: Layout,
    children: [{
      path: 'list',
      name: 'list-lmostudy',
      component: LmoStudyList,
      meta: { title: 'LMO Studies', icon: 'table' }
    }]
  },
  {
    path: '/',
    component: Layout,
    children: [{
      path: '/study/:id',
      name: 'lmostudy',
      component: LmoStudyMain,
      meta: { title: 'List of studies' },
      props: (route) => {
        const id = Number.parseInt(route.params.id, 10)
        if (Number.isNaN(id)) {
          return 0
        }
        return { id }
      },
      children: [
        {
          name: 'lmostudy-update',
          path: '',
          component: LmoStudyUpdate,
          props: true
        },
        {
          name: 'lmostudy-open',
          path: 'home',
          component: LmoStudyOpen,
          props: true
        },
        {
          name: 'inputs-site',
          path: 'inputs/site',
          component: InputsSiteCreate,
          props: true
        },
        {
          name: 'inputs-project',
          path: 'inputs/project',
          component: InputsProjectCreate,
          props: true
        },
        {
          name: 'inputs-phases-requirements',
          path: 'inputs/phases/requirements',
          component: InputsPhasesRequirementsCreate,
          props: true
        },
        {
          name: 'inputs-operations-methods',
          path: 'inputs/operations/methods',
          component: InputsOperationsMethods,
          props: true
        },
        {
          name: 'lmostudy-results',
          path: 'results',
          component: LmoStudyResults,
          props: true
        },
        {
          name: 'installation-results',
          path: 'results/installation',
          component: InstallationResults,
          props: true
        },
        {
          name: 'maintenance-results',
          path: 'results/maintenance',
          component: MaintenanceResults,
          props: true
        },
        {
          name: 'decommissioning-results',
          path: 'results/decommissioning',
          component: DecommissioningResults,
          props: true
        },
        {
          name: 'export-study',
          path: 'export',
          component: ExportStudy,
          props: true
        }
      ],
      hidden: true
    }]
  },
  {
    path: 'external-link',
    name: 'Links',
    meta: { title: 'Links', icon: 'link' },
    children: [
      {
        path: 'https://www.dtoceanplus.eu/',
        meta: { title: 'DTOceanPlus' }
      },
      {
        path: 'http://127.0.0.1:5000/api/',
        meta: { title: 'ReDoc (API)' }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
