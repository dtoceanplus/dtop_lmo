# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from datetime import datetime
import json

from . import db

from sqlalchemy.orm import backref
from sqlalchemy.ext import mutable


class JsonEncodedDict(db.TypeDecorator):
  """Decode and encode the json data"""
  impl = db.Text

  def process_bind_param(self, value, dialect):
    if value is None:
      return '{}'
    else:
      return json.dumps(value)

  def process_result_value(self, value, dialect):
    if value is None:
      return {}
    else:
      return json.loads(value) 

mutable.MutableDict.associate_with(JsonEncodedDict)


class LMOStudy(db.Model):
  """An SQLAlchemy database model class for a LMO study."""
  __tablename__ = 'study'

  id = db.Column(db.Integer, primary_key=True)
  name = db.Column(db.String(64), index=True, unique=True, nullable=False)
  description = db.Column(db.String(120), index=True)
  complexity = db.Column(db.String(5), nullable=False)
  date = db.Column(db.DateTime, default=datetime.now)
  status = db.Column(db.Integer, default=0)

  inputs_locked = db.Column(db.Boolean, default=False)

  # one-to-many relationship with objects
  objects = db.relationship('Object',
                            backref='study',
                            uselist=True,       # ONE-TO-MANY (True is default)
                            cascade='all, delete-orphan')

  # one-to-many relationship with cables
  cables = db.relationship('Cable',
                           backref='study',
                           uselist=True,        # ONE-TO-MANY (True is default)
                           cascade='all, delete-orphan')

  # one-to-one relationship with site
  site = db.relationship('Site',
                         backref='study',
                         uselist=False,         # ONE-TO-ONE
                         cascade='all, delete-orphan')

  # one-to-one relationship with inputs
  inputs = db.relationship('Inputs',
                           backref='study',
                           uselist=False,       # ONE-TO-ONE
                           cascade='all, delete-orphan')

  # one-to-many relationship with other module inputs
  modules = db.relationship('Module',
                            backref='study',
                            uselist=True,       # ONE-TO-MANY (True is default)
                            cascade='all, delete-orphan')

  # one-to-many relationship with phases
  phases = db.relationship('Phase',
                           backref='study',
                           uselist=True,        # ONE-TO-MANY (True is default)
                           cascade='all, delete-orphan')


class Inputs(db.Model):
  """An SQLAlchemy database model class for Inputs."""
  __tablename__ = 'inputs'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  # Common inputs
  quantile = db.Column(db.Float)
  quantile_vessels = db.Column(db.Float)
  period = db.Column(db.String(32))

  # Core inputs
  max_wait = db.Column(db.Float)
  montecarlo = db.Column(db.Float)
  comb_retain_ratio = db.Column(db.Float)
  comb_retain_min = db.Column(db.Integer)
  safety_factor = db.Column(db.Float)
  device_draft = db.Column(db.Float)
  consider_draft = db.Column(db.Boolean)
  fuel_price = db.Column(db.Float)
  sfoc = db.Column(db.Float)
  load_factor = db.Column(db.Float)

  # Installation inputs
  installation_date = db.Column(db.String(32))
  operations_hs = db.Column(db.Float)
  post_burial = db.Column(db.Boolean)

  # Maintenance inputs
  maintenance_date = db.Column(db.String(32))
  project_life = db.Column(db.Integer)
  topside_exists = db.Column(db.Boolean)
  device_repair_at_port = db.Column(db.Boolean)


class Module(db.Model):
  """An SQLAlchemy database model class for other module inputs."""
  __tablename__ = 'module'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  module = db.Column(db.String(8), nullable=True)
  design = db.Column(JsonEncodedDict, default={})
  hierarchy = db.Column(JsonEncodedDict, default={})


class Hierarchy(db.Model):
  """An SQLAlchemy database model class for the Hierarchy data."""
  inputs_id = db.Column(db.Integer, db.ForeignKey("inputs.id"))
  id = db.Column(db.Integer, primary_key=True)

  system = db.Column(db.String(8))
  name_of_node = db.Column(db.String(64))
  design_id = db.Column(db.String(64))
  node_type = db.Column(db.String(64))
  node_subtype = db.Column(db.String(64))
  category = db.Column(db.String(64))
  parent = db.Column(db.String(64))
  child = db.Column(db.String(64))
  gate_type = db.Column(db.String(8))
  failure_rate_repair = db.Column(db.Float)
  failure_rate_replacement = db.Column(db.Float)

  def update(self, **kwargs):
    for key, value in kwargs.items():
      setattr(self, key, value)


class Site(db.Model):

  """An SQLAlchemy database model class for a Site."""
  __tablename__ = 'site'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  map_name = db.Column(db.String(64))
  consider_boudaries = db.Column(db.Boolean)
  map_lat_min = db.Column(db.Float())
  map_lat_max = db.Column(db.Float())
  map_lon_min = db.Column(db.Float())
  map_lon_max = db.Column(db.Float())
  map_res_lat = db.Column(db.Integer())
  map_res_lon = db.Column(db.Integer())

  name = db.Column(db.String(128), nullable=False)
  latitude = db.Column(db.Float(), nullable=False)
  longitude = db.Column(db.Float(), nullable=False)
  bathymetry = db.Column(JsonEncodedDict, default={})
  seabed_type = db.Column(JsonEncodedDict, default={})
  metocean = db.Column(JsonEncodedDict, default={}, nullable=False)


class Phase(db.Model):

  """An SQLAlchemy database model class for a logistic Phase."""
  __tablename__ = 'phase'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  name = db.Column(db.String(64), nullable=False)
  computed = db.Column(db.Boolean, nullable=False, default=False)

  # one-to-many relationship with operations
  operations = db.relationship('Operation',
                               backref='phase',
                               uselist=True,    # ONE-TO-MANY (True is default)
                               cascade='all, delete-orphan')

  requirements = db.relationship('Requirements',
                                 backref='phase',
                                 uselist=False,       # ONE-TO-ONE
                                 cascade='all, delete-orphan')

  calculating = db.Column(db.Boolean, default=False)


class Object(db.Model):
  """An SQLAlchemy database model class for a Object."""
  __tablename__ = 'object'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  operations = db.relationship('Operation', secondary='operationobject', back_populates='objects')

  id_external = db.Column(db.String(64), index=True, nullable=False)
  name = db.Column(db.String(128), nullable=False)
  type_ = db.Column(db.String(128), nullable=False)
  system = db.Column(db.String(10), nullable=False)
  component_cost = db.Column(db.Float)
  length = db.Column(db.Float)
  drymass = db.Column(db.Float)
  width = db.Column(db.Float)
  height = db.Column(db.Float)
  diameter = db.Column(db.Float)
  bathymetry = db.Column(db.Float)
  seabed_type = db.Column(db.String(64))
  density = db.Column(db.Float)
  base_area = db.Column(db.Float)
  material = db.Column(db.String(128))
  upstream = db.Column(db.String(512))                # List in string format
  parent = db.Column(db.String(8192))
  child = db.Column(db.String(8192))                  # List in string format
  burial_depth = db.Column(db.Float)
  electrical_interfaces = db.Column(db.String(512))   # List in string format
  coordinates = db.Column(db.String(64))              # Tuple in string format
  topside_exists = db.Column(db.Boolean)
  failure_rates = db.Column(db.String(64))            # List in string format


class Cable(db.Model):
  """An SQLAlchemy database model class for a Cable."""
  __tablename__ = 'cable'

  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  operations = db.relationship('Operation', secondary='operationcable', back_populates='cables')

  id_external = db.Column(db.String(64), index=True, nullable=False)
  name = db.Column(db.String(128), nullable=False)
  type_ = db.Column(db.String(128), nullable=False)
  system = db.Column(db.String(10), nullable=False)
  component_cost = db.Column(db.Float)
  bathymetry = db.Column(db.String(8192))             # List in string format
  seabed_type = db.Column(db.String(32768))           # List in string format
  diameter = db.Column(db.Float)
  length = db.Column(db.String(8192))                 # List in string format
  drymass = db.Column(db.Float)
  mbr = db.Column(db.Float)
  electrical_interfaces = db.Column(db.String(512))   # List in string format
  subtype = db.Column(db.String(64))
  burial_depth = db.Column(db.String(8192))           # List in string format
  burial_method = db.Column(db.String(64))
  split_pipes = db.Column(db.String(8192))            # List in string format
  buoyancy_modules = db.Column(db.String(8192))       # List in string format
  parent = db.Column(db.String(8192))
  child = db.Column(db.String(8192))                  # List in string format
  failure_rates = db.Column(db.String(64))            # List in string format


OperationObject = db.Table(
    'operationobject',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('operation_id', db.Integer, db.ForeignKey('operation.id')),
    db.Column('object_id', db.Integer, db.ForeignKey('object.id'))
)


OperationCable = db.Table(
    'operationcable',
    db.Column('id', db.Integer, primary_key=True),
    db.Column('operation_id', db.Integer, db.ForeignKey('operation.id')),
    db.Column('cable_id', db.Integer, db.ForeignKey('cable.id'))
)


class Operation(db.Model):

  """An SQLAlchemy database model class for Operation."""
  __tablename__ = 'operation'

  id = db.Column(db.Integer, primary_key=True)
  phase_id = db.Column(db.Integer, db.ForeignKey('phase.id'))

  id_external = db.Column(
      db.String(64), index=True,
      nullable=False
  )
  name = db.Column(db.String(64), nullable=False)
  description = db.Column(db.String(128))

  # Methods and requirements
  methods = db.relationship('Methods',
                            backref='operation',
                            uselist=False,            # ONE-TO-ONE
                            cascade='all, delete-orphan')

  objects = db.relationship('Object', secondary='operationobject', back_populates='operations')
  cables = db.relationship('Cable', secondary='operationcable', back_populates='operations')

  activities = db.relationship('Activity',
                               backref='operation',
                               uselist=True,    # ONE-TO-MANY (True is default)
                               cascade='all, delete-orphan')

  durations = db.Column(JsonEncodedDict, default={})
  waitings = db.Column(JsonEncodedDict, default={})
  costs = db.Column(JsonEncodedDict, default={})
  logistic_solution = db.Column(JsonEncodedDict, default={})

  multiple_operations = db.Column(db.Boolean, default=False)
  dates_maystart_delta = db.Column(JsonEncodedDict, default={})

  vec = db.Column(db.String(64))
  terminal = db.Column(db.String(64))
  vessels = db.Column(db.String(256))
  equipment = db.Column(db.String(265))
  consumption = db.Column(db.Float)


class Methods(db.Model):
  """An SQLAlchemy database model class for Methods."""
  __tablename__ = 'methods'

  id = db.Column(db.Integer, primary_key=True)
  operation_id = db.Column(db.Integer, db.ForeignKey('operation.id'))

  load_out = db.Column(db.String(64))
  transportation = db.Column(db.String(64))
  piling = db.Column(db.String(64))
  burial = db.Column(db.String(64))
  assembly = db.Column(db.String(64))
  landfall = db.Column(db.String(64))


class Activity(db.Model):
  """An SQLAlchemy database model class for Activity."""
  __tablename__ = 'activity'

  id = db.Column(db.Integer, primary_key=True)
  operation_id = db.Column(db.Integer, db.ForeignKey('operation.id'))

  id_external = db.Column(db.String(64), index=True, nullable=False)
  name = db.Column(db.String(128), nullable=False)
  duration = db.Column(db.Float)
  hs = db.Column(db.Float)
  tp = db.Column(db.Float)
  ws = db.Column(db.Float)
  cs = db.Column(db.Float)
  light = db.Column(db.Boolean, default=False)
  location = db.Column(db.String(8), nullable=False)


class Requirements(db.Model):

  """An SQLAlchemy database model class for Requirements."""
  __tablename__ = 'requirements'

  id = db.Column(db.Integer, primary_key=True)
  phase_id = db.Column(db.Integer, db.ForeignKey('phase.id'))

  rov = db.Column(db.Boolean)
  divers = db.Column(db.Boolean)
  previous_projects = db.Column(db.Boolean)
  terminal_area = db.Column(db.Boolean)
  terminal_load = db.Column(db.Boolean)
  terminal_crane = db.Column(db.Boolean)
  # connect_to_electrical = db.Column(db.Boolean)
  port_maximum_distance = db.Column(db.Float)


class Representation(db.Model):
  __tablename__ = "representation"
  id = db.Column(db.Integer, primary_key=True)
  study_id = db.Column(db.Integer, db.ForeignKey('study.id'))

  phase_installation = db.Column(db.JSON)
  phase_maintenance = db.Column(db.JSON)
  phase_decommissioning = db.Column(db.JSON)


class Task(db.Model):
    __tablename__ = 'tasks'
    study_id = db.Column(db.Integer, db.ForeignKey("study.id"), nullable=False)
    id = db.Column(db.Integer, primary_key=True)
    rq_job_id = db.Column(db.String, nullable=False)
