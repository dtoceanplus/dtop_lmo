# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from marshmallow import fields, validates, ValidationError

from . import ma, db

from .models import (
    LMOStudy, Inputs, Module, Object, Cable, Site, Phase,
    Operation, Activity, Methods, Requirements, Representation
)


class LMOStudySchema(ma.ModelSchema):
  """Marshmallow schema for LMO Study database model"""

  class Meta:
    model = LMOStudy
    sqla_session = db.session


class LoadLMOStudySchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of a LMO Study.

  Schema validates the provided data against these criteria.
  """
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )
  description = fields.String()
  complexity = fields.String(
      required=True,
      error_messages={"required": "\"complexity\" needs to be specified."}
  )

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError("Name cannot be empty.")

  @validates("complexity")
  def validate_complexity(self, value):
    """Validate complexity field is not empty"""
    if len(value) == 0:
      raise ValidationError("Complexity cannot be empty.")


class UpdateLMOStudySchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for updating a LMO Study.

  No field is required. Schema validates the provided data against these criteria.
  """
  name = fields.String()
  description = fields.String()
  complexity = fields.String()
  inputs_locked = fields.Boolean()

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError("Name cannot be empty.")

  @validates("complexity")
  def validate_complexity(self, value):
    """Validate complexity field is not empty"""
    if len(value) == 0:
      raise ValidationError("Complexity cannot be empty.")


class ModuleSchema(ma.ModelSchema):
  """Marshmallow schema for other module inputs database model"""

  class Meta:
    include_fk = True
    model = Module
    sqla_session = db.session


class SiteSchema(ma.ModelSchema):

  """Marshmallow schema for a Site database model"""
  class Meta:
    include_fk = True
    model = Site
    sqla_session = db.session


class LoadSiteSchema(ma.Schema):

  """
  Marshmallow schema for loading the request body for creation of a site.

  Schema validates the provided data against these criteria.
  """
  study_id = fields.Integer()
  map_name = fields.String()
  consider_boudaries = fields.Boolean()
  map_lat_min = fields.Float()
  map_lat_max = fields.Float()
  map_lon_min = fields.Float()
  map_lon_max = fields.Float()
  map_res_lat = fields.Integer()
  map_res_lon = fields.Integer()
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )
  latitude = fields.Float(
      required=True,
      error_messages={"required": "\"latitude\" is required."}
  )
  longitude = fields.Float(
      required=True,
      error_messages={"required": "\"longitude\" is required."}
  )
  map_name = fields.String()
  map_resolution = fields.String()
  map_boundaries = fields.String()

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError('Name cannot be empty.')
  @validates("map_lat_min")
  def validate_map_lat_min(self, value):
    """Validate map_lat_min field is between -180 and 180"""
    if value < -180 or value > 180:
      raise ValidationError('map_lat_min must be between -180 and 180.')
  @validates("map_lat_max")
  def validate_map_lat_max(self, value):
    """Validate map_lat_max field is between -180 and 180"""
    if value < -180 or value > 180:
      raise ValidationError('map_lat_max must be between -180 and 180.')
  @validates("map_lon_min")
  def validate_map_lon_min(self, value):
    """Validate map_lon_min field is between -90 and 90"""
    if value < -90 or value > 90:
      raise ValidationError('map_lon_min must be between -90 and 90.')
  @validates("map_lon_max")
  def validate_map_lon_max(self, value):
    """Validate map_lon_max field is between -90 and 90"""
    if value < -90 or value > 90:
      raise ValidationError('map_lon_max must be between -90 and 90.')


class InputsSchema(ma.ModelSchema):
  """Marshmallow schema for a Inputs database model"""

  class Meta:
    include_fk = True
    model = Inputs
    sqla_session = db.session


class LoadInputsSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of a set of inputs.

  Schema validates the provided data against these criteria.
  """
  quantile = fields.Float()
  quantile_vessels = fields.Float()
  period = fields.String()

  max_wait = fields.Float()
  montecarlo = fields.Float()
  comb_retain_ratio = fields.Float()
  comb_retain_min = fields.Integer()
  safety_factor = fields.Float()
  device_draft = fields.Float()
  consider_draft = fields.Boolean()
  fuel_price = fields.Float()
  sfoc = fields.Float()
  load_factor = fields.Float()

  installation_date = fields.String()
  operations_hs = fields.Float()
  post_burial = fields.Boolean()
  maintenance_date = fields.String()
  project_life = fields.Integer()
  topside_exists = fields.Boolean()
  device_repair_at_port = fields.Boolean()

  @validates("quantile")
  def validate_quantile(self, value):
    """Validate quantile field is 0.25, 0.5 or 0.75"""
    if value != 0.25 and value != 0.5 and value != 0.75:
      raise ValidationError('Quantile must be: 0.25, 0.75 or 0.5.')

  @validates("quantile_vessels")
  def validate_quantile_vessels(self, value):
    """Validate quantile_vessels field is 0.25, 0.5 or 0.75"""
    if value != 0.25 and value != 0.5 and value != 0.75:
      raise ValidationError('Quantile must be: 0.25, 0.75 or 0.5.')

  @validates("period")
  def validate_period(self, value):
    """Validate period field is month, quarter or trimester"""
    if (value.lower() != 'month' and
        value.lower() != 'quarter' and
        value.lower() != 'trimester'):
      raise ValidationError('Period must be: month, quarter or trimester.')

  @validates("max_wait")
  def validate_max_wait(self, value):
    """Validate max_wait field is equal or greater than zero"""
    if value < 0:
      raise ValidationError('Maximum wait must be equal or greater than zero.')

  @validates("montecarlo")
  def validate_montecarlo(self, value):
    """Validate montecarlo field is between 0 and 1"""
    if value < 0 or value > 1:
      raise ValidationError('Montecarlo percentage must be between zero and one.')

  @validates("comb_retain_ratio")
  def validate_comb_retain_ratio(self, value):
    """Validate comb_retain_ratio field is between 0 and 1"""
    if value < 0 or value > 1:
      raise ValidationError('Combinations retain ratio must be between 0 and 1 (exclusive).')

  @validates("comb_retain_min")
  def validate_comb_retain_min(self, value):
    """Validate comb_retain_min field is greater than 0"""
    if value <= 0:
      raise ValidationError('Minimum retained combinations must be greater than 0.')

  @validates("safety_factor")
  def validate_safety_factor(self, value):
    """Validate safety_factor field is grater or equal than 0"""
    if value < 0:
      raise ValidationError('Safety factor must be greater or equal than 0.')

  @validates("device_draft")
  def validate_device_draft(self, value):
    """Validate device_draft field is not less than 0"""
    if value < 0:
      raise ValidationError('Device draft must not be less than 0.')

  @validates("fuel_price")
  def validate_fuel_price(self, value):
    """Validate fuel_price field is not less than 0"""
    if value < 0:
      raise ValidationError('Fuel price must not be less than 0.')

  @validates("sfoc")
  def validate_sfoc(self, value):
    """Validate sfoc field is not less than 0"""
    if value < 0:
      raise ValidationError('SFOC must not be less than 0.')

  @validates("load_factor")
  def validate_load_factor(self, value):
    """Validate load_factor field is not less than 0"""
    if value <= 0:
      raise ValidationError('Load factor must be greater than 0.')

  @validates("installation_date")
  def validate_installation_date(self, value):
    """Validate installation_date field is greater than 0"""
    if len(value) == 0:
      raise ValidationError('Installation date cannot be empty.')

  @validates("operations_hs")
  def validate_operations_hs(self, value):
    """Validate operations_hs field is greater than 0"""
    if value <= 0:
      raise ValidationError('Operation limit Hs must be greater than 0.')

  @validates("maintenance_date")
  def validate_maintenance_date(self, value):
    """Validate maintenance_date field is not empty"""
    if len(value) == 0:
      raise ValidationError('Maintenance date cannot be empty.')

  @validates("project_life")
  def validate_project_life(self, value):
    """Validate project life field is greater than 0"""
    if value <= 0:
      raise ValidationError('Project life must be greater than 0.')


class UpdateInputsSchema(ma.Schema):
  """
  Marshmallow schema for updating the request body of a set of inputs.

  Schema validates the provided data against these criteria.
  """
  quantile = fields.Float()
  quantile_vessels = fields.Float()
  period = fields.String()

  max_wait = fields.Float()
  montecarlo = fields.Float()
  comb_retain_ratio = fields.Float()
  comb_retain_min = fields.Integer()
  safety_factor = fields.Float()
  device_draft = fields.Float()
  consider_draft = fields.Boolean()
  fuel_price = fields.Float()
  sfoc = fields.Float()
  load_factor = fields.Float()

  installation_date = fields.String()
  operations_hs = fields.Float()
  post_burial = fields.Boolean()
  maintenance_date = fields.String()
  project_life = fields.Integer()
  topside_exists = fields.Boolean()
  device_repair_at_port = fields.Boolean()

  @validates("quantile")
  def validate_quantile(self, value):
    """Validate quantile field is 0.25, 0.5 or 0.75"""
    if value != 0.25 and value != 0.5 and value != 0.75:
      raise ValidationError('Quantile must be: 0.25, 0.75 or 0.5.')

  @validates("quantile_vessels")
  def validate_quantile_vessels(self, value):
    """Validate quantile_vessels field is 0.25, 0.5 or 0.75"""
    if value != 0.25 and value != 0.5 and value != 0.75:
      raise ValidationError('Quantile must be: 0.25, 0.75 or 0.5.')

  @validates("period")
  def validate_period(self, value):
    """Validate period field is month, quarter or trimester"""
    if (value.lower() != 'month' and
        value.lower() != 'quarter' and
        value.lower() != 'trimester'):
      raise ValidationError('Period must be: month, quarter or trimester.')

  @validates("max_wait")
  def validate_max_wait(self, value):
    """Validate max_wait field is equal or greater than zero"""
    if value < 0:
      raise ValidationError('Maximum wait must be equal or greater than zero.')

  @validates("montecarlo")
  def validate_montecarlo(self, value):
    """Validate montecarlo field is between 0 and 1"""
    if value < 0 or value > 1:
      raise ValidationError('Montecarlo percentage must be between zero and one.')

  @validates("comb_retain_ratio")
  def validate_comb_retain_ratio(self, value):
    """Validate comb_retain_ratio field is between 0 and 1"""
    if value < 0 or value > 1:
      raise ValidationError('Combinations retain ratio must be between 0 and 1 (exclusive).')

  @validates("comb_retain_min")
  def validate_comb_retain_min(self, value):
    """Validate comb_retain_min field is greater than 0"""
    if value <= 0:
      raise ValidationError('Minimum retained combinations must be greater than 0.')

  @validates("safety_factor")
  def validate_safety_factor(self, value):
    """Validate safety_factor field is greater or equal than 0"""
    if value < 0:
      raise ValidationError('Safety factor must be greater or equal than 0.')

  @validates("device_draft")
  def validate_device_draft(self, value):
    """Validate device_draft field is not less than 0"""
    if value < 0:
      raise ValidationError('Device draft must not be less than 0.')

  @validates("fuel_price")
  def validate_fuel_price(self, value):
    """Validate fuel_price field is not less than 0"""
    if value < 0:
      raise ValidationError('Fuel price must not be less than 0.')

  @validates("sfoc")
  def validate_sfoc(self, value):
    """Validate sfoc field is not less than 0"""
    if value < 0:
      raise ValidationError('SFOC must not be less than 0.')

  @validates("load_factor")
  def validate_load_factor(self, value):
    """Validate load_factor field is not less than 0"""
    if value <= 0:
      raise ValidationError('Load factor must be greater than 0.')

  @validates("installation_date")
  def validate_installation_date(self, value):
    """Validate installation_date field is greater than 0"""
    if len(value) == 0:
      raise ValidationError('Installation date cannot be empty.')

  @validates("operations_hs")
  def validate_operations_hs(self, value):
    """Validate operations_hs field is greater than 0"""
    if value <= 0:
      raise ValidationError('Operation limit Hs must be greater than 0.')

  @validates("maintenance_date")
  def validate_maintenance_date(self, value):
    """Validate maintenance_date field is greater than 0"""
    if len(value) == 0:
      raise ValidationError('Maintenance date cannot be empty.')

  @validates("project_life")
  def validate_project_life(self, value):
    """Validate project life field is greater than 0"""
    if value <= 0:
      raise ValidationError('Project life must be greater than 0.')


class PhaseSchema(ma.ModelSchema):

  """Marshmallow schema for a logistic Phase database model"""

  class Meta:
    include_fk = True
    model = Phase
    sqla_session = db.session


class LoadPhaseSchema(ma.Schema):

  """
  Marshmallow schema for loading the request body for creation of a Phase.

  Schema validates the provided data against these criteria.
  """
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not different than "installation", "maintenance" or "decommissioning\""""
    if (value.lower() != 'installation' and
        value.lower() != 'maintenance' and
        value.lower() != 'decommissioning'):
      _e = 'Name must be: installation, maintenance or decommissioning'
      raise ValidationError(_e)


class ObjectSchema(ma.ModelSchema):

  """Marshmallow schema for a Object database model"""
  class Meta:
    include_fk = True
    model = Object
    sqla_session = db.session


class LoadObjectSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of a object.

  Schema validates the provided data against these criteria.
  """
  id_external = fields.String(
      required=True,
      error_messages={"required": "\"id_external\" is required."}
  )
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )
  type_ = fields.String(
      required=True,
      error_messages={"required": "\"type_\" is required."}
  )
  system = fields.String(
      required=True,
      error_messages={"required": "\"system\" is required."}
  )
  component_cost = fields.Float()
  length = fields.Float()
  drymass = fields.Float()
  width = fields.Float()
  height = fields.Float()
  diameter = fields.Float()
  bathymetry = fields.Float()
  seabed_type = fields.String()
  density = fields.Float()
  base_area = fields.Float()
  material = fields.String()
  upstream = fields.String()
  parent = fields.String()
  child = fields.String()
  burial_depth = fields.Float()
  electrical_interfaces = fields.String()
  coordinates = fields.String()
  failure_rates = fields.String()

  @validates("id_external")
  def validate_id_external(self, value):
    """Validate id_external field is not empty"""
    if len(value) == 0:
      raise ValidationError('Object ID cannot be empty.')

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError('Name cannot be empty.')

  @validates("type_")
  def validate_type_(self, value):
    """Validate type field is not empty"""
    if len(value) == 0:
      raise ValidationError('Type cannot be empty.')

  @validates("system")
  def validate_system(self, value):
    """Validate system field is not empty"""
    if len(value) == 0:
      raise ValidationError('System cannot be empty.')


class CableSchema(ma.ModelSchema):

  """Marshmallow schema for a Cable database model"""
  class Meta:
    include_fk = True
    model = Cable
    sqla_session = db.session


class LoadCableSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of a cable.

  Schema validates the provided data against these criteria.
  """
  id_ = fields.String(
      required=True,
      error_messages={"required": "\"id_\" is required."}
  )
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )
  type_ = fields.String(
      required=True,
      error_messages={"required": "\"type_\" is required."}
  )
  system = fields.String(
      required=True,
      error_messages={"required": "\"system\" is required."}
  )
  bathymetry = fields.String(
      required=True,
      error_messages={"required": "\"bathymetry\" is required."}
  )
  seabed_type = fields.String(
      required=True,
      error_messages={"required": "\"bathymetry\" is required."}
  )
  component_cost = fields.Float()

  @validates("id_")
  def validate_id_(self, value):
    """Validate id_ field is not empty"""
    if len(value) == 0:
      raise ValidationError('Cable ID cannot be empty.')

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError('Name cannot be empty.')

  @validates("type_")
  def validate_type_(self, value):
    """Validate type field is not empty"""
    if len(value) == 0:
      raise ValidationError('Type cannot be empty.')

  @validates("system")
  def validate_system(self, value):
    """Validate system field is not empty"""
    if len(value) == 0:
      raise ValidationError('System cannot be empty.')


class OperationSchema(ma.ModelSchema):
  """Marshmallow schema for a logistic Operation database model"""

  class Meta:
    include_fk = True
    model = Operation
    sqla_session = db.session


class LoadOperationSchema(ma.Schema):
  """
  Marshmallow schema for loading the request body for creation of an Operation.

  Schema validates the provided data against these criteria.
  """
  id_external = fields.String(
      required=True,
      error_messages={"required": "\"id_external\" is required."}
  )
  name = fields.String(
      required=True,
      error_messages={"required": "\"name\" is required."}
  )
  description = fields.String()
  multiple_operations = fields.Boolean()

  @validates("id_external")
  def validate_id_external(self, value):
    """Validate id_external field is not empty"""
    if len(value) == 0:
      raise ValidationError("External ID cannot be empty.")

  @validates("name")
  def validate_name(self, value):
    """Validate name field is not empty"""
    if len(value) == 0:
      raise ValidationError("Name cannot be empty.")


class MethodsSchema(ma.ModelSchema):
  """Marshmallow schema for user input methods database model"""
  class Meta:
    include_fk = True
    model = Methods
    sqla_session = db.session


class ActivitySchema(ma.ModelSchema):
  """Marshmallow schema for activity database model"""
  class Meta:
    include_fk = True
    model = Activity
    sqla_session = db.session


class RequirementsSchema(ma.ModelSchema):
  """Marshmallow schema for user input requirements database model"""
  class Meta:
    include_fk = True
    model = Requirements
    sqla_session = db.session


class RepresentationSchema(ma.ModelSchema):
  """Marshmallow schema for Digital Representation database model"""
  class Meta:
    include_fk = True
    model = Representation
    sqla_session = db.session
