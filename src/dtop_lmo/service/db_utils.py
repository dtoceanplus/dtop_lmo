# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import json

import click
from flask import current_app
from flask.cli import with_appcontext

from .models import LMOStudy

from . import db


def init_db():
  """Initialise the database; drop all existing tables then create all tables from imported models"""
  db.drop_all()
  db.create_all()


def install_db():
  """Populate the sqlite database using the data stored in 'module parameters'."""

  # TODO: Not sure how best to implement this "install database" operation
  # that needs to be run when app is first installed
  # TODO: May not be used
  add_json_object_to_db()


def add_json_object_to_db():
  """
  Add a JSON object to the database.

  :param schema: the marshmallow schema to use when loading (de-serialising)
  the raw JSON data
  :param str json_filename: the name of the JSON file stored in
  dtop_lmo/business/module_parameters
  """
  test = '[{ "id": 1, "name": "Acceptability","description": "desc"}]'
  new_instance = test
  db.session.add(new_instance)
  db.session.commit()


def add_json_collection_to_db(schema, json_filename):
   """
   Add a list (collection) of JSON objects to the database.

   :param schema: the marshmallow schema to use when loading (de-serialising) the raw JSON data
   :param str json_filename: the name of the JSON file stored in dtop_lmo/business/module_parameters
   """
   models_schema = schema(many=True)
   json_data = load_json_params(json_filename)
   new_instances = models_schema.load(json_data)
   db.session.bulk_save_objects(new_instances)
   db.session.commit()


def load_json_params(json_filename):
   """
   Load the JSON data stored in the *json_filename* file.

   :param str json_filename: the name of the JSON file to be loaded
   :return: the JSON data converted to a Python dictionary or list of Python dictionaries
   :rtype: dict or list
   """
   with current_app.open_resource("static/module_parameters/{}.json".format(json_filename)) as read_file:
       return json.load(read_file)


@click.command('init-db')
@with_appcontext
def init_db_command():
  """Clear the existing data and create new tables."""
  init_db()
  click.echo('Initialized the database.')


def init_app(app):
  """Add init_db_command to app.cli"""
  app.cli.add_command(init_db_command)
