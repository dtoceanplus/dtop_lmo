# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This file should contain create_app() function.
# This function is used by Flask.

import os

from flask import Flask, request
from flask_babel import Babel
from flask_cors import CORS
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy

basedir = os.path.abspath(os.path.dirname(__file__))
db = SQLAlchemy()
ma = Marshmallow()
babel = Babel()


def create_app(test_config=None):
  # create and configure the app
  app = Flask(__name__, instance_relative_config=True)
  app.config.from_mapping(
      SECRET_KEY='dev',   # TODO CHANGE PASSWORD
      SQLALCHEMY_DATABASE_URI=os.environ.get('DATABASE_URL') or
          'sqlite:///' + os.path.join(app.instance_path, 'lmo.db'),
      SQLALCHEMY_TRACK_MODIFICATIONS=False
  )

  # enable CORS
  CORS(app, resources={r'/*': {'origins': '*'}})

  babel.init_app(app)

  if test_config is None:
    # load the instance config, if it exists, when not testing
    app.config.from_pyfile('config.py', silent=True)
  else:
    # load the test config if passed in
    app.config.from_mapping(test_config)

  # ensure the instance folder exists
  try:
    os.makedirs(app.instance_path)
  except OSError:
    pass

  db.init_app(app)
  ma.init_app(app)

  from . import db_utils
  db_utils.init_app(app)

  # Registering Blueprints
  from .api import api as openapi_design
  app.register_blueprint(openapi_design.bp, url_prefix='/api')

  from .api import studies as studies_api
  app.register_blueprint(studies_api.bp, url_prefix='/api')

  from .api import inputs as inputs_api
  app.register_blueprint(inputs_api.bp, url_prefix='/api/<lmoid>')

  from .api import integration as integration_api
  app.register_blueprint(integration_api.bp, url_prefix='/api/<lmoid>')

  from .api import components as components_api
  app.register_blueprint(components_api.bp, url_prefix='/api/<lmoid>')

  from .api import site as site_api
  app.register_blueprint(site_api.bp, url_prefix='/api/<lmoid>')

  from .api import phases as phases_api
  app.register_blueprint(phases_api.bp, url_prefix='/api/<lmoid>')

  from .api import operations as operations_api
  app.register_blueprint(operations_api.bp, url_prefix='/api/<lmoid>')

  from .api import representation as representation_api
  app.register_blueprint(representation_api.bp)

  from .api import results as results_api
  app.register_blueprint(results_api.bp, url_prefix='/api/<lmoid>')

  from .api import export as export_api
  app.register_blueprint(export_api.bp, url_prefix='/api')

  from .api import bp as bp_pact_test_api
  app.register_blueprint(bp_pact_test_api, url_prefix='/api')

  if os.environ.get("FLASK_ENV") == "development":
    from .api.integration import provider_states
    app.register_blueprint(provider_states.bp)

  from .gui import foo as foo_gui
  app.register_blueprint(foo_gui.bp, url_prefix='/gui')

  from .gui import main
  app.register_blueprint(main.bp)

  return app


@babel.localeselector
def get_locale():
  return request.accept_languages.best_match(['en', 'fr'])
