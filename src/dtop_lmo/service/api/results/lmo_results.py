# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
from marshmallow import ValidationError
from datetime import datetime
from datetime import timedelta
import pandas as pd
import json
from copy import deepcopy
import os

# Import business logic
from dtop_lmo import business

from ..components.lmo_components import convert_object, convert_cable

from ....service import db

from ...models import LMOStudy, Object, Cable, Module, Inputs, Site, Phase, Operation, Activity
from ...schemas import ActivitySchema

from dtop_lmo.service.api.catalogues import get_catalog_terminals
from dtop_lmo.service.api.catalogues import get_catalog_vecs
from dtop_lmo.service.api.catalogues import get_catalog_vessels
from dtop_lmo.service.api.catalogues import get_catalog_rovs
from dtop_lmo.service.api.catalogues import get_catalog_divers
from dtop_lmo.service.api.catalogues import get_catalog_equip_burial
from dtop_lmo.service.api.catalogues import get_catalog_equip_piling
from dtop_lmo.service.api.catalogues import get_catalog_equip_protect

class Operation_aux:
  id = None
  name = None
  description = None


class Dates_aux:
  start = None
  may_start = None
  end = None


def calculate_phase_results(lmoid, phase_id):
  """
  Considering objects and cables, site and inputs, calculates all logistic operations
  to install, maintain or decommission the MRE array

  Returns the operations created;
  Returns error messages if the LMO Study does not exist or if the site was not attributed

  Calls :meth:`convert_object()` and :meth:`convert_cable()`methods

  Args:
    lmoid (int): The ID of the LMO Study
    phase_id (int): The ID of the Phase

  Returns:
    list: List of SQLAlchemy Operation elements
  """
  # Define study
  lmo_study = LMOStudy.query.get(lmoid)
  complexity = lmo_study.complexity

  # Define inputs from other modules
  lmo_modules = Module.query.filter_by(study_id=lmoid).all()
  if len(lmo_modules) == 0:
    return 'No other module inputs defined for this LMO study.'
  # Define inputs
  lmo_inputs = Inputs.query.filter_by(study_id=lmoid).first()
  if lmo_inputs is None:
    return 'No inputs defined for this LMO study.'
  lmo_site = Site.query.filter_by(study_id=lmoid).first()
  if lmo_site is None:
    return 'No site defined for this LMO study.'

  # Define phase
  lmo_phase = Phase.query.get(phase_id)
  # Define phase requirements
  if lmo_phase.requirements is not None:
    requirement_rov = lmo_phase.requirements.rov
    requirement_divers = lmo_phase.requirements.divers
    requirement_previous_projects = lmo_phase.requirements.previous_projects
    requirement_terminal_area = lmo_phase.requirements.terminal_area
    requirement_terminal_load = lmo_phase.requirements.terminal_load
    requirement_terminal_crane = lmo_phase.requirements.terminal_crane
    # requirement_connect_to_electrical = lmo_phase.requirements.connect_to_electrical
    port_maximum_distance = lmo_phase.requirements.port_maximum_distance
  else:
    requirement_rov = None
    requirement_divers = None
    requirement_previous_projects = None
    requirement_terminal_area = None
    requirement_terminal_load = None
    requirement_terminal_crane = None
    # requirement_connect_to_electrical = None
    port_maximum_distance = None
  requirement_connect_to_electrical = None

  # Define operations
  lmo_operations = Operation.query.filter_by(phase_id=phase_id).all()

  if complexity.lower() == 'low':
    # Get General inputs from database
    period = lmo_inputs.period
    topside_exists = lmo_inputs.topside_exists
    operations_hs = lmo_inputs.operations_hs

    site_name = lmo_site.name
    site_coords = (lmo_site.latitude, lmo_site.longitude)
    site_metocean_json = lmo_site.metocean

    # Convert site_metocean_json to pandas DataFrame
    site_metocean_df = pd.DataFrame(site_metocean_json)

    site = business.classes.Site(
        name=site_name,
        coord=site_coords,
        metocean=site_metocean_df
    )

    hierarchy_et_json = None
    hierarchy_ed_json = None
    hierarchy_sk_json = None
    lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
    if lmo_module_et is not None:
      hierarchy_et_json = lmo_module_et.hierarchy
    lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if lmo_module_ed is not None:
      hierarchy_ed_json = lmo_module_ed.hierarchy
    lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
    if lmo_module_sk is not None:
      hierarchy_sk_json = lmo_module_sk.hierarchy

    # Convert hierarchies to pandas DataFrame
    hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
    hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
    hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)

    # Get Core functionalities inputs from database
    quantile = lmo_inputs.quantile

    if lmo_phase.name == 'installation':
      # Import installation and core functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df,
          operations_hs=operations_hs,
          period=period
      )
      Core_funcs = business.Core.get_complexity(
          complexity=complexity,
          quantile=quantile,
          period=period
      )

      Inst_funcs.site = site
      Inst_funcs.objects = [convert_object(obj, complexity, topside_exists)
                            for obj in lmo_study.objects]
      Inst_funcs.cables = [convert_cable(cbl, complexity)
                           for cbl in lmo_study.cables]
      # Some umbilicals are only to be considered for maintenance since they
      # are part of a static cable already
      idx = 0
      while idx < len(Inst_funcs.cables):
        if Inst_funcs.cables[idx].type.lower() == 'array_maintenance':
          del Inst_funcs.cables[idx]
        else:
          idx += 1

      # Call Installation functionalities
      Inst_funcs.merge_hierarchy()
      Inst_funcs.redefine_operations(lmo_operations)
      Inst_funcs.allocate_site_to_operations()

      # Call Core functionalities
      operations = Core_funcs.run_feasibility_functions(Inst_funcs.operations)
      operations = Core_funcs.select_optimal_terminal(Inst_funcs.operations)
      operations = Core_funcs.get_net_durations(Inst_funcs.operations)
      operations = Core_funcs.select_optimal_solution(Inst_funcs.operations)
      operations = Core_funcs.check_workabilities(Inst_funcs.operations)
      operations = Core_funcs.check_startabilities(Inst_funcs.operations)
      operations = Core_funcs.check_waiting_time(Inst_funcs.operations)
      operations = Core_funcs.define_durations_waitings_timestep(
          Inst_funcs.operations)
      operations = Core_funcs.define_statistics(Inst_funcs.operations)
      operations = Core_funcs.allocate_costs(Inst_funcs.operations)
      operations = Core_funcs.allocate_consumptions(Inst_funcs.operations)

      Inst_funcs.operations = operations

    elif lmo_phase.name == 'maintenance':
      # Import maintenance and core functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          operations_hs=operations_hs,
          period=period,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df
      )
      Core_funcs = business.Core.get_complexity(
          complexity=complexity,
          quantile=quantile,
          period=period
      )

      Main_funcs.site = site
      Main_funcs.objects = [convert_object(obj, complexity, topside_exists)
                            for obj in lmo_study.objects]
      Main_funcs.cables = [convert_cable(cbl, complexity)
                           for cbl in lmo_study.cables]

      # Call Maintenance functionalities
      Main_funcs.redefine_operations(lmo_operations)
      Main_funcs.allocate_site_to_operations()

      # Call Core functionalities
      operations = Core_funcs.run_feasibility_functions(Main_funcs.operations)
      operations = Core_funcs.select_optimal_terminal(operations)
      operations = Core_funcs.get_net_durations(operations)
      operations = Core_funcs.select_optimal_solution(operations)
      operations = Core_funcs.check_workabilities(operations)
      operations = Core_funcs.check_startabilities(operations)
      operations = Core_funcs.check_waiting_time(operations)
      operations = Core_funcs.define_durations_waitings_timestep(operations)
      operations = Core_funcs.define_statistics(operations)
      operations = Core_funcs.allocate_costs(operations)
      operations = Core_funcs.allocate_consumptions(operations)

      Main_funcs.operations = operations

    elif lmo_phase.name == 'decommissioning':
      installation_phase = Phase.query.filter_by(
          study_id=lmoid,
          name='installation'
      ).first()
      if installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'

      if len(installation_phase.operations) == 0:
        return 'Installation phase operations not found in database.'

      for lmo_operation in lmo_operations:
        op_deco_name = lmo_operation.name
        op_deco_name_short = op_deco_name.replace('removal', '')
        for lmo_inst_operation in installation_phase.operations:
          op_inst_name = lmo_inst_operation.name
          op_inst_name_short = op_inst_name.replace('installation', '')
          if op_deco_name_short.lower() == op_inst_name_short.lower():
            break

        lmo_operation.vec = lmo_inst_operation.vec
        lmo_operation.vessels = lmo_inst_operation.vessels
        lmo_operation.equipment = lmo_inst_operation.equipment
        lmo_operation.terminal = lmo_inst_operation.terminal

        lmo_operation.durations = lmo_inst_operation.durations
        lmo_operation.waitings = lmo_inst_operation.waitings
        lmo_operation.consumption = lmo_inst_operation.consumption
        lmo_operation.costs = lmo_inst_operation.costs
        lmo_operation.logistic_solution = lmo_inst_operation.logistic_solution

        # Update operation in the database
        try:
          db.session.commit()
        except IntegrityError:
          db.session.rollback()
          return 'Unknown error.'

        del lmo_inst_operation

      # Update phase in the database
      lmo_phase.computed = True
      try:
        db.session.commit()
        return
      except IntegrityError:
        db.session.rollback()
        return 'Unknown error.'

  # elif complexity.lower() == 'med':
  #   if lmo_phase.name == 'installation':
  #     pass
  #   if lmo_phase.name == 'maintenance':
  #     pass
  #   if lmo_phase.name == 'descommissioning':
  #     pass

  elif complexity.lower() == 'high' or complexity.lower() == 'med':
    # Get General inputs from database
    period = lmo_inputs.period
    topside_exists = lmo_inputs.topside_exists

    site_name = lmo_site.name
    site_coords = (lmo_site.latitude, lmo_site.longitude)
    site_metocean_json = lmo_site.metocean
    map_name = lmo_site.map_name

    hierarchy_et_json = None
    hierarchy_ed_json = None
    hierarchy_sk_json = None
    lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
    if lmo_module_et is not None:
      hierarchy_et_json = lmo_module_et.hierarchy
    lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if lmo_module_ed is not None:
      hierarchy_ed_json = lmo_module_ed.hierarchy
    lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
    if lmo_module_sk is not None:
      hierarchy_sk_json = lmo_module_sk.hierarchy

    study_objects = [convert_object(obj, complexity, topside_exists)
                     for obj in lmo_study.objects]
    study_cables = [convert_cable(cbl, complexity)
                    for cbl in lmo_study.cables]

    # Get Core functionalities inputs from database
    quantile = lmo_inputs.quantile
    montecarlo_percentage = lmo_inputs.montecarlo
    max_wait = lmo_inputs.max_wait
    comb_retain_ratio = lmo_inputs.comb_retain_ratio
    comb_retain_min = lmo_inputs.comb_retain_min
    load_factor = lmo_inputs.load_factor
    sfoc = lmo_inputs.sfoc
    fuel_price = lmo_inputs.fuel_price

    # Convert site_metocean_json to pandas DataFrame
    site_metocean_df = pd.DataFrame(site_metocean_json)
    # Convert hierarchies to pandas DataFrame
    hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
    hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
    hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)

    # Convert map resolution to tuple
    if lmo_site.map_res_lon is None and lmo_site.map_res_lat is None:
      map_resolution = None
    else:
      map_resolution = tuple(lmo_site.map_res_lon, lmo_site.map_res_lat)

    # Convert map boundaries to tuple of tuples
    if lmo_site.consider_boudaries is True:
      map_boundaries = (
          (lmo_site.map_lat_min, lmo_site.map_lat_max),
          (lmo_site.map_lon_min, lmo_site.map_lon_max)
      )
    else:
      map_boundaries = None

    if lmo_phase.name == 'installation':
      # Import installation and core functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df,
          period=period
      )
      Core_funcs = business.Core.get_complexity(
          complexity=complexity,
          max_wait=max_wait,
          comb_retain_ratio=comb_retain_ratio,
          comb_retain_min=comb_retain_min,
          quantile=quantile,
          period=period,
          load_factor=load_factor,
          sfoc=sfoc,
          fuel_price=fuel_price
      )
      site = business.classes.Site(
          name=site_name,
          coord=site_coords,
          metocean=site_metocean_df,
          map_name=map_name,
          map_res=map_resolution,
          map_bound=map_boundaries,
          montecarlo_per=montecarlo_percentage
      )
      Inst_funcs.site = site
      Inst_funcs.objects = study_objects
      Inst_funcs.cables = study_cables
      # Some umbilicals are only to be considered for maintenance since they
      # are part of a static cable already
      idx = 0
      while idx < len(Inst_funcs.cables):
        if Inst_funcs.cables[idx].type.lower() == 'array_maintenance':
          del Inst_funcs.cables[idx]
        else:
          idx += 1

      # Call Installation functionalities
      Inst_funcs.merge_hierarchy()
      Inst_funcs.redefine_operations(lmo_operations)
      Inst_funcs.allocate_site_to_operations()

      # Update operations methods and requirements
      for op_idx, op in enumerate(Inst_funcs.operations):
        # Get this operation methods
        for lmo_operation in lmo_operations:
          if op.id == lmo_operation.id_external:
            break
        if lmo_operation.methods is not None:
          method_load_out = lmo_operation.methods.load_out
          method_transportation = lmo_operation.methods.transportation
          method_piling = lmo_operation.methods.piling
          method_burial = lmo_operation.methods.burial
          method_assembly = lmo_operation.methods.assembly
          method_landfall = lmo_operation.methods.landfall
        else:
          method_load_out = None
          method_transportation = None
          method_piling = None
          method_burial = None
          method_assembly = None
          method_landfall = None

        Inst_funcs.operations[op_idx].update_methods_requirements_user(
            method_load_out=method_load_out,
            method_transportation=method_transportation,
            method_piling=method_piling,
            method_burial=method_burial,
            method_assembly=method_assembly,
            method_landfall=method_landfall,
            requirement_rov=requirement_rov,
            requirement_divers=requirement_divers,
            requirement_prev_proj=requirement_previous_projects,
            requirement_terminal_area=requirement_terminal_area,
            requirement_terminal_load=requirement_terminal_load,
            requirement_terminal_crane=requirement_terminal_crane,
            requirement_connect_to_elect=requirement_connect_to_electrical,
            port_max_dist=port_maximum_distance
        )

      # Call Core functionalities
      try:
        operations = Inst_funcs.operations
        operations = Core_funcs.run_feasibility_functions(
            operations=operations,
            quantile_vessels=lmo_inputs.quantile_vessels,
            safety_factor=lmo_inputs.safety_factor
        )
        operations = Core_funcs.run_matchability_functions(operations)
        operations = Core_funcs.define_activities(operations)
        operations = Core_funcs.delete_combinations(operations)
        operations = Core_funcs.check_workabilities(operations)
        operations = Core_funcs.check_startabilities(operations)
        operations = Core_funcs.define_durations_waitings_timestep(operations)
        operations = Core_funcs.statistical_analysis(operations)
        operations = Core_funcs.get_vessels_cost(operations)
        operations = Core_funcs.select_combination(operations)
      except AssertionError as err:
        return str(err)
      except Exception as err:
        return str(err)

      Inst_funcs.operations = operations

    elif lmo_phase.name == 'maintenance':
      # Get Maintenance inputs from database
      start_date = lmo_inputs.maintenance_date
      project_life = lmo_inputs.project_life

      # Import maintenance and core functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df,
          period=period
      )
      Core_funcs = business.Core.get_complexity(
          complexity=complexity,
          max_wait=max_wait,
          comb_retain_ratio=comb_retain_ratio,
          comb_retain_min=comb_retain_min,
          quantile=quantile,
          period=period,
          load_factor=load_factor,
          sfoc=sfoc,
          fuel_price=fuel_price
      )
      site = business.classes.Site(
          name=site_name,
          coord=site_coords,
          metocean=site_metocean_df,
          map_name=map_name,
          map_res=map_resolution,
          map_bound=map_boundaries,
          montecarlo_per=montecarlo_percentage
      )

      Main_funcs.site = site
      Main_funcs.objects = study_objects
      Main_funcs.cables = study_cables

      # Call Maintenance functionalities
      Main_funcs.redefine_operations(lmo_operations)
      Main_funcs.allocate_site_to_operations()

      # Update operations methods and requirements
      for op_idx, op in enumerate(Main_funcs.operations):
        # Get this operation methods
        for lmo_operation in lmo_operations:
          if op.id == lmo_operation.id_external:
            break
        if lmo_operation.methods is not None:
          method_load_out = lmo_operation.methods.load_out
          method_transportation = lmo_operation.methods.transportation
          method_piling = lmo_operation.methods.piling
          method_burial = lmo_operation.methods.burial
          method_assembly = lmo_operation.methods.assembly
          method_landfall = lmo_operation.methods.landfall
        else:
          method_load_out = None
          method_transportation = None
          method_piling = None
          method_burial = None
          method_assembly = None
          method_landfall = None

        Main_funcs.operations[op_idx].update_methods_requirements_user(
            method_load_out=method_load_out,
            method_transportation=method_transportation,
            method_piling=method_piling,
            method_burial=method_burial,
            method_assembly=method_assembly,
            method_landfall=method_landfall,
            requirement_rov=requirement_rov,
            requirement_divers=requirement_divers,
            requirement_prev_proj=requirement_previous_projects,
            requirement_terminal_area=requirement_terminal_area,
            requirement_terminal_load=requirement_terminal_load,
            requirement_terminal_crane=requirement_terminal_crane,
            requirement_connect_to_elect=requirement_connect_to_electrical,
            port_max_dist=port_maximum_distance
        )

      # Call Core functionalities
      try:
        operations = Main_funcs.operations
        operations = Core_funcs.run_feasibility_functions(
            operations=operations,
            quantile_vessels=lmo_inputs.quantile_vessels,
            safety_factor=lmo_inputs.safety_factor
        )
        operations = Core_funcs.run_matchability_functions(operations)
        operations = Core_funcs.define_activities(operations)
        operations = Core_funcs.delete_combinations(operations)
        operations = Core_funcs.check_workabilities(operations)
        operations = Core_funcs.check_startabilities(operations)
        operations = Core_funcs.define_durations_waitings_timestep(operations)
        operations = Core_funcs.statistical_analysis(operations)
        operations = Core_funcs.get_vessels_cost(operations)
        operations = Core_funcs.select_combination(operations)
      except AssertionError as err:
        return str(err)
      except Exception as err:
        return str(err)

      Main_funcs.operations = operations

    elif lmo_phase.name == 'decommissioning':
      installation_phase = Phase.query.filter_by(
          study_id=lmoid,
          name='installation'
      ).first()
      if installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'

      if len(installation_phase.operations) == 0:
        return 'Installation phase operations not found in database.'

      for lmo_operation in lmo_operations:
        op_deco_name = lmo_operation.name
        op_deco_name_short = op_deco_name.replace('removal', '')
        for lmo_inst_operation in installation_phase.operations:
          op_inst_name = lmo_inst_operation.name
          op_inst_name_short = op_inst_name.replace('installation', '')
          if op_deco_name_short.lower() == op_inst_name_short.lower():
            break

        lmo_operation.vec = lmo_inst_operation.vec
        lmo_operation.vessels = lmo_inst_operation.vessels
        lmo_operation.equipment = lmo_inst_operation.equipment
        lmo_operation.terminal = lmo_inst_operation.terminal

        lmo_operation.durations = lmo_inst_operation.durations
        lmo_operation.waitings = lmo_inst_operation.waitings
        lmo_operation.consumption = lmo_inst_operation.consumption
        lmo_operation.costs = lmo_inst_operation.costs
        lmo_operation.logistic_solution = lmo_inst_operation.logistic_solution

        # Update operation in the database
        try:
          db.session.commit()
        except IntegrityError:
          db.session.rollback()
          return 'Unknown error.'

        del lmo_inst_operation

      # Update phase in the database
      lmo_phase.computed = True
      try:
        db.session.commit()
        return
      except IntegrityError:
        db.session.rollback()
        return 'Unknown error.'

    else:
      return 'Phase %s not recognized' % lmo_phase.name

  if lmo_phase.name == 'installation':
    Phase_funcs = Inst_funcs
  elif lmo_phase.name == 'maintenance':
    Phase_funcs = Main_funcs
  elif lmo_phase.name == 'decommissioning':
    Phase_funcs = Deco_funcs

  # Convert operations to JSON format
  for op in Phase_funcs.operations:
    # Get this operation from database as Operation instance
    lmo_operation = Operation.query.filter_by(
        phase_id=phase_id,
        id_external=op.id
    ).first()
    # Update this Operation instance
    #   Activities
    if lmo_study.complexity != 'low':
      for activity in op.activities:
        activity_data = {
            "operation_id": lmo_operation.id,
            "id_external": activity.id,
            "name": activity.name,
            "duration": activity.duration,
            "hs": activity.hs,
            "tp": activity.tp,
            "ws": activity.ws,
            "cs": activity.cs,
            "light": activity.light,
            "location": activity.location
        }
        lmo_activity_schema = ActivitySchema()
        lmo_activity = lmo_activity_schema.load(activity_data)
        try:
          db.session.add(lmo_activity)
          db.session.commit()
        except IntegrityError:
          db.session.rollback()
          return 'Error creating activity: %s' % activity_data["id_external"]

    #   Other
    lmo_operation.vec = op.vec
    lmo_operation.terminal = op.terminal
    try:
      # For cpx 2 and 3
      lmo_operation.vessels = ', '.join(op.vessels)
    except AttributeError:
      # For cpx 1
      try:
        lmo_operation.vessels = op.vessel
      except TypeError:
        lmo_operation.vessels = ''
    lmo_operation.equipment = ', '.join(op.equipment)
    lmo_operation.consumption = op.consumption

    lmo_operation.durations = op.durations
    lmo_operation.waitings = op.waitings
    lmo_operation.costs = op.costs
    # Convert terminal distance from numpy.Int64 to int
    if 'repair at port' in op.description:
      op.logistic_solution["terminal_dist"] = 0
    else:
      op.logistic_solution["terminal_dist"] = int(op.logistic_solution["terminal_dist"])
    lmo_operation.logistic_solution = op.logistic_solution

    # Update operation in the database
    try:
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return 'Unknown error.'

    del lmo_operation

  # Update phase in the database
  lmo_phase.computed = True
  try:
    db.session.commit()
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_results(phase_id):
  """
  Delete Results from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    phase_id (int): The ID of the Phase

  Returns:
    str: Success message if study is deleted; Error message if it runs into an error trying to delete operation results
  """
  lmo_operations = Operation.query.filter_by(phase_id=phase_id).all()
  for lmo_operation in lmo_operations:
    lmo_operation.durations = {}
    lmo_operation.waitings = {}
    lmo_operation.costs = {}
    lmo_operation.logistic_solution = {}

    try:
      db.session.query(Activity).filter_by(operation_id=lmo_operation.id).delete()
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return 'Error deleteing results from operation %s' % lmo_operation.id_external

  # Update phase in the database
  lmo_phase = Phase.query.get(phase_id)
  lmo_phase.computed = False
  try:
    db.session.commit()
    return 'Results deleted'
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def get_plan(lmoid, phase_id):
  try:
    df_vessels = get_catalog_vessels()
  except ConnectionError:
    path_catalogue_vessels = os.path.join(os.getcwd(), 'catalogues', 'Vessels.csv')
    df_vessels = pd.read_csv(path_catalogue_vessels, sep=',')
  try:
    df_terminals = get_catalog_terminals()
  except ConnectionError:
    path_catalogue_terminals = os.path.join(os.getcwd(), 'catalogues', 'Terminals.csv')
    df_terminals = pd.read_csv(path_catalogue_terminals, sep=',', encoding="ISO-8859-1")
  try:
    df_burial = get_catalog_equip_burial()
  except ConnectionError:
    path_catalogue_burial = os.path.join(os.getcwd(), 'catalogues', 'Equip_burial.csv')
    df_burial = pd.read_csv(path_catalogue_burial, sep=',')
  try:
    df_divers = get_catalog_divers()
  except ConnectionError:
    path_catalogue_divers = os.path.join(os.getcwd(), 'catalogues', 'Equip_divers.csv')
    df_divers = pd.read_csv(path_catalogue_divers, sep=',')
  try:
    df_piling = get_catalog_equip_piling()
  except ConnectionError:
    path_catalogue_piling = os.path.join(os.getcwd(), 'catalogues', 'Equip_piling.csv')
    df_piling = pd.read_csv(path_catalogue_piling, sep=',')
  try:
    df_protect = get_catalog_equip_protect()
  except ConnectionError:
    path_catalogue_protect = os.path.join(os.getcwd(), 'catalogues', 'Equip_protect.csv')
    df_protect = pd.read_csv(path_catalogue_protect, sep=',')
  try:
    df_rov = get_catalog_rovs()
  except ConnectionError:
    path_catalogue_rovs = os.path.join(os.getcwd(), 'catalogues', 'Equip_rov.csv')
    df_rov = pd.read_csv(path_catalogue_rovs, sep=',')

  lmo_study = LMOStudy.query.get(lmoid)
  study_inputs = Inputs.query.filter_by(study_id=lmoid).first()
  lmo_phase = Phase.query.get(phase_id)

  complexity = lmo_study.complexity

  # Prepare a dictionary to call Business Logic functions
  lmo_operations = Operation.query.filter_by(phase_id=phase_id).all()
  data = _prepare_data(lmo_operations)

  if complexity.lower() == 'low':
    if lmo_phase.name == 'installation':
      start_date = study_inputs.installation_date
      period = study_inputs.period
      # Import installation and core functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          start_date=start_date,
          period=period
      )
      data = Inst_funcs.define_operations_dates(data)

    elif lmo_phase.name == 'maintenance':
      start_date = study_inputs.maintenance_date
      project_life = study_inputs.project_life
      period = study_inputs.period

      hierarchy_et_json = None
      hierarchy_ed_json = None
      hierarchy_sk_json = None
      lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
      if lmo_module_et is not None:
        hierarchy_et_json = lmo_module_et.hierarchy
      lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
      if lmo_module_ed is not None:
        hierarchy_ed_json = lmo_module_ed.hierarchy
      lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
      if lmo_module_sk is not None:
        hierarchy_sk_json = lmo_module_sk.hierarchy

      # Convert hierarchies to pandas DataFrame
      hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
      hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
      hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)
      # Import maintenance and core functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          start_date=start_date,
          t_life=project_life,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df
      )

      data = Main_funcs.define_operations_dates(data)
      PTO_dowtime = Main_funcs.get_downtime_cor(data, lmo_study.objects + lmo_study.cables)
      device_downtime = Main_funcs.get_downtime_per_device(PTO_dowtime)

    elif lmo_phase.name == 'decommissioning':
      # Get commissioning date - installation finish date
      lmo_installation_phase = Phase.query.filter_by(
          study_id=lmo_study.id,
          name='installation'
      ).first()
      if lmo_installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'
      if len(lmo_installation_phase.operations) == 0:
        return 'Installation phase operations not found in database.'
      if not lmo_installation_phase.operations[0].durations:
        return 'Installation phase calculations needed for decommissioning plan.'

      # Get installation plan
      inst_plan = get_plan(lmo_study.id, lmo_installation_phase.id)

      inst_end_date = inst_plan["date_end"][-1]
      inst_end_date = inst_end_date[:10]
      inst_end_datetime = datetime.strptime(inst_end_date, '%Y/%m/%d')

      project_life = study_inputs.project_life * 8760
      start_date_datetime = inst_end_datetime + timedelta(hours=project_life)
      start_date = start_date_datetime.strftime('%Y/%m/%d')

      period = study_inputs.period
      # Import installation and core functionalities
      Deco_funcs = business.Decommissioning.get_complexity(
          complexity=complexity,
          start_date=start_date,
          period=period
      )
      data = Deco_funcs.define_operations_dates(data)

  # elif complexity.lower() == 'med':
  #   if lmo_phase.name == 'installation':
  #     pass
  #   if lmo_phase.name == 'maintenance':
  #     pass
  #   if lmo_phase.name == 'descommissioning':
  #     pass

  elif complexity.lower() == 'high' or complexity.lower() == 'med':
    if lmo_phase.name == 'installation':
      start_date = study_inputs.installation_date
      period = study_inputs.period
      # Import installation and core functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          start_date=start_date,
          period=period
      )
      data = Inst_funcs.define_operations_dates(data)

    elif lmo_phase.name == 'maintenance':
      start_date = study_inputs.maintenance_date
      project_life = study_inputs.project_life
      period = study_inputs.period

      hierarchy_et_json = None
      hierarchy_ed_json = None
      hierarchy_sk_json = None
      lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
      if lmo_module_et is not None:
        hierarchy_et_json = lmo_module_et.hierarchy
      lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
      if lmo_module_ed is not None:
        hierarchy_ed_json = lmo_module_ed.hierarchy
      lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
      if lmo_module_sk is not None:
        hierarchy_sk_json = lmo_module_sk.hierarchy

      # Convert hierarchies to pandas DataFrame
      hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
      hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
      hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)

      # Import maintenance and core functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          start_date=start_date,
          t_life=project_life,
          hierarchy_et=hierarchy_et_df,
          hierarchy_ed=hierarchy_ed_df,
          hierarchy_sk=hierarchy_sk_df
      )

      data = Main_funcs.define_operations_dates(data)
      PTO_dowtime = Main_funcs.get_downtime_cor(data, lmo_study.objects + lmo_study.cables)
      device_downtime = Main_funcs.get_downtime_per_device(PTO_dowtime)

    elif lmo_phase.name == 'decommissioning':
      # Get commissioning date - installation finish date
      lmo_installation_phase = Phase.query.filter_by(
          study_id=lmo_study.id,
          name='installation'
      ).first()
      if lmo_installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'
      if len(lmo_installation_phase.operations) == 0:
        return 'Installation phase operations not found in database.'
      if not lmo_installation_phase.operations[0].durations:
        return 'Installation phase calculations needed for decommissioning plan.'

      # Get installation plan
      inst_plan = get_plan(lmo_study.id, lmo_installation_phase.id)

      inst_end_date = inst_plan["date_end"][-1]
      inst_end_date = inst_end_date[:10]
      inst_end_datetime = datetime.strptime(inst_end_date, '%Y/%m/%d')

      project_life = study_inputs.project_life * 8760
      start_date_datetime = inst_end_datetime + timedelta(hours=project_life)
      start_date = start_date_datetime.strftime('%Y/%m/%d')

      period = study_inputs.period
      # Import installation and core functionalities
      Deco_funcs = business.Decommissioning.get_complexity(
          complexity=complexity,
          start_date=start_date,
          period=period
      )
      data = Deco_funcs.define_operations_dates(data)

  # Add more data to "data"
  data_op_ids = [op_key for op_key, _ in data.items()]
  for lmo_operation in lmo_operations:
    component_ids = ''
    for lmo_obj in lmo_operation.objects:
      component_ids = component_ids + lmo_obj.id_external + ', '
    for lmo_cbl in lmo_operation.cables:
      component_ids = component_ids + lmo_cbl.id_external + ', '
    if len(component_ids) > 0:
      component_ids = component_ids[:-2]

    op_id = lmo_operation.id_external

    # Vessel IDs to vessel names
    list_vessel_ids = lmo_operation.vessels.replace(' ','').split(',')
    list_vessel_names = []
    for vessel_id in list_vessel_ids:
      ds_vessel = df_vessels[df_vessels['id_name'].str.lower() == vessel_id.lower()].iloc[0, :]
      list_vessel_names.append(ds_vessel['vessel_type'])
    vessel_names = ', '.join(list_vessel_names)

    # Equipment IDs to equipment names
    list_equipment_ids = lmo_operation.equipment.replace(' ','').split(',')
    list_equipment_names = []
    for equip_id in list_equipment_ids:
      df_equip = df_burial[df_burial['id_name'].str.lower() == equip_id.lower()]
      if df_equip.shape[0] > 0:
        ds_equip = df_equip.iloc[0, :]
        list_equipment_names.append('burial ' + ds_equip['type'])
        continue
      df_equip = df_divers[df_divers['id_name'].str.lower() == equip_id.lower()]
      if df_equip.shape[0] > 0:
        ds_equip = df_equip.iloc[0, :]
        list_equipment_names.append('divers')
        continue
      df_equip = df_piling[df_piling['id_name'].str.lower() == equip_id.lower()]
      if df_equip.shape[0] > 0:
        ds_equip = df_equip.iloc[0, :]
        list_equipment_names.append('piling ' + ds_equip['type'])
        continue
      df_equip = df_protect[df_protect['id_name'].str.lower() == equip_id.lower()]
      if df_equip.shape[0] > 0:
        ds_equip = df_equip.iloc[0, :]
        list_equipment_names.append('protect ' + ds_equip['type'])
        continue
      df_equip = df_rov[df_rov['id_name'].str.lower() == equip_id.lower()]
      if df_equip.shape[0] > 0:
        ds_equip = df_equip.iloc[0, :]
        list_equipment_names.append('ROV ' + ds_equip['_class'])
        continue
    equip_names = ', '.join(list_equipment_names)

    # Terminal ID to terminal name
    terminal_id = lmo_operation.terminal
    ds_terminal = df_terminals[df_terminals['id_name'].str.lower() == terminal_id.lower()].iloc[0, :]
    terminal_name = ds_terminal['port_name'] + ' - ' + ds_terminal['terminal_name']

    if lmo_operation.multiple_operations is True:
      idx = 0
      if len(lmo_operation.dates_maystart_delta["dates_maystart_delta"]) == 0:
        del data[op_id + '_' + str(idx)]
        continue

      for start_delta in lmo_operation.dates_maystart_delta["dates_maystart_delta"]:
        if op_id + '_' + str(idx) not in data_op_ids:
          idx += 1
          continue
        data[op_id + '_' + str(idx)]["component_ids"] = component_ids
        data[op_id + '_' + str(idx)]["consumption"] = lmo_operation.consumption
        data[op_id + '_' + str(idx)]["vec"] = lmo_operation.vec
        data[op_id + '_' + str(idx)]["vessels"] = vessel_names
        data[op_id + '_' + str(idx)]["equipment"] = equip_names
        data[op_id + '_' + str(idx)]["terminal"] = terminal_name
        data[op_id + '_' + str(idx)]["activities"] = [
            lmo_act.id_external for lmo_act in lmo_operation.activities
        ]
        idx += 1
    else:
      data[op_id]["component_ids"] = component_ids
      data[op_id]["consumption"] = lmo_operation.consumption
      data[op_id]["vec"] = lmo_operation.vec
      data[op_id]["vessels"] = vessel_names
      data[op_id]["equipment"] = equip_names
      data[op_id]["terminal"] = terminal_name
      data[op_id]["activities"] = [
          lmo_act.id_external for lmo_act in lmo_operation.activities
      ]

  if complexity.lower() == 'low':
    if lmo_phase.name == 'installation':
      period = study_inputs.period
      # Import installation and core functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          period=period
      )
      df_phase_plan = Inst_funcs.build_output_table(data)

    elif lmo_phase.name == 'maintenance':
      period = study_inputs.period
      start_date = study_inputs.maintenance_date
      # Import maintenance and core functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          period=period,
          start_date=start_date
      )
      df_phase_plan = Main_funcs.build_output_table(data, device_downtime)

    elif lmo_phase.name == 'decommissioning':
      period = study_inputs.period
      # Import decommissioning and core functionalities
      Deco_funcs = business.Decommissioning.get_complexity(
          complexity=complexity,
          period=period
      )
      df_phase_plan = Deco_funcs.build_output_table(data)

  # elif complexity.lower() == 'med':
  #   if lmo_phase.name == 'installation':
  #     pass
  #   if lmo_phase.name == 'maintenance':
  #     pass
  #   if lmo_phase.name == 'descommissioning':
  #     pass

  elif complexity.lower() == 'high' or complexity.lower() == 'med':
    if lmo_phase.name == 'installation':
      period = study_inputs.period
      # Import installation functionalities
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity,
          period=period
      )
      df_phase_plan = Inst_funcs.build_output_table(data)

    elif lmo_phase.name == 'maintenance':
      period = study_inputs.period
      start_date = study_inputs.maintenance_date
      # Import maintenance functionalities
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          period=period,
          start_date=start_date
      )
      df_phase_plan = Main_funcs.build_output_table(data, device_downtime)

    elif lmo_phase.name == 'decommissioning':
      period = study_inputs.period
      # Import decommissioning functionalities
      Deco_funcs = business.Decommissioning.get_complexity(
          complexity=complexity,
          period=period
      )
      df_phase_plan = Deco_funcs.build_output_table(data)

  phase_plan_dict = df_phase_plan.to_dict('list')
  for key, item in phase_plan_dict.items():
    item = [value if value != 'NA' else None for value in item]
    phase_plan_dict[key] = item
  if 'fail_date' in phase_plan_dict:
    for item_idx, item in enumerate(phase_plan_dict["fail_date"]):
      if item is None:
        phase_plan_dict["fail_date"][item_idx] = "none"

  return phase_plan_dict


def get_downtime(lmoid, phase_id):

  lmo_study = LMOStudy.query.get(lmoid)
  study_inputs = lmo_study.inputs
  lmo_phase = Phase.query.get(phase_id)

  complexity = lmo_study.complexity

  # Prepare a dictionary to call Business Logic functions
  lmo_operations = Operation.query.filter_by(phase_id=phase_id).all()

  data = _prepare_data(lmo_operations)
  for lmo_operation in lmo_operations:
    if lmo_operation.multiple_operations is True:
      data[lmo_operation.id_external + '_0'] = {
          "name": lmo_operation.name,
          "description": lmo_operation.description,
          "durations": lmo_operation.durations,
          "waitings": lmo_operation.waitings,
          "costs": lmo_operation.costs,
          "may_start": 1000000
      }
      data[lmo_operation.id_external + '_0']["objects"] = []
      for lmo_object in lmo_operation.objects:
        try:
          failure_rates = lmo_object.failure_rates.replace(' ', '')
          failure_rates = failure_rates.replace('(', '').replace(')', '')
          failure_rates = failure_rates.split(',')
        except AttributeError:
          failure_rates = None
        data[lmo_operation.id_external + '_0']["objects"].append(
            {
                "id": lmo_object.id_external,
                "name": lmo_object.name,
                "type": lmo_object.type_,
                "failure_rates": failure_rates,
                "parent": lmo_object.parent,
                "cost": lmo_object.component_cost
            }
        )
      data[lmo_operation.id_external + '_0']["cables"] = []
      for lmo_cable in lmo_operation.cables:
        try:
          failure_rates = lmo_cable.failure_rates.replace(' ','')
          failure_rates = failure_rates.replace('(','').replace(')','')
          failure_rates = failure_rates.split(',')
        except AttributeError:
          failure_rates = None
        data[lmo_operation.id_external + '_0']["cables"].append(
            {
                "id": lmo_cable.id_external,
                "name": lmo_cable.name,
                "type": lmo_cable.type_,
                "failure_rates": failure_rates,
                "parent": lmo_object.parent,
                "cost": lmo_object.component_cost
            }
        )

      idx = 0
      for start_delta in lmo_operation.dates_maystart_delta["dates_maystart_delta"]:
        data[lmo_operation.id_external + '_' + str(idx)] = deepcopy(
            data[lmo_operation.id_external + '_0']
        )
        data[lmo_operation.id_external + '_' + str(idx)]["may_start"] = start_delta
        idx += 1
    else:
      data[lmo_operation.id_external] = {
          "name": lmo_operation.name,
          "description": lmo_operation.description,
          "durations": lmo_operation.durations,
          "waitings": lmo_operation.waitings,
          "costs": lmo_operation.costs
      }

  if complexity.lower() == 'low':
    start_date = study_inputs.maintenance_date
    project_life = study_inputs.project_life
    period = study_inputs.period

    hierarchy_et_json = None
    hierarchy_ed_json = None
    hierarchy_sk_json = None
    lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
    if lmo_module_et is not None:
      hierarchy_et_json = lmo_module_et.hierarchy
    lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if lmo_module_ed is not None:
      hierarchy_ed_json = lmo_module_ed.hierarchy
    lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
    if lmo_module_sk is not None:
      hierarchy_sk_json = lmo_module_sk.hierarchy

    # Convert hierarchies to pandas DataFrame
    hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
    hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
    hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)
    # Import maintenance and core functionalities
    Main_funcs = business.Maintenance.get_complexity(
        complexity=complexity,
        start_date=start_date,
        t_life=project_life,
        hierarchy_et=hierarchy_et_df,
        hierarchy_ed=hierarchy_ed_df,
        hierarchy_sk=hierarchy_sk_df
    )

    data = Main_funcs.define_operations_dates(data)
    PTO_dowtime = Main_funcs.get_downtime_cor(data, lmo_study.objects + lmo_study.cables)
    device_downtime = Main_funcs.get_downtime_per_device(PTO_dowtime)

  # elif complexity.lower() == 'med':
  #   pass

  elif complexity.lower() == 'high' or complexity.lower() == 'med':
    start_date = study_inputs.maintenance_date
    project_life = study_inputs.project_life
    period = study_inputs.period

    hierarchy_et_json = None
    hierarchy_ed_json = None
    hierarchy_sk_json = None
    lmo_module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
    if lmo_module_et is not None:
      hierarchy_et_json = lmo_module_et.hierarchy
    lmo_module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if lmo_module_ed is not None:
      hierarchy_ed_json = lmo_module_ed.hierarchy
    lmo_module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
    if lmo_module_sk is not None:
      hierarchy_sk_json = lmo_module_sk.hierarchy

    # Convert hierarchies to pandas DataFrame
    hierarchy_et_df = pd.DataFrame(hierarchy_et_json)
    hierarchy_ed_df = pd.DataFrame(hierarchy_ed_json)
    hierarchy_sk_df = pd.DataFrame(hierarchy_sk_json)

    # Import maintenance and core functionalities
    Main_funcs = business.Maintenance.get_complexity(
        complexity=complexity,
        start_date=start_date,
        t_life=project_life,
        hierarchy_et=hierarchy_et_df,
        hierarchy_ed=hierarchy_ed_df,
        hierarchy_sk=hierarchy_sk_df
    )

    data = Main_funcs.define_operations_dates(data)
    PTO_dowtime = Main_funcs.get_downtime_cor(data, lmo_study.objects + lmo_study.cables)
    device_downtime = Main_funcs.get_downtime_per_device(PTO_dowtime)

  return device_downtime


def get_bom(lmoid, phase_id):
  # Get Installation plan
  inst_plan = get_plan(lmoid, phase_id)

  # BoM raw
  bom = {
      "id": [
          'Tot_Inst_Dev', 
          'Tot_Inst_Anc',
          'Tot_Inst_Cable',
          'Tot_Inst_Other'
      ],
      "name": [
          'Total cost of installation of devices',
          'Total cost of installation of Anchors',
          'Total cost of installation of cables',
          'Total other costs'
      ],
      "qnt": ['-', '-', '-', '-'],
      "uom": ['-', '-', '-', '-'],
      "unit_cost": ['-', '-', '-', '-'],
      "total_cost": [0, 0, 0, 0]
  }

  for idx, tech in enumerate(inst_plan["tech_group"]):
    if tech == 'device':
      bom["total_cost"][0] += inst_plan["cost"][idx]
    elif tech == 'station keeping':
      bom["total_cost"][1] += inst_plan["cost"][idx]
    elif tech == 'electrical':
      bom["total_cost"][2] += inst_plan["cost"][idx]
    else:
      bom["total_cost"][3] += inst_plan["cost"][idx]

  return bom


def _prepare_data(lmo_operations):
  data = {}
  for lmo_operation in lmo_operations:
    if lmo_operation.multiple_operations is True:
      data[lmo_operation.id_external + '_0'] = {
          "name": lmo_operation.name,
          "description": lmo_operation.description,
          "durations": lmo_operation.durations,
          "waitings": lmo_operation.waitings,
          "costs": lmo_operation.costs,
          "may_start": 1000000
      }
      data[lmo_operation.id_external + '_0']["objects"] = []
      for lmo_object in lmo_operation.objects:
        try:
          failure_rates = lmo_object.failure_rates.replace(' ', '')
          failure_rates = failure_rates.replace('(', '').replace(')', '')
          failure_rates = failure_rates.split(',')
        except AttributeError:
          failure_rates = None
        data[lmo_operation.id_external + '_0']["objects"].append(
            {
                "id": lmo_object.id_external,
                "name": lmo_object.name,
                "type": lmo_object.type_,
                "failure_rates": failure_rates,
                "parent": lmo_object.parent,
                "cost": lmo_object.component_cost
            }
        )
      data[lmo_operation.id_external + '_0']["cables"] = []
      for lmo_cable in lmo_operation.cables:
        try:
          failure_rates = lmo_cable.failure_rates.replace(' ','')
          failure_rates = failure_rates.replace('(','').replace(')','')
          failure_rates = failure_rates.split(',')
        except AttributeError:
          failure_rates = None
        data[lmo_operation.id_external + '_0']["cables"].append(
            {
                "id": lmo_cable.id_external,
                "name": lmo_cable.name,
                "type": lmo_cable.type_,
                "failure_rates": failure_rates,
                "parent": lmo_object.parent,
                "cost": lmo_object.component_cost
            }
        )

      idx = 0
      for start_delta in lmo_operation.dates_maystart_delta["dates_maystart_delta"]:
        data[lmo_operation.id_external + '_' + str(idx)] = deepcopy(
            data[lmo_operation.id_external + '_0']
        )
        data[lmo_operation.id_external + '_' + str(idx)]["may_start"] = start_delta
        idx += 1
    else:
      data[lmo_operation.id_external] = {
          "name": lmo_operation.name,
          "description": lmo_operation.description,
          "durations": lmo_operation.durations,
          "waitings": lmo_operation.waitings,
          "costs": lmo_operation.costs
      }

  return data
