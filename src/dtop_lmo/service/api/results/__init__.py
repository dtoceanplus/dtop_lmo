# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
from sqlalchemy.exc import IntegrityError
from flask import Blueprint, jsonify
import pandas as pd
import numpy as np
import json
from datetime import datetime
import time

from ....service import db

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ..studies.lmo_studies import update_status

from ..operations.lmo_operations import delete_phase_operations
from .lmo_results import (
    calculate_phase_results, delete_results,
    get_plan, get_downtime, get_bom
)
from dtop_lmo.service.models import LMOStudy, Module, Phase, Operation

bp = Blueprint('api_results', __name__)


@bp.route('/phases/<phase>/calculate', methods=['POST'])
def post_phase_calculate(lmoid, phase):
  # """
  # Flask blueprint route to preform calculations related to a given phase.

  # Calculates the operations required to install objects and cables given by other modules
  # considering site metocean data and user inputs

  # Calls the :meth:`~dtop_lmo.service.api.results.calculate_phase_results()` method.
  # Calls the :meth:`~dtop_lmo.service.api.results.delete_phase_operations()` method.
  # Returns HTTP 400 error if this phase is not defined for this this study;
  # Returns HTTP 400 error if there is any error during the calculation process;
  # Returns 404 error if study ID does not exist.

  # Args:
  #   lmoid (int): The ID of the LMO Study
  #   phase (str): The name of the logistic phase

  # Returns:
  #   dict: the serialised response containing a success message.
  # """
  # try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    if lmo_phase.operations is None:
      return bad_request('No operations defined for this phase yet.')

    # Flag that phase calculations are on going
    lmo_phase.calculating = True
    try:
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return bad_request('Error turning calculationg flag to True')

    # Perform calculations
    time_st = time.time()
    phase_results = calculate_phase_results(lmoid, lmo_phase.id)
    calculation_time = time.time() - time_st

    lmo_phase.calculating = False
    try:
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return bad_request('Error turning calculationg flag to False')

    if type(phase_results) is str:
      return bad_request(phase_results)
    # Update LMO study status
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    update_status(lmoid)

    result = {
        "message": "results calculated for %s phase" % phase,
        "computation_time": "%.3f seconds" % calculation_time
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  # except Exception as err:
  #   lmo_phase.calculating = False
  #   try:
  #     db.session.commit()
  #   except IntegrityError:
  #     db.session.rollback()
  #     return bad_request('Error turning calculationg flag to False')

  #   return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/calculate', methods=['DELETE'])
def delete_phase_calculate(lmoid, phase):
  """
  Flask blueprint route for deleting calculated results from a phase.

  Calls the :meth:`~dtop_lmo.service.api.results.delete_results()` method.
  Returns HTTP 404 error if LMO study id was not found.
  Returns HTTP 400 error if phase was not defined.
  Returns HTTP 400 error if results were not calculated yet.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted
    phase (str): The name of the logistic phase

  Returns:
    dict: serialised response containing 'Results deleted' message; not found error otherwise
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)

    data = delete_results(lmo_phase.id)
    if data.lower() == 'results deleted':
      # Update LMO study status
      lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
      update_status(lmoid)

      response = {
          'study_id': lmoid,
          'phase': phase,
          'message': 'Results deleted.'
      }
      return jsonify(response)
    else:
      return not_found(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/plan', methods=['GET'])
def get_phase_plan(lmoid, phase):
  """
  Flask blueprint route to get the plan for a given phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a plan.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the plan for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('This phase is not defined for this study.')
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    phase_plan = get_plan(lmo_study.id, lmo_phase.id)
    if type(phase_plan) is str:
      return bad_request(phase_plan)

    result = phase_plan

    response = jsonify(result)
    response.status_code = 200
    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/cost', methods=['GET'])
def get_phase_cost(lmoid, phase):
  """
  Flask blueprint route to get total cost of a given phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a plan.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the cost for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    phase_plan = get_plan(lmo_study.id, lmo_phase.id)
    if type(phase_plan) is str:
      return bad_request(phase_plan)

    phase_plan_df = pd.DataFrame(phase_plan)
    phase_operations_cost = phase_plan_df['cost'].sum()
    phase_parts_costs_list = phase_plan_df['replacement_costs'].to_list()
    phase_parts_costs_floats = [float(cost)
                                for cost in phase_parts_costs_list
                                if type(cost) is float]
    phase_parts_cost = sum(phase_parts_costs_floats)
    phase_total_cost = phase_operations_cost + phase_parts_cost

    result = {
        "value": phase_total_cost,
        "unit": "euro"
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/logistic', methods=['GET'])
def get_phase_logistic(lmoid, phase):
  """
  Flask blueprint route to get the logistic solution of a given phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a plan.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the logistic solution for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    phase_logistic_dict = {
        "terminal": [],
        "vec": [],
        "vessels": [],
        "equipment": [],
        "terminal_dist": None
    }
    for operation in lmo_phase.operations:
      op_logistic_dict = operation.logistic_solution

      phase_logistic_dict["terminal"] += op_logistic_dict["terminal"]
      phase_logistic_dict["vec"] += op_logistic_dict["vec"]
      phase_logistic_dict["vessels"] += op_logistic_dict["vessels"]
      phase_logistic_dict["equipment"] += op_logistic_dict["equipment"]

    return jsonify(phase_logistic_dict)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/consumption', methods=['GET'])
def get_phase_consumption(lmoid, phase):
  """
  Flask blueprint route to get the total fuel consumption of a given phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a plan.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the total fuel consumption for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    phase_plan = get_plan(lmo_study.id, lmo_phase.id)
    if type(phase_plan) is str:
      return bad_request(phase_plan)

    phase_plan_df = pd.DataFrame(phase_plan)

    total_consumption = 0
    for idx, row in phase_plan_df.iterrows():
      op_date_start_str = row['date_start']
      op_date_end_str = row['date_end']

      op_date_start_str = op_date_start_str[:10]
      op_date_end_str = op_date_end_str[:10]

      date_start = datetime.strptime(op_date_start_str, '%Y/%m/%d')
      date_end = datetime.strptime(op_date_end_str, '%Y/%m/%d')

      delta = date_end - date_start

      for operation in lmo_phase.operations:
        if operation.id_external.lower() in row["operation_id"].lower():
          consumption_daily = operation.consumption
          break

      total_consumption += consumption_daily * delta.days
      del consumption_daily

    result = {
        "value": total_consumption,
        "unit": "ton"
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/duration', methods=['GET'])
def get_phase_duration(lmoid, phase):
  """
  Flask blueprint route to get the total duration of a given phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a plan.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the total duration for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    phase_plan = get_plan(lmo_study.id, lmo_phase.id)
    if type(phase_plan) is str:
      return bad_request(phase_plan)

    total_duration = 0
    for duration in phase_plan["duration_total"]:
      total_duration += duration

    # Get total installed power
    et_module = Module.query.filter_by(study_id=lmoid, module='et').first()
    if et_module is None:
      return bad_request('ET inputs not defined for this study.')
    device_rated_power = et_module.design["devices"][0]["Dev_rated_power"]["value"]

    rated_power = 0
    for object_ in lmo_study.objects:
      if 'device' in object_.name.lower():
        rated_power += float(device_rated_power)

    if phase.lower() == 'installation' or phase.lower() == 'decommissioning':
      duration_value = total_duration / rated_power
      duration_unit = 'hour/kW'
    if phase.lower() == 'maintenance':
      # Get project lifetime in years
      lifetime = lmo_study.inputs.project_life
      duration_value = total_duration / rated_power / lifetime
      duration_unit = 'hour/kW/year'
    result = {
        "value": duration_value,
        "unit": duration_unit
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/maintenance/downtime', methods=['GET'])
def get_phase_downtime(lmoid):
  """
  Flask blueprint route to get the downtime of maintenance logistic phase.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns HTTP 400 error if this phase does not have a downtime.

  Args:
    lmoid (int): The ID of the LMO Study

  Returns:
    dict: the serialised response containing the downtime of maintenance.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name='maintenance').first()
    if lmo_phase is None:
      return bad_request('maintenance phase not defined for this LMO study')
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')

    dowtime_per_device = get_downtime(lmoid, lmo_phase.id)
    dowtime_array = [
        {"device_id": device_id, "downtime_table": item.to_dict('list')}
        for device_id, item in dowtime_per_device.items()
    ]

    result = {
        "project_life": lmo_study.inputs.project_life,
        "n_devices": len(dowtime_per_device),
        "downtime": dowtime_array
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/bom', methods=['GET'])
def get_study_bom(lmoid):
  """
  Flask blueprint route to get the Bill of Materials of a given study.

  Returns HTTP 404 error if study ID does not exist;
  Returns HTTP 400 error if installation phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for installation;
  Returns HTTP 400 error if installation operations do not have costs.

  Args:
    lmoid (int): The ID of the LMO Study

  Returns:
    dict: the serialised response containing the total duration for this phase.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name='installation').first()
    if lmo_phase is None:
      return bad_request('Installation phase not defined for this LMO study')
    if not lmo_phase.operations:
      return bad_request('The are no operations defined for this phase.')
    if not lmo_phase.operations[0].costs:
      return bad_request('The are no costs associated to this phase.')

    bom = get_bom(lmo_study.id, lmo_phase.id)
    if type(bom) is str:
      return bad_request(bom)

    result = bom
    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
