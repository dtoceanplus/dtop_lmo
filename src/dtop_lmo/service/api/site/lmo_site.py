# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
import pylab
import datetime
import pandas as pd

from ....service import db

from ...models import Site
from ...schemas import SiteSchema


def create_site(lmoid, data):
  """
  Create a new Site and add it to the local storage database.

  Args:
    lmoid (int): LMO study ID
    data (dict): dictionary with keys required to create the Site object.

  Returns:
    dict: the newly created Site instance
  """
  try:
    module_sc_data = data["module_sc"]
  except KeyError:
    return 'Could not find data from Site Characterization'
  try:
    sc_timeseries = module_sc_data["point"]["time_series"]
  except KeyError:
    return 'Could not find metocean data in Site Characterization module'
  try:
    site_name = 'site_id_' + str(module_sc_data["site_id"])
  except KeyError:
    return 'Could not find site\'s name in Site Characterization module'

  try:
    # Get SC data from request body (stand-alone mode)
    sc_bathymetry = module_sc_data["farm"]["direct_values"]["bathymetry"]
  except KeyError:
    sc_bathymetry = None
  try:
    # Get SC data from request body (stand-alone mode)
    sc_seabed_type = module_sc_data["farm"]["direct_values"]["seabed_type"]
  except KeyError:
    sc_seabed_type = None
  try:
    # Get SC data from request body (stand-alone mode)
    site_info = module_sc_data["farm"]["info"]
  except KeyError:
    site_info = None

  del data["module_sc"]

  lat_float = [float(value) for value in site_info["latitude"]]
  latitude = sum(lat_float) / len(lat_float)
  lon_float = [float(value) for value in site_info["longitude"]]
  longitude = sum(lon_float) / len(lon_float)

  time_stamps = sc_timeseries["waves"]["hs"]["times"]
  hs = sc_timeseries["waves"]["hs"]["values"]
  tp = sc_timeseries["waves"]["tp"]["values"]
  ws = sc_timeseries["winds"]["mag10"]["values"]
  cs = sc_timeseries["currents"]["mag"]["values"]

  # Convert timestamps to Year, Month, Day and Hour
  list_datetime = pylab.num2date(time_stamps)
  year = [elem.year for elem in list_datetime]
  month = [elem.month for elem in list_datetime]
  day = [elem.day for elem in list_datetime]
  hour = [elem.hour for elem in list_datetime]

  df_metocean = pd.DataFrame(
      {
          "year": year,
          "month": month,
          "day": day,
          "hour": hour,
          "hs": hs,
          "tp": tp,
          "ws": ws,
          "cs": cs
      }
  )

  # TODO: REMOVE THE FOLLOWING LINE. ###
  # Consider only 5 years
  # years_considered = 5
  # max_year = df_metocean['year'].max()
  # min_year = max_year - years_considered
  # df_metocean = df_metocean[df_metocean['year'] > min_year]

  metocean_dict = df_metocean.to_dict('list')

  data["name"] = site_name
  data["latitude"] = latitude
  data["longitude"] = longitude

  lmo_site_schema = SiteSchema()
  lmo_site = lmo_site_schema.load(data)

  lmo_site.bathymetry = dict(sc_bathymetry)
  lmo_site.seabed_type = dict(sc_seabed_type)
  lmo_site.metocean = metocean_dict

  try:
    db.session.add(lmo_site)
    db.session.commit()
    return lmo_site
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_site(lmoid):
  """
  Delete a LMO study from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted

  Returns:
    str: Success message if study is deleted; Error message if the ID does not exist.
  """
  site = Site.query.filter_by(study_id=lmoid).first()
  if site is None:
    return 'LMO study with that ID does not have a site yet.'

  try:
    db.session.delete(site)
    db.session.commit()
    return 'Site deleted'
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'
