# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
# from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ..studies.lmo_studies import update_status

from .lmo_site import create_site, delete_site
from dtop_lmo.service.models import LMOStudy, Site
from dtop_lmo.service.schemas import SiteSchema


bp = Blueprint('api_site', __name__)


@bp.route('/site', methods=['GET'])
def get_site(lmoid):
  """
  Flask blueprint route to get the site of a given LMO study.

  Returns error id the LMO study does not exist or if it does not have a site yet.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the Site of a LMO study or a HTTP 404 error.
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if lmo_study.site is None:
      return bad_request('This LMO study does not have a site yet.')

    site = Site.query.filter_by(study_id=lmoid).first()

    site_schema = SiteSchema()
    response = site_schema.dump(site)

    response["bathymetry"] = site.bathymetry
    response["seabed_type"] = site.seabed_type
    response["metocean"] = site.metocean

    return jsonify(response)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/site', methods=['POST'])
def post_site(lmoid):
  """
  Flask blueprint route to add attributes to a Site.

  Calls the :meth:`~dtop_lmo.service.api.site.get_site()` method.
  Returns 400 error if LMO study has a Site already;
  Returns 404 error if project ID does not exist.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the Site added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if lmo_study.site is not None:
      return bad_request("LMO study already has a site.")

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    request_body['study_id'] = lmo_study.id
    lmo_site = create_site(lmoid, request_body)
    if type(lmo_site) is str:
      # Error inside create_site()
      return bad_request(lmo_site)

    lmo_site_schema = SiteSchema()

    result = {
        "created_site": lmo_site_schema.dump(lmo_site),
        "message": "Site created"
    }
    result["created_site"]["bathymetry"] = lmo_site.bathymetry
    result["created_site"]["seabed_type"] = lmo_site.seabed_type
    result["created_site"]["metocean"] = lmo_site.metocean
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/site', methods=['DELETE'])
def del_site(lmoid):
  """
  Flask blueprint route for deleting a Site.

  Calls the :meth:`~dtop_lmo.service.api.site.delete_site()` method.
  Returns HTTP 404 error if LMO study id was not found.
  Returns HTTP 400 error if the LMO study does not have a site yet.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted

  Returns:
    dict: serialised response containing 'Site deleted' message; error otherwise
  """
  try:
    data = delete_site(lmoid)
    if data.lower() == 'site deleted':
      response = {
          'study_id': lmoid,
          'message': 'Site deleted.'
      }
      return jsonify(response)
    else:
      return not_found(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
