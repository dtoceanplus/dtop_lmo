# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, jsonify
import json
from copy import deepcopy

from ..errors import bad_request, not_found

from dtop_lmo.service.models import LMOStudy
from dtop_lmo.service.schemas import (
    LMOStudySchema,
    InputsSchema, ModuleSchema, ObjectSchema, CableSchema,
    MethodsSchema, RequirementsSchema,
    PhaseSchema, OperationSchema, ActivitySchema
)
from dtop_lmo.service.api.results import get_phase_plan

bp = Blueprint('api_export', __name__)
"""
  @oas [get] /api/study/{lmoid}/export
  description: Returns all inputs and outputs of LMO study
"""


@bp.route('/study/<lmoid>/export', methods=['GET'])
def export_lmo_study(lmoid):
  """
  Flask blueprint route to export a LMO study defined by its ID.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the LMO study or a HTTP 404 error.
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    lmo_study_schema = LMOStudySchema()
    lmo_inputs_schema = InputsSchema()
    lmo_module_schema = ModuleSchema(many=True)
    lmo_object_schema = ObjectSchema(many=True)
    lmo_cable_schema = CableSchema(many=True)
    lmo_phase_schema = PhaseSchema(many=True)
    lmo_requirements_schema = RequirementsSchema()
    lmo_operation_schema = OperationSchema(many=True)
    lmo_methods_schema = MethodsSchema()
    lmo_activity_schema = ActivitySchema(many=True)

    study = lmo_study_schema.dump(lmo_study)
    inputs = lmo_inputs_schema.dump(lmo_study.inputs)
    study["inputs"] = inputs
    modules = lmo_module_schema.dump(lmo_study.modules)
    for module_idx, module in enumerate(lmo_study.modules):
      modules[module_idx]["design"] = module.design
      modules[module_idx]["hierarchy"] = module.hierarchy
    study["modules"] = modules

    objects = lmo_object_schema.dump(lmo_study.objects)
    study["objects"] = objects
    cables = lmo_cable_schema.dump(lmo_study.cables)
    study["cables"] = cables
    for component in objects + cables:
      del component["operations"]

    phases = lmo_phase_schema.dump(lmo_study.phases)
    study["phases"] = phases
    for lmp_phase_idx, lmo_phase in enumerate(lmo_study.phases):
      requirements = lmo_requirements_schema.dump(lmo_phase.requirements)
      study["phases"][lmp_phase_idx]["requirements"] = requirements

      lmo_operations = lmo_operation_schema.dump(lmo_phase.operations)
      for lmp_operation_idx, lmo_operation in enumerate(lmo_phase.operations):
        objects = lmo_object_schema.dump(lmo_operation.objects)
        cables = lmo_cable_schema.dump(lmo_operation.cables)
        for component in objects + cables:
          del component["operations"]
        lmo_operations[lmp_operation_idx]["objects"] = objects
        lmo_operations[lmp_operation_idx]["cables"] = cables

        methods = lmo_methods_schema.dump(lmo_operation.methods)
        lmo_operations[lmp_operation_idx]["methods"] = methods

        activities = lmo_activity_schema.dump(lmo_operation.activities)
        lmo_operations[lmp_operation_idx]["activities"] = \
            activities
        lmo_operations[lmp_operation_idx]["costs"] = lmo_operation.costs
        lmo_operations[lmp_operation_idx]["durations"] = lmo_operation.durations
        lmo_operations[lmp_operation_idx]["waitings"] = lmo_operation.waitings

        del lmo_operations[lmp_operation_idx]["logistic_solution"]
        del lmo_operations[lmp_operation_idx]["dates_maystart_delta"]
        del lmo_operations[lmp_operation_idx]["multiple_operations"]

      operations = []
      #   Dates
      request_plan = get_phase_plan(lmo_phase.study.id, lmo_phase.name)
      plan = json.loads(request_plan.data)
      for plan_op_idx, plan_op_id in enumerate(plan["operation_id"]):
        for lmo_operation in lmo_operations:
          if lmo_operation["id_external"].lower() in plan_op_id.lower():
            idx = plan_op_idx
            break
        try:
          idx = int(idx)
        except NameError:
          continue

        operation = deepcopy(lmo_operation)

        operation["id_external"] = plan["operation_id"][idx]

        operation["date_maystart"] = plan["date_may_start"][idx]
        operation["date_start"] = plan["date_start"][idx]
        operation["date_end"] = plan["date_end"][idx]

        month = operation["date_maystart"].split('/')[1]
        month = int(month)
        if lmo_phase.study.inputs.period.lower() == 'month':
          period = month
        elif lmo_phase.inputs.period.lower() == 'quarter':
          # Check in which quarter this month is
          period = mt.ceil(month / 3)
        elif lmo_phase.study.inputs.period.lower() == 'trimester':
          # Check in which trimester this month is
          period = mt.ceil(month / 4)
        period = str(period)

        #   Durations and waitings
        try:
          # Complexity 2 and 3
          operation["durations"] = {
              "total": operation["durations"]["total"][period],
              "port": operation["durations"]["port"][period],
              "site": operation["durations"]["site"][period],
              "transit": operation["durations"]["transit"][period],
              "mobilization": operation["durations"]["mobilization"][period]
          }
        except KeyError:
          # Complexity 1
          operation["durations"] = operation["durations"][period]
        try:
          # Complexity 2 and 3
          operation["waitings"] = {
              "to_start": operation["waitings"]["to_start"][period],
              "port": operation["waitings"]["port"][period],
              "site": operation["waitings"]["site"][period]
          }
        except KeyError:
          # Complexity 1
          operation["waitings"] = operation["waitings"][period]

        #   Costs
        operation["costs"] = {
            "vessels": operation["costs"]["vessels"][period],
            "terminal": operation["costs"]["terminal"][period],
            "equipment": operation["costs"]["equipment"][period]
        }

        operations.append(operation)

        del idx

      study["phases"][lmp_phase_idx]["operations"] = operations

      del study["phases"][lmp_phase_idx]["calculating"]
      del study["phases"][lmp_phase_idx]["computed"]

    del study["inputs_locked"]

    result = study

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
