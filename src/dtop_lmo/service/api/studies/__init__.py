# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError
import json

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from .lmo_studies import create_study, update_study, delete_study

from dtop_lmo.service.models import LMOStudy
from dtop_lmo.service.schemas import (
    LMOStudySchema, LoadLMOStudySchema, UpdateLMOStudySchema
)

bp = Blueprint('api_studies', __name__)
"""
  @oas [get] /api/studies
  description: Returns LMO studies stored in database
"""


@bp.route('/study', methods=['GET'])
def get_lmo_studies():
  """
  Flask blueprint route to get all LMO studies.

  Returns:
    dict: the serialised response containing the LMO studies in database.
  """
  try:
    lmo_studies = LMOStudy.query.all()
    lmo_studies_schema = LMOStudySchema(many=True)
    result = lmo_studies_schema.dump(lmo_studies)

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/study', methods=['POST'])
def post_lmo_study():
  """
  Flask blueprint route to add a new LMO study.

  Calls the :meth:`~dtop_lmo.service.api.studies.create_study()` method.
  Returns HTTP 400 error if no request body provided;
  Returns HTTP 400 error if request body provided is not valid.

  Returns:
    dict: the serialised response containing the LMO study added to the database.
  """
  try:
    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    try:
      LoadLMOStudySchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    lmo_study = create_study(request_body)
    if type(lmo_study) is str:
      # Error inside create_study()
      return bad_request(lmo_study)

    lmo_study_schema = LMOStudySchema()

    result = {
        "created_study": lmo_study_schema.dump(lmo_study),
        "message": "Study created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/study/<lmoid>', methods=['GET'])
def get_lmo_study(lmoid):
  """
  Flask blueprint route to get a LMO study defined by its ID.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the LMO study or a HTTP 404 error.
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    lmo_study_schema = LMOStudySchema()
    result = lmo_study_schema.dump(lmo_study)

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/study/<lmoid>', methods=['PUT'])
def put_lmo_study(lmoid):
  """
  Flask blueprint route to modify a LMO study defined by its ID.

  Calls the :meth:`~dtop_lmo.service.api.lmo_studies.update_study()` method.
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if study ID does not exist.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the LMO study or a HTTP 404 error.
  """
  try:
    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    try:
      UpdateLMOStudySchema().load(request_body)
    except ValidationError as err:
      return bad_request('Body request fields not recognized')
    lmo_study = update_study(request_body, lmoid)

    if type(lmo_study) is str:
      if 'does not exist' in lmo_study:
        return not_found(lmo_study)
      else:
        return bad_request(lmo_study)
    lmo_study_schema = LMOStudySchema()

    response = {
        "updated_study": lmo_study_schema.dump(lmo_study),
        "message": "Study updated."
    }

    return jsonify(response)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/study/<lmoid>', methods=['DELETE'])
def del_lmo_study(lmoid):
  """
  Flask blueprint route for deleting a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.projects.delete_study()` method.
  Returns HTTP 404 error if LMO study id was not found.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted

  Returns:
    dict: serialised response containing 'Study deleted' message; not found error otherwise
  """
  try:
    data = delete_study(lmoid)
    if data.lower() == 'study deleted':
      response = {
          'study_id': lmoid,
          'message': 'Study deleted.'
      }
      return jsonify(response)
    else:
      return not_found(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
