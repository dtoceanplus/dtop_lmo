# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
from sqlalchemy.exc import OperationalError

from ....service import db

from ...models import LMOStudy, Phase, Operation
from ...schemas import LMOStudySchema


def create_study(data):
  """
  Create a new LMO study and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create the LMO study object.

  Returns:
    dict: the newly created LMO study instance
  """
  lmo_study_schema = LMOStudySchema()
  lmo_study = lmo_study_schema.load(data)

  try:
    db.session.add(lmo_study)
    db.session.commit()
    return lmo_study
  except IntegrityError:
    db.session.rollback()
    return 'LMO study with that name already exists.'


def update_study(data, lmoid):
  """
  Update a LMO study entry in the local storage database.

  Args:
    data (dict): dictionary with keys required to update the LMO study object in the local database.
    lmoid (str): the ID of the LMO study in the local storage database

  Returns:
    dict: the updated LMO study instance
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return 'LMO study with that ID does not exist.'

  # Update existing study (ID=lmoid) with existing data from "data"
  name = data.get('name', None)
  desc = data.get('description', None)
  complexity = data.get('complexity', None)
  inputs_locked = data.get('inputs_locked', None)
  if name is not None:
    lmo_study.name = name
  if desc is not None:
    lmo_study.description = desc
  if complexity is not None:
    lmo_study.complexity = complexity
  if inputs_locked is not None:
    lmo_study.inputs_locked = inputs_locked

  # Update database
  try:
    db.session.commit()
    return lmo_study
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'
  # except OperationalError:
  #   db.session.rollback()
  #   return 'Operational error.'


def delete_study(lmoid):
  """
  Delete a LMO study from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted

  Returns:
    str: Success message if study is deleted; Error message if the ID does not exist.
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return 'LMO study with that ID does not exist.'

  try:
    db.session.delete(lmo_study)
    db.session.commit()
    return 'Study deleted'
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def update_status(lmoid):
  """
  Update the status of a LMO study.

  Returns an error message if:
  - the LMOStudy ID that is trying to be updated does not exist

  :param str lmoid: the ID of the LMOStudy in the local storage database
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return "LMO Study with that ID does not exist."

  if lmo_study.inputs is not None:
    status = 15
    if len(lmo_study.modules) != 0:
      status = 30
      if len(lmo_study.phases) != 0:
        for lmo_phase in lmo_study.phases:
          if len(lmo_phase.operations) != 0:
            status += 30 / len(lmo_study.phases)

            if lmo_phase.operations[0].durations:
              status += 40 / len(lmo_study.phases)
  else:
    status = 0

  lmo_study.status = status

  # Update database
  try:
    db.session.commit()
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."
