# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from contextlib import suppress

import sqlalchemy.exc
from flask import Blueprint, request
import requests
import json

from dtop_lmo.service.db_utils import init_db
from dtop_lmo.service import db
from dtop_lmo.service.models import (
    LMOStudy, Inputs, Module, Phase, Operation
)

from .data_dummy import (
    operaion_durations, operaion_waitings, dates_maystart_delta,
    design_mc, design_ec, hierarchy_et, design_et, site
)

bp = Blueprint("provider_states", __name__)


@bp.route('/provider_states/setup', methods=['POST'])
def provider_states_setup():
  """
  Reset database and create data neccessary for contract testing.
  Should be available only with `FLASK_ENV=development`.
  """
  init_db()

  consumer = request.json['consumer']
  state = request.json['state']

  if state.startswith('lmo 1 exists'):
    with suppress(sqlalchemy.exc.IntegrityError):
      with db.session.begin_nested():
        study = LMOStudy(
            id=1,
            name='test',
            complexity='low'
        )
        db.session.add(study)

    if state == 'lmo 1 exists and has installation':
      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          inputs = Inputs(
              id=1,
              study_id=1,
              installation_date='2021/06',
              topside_exists=False,
              device_repair_at_port=False
          )
          db.session.add(inputs)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          module_mc = Module(
              id=1,
              study_id=1,
              module='mc',
              design=design_mc
          )
          db.session.add(module_mc)
          module_ec = Module(
              id=2,
              study_id=1,
              module='ec',
              design=design_ec
          )
          db.session.add(module_ec)
          module_et = Module(
              id=3,
              study_id=1,
              module='et',
              design=design_et,
              hierarchy=hierarchy_et
          )
          db.session.add(module_et)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          phase = Phase(
              id=1,
              study_id=1,
              name='installation',
              computed=True,
              calculating=False,
          )
          db.session.add(phase)

      url_path = f"{request.url_root}api/1"
      r_site = requests.post(f"{url_path}/site", json={"module_sc": site})
      r_objects = requests.post(f"{url_path}/objects")
      url_path = f"{request.url_root}api/1/phases/installation"
      r_operations = requests.post(f"{url_path}/operations")

      lmo_operations = Operation.query.all()
      for lmo_operation in lmo_operations:
        if lmo_operation.description == 'device repair on site':
          lmo_operation.dates_maystart_delta = dates_maystart_delta

      r_calculate = requests.post(f"{url_path}/calculate")

    if state == 'lmo 1 exists and has maintenance':
      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          inputs = Inputs(
              id=1,
              study_id=1,
              maintenance_date='2030/06',
              project_life=20,
              topside_exists=False,
              device_repair_at_port=False
          )
          db.session.add(inputs)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          module_mc = Module(
              id=1,
              study_id=1,
              module='mc',
              design=design_mc
          )
          db.session.add(module_mc)
          module_ec = Module(
              id=2,
              study_id=1,
              module='ec',
              design=design_ec
          )
          db.session.add(module_ec)
          module_et = Module(
              id=3,
              study_id=1,
              module='et',
              design=design_et,
              hierarchy=hierarchy_et
          )
          db.session.add(module_et)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          phase = Phase(
              id=1,
              study_id=1,
              name='maintenance',
              computed=True,
              calculating=False,
          )
          db.session.add(phase)

      url_path = f"{request.url_root}api/1"
      r_site = requests.post(f"{url_path}/site", json={"module_sc": site})
      r_objects = requests.post(f"{url_path}/objects")
      url_path = f"{request.url_root}api/1/phases/maintenance"
      r_operations = requests.post(f"{url_path}/operations")

      lmo_operations = Operation.query.all()
      for lmo_operation in lmo_operations:
        if lmo_operation.description == 'device repair on site':
          lmo_operation.dates_maystart_delta = dates_maystart_delta

      r_calculate = requests.post(f"{url_path}/calculate")

    if state == 'lmo 1 exists and it has downtime':
      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          inputs = Inputs(
              id=1,
              study_id=1,
              maintenance_date='2030/06',
              project_life=20,
              topside_exists=False,
              device_repair_at_port=False
          )
          db.session.add(inputs)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          module_mc = Module(
              id=1,
              study_id=1,
              module='mc',
              design=design_mc
          )
          db.session.add(module_mc)
          module_ec = Module(
              id=2,
              study_id=1,
              module='ec',
              design=design_ec
          )
          db.session.add(module_ec)
          module_et = Module(
              id=3,
              study_id=1,
              module='et',
              design=design_et,
              hierarchy=hierarchy_et
          )
          db.session.add(module_et)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          phase = Phase(
              id=1,
              study_id=1,
              name='maintenance',
              computed=True,
              calculating=False,
          )
          db.session.add(phase)

      url_path = f"{request.url_root}api/1"
      r_site = requests.post(f"{url_path}/site", json={"module_sc": site})
      r_objects = requests.post(f"{url_path}/objects")
      url_path = f"{request.url_root}api/1/phases/maintenance"
      r_operations = requests.post(f"{url_path}/operations")

      lmo_operations = Operation.query.all()
      for lmo_operation in lmo_operations:
        if lmo_operation.description == 'device repair on site':
          lmo_operation.dates_maystart_delta = dates_maystart_delta

      r_calculate = requests.post(f"{url_path}/calculate")
      r_downtime = requests.get(f"{url_path}/downtime")

    if state == 'lmo 1 exists and it has maintenance plan':
      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          inputs = Inputs(
              id=1,
              study_id=1,
              maintenance_date='2030/06',
              project_life=20,
              topside_exists=False,
              device_repair_at_port=False
          )
          db.session.add(inputs)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          module_mc = Module(
              id=1,
              study_id=1,
              module='mc',
              design=design_mc
          )
          db.session.add(module_mc)
          module_ec = Module(
              id=2,
              study_id=1,
              module='ec',
              design=design_ec
          )
          db.session.add(module_ec)
          module_et = Module(
              id=3,
              study_id=1,
              module='et',
              design=design_et,
              hierarchy=hierarchy_et
          )
          db.session.add(module_et)

      with suppress(sqlalchemy.exc.IntegrityError):
        with db.session.begin_nested():
          phase = Phase(
              id=1,
              study_id=1,
              name='maintenance',
              computed=True,
              calculating=False,
          )
          db.session.add(phase)

      url_path = f"{request.url_root}api/1"
      r_site = requests.post(f"{url_path}/site", json={"module_sc": site})
      r_objects = requests.post(f"{url_path}/objects")
      url_path = f"{request.url_root}api/1/phases/maintenance"
      r_operations = requests.post(f"{url_path}/operations")

      lmo_operations = Operation.query.all()
      for lmo_operation in lmo_operations:
        if lmo_operation.description == 'device repair on site':
          lmo_operation.dates_maystart_delta = dates_maystart_delta

      r_calculate = requests.post(f"{url_path}/calculate")
      r_plan = requests.get(f"{url_path}/plan")

  return ''
