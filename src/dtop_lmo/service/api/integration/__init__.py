# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, request, jsonify
import requests
import os
from copy import deepcopy

from ..errors import bad_request, not_found

from ..studies.lmo_studies import update_status

from dtop_lmo.service.api.inputs import create_inputs_modules
from dtop_lmo.service.api.site import create_site
from dtop_lmo.service.models import LMOStudy, Module
from dtop_lmo.service.schemas import ModuleSchema

bp = Blueprint('api_integration', __name__)


def get_url_and_headers(module):
  """
  Common function provided by OCC to get DTOP basic module URL and headers

  :param str module: the DTOP module nickname (e.g. 'mm')
  :return: the module API url and the headers instance
  :rtype: tuple
  """
  protocol = os.getenv("DTOP_PROTOCOL", "http")
  domain = os.environ["DTOP_DOMAIN"]
  auth = os.getenv("DTOP_AUTH", "")

  print()
  print("Authorization header value :")
  header_auth = f"{auth}"
  print(header_auth)

  print()
  print("Headers - auth :")
  headers = {"Authorization": header_auth}
  print(headers)

  print()
  print("Host header value :")
  header_host = f'{module + "." + domain}'
  print(header_host)

  server_url = f'{protocol + "://" + header_host}'

  print()
  print("Check if DTOP is deployed on DNS resolved server or on workstation :")

  try:
    response = requests.get(server_url, headers=headers)
    print("DTOP appears to be deployed on DNS resolved server")
    module_api_url = f"{server_url}"
  except requests.ConnectionError as exception:
    print("DTOP appears to be deployed on workstation")
    docker_ws_ip = "http://172.17.0.1:80"
    module_api_url = f"{docker_ws_ip}"
    # print()
    print("Headers - auth and localhost :")
    headers["Host"] = header_host
    print(headers)

  print()
  print("Basic Module Open API URL :")
  print(module_api_url)
  print()

  return module_api_url, headers


def call_intermodule_service(module, uri):
  """
  Generic function for communicating directly with the BE of other modules.

  Uses the *get_url_and_headers* function.
  Leads to a 400 error if the module server is not available.
  If the request is unsuccessful, a HTTP error is returned.

  :param str module: the nickname for the module that is being communicated with
  :param str uri: the URI specifying the appropriate route to call for the metric
  :return: the request response if successful, tuple of error and status code otherwise
  :rtype: requests.Response or tuple
  """
  # TODO: docstrings
  # TODO: update print statements
  # print()
  # print("Test example - request from Catalog BE to Main Module BE to get users' list")

  print()
  api_url, headers = get_url_and_headers(module)
  print("MM Open API URL to Get users :")
  users_url = f"{api_url + uri}"
  print(users_url)

  print()
  print("CM BE to MM BE request and response :")
  print()

  try:
    resp = requests.get(users_url, headers=headers)
    resp.raise_for_status()
    print()
    return resp
  except requests.exceptions.ConnectionError as errc:
    print("Error Connecting:", errc)
    print("The module server is not available")
    return errc, 400
  except requests.exceptions.HTTPError as errh:
    print("HTTP Error:", errh)
    return errh, errh.response.status_code


@bp.route('/integration/<module>/', methods=['POST'])
def post_lmo_integration_module(lmoid, module):
  """
  Flask blueprint route to add data from other modules to a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.inputs.create_inputs_modules()` method.
  Returns HTTP 400 error if no request body provided or if a the this module inputs for this study were already defined
  Returns 404 error if study ID does not exist.

  Args:
  lmoid (int): LMO study ID
  module (str): External module short string - mc, ec, et, ed, sk

  Returns:
  dict: the serialised response containing this module feched data added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_urls = request.get_json()
    if not request_urls:
      return bad_request('No request body provided')

    module_name = module.lower()
    lmo_module = Module.query.filter_by(study_id=lmoid, module=module_name).first()
    if lmo_module is not None:
      return bad_request('%s module inputs already defined' % module_name.upper())

    request_body = {}
    if module_name == 'mc':
      mc_general = call_intermodule_service(
          "mc", request_urls["mc_general"]).json()
      mc_dimensions = call_intermodule_service(
          "mc", request_urls["mc_dimensions"]).json()
      request_body["general"] = mc_general
      request_body["dimensions"] = mc_dimensions

    elif module_name == 'ec':
      ec_farm = call_intermodule_service(
          "ec", request_urls["ec_farm"]).json()
      request_body = ec_farm

    elif module_name == 'et':
      et_array = call_intermodule_service(
          "et", request_urls["et_array"]).json()
      et_devices = call_intermodule_service(
          "et", request_urls["et_devices"]).json()
      # PTOs data must be fetched and included in "et_devices" key
      for idx, device_id in enumerate(et_array["device_results"]):
        pto_request = deepcopy(request_urls["et_device_ptos"])
        pto_request = pto_request.replace('device_id', str(device_id))
        et_device_ptos = call_intermodule_service("et", pto_request).json()
        et_devices[idx]["ptos"] = et_device_ptos
        del pto_request, et_device_ptos
      request_body["array"] = et_array
      request_body["devices"] = et_devices

    elif module_name == 'ed':
      ed_results = call_intermodule_service(
          "ed", request_urls["ed_results"]).json()
      request_body = ed_results

    elif module_name == 'sk':
      sk_hierarchy = call_intermodule_service(
          "sk", request_urls["sk_hierarchy"]).json()
      request_body = sk_hierarchy

    else:
      return bad_request('Unknown module: %s' % module_name.upper())

    request_body["module"] = module_name
    request_body['study_id'] = lmo_study.id

    lmo_inputs_module = create_inputs_modules(request_body, lmo_study.complexity)
    if type(lmo_inputs_module) is str:
      # Error inside create_inputs_modules()
      return bad_request(lmo_inputs_module)
    # Update LMO study status
    update_status(lmoid)

    lmo_inputs_module_schema = ModuleSchema()

    result = {
      "created_inputs": lmo_inputs_module_schema.dump(lmo_inputs_module),
      "message": "%s module inputs created" % module.upper()
    }
    result["created_inputs"]["design"] = lmo_inputs_module.design
    result["created_inputs"]["hierarchy"] = lmo_inputs_module.hierarchy
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/integration/site/', methods=['POST'])
def post_lmo_integration_site(lmoid):
  """
  Flask blueprint route to add data from other site to a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.site.create_site()` method.
  Returns HTTP 400 error if no request body provided or if a the this site inputs for this study were already defined
  Returns 404 error if study ID does not exist.

  Args:
  lmoid (int): LMO study ID

  Returns:
  dict: the serialised response containing site feched data added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_urls = request.get_json()
    if not request_urls:
      return bad_request('No request body provided')

    request_body = {}
    # sc_all = call_intermodule_service(
    #     "sc", request_urls["sc_all"]).json()
    sc_info = call_intermodule_service(
        "sc", request_urls["sc_info"]).json()
    sc_bathymetry = call_intermodule_service(
        "sc", request_urls["sc_bathymetry"]).json()
    sc_seabedtype = call_intermodule_service(
        "sc", request_urls["sc_seabedtype"]).json()
    sc_timeseries = call_intermodule_service(
        "sc", request_urls["sc_timeseries"]).json()

    request_body["site_id"] = request_urls["sc_id"]
    request_body["farm"] = {}
    request_body["farm"]["info"] = sc_info
    request_body["farm"]["direct_values"] = {}
    request_body["farm"]["direct_values"]["bathymetry"] = sc_bathymetry
    request_body["farm"]["direct_values"]["seabed_type"] = sc_seabedtype
    request_body["point"] = {}
    request_body["point"]["time_series"] = sc_timeseries

    response = jsonify(request_body)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
