# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
from marshmallow import ValidationError
from copy import deepcopy

# Import business logic
from dtop_lmo import business

from ..components.lmo_components import convert_object, convert_cable

from ..utils import join_validation_error_messages

from ....service import db

from ...models import Object, Cable, Phase, Operation, Methods
from ...schemas import OperationSchema, LoadOperationSchema, MethodsSchema


def create_operations(lmo_study, lmo_phase):
  """
  Create a new Operation and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create the Operation object.

  Returns:
    dict: the newly created Operation instance
  """
  complexity = lmo_study.complexity

  topside_exists = lmo_study.inputs.topside_exists
  project_life = lmo_study.inputs.project_life
  device_repair_at_port = lmo_study.inputs.device_repair_at_port

  if lmo_study.objects is not None:
    phase_objects = [convert_object(obj, complexity, topside_exists)
                     for obj in lmo_study.objects]
  if lmo_study.cables is not None:
    phase_cables = [convert_cable(cbl, complexity)
                    for cbl in lmo_study.cables]

  if complexity == 'low':
    if lmo_phase.name == 'installation':
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity
      )
      # Some umbilicals are only to be considered for maintenance since they
      # are part of a static cable already
      idx = 0
      while idx < len(phase_cables):
        if phase_cables[idx].type.lower() == 'array_maintenance':
          del phase_cables[idx]
        else:
          idx += 1
      Inst_funcs.objects = phase_objects
      Inst_funcs.cables = phase_cables

      # Call Installation functionalities
      operations_sequence = Inst_funcs.get_operations_sequence()
      operations = Inst_funcs.set_operations(operations_sequence)

    elif lmo_phase.name == 'maintenance':
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          t_life=project_life,
          device_repair_at_port=device_repair_at_port
      )
      Main_funcs.objects = phase_objects
      Main_funcs.cables = phase_cables

      # Call Maintenance functionalities
      failure_times = Main_funcs.set_object_failures()
      operations = Main_funcs.set_maintenance_operations(failure_times)

    elif lmo_phase.name == 'decommissioning':
      # Get installation SQL element
      installation_phase = Phase.query.filter_by(
          study_id=lmo_study.id,
          name='installation'
      ).first()
      if installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'

      if len(installation_phase.operations) == 0:
        return 'Installation phase operations not found in database.'

      # Decommissioning operation IDs
      dict_operations = {
          "op101": "device removal",
          "op102": "collection point removal",
          "op103": "support structure removal",
          "op104": "mooring removal",
          "op105": "foundation removal"
      }

      for inst_ops in installation_phase.operations[::-1]:
        if 'cable' not in inst_ops.name:
          # Operation name
          operation_name = inst_ops.name.replace('installation', 'removal')
          # Get ID
          id_external = None
          for op_id, op_name in dict_operations.items():
            if op_name == operation_name:
              id_external = op_id + '_' + inst_ops.id_external.split('_')[1]
              break

          operation_data = {
              "id_external": id_external,
              "name": operation_name,
              "description": inst_ops.description,
              "multiple_operations": inst_ops.multiple_operations
          }
          try:
            LoadOperationSchema().load(operation_data)
          except ValidationError as err:
            msg = join_validation_error_messages(err)
            return msg

          operation_data["phase_id"] = lmo_phase.id
          lmo_operation_schema = OperationSchema()
          lmo_operation = lmo_operation_schema.load(operation_data)

          # Operation was created, now we fill it with objects
          if inst_ops.objects is not None:
            for lmo_object in inst_ops.objects:
              lmo_operation.objects.append(lmo_object)

          try:
            db.session.add(lmo_operation)
            db.session.commit()

          except IntegrityError:
            db.session.rollback()
            return 'Error creating Operation %s' % lmo_operation.id_external

          del lmo_operation

      lmo_operations = Operation.query.filter_by(phase_id=lmo_phase.id).all()
      return lmo_operations

  # if complexity == 'med':
  #   if lmo_phase.name == 'installation':
  #     pass
  #   elif lmo_phase.name == 'maintenance':
  #     pass
  #   elif lmo_phase.name == 'decomissioning':
  #     pass

  if complexity == 'high' or complexity == 'med':
    if lmo_phase.name == 'installation':
      Inst_funcs = business.Installation.get_complexity(
          complexity=complexity
      )
      # Some umbilicals are only to be considered for maintenance since they
      # are part of a static cable already
      idx = 0
      while idx < len(phase_cables):
        if phase_cables[idx].type.lower() == 'array_maintenance':
          del phase_cables[idx]
        else:
          idx += 1
      Inst_funcs.objects = phase_objects
      Inst_funcs.cables = phase_cables

      # Call Installation functionalities
      operations_sequence = Inst_funcs.get_operations_sequence()
      operations = Inst_funcs.set_operations(operations_sequence)

    elif lmo_phase.name == 'maintenance':
      Main_funcs = business.Maintenance.get_complexity(
          complexity=complexity,
          t_life=project_life,
          device_repair_at_port=device_repair_at_port
      )
      Main_funcs.objects = phase_objects
      Main_funcs.cables = phase_cables

      # Call Maintenance functionalities
      failure_times = Main_funcs.set_object_failures()
      operations = Main_funcs.set_maintenance_operations(failure_times)

    elif lmo_phase.name == 'decommissioning':
      # Get installation SQL element
      lmoid = Phase.query.get(lmo_phase.id).study_id
      installation_phase = Phase.query.filter_by(
          study_id=lmoid,
          name='installation'
      ).first()
      if installation_phase is None:
        return 'Installation phase required to calculate Decommissioning results.'

      installation_operations = Operation.query.filter_by(phase_id=installation_phase.id).all()
      if len(installation_operations) == 0:
        return 'Installation phase operations not found in database.'

      # Decommissioning operation IDs
      dict_operations = {
          "op101": "device removal",
          "op102": "collection point removal",
          "op103": "support structure removal",
          "op104": "mooring removal",
          "op105": "foundation removal"
      }

      for inst_ops in installation_operations[::-1]:
        if 'cable' not in inst_ops.name:
          # Operation name
          operation_name = inst_ops.name.replace('installation', 'removal')
          # Get ID
          id_external = None
          for op_id, op_name in dict_operations.items():
            if op_name == operation_name:
              id_external = op_id + '_' + inst_ops.id_external.split('_')[1]
              break

          operation_data = {
              "id_external": id_external,
              "name": operation_name,
              "description": inst_ops.description,
              "multiple_operations": inst_ops.multiple_operations
          }
          try:
            LoadOperationSchema().load(operation_data)
          except ValidationError as err:
            msg = join_validation_error_messages(err)
            return msg

          operation_data["phase_id"] = lmo_phase.id
          lmo_operation_schema = OperationSchema()
          lmo_operation = lmo_operation_schema.load(operation_data)

          # Operation was created, now we fill it with objects
          if inst_ops.objects is not None:
            for lmo_object in inst_ops.objects:
              lmo_operation.objects.append(lmo_object)

          try:
            db.session.add(lmo_operation)
            db.session.commit()

          except IntegrityError:
            db.session.rollback()
            return 'Error creating Operation %s' % lmo_operation.id_external

          del lmo_operation

      lmo_operations = Operation.query.filter_by(phase_id=lmo_phase.id).all()
      return lmo_operations

    else:
      return 'Phase %s not recognized' % lmo_phase.name

  for operation in operations:
    operation_data = {
        "id_external": operation.id,
        "name": operation.name,
        "description": operation.description,
        "multiple_operations": operation.multiple_operations
    }
    try:
      LoadOperationSchema().load(operation_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return msg

    operation_data["phase_id"] = lmo_phase.id
    lmo_operation_schema = OperationSchema()
    lmo_operation = lmo_operation_schema.load(operation_data)

    if lmo_operation.multiple_operations is True:
      lmo_operation.dates_maystart_delta = {
          "dates_maystart_delta": operation.maystart_deltas
      }

    # Operation was created, now we fill it with objects and cables
    if operation.objects is not None:
      for obj in operation.objects:
        lmo_object = Object.query.filter_by(
            study_id=lmo_study.id,
            id_external=obj.id
        ).first()
        lmo_operation.objects.append(lmo_object)
    if operation.cables is not None:
      for cbl in operation.cables:
        lmo_cable = Cable.query.filter_by(
            study_id=lmo_study.id,
            id_external=cbl.id
        ).first()
        lmo_operation.cables.append(lmo_cable)

    try:
      db.session.add(lmo_operation)
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return 'Error creating Operation %s' % lmo_operation.id_external

    del lmo_operation

  lmo_operations = Operation.query.filter_by(phase_id=lmo_phase.id).all()
  return lmo_operations


def delete_phase_operations(phase_id):
  """
  Delete operations of a phase from the local storage database.

  Args:
    phase_id (int): the ID of a phase in local storage database

  Returns:
    str: Success message when operations are deleted
  """
  lmo_phase = Phase.query.get(phase_id)

  try:
    db.session.query(Operation).filter_by(phase_id=phase_id).delete()
    db.session.commit()
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."

  # Update phase in the database
  lmo_phase.computed = False
  try:
    db.session.commit()
    return 'Phase operations deleted'
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def create_methods(data):
  """
  Create new operation Methods and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create Methods object.

  Returns:
    dict: the newly created Methods instance
  """
  lmo_methods_schema = MethodsSchema()
  lmo_methods = lmo_methods_schema.load(data)

  try:
    db.session.add(lmo_methods)
    db.session.commit()
    return lmo_methods
  except IntegrityError:
    db.session.rollback()
    return 'LMO study with that name already exists.'


def update_methods(data, opid):
  """
  Update a operation Methods entry in the local storage database.

  Args:
    data (dict): dictionary with keys required to update the operation Methods object in the local database.
    lmoid (str): the ID of the Operation in the local storage database

  Returns:
    dict: the updated operation Methods instance
  """
  lmo_methods = Methods.query.filter_by(operation_id=opid).first()
  if lmo_methods is None:
    return 'Operation with that ID does not have methods yet.'

  # Update existing methods with existing data from "data"
  load_out = data.get('load_out', None)
  transportation = data.get('transportation', None)
  piling = data.get('piling', None)
  burial = data.get('burial', None)
  assembly = data.get('assembly', None)
  landfall = data.get('landfall', None)
  if load_out is not None:
    lmo_methods.load_out = load_out
  if transportation is not None:
    lmo_methods.transportation = transportation
  if piling is not None:
    lmo_methods.piling = piling
  if burial is not None:
    lmo_methods.burial = burial
  if assembly is not None:
    lmo_methods.assembly = assembly
  if landfall is not None:
    lmo_methods.landfall = landfall

  # Update database
  try:
    db.session.commit()
    return lmo_methods
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_all_methods(lmo_id):
  """
  Delete all methods from the local storage database.

  Args:
    lmo_id (int): the ID of a LMO study in local storage database

  Returns:
    str: Success message when methods are deleted
  """
  lmo_phases = Phase.query.filter_by(study_id=lmo_id).all()
  for lmo_phase in lmo_phases:
    lmo_operations = Operation.query.filter_by(phase_id=lmo_phase.id).all()
    for operation in lmo_operations:
      if operation.methods is not None:
        db.session.delete(operation.methods)
        db.session.commit()
  return 'All methods deleted'


def delete_methods(methods_id):
  """
  Delete operation methods from the local storage database.

  Args:
    methods_id (int): the ID of Methods in local storage database

  Returns:
    str: Success message when methods are deleted
  """
  lmo_methods = Methods.query.get(methods_id)
  try:
    db.session.delete(lmo_methods)
    db.session.commit()
    return 'Operation methods deleted'
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'
