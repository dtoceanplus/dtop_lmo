# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# Import packages
from sqlalchemy.exc import IntegrityError
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ....service import db

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages, dict_replace_nan

from ..studies.lmo_studies import update_status

from .lmo_operations import (
    create_operations, delete_phase_operations,
    create_methods, update_methods, delete_all_methods, delete_methods
)

from dtop_lmo.service.models import LMOStudy, Module, Phase, Operation, Activity
from dtop_lmo.service.schemas import (
    OperationSchema, ActivitySchema, MethodsSchema
)

bp = Blueprint('api_operations', __name__)


@bp.route('/operations', methods=['DELETE'])
def del_all_operations(lmoid):
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    if len(lmo_phases) == 0:
      return not_found('This LMO study does not have phases')

    for lmo_phase in lmo_phases:
      if lmo_phase.operations is not None and len(lmo_phase.operations) > 0:
        delete_phase_operations(lmo_phase.id)

    # Update LMO study status
    update_status(lmoid)

    return jsonify(
        {"message": 'All operations deleted'}
    )

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/operations', methods=['GET'])
def get_phase_operations(lmoid, phase):
  """
  Flask blueprint route to get operations of a given phase.

  Returns HTTP 400 error if this phase is not defined for this this study;
  Returns HTTP 400 error if there are no operations defined for this phase;
  Returns 404 error if study ID does not exist.

  Args:
    lmoid (int): The ID of the LMO Study
    phase (str): The name of the logistic phase

  Returns:
    dict: the serialised response containing the operations.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    lmo_operations = Operation.query.filter_by(phase_id=lmo_phase.id).all()
    if not lmo_operations:
      return not_found('The are no operations defined for this phase.')

    lmo_operation_schema = OperationSchema()
    lmo_activity_schema = ActivitySchema()
    result = []
    for operation in lmo_operations:
      lmo_operation = lmo_operation_schema.dump(operation)
      lmo_operation["durations"] = dict(operation.durations)
      lmo_operation["waitings"] = dict(operation.waitings)
      lmo_operation["costs"] = dict(operation.costs)
      lmo_operation["logistic_solution"] = dict(operation.logistic_solution)
      lmo_operation["dates_maystart_delta"] = dict(operation.dates_maystart_delta)

      lmo_operation["activities"] = {}
      lmo_activities = Activity.query.filter_by(operation_id=operation.id)
      for act in lmo_activities:
        lmo_activitie = lmo_activity_schema.dump(act)
        lmo_activitie = dict_replace_nan(lmo_activitie)
        lmo_operation["activities"][act.id] = lmo_activitie

      lmo_operation = dict_replace_nan(lmo_operation)
      result.append(lmo_operation)

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/operations', methods=['POST'])
def post_phase_operations(lmoid, phase):
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if lmo_study.inputs is None:
      return bad_request('Inputs not defined for this LMO study')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return not_found('%s phase not defined for this LMO study' % phase)
    if len(lmo_phase.operations) != 0:
      return bad_request('Operations already defined')

    lmo_operations = create_operations(lmo_study, lmo_phase)
    if type(lmo_operations) is str:
      # Error inside create_operations()
      return bad_request(lmo_operations)

    # Update LMO study status
    update_status(lmoid)

    lmo_operation_schema = OperationSchema()

    result = []
    for operation in lmo_operations:
      operation_schema = lmo_operation_schema.dump(operation)
      operation_schema["durations"] = dict(operation.durations)
      operation_schema["waitings"] = dict(operation.waitings)
      operation_schema["costs"] = dict(operation.costs)
      operation_schema["logistic_solution"] = dict(operation.logistic_solution)
      operation_schema["dates_maystart_delta"] = dict(operation.dates_maystart_delta)
      result.append(operation_schema)
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/operations', methods=['DELETE'])
def del_phase_operations(lmoid, phase):
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return not_found('%s phase not defined for this LMO study' % phase)

    lmo_phase.calculating = False
    try:
      db.session.commit()
    except IntegrityError:
      db.session.rollback()
      return bad_request('Error turning calculationg flag to False')

    if lmo_phase.operations is None or len(lmo_phase.operations) == 0:
      return not_found('No operations to delete')

    data = delete_phase_operations(lmo_phase.id)

    # Update LMO study status
    update_status(lmoid)

    return jsonify(
        {"message": data}
    )

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods/device', methods=['POST'])
def post_methods_device(lmoid):
  """
  Flask blueprint route to add new Methods to operations with devices.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_methods()` and
  :meth:`~dtop_lmo.service.api.operations.delete_methods()` methods.
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if no request body provided.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised response containing the operation Methods added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    # if not request_body:
    #   return bad_request('No request body provided')

    try:
      MethodsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get operations with devices -> device installation, removal and retrival
    lmo_operations_device = []
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    for ph in lmo_phases:
      lmo_operations = Operation.query.filter_by(phase_id=ph.id).all()
      for op in lmo_operations:
        if 'device' in op.name.lower() and 'installation' in op.name.lower():
          # Device installation operation
          lmo_operations_device.append(op)
        elif 'op15' in op.id_external.lower() or 'op17' in op.id_external.lower():
          # Device removal and retrival operations
          # Check for device objects in this operation (it can be a CP)
          for lmo_object in op.objects:
            if 'device' in lmo_object.id_external.lower():
              lmo_operations_device.append(op)
              break

    lmo_methods_list = []
    for lmo_operation in lmo_operations_device:
      if lmo_operation.methods is not None:
        # Methods already defined, delete them
        lmo_methods = delete_methods(lmo_operation.methods.id)

      # Create methods
      request_body["operation_id"] = lmo_operation.id
      lmo_methods = create_methods(request_body)
      del request_body["operation_id"]
      if type(lmo_methods) is str:
        # Error inside create_methods()
        return bad_request(lmo_methods)

      lmo_methods_schema = MethodsSchema()
      lmo_methods_list.append(lmo_methods_schema.dump(lmo_methods))

    result = {
        "methods": lmo_methods_list,
        "message": "Device methods created/updated"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods/foundation', methods=['POST'])
def post_methods_foundation(lmoid):
  """
  Flask blueprint route to add new Methods to operations with foundations.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_methods()` and
  :meth:`~dtop_lmo.service.api.operations.delete_methods()` methods.
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if no request body provided.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised response containing the operation Methods added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    # if not request_body:
    #   return bad_request('No request body provided')

    try:
      MethodsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get operations with foundations -> foundation installation and inspection
    lmo_operations_found = []
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    for ph in lmo_phases:
      lmo_operations = Operation.query.filter_by(phase_id=ph.id).all()
      for op in lmo_operations:
        if 'foundation' in op.name.lower() and 'installation' in op.name.lower():
          # Foundation installation operation
          lmo_operations_found.append(op)
        # elif 'op11' in op.id_external.lower():
        #   # Check for foundations objects in this operation
        #   if 'gravity' in op.objects_id.lower() or 'found' in op.objects_id.lower():
        #     lmo_operations_found.append(op)

    lmo_methods_list = []
    for lmo_operation in lmo_operations_found:
      if lmo_operation.methods is not None:
        # Methods already defined, delete them
        lmo_methods = delete_methods(lmo_operation.methods.id)

      # Create methods
      request_body["operation_id"] = lmo_operation.id
      lmo_methods = create_methods(request_body)
      del request_body["operation_id"]
      if type(lmo_methods) is str:
        # Error inside create_methods()
        return bad_request(lmo_methods)

      lmo_methods_schema = MethodsSchema()
      lmo_methods_list.append(lmo_methods_schema.dump(lmo_methods))

    result = {
        "methods": lmo_methods_list,
        "message": "Foundation methods created/updated"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods/anchor', methods=['POST'])
def post_methods_anchor(lmoid):
  """
  Flask blueprint route to add new Methods to operations with anchors.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_methods()` and
  :meth:`~dtop_lmo.service.api.operations.delete_methods()` methods.
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if no request body provided.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised response containing the operation Methods added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    # if not request_body:
    #   return bad_request('No request body provided')

    try:
      MethodsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get operations with anchors -> mooring installation and replacement
    lmo_operations_anchor = []
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    for ph in lmo_phases:
      lmo_operations = Operation.query.filter_by(phase_id=ph.id).all()
      for op in lmo_operations:
        if 'mooring' in op.name.lower() and 'installation' in op.name.lower():
          # Mooring installation operation
          # Check for anchor objects in this operation
          for lmo_object in op.objects:
            if 'anchor' in lmo_object.id_external.lower():
              lmo_operations_anchor.append(op)
              break
        elif 'op19' in op.id_external.lower():
          # Check for anchor objects in this operation
          for lmo_object in op.objects:
            if 'anchor' in lmo_object.id_external.lower():
              lmo_operations_anchor.append(op)
              break

    lmo_methods_list = []
    for lmo_operation in lmo_operations_anchor:
      if lmo_operation.methods is not None:
        # Methods already defined, delete them
        lmo_methods = delete_methods(lmo_operation.methods.id)

      # Create methods
      request_body["operation_id"] = lmo_operation.id
      lmo_methods = create_methods(request_body)
      del request_body["operation_id"]
      if type(lmo_methods) is str:
        # Error inside create_methods()
        return bad_request(lmo_methods)

      lmo_methods_schema = MethodsSchema()
      lmo_methods_list.append(lmo_methods_schema.dump(lmo_methods))

    result = {
        "methods": lmo_methods_list,
        "message": "Anchor methods created/updated"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods/collectionpoint', methods=['POST'])
def post_methods_collection_point(lmoid):
  """
  Flask blueprint route to add new Methods to operations with collection points.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_methods()` and
  :meth:`~dtop_lmo.service.api.operations.delete_methods()` methods.
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if no request body provided.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised response containing the operation Methods added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    # if not request_body:
    #   return bad_request('No request body provided')

    try:
      MethodsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get operations with collection points -> collection point installation,
    # removal and retrival
    lmo_operations_cp = []
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    for ph in lmo_phases:
      lmo_operations = Operation.query.filter_by(phase_id=ph.id).all()
      for op in lmo_operations:
        if ('collection' in op.name.lower() and 'point' in op.name.lower() and
            'installation' in op.name.lower()):
          # Collection point installation operation
          lmo_operations_cp.append(op)
        elif 'op15' in op.id_external.lower() or 'op17' in op.id_external.lower():
          # Collection point removal and retrival operations
          # Check for collection point objects in this operation (it can be a device)
          for lmo_object in op.objects:
            if 'ed' in lmo_object.id_external.lower():
              lmo_operations_cp.append(op)
              break

    lmo_methods_list = []
    for lmo_operation in lmo_operations_cp:
      if lmo_operation.methods is not None:
        # Methods already defined, delete them
        lmo_methods = delete_methods(lmo_operation.methods.id)

      # Create methods
      request_body["operation_id"] = lmo_operation.id
      lmo_methods = create_methods(request_body)
      del request_body["operation_id"]
      if type(lmo_methods) is str:
        # Error inside create_methods()
        return bad_request(lmo_methods)

      lmo_methods_schema = MethodsSchema()
      lmo_methods_list.append(lmo_methods_schema.dump(lmo_methods))

    result = {
        "methods": lmo_methods_list,
        "message": "Collection point methods created/updated"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods/cable', methods=['POST'])
def post_methods_cable(lmoid):
  """
  Flask blueprint route to add new Methods to operations with cables.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_methods()` and
  :meth:`~dtop_lmo.service.api.operations.delete_methods()` methods.
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if no request body provided.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised response containing the operation Methods added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if module_ed is not None:
      module_ed_data = module_ed.design
      method_burial = module_ed_data.get('cable_installation')

    request_body = request.get_json()
    if method_burial is not None:
      request_body["burial"] = method_burial.lower()
    # if not request_body:
    #   return bad_request('No request body provided')

    try:
      MethodsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get operations with cables -> cable installation
    lmo_operations_cable = []
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    for ph in lmo_phases:
      lmo_operations = Operation.query.filter_by(phase_id=ph.id).all()
      for op in lmo_operations:
        if 'cable' in op.name.lower() and 'installation' in op.name.lower():
          # Cable installation operation
          lmo_operations_cable.append(op)

    lmo_methods_list = []
    for lmo_operation in lmo_operations_cable:
      if lmo_operation.methods is not None:
        # Methods already defined, delete them
        lmo_methods = delete_methods(lmo_operation.methods.id)

      # Create methods
      request_body["operation_id"] = lmo_operation.id
      lmo_methods = create_methods(request_body)
      del request_body["operation_id"]
      if type(lmo_methods) is str:
        # Error inside create_methods()
        return bad_request(lmo_methods)

      lmo_methods_schema = MethodsSchema()
      lmo_methods_list.append(lmo_methods_schema.dump(lmo_methods))

    result = {
        "methods": lmo_methods_list,
        "message": "Cable methods created/updated"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/<opid>/methods', methods=['GET'])
def get_operation_methods(lmoid, opid):
  """
  Flask blueprint route to get an operation Methods.

  Returns:
    dict: the serialised response containing the operation Methods in database.
  """
  try:
    lmo_operation = Operation.query.get(opid)
    if lmo_operation is None:
      return not_found('Operation with that ID could not be found')
    if lmo_operation.methods is None:
      return bad_request('This operation does not have methods defined yet')
    lmo_methods_schema = MethodsSchema()
    result = {
        "methods": lmo_methods_schema.dump(lmo_operation.methods)
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/methods', methods=['DELETE'])
def del_methods_all(lmoid):
  """
  Flask blueprint route to add delete all Methods.

  Calls the :meth:`~dtop_lmo.service.api.operations.delete_all_methods()`
  Returns HTTP 404 error if there is no LMO study with this ID.

  Args:
      lmoid (str): ID of the LMO study

  Returns:
      dict: the serialised success response.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    data = delete_all_methods(lmoid)

    return jsonify(
        {"message": data}
    )

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
