# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
from marshmallow import ValidationError
import json
import math
import utm
import re
import os

# Import business logic
from dtop_lmo import business

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ....service import db

from ...models import Site, Object, Cable
from ...schemas import ObjectSchema, CableSchema


def get_objects(
    lmoid: int,
    siteid: int,
    mc_data: dict,
    ec_data: dict,
    et_data: dict,
    ed_data: dict,
    sk_data: dict,
    hierarchy_et: dict,
    hierarchy_ed: dict,
    hierarchy_sk: dict
) -> dict:
  """
  Gets other modules designs and build a request body type with all objects of a project

  Uses the following functions:
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_device_objects()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_collection_points()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_connectors()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_anchors()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_foundations()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_moorings()`

  Args:
    lmoid (int): the ID of the LMO study in local storage database to add objects
    siteid (int): the ID of the LMO study Site in local storage database
    mc_data (dict): all data from MC module
    ec_data (dict): all data from EC module
    et_data (dict): all data from ET module
    ed_data (dict): all data from ED module
    sk_data (dict): all data from SK module
    hierarchy_et (dict): Hierarchy from ET module
    hierarchy_ed (dict): Hierarchy from ED module
    hierarchy_sk (dict): Hierarchy from SK module

  Returns:
    dict: dictionary with all created objects and their attributes or an error message
  """
  objects_in_db = []
  # Define devices
  objects_devices = get_device_objects(
      siteid=siteid,
      mc_data=mc_data,
      ec_data=ec_data,
      et_data=et_data
  )
  for key, device_data in objects_devices.items():
    device_data["study_id"] = lmoid
    try:
      ObjectSchema().load(device_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = device_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(device_data)
    objects_in_db.append(lmo_object)

  # Define PTOs
  objects_ptos = get_pto_objects(et_data=et_data, hierarchy_et=hierarchy_et)
  for key, pto_data in objects_ptos.items():
    pto_data["study_id"] = lmoid
    try:
      ObjectSchema().load(pto_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = pto_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(pto_data)
    objects_in_db.append(lmo_object)

  # Define collection points
  objects_cps = get_collection_points(
      siteid=siteid,
      ed_data=ed_data,
      hierarchy_ed=hierarchy_ed
  )
  for key, cp_data in objects_cps.items():
    cp_data["study_id"] = lmoid
    try:
      ObjectSchema().load(cp_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = cp_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(cp_data)
    objects_in_db.append(lmo_object)

  # Define connectors
  objects_connectors = get_connectors(
      ed_data=ed_data,
      hierarchy_ed=hierarchy_ed
  )
  for key, connector_data in objects_connectors.items():
    connector_data["study_id"] = lmoid
    try:
      ObjectSchema().load(connector_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = connector_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(connector_data)
    objects_in_db.append(lmo_object)

  # Define anchors
  objects_anchors = get_anchors(
      siteid=siteid,
      sk_data=sk_data,
      hierarchy_sk=hierarchy_sk
  )
  for key, anchor_data in objects_anchors.items():
    anchor_data["study_id"] = lmoid
    try:
      ObjectSchema().load(anchor_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = anchor_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(anchor_data)
    objects_in_db.append(lmo_object)

  # Define foundations
  objects_foundations = get_foundations(
      siteid=siteid,
      sk_data=sk_data,
      hierarchy_sk=hierarchy_sk
  )
  for key, foundation_data in objects_foundations.items():
    foundation_data["study_id"] = lmoid
    try:
      ObjectSchema().load(foundation_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = foundation_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(foundation_data)
    objects_in_db.append(lmo_object)

  # Define mooring lines
  objects_moorings = get_moorings(
      sk_data=sk_data,
      hierarchy_sk=hierarchy_sk
  )
  for key, mooring_data in objects_moorings.items():
    mooring_data["study_id"] = lmoid
    try:
      ObjectSchema().load(mooring_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = mooring_data["id_external"] + ': ' + msg
      return msg

    lmo_object = create_object(mooring_data)
    objects_in_db.append(lmo_object)

  return objects_in_db


def get_cables(
    lmoid: int,
    ed_data: dict,
    hierarchy_ed: dict
) -> dict:
  """
  Defines all cables of the project as a dictionary

  Uses the following functions:
    - :meth:`~dtop_lmo.service.api.components.lmo_components.subroutes_length()`

  Args:
    lmoid (int): the ID of the LMO study in local storage database to add objects
    ed_data (dict): all data from ED module
    hierarchy_ed (dict): Hierarchy from ED module

  Returns:
    dict: Dictionary with created cables and their attributes.
  """
  if ed_data:
    outputs_ed = ed_data
  else:
    outputs_ed = {}
  if hierarchy_ed is None:
    hierarchy_ed = {}

  # Define cables
  dict_cables = {}
  try:
    cables_design = outputs_ed["cable_dict"]
  except KeyError:
    cables_design = []
  try:
    umbilical_design = outputs_ed["umbilical_dict"]
  except KeyError:
    umbilical_design = []

  if cables_design is None:
    cables_design = []
  if umbilical_design is None:
    umbilical_design = []

  for cable in cables_design:
    cable_attr = {}

    # From ED design
    cable_attr['id_external'] = 'ed_' + str(cable.get('marker'))
    cable_attr['name'] = 'cable' + str(cable.get('marker'))
    cable_attr['type_'] = cable.get('type_')
    cable_attr['burial_method'] = outputs_ed.get('cable_installation')
    cable_attr['component_cost'] = cable.get('cost')
    cable_attr["diameter"] = cable.get('diameter')
    cable_attr["drymass"] = cable.get('mass')
    cable_attr["mbr"] = cable.get('mbr')

    bathymetry_floats = cable.get('layer_1_start')
    seabed_str = cable.get('layer_1_type')
    burial_floats = cable.get('burial_depth')
    split_bools = cable.get('split_pipe')

    # TODO: Check if the following can be considered. Check project assumptions
    # ###
    if bathymetry_floats is None:
      bathymetry_floats = []
    else:
      bathymetry_floats = bathymetry_floats[:-1]
    if seabed_str is None:
      seabed_str = []
    else:
      seabed_str = seabed_str[:-1]
    if len(burial_floats) == 1:
      # CPX1
      pass
    else:
      burial_floats = burial_floats[:-1]
    if len(split_bools) == 1:
      # CPX1
      pass
    else:
      split_bools = split_bools[:-1]

    # Cable subroutes length
    # TODO: cable_x and cable_y length should be +1 then len(split_pipes) ###
    list_x = cable.get('cable_x')
    list_y = cable.get('cable_y')
    if list_x is None or list_y is None:
      # CPX1
      list_length = [cable.get('length')]
    else:
      list_length = subroutes_length(list_x, list_y)

    # If there is any dynamic cable in ED design, buoyancy_modules should be
    # added to the cable
    if umbilical_design:
      # There is something related to dynamic cables in ED Design
      static_x_st = round(list_x[0])
      static_y_st = round(list_y[0])
      static_x_end = round(list_x[-1])
      static_y_end = round(list_y[-1])

      u_idx = 0
      while u_idx < len(umbilical_design):
        umbilical = umbilical_design[u_idx]
        dynam_seabed_conn_x = umbilical.get('seabed_connection_point')[0]
        dynam_seabed_conn_y = umbilical.get('seabed_connection_point')[1]
        dynam_seabed_conn_x = round(dynam_seabed_conn_x)
        dynam_seabed_conn_y = round(dynam_seabed_conn_y)
        if ((static_x_st == dynam_seabed_conn_x and
             static_y_st == dynam_seabed_conn_y) or
            (static_x_end == dynam_seabed_conn_x and
             static_y_end == dynam_seabed_conn_y)):
          # This umbilical connects to this static
          # Add this part to the cable already defined
          list_length.append(umbilical.get('length'))
          bathymetry_floats.append(0)
          seabed_str.append(seabed_str[-1])
          burial_floats.append(0)
          split_bools.append(False)
          list_buoys = ['False'] * len(split_bools)
          list_buoys.append('True')
          buoys_str = '[' + ', '.join(list_buoys) + ']'
          cable_attr['buoyancy_modules'] = buoys_str

          # Add as cable for maintenance but ir won't be installed alone
          # From ED Hierarchy
          name_of_node = str(umbilical.get('marker')).lower()
          name_of_nodes = hierarchy_ed.get('name_of_node')
          name_of_nodes = [str(name).lower() for name in name_of_nodes]
          idx = name_of_nodes.index(name_of_node)

          system = hierarchy_ed.get('system')[idx].lower()
          parent_list = hierarchy_ed.get('parent')[idx]
          child_list = hierarchy_ed.get('child')[idx]
          try:
            parent = parent_list.lower()
          except AttributeError:
            parent = ', '.join(parent_list).lower()
          try:
            child = child_list.lower()
          except AttributeError:
            child = ', '.join(child_list).lower()
          failure_repair = hierarchy_ed.get('failure_rate_repair')[idx]
          failure_replace = hierarchy_ed.get('failure_rate_replacement')[idx]
          # Check if failure is "NA"
          try:
            failure_repair = float(failure_repair)
            failure_repair = str(failure_repair)  # Convert to string for DB storage
            try:
              failure_replace = float(failure_replace)
              failure_replace = str(failure_replace)
              failures = '(' + failure_repair + ', ' + failure_replace + ')'
            except ValueError:
              failures = None
          except ValueError:
            try:
              failure_replace = float(failure_replace)
              failure_replace = str(failure_replace)
              failures = '(' + failure_repair + ', ' + failure_replace + ')'
            except ValueError:
              failures = None

          cable_attr["diameter"] = umbilical.get('diameter')
          cable_attr["drymass"] = umbilical.get('mass')
          cable_attr["mbr"] = umbilical.get('mbr')

          dict_cables['ed_' + str(umbilical.get('marker'))] = {
              "id_external": 'ed_' + str(umbilical.get('marker')),
              "name": 'umbilical' + str(umbilical.get('marker')),
              "type_": 'array_maintenance',
              "length": '[' + str(umbilical.get('length')) + ']',
              "buoyancy_modules": '[True]',
              "bathymetry": '[0]',
              "component_cost": umbilical.get('cost'),
              "system": system,
              "parent": parent,
              "child": child,
              "failure_rates": failures,
              "diameter": umbilical.get('diameter'),
              "drymass": umbilical.get('mass'),
              "mbr": umbilical.get('mbr')
          }
          del umbilical_design[u_idx]
          break
        else:
          # Does not connect
          pass
        u_idx += 1

    # Convert lists to strings
    if len(bathymetry_floats) == 0:
      # CPX1
      bathymetry_str = None
    else:
      bathymetry_str = [str(abs(round(val, 1))) for val in bathymetry_floats]
      bathymetry_str = '[' + ', '.join(bathymetry_str) + ']'
    cable_attr['bathymetry'] = bathymetry_str

    if seabed_str is None:
      # CPX1
      seabed_str = None
    else:
      seabed_str = [val.lower() for val in seabed_str]
      seabed_str = '[' + ', '.join(seabed_str) + ']'
    cable_attr['seabed_type'] = seabed_str

    length_str = [str(round(val, 1)) for val in list_length]
    length_str = '[' + ', '.join(length_str) + ']'
    cable_attr['length'] = length_str

    burial_str = [str(round(val, 1)) for val in burial_floats]
    burial_str = '[' + ', '.join(burial_str) + ']'
    cable_attr['burial_depth'] = burial_str

    split_str = [str(val) for val in split_bools]
    split_str = '[' + ', '.join(split_str) + ']'
    cable_attr['split_pipes'] = split_str

    # From ED Hierarchy
    name_of_node = str(cable.get('marker')).lower()
    name_of_nodes = hierarchy_ed.get('name_of_node')
    name_of_nodes = [str(name).lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_ed.get('system')[idx].lower()
    parent_list = hierarchy_ed.get('parent')[idx]
    child_list = hierarchy_ed.get('child')[idx]
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_ed.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_ed.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        failures = None
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        failures = None

    cable_attr["system"] = system
    cable_attr["parent"] = parent
    cable_attr["child"] = child
    cable_attr["failure_rates"] = failures

    dict_cables[cable_attr['id_external']] = cable_attr

  # If there is any dynamic cable still, should be added as a cable
  if umbilical_design:
    for cable in umbilical_design:
      cable_attr = {}

      # From ED design
      cable_attr['id_external'] = 'ed_' + str(cable.get('marker'))
      cable_attr['name'] = 'umbilical' + str(cable.get('marker'))
      cable_attr['type_'] = 'array'
      cable_attr['buoyancy_modules'] = ['True']
      cable_attr['component_cost'] = cable.get('cost')
      cable_attr['length'] = '[' + str(cable.get('length')) + ']'
      cable_attr['bathymetry'] = '[0]'

      cable_attr["diameter"] = cable.get('diameter')
      cable_attr["drymass"] = cable.get('dry_mass')
      cable_attr["mbr"] = cable.get('mbr')

      # From ED Hierarchy
      name_of_node = str(cable.get('marker')).lower()
      name_of_nodes = hierarchy_ed.get('name_of_node')
      name_of_nodes = [str(name).lower() for name in name_of_nodes]
      idx = name_of_nodes.index(name_of_node)

      system = hierarchy_ed.get('system')[idx].lower()
      parent_list = hierarchy_ed.get('parent')[idx]
      child_list = hierarchy_ed.get('child')[idx]
      try:
        parent = parent_list.lower()
      except AttributeError:
        parent = ', '.join(parent_list).lower()
      try:
        child = child_list.lower()
      except AttributeError:
        child = ', '.join(child_list).lower()
      failure_repair = hierarchy_ed.get('failure_rate_repair')[idx]
      failure_replace = hierarchy_ed.get('failure_rate_replacement')[idx]
      # Check if failure is "NA"
      try:
        failure_repair = float(failure_repair)
        failure_repair = str(failure_repair)  # Convert to string for DB storage
        try:
          failure_replace = float(failure_replace)
          failure_replace = str(failure_replace)
          failures = '(' + failure_repair + ', ' + failure_replace + ')'
        except ValueError:
          failures = None
      except ValueError:
        try:
          failure_replace = float(failure_replace)
          failure_replace = str(failure_replace)
          failures = '(' + failure_repair + ', ' + failure_replace + ')'
        except ValueError:
          failures = None

      cable_attr["system"] = system
      cable_attr["parent"] = parent
      cable_attr["child"] = child
      cable_attr["failure_rates"] = failures

      dict_cables[cable_attr['id_external']] = cable_attr

  cables_in_db = []
  # Add cables to database
  for key, cable_data in dict_cables.items():
    cable_data["study_id"] = lmoid
    try:
      CableSchema().load(cable_data)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      msg = cable_data["id_external"] + ': ' + msg
      return msg

    lmo_cable = create_cable(cable_data)
    cables_in_db.append(lmo_cable)

  return cables_in_db


def get_device_objects(
    siteid: int,
    mc_data: dict,
    ec_data: dict,
    et_data: dict
) -> dict:
  """
  Defines all objects of the project as a dictionary

  Uses the following functions:
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_bathymetry()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_seabed_type()`

  Args:
    siteid (int): ID of the study Site
    mc_data (dict): all data from MC module
    ec_data (dict): all data from EC module
    et_data (dict): all data from ET module

  Returns:
    dict: Dictionary with devices and their attributes.
  """
  if mc_data:
    design_mc_general = mc_data["general"]
    design_mc_dim = mc_data["dimensions"]
  else:
    design_mc_general = {}
    design_mc_dim = {}
  if ec_data:
    design_ec = ec_data
  else:
    design_ec = {}
  if et_data:
    design_et_devices = et_data["devices"]
  else:
    design_et_devices = {}

  try:
    number_devices = design_ec["number_devices"]
  except KeyError:
    number_devices = 0

  # Define devices
  dict_devices = {}
  for id_ in range(0, number_devices):
    # From EC design
    device_attr = {}
    device_id = str(design_ec["layout"]["deviceID"][id_])
    device_attr["id_external"] = 'device_' + device_id
    device_attr["name"] = 'Device' + device_id

    lat, lon = get_latlon_from_utm(
        siteid=siteid,
        easting=design_ec["layout"]["easting"][id_],
        northing=design_ec["layout"]["northing"][id_]
    )
    device_attr["coordinates"] = '(' + str(lat) + ', ' + str(lon) + ')'

    # From MC design
    device_floating = design_mc_general.get('floating')
    if device_floating is True:
      device_attr["type_"] = 'float'
    else:
      device_attr["type_"] = 'fixed'
    device_attr["length"] = design_mc_dim.get('length')
    machine_mass = design_mc_dim.get('mass')
    device_attr["width"] = design_mc_dim.get('width')
    device_attr["height"] = design_mc_dim.get('height')
    device_attr["diameter"] = design_mc_dim.get('diameter')
    device_attr["density"] = design_mc_dim.get('density')
    device_attr["material"] = design_mc_dim.get('material')
    machine_cost = design_mc_general.get('machine_cost')

    # From ET
    # Pick a device
    design_device = design_et_devices[0]
    device_pto_mass = design_device["Dev_PTO_mass"]["value"]
    device_cost = design_device["Dev_PTO_cost"]["value"]

    device_attr["drymass"] = machine_mass + device_pto_mass
    device_attr["component_cost"] = machine_cost + device_cost

    # From SC
    bathymetry = get_bathymetry(siteid, lat, lon)
    device_attr["bathymetry"] = bathymetry
    seabed_type = get_seabed_type(siteid, lat, lon)
    device_attr["seabed_type"] = seabed_type

    device_attr["system"] = 'et'    # TODO: CHECK IF DEVICE SYSTEM IS ET

    dict_devices[str(id_)] = device_attr

  return dict_devices


def get_pto_objects(et_data: dict, hierarchy_et: dict) -> dict:
  if et_data:
    design_devices = et_data["devices"]
  else:
    design_devices = []
  if hierarchy_et is not None:
    et_hierarchy = hierarchy_et
  else:
    et_hierarchy = {}

  dict_ptos = {}

  # Define PTOs - main
  for idx, design_device in enumerate(design_devices):
    device_id = str(idx + 1)

    # Get this device PTOs
    ptos_design = design_device["ptos"]

    for pto_design in ptos_design:
      pto_attr = {}
      pto_num = str(pto_design["id"]["value"])

      pto_attr["id_external"] = 'et_' + device_id + '_' + pto_num
      pto_attr["name"] = 'ET' + device_id + '_' + pto_num
      pto_attr["type_"] = 'pto'
      pto_elec_mass = pto_design.get('Elect_mass')["value"]
      pto_grid_mass = pto_design.get('Grid_mass')["value"]
      pto_mech_mass = pto_design.get('Mech_mass')["value"]
      pto_attr["drymass"] = pto_elec_mass + pto_grid_mass + pto_mech_mass
      pto_elec_cost = pto_design.get('Elect_cost')["value"]
      pto_grid_cost = pto_design.get('Grid_cost')["value"]
      pto_mech_cost = pto_design.get('Mech_cost')["value"]
      pto_attr["component_cost"] = pto_elec_cost + pto_grid_cost + pto_mech_cost

      # From ET Hierarchy
      name_of_node = pto_attr["name"]
      name_of_nodes = et_hierarchy.get('name_of_node')
      name_of_nodes = [str(name).lower() for name in name_of_nodes]
      idx = name_of_nodes.index(name_of_node.lower())

      system = et_hierarchy.get('system')[idx].lower()
      parent_list = et_hierarchy.get('parent')[idx]
      child_list = et_hierarchy.get('child')[idx]
      try:
        parent = parent_list.lower()
      except AttributeError:
        parent = ', '.join(parent_list).lower()
      try:
        child = child_list.lower()
      except AttributeError:
        child = ', '.join(child_list).lower()
      failure_repair = et_hierarchy.get('failure_rate_repair')[idx]
      failure_replace = et_hierarchy.get('failure_rate_replacement')[idx]
      # Check if failure is "NA"
      try:
        failure_repair = float(failure_repair)
        failure_repair = str(failure_repair)  # Convert to string for DB storage
        try:
          failure_replace = float(failure_replace)
          failure_replace = str(failure_replace)
          failures = '(' + failure_repair + ', ' + failure_replace + ')'
        except ValueError:
          _e = 'For component %s both or none FR must be defined in the Hierarchy' % pto_attr["id_external"]
          raise AssertionError(_e)
      except ValueError:
        try:
          failure_replace = float(failure_replace)
          _e = 'For component %s both or none FR must be defined in the Hierarchy' % pto_attr["id_external"]
          raise AssertionError(_e)
        except ValueError:
          failures = None

      pto_attr["system"] = system
      pto_attr["parent"] = parent
      pto_attr["child"] = child
      pto_attr["failure_rates"] = failures

      id_ = pto_attr["id_external"]

      dict_ptos[str(id_)] = pto_attr

  # Define other PTO parts
  # Loop through ET hierarchy
  try:
    hierarchy_nodes = et_hierarchy["name_of_node"]
  except KeyError:
    hierarchy_nodes = []
  for idx, name_of_node in enumerate(hierarchy_nodes):
    part_attr = {}

    dev_id = re.search('et(.*)_pto', name_of_node.lower())
    pto_id = re.search('pto_(.*)_', name_of_node.lower())

    try:
      dev_id = dev_id.group(1)
      pto_id = pto_id.group(1)

      # Get this device PTOs design
      for design_device in design_devices:
        if design_device["id"]["value"] == dev_id:
          ptos_design = design_device["ptos"]
          break

      if 'mech' in name_of_node.lower():
        key_word = 'Mech'
      elif 'elect' in name_of_node.lower():
        key_word = 'Elect'
      elif 'grid' in name_of_node.lower():
        key_word = 'Grid'
      else:
        raise AttributeError()

      part_attr["id_external"] = 'et_' + name_of_node.lower()
      part_attr["name"] = key_word.lower()
      part_attr["type_"] = 'pto_' + key_word.lower()
      # Find parts drymass and cost
      for pto_design in ptos_design:
        if pto_design["id"]["value"].lower() == 'pto_' + pto_id:
          part_attr["drymass"] = pto_design[key_word + "_mass"]["value"]
          part_attr["component_cost"] = pto_design[key_word + "_cost"]["value"]
          break

      part_attr["system"] = et_hierarchy["system"][idx].lower()
      parent_list = et_hierarchy["parent"][idx]
      child_list = et_hierarchy["child"][idx]
      try:
        parent = parent_list.lower()
      except AssertionError:
        parent = ', '.join(parent_list).lower()
      try:
        child = child_list.lower()
      except AssertionError:
        parent = ', '.join(child_list).lower()
      part_attr["parent"] = parent
      part_attr["child"] = child
      failure_repair = et_hierarchy["failure_rate_repair"][idx]
      failure_replace = et_hierarchy["failure_rate_replacement"][idx]
      # Check if failure is "NA"
      try:
        failure_repair = float(failure_repair)
        failure_repair = str(failure_repair)  # Convert to string for DB storage
        try:
          failure_replace = float(failure_replace)
          failure_replace = str(failure_replace)
        except ValueError:
          failure_replace = '0'
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        failure_repair = '0'
        try:
          failure_replace = float(failure_replace)
          failure_replace = str(failure_replace)
          failures = '(' + failure_repair + ', ' + failure_replace + ')'
        except ValueError:
          failures = None
      part_attr["failure_rates"] = failures

      dict_ptos[part_attr["id_external"]] = part_attr

    except AttributeError:
      pass

  return dict_ptos


def get_collection_points(
    siteid: int,
    ed_data: dict,
    hierarchy_ed: dict
) -> dict:
  """
  Defines all collection points of the project as a dictionary

  Uses the following functions:
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_bathymetry()`
  - :meth:`~dtop_lmo.service.api.components.lmo_components.get_seabed_type()`

  Args:
    siteid (int): ID of the study Site
    ed_data (dict): all data from ED module
    hierarchy_ed (dict): ED module hierarchy

  Returns:
    dict: Dictionary with collection points and their attributes.
  """
  if ed_data:
    outputs_ed = ed_data
  else:
    outputs_ed = {}
  if hierarchy_ed is None:
    hierarchy_ed = {}

  # Define collection points
  dict_collection = {}
  try:
    collection_design = outputs_ed["collection_point_dict"]
  except KeyError:
    collection_design = []

  if collection_design is None:
    collection_design = []

  for cp in collection_design:
    cp_attr = {}

    # From ED design
    cp_attr['id_external'] = 'ed_' + str(cp.get('marker'))
    cp_attr['name'] = 'collection_point' + str(cp.get('marker'))
    cp_attr['type_'] = cp.get('type_')
    cp_location = cp.get('location')
    if cp_location is None:
      # CPX1
      cp_attr["bathymetry"] = None
      cp_attr["seabed_type"] = None
    else:
      cp_attr["coordinates"] = '[' + str(cp_location[0]) + ', ' + \
          str(cp_location[1]) + ']'
      # From SC
      bathymetry = get_bathymetry(
          siteid=siteid,
          lat=cp.get('location')[0],
          lon=cp.get('location')[1]
      )
      cp_attr["bathymetry"] = bathymetry
      seabed_type = get_seabed_type(
          siteid=siteid,
          lat=cp.get('location')[0],
          lon=cp.get('location')[1]
      )
      cp_attr["seabed_type"] = seabed_type
      cp_attr["length"] = cp.get('width')
      cp_attr["width"] = cp.get('width')
      cp_attr["height"] = cp.get('height')
      ei_input = cp.get('input_connectors')
      ei_output = cp.get('output_connectors')
      cp_attr["electrical_interfaces"] = '[' + ei_input + ', ' + ei_output + ']'

    cp_attr["drymass"] = cp.get('mass')
    cp_attr["component_cost"] = float(cp.get('cost'))

    # From ED Hierarchy
    name_of_node = str(cp.get('marker')).lower()
    name_of_nodes = hierarchy_ed.get('name_of_node')
    name_of_nodes = [str(name).lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_ed.get('system')[idx].lower()
    parent_list = hierarchy_ed.get('parent')[idx]
    child_list = hierarchy_ed.get('child')[idx]
    # Lower all list entries
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_ed.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_ed.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % cp_attr["id_external"]
        raise AssertionError(_e)
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % cp_attr["id_external"]
        raise AssertionError(_e)
      except ValueError:
        failures = None

    cp_attr["system"] = system
    cp_attr["parent"] = parent
    cp_attr["child"] = child
    cp_attr["failure_rates"] = failures

    dict_collection[cp_attr['id_external']] = cp_attr

  return dict_collection


def get_connectors(ed_data: dict, hierarchy_ed: dict) -> dict:
  """
  Defines all connectors of the project as a dictionary

  Args:
    ed_data (dict): all data from ED module
    hierarchy_ed (dict): ED module hierarchy

  Returns:
      dict: Dictionary with connectors and their attributes.
  """
  if ed_data:
    outputs_ed = ed_data
  else:
    outputs_ed = {}
  if hierarchy_ed is None:
    hierarchy_ed = {}

  # Define connectors
  dict_connectors = {}
  try:
    connectors_design = outputs_ed["connectors_dict"]
  except KeyError:
    connectors_design = []

  if connectors_design is None:
    connectors_design = []

  for connector in connectors_design:
    connector_attr = {}

    connector_attr['id_external'] = 'ed_' + str(connector.get('marker'))
    connector_attr['name'] = 'connector' + str(connector.get('marker'))
    connector_attr['type_'] = connector.get('type_')
    connector_attr['coordinates'] = str(connector.get('utm_x')) + ', ' + \
        str(connector.get('utm_y'))
    connector_attr['coordinates'] = '[' + connector_attr['coordinates'] + ']'
    connector_attr['component_cost'] = float(connector.get('cost'))

    connector_attr["length"] = connector.get('width')
    connector_attr["width"] = connector.get('width')
    connector_attr["height"] = connector.get('height')

    # From ED Hierarchy
    name_of_node = str(connector.get('marker')).lower()
    name_of_nodes = hierarchy_ed.get('name_of_node')
    name_of_nodes = [str(name).lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_ed.get('system')[idx].lower()
    parent_list = hierarchy_ed.get('parent')[idx]
    child_list = hierarchy_ed.get('child')[idx]
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_ed.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_ed.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % connector_attr["id_external"]
        raise AssertionError(_e)
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % connector_attr["id_external"]
        raise AssertionError(_e)
      except ValueError:
        failures = None

    connector_attr["system"] = system
    connector_attr["parent"] = parent
    connector_attr["child"] = child
    connector_attr["failure_rates"] = failures

    dict_connectors[connector_attr['id_external']] = connector_attr

  return dict_connectors


def get_anchors(siteid: int, sk_data: dict, hierarchy_sk: dict) -> dict:
  """
  Defines all anchors of the project as a dictionary

  Uses :meth:`~dtop_lmo.service.api.components.lmo_components.get_seabed_type()`

  Args:
    siteid (int): ID of the study Site
    sk_data (dict): all data from SK module
    hierarchy_sk (dict): SK module hierarchy

  Returns:
      dict: Dictionary with anchors and their attributes.
  """
  if sk_data:
    design_sk = sk_data
  else:
    design_sk = {}
  if hierarchy_sk is None:
    hierarchy_sk = {}

  # Define anchors
  dict_anchors = {}
  try:
    anchors_design = design_sk["anchor_list"]
  except KeyError:
    anchors_design = []

  for anchor in anchors_design:
    anchor_attr = {}

    anchor_attr["id_external"] = 'sk_' + str(anchor.get('design_id')).lower()
    anchor_attr["name"] = 'anchor_' + str(anchor.get('design_id')).lower()
    anchor_attr["length"] = anchor.get('length')
    anchor_attr["width"] = anchor.get('width')
    anchor_attr["height"] = anchor.get('height')
    anchor_attr["drymass"] = anchor.get('mass')
    anchor_attr["component_cost"] = anchor.get('cost')

    achor_type = anchor.get('type')
    if 'drag' in achor_type:
      anchor_attr["type_"] = 'drag-embedment'
    elif 'direct' in achor_type:
      anchor_attr["type_"] = 'direct-embedment'
    elif 'hydro' in achor_type:
      anchor_attr["type_"] = 'hydrojetting'
    elif 'pre' in achor_type:
      anchor_attr["type_"] = 'pre-installed'
    else:
      anchor_attr["type_"] = 'other'

    coordinates_float = anchor.get('coordinates')
    lat = coordinates_float[0]
    lon = coordinates_float[1]
    bath = abs(coordinates_float[2])
    anchor_attr["coordinates"] = '(' + str(lat) + ', ' + str(lon) + ')'
    anchor_attr["bathymetry"] = bath
    anchor_attr["seabed_type"] = get_seabed_type(siteid, lat, lon)

    upstream_ids = anchor.get('upstream_id')
    anchor_attr["upstream"] = '[' + ', '.join(upstream_ids) + ']'

    # From Hierarchies
    name_of_node = anchor_attr["id_external"][3:]
    name_of_nodes = hierarchy_sk.get('name_of_node')
    name_of_nodes = [name.lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_sk.get('system')[idx].lower()
    parent_list = hierarchy_sk.get('parent')[idx]
    child_list = hierarchy_sk.get('child')[idx]
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_sk.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_sk.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % anchor_attr["id_external"]
        raise AssertionError(_e)
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % anchor_attr["id_external"]
        raise AssertionError(_e)
      except ValueError:
        failures = None

    anchor_attr["system"] = system
    anchor_attr["parent"] = parent
    anchor_attr["child"] = child
    anchor_attr["failure_rates"] = failures

    dict_anchors[anchor_attr["id_external"]] = anchor_attr

  return dict_anchors


def get_foundations(siteid: int, sk_data: dict, hierarchy_sk: dict) -> dict:
  """
  Defines all foundations of the project as a dictionary

  Uses :meth:`~dtop_lmo.service.api.components.lmo_components.get_seabed_type()`

  Args:
    siteid (int): ID of the study Site
    sk_data (dict): all data from SK module
    hierarchy_sk (dict): SK module hierarchy

  Returns:
      dict: Dictionary with foundations and their attributes.
  """
  if sk_data:
    design_sk = sk_data
  else:
    design_sk = {}
  if hierarchy_sk is None:
    hierarchy_sk = {}

  # Define foundations
  dict_foundations = {}
  try:
    foundations_design = design_sk["foundation_list"]
  except KeyError:
    foundations_design = []

  for foundation in foundations_design:
    foundation_attr = {}

    foundation_attr["id_external"] = 'sk_' + str(foundation.get('design_id')).lower()
    foundation_attr["name"] = 'foundation_' + str(foundation.get('design_id')).lower()
    found_name = foundation.get('design_id')
    # Foundation type
    if 'pile' in found_name:
      foundation_attr["type_"] = 'pile'
    elif 'gravity' in found_name:
      foundation_attr["type_"] = 'gravity base'
    elif 'suction' in found_name:
      foundation_attr["type_"] = 'suction caisson'
    else:
      foundation_attr["type_"] = foundation.get('type')
    foundation_attr["length"] = foundation.get('length')
    foundation_attr["height"] = foundation.get('height')
    foundation_attr["diameter"] = foundation.get('width')
    if foundation_attr["diameter"] is None:
      foundation_attr["width"] = foundation.get('width')
    foundation_attr["drymass"] = foundation.get('mass')
    foundation_attr["burial_depth"] = foundation.get('buried_height')
    foundation_attr["component_cost"] = foundation.get('cost')

    coordinates_float = foundation.get('coordinates')
    lat = coordinates_float[0]
    lon = coordinates_float[1]
    bath = abs(coordinates_float[2])
    foundation_attr["coordinates"] = '(' + str(lat) + ', ' + str(lon) + ')'
    foundation_attr["bathymetry"] = bath
    foundation_attr["seabed_type"] = get_seabed_type(siteid, lat, lon)

    upstream_ids = foundation.get('upstream_id')
    foundation_attr["upstream"] = '[' + ', '.join(upstream_ids) + ']'

    # From Hierarchies
    name_of_node = foundation_attr["id_external"][3:]
    name_of_nodes = hierarchy_sk.get('name_of_node')
    name_of_nodes = [name.lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_sk.get('system')[idx].lower()
    parent_list = hierarchy_sk.get('parent')[idx]
    child_list = hierarchy_sk.get('child')[idx]
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_sk.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_sk.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % foundation_attr["id_external"]
        raise AssertionError(_e)
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        failure_repair = float(failure_replace)   # assume same failure
        failure_replace = str(failure_replace)
        failure_repair = str(failure_repair)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
        # _e = 'For component %s both or none FR must be defined in the Hierarchy' % foundation_attr["id_external"]
        # raise AssertionError(_e)
      except ValueError:
        failures = None

    foundation_attr["system"] = system
    foundation_attr["parent"] = parent
    foundation_attr["child"] = child
    foundation_attr["failure_rates"] = failures

    dict_foundations[foundation_attr["id_external"]] = foundation_attr

  return dict_foundations


def get_moorings(sk_data: dict, hierarchy_sk: dict) -> dict:
  """
  Defines all moorings of the project as a dictionary

  Args:
      sk_data (dict): all data from SK module
      hierarchy_sk (dict): SK module hierarchy

  Returns:
      dict: Dictionary with moorings and their attributes.
  """
  if sk_data:
    design_sk = sk_data
  else:
    design_sk = {}
  if hierarchy_sk is None:
    hierarchy_sk = {}

  # Define mooring lines
  dict_moorings = {}
  try:
    moorings_design = design_sk["line_segment_list"]
  except KeyError:
    moorings_design = []

  for mooring in moorings_design:
    mooring_attr = {}

    mooring_attr["id_external"] = 'sk_' + str(mooring.get('design_id')).lower()
    mooring_attr["name"] = 'line_' + str(mooring.get('design_id')).lower()
    mooring_attr["type_"] = 'mooring_line'
    mooring_attr["length"] = mooring.get('length')
    mooring_attr["diameter"] = mooring.get('diameter')
    mooring_attr["drymass"] = mooring.get('total_mass')
    mooring_attr["material"] = mooring.get('material')
    mooring_attr["component_cost"] = mooring.get('cost')

    # From Hierarchies
    name_of_node = mooring_attr["id_external"][3:]
    name_of_nodes = hierarchy_sk.get('name_of_node')
    name_of_nodes = [name.lower() for name in name_of_nodes]
    idx = name_of_nodes.index(name_of_node)

    system = hierarchy_sk.get('system')[idx].lower()
    parent_list = hierarchy_sk.get('parent')[idx]
    child_list = hierarchy_sk.get('child')[idx]
    try:
      parent = parent_list.lower()
    except AttributeError:
      parent = ', '.join(parent_list).lower()
    try:
      child = child_list.lower()
    except AttributeError:
      child = ', '.join(child_list).lower()
    failure_repair = hierarchy_sk.get('failure_rate_repair')[idx]
    failure_replace = hierarchy_sk.get('failure_rate_replacement')[idx]
    # Check if failure is "NA"
    try:
      failure_repair = float(failure_repair)
      failure_repair = str(failure_repair)  # Convert to string for DB storage
      try:
        failure_replace = float(failure_replace)
        failure_replace = str(failure_replace)
        failures = '(' + failure_repair + ', ' + failure_replace + ')'
      except ValueError:
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % mooring_attr["id_external"]
        raise AssertionError(_e)
    except ValueError:
      try:
        failure_replace = float(failure_replace)
        _e = 'For component %s both or none FR must be defined in the Hierarchy' % mooring_attr["id_external"]
        raise AssertionError(_e)
      except ValueError:
        failures = None

    mooring_attr["system"] = system
    mooring_attr["parent"] = parent
    mooring_attr["child"] = child
    mooring_attr["failure_rates"] = failures

    dict_moorings[mooring_attr["id_external"]] = mooring_attr

  return dict_moorings


def get_latlon_from_utm(
    siteid: int,
    easting: float,
    northing: float
) -> tuple:
  site = Site.query.get(siteid)
  site_lat = site.latitude
  site_lon = site.longitude

  utm_data = utm.from_latlon(site_lat, site_lon)
  utm_zone_number = utm_data[2]
  utm_zone_letter = utm_data[3]

  lat_lon = utm.to_latlon(easting, northing, utm_zone_number, utm_zone_letter)
  return lat_lon


def get_bathymetry(
    siteid: int,
    lat: float,
    lon: float
) -> float:
  """
  Get bathymetry value for a given coordinate

  Args:
      siteid (int): ID of the study Site
      lat (float): latitude
      lon (float): longitude

  Returns:
      float: closest value of bathymetry for lat and lon inputs
  """
  site = Site.query.get(siteid)
  dict_bathymetry = site.bathymetry

  # Save and convert SC data
  latitude_str = dict_bathymetry.get('latitude')
  longitude_str = dict_bathymetry.get('longitude')
  bath_value = dict_bathymetry.get('value')
  latitude_float = [float(value) for value in latitude_str]
  longitude_float = [float(value) for value in longitude_str]

  # Get closest value of latitude and longitude
  lat_closest_value = min(latitude_float, key=lambda x: abs(x - lat))
  lon_closest_value = min(longitude_float, key=lambda x: abs(x - lon))
  # Get that indexes of that value
  lat_index_multiple = [idx
                        for idx in range(0, len(latitude_float))
                        if latitude_float[idx] == lat_closest_value]
  lon_index_multiple = [idx
                        for idx in range(0, len(longitude_float))
                        if longitude_float[idx] == lon_closest_value]
  # Find matching indexes
  idx_found = False
  for idx_lat in lat_index_multiple:
    for idx_lon in lon_index_multiple:
      if idx_lat == idx_lon:
        idx_found = True
        break
    if idx_found is True:
      break

  bathymetry = float(bath_value[idx_lat])
  return bathymetry


def get_seabed_type(
    siteid: int,
    lat: float,
    lon: float
) -> str:
  """
  Get seabed type for a given coordinate

  Args:
      siteid (int): ID of the study Site
      lat (float): latitude
      lon (float): longitude

  Returns:
      str: closest type of seabed for lat and lon inputs
  """
  site = Site.query.get(siteid)
  dict_seabed = site.seabed_type

  latitude_str = dict_seabed.get('latitude')
  longitude_str = dict_seabed.get('longitude')
  seabed_value = dict_seabed.get('value')
  latitude_float = [float(value) for value in latitude_str]
  longitude_float = [float(value) for value in longitude_str]

  # Get closest value of latitude and longitude
  lat_closest_value = min(latitude_float, key=lambda x: abs(x - lat))
  lon_closest_value = min(longitude_float, key=lambda x: abs(x - lon))
  # Get that indexes of that value
  lat_index_multiple = [idx
                        for idx in range(0, len(latitude_float))
                        if latitude_float[idx] == lat_closest_value]
  lon_index_multiple = [idx
                        for idx in range(0, len(longitude_float))
                        if longitude_float[idx] == lon_closest_value]
  # Find matching indexes
  idx_found = False
  for idx_lat in lat_index_multiple:
    for idx_lon in lon_index_multiple:
      if idx_lat == idx_lon:
        idx_found = True
        break
    if idx_found is True:
      break

  seabed_type = seabed_value[idx_lat].lower()
  return seabed_type


def subroutes_length(x: list, y: list) -> list:
  """
  Calculates distances between two point of a list of coordinates

  Uses :meth:`math.hypot()`

  Args:
      x (list): list of x points
      y (list): list of y points

  Raises:
      AssertionError: raises an error if x an y does not have the same length

  Returns:
      list: list of distances between all points
  """
  if len(x) != len(y):
    raise AssertionError('Both list must have the same length.')

  pts = zip(x, y)
  pts = [pt for pt in zip(x, y)]

  distances = [math.hypot(pts[i][0] - pts[i - 1][0], pts[i][1] - pts[i - 1][1])
               for i in range(1, len(pts))]

  return distances


def create_object(data):
  """
  Create a new LMO Object and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create the LMO Object object.

  Returns:
    dict: the newly created LMO Object instance
  """
  lmo_object_schema = ObjectSchema()
  lmo_object = lmo_object_schema.load(data)

  try:
    db.session.add(lmo_object)
    db.session.commit()
    return lmo_object
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def create_cable(data):
  """
  Create a new LMO Cable and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create the LMO Cable cable.

  Returns:
    dict: the newly created LMO Cable instance
  """
  lmo_cable_schema = CableSchema()
  lmo_cable = lmo_cable_schema.load(data)

  try:
    db.session.add(lmo_cable)
    db.session.commit()
    return lmo_cable
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_objects(lmoid):
  """
  Delete a Objects from the local storage database.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to delete objects

  Returns:
    str: Success message if objects are deleted; Error message if no objects exists.
  """
  try:
    db.session.query(Object).filter_by(study_id=lmoid).delete()
    db.session.commit()
    return 'Objects deleted'
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def delete_cables(lmoid):
  """
  Delete a Cables from the local storage database.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to delete cables

  Returns:
    str: Success message if cables are deleted; Error message if no cables exists.
  """
  try:
    db.session.query(Cable).filter_by(study_id=lmoid).delete()
    db.session.commit()
    return 'Cables deleted'
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def convert_object(
    object_sql: db.Model,
    cpx: str,
    topside_exists: bool = None
):
  """
  Converts a given object from SQLAlchemy Model format to a python native class

  Args:
    object_sql (db.Model): Object in SQLAlchemy Model format
    cpx (str): Project complexity
    topside_exists (bool, optional): Only for devices; if the topside is accessible. Defaults to None.

  Raises:
    AssertionError: Raise an exception if cpx is unknown.

  Returns:
    Object: Returns a python class of type Object.
  """

  Object = business.classes.Object_class.get_complexity(cpx)

  failures = None
  if object_sql.failure_rates is not None:
    # Convert failures rates
    failures_str = object_sql.failure_rates
    failures_str = failures_str.replace('NA', '0')    # TODO: Check if is ok
    failures_str = failures_str.replace('na', '0')    # TODO: Check if is ok
    failures_str = failures_str.replace('N/A', '0')   # TODO: Check if is ok
    failures_str = failures_str.replace('n/a', '0')   # TODO: Check if is ok
    failures_str = failures_str.replace('(', '').replace(')', '')
    failures_str = failures_str.replace(' ', '')
    failures_list = failures_str.split(',')
    failures = (float(failures_list[0]), float(failures_list[1]))
  if cpx == 'low':
    return Object(
        id_=object_sql.id_external,
        name_of_object=object_sql.name,
        type_of_object=object_sql.type_,
        system=object_sql.system,
        comp_cost=object_sql.component_cost,
        drymass=object_sql.drymass,
        bathymetry=object_sql.bathymetry,
        seabed_type=object_sql.seabed_type,
        upstream=object_sql.upstream,
        parent=object_sql.parent,
        child=object_sql.child,
        topside_exists=topside_exists,
        failure_rates=failures
    )
  # elif cpx == 'med':
  #   return Object(
  #       id_=object_sql.id_external,
  #       name_of_object=object_sql.name,
  #       type_of_object=object_sql.type_,
  #       system=object_sql.system,
  #       comp_cost=object_sql.component_cost,
  #       drymass=object_sql.drymass,
  #       bathymetry=object_sql.bathymetry,
  #       seabed_type=object_sql.seabed_type,
  #       upstream=object_sql.upstream,
  #       parent=object_sql.parent,
  #       child=object_sql.child,
  #       topside_exists=topside_exists,
  #       failure_rates=failures
  #   )
  elif cpx == 'high' or cpx == 'med':
    # TODO: CHECK ELECTRICAL INTERFACES
    if ('connector' in object_sql.name.lower() or 'collection' in object_sql.name.lower() or 
        'device' in object_sql.name.lower()):
      electric_interfaces = ['dry-mate']
    else:
      electric_interfaces = None
    return Object(
        id_=object_sql.id_external,
        name_of_object=object_sql.name,
        type_of_object=object_sql.type_,
        system=object_sql.system,
        comp_cost=object_sql.component_cost,
        length=object_sql.length,
        drymass=object_sql.drymass,
        width=object_sql.width,
        height=object_sql.height,
        diameter=object_sql.diameter,
        bathymetry=object_sql.bathymetry,
        seabed_type=object_sql.seabed_type,
        density=object_sql.density,
        base_area=object_sql.base_area,
        material=object_sql.material,
        upstream=object_sql.upstream,
        parent=object_sql.parent,
        child=object_sql.child,
        burial_depth=object_sql.burial_depth,
        elect_interfaces=electric_interfaces,
        coordinates=object_sql.coordinates,
        topside_exists=topside_exists,
        failure_rates=failures
    )
  raise AssertionError('Complexity not found')


def convert_cable(
    cable_sql: db.Model,
    cpx: str
):
  """
  Converts a given cable from SQLAlchemy Model format to a python native class

  Args:
    cable_sql (db.Model): Cable in SQLAlchemy Model format
    cpx (str): Project complexity

  Raises:
    AssertionError: Raise an exception if cpx is unknown.

  Returns:
    Cable: Returns a python class of type Cable.
  """

  Cable = business.classes.Cable_class.get_complexity(cpx)

  failures = None
  if cable_sql.failure_rates is not None:
    # Convert bathymetry from list of strings to a single float
    bathymetry_str = cable_sql.bathymetry
    if bathymetry_str is None:
      # CPX1
      pass
    else:
      bathymetry_str = bathymetry_str.replace('[', '')
      bathymetry_str = bathymetry_str.replace(']', '')
      bathymetry_str = bathymetry_str.replace(' ', '')
      bathymetry_list = bathymetry_str.split(',')
      try:
        bathymetry_list = [float(value for value in bathymetry_list)]
        bathymetry = float(sum(bathymetry_list) / len(bathymetry_list))
      except TypeError:
        bathymetry = float(bathymetry_list[0])
    # Convert length from list of strings to a single float
    length_list = cable_sql.length
    length_str = length_list.replace('[', '')
    length_str = length_str.replace(']', '')
    length_str = length_str.replace(' ', '')
    length_list = length_str.split(',')
    try:
      length_list = [float(value for value in length_list)]
      length = float(sum(length_list) / len(length_list))
    except TypeError:
      length = float(length_list[0])
    # Convert failures rates
    failures_str = cable_sql.failure_rates
    failures_str = failures_str.replace('NA', '0')    # TODO: Check if is ok
    failures_str = failures_str.replace('na', '0')    # TODO: Check if is ok
    failures_str = failures_str.replace('N/A', '0')   # TODO: Check if is ok
    failures_str = failures_str.replace('n/a', '0')   # TODO: Check if is ok
    failures_str = failures_str.replace('(', '').replace(')', '')
    failures_str = failures_str.replace(' ', '')
    failures_list = failures_str.split(',')
    failures = (float(failures_list[0]), float(failures_list[1]))
  if cpx == 'low':
    return Cable(
        id_=cable_sql.id_external,
        name=cable_sql.name,
        cable_type=cable_sql.type_,
        system=cable_sql.system,
        length=length,
        comp_cost=cable_sql.component_cost,
        failure_rates=failures
    )
  # elif cpx == 'med':
  #   return Cable(
  #       id_=cable_sql.id_external,
  #       name=cable_sql.name,
  #       cable_type=cable_sql.type_,
  #       system=cable_sql.system,
  #       comp_cost=cable_sql.component_cost,
  #       bathymetry=cable_sql.bathymetry,
  #       length=cable_sql.length,
  #       diameter=cable_sql.diameter,
  #       parent=cable_sql.parent,
  #       child=cable_sql.child,
  #       failure_rates=cable_sql.failure_rates
  #   )
  elif cpx == 'high' or cpx == 'med':
    # TODO: CHECK ELECTRICAL INTERFACES
    elect_interfaces = ['dry-mate']
    # Convert length from sting to list
    length_str = cable_sql.length.replace('[', '')
    length_str = length_str.replace(']', '')
    length = length_str.split(', ')

    # Convert bathymetry and seabed from sting to list
    bathymetry_str = cable_sql.bathymetry.replace('[', '')
    bathymetry_str = bathymetry_str.replace(']', '')
    bathymetry = bathymetry_str.split(', ')
    try:
      seabed_type_str = cable_sql.seabed_type.replace('[', '')
      seabed_type_str = seabed_type_str.replace(']', '')
      seabed_type = seabed_type_str.split(', ')
    except AttributeError:
      seabed_type = None

    # Convert burial depth from sting to list
    try:
      burial_str = cable_sql.burial_depth.replace('[', '')
      burial_str = burial_str.replace(']', '')
      burial = burial_str.split(', ')
    except AttributeError:
      burial = None

    return Cable(
        id_=cable_sql.id_external,
        name=cable_sql.name,
        cable_type=cable_sql.type_,
        system=cable_sql.system,
        comp_cost=cable_sql.component_cost,
        bathymetry=bathymetry,
        seabed_type=seabed_type,
        diameter=cable_sql.diameter,
        length=length,
        drymass=cable_sql.drymass,
        min_bend_radius=cable_sql.mbr,
        elect_interfaces=elect_interfaces,
        subtype=cable_sql.subtype,
        burial_depth=burial,
        burial_method=cable_sql.burial_method,
        split_pipes=cable_sql.split_pipes,
        buoyancy_modules=cable_sql.buoyancy_modules,
        parent=cable_sql.parent,
        child=cable_sql.child,
        failure_rates=failures
    )
    raise AssertionError('Complexity not found')
