# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from .lmo_components import get_objects, get_cables
from .lmo_components import delete_objects, delete_cables
from dtop_lmo.service.models import LMOStudy, Module, Object, Cable
from dtop_lmo.service.schemas import ObjectSchema, CableSchema

bp = Blueprint('api_components', __name__)


@bp.route('/objects', methods=['GET'])
def get_lmo_objects(lmoid):
  """
  Flask blueprint route for getting the objects of a single LMO study.

  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (str): the ID of the LMO study

  Returns:
    dict: the serialised response containing the selected LMO objects or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    # Look for objects
    lmo_objects_query = Object.query.filter_by(study_id=lmoid)
    lmo_object_schema = ObjectSchema(many=True)
    result = lmo_object_schema.dump(lmo_objects_query)

    return jsonify(result)
  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/objects', methods=['POST'])
def post_lmo_objects(lmoid):
  """
  Flask blueprint route to add objects to an Object.

  Calls the :meth:`~dtop_lmo.service.api.components.get_objects()` method.
  Returns 404 error if project ID does not exist.

  Returns:
    dict: the serialised response containing the Object added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    if len(lmo_study.modules) == 0:
      return bad_request('No information about external modules defined')

    module_mc = Module.query.filter_by(study_id=lmoid, module='mc').first()
    if module_mc is not None:
      module_mc_data = module_mc.design
    else:
      module_mc_data = None
    module_ec = Module.query.filter_by(study_id=lmoid, module='ec').first()
    if module_ec is not None:
      module_ec_data = module_ec.design
    else:
      module_ec_data = None
    module_et = Module.query.filter_by(study_id=lmoid, module='et').first()
    if module_et is not None:
      module_et_data = module_et.design
      hierarchy_et = module_et.hierarchy
    else:
      module_et_data = None
      hierarchy_et = None
    module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if module_ed is not None:
      module_ed_data = module_ed.design
      hierarchy_ed = module_ed.hierarchy
    else:
      module_ed_data = None
      hierarchy_ed = None
    module_sk = Module.query.filter_by(study_id=lmoid, module='sk').first()
    if module_sk is not None:
      module_sk_data = module_sk.design
      hierarchy_sk = module_sk.hierarchy
    else:
      module_sk_data = None
      hierarchy_sk = None

    if lmo_study.objects:
      objects_deleted = delete_objects(lmoid)
      if objects_deleted.lower() != 'objects deleted':
        return bad_request('Could not delete current objects')

    lmo_objects = get_objects(
        lmoid=lmoid,
        siteid=lmo_study.site.id,
        mc_data=module_mc_data,
        ec_data=module_ec_data,
        et_data=module_et_data,
        ed_data=module_ed_data,
        sk_data=module_sk_data,
        hierarchy_et=hierarchy_et,
        hierarchy_ed=hierarchy_ed,
        hierarchy_sk=hierarchy_sk
    )

    lmo_objects_str = [type(object_) is str
                       for object_ in lmo_objects]
    if any(lmo_objects_str) is True:
      # Error inside get_objects(). Some of the objects could not be created
      # Delete objects that, eventually, were created
      _e = lmo_objects
      del_msg = delete_objects(lmoid)
      if del_msg.lower() == 'objects deleted':
        return bad_request(_e + '\nCould not create all objects. All objects deleted')
      return bad_request(_e + '\nCould not create objects and could not rollback')

    lmo_object_schema = ObjectSchema(many=True)
    created_objects = lmo_object_schema.dump(lmo_objects)    

    result = {
        "created_objects": created_objects,
        "message": "Objects created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/cables', methods=['GET'])
def get_lmo_cables(lmoid):
  """
  Flask blueprint route for getting the cables of a single LMO study.

  Returns an error if this study ID does not have any cables;
  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (str): the ID of the LMO study

  Returns:
    dict: the serialised response containing the selected LMO cables or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    # Look for cables
    lmo_cables_query = Cable.query.filter_by(study_id=lmoid)
    lmo_cable_schema = CableSchema(many=True)
    result = lmo_cable_schema.dump(lmo_cables_query)

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/cables', methods=['POST'])
def post_lmo_cables(lmoid):
  """
  Flask blueprint route to add cables to an Cable.

  Calls the :meth:`~dtop_lmo.service.api.components.get_cables()` method.
  Returns 404 error if project ID does not exist.

  Returns:
    dict: the serialised response containing the Cable added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    if len(lmo_study.modules) == 0:
      return bad_request('No information about external modules found')

    module_ed = Module.query.filter_by(study_id=lmoid, module='ed').first()
    if module_ed is not None:
      module_ed_data = module_ed.design
      hierarchy_ed = module_ed.hierarchy
    else:
      module_ed_data = None
      hierarchy_ed = None

    if lmo_study.cables:
      cables_deleted = delete_cables(lmoid)
      if cables_deleted.lower() != 'cables deleted':
        return bad_request('Could not delete current cables')

    lmo_cables = get_cables(
        lmoid=lmoid,
        ed_data=module_ed_data,
        hierarchy_ed=hierarchy_ed
    )

    lmo_cables_str = [type(cable_) is str for cable_ in lmo_cables]
    if any(lmo_cables_str) is True:
      # Error inside get_cables(). Some of the cables could not be created
      # Delete cables that, eventually, were created
      _e = lmo_cables
      del_msg = delete_cables(lmoid)
      if del_msg.lower() == 'cables deleted':
        return bad_request(_e + '\nCould not create all cables. All cables deleted')
      return bad_request(_e + '\nCould not create cables and could not rollback')

    lmo_cable_schema = CableSchema(many=True)
    created_cables = lmo_cable_schema.dump(lmo_cables)

    result = {
        "created_cables": created_cables,
        "message": "Cables created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
