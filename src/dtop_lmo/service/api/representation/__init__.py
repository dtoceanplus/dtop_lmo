# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from flask import Blueprint, jsonify
import pandas as pd
import json

from ..errors import bad_request, not_found

from .lmo_representation import prepare_operations, prepare_logistic_solution
from dtop_lmo.service.models import LMOStudy, Phase, Operation
from dtop_lmo.service.schemas import (
    PhaseSchema, OperationSchema, ObjectSchema, CableSchema, ActivitySchema,
    MethodsSchema, RequirementsSchema,
    RepresentationSchema
)
from dtop_lmo.service.api.results import get_phase_plan


headers = {"Access-Control-Allow-Headers": "Content-Type"}


bp = Blueprint("api_representation", __name__)


@bp.route("/representation/<lmoid>", methods=["GET"])
def get_representation(lmoid):
  """Get the DR of a Study"""
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if not lmo_study.phases:
      return bad_request('LMO study does not have any phase defined.')

    representation = {
        "phase_installation": {},
        "phase_maintenance": {},
        "phase_decommissioning": {}
    }
    for lmo_phase in lmo_study.phases:
      operations = prepare_operations(lmo_phase.id)
      logistic_solution = prepare_logistic_solution(operations)

      if lmo_phase.name == 'installation':
        representation["phase_installation"]["operations"] = operations
        representation["phase_installation"]["logistic_solution"] = logistic_solution
      elif lmo_phase.name == 'maintenance':
        representation["phase_maintenance"]["operations"] = operations
        representation["phase_maintenance"]["logistic_solution"] = logistic_solution
      elif lmo_phase.name == 'decommissioning':
        representation["phase_decommissioning"]["operations"] = operations
        representation["phase_decommissioning"]["logistic_solution"] = logistic_solution

    representation_schema = RepresentationSchema()
    response = representation_schema.dump(representation)

    return jsonify(response)
  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
