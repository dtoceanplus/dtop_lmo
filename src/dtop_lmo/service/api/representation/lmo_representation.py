# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import pandas as pd
import json
from copy import deepcopy
import os

from dtop_lmo.service.models import LMOStudy, Phase, Operation
from dtop_lmo.service.schemas import (
    PhaseSchema, OperationSchema, ObjectSchema, CableSchema, ActivitySchema,
    MethodsSchema, RequirementsSchema,
    RepresentationSchema
)
from dtop_lmo.service.api.results import get_phase_plan

# Import functions
from dtop_lmo.service.api.catalogues import get_catalog_terminals
from dtop_lmo.service.api.catalogues import get_catalog_vecs
from dtop_lmo.service.api.catalogues import get_catalog_vessels
from dtop_lmo.service.api.catalogues import get_catalog_rovs
from dtop_lmo.service.api.catalogues import get_catalog_divers
from dtop_lmo.service.api.catalogues import get_catalog_equip_burial
from dtop_lmo.service.api.catalogues import get_catalog_equip_piling
from dtop_lmo.service.api.catalogues import get_catalog_equip_protect


def prepare_operations(phase_id):
  lmo_phase = Phase.query.get(phase_id)

  operation_schema = OperationSchema()
  methods_schema = MethodsSchema()
  requirements_schema = RequirementsSchema()
  object_schema = ObjectSchema()
  cable_schema = CableSchema()
  activity_schema = ActivitySchema(many=True)

  catalogue_path = os.path.join(os.getcwd(), 'catalogues')
  # Get vessels Catalogue
  try:
    df_vessels = get_catalog_vessels()
  except ConnectionError:
    path_catalogue_vessels = os.path.join(catalogue_path, 'Vessels.csv')
    df_vessels = pd.read_csv(path_catalogue_vessels, sep=',')
  df_vessels.dropna(how='all', inplace=True)
  # Get terminals Catalogue
  try:
    df_terminals = get_catalog_terminals()
  except ConnectionError:
    path_catalogue_terminals = os.path.join(catalogue_path, 'Terminals.csv')
    df_terminals = pd.read_csv(path_catalogue_terminals, sep=',', encoding="ISO-8859-1")
  df_terminals.dropna(how='all', inplace=True)
  # Get rov Catalogue
  try:
    df_rov = get_catalog_rovs()
  except ConnectionError:
    path_catalogue_rovs = os.path.join(catalogue_path, 'Equip_rov.csv')
    df_rov = pd.read_csv(path_catalogue_rovs, sep=',')
  df_rov.dropna(how='all', inplace=True)
  # Get divers Catalogue
  try:
    df_divers = get_catalog_divers()
  except ConnectionError:
    path_catalogue_divers = os.path.join(catalogue_path, 'Equip_divers.csv')
    df_divers = pd.read_csv(path_catalogue_divers, sep=',')
  df_divers.dropna(how='all', inplace=True)
  # Get burial Catalogue
  try:
    df_burial = get_catalog_equip_burial()
  except ConnectionError:
    path_catalogue_burial = os.path.join(catalogue_path, 'Equip_burial.csv')
    df_burial = pd.read_csv(path_catalogue_burial, sep=',')
  df_burial.dropna(how='all', inplace=True)
  # Get protect Catalogue
  try:
    df_protect = get_catalog_equip_protect()
  except ConnectionError:
    path_catalogue_protect = os.path.join(catalogue_path, 'Equip_protect.csv')
    df_protect = pd.read_csv(path_catalogue_protect, sep=',')
  df_protect.dropna(how='all', inplace=True)
  # Get piling Catalogue
  try:
    df_piling = get_catalog_equip_piling()
  except ConnectionError:
    path_catalogue_piling = os.path.join(catalogue_path, 'Equip_piling.csv')
    df_piling = pd.read_csv(path_catalogue_piling, sep=',')
  df_piling.dropna(how='all', inplace=True)

  lmo_operations = []
  for lmo_operation in lmo_phase.operations:
    operation = operation_schema.dump(lmo_operation)

    # Direct fields
    operation["activities"] = activity_schema.dump(lmo_operation.activities)
    for act_idx, act in enumerate(operation["activities"]):
      operation["activities"][act_idx]["id"] = act["id_external"]
      del operation["activities"][act_idx]["operation"]
      del operation["activities"][act_idx]["operation_id"]
      del operation["activities"][act_idx]["id_external"]
      del operation["activities"][act_idx]["light"]
      del operation["activities"][act_idx]["location"]
    op_methods = methods_schema.dump(lmo_operation.methods)
    operation["methods"] = op_methods
    op_requirements = requirements_schema.dump(lmo_phase.requirements)
    operation["requirements"] = op_requirements

    # Non-direct fields
    #   Objects
    operation["objects"] = []
    for lmo_object in lmo_operation.objects:
      object_id = lmo_object.id_external
      operation["objects"].append(object_id)
    operation["cables"] = []
    #   Cables
    for lmo_cable in lmo_operation.cables:
      cable_id = lmo_cable.id_external
      operation["cables"].append(cable_id)

    #   Vessels
    vessel_ids_str = lmo_operation.vessels
    vessel_ids_str = vessel_ids_str.replace(' ', '')
    vessel_ids_str = vessel_ids_str.replace('[', '').replace(']', '')
    vessel_ids_list = vessel_ids_str.split(',')
    operation["vessels"] = []
    for vessel_id in vessel_ids_list:
      vessel_row = df_vessels[df_vessels['id_name'].str.lower() == vessel_id]
      vessel_row = vessel_row.squeeze()
      operation["vessels"].append(
          {
              "id": vessel_id,
              "type": vessel_row.get('vessel_type')
          }
      )
    #   Terminal
    terminal_id = lmo_operation.terminal
    terminal_row = df_terminals[df_terminals['id_name'].str.lower() == terminal_id]
    terminal_row = terminal_row.squeeze()
    operation["terminal"] = {
        "id": terminal_id,
        "port_name": terminal_row.get('port_name'),
        "name": terminal_row.get('terminal_name'),
        "country": terminal_row.get('country'),
        "type": terminal_row.get('type'),
        "location": {
            "latitude": terminal_row.get('latitude'),
            "longitude": terminal_row.get('longitude')
        }
    }
    #   Equipment
    equipment_ids_str = lmo_operation.equipment
    equipment_ids_str = equipment_ids_str.replace(' ', '')
    equipment_ids_str = equipment_ids_str.replace('[', '').replace(']', '')
    equipment_ids_list = equipment_ids_str.split(',')
    if equipment_ids_list[0] == '':
      equipment_ids_list = []
    operation["equipment"] = []
    for equipment_id in equipment_ids_list:
      # if 
      if 'rov' in equipment_id.lower():
        equipment_row = df_rov[df_rov['id_name'].str.lower() == equipment_id]
        equipment_row = equipment_row.squeeze()
        equipment_type = equipment_row.get('_class')
      elif 'div' in equipment_id.lower():
        equipment_row = df_divers[df_divers['id_name'].str.lower() == equipment_id]
        equipment_row = equipment_row.squeeze()
        equipment_type = 'None'
      elif 'bur' in equipment_id.lower():
        equipment_row = df_burial[df_burial['id_name'].str.lower() == equipment_id]
        equipment_row = equipment_row.squeeze()
        equipment_type = equipment_row.get('type')
      elif 'pro' in equipment_id.lower():
        equipment_row = df_protect[df_protect['id_name'].str.lower() == equipment_id]
        equipment_row = equipment_row.squeeze()
        equipment_type = equipment_row.get('type')
      elif 'pil' in equipment_id.lower():
        equipment_row = df_piling[df_piling['id_name'].str.lower() == equipment_id]
        equipment_row = equipment_row.squeeze()
        equipment_type = equipment_row.get('type')
      operation["equipment"].append(
          {
              "id": equipment_id,
              "type": equipment_type
          }
      )

    lmo_operations.append(operation)

  operations = []
  #   Dates
  request_plan = get_phase_plan(lmo_phase.study.id, lmo_phase.name)
  plan = json.loads(request_plan.data)
  for plan_op_idx, plan_op_id in enumerate(plan["operation_id"]):
    for lmo_operation in lmo_operations:
      if lmo_operation["id_external"].lower() in plan_op_id.lower():
        idx = plan_op_idx
        break
    try:
      idx = int(idx)
    except NameError:
      continue

    operation = deepcopy(lmo_operation)

    operation["id_external"] = plan["operation_id"][idx]

    operation["date_maystart"] = plan["date_may_start"][idx]
    operation["date_start"] = plan["date_start"][idx]
    operation["date_end"] = plan["date_end"][idx]

    month = operation["date_maystart"].split('/')[1]
    month = int(month)
    if lmo_phase.study.inputs.period.lower() == 'month':
      period = month
    elif lmo_phase.inputs.period.lower() == 'quarter':
      # Check in which quarter this month is
      period = mt.ceil(month / 3)
    elif lmo_phase.study.inputs.period.lower() == 'trimester':
      # Check in which trimester this month is
      period = mt.ceil(month / 4)
    period = str(period)

    #   Durations and waitings
    operation["durations"] = json.loads(operation["durations"].replace('\'', '"'))
    try:
      # Complexity 2 and 3
      operation["durations"] = {
          "total": operation["durations"]["total"][period],
          "port": operation["durations"]["port"][period],
          "site": operation["durations"]["site"][period],
          "transit": operation["durations"]["transit"][period],
          "mobilization": operation["durations"]["mobilization"][period]
      }
    except KeyError:
      # Complexity 1
      operation["durations"] = operation["durations"][period]

    operation["waitings"] = json.loads(operation["waitings"].replace('\'', '"'))
    try:
      # Complexity 2 and 3
      operation["waitings"] = {
          "to_start": operation["waitings"]["to_start"][period],
          "port": operation["waitings"]["port"][period],
          "site": operation["waitings"]["site"][period]
      }
    except KeyError:
      # Complexity 1
      operation["waitings"] = operation["waitings"][period]

    #   Costs
    operation["costs"] = json.loads(operation["costs"].replace('\'', '"'))
    operation["costs"] = {
        "vessels": operation["costs"]["vessels"][period],
        "terminal": operation["costs"]["terminal"][period],
        "equipment": operation["costs"]["equipment"][period]
    }

    del operation["phase"]
    del operation["phase_id"]
    del operation["dates_maystart_delta"]
    del operation["multiple_operations"]
    del operation["logistic_solution"]

    operations.append(operation)

    del idx

  return operations


def prepare_logistic_solution(operations):
  logistic_solution = {
      "vecs": [],
      "vessels": [],
      "terminals": [],
      "equipment": []
  }
  for operation in operations:
    if operation["vec"] is not None:
      logistic_solution["vecs"].append(operation["vec"])
    for vessel in operation["vessels"]:
      logistic_solution["vessels"].append(vessel["id"])
    if operation["terminal"] is not None:
      logistic_solution["terminals"].append(operation["terminal"]["id"])
    for equipment in operation["equipment"]:
      logistic_solution["equipment"].append(equipment["id"])

  return logistic_solution
