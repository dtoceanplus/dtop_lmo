# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
 
# Import packages
import pandas as pd

# Import functions
from dtop_lmo.service.api.integration import call_intermodule_service


def get_catalog_terminals():
  try:
    catalog_response_terminals = call_intermodule_service('cm', '/api/terminals')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    terminals_list = catalog_response_terminals.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(terminals_list):
    terminals_list[idx] = item["attributes"]

  terminals_df = pd.DataFrame(terminals_list)
  return terminals_df

def get_catalog_vecs():
  try:
    catalog_response_vecs = call_intermodule_service('cm', '/api/vecs')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    vecs_list = catalog_response_vecs.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(vecs_list):
    vecs_list[idx] = item["attributes"]

  vecs_df = pd.DataFrame(vecs_list)
  return vecs_df

def get_catalog_vessels():
  try:
    catalog_response_vessels = call_intermodule_service('cm', '/api/vessels')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    vessels_list = catalog_response_vessels.json()["data"]
  except AttributeError:
    raise ConnectionError('Vessels catalogue response not as expected')

  # Remove "attributes" key
  for idx, item in enumerate(vessels_list):
    vessels_list[idx] = item["attributes"]

  vessels_df = pd.DataFrame(vessels_list)
  return vessels_df

def get_catalog_rovs():
  try:
    catalog_response_rovs = call_intermodule_service('cm', '/api/equipment_rov')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    rovs_list = catalog_response_rovs.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(rovs_list):
    rovs_list[idx] = item["attributes"]

  rovs_df = pd.DataFrame(rovs_list)
  return rovs_df

def get_catalog_divers():
  try:
    catalog_response_divers = call_intermodule_service('cm', '/api/equipment_divers')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    divers_list = catalog_response_divers.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(divers_list):
    divers_list[idx] = item["attributes"]

  divers_df = pd.DataFrame(divers_list)
  return divers_df

def get_catalog_equip_burial():
  try:
    catalog_response_equip_burial = call_intermodule_service('cm', '/api/equipment_burial')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    equip_burial_list = catalog_response_equip_burial.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(equip_burial_list):
    equip_burial_list[idx] = item["attributes"]

  equip_burial_df = pd.DataFrame(equip_burial_list)
  return equip_burial_df

def get_catalog_equip_piling():
  try:
    catalog_response_equip_piling = call_intermodule_service('cm', '/api/equipment_piling')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    equip_piling_list = catalog_response_equip_piling.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(equip_piling_list):
    equip_piling_list[idx] = item["attributes"]

  equip_piling_df = pd.DataFrame(equip_piling_list)
  return equip_piling_df

def get_catalog_equip_protect():
  try:
    catalog_response_equip_protect = call_intermodule_service('cm', '/api/equipment_protection')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    equip_protect_list = catalog_response_equip_protect.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(equip_protect_list):
    equip_protect_list[idx] = item["attributes"]

  equip_protect_df = pd.DataFrame(equip_protect_list)
  return equip_protect_df

def get_catalog_operations_installation():
  try:
    catalog_response_operations_inst = call_intermodule_service('cm', '/api/operations_inst')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    operations_list = catalog_response_operations_inst.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(operations_list):
    operations_list[idx] = item["attributes"]

  operations_inst_df = pd.DataFrame(operations_list)
  return operations_inst_df

def get_catalog_operations_maintenance():
  try:
    catalog_response_operations_main = call_intermodule_service('cm', '/api/operations_main')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    operations_list = catalog_response_operations_main.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(operations_list):
    operations_list[idx] = item["attributes"]

  operations_main_df = pd.DataFrame(operations_list)
  return operations_main_df

def get_catalog_activities():
  try:
    catalog_response_activities = call_intermodule_service('cm', '/api/activities')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    activities_list = catalog_response_activities.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(activities_list):
    activities_list[idx] = item["attributes"]

  activities_df = pd.DataFrame(activities_list)
  return activities_df

def get_catalog_speeds():
  try:
    catalog_response_speeds = call_intermodule_service('cm', '/api/speeds')
  except:
    raise ConnectionError('Could not connect to Catalogues Module')

  try:
    speeds_list = catalog_response_speeds.json()["data"]
  except AttributeError:
    raise ConnectionError('Bad response from CM module')

  # Remove "attributes" key
  for idx, item in enumerate(speeds_list):
    speeds_list[idx] = item["attributes"]

  speeds_df = pd.DataFrame(speeds_list)
  return speeds_df
