# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import math as mt


def join_validation_error_messages(error):
    """
    Converts the marshmallow validation error messages from a dictionary to a single string.

    Required in the case that more than one validation errors are raised. Extracts the actual messages from each
    validation error and concatenates to a single string.

    :param marshmallow.exceptions.ValidationError error: the validation error raised by marshmallow
    :return: the single concatenated error message
    :rtype: str
    """
    msg = ' '.join([
            ''.join(str(key) + ': ' + str(value))
            for key, value in error.messages.items()
    ])

    return msg


def dict_replace_nan(d):
    """
    Replace NaN values in a dictionary.
    It supports nested dictionaries.
    """
    new_dict = {}
    for key, value in d.items():
        if isinstance(value, dict):
            new_dict[key] = dict_replace_nan(value)
        else:
            try:
                if mt.isnan(value) is True:
                    new_dict[key] = None
                else:
                    new_dict[key] = value
            except TypeError:
                new_dict[key] = value
    return new_dict
