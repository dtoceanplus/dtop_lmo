# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

def validate_module_mc(mc_data, complexity):
  data_general = mc_data.get('general', None)
  if data_general is None:
    return 'MC module input: "general" field is missing.'
  data_floating = data_general.get('floating', None)
  if data_floating is None:
    return 'MC module input: "general.floating" field is missing.'
  data_cost = data_general.get('machine_cost', None)
  if data_cost is None:
    return 'MC module input: "general.machine_cost" field is missing.'

  data_dimensions = mc_data.get('dimensions', None)
  if data_dimensions is None:
    return 'MC module input: "dimensions" field is missing.'
  data_mass = data_dimensions.get('mass', None)
  if data_mass is None:
    return 'MC module input: "dimensions.mass" field is missing.'

  if complexity == 'high':
    data_height = data_dimensions.get('height', None)
    if data_height is None:
      return 'MC module input: "dimensions.height" field is missing.'
    data_width = data_dimensions.get('width', None)
    if data_width is None:
      return 'MC module input: "dimensions.width" field is missing.'
    data_length = data_dimensions.get('length', None)
    if data_length is None:
      return 'MC module input: "dimensions.length" field is missing.'

  return True


def validate_module_ec(ec_data, complexity):
  data_layout = ec_data.get('layout', None)
  if data_layout is None:
    return 'EC module input: "layout" field is missing.'
  data_deviceID = data_layout.get('deviceID', None)
  if data_deviceID is None:
    return 'EC module input: "layout.deviceID" field is missing.'
  for id_ in data_deviceID:
    try:
      id_ = int(id_)
    except ValueError:
      return 'EC module input: all IDs must be integers in "layout.deviceID".'
  data_number_devices = ec_data.get('number_devices', None)
  if data_number_devices is None:
    return 'EC module input: "number_devices" field is missing.'

  if complexity == 'high':
    data_easting = data_layout.get('easting', None)
    if data_easting is None:
      return 'EC module input: "layout.easting" field is missing.'
    data_northing = data_layout.get('northing', None)
    if data_northing is None:
      return 'EC module input: "layout.northing" field is missing.'

  return True


def validate_module_et(et_data, complexity):
  data_array = et_data.get('array', None)
  if data_array is None:
    return 'ET module input: "array" field is missing.'
  data_hierarchy = data_array.get('Hierarchy', None)
  if data_hierarchy is None:
    return 'ET module input: "array.Hierarchy" field is missing.'
  data_hierarchy_value = data_hierarchy.get('value', None)
  if data_hierarchy_value is None:
    return 'ET module input: "array.Hierarchy.value" field is missing.'
  # Validate hierarchy
  # TODO: Call function to verify if hierarchy structure is correct

  data_devices = et_data.get('devices', None)
  if data_devices is None:
    return 'ET module input: "devices" field is missing.'
  for idx_device, data_device in enumerate(data_devices):
    data_pto_cost = data_device.get('Dev_PTO_cost', None)
    if data_pto_cost is None:
      return 'ET module input: "devices[%d].Dev_PTO_cost" field is missing.' % idx_device
    data_pto_cost_value = data_pto_cost.get('value', None)
    if data_pto_cost_value is None:
      return 'ET module input: "devices[%d].Dev_PTO_cost.value" field is missing.' % idx_device
    data_pto_mass = data_device.get('Dev_PTO_mass', None)
    if data_pto_mass is None:
      return 'ET module input: "devices[%d].Dev_PTO_mass" field is missing.' % idx_device
    data_pto_mass_value = data_pto_mass.get('value', None)
    if data_pto_mass_value is None:
      return 'ET module input: "devices[%d].Dev_PTO_mass.value" field is missing.' % idx_device
    data_power = data_device.get('Dev_rated_power', None)
    if data_power is None:
      return 'ET module input: "devices[%d].Dev_rated_power" field is missing.' % idx_device
    data_power_value = data_power.get('value', None)
    if data_power_value is None:
      return 'ET module input: "devices[%d].Dev_rated_power.value" field is missing.' % idx_device
    data_device_id = data_device.get('id', None)
    if data_device_id is None:
      return 'ET module input: "devices[%d].id" field is missing.' % idx_device
    data_device_id_value = data_device_id.get('value', None)
    if data_device_id_value is None:
      return 'ET module input: "devices[%d].id.value" field is missing.' % idx_device
    try:
      id_ = int(data_device_id_value)
    except ValueError:
      return 'ET module input: "devices[%d].id.value" is not an integer.' % idx_device
    data_device_ptos = data_device.get('ptos', None)
    if data_device_ptos is None:
      return 'ET module input: "devices[%d].ptos" field is missing.' % idx_device
    for idx_pto, data_pto in enumerate(data_device_ptos):
      data_pto_elect_cost = data_pto.get('Elect_cost', None)
      if data_pto_elect_cost is None:
        return 'ET module input: "devices[%d].ptos[%d].Elect_cost" field is missing.' % (idx_device, idx_pto)
      data_pto_elect_cost_value = data_pto_elect_cost.get('value', None)
      if data_pto_elect_cost_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Elect_cost.value" field is missing.' % (idx_device, idx_pto)
      data_pto_elect_mass = data_pto.get('Elect_mass', None)
      if data_pto_elect_mass is None:
        return 'ET module input: "devices[%d].ptos[%d].Elect_mass" field is missing.' % (idx_device, idx_pto)
      data_pto_elect_mass_value = data_pto_elect_mass.get('value', None)
      if data_pto_elect_mass_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Elect_mass.value" field is missing.' % (idx_device, idx_pto)

      data_pto_grid_cost = data_pto.get('Grid_cost', None)
      if data_pto_grid_cost is None:
        return 'ET module input: "devices[%d].ptos[%d].Grid_cost" field is missing.' % (idx_device, idx_pto)
      data_pto_grid_cost_value = data_pto_grid_cost.get('value', None)
      if data_pto_grid_cost_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Grid_cost.value" field is missing.' % (idx_device, idx_pto)
      data_pto_grid_mass = data_pto.get('Grid_mass', None)
      if data_pto_grid_mass is None:
        return 'ET module input: "devices[%d].ptos[%d].Grid_mass" field is missing.' % (idx_device, idx_pto)
      data_pto_grid_mass_value = data_pto_grid_mass.get('value', None)
      if data_pto_grid_mass_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Grid_mass.value" field is missing.' % (idx_device, idx_pto)

      data_pto_mech_cost = data_pto.get('Mech_cost', None)
      if data_pto_mech_cost is None:
        return 'ET module input: "devices[%d].ptos[%d].Mech_cost" field is missing.' % (idx_device, idx_pto)
      data_pto_mech_cost_value = data_pto_mech_cost.get('value', None)
      if data_pto_mech_cost_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Mech_cost.value" field is missing.' % (idx_device, idx_pto)
      data_pto_mech_mass = data_pto.get('Mech_mass', None)
      if data_pto_mech_mass is None:
        return 'ET module input: "devices[%d].ptos[%d].Mech_mass" field is missing.' % (idx_device, idx_pto)
      data_pto_mech_mass_value = data_pto_mech_mass.get('value', None)
      if data_pto_mech_mass_value is None:
        return 'ET module input: "devices[%d].ptos[%d].Mech_mass.value" field is missing.' % (idx_device, idx_pto)

      data_pto_id = data_pto.get('id', None)
      if data_pto_id is None:
        return 'ET module input: "devices[%d].ptos[%d].id" field is missing.' % (idx_device, idx_pto)
      data_pto_id_value = data_pto_id.get('value', None)
      if data_pto_id_value is None:
        return 'ET module input: "devices[%d].ptos[%d].id.value is missing.' % (idx_device, idx_pto)

  return True


def validate_module_ed(ed_data, complexity):
  data_cable_dict = ed_data.get('cable_dict', None)
  if data_cable_dict is not None:
    for idx_cable, data_cable in enumerate(data_cable_dict):
      data_burial_depth = data_cable.get('burial_depth')
      if data_burial_depth is None:
        return 'ED module input: "cable_dict[%d].burial_depth" field is missing.' % idx_cable
      data_length = data_cable.get('length')
      if data_length is None:
        return 'ED module input: "cable_dict[%d].length" field is missing.' % idx_cable
      data_marker = data_cable.get('marker')
      if data_marker is None:
        return 'ED module input: "cable_dict[%d].marker" field is missing.' % idx_cable
      data_split = data_cable.get('split_pipe')
      if data_split is None:
        return 'ED module input: "cable_dict[%d].split_pipe" field is missing.' % idx_cable
      data_type = data_cable.get('type_')
      if data_type is None:
        return 'ED module input: "cable_dict[%d].type_" field is missing.' % idx_cable

      if complexity == 'high':
        data_cable_x = data_cable.get('cable_x', None)
        if data_cable_x is None:
          return 'ED module input: "cable_dict[%d].cable_x" field is missing.' % idx_cable
        data_cable_y = data_cable.get('cable_y', None)
        if data_cable_y is None:
          return 'ED module input: "cable_dict[%d].cable_y" field is missing.' % idx_cable
        data_cable_cost = data_cable.get('cost', None)
        if data_cable_cost is None:
          return 'ED module input: "cable_dict[%d].cost" field is missing.' % idx_cable
        data_cable_layer_1_start = data_cable.get('layer_1_start', None)
        if data_cable_layer_1_start is None:
          return 'ED module input: "cable_dict[%d].layer_1_start" field is missing.' % idx_cable
        data_cable_layer_1_type = data_cable.get('layer_1_type', None)
        if data_cable_layer_1_type is None:
          return 'ED module input: "cable_dict[%d].layer_1_type" field is missing.' % idx_cable

  data_umbilical_dict = ed_data.get('umbilical_dict', None)
  if data_umbilical_dict is not None:
    for idx_umbilical, data_umbilical in enumerate(data_umbilical_dict):
      data_burial_marker = data_umbilical.get('marker')
      if data_burial_marker is None:
        return 'ED module input: "umbilical_dict[%d].marker" field is missing.' % idx_umbilical
      data_length = data_umbilical.get('length')
      if data_length is None:
        return 'ED module input: "umbilical_dict[%d].length" field is missing.' % idx_umbilical
      data_cost = data_umbilical.get('cost')
      if data_cost is None:
        return 'ED module input: "umbilical_dict[%d].cost" field is missing.' % idx_umbilical

  data_collection_point_dict = ed_data.get('collection_point_dict', None)
  if data_collection_point_dict is not None:
    for idx_cp, data_cp in enumerate(data_collection_point_dict):
      data_marker = data_cp.get('marker')
      if data_marker is None:
        return 'ED module input: "collection_point_dict[%d].marker" field is missing.' % idx_cp
      data_type = data_cp.get('type_')
      if data_type is None:
        return 'ED module input: "collection_point_dict[%d].type_" field is missing.' % idx_cp
      data_cost = data_cp.get('cost')
      if data_cost is None:
        return 'ED module input: "collection_point_dict[%d].cost" field is missing.' % idx_cp

      if complexity == 'high':
        data_location = data_cp.get('location', None)
        if data_location is None:
          return 'ED module input: "collection_point_dict[%d].location" field is missing.' % idx_cp

  data_hierarchy_value = ed_data.get('hierarchy_new', None)
  if data_hierarchy_value is None:
    return 'ETD module input: "hierarchy_new" field is missing.'
  # Validate hierarchy
  # TODO: Call function to verify if hierarchy structure is correct

  if complexity == 'high':
    data_connectors_dict = ed_data.get('connectors_dict', None)
    if data_connectors_dict is not None:
      for idx_conn, data_conn in enumerate(data_connectors_dict):
        data_marker = data_conn.get('marker')
        if data_marker is None:
          return 'ED module input: "connectors_dict[%d].marker" field is missing.' % idx_conn
        data_cost = data_conn.get('cost')
        if data_cost is None:
          return 'ED module input: "connectors_dict[%d].cost" field is missing.' % idx_conn
        data_type = data_conn.get('type_')
        if data_type is None:
          return 'ED module input: "connectors_dict[%d].type_" field is missing.' % idx_conn

  return True


def validate_module_sk(sk_data, complexity):
  # Validate hierarchy
  # TODO: Call function to verify if hierarchy structure is correct
  data_hierarchy_data = sk_data.get('hierarchy_data', None)
  if data_hierarchy_data is None:
    return 'SK module input: "hierarchy.hierarchy_data" field is missing.'
  data_anchor_list = data_hierarchy_data.get('anchor_list', None)
  if data_anchor_list is not None:
    for idx_anchor, data_anchor in enumerate(data_anchor_list):
      data_anchor_id = data_anchor.get('design_id', None)
      if data_anchor_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].design_id" field is missing.' % idx_anchor
      data_anchor_type = data_anchor.get('type', None)
      if data_anchor_type is None:
        return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].type" field is missing.' % idx_anchor
      data_anchor_mass = data_anchor.get('mass', None)
      if data_anchor_mass is None:
        return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].mass" field is missing.' % idx_anchor
      data_anchor_upstream_id = data_anchor.get('upstream_id', None)
      if data_anchor_upstream_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].upstream_id" field is missing.' % idx_anchor
      data_anchor_cost = data_anchor.get('cost', None)
      if data_anchor_cost is None:
        return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].cost" field is missing.' % idx_anchor
      
      if complexity == 'high':
        data_anchor_height = data_anchor.get('height', None)
        if data_anchor_height is None:
          return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].height" field is missing.' % idx_anchor
        data_anchor_width = data_anchor.get('width', None)
        if data_anchor_width is None:
          return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].width" field is missing.' % idx_anchor
        data_anchor_length = data_anchor.get('length', None)
        if data_anchor_length is None:
          return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].length" field is missing.' % idx_anchor
        data_anchor_coord = data_anchor.get('coordinates', None)
        if data_anchor_coord is None:
          return 'SK module input: "hierarchy.hierarchy_data.anchor_list[%d].coordinates" field is missing.' % idx_anchor

  data_foundation_list = data_hierarchy_data.get('foundation_list', None)
  if data_foundation_list is not None:
    for idx_foundation, data_foundation in enumerate(data_foundation_list):
      data_foundation_id = data_foundation.get('design_id', None)
      if data_foundation_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].design_id" field is missing.' % idx_foundation
      data_foundation_type = data_foundation.get('type', None)
      if data_foundation_type is None:
        return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].type" field is missing.' % idx_foundation
      data_foundation_mass = data_foundation.get('mass', None)
      if data_foundation_mass is None:
        return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].mass" field is missing.' % idx_foundation
      data_foundation_upstream_id = data_foundation.get('upstream_id', None)
      if data_foundation_upstream_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].upstream_id" field is missing.' % idx_foundation
      data_foundation_cost = data_foundation.get('cost', None)
      if data_foundation_cost is None:
        return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].cost" field is missing.' % idx_foundation

      if complexity == 'high':
        data_foundation_height = data_foundation.get('height', None)
        if data_foundation_height is None:
          return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].height" field is missing.' % idx_foundation
        data_foundation_width = data_foundation.get('width', None)
        if data_foundation_width is None:
          return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].width" field is missing.' % idx_foundation
        data_foundation_length = data_foundation.get('length', None)
        if data_foundation_length is None:
          return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].length" field is missing.' % idx_foundation
        data_foundation_coord = data_foundation.get('coordinates', None)
        if data_foundation_coord is None:
          return 'SK module input: "hierarchy.hierarchy_data.foundation_list[%d].coordinates" field is missing.' % idx_foundation

  data_line_seg_list = data_hierarchy_data.get('line_segment_list', None)
  if data_line_seg_list is not None:
    for idx_line_seg, data_line_seg in enumerate(data_line_seg_list):
      data_line_seg_id = data_line_seg.get('design_id', None)
      if data_line_seg_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.line_segment_list[%d].design_id" field is missing.' % idx_line_seg
      data_line_seg_mass = data_line_seg.get('total_mass', None)
      if data_line_seg_mass is None:
        return 'SK module input: "hierarchy.hierarchy_data.line_segment_list[%d].mass" field is missing.' % idx_line_seg
      data_line_seg_upstream_id = data_line_seg.get('upstream_id', None)
      if data_line_seg_upstream_id is None:
        return 'SK module input: "hierarchy.hierarchy_data.line_segment_list[%d].upstream_id" field is missing.' % idx_line_seg
      data_line_seg_cost = data_line_seg.get('cost', None)
      if data_line_seg_cost is None:
        return 'SK module input: "hierarchy.hierarchy_data.line_segment_list[%d].cost" field is missing.' % idx_line_seg

      if complexity == 'high':
        data_line_seg_diameter = data_line_seg.get('diameter', None)
        if data_line_seg_diameter is None:
          return 'SK module input: "hierarchy.hierarchy_data.line_segment_list[%d].height" field is missing.' % idx_line_seg

  return True
