# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from datetime import date
from datetime import datetime
from datetime import timedelta
import json
from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from ..studies.lmo_studies import update_status

from .lmo_inputs import (
    create_inputs, update_inputs, delete_inputs,
    create_inputs_modules, update_inputs_modules, delete_inputs_modules
)
from dtop_lmo.service.models import LMOStudy, Inputs, Module
from dtop_lmo.service.schemas import (
    InputsSchema, LoadInputsSchema, UpdateInputsSchema, ModuleSchema
)

bp = Blueprint('api_inputs', __name__)


@bp.route('/inputs/project', methods=['GET'])
def get_lmo_inputs(lmoid):
  """
  Flask blueprint route for getting the inputs of a LMO study.

  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the selected LMO study inputs or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_inputs = Inputs.query.filter_by(study_id=lmoid).first()
    if lmo_inputs is None:
      return 'Inputs not defined fot this study yet.'

    lmo_inputs_schema = InputsSchema()
    response = lmo_inputs_schema.dump(lmo_inputs)

    return jsonify(response)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/project', methods=['POST'])
def post_lmo_inputs(lmoid):
  """
  Flask blueprint route to add a set of new Inputs to a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.inputs.create_inputs()` method.
  Returns HTTP 400 error if no request body provided or if a the inputs for this study were already defined
  Returns 404 error if study ID does not exist.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the Inputs added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if lmo_study.inputs is not None:
      return bad_request('A set of inputs is already defined for this study')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    if 'installation_date' in request_body:
      try:
        inst_str = request_body['installation_date'][0:10]
        inst_dt = datetime.strptime(inst_str, '%Y/%m/%d')
      except ValueError:
        return bad_request('Wrong installation format data')
      inst_str = datetime.strftime(inst_dt, '%Y/%m/%d')
      request_body['installation_date'] = inst_str
    if 'maintenance_date' in request_body:
      try:
        maint_str = request_body['maintenance_date'][0:10]
        maint_dt = datetime.strptime(maint_str, '%Y/%m/%d')
      except ValueError:
        return bad_request('Wrong maintenance format data')
      maint_str = datetime.strftime(maint_dt, '%Y/%m/%d')
      request_body['maintenance_date'] = maint_str

    try:
      LoadInputsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    request_body['study_id'] = lmo_study.id
    lmo_inputs = create_inputs(request_body)
    if type(lmo_inputs) is str:
      # Error inside create_inputs()
      return bad_request(lmo_inputs)
    # Update LMO study status
    update_status(lmoid)

    lmo_inputs_schema = InputsSchema()

    result = {
        "created_inputs": lmo_inputs_schema.dump(lmo_inputs),
        "message": "Inputs created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/project', methods=['PUT'])
def put_lmo_inputs(lmoid):
  """
  Flask blueprint route to modify a set of Inputs of a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.inputs.update_inputs()` method.
  Returns HTTP 400 error if no request body provided.
  Returns 404 error if study ID does not exist
  Returns 404 error if Inputs were not defined yet.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the Inputs updated or a HTTP 404 error.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    if lmo_study.inputs is None:
      return bad_request('Inputs for this study are not defined yet.')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    if 'installation_date' in request_body:
      inst_str = request_body['installation_date'][0:10]
      inst_dt = datetime.strptime(inst_str, '%Y/%m/%d')
      inst_dt = date(inst_dt.year, inst_dt.month, inst_dt.day)
      inst_str = datetime.strftime(inst_dt, '%Y/%m/%d')
      request_body['installation_date'] = inst_str
    if 'maintenance_date' in request_body:
      maint_str = request_body['maintenance_date'][0:10]
      maint_dt = datetime.strptime(maint_str, '%Y/%m/%d')
      maint_dt = date(maint_dt.year, maint_dt.month, maint_dt.day)
      maint_str = datetime.strftime(maint_dt, '%Y/%m/%d')
      request_body['maintenance_date'] = maint_str

    try:
      UpdateInputsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    lmo_inputs = update_inputs(request_body, lmo_study.id)
    if type(lmo_inputs) is str:
      # Error inside create_inputs()
      return bad_request(lmo_inputs)
    lmo_inputs_schema = InputsSchema()

    result = {
        "updated_inputs": lmo_inputs_schema.dump(lmo_inputs),
        "message": "Inputs updated"
    }
    response = jsonify(result)

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/project', methods=['DELETE'])
def delete_lmo_inputs(lmoid):
  """
  Flask blueprint route for deleting a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.studies.delete_inputs()` method.
  Returns HTTP 400 error if deletion of LMO study inputs is unsuccessful or
  if LMO study id was not found.

  :param str lmoid: the ID of the LMO study to delete inputs
  :return: serialised response containing 'Inputs deleted' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  try:
    data = delete_inputs(lmoid)
    if data == "Inputs deleted":
      # Update LMO study status
      update_status(lmoid)
      response = {
          'study_id': lmoid,
          'message': 'Inputs deleted.'
      }
      return jsonify(response)
    else:
      return bad_request(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/<module>', methods=['GET'])
def get_lmo_inputs_modules(lmoid, module):
  """
  Flask blueprint route for getting the other modules inputs of a LMO study.

  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the selected LMO study other modules inputs or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    module_name = module.lower()
    lmo_module = Module.query.filter_by(study_id=lmoid, module=module_name).first()
    if lmo_module is None:
      _e = '%s module inputs not defined fot this study yet.' % module_name.upper()
      return bad_request(_e)

    lmo_module_schema = ModuleSchema()
    response = lmo_module_schema.dump(lmo_module)

    response["design"] = lmo_module.design
    response["hierarchy"] = lmo_module.hierarchy

    return jsonify(response)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/<module>', methods=['POST'])
def post_lmo_inputs_module(lmoid, module):
  """
  Flask blueprint route to add external inputs to a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.inputs.create_inputs_modules()` method.
  Returns HTTP 400 error if no request body provided or if a the this module inputs for this study were already defined
  Returns 404 error if study ID does not exist.

  Args:
    lmoid (int): LMO study ID
    module (str): External module short string - mc, ec, et, ed, sk

  Returns:
    dict: the serialised response containing this module Inputs added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    module_name = module.lower()
    lmo_module = Module.query.filter_by(study_id=lmoid, module=module_name).first()
    if lmo_module is not None:
      return bad_request('%s module inputs already defined' % module_name.upper())

    request_body['module'] = module_name
    request_body['study_id'] = lmo_study.id

    lmo_inputs_module = create_inputs_modules(request_body, lmo_study.complexity)
    if type(lmo_inputs_module) is str:
      # Error inside create_inputs_modules()
      return bad_request(lmo_inputs_module)
    # Update LMO study status
    update_status(lmoid)

    lmo_inputs_module_schema = ModuleSchema()

    result = {
        "created_inputs": lmo_inputs_module_schema.dump(lmo_inputs_module),
        "message": "%s module inputs created" % module.upper()
    }
    result["created_inputs"]["design"] = lmo_inputs_module.design
    result["created_inputs"]["hierarchy"] = lmo_inputs_module.hierarchy
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/<module>', methods=['PUT'])
def put_lmo_inputs_module(lmoid, module):
  """
  Flask blueprint route to update external inputs of a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.inputs.update_inputs_modules()` method.
  Returns HTTP 400 error if no request body provided.
  Returns 404 error if study ID does not exist
  Returns 404 error if Modules were not defined yet.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the other modules inputs.
  """
  return bad_request('Not implemented!')
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if lmo_study.modules is None:
      return bad_request('A set of inputs from other modules is not defined for this study yet')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    request_body['study_id'] = lmo_study.id
    lmo_inputs_modules = update_inputs_modules(request_body, lmo_study.id)
    if type(lmo_inputs_modules) is str:
      # Error inside update_inputs_modules()
      return bad_request(lmo_inputs_modules)

    lmo_inputs_modules_schema = ModuleSchema()

    result = {
        "updated_inputs": lmo_inputs_modules_schema.dump(lmo_inputs_modules),
        "message": "Other modules inputs updated"
    }
    result["updated_inputs"]["module_mc"] = lmo_inputs_modules.module_mc
    result["updated_inputs"]["module_ec"] = lmo_inputs_modules.module_ec
    result["updated_inputs"]["module_et"] = lmo_inputs_modules.module_et
    result["updated_inputs"]["module_ed"] = lmo_inputs_modules.module_ed
    result["updated_inputs"]["module_sk"] = lmo_inputs_modules.module_sk
    result["updated_inputs"]["hierarchy_et"] = lmo_inputs_modules.hierarchy_et
    result["updated_inputs"]["hierarchy_ed"] = lmo_inputs_modules.hierarchy_ed
    result["updated_inputs"]["hierarchy_sk"] = lmo_inputs_modules.hierarchy_sk
    response = jsonify(result)

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/inputs/<module>', methods=['DELETE'])
def delete_lmo_inputs_module(lmoid, module):
  """
  Flask blueprint route for deleting other modules inputs of a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.studies.delete_inputs_modules()` method.
  Returns HTTP 400 error if deletion of LMO study other modules inputs is
  unsuccessful or if LMO study id was not found.

  :param str module: short name of the external module
  :return: serialised response containing '<module> module inputs deleted.' message; bad request error otherwise
  :rtype: flask.wrappers.Response
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return bad_request('LMO study with that ID does not exist.')
    lmo_module = Module.query.filter_by(study_id=lmoid, module=module).first()
    if lmo_module is None:
      return bad_request('There is no %s module inputs for this LMO study.' % module.upper())

    data = delete_inputs_modules(lmo_module.id)
    if data == "module inputs deleted":
      # Update LMO study status
      update_status(lmoid)
      response = {
          'study_id': lmoid,
          'message': '%s module inputs deleted.' % module.upper()
      }
      return jsonify(response)
    else:
      return bad_request(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/loaded_modules', methods=['GET'])
def get_lmo_loaded_module(lmoid):
  """
  Flask blueprint route for getting a list of other modules loaded for a LMO study.

  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (int): LMO study ID

  Returns:
    dict: the serialised response containing the selected LMO study other modules inputs or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    lmo_modules = Module.query.filter_by(study_id=lmoid).all()
    lmo_modules_list = [lmo_module.module.lower() for lmo_module in lmo_modules]

    return jsonify(lmo_modules_list)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
