# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError
from copy import deepcopy

from ....service import db

from .lmo_module_validator import (
    validate_module_mc, validate_module_ec, validate_module_et,
    validate_module_ed, validate_module_sk
)

from ...models import LMOStudy, Inputs, Module
from ...schemas import InputsSchema, ModuleSchema


def create_inputs(data):
  """
  Create a new set of inputs and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create a Inputs object.

  Returns:
    dict: the newly created Inputs instance
  """
  if data["consider_draft"] is False:
    try:
      del data["device_draft"]
    except KeyError:
      pass

  lmo_inputs_schema = InputsSchema()
  lmo_inputs = lmo_inputs_schema.load(data)

  try:
    db.session.add(lmo_inputs)
    db.session.commit()
    return lmo_inputs
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'


def update_inputs(data, study_id):
  """
  Update a set of Inputs in the local storage database.

  Args:
    data (dict): dictionary with keys required to update the LMO study object in the local database.
    study_id (int): the ID of the LMO study in the local storage database

  Returns:
    dict: the updated LMO study instance
  """
  lmo_study = LMOStudy.query.get(study_id)

  # Update existing set of inputs (ID=lmoid) with existing data from "data"
  quantile = data.get('quantile', None)
  quantile_vessels = data.get('quantile_vessels', None)
  period = data.get('period', None)

  max_wait = data.get('max_wait', None)
  montecarlo = data.get('montecarlo', None)
  comb_retain_ratio = data.get('comb_retain_ratio', None)
  comb_retain_min = data.get('comb_retain_min', None)
  safety_factor = data.get('safety_factor', None)
  device_draft = data.get('device_draft', None)
  consider_draft = data.get('consider_draft', None)
  if device_draft is not None and consider_draft is False:
    device_draft = 0
  fuel_price = data.get('fuel_price', None)
  sfoc = data.get('sfoc', None)
  load_factor = data.get('load_factor', None)

  installation_date = data.get('installation_date', None)
  operations_hs = data.get('operations_hs', None)
  topside_exists = data.get('topside_exists', None)

  maintenance_date = data.get('maintenance_date', None)
  post_burial = data.get('post_burial', None)
  project_life = data.get('project_life', None)
  device_repair_at_port = data.get('device_repair_at_port', None)

  if quantile is not None:
    lmo_study.inputs.quantile = quantile
  if quantile_vessels is not None:
    lmo_study.inputs.quantile_vessels = quantile_vessels
  if period is not None:
    lmo_study.inputs.period = period

  if max_wait is not None:
    lmo_study.inputs.max_wait = max_wait
  if montecarlo is not None:
    lmo_study.inputs.montecarlo = montecarlo
  if comb_retain_ratio is not None:
    lmo_study.inputs.comb_retain_ratio = comb_retain_ratio
  if comb_retain_min is not None:
    lmo_study.inputs.comb_retain_min = comb_retain_min
  if safety_factor is not None:
    lmo_study.inputs.safety_factor = safety_factor
  if device_draft is not None:
    lmo_study.inputs.device_draft = device_draft
  if consider_draft is not None:
    lmo_study.inputs.consider_draft = consider_draft
  if fuel_price is not None:
    lmo_study.inputs.fuel_price = fuel_price
  if sfoc is not None:
    lmo_study.inputs.sfoc = sfoc
  if load_factor is not None:
    lmo_study.inputs.load_factor = load_factor

  if installation_date is not None:
    lmo_study.inputs.installation_date = installation_date
  if operations_hs is not None:
    lmo_study.inputs.operations_hs = operations_hs
  if topside_exists is not None:
    lmo_study.inputs.topside_exists = topside_exists

  if maintenance_date is not None:
    lmo_study.inputs.maintenance_date = maintenance_date
  if post_burial is not None:
    lmo_study.inputs.post_burial = post_burial
  if project_life is not None:
    lmo_study.inputs.project_life = project_life
  if device_repair_at_port is not None:
    lmo_study.inputs.device_repair_at_port = device_repair_at_port

  # Update database
  try:
    db.session.commit()
    return lmo_study.inputs
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_inputs(lmoid):
  """
  Delete LMO study inputs from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str lmoid: the ID of the LMO study in local storage database to delete inputs
  :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
  :rtype: dict
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return "LMO study with that ID does not exist."
  elif lmo_study.inputs is None:
    return "LMO study with that ID does not have inputs defined."
  else:
    db.session.delete(lmo_study.inputs)
    db.session.commit()
    return "Inputs deleted"


def create_inputs_modules(data, complexity):
  """
  Create a new set of other module inputs and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create a Module object.
    complexity (str): LMO study complexity.

  Returns:
    dict: the newly created Module instance
  """
  module_name = data["module"].lower()
  if module_name == 'mc':
    valid = validate_module_mc(data, complexity)
  elif module_name == 'ec':
    valid = validate_module_ec(data, complexity)
  elif module_name == 'et':
    valid = validate_module_et(data, complexity)
  elif module_name == 'ed':
    valid = validate_module_ed(data, complexity)
  elif module_name == 'sk':
    valid = validate_module_sk(data, complexity)
  else:
    valid = 'Unknown module: ' + str(module_name)

  if valid is True:
    new_data = {
        "module": deepcopy(data["module"]),
        "study_id": deepcopy(data["study_id"])
    }
    del data["module"]
    del data["study_id"]
    if module_name == 'mc':
      design = deepcopy(data)
      hierarchy = None
    elif module_name == 'ec':
      design = deepcopy(data)
      hierarchy = None
    elif module_name == 'et':
      design = deepcopy(data)
      del design["array"]
      hierarchy = deepcopy(data["array"]["Hierarchy"]["value"])
    elif module_name == 'ed':
      design = deepcopy(data)
      del design["hierarchy_new"]
      hierarchy = deepcopy(data["hierarchy_new"])
    elif module_name == 'sk':
      design = deepcopy(data["hierarchy_data"])
      hierarchy = deepcopy(data)
      del hierarchy["hierarchy_data"]

    lmo_inputs_module_schema = ModuleSchema()
    lmo_inputs_module = lmo_inputs_module_schema.load(new_data)

    lmo_inputs_module.design = dict(design)
    if hierarchy is not None:
      # TODO: Review this!!!
      # If repair failure rates are not provided, assume the same value of replacement
      all_na = True
      for item in hierarchy["failure_rate_repair"]:
        try:
          if item.lower() != 'na':
            all_na = False
            break
        except AttributeError:
          # Not a string
          all_na = False
          break
      if all_na:
        hierarchy["failure_rate_repair"] = hierarchy["failure_rate_replacement"]
      # If replacement failure rates are not provided, assume the same value of repair
      all_na = True
      for item in hierarchy["failure_rate_replacement"]:
        try:
          if item.lower() != 'na':
            all_na = False
            break
        except AttributeError:
          # Not a string
          all_na = False
          break
      if all_na:
        hierarchy["failure_rate_replacement"] = hierarchy["failure_rate_repair"]

      hierarchy = _hierarchy_linearizer(hierarchy)
      lmo_inputs_module.hierarchy = dict(hierarchy)
  else:
    _e = '%s module inputs not valid. ' % module_name.upper()
    _e = _e + str(valid)
    return _e

  try:
    db.session.add(lmo_inputs_module)
    db.session.commit()
    return lmo_inputs_module
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'


def update_inputs_modules(data, study_id):
  """
  Update a set of other modules inputs and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to update a Module object.

  Returns:
    dict: the updated Module instance
  """
  lmo_study = LMOStudy.query.get(study_id)

  try:
    module_mc_data = data["module_mc"]
    lmo_study.modules.module_mc = module_mc_data
  except TypeError:
    lmo_study.modules.module_mc = None
  except KeyError:
    pass

  try:
    module_ec_data = data["module_ec"]
    lmo_study.modules.module_ec = module_ec_data
  except TypeError:
    lmo_study.modules.module_ec = None
  except KeyError:
    pass

  try:
    module_et_data = data["module_et"]
    lmo_study.modules.module_et = module_et_data
    try:
      hierarchy_et = module_et_data["array"]["Hierarchy"]["value"]
      hierarchy_et = _hierarchy_linearizer(hierarchy_et)
      lmo_study.modules.hierarchy_et = hierarchy_et
    except TypeError:
      lmo_study.modules.hierarchy_et = None
    except KeyError:
      pass
  except TypeError:
    lmo_study.modules.module_et = None
  except KeyError:
    pass

  try:
    module_ed_data = data["module_ed"]
    lmo_study.modules.module_ed = module_ed_data
    try:
      hierarchy_ed = module_ed_data["hierarchy_new"]
      hierarchy_ed = _hierarchy_linearizer(hierarchy_ed)
      lmo_study.modules.hierarchy_ed = hierarchy_ed
    except TypeError:
      lmo_study.modules.hierarchy_ed = None
    except KeyError:
      pass
  except TypeError:
    lmo_study.modules.module_ed = None
  except KeyError:
    pass

  try:
    module_sk_data = data["module_sk"]
    lmo_study.modules.module_sk = module_sk_data
    try:
      hierarchy_sk = module_sk_data["hierarchy"]
      hierarchy_sk = _hierarchy_linearizer(hierarchy_sk)
      lmo_study.modules.hierarchy_sk = hierarchy_sk
    except TypeError:
      lmo_study.modules.hierarchy_sk = None
    except KeyError:
      pass
  except TypeError:
    lmo_study.modules.module_sk = None
  except KeyError:
    pass

  # Update database
  try:
    db.session.commit()
    return lmo_study.modules
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_inputs_modules(module_id):
  """
  Delete LMO study other modules inputs from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  :param str module_id: the ID of the external Module in local storage database to delete inputs
  :return: error message if ID does not exist, dictionary with deleted status set to True if deleted successfully
  :rtype: dict
  """
  lmo_module = Module.query.get(module_id)
  try:
    db.session.delete(lmo_module)
    db.session.commit()
    return "module inputs deleted"
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'


def _hierarchy_linearizer(hierarchy: dict):
  # Join parents in a string
  new_parents = []
  for parents in hierarchy["parent"]:
    try:
      new_parents.append(parents.lower())
    except AttributeError:
      new_parents.append(', '.join(parents))
  hierarchy["parent"] = new_parents
  # Join children in a string
  new_childs = []
  for childs in hierarchy["child"]:
    try:
      new_childs.append(childs.lower())
    except AttributeError:
      new_childs.append(', '.join(childs))
  hierarchy["child"] = new_childs

  return hierarchy
