# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses business logic functions
# It provides business logic via HTTP API.
# It create a blueprint and define routes.

from flask import Blueprint, request, jsonify
from marshmallow import ValidationError

from ..errors import bad_request, not_found
from ..utils import join_validation_error_messages

from .lmo_phases import (
    create_phase, delete_phase, delete_phases,
    create_requirements, update_requirements, delete_phase_requirements
)
from dtop_lmo.service.models import LMOStudy, Phase, Requirements
from dtop_lmo.service.schemas import (
    PhaseSchema, LoadPhaseSchema, RequirementsSchema
)

from ....service import db

bp = Blueprint('api_phases', __name__)
"""
  @oas [get] /api/{lmoid}/{phase}
  description: Returns phase information stored in database
"""

@bp.route('/phases', methods=['GET'])
def get_lmo_study_phases(lmoid):
  """
  Flask blueprint route for getting phases of a single LMO study.

  Returns an error if this study ID does not have any phase;
  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (str): the ID of the LMO study

  Returns:
    dict: the serialised response containing the selected LMO study phases or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    if not lmo_study.phases:
      return bad_request('LMO study does not have any phase defined.')

    lmo_phase_schema = PhaseSchema(many=True)
    lmo_phases = lmo_phase_schema.dump(lmo_study.phases)

    return jsonify(lmo_phases)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases', methods=['POST'])
def post_lmo_phase(lmoid):
  """
  Flask blueprint route to add a new Phase to a LMO study.

  Calls the :meth:`~dtop_lmo.service.api.phases.create_phase()` method.
  Returns HTTP 400 error if no request body provided or if a Phase with te same name was already defined for this study
  Returns 404 error if study ID does not exist.

  Returns:
    dict: the serialised response containing the logistic Phase added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    try:
      LoadPhaseSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    phase_name = request_body['name']
    phases_names = [ph.name for ph in lmo_study.phases]
    if phase_name in phases_names:
      _e = 'A Phase with this \"name\" is already present in this LMO study'
      return bad_request(_e)

    request_body['study_id'] = lmoid
    request_body['name'] = request_body['name'].lower()
    lmo_phase = create_phase(request_body)
    if type(lmo_phase) is str:
      # Error inside create_study()
      return bad_request(lmo_phase)
    lmo_phase.plan = None
    lmo_phase.downtime = None
    lmo_phase_schema = PhaseSchema()

    result = {
        "created_phase": lmo_phase_schema.dump(lmo_phase),
        "message": lmo_phase.name + " Phase created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases', methods=['DELETE'])
def del_lmo_phases(lmoid):
  """
  Flask blueprint route for delete all phases of a single LMO study.

  Calls the :meth:`~dtop_lmo.service.api.projects.delete_phases()` method.
  Returns 404 error if study ID does not exist in the database.

  Args:
    lmoid (str): the ID of the LMO study

  Returns:
    dict: serialised response containing 'Phases deleted' message; not found error otherwise
  """
  try:
    data = delete_phases(lmoid)
    if data.lower() == 'phases deleted':
      response = {
          'study_id': lmoid,
          'message': 'Phases deleted.'
      }
      return jsonify(response)
    else:
      return not_found(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>', methods=['GET'])
def get_lmo_phase(lmoid, phase):
  """
  Flask blueprint route for getting a specific phase of a single LMO study.

  Returns an error if this study ID does not have any phase;
  Returns 404 error if study ID does not exist in the database.
  Returns 404 error if specified phase is not defined for this study.

  Args:
    lmoid (str): the ID of the LMO study
    phase (str): name of the logistic phase

  Returns:
    dict: the serialised response containing the selected LMO study phase or a HTTP 404 error
  """
  try:
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return 'This phase is not defined for this LMO study yet.'

    lmo_phase_schema = PhaseSchema()

    return jsonify(lmo_phase_schema.dump(lmo_phase))

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>', methods=['DELETE'])
def del_lmo_phase(lmoid, phase):
  """
  Flask blueprint route for delete a specific phase of a single LMO study.

  Calls the :meth:`~dtop_lmo.service.api.projects.delete_phase()` method.
  Returns an error if this study ID does not have any phase;
  Returns 404 error if study ID does not exist in the database.
  Returns 404 error if specified phase is not defined for this study.

  Args:
    lmoid (str): the ID of the LMO study
    phase (str): name of the logistic phase

  Returns:
    dict: serialised response containing 'Phase deleted' message; not found error otherwise
  """
  try:
    data = delete_phase(lmoid, phase)
    if data.lower() == 'phase deleted':
      response = {
          'study_id': lmoid,
          'phase_name': phase,
          'message': 'Phase deleted.'
      }
      return jsonify(response)
    else:
      return not_found(data)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/requirements', methods=['POST'])
def post_phase_requirements(lmoid, phase):
  """
  Flask blueprint route to add Requirements to a given phase.

  Calls the :meth:`~dtop_lmo.service.api.operations.create_requirements()` method.
  Returns HTTP 400 error if operations for this phase were not defined yet;
  Returns HTTP 400 error if no request body provided;
  Returns 404 error if project ID does not exist.
  Returns 404 error if operation ID does not exist.

  Returns:
    dict: the serialised response containing the operation Requirements added to the database.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')

    request_body = request.get_json()
    # if not request_body:
    #   return bad_request('No request body provided')
    try:
      RequirementsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    # Get phase ID
    lmo_phases = Phase.query.filter_by(study_id=lmoid).all()
    if len(lmo_phases) == 0:
      return bad_request('No phases defined.')
    for lmo_phase in lmo_phases:
      if lmo_phase.name == phase:
        break

    request_body["phase_id"] = lmo_phase.id
    lmo_requirements = create_requirements(request_body)
    if type(lmo_requirements) is str:
      # Error inside create_requirements()
      return bad_request(lmo_requirements)

    lmo_requirements_schema = RequirementsSchema()

    result = {
        "created_requirements": lmo_requirements_schema.dump(lmo_requirements),
        "phase": phase,
        "message": "Requirements created"
    }
    response = jsonify(result)
    response.status_code = 201

    return response

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/requirements', methods=['GET'])
def get_phase_requirements(lmoid, phase):
  """
  Flask blueprint route to get a phase Requirements.

  Returns:
    dict: the serialised response containing the phase Requirements in database.
  """
  try:
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return not_found('Phase with that name could not be found')
    lmo_requirements = Requirements.query.filter_by(phase_id=lmo_phase.id).first()
    if lmo_requirements is None:
      return bad_request('This phase does not have requirements defined yet')
    lmo_requirements_schema = RequirementsSchema()

    result = lmo_requirements_schema.dump(lmo_requirements)
    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/requirements', methods=['PUT'])
def update_phase_requirements(lmoid, phase):
  """
  Flask blueprint route to update a phase Requirements.

  Returns:
    dict: the serialised response containing the phase Requirements in database.
  """
  try:
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return not_found('Phase with that name could not be found')

    if lmo_phase.requirements is None:
      return bad_request('This phase does not have requirements defined yet')

    request_body = request.get_json()
    if not request_body:
      return bad_request('No request body provided')

    try:
      RequirementsSchema().load(request_body)
    except ValidationError as err:
      msg = join_validation_error_messages(err)
      return bad_request(msg)

    lmo_requirements = update_requirements(request_body, lmo_phase.id)
    if type(lmo_requirements) is str:
      # Error inside update_requirements()
      return bad_request(lmo_requirements)
    lmo_requirements_schema = RequirementsSchema()

    result = {
        "updated_requirements": lmo_requirements_schema.dump(lmo_requirements),
        "phase": phase,
        "message": "Requirements updated"
    }

    return jsonify(result)

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))


@bp.route('/phases/<phase>/requirements', methods=['DELETE'])
def del_phase_requirements(lmoid, phase):
  """
  Flask blueprint route to add delete all Requirements from a phase.

  Calls the :meth:`~dtop_lmo.service.api.operations.delete_phase_requirements()`
  Returns HTTP 404 error if there is no LMO study with this ID;
  Returns HTTP 400 error if a phase with this name was not defined yet.

  Args:
      lmoid (str): ID of the LMO study
      phase (str): The name of the logistic phase

  Returns:
      dict: the serialised success response.
  """
  try:
    # Look for a study with this lmoid
    lmo_study = LMOStudy.query.get(lmoid)
    if lmo_study is None:
      return not_found('LMO study with that ID could not be found')
    lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
    if lmo_phase is None:
      return bad_request('%s phase not defined for this LMO study' % phase)
    lmo_requirements = Requirements.query.filter_by(phase_id=lmo_phase.id).first()
    if lmo_requirements is None:
      return bad_request('Requirements not defined for this phase')

    data = delete_phase_requirements(lmo_requirements.id)

    return jsonify(
        {
            "study_id": lmoid,
            "phase_name": phase,
            "message": data
        }
    )

  except Exception as err:
    return bad_request('Unknown error: ' + str(err))
