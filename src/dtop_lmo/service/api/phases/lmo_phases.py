# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from sqlalchemy.exc import IntegrityError

from ....service import db

from ...models import LMOStudy, Phase, Requirements
from ...schemas import PhaseSchema, RequirementsSchema


def create_phase(data):
  """
  Create a new LMO phase and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create a Phase object.

  Returns:
    dict: the newly created Phase instance
  """
  lmo_phase_schema = PhaseSchema()
  lmo_phase = lmo_phase_schema.load(data)

  try:
    db.session.add(lmo_phase)
    db.session.commit()
    return lmo_phase
  except IntegrityError:
    db.session.rollback()
    return 'Phase with that name already exists.'


def delete_phase(lmoid, phase):
  """
  Delete a logistic phase from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted
    phase (str): the name of the logistic phase in local storage database to be deleted

  Returns:
    str: Success message if study is deleted; Error message if the ID does not exist.
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return 'LMO study with that ID does not exist.'
  lmo_phase = Phase.query.filter_by(study_id=lmoid, name=phase).first()
  if lmo_phase is None:
    return 'Phase with this name was not found for this study.'

  db.session.delete(lmo_phase)
  db.session.commit()
  return 'Phase deleted'


def delete_phases(lmoid):
  """
  Delete all logistic phases of a study from the local storage database.

  If study with the specified ID does not exist, returns an error message that leads to a HTTP 404 error.

  Args:
    lmoid (int): the ID of the LMO study in local storage database to be deleted

  Returns:
    str: Success message if study is deleted; Error message if the ID does not exist.
  """
  lmo_study = LMOStudy.query.get(lmoid)
  if lmo_study is None:
    return 'LMO study with that ID does not exist.'

  try:
    db.session.query(Phase).filter_by(study_id=lmoid).delete()
    db.session.commit()
    return 'Phases deleted'
  except IntegrityError:
    db.session.rollback()
    return "Unknown error."


def create_requirements(data):
  """
  Create new phase Requirements and add it to the local storage database.

  Args:
    data (dict): dictionary with keys required to create Requirements object.

  Returns:
    dict: the newly created Requirements instance
  """
  lmo_requirements_schema = RequirementsSchema()
  lmo_requirements = lmo_requirements_schema.load(data)

  try:
    db.session.add(lmo_requirements)
    db.session.commit()
    return lmo_requirements
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def update_requirements(data, phaseid):
  """
  Update a operation Requirements entry in the local storage database.

  Args:
    data (dict): dictionary with keys required to update the operation Requirements object in the local database.
    phaseid (str): the ID of the Phase in the local storage database

  Returns:
    dict: the updated operation Requirements instance
  """
  lmo_phase = Phase.query.get(phaseid)

  # Update existing requirements with existing data from "data"
  rov = data.get('rov', None)
  divers = data.get('divers', None)
  previous_projects = data.get('previous_projects', None)
  terminal_area = data.get('terminal_area', None)
  terminal_load = data.get('terminal_load', None)
  terminal_crane = data.get('terminal_crane', None)
  connect_to_electrical = data.get('connect_to_electrical', None)
  port_maximum_distance = data.get('port_maximum_distance', None)
  if rov is not None:
    lmo_phase.requirements.rov = rov
  if divers is not None:
    lmo_phase.requirements.divers = divers
  if previous_projects is not None:
    lmo_phase.requirements.previous_projects = previous_projects
  if terminal_area is not None:
    lmo_phase.requirements.terminal_area = terminal_area
  if terminal_load is not None:
    lmo_phase.requirements.terminal_load = terminal_load
  if terminal_crane is not None:
    lmo_phase.requirements.terminal_crane = terminal_crane
  if connect_to_electrical is not None:
    lmo_phase.requirements.connect_to_electrical = connect_to_electrical
  if port_maximum_distance is not None:
    lmo_phase.requirements.port_maximum_distance = port_maximum_distance

  # Update database
  try:
    db.session.commit()
    return lmo_phase.requirements
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error.'


def delete_phase_requirements(reqid):
  """
  Delete phase requirements from the local storage database.

  Args:
    reqid (int): the ID of the phase requirements in local storage database

  Returns:
    str: Success message when requirements are deleted
  """
  lmo_requirements = Requirements.query.get(reqid)
  try:
    db.session.delete(lmo_requirements)
    db.session.commit()
    return 'Phase requirements deleted'
  except IntegrityError:
    db.session.rollback()
    return 'Unknown error'


def delete_requirements(requirements_id):
  """
  Delete operation requirements from the local storage database.

  Args:
    requirements_id (int): the ID of Requirements in local storage database

  Returns:
    str: Success message when requirements are deleted
  """
  lmo_requirements = Requirements.query.get(requirements_id)
  db.session.delete(lmo_requirements)
  db.session.commit()
  return 'All requirements deleted'
