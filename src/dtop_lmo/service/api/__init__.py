# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import requests
from flask import Blueprint, request, jsonify

from .errors import bad_request

bp = Blueprint('api_pact', __name__)


@bp.route('/sc-farm', methods=['POST'])
def sc_farm():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *farm* from SC module.

  To be used in PACT testing example.

  Example request body for SC:

  {
    "sc_farm": "http://localhost:{port}/sc/{ProjectId}/farm"
  }

  :return: SC farm
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["sc_farm"]
  try:
    direct_values = requests.get(url).json()["direct_values"]
    bathymetry = direct_values["bathymetry"]
    bath_latitude = bathymetry["latitude"]
    bath_longitude = bathymetry["longitude"]
    bath_value = bathymetry["value"]
    seabed_type = direct_values["seabed_type"]
    sb_latitude = seabed_type["latitude"]
    sb_longitude = seabed_type["longitude"]
    sb_value = seabed_type["value"]
    
    info = requests.get(url).json()["info"]
    latitude = info["latitude"]
    longitude = info["longitude"]
    print(bath_latitude)
    print(bath_longitude)
    print(bath_value)
    print(sb_latitude)
    print(sb_longitude)
    print(sb_value)
    print(latitude)
    print(longitude)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SC farm failed')

  result = {
      "direct_values": {
          "bathymetry": {
              "latitude": bath_latitude,
              "longitude": bath_longitude,
              "value": bath_longitude
          },
          "seabed_type": {
              "latitude": sb_latitude,
              "longitude": sb_longitude,
              "value": sb_value
          }
      },
      "info": {
          "latitude": latitude,
          "longitude": longitude
      }
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/sc-point', methods=['POST'])
def sc_point():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *point* from SC module.

  To be used in PACT testing example.

  Example request body for SC:

  {
    "sc_point": "http://localhost:{port}/sc/{ProjectId}/point"
  }

  :return: SC point
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["sc_point"]
  try:
    name = requests.get(url).json()["name"]
    print(name)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SC point failed')

  result = {
      "name": name
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/sc-time_series', methods=['POST'])
def sc_time_series():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *time_series* from SC module.

  To be used in PACT testing example.

  Example request body for SC:

  {
    "sc_time_series": "http://localhost:{port}/sc/{ProjectId}/time_series"
  }

  :return: SC time_series
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["sc_time_series"]
  try:
    times = requests.get(url).json()["waves"]["hs"]["times"]
    waves = requests.get(url).json()["waves"]
    hs = waves["hs"]["values"]
    tp = waves["tp"]["values"]
    currents = requests.get(url).json()["currents"]
    cs = currents["mag"]["values"]
    winds = requests.get(url).json()["winds"]
    ws = winds["mag10"]["values"]
    print(times)
    print(hs)
    print(tp)
    print(cs)
    print(ws)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SC time_series failed')

  result = {
      "times": times,
      "hs": hs,
      "tp": tp,
      "cs": cs,
      "ws": ws
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/mc-general', methods=['POST'])
def mc_general():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *general* details from MC module.

  To be used in PACT testing example.

  Example request body for MC:

  {
    "mc_general": "http://localhost:{port}/mc/{mcId}/general"
  }

  :return: the general characteristics of machine
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["mc_general"]
  try:
    floating = requests.get(url).json()["floating"]
    machine_cost = requests.get(url).json()["machine_cost"]
    print(floating)
    print(machine_cost)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to MC general failed')

  result = {
      "floating": floating,
      "machine_cost": machine_cost
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/mc-dimensions', methods=['POST'])
def mc_dimensions():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *dimensions* details from MC module.

  To be used in PACT testing example.

  Example request body for MC:

  {
    "mc_dimensions": "http://localhost:{port}/mc/{mcId}/dimensions"
  }

  :return: the dimensions characteristics of machine
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["mc_dimensions"]
  try:
    height = requests.get(url).json()["height"]
    width = requests.get(url).json()["width"]
    length = requests.get(url).json()["length"]
    mass = requests.get(url).json()["mass"]
    print(height)
    print(width)
    print(length)
    print(mass)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to MC dimensions failed')

  result = {
      "height": height,
      "width": width,
      "length": length,
      "mass": mass
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/ec-farm', methods=['POST'])
def ec_farm():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *farm* details from EC module.

  To be used in PACT testing example.

  Example request body for EC:

  {
    "ec_farm": "http://localhost:{port}/ec/{ecId}/farm"
  }

  :return: the farm layout
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["ec_farm"]
  try:
    layout = requests.get(url).json()["layout"]
    number_devices = requests.get(url).json()["number_devices"]
    print(layout)
    print(number_devices)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to EC farm failed')

  result = {
      "layout": layout,
      "number_devices": number_devices
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/et-array', methods=['POST'])
def et_array():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *array* from ET module.

  To be used in PACT testing example.

  Example request body for ET:

  {
    "et_array": "http://localhost:{port}/energy_transf/{etId}/array"
  }

  :return: array
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["et_array"]
  try:
    hierarchy = requests.get(url).json()["Hierarchy"]["value"]
    system = hierarchy["system"]
    name_of_node = hierarchy["name_of_node"]
    design_id = hierarchy["design_id"]
    node_type = hierarchy["node_type"]
    node_subtype = hierarchy["node_subtype"]
    category = hierarchy["category"]
    parent = hierarchy["parent"]
    child = hierarchy["child"]
    gate_type = hierarchy["gate_type"]
    failure_rate_repair = hierarchy["failure_rate_repair"]
    failure_rate_replacement = hierarchy["failure_rate_replacement"]
    print(hierarchy)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ET array failed')

  result = {
      "hierarchy": hierarchy
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/et-devices', methods=['POST'])
def et_devices():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *devices* from ET module.

  To be used in PACT testing example.

  Example request body for ET:

  {
    "et_devices": "http://localhost:{port}/energy_transf/{etId}/devices"
  }

  :return: devices
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["et_devices"]
  try:
    pto_cost = requests.get(url).json()[0]["Dev_PTO_cost"]["value"]
    pto_mass = requests.get(url).json()[0]["Dev_PTO_mass"]["value"]
    print(pto_cost)
    print(pto_mass)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ET devices failed')

  result = {
      "pto_cost": pto_cost,
      "pto_mass": pto_mass
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/et-ptos', methods=['POST'])
def et_ptos():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *ptos* from ET module.

  To be used in PACT testing example.

  Example request body for ET:

  {
    "et_ptos": "http://localhost:{port}/energy_transf/{etId}/device/{deviceId}/ptos"
  }

  :return: ptos
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["et_ptos"]
  try:
    elect_mass = requests.get(url).json()[0]["Elect_mass"]["value"]
    grid_mass = requests.get(url).json()[0]["Grid_mass"]["value"]
    mech_mass = requests.get(url).json()[0]["Mech_mass"]["value"]
    id_ = requests.get(url).json()[0]["id"]["value"]
    print(elect_mass)
    print(grid_mass)
    print(mech_mass)
    print(id_)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ET ptos failed')

  result = {
      "elect_mass": elect_mass,
      "grid_mass": grid_mass,
      "mech_mass": mech_mass,
      "id": id_
  }
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/ed-results', methods=['POST'])
def ed_results():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *results* from ED module.

  To be used in PACT testing example.

  Example request body for ED:

  {
    "ed_results": "http://localhost:{port}/api/energy-deliv-studies/{edId}/results"
  }

  :return: ED results
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["ed_results"]
  try:
    hierarchy = requests.get(url).json()["hierarchy_new"]
    cable_dict = requests.get(url).json()["cable_dict"]
    collection_point_dict = requests.get(url).json()["collection_point_dict"]
    if request_body["cpx"] == '3':
      umbilical_dict = requests.get(url).json()["umbilical_dict"]
      cable_inst = requests.get(url).json()["cable_installation"]
      connectors_dict = requests.get(url).json()["connectors_dict"]
    print(hierarchy)
    print(cable_dict)
    print(collection_point_dict)
    if request_body["cpx"] == '3':
      print(umbilical_dict)
      print(cable_inst)
      print(connectors_dict)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to ED results failed')

  result = {
      "hierarchy": hierarchy,
      "cable_dict": cable_dict,
      "collection_point_dict": collection_point_dict
  }
  if request_body["cpx"] == '3':
    result["umbilical_dict"] = umbilical_dict
    result["cable_inst"] = cable_inst
    result["connectors_dict"] = connectors_dict
  response = jsonify(result)
  response.status_code = 201

  return response


@bp.route('/sk-hierarchy', methods=['POST'])
def sk_hierarchy():
  """
  Flask blueprint route for posting a URL to the appropriate path for getting *hierarchy* from SK module.

  To be used in PACT testing example.

  Example request body for SK:

  {
    "sk_hierarchy": "http://localhost:{port}/sk/{ProjectId}/hierarchy"
  }

  :return: SK results
  :rtype: dict
  """
  request_body = request.get_json()
  if not request_body:
    return bad_request('No request body provided')
  url = request_body["sk_hierarchy"]
  try:
    system = requests.get(url).json()["system"]
    name_of_node = requests.get(url).json()["name_of_node"]
    node_type = requests.get(url).json()["node_type"]
    node_subtype = requests.get(url).json()["node_subtype"]
    category = requests.get(url).json()["category"]
    parent = requests.get(url).json()["parent"]
    child = requests.get(url).json()["child"]
    gate_type = requests.get(url).json()["gate_type"]
    failure_rate_repair = requests.get(url).json()["failure_rate_repair"]
    failure_rate_replacement = requests.get(url).json()["failure_rate_replacement"]
    hierarchy_data = requests.get(url).json()["hierarchy_data"]
    anchor_list = hierarchy_data["anchor_list"]
    foundation_list = hierarchy_data["foundation_list"]
    line_segment_list = hierarchy_data["line_segment_list"]
    print(anchor_list)
    print(foundation_list)
    print(line_segment_list)
  except requests.exceptions.RequestException as err:
    print(err)
    return bad_request('HTTP request to SK hierarchy failed')

  result = {
      "hierarchy": {
          "system": system,
          "name_of_node": name_of_node,
          "node_type": node_type,
          "node_subtype": node_subtype,
          "category": category,
          "parent": parent,
          "child": child,
          "gate_type": gate_type,
          "failure_rate_repair": failure_rate_repair,
          "failure_rate_replacement": failure_rate_replacement
      },
      "anchor_list": anchor_list,
      "foundation_list": foundation_list,
      "line_segment_list": line_segment_list
  }
  response = jsonify(result)
  response.status_code = 201

  return response
