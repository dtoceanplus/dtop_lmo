# Logistics and Marine Operations (LMO) tool.
# LMO is DTOceanPlus tool to assess marine operations withing wave and tidal
# energy projects.
# Copyright (C) 2020 Luis Amaral and Francisco Correira da Fonseca
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the Affero GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the Affero GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# This package uses service logic API.
# It provides GUI via HTTP API.
# It create a blueprint and define routes.

# Templates: http://flask.pocoo.org/docs/1.0/blueprints/#templates

from flask import Blueprint, render_template, url_for, request

from dtop_lmo.business import foo


bp = Blueprint('gui_foo', __name__)


@bp.route('/foo')
def index():
  # Force access to service level via HTTP request. For example purpose.

  # Get url to sevice.foo.index()
  # It works because service and gui is a single service now.
  # For access to different service you should get url in other way.
  foo_index_url = url_for('api_foo.index')
  url = request.url_root + foo_index_url

  # access to Service API
  # import requests
  # text = requests.get(url).text
  content = 'bar'

  return render_template('gui/index.html', content=content)
