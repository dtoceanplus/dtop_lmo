# %%
import os
import json
from matplotlib import pyplot as plt


sc_file = os.path.join('C:\\Users','LAmaral','WavEC Offshore Renewables','WavEC_Tecnologias - Documents','DTOceanPlus','LMO','Verification','Verification data','Integration','RM3','response_sc_cpx1.json')
with open(sc_file, 'r') as f:
  sc_data = json.load(f)
# %%

hs = sc_data["point"]["time_series"]["waves"]["hs"]["values"]