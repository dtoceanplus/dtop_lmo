import json
import sys

import dredd_hooks as hooks
import requests

sys.stdout = sys.stderr = open("dredd-hooks-output.txt", "w")

def if_not_skipped(func):
  def wrapper(transaction):
    if not transaction['skip']:
      func(transaction)
  return wrapper

# List of hooks to be skipped.
# List all possible hooks here. Uncomment to check that DREDD is correctly and ready for testing.
# @hooks.before('/api/study > Return the list of Logistic and Marine Operations (LMO) studies > 200 > application/json')
# @hooks.before('/api/study > Add a Logistic and Marine Operations (LMO) study > 201 > application/json')
# @hooks.before('/api/study > Add a Logistic and Marine Operations (LMO) study > 400 > application/json')
# @hooks.before('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 200 > application/json')
# @hooks.before('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 404 > application/json')
# @hooks.before('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 200 > application/json')
# @hooks.before('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 404 > application/json')
# @hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 200 > application/json')
# @hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 400 > application/json')
# @hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 404 > application/json')
# @hooks.before('/api/{lmoid}/objects > Return the list of Objects > 200 > application/json')
# @hooks.before('/api/{lmoid}/objects > Return the list of Objects > 404 > application/json')
# @hooks.before('/api/{lmoid}/objects > Add objects > 201 > application/json')
@hooks.before('/api/{lmoid}/objects > Add objects > 400 > application/json')    # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
# @hooks.before('/api/{lmoid}/objects > Add objects > 404 > application/json')
# @hooks.before('/api/{lmoid}/cables > Return the list of Cables > 200 > application/json')
# @hooks.before('/api/{lmoid}/cables > Return the list of Cables > 404 > application/json')
# @hooks.before('/api/{lmoid}/cables > Add cables > 201 > application/json')
@hooks.before('/api/{lmoid}/cables > Add cables > 400 > application/json')      # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
# @hooks.before('/api/{lmoid}/cables > Add cables > 404 > application/json')
@hooks.before('/api/{lmoid}/site > Return the Site > 200 > application/json')   # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
@hooks.before('/api/{lmoid}/site > Return the Site > 400 > application/json')   # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
# @hooks.before('/api/{lmoid}/site > Return the Site > 404 > application/json')
@hooks.before('/api/{lmoid}/site > Add site > 201 > application/json')          # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
@hooks.before('/api/{lmoid}/site > Add site > 400 > application/json')          # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
@hooks.before('/api/{lmoid}/site > Add site > 404 > application/json')          # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
@hooks.before('/api/{lmoid}/site > Delete the Logistic and Marine Operations (LMO) study > 200 > application/json') # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
@hooks.before('/api/{lmoid}/site > Delete the Logistic and Marine Operations (LMO) study > 404 > application/json') # TODO: DREDD TEST LATER; NOTE: need assyncronous tasks
# @hooks.before('/api/{lmoid}/inputs > Return inputs > 200 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Return inputs > 404 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Add user inputs > 201 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Add user inputs > 400 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Add user inputs > 404 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Update inputs > 200 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Update inputs > 400 > application/json')
# @hooks.before('/api/{lmoid}/inputs > Update inputs > 404 > application/json')
@hooks.before('/api/{lmoid}/phases > Return phases > 200 > application/json')
@hooks.before('/api/{lmoid}/phases > Return phases > 404 > application/json')
@hooks.before('/api/{lmoid}/phases > Add phase > 201 > application/json')
@hooks.before('/api/{lmoid}/phases > Add phase > 400 > application/json')
@hooks.before('/api/{lmoid}/phases > Add phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase} > Return phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase} > Return phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase} > Delete phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase} > Delete phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Reads operations from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Reads operations from a given phase > 400 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Reads operations from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Adds operations to a given phase > 201 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Adds operations to a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Deletes all operation from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/operations > Deletes all operation from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/methods/{component_type} > Define operations methods > 201 > application/json')
@hooks.before('/api/{lmoid}/methods/{component_type} > Define operations methods > 400 > application/json')
@hooks.before('/api/{lmoid}/methods/{component_type} > Define operations methods > 404 > application/json')
@hooks.before('/api/{lmoid}/methods > Delete operations methods > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Define operations requirements > 201 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Define operations requirements > 400 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Define operations requirements > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Delete operations requirements from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Delete operations requirements from a given phase > 400 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/requirements > Delete operations requirements from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Perform all phase calculations > 201 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Perform all phase calculations > 400 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Perform all phase calculations > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Delete results from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Delete results from a given phase > 400 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/calculate > Delete results from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/plan > Reads phase plan from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/plan > Reads phase plan from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/plan > Reads phase plan from a given phase > 500 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/cost > Get the overall cost from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/cost > Get the overall cost from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/cost > Get the overall cost from a given phase > 500 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/logistic > Get logistic solution from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/logistic > Get logistic solution from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/logistic > Get logistic solution from a given phase > 500 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/consumption > Get vessels fuel consumption from a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/consumption > Get vessels fuel consumption from a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/consumption > Get vessels fuel consumption from a given phase > 500 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/duration > Get the total duration of a given phase > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/duration > Get the total duration of a given phase > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/{phase}/duration > Get the total duration of a given phase > 500 > application/json')
@hooks.before('/api/{lmoid}/phases/maintenance/downtime > Read downtime due to failures > 200 > application/json')
@hooks.before('/api/{lmoid}/phases/maintenance/downtime > Read downtime due to failures > 404 > application/json')
@hooks.before('/api/{lmoid}/phases/maintenance/downtime > Read downtime due to failures > 500 > application/json')
def _skip(transaction):
  transaction['skip'] = True


@hooks.before('/api/study > Return the list of Logistic and Marine Operations (LMO) studies > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.before('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 400 > application/json')
@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.before('/api/{lmoid}/objects > Return the list of Objects > 200 > application/json')
@hooks.before('/api/{lmoid}/objects > Return the list of Objects > 404 > application/json')
@hooks.before('/api/{lmoid}/objects > Add objects > 201 > application/json')
@hooks.before('/api/{lmoid}/objects > Add objects > 404 > application/json')
@hooks.before('/api/{lmoid}/cables > Return the list of Cables > 200 > application/json')
@hooks.before('/api/{lmoid}/cables > Return the list of Cables > 404 > application/json')
@hooks.before('/api/{lmoid}/cables > Add cables > 201 > application/json')
@hooks.before('/api/{lmoid}/cables > Add cables > 404 > application/json')
@hooks.before('/api/{lmoid}/site > Return the Site > 404 > application/json')
@hooks.before('/api/{lmoid}/inputs > Add user inputs > 201 > application/json')
@hooks.before('/api/{lmoid}/inputs > Add user inputs > 400 > application/json')
@if_not_skipped
def create_temp_study(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/study',
                           json={'name': 'Temporary study',
                                 'complexity': "low"})
  transaction['_study_id'] = response.json()['created_study']['id']


@hooks.after('/api/study > Return the list of Logistic and Marine Operations (LMO) studies > 200 > application/json')
@hooks.after('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.after('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.after('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.after('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.after('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 400 > application/json')
@hooks.after('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 404 > application/json')
@hooks.after('/api/{lmoid}/objects > Return the list of Objects > 200 > application/json')
@hooks.after('/api/{lmoid}/objects > Return the list of Objects > 404 > application/json')
@hooks.after('/api/{lmoid}/objects > Add objects > 201 > application/json')
@hooks.after('/api/{lmoid}/objects > Add objects > 404 > application/json')
@hooks.after('/api/{lmoid}/cables > Return the list of Cables > 200 > application/json')
@hooks.after('/api/{lmoid}/cables > Add objects > 404 > application/json')
@hooks.after('/api/{lmoid}/cables > Return the list of Cables > 404 > application/json')
@hooks.after('/api/{lmoid}/cables > Add cables > 201 > application/json')
@hooks.after('/api/{lmoid}/cables > Add cables > 404 > application/json')
@hooks.after('/api/{lmoid}/site > Return the Site > 404 > application/json')
@hooks.after('/api/{lmoid}/inputs > Add user inputs > 201 > application/json')
@hooks.after('/api/{lmoid}/inputs > Add user inputs > 400 > application/json')
@if_not_skipped
def delete_temp_study(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  study_id = transaction['_study_id']
  requests.delete(f'{protocol}//{host}:{port}/api/study/{study_id}')


@hooks.after('/api/study > Add a Logistic and Marine Operations (LMO) study > 201 > application/json')
@if_not_skipped
def delete_study_created_by_test(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  study_id = json.loads(transaction['results']['fields']['body']['values']['actual'])['created_study']['id']
  requests.delete(f'{protocol}//{host}:{port}/api/study/{study_id}')


@hooks.before('/api/{lmoid}/site > Return the Site > 200 > application/json')
@if_not_skipped
def create_temp_study_site(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/study',
                           json={'name': 'Temporary study',
                                 'complexity': "low"})
  transaction['_study_id'] = response.json()['created_study']['id']

  study_id = transaction['_study_id']
  response = requests.post(f'{protocol}//{host}:{port}/api/{study_id}/site')


@hooks.before('/api/{lmoid}/site > Return the Site > 400 > application/json')
@if_not_skipped
def create_invalid_temp_study_site(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/study',
                           json={'name': 'Temporary study',
                                 'complexity': "low"})
  transaction['_study_id'] = response.json()['created_study']['id']

  study_id = transaction['_study_id']
  response = requests.post(f'{protocol}//{host}:{port}/api/{study_id}/site',
                           json={'map_name_wrong': 'Map name'})


@hooks.before('/api/{lmoid}/inputs > Return inputs > 200 > application/json')
@hooks.before('/api/{lmoid}/inputs > Update inputs > 200 > application/json')
@hooks.before('/api/{lmoid}/inputs > Update inputs > 400 > application/json')
@if_not_skipped
def create_temp_study_inputs(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  response = requests.post(f'{protocol}//{host}:{port}/api/study',
                           json={'name': 'Temporary study',
                                 'complexity': "low"})
  transaction['_study_id'] = response.json()['created_study']['id']

  study_id = transaction['_study_id']
  response = requests.post(f'{protocol}//{host}:{port}/api/{study_id}/inputs',
                           json={"period": "month"})


@hooks.after('/api/{lmoid}/site > Return the Site > 200 > application/json')
@hooks.after('/api/{lmoid}/site > Return the Site > 400 > application/json')
@hooks.after('/api/{lmoid}/inputs > Return inputs > 200 > application/json')
@hooks.after('/api/{lmoid}/inputs > Update inputs > 200 > application/json')
@hooks.after('/api/{lmoid}/inputs > Update inputs > 400 > application/json')
@if_not_skipped
def delete_created_study(transaction):
  protocol = transaction['protocol']
  host = transaction['host']
  port = transaction['port']

  study_id = 1
  requests.delete(f'{protocol}//{host}:{port}/api/study/{study_id}')


@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 400 > application/json')
@hooks.before('/api/{lmoid}/inputs > Add user inputs > 400 > application/json')
@hooks.before('/api/{lmoid}/inputs > Update inputs > 400 > application/json')
@if_not_skipped
def delete_request_body(transaction):
  transaction['request']['body'] = json.dumps('{}')


@hooks.before('/api/study > Add a Logistic and Marine Operations (LMO) study > 400 > application/json')
@if_not_skipped
def make_query_invalid(transaction):
  data = json.loads(transaction['request']['body'])
  del data['name']
  transaction['request']['body'] = json.dumps(data)


@hooks.before('/api/study/{lmoid} > Return the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Delete the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 200 > application/json')
@hooks.before('/api/study/{lmoid} > Update the Logistic and Marine Operations (LMO) study > 400 > application/json')
@hooks.before('/api/{lmoid}/objects > Return the list of Objects > 200 > application/json')
@hooks.before('/api/{lmoid}/objects > Add objects > 201 > application/json')
@hooks.before('/api/{lmoid}/cables > Return the list of Cables > 200 > application/json')
@hooks.before('/api/{lmoid}/cables > Add cables > 201 > application/json')
@hooks.before('/api/{lmoid}/site > Return the Site > 200 > application/json')
@hooks.before('/api/{lmoid}/inputs > Return inputs > 200 > application/json')
@hooks.before('/api/{lmoid}/inputs > Add user inputs > 201 > application/json')
@hooks.before('/api/{lmoid}/inputs > Add user inputs > 400 > application/json')
@hooks.before('/api/{lmoid}/inputs > Update inputs > 200 > application/json')
@hooks.before('/api/{lmoid}/inputs > Update inputs > 400 > application/json')
@if_not_skipped
def replace_study_id(transaction):
  study_id = transaction['_study_id']

  uri = transaction['request']['uri']
  full_path = transaction['fullPath']
  if 'study' in uri:
    idx = uri.replace('/api/study/', '')
  else:
    if 'objects' in uri:
      idx = uri.replace('/api/', '').replace('/objects', '')
    if 'cables' in uri:
      idx = uri.replace('/api/', '').replace('/cables', '')
    if 'site' in uri:
      idx = uri.replace('/api/', '').replace('/site', '')
    if 'inputs' in uri:
      idx = uri.replace('/api/', '').replace('/inputs', '')
    if 'phases' in uri:
      idx = uri.replace('/api/', '').replace('/phases', '')
    if 'methods' in uri:
      idx = uri.replace('/api/', '').replace('/methods', '')

  transaction['fullPath'] = full_path.replace(idx, str(study_id))
