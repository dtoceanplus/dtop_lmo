FROM python:3.8-slim-buster

WORKDIR /app

COPY dev-requirements.txt .
COPY setup.py .
COPY src/dtop_lmo/ ./src/dtop_lmo/
COPY ./src/dtop-shared-library/setup.py ./src/dtop-shared-library/setup.py
COPY ./src/dtop-shared-library/dtop_shared_library/ ./src/dtop-shared-library/dtop_shared_library/
COPY ./src/dtop-shared-library/bin/ ./src/dtop-shared-library/bin/
COPY TEMP_DATABASE/Maps ./TEMP_DATABASE/Maps
COPY catalogues ./catalogues

RUN apt-get update && \
    apt-get install --yes --no-install-recommends gcc libc6-dev && \
    tar -czf TEMP_DATABASE_backup.tar.gz TEMP_DATABASE && \
    tar -czf catalogues_backup.tar.gz catalogues && \
    pip install --requirement dev-requirements.txt && \
    (cd ./src/dtop-shared-library && pip install --editable .) && \
    pip install --editable .

ENV FLASK_APP dtop_lmo.service

EXPOSE 5000
