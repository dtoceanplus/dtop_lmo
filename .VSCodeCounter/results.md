# Summary

Date : 2020-07-29 10:10:40

Directory c:\Users\Lamaral\Documents\DTOceanPlus\GitLab\dtop_lmo

Total : 145 files,  7789789 codes, 4483 comments, 2918 blanks, all 7797190 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 21 | 7,774,924 | 0 | 9 | 7,774,933 |
| Python | 99 | 14,245 | 4,466 | 2,723 | 21,434 |
| Markdown | 11 | 397 | 0 | 128 | 525 |
| YAML | 4 | 93 | 13 | 21 | 127 |
| Makefile | 1 | 49 | 0 | 20 | 69 |
| HTML | 3 | 34 | 4 | 3 | 41 |
| toml | 1 | 17 | 0 | 4 | 21 |
| Dockerfile | 2 | 13 | 0 | 9 | 22 |
| pip requirements | 1 | 11 | 0 | 0 | 11 |
| Properties | 2 | 6 | 0 | 1 | 7 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 145 | 7,789,789 | 4,483 | 2,918 | 7,797,190 |
| dtop-shared-library | 23 | 1,421 | 275 | 260 | 1,956 |
| dtop-shared-library\docs | 1 | 142 | 0 | 23 | 165 |
| dtop-shared-library\dtop_shared_library | 7 | 487 | 229 | 78 | 794 |
| dtop-shared-library\tests | 6 | 249 | 43 | 71 | 363 |
| src | 73 | 18,742 | 4,000 | 2,011 | 24,753 |
| src\dtop_lmo | 70 | 18,601 | 3,960 | 1,976 | 24,537 |
| src\dtop_lmo\business | 33 | 7,154 | 2,879 | 1,205 | 11,238 |
| src\dtop_lmo\business\classes | 11 | 2,819 | 1,158 | 379 | 4,356 |
| src\dtop_lmo\business\classes\cpx1 | 4 | 514 | 209 | 79 | 802 |
| src\dtop_lmo\business\classes\cpx3 | 4 | 2,226 | 905 | 282 | 3,413 |
| src\dtop_lmo\business\cpx1 | 4 | 668 | 333 | 152 | 1,153 |
| src\dtop_lmo\business\cpx3 | 6 | 2,179 | 698 | 376 | 3,253 |
| src\dtop_lmo\business\cpx3\travel | 2 | 124 | 26 | 27 | 177 |
| src\dtop_lmo\business\feasibilities | 5 | 615 | 344 | 141 | 1,100 |
| src\dtop_lmo\service | 35 | 11,431 | 1,081 | 768 | 13,280 |
| src\dtop_lmo\service\api | 21 | 2,106 | 912 | 539 | 3,557 |
| src\dtop_lmo\service\api\api | 2 | 11 | 7 | 5 | 23 |
| src\dtop_lmo\service\api\components | 2 | 947 | 321 | 207 | 1,475 |
| src\dtop_lmo\service\api\foo | 2 | 11 | 7 | 8 | 26 |
| src\dtop_lmo\service\api\inputs | 2 | 139 | 67 | 40 | 246 |
| src\dtop_lmo\service\api\operations | 2 | 267 | 95 | 68 | 430 |
| src\dtop_lmo\service\api\phases | 2 | 109 | 88 | 36 | 233 |
| src\dtop_lmo\service\api\results | 2 | 342 | 145 | 83 | 570 |
| src\dtop_lmo\service\api\site | 2 | 127 | 67 | 39 | 233 |
| src\dtop_lmo\service\api\studies | 2 | 122 | 92 | 38 | 252 |
| src\dtop_lmo\service\gui | 4 | 16 | 11 | 18 | 45 |
| src\dtop_lmo\service\gui\foo | 1 | 9 | 11 | 11 | 31 |
| src\dtop_lmo\service\gui\main | 1 | 6 | 0 | 6 | 12 |
| src\dtop_lmo\service\static | 1 | 8,655 | 0 | 0 | 8,655 |
| src\dtop_lmo\service\templates | 4 | 39 | 4 | 5 | 48 |
| src\dtop_lmo\service\templates\api | 1 | 19 | 4 | 1 | 24 |
| src\dtop_lmo\service\templates\gui | 1 | 7 | 0 | 1 | 8 |
| src\functions | 3 | 141 | 40 | 35 | 216 |
| test | 42 | 7,769,410 | 193 | 584 | 7,770,187 |
| test\business | 20 | 3,427 | 190 | 550 | 4,167 |
| test\business\core | 10 | 1,572 | 77 | 318 | 1,967 |
| test\business\installation | 7 | 532 | 9 | 110 | 651 |
| test\business\maintenance | 1 | 912 | 98 | 80 | 1,090 |
| test\service | 2 | 29 | 3 | 20 | 52 |
| test\test_inputs | 19 | 7,765,945 | 0 | 8 | 7,765,953 |
| test\test_inputs\Catalogues | 5 | 350 | 0 | 0 | 350 |
| test\test_inputs\Hierarchies | 3 | 916 | 0 | 0 | 916 |
| test\test_inputs\JSON | 11 | 7,764,679 | 0 | 8 | 7,764,687 |

[details](details.md)