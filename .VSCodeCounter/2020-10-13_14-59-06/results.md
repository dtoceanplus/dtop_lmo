# Summary

Date : 2020-10-13 14:59:06

Directory c:\Users\Lamaral\Documents\DTOceanPlus\GitLab\dtop_lmo

Total : 148 files,  2052392 codes, 5549 comments, 3584 blanks, all 2061525 lines

[details](details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 24 | 2,032,663 | 0 | 15 | 2,032,678 |
| Python | 100 | 19,097 | 5,532 | 3,381 | 28,010 |
| Markdown | 10 | 405 | 0 | 130 | 535 |
| YAML | 5 | 100 | 13 | 22 | 135 |
| Makefile | 1 | 49 | 0 | 20 | 69 |
| HTML | 3 | 34 | 4 | 3 | 41 |
| toml | 1 | 17 | 0 | 4 | 21 |
| Docker | 2 | 13 | 0 | 9 | 22 |
| pip requirements | 1 | 11 | 0 | 0 | 11 |
| Properties | 1 | 3 | 0 | 0 | 3 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 148 | 2,052,392 | 5,549 | 3,584 | 2,061,525 |
| dtop-shared-library | 23 | 1,421 | 275 | 260 | 1,956 |
| dtop-shared-library\docs | 1 | 142 | 0 | 23 | 165 |
| dtop-shared-library\dtop_shared_library | 7 | 487 | 229 | 78 | 794 |
| dtop-shared-library\tests | 6 | 249 | 43 | 71 | 363 |
| src | 70 | 16,846 | 4,921 | 2,435 | 24,202 |
| src\dtop_lmo | 68 | 16,723 | 4,890 | 2,406 | 24,019 |
| src\dtop_lmo\business | 33 | 8,604 | 3,347 | 1,417 | 13,368 |
| src\dtop_lmo\business\classes | 11 | 3,025 | 1,207 | 394 | 4,626 |
| src\dtop_lmo\business\classes\cpx1 | 4 | 635 | 233 | 91 | 959 |
| src\dtop_lmo\business\classes\cpx3 | 4 | 2,306 | 940 | 287 | 3,533 |
| src\dtop_lmo\business\cpx1 | 4 | 1,648 | 656 | 313 | 2,617 |
| src\dtop_lmo\business\cpx3 | 6 | 2,435 | 772 | 410 | 3,617 |
| src\dtop_lmo\business\cpx3\travel | 2 | 125 | 26 | 28 | 179 |
| src\dtop_lmo\business\feasibilities | 5 | 628 | 348 | 142 | 1,118 |
| src\dtop_lmo\service | 33 | 8,103 | 1,543 | 986 | 10,632 |
| src\dtop_lmo\service\api | 19 | 3,143 | 1,378 | 756 | 5,277 |
| src\dtop_lmo\service\api\api | 2 | 11 | 7 | 5 | 23 |
| src\dtop_lmo\service\api\components | 2 | 1,138 | 335 | 257 | 1,730 |
| src\dtop_lmo\service\api\inputs | 2 | 143 | 67 | 43 | 253 |
| src\dtop_lmo\service\api\operations | 2 | 657 | 466 | 188 | 1,311 |
| src\dtop_lmo\service\api\phases | 2 | 143 | 88 | 41 | 272 |
| src\dtop_lmo\service\api\results | 2 | 768 | 232 | 129 | 1,129 |
| src\dtop_lmo\service\api\site | 2 | 130 | 68 | 40 | 238 |
| src\dtop_lmo\service\api\studies | 2 | 122 | 92 | 38 | 252 |
| src\dtop_lmo\service\gui | 4 | 16 | 11 | 18 | 45 |
| src\dtop_lmo\service\gui\foo | 1 | 9 | 11 | 11 | 31 |
| src\dtop_lmo\service\gui\main | 1 | 6 | 0 | 6 | 12 |
| src\dtop_lmo\service\static | 1 | 4,267 | 0 | 1 | 4,268 |
| src\dtop_lmo\service\templates | 4 | 39 | 4 | 5 | 48 |
| src\dtop_lmo\service\templates\api | 1 | 19 | 4 | 1 | 24 |
| src\dtop_lmo\service\templates\gui | 1 | 7 | 0 | 1 | 8 |
| src\functions | 2 | 123 | 31 | 29 | 183 |
| test | 47 | 2,033,867 | 251 | 814 | 2,034,932 |
| test\business | 21 | 5,720 | 246 | 767 | 6,733 |
| test\business\core | 10 | 1,726 | 67 | 336 | 2,129 |
| test\business\installation | 6 | 653 | 7 | 129 | 789 |
| test\business\maintenance | 3 | 2,920 | 166 | 259 | 3,345 |
| test\service | 3 | 66 | 5 | 28 | 99 |
| test\test_inputs | 22 | 2,028,072 | 0 | 13 | 2,028,085 |
| test\test_inputs\Catalogues | 6 | 872 | 0 | 0 | 872 |
| test\test_inputs\Hierarchies | 2 | 422 | 0 | 1 | 423 |
| test\test_inputs\JSON | 13 | 1,675,976 | 0 | 12 | 1,675,988 |
| test\test_inputs\Metocean | 1 | 350,802 | 0 | 0 | 350,802 |

[details](details.md)